(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[0],{

/***/ "./resources/demo2/src/app/modules/ECommerce/_redux/customers/customersActions.js":
/*!****************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/_redux/customers/customersActions.js ***!
  \****************************************************************************************/
/*! exports provided: fetchCustomers, fetchCustomer, deleteCustomer, createCustomer, updateCustomer, updateCustomersStatus, deleteCustomers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchCustomers", function() { return fetchCustomers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchCustomer", function() { return fetchCustomer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteCustomer", function() { return deleteCustomer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createCustomer", function() { return createCustomer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateCustomer", function() { return updateCustomer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateCustomersStatus", function() { return updateCustomersStatus; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteCustomers", function() { return deleteCustomers; });
/* harmony import */ var _customersCrud__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./customersCrud */ "./resources/demo2/src/app/modules/ECommerce/_redux/customers/customersCrud.js");
/* harmony import */ var _customersSlice__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./customersSlice */ "./resources/demo2/src/app/modules/ECommerce/_redux/customers/customersSlice.js");


var actions = _customersSlice__WEBPACK_IMPORTED_MODULE_1__["customersSlice"].actions;
var fetchCustomers = function fetchCustomers(queryParams) {
  return function (dispatch) {
    dispatch(actions.startCall({
      callType: _customersSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].list
    }));
    return _customersCrud__WEBPACK_IMPORTED_MODULE_0__["findCustomers"](queryParams).then(function (response) {
      var _response$data = response.data,
          totalCount = _response$data.totalCount,
          entities = _response$data.entities;
      dispatch(actions.customersFetched({
        totalCount: totalCount,
        entities: entities
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't find customers";
      dispatch(actions.catchError({
        error: error,
        callType: _customersSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].list
      }));
    });
  };
};
var fetchCustomer = function fetchCustomer(id) {
  return function (dispatch) {
    if (!id) {
      return dispatch(actions.customerFetched({
        customerForEdit: undefined
      }));
    }

    dispatch(actions.startCall({
      callType: _customersSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
    }));
    return _customersCrud__WEBPACK_IMPORTED_MODULE_0__["getCustomerById"](id).then(function (response) {
      var customer = response.data;
      dispatch(actions.customerFetched({
        customerForEdit: customer
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't find customer";
      dispatch(actions.catchError({
        error: error,
        callType: _customersSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
      }));
    });
  };
};
var deleteCustomer = function deleteCustomer(id) {
  return function (dispatch) {
    dispatch(actions.startCall({
      callType: _customersSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
    }));
    return _customersCrud__WEBPACK_IMPORTED_MODULE_0__["deleteCustomer"](id).then(function (response) {
      dispatch(actions.customerDeleted({
        id: id
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't delete customer";
      dispatch(actions.catchError({
        error: error,
        callType: _customersSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
      }));
    });
  };
};
var createCustomer = function createCustomer(customerForCreation) {
  return function (dispatch) {
    dispatch(actions.startCall({
      callType: _customersSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
    }));
    return _customersCrud__WEBPACK_IMPORTED_MODULE_0__["createCustomer"](customerForCreation).then(function (response) {
      var customer = response.data.customer;
      dispatch(actions.customerCreated({
        customer: customer
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't create customer";
      dispatch(actions.catchError({
        error: error,
        callType: _customersSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
      }));
    });
  };
};
var updateCustomer = function updateCustomer(customer) {
  return function (dispatch) {
    dispatch(actions.startCall({
      callType: _customersSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
    }));
    return _customersCrud__WEBPACK_IMPORTED_MODULE_0__["updateCustomer"](customer).then(function () {
      dispatch(actions.customerUpdated({
        customer: customer
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't update customer";
      dispatch(actions.catchError({
        error: error,
        callType: _customersSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
      }));
    });
  };
};
var updateCustomersStatus = function updateCustomersStatus(ids, status) {
  return function (dispatch) {
    dispatch(actions.startCall({
      callType: _customersSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
    }));
    return _customersCrud__WEBPACK_IMPORTED_MODULE_0__["updateStatusForCustomers"](ids, status).then(function () {
      dispatch(actions.customersStatusUpdated({
        ids: ids,
        status: status
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't update customers status";
      dispatch(actions.catchError({
        error: error,
        callType: _customersSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
      }));
    });
  };
};
var deleteCustomers = function deleteCustomers(ids) {
  return function (dispatch) {
    dispatch(actions.startCall({
      callType: _customersSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
    }));
    return _customersCrud__WEBPACK_IMPORTED_MODULE_0__["deleteCustomers"](ids).then(function () {
      dispatch(actions.customersDeleted({
        ids: ids
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't delete customers";
      dispatch(actions.catchError({
        error: error,
        callType: _customersSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
      }));
    });
  };
};

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/_redux/customers/customersCrud.js":
/*!*************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/_redux/customers/customersCrud.js ***!
  \*************************************************************************************/
/*! exports provided: CUSTOMERS_URL, createCustomer, getAllCustomers, getCustomerById, findCustomers, updateCustomer, updateStatusForCustomers, deleteCustomer, deleteCustomers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CUSTOMERS_URL", function() { return CUSTOMERS_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createCustomer", function() { return createCustomer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAllCustomers", function() { return getAllCustomers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCustomerById", function() { return getCustomerById; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "findCustomers", function() { return findCustomers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateCustomer", function() { return updateCustomer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateStatusForCustomers", function() { return updateStatusForCustomers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteCustomer", function() { return deleteCustomer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteCustomers", function() { return deleteCustomers; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);

var CUSTOMERS_URL = "api/customers"; // CREATE =>  POST: add a new customer to the server

function createCustomer(customer) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(CUSTOMERS_URL, {
    customer: customer
  });
} // READ

function getAllCustomers() {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(CUSTOMERS_URL);
}
function getCustomerById(customerId) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("".concat(CUSTOMERS_URL, "/").concat(customerId));
} // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result

function findCustomers(queryParams) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("".concat(CUSTOMERS_URL, "/find"), {
    queryParams: queryParams
  });
} // UPDATE => PUT: update the customer on the server

function updateCustomer(customer) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.put("".concat(CUSTOMERS_URL, "/").concat(customer.id), {
    customer: customer
  });
} // UPDATE Status

function updateStatusForCustomers(ids, status) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("".concat(CUSTOMERS_URL, "/updateStatusForCustomers"), {
    ids: ids,
    status: status
  });
} // DELETE => delete the customer from the server

function deleteCustomer(customerId) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a["delete"]("".concat(CUSTOMERS_URL, "/").concat(customerId));
} // DELETE Customers by ids

function deleteCustomers(ids) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("".concat(CUSTOMERS_URL, "/deleteCustomers"), {
    ids: ids
  });
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/_redux/customers/customersSlice.js":
/*!**************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/_redux/customers/customersSlice.js ***!
  \**************************************************************************************/
/*! exports provided: callTypes, customersSlice */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "callTypes", function() { return callTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "customersSlice", function() { return customersSlice; });
!(function webpackMissingModule() { var e = new Error("Cannot find module '@reduxjs/toolkit'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());

var initialCustomersState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  customerForEdit: undefined,
  lastError: null
};
var callTypes = {
  list: "list",
  action: "action"
};
var customersSlice = !(function webpackMissingModule() { var e = new Error("Cannot find module '@reduxjs/toolkit'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())({
  name: "customers",
  initialState: initialCustomersState,
  reducers: {
    catchError: function catchError(state, action) {
      state.error = "".concat(action.type, ": ").concat(action.payload.error);

      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: function startCall(state, action) {
      state.error = null;

      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getCustomerById
    customerFetched: function customerFetched(state, action) {
      state.actionsLoading = false;
      state.customerForEdit = action.payload.customerForEdit;
      state.error = null;
    },
    // findCustomers
    customersFetched: function customersFetched(state, action) {
      var _action$payload = action.payload,
          totalCount = _action$payload.totalCount,
          entities = _action$payload.entities;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createCustomer
    customerCreated: function customerCreated(state, action) {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.customer);
    },
    // updateCustomer
    customerUpdated: function customerUpdated(state, action) {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(function (entity) {
        if (entity.id === action.payload.customer.id) {
          return action.payload.customer;
        }

        return entity;
      });
    },
    // deleteCustomer
    customerDeleted: function customerDeleted(state, action) {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(function (el) {
        return el.id !== action.payload.id;
      });
    },
    // deleteCustomers
    customersDeleted: function customersDeleted(state, action) {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(function (el) {
        return !action.payload.ids.includes(el.id);
      });
    },
    // customersUpdateState
    customersStatusUpdated: function customersStatusUpdated(state, action) {
      state.actionsLoading = false;
      state.error = null;
      var _action$payload2 = action.payload,
          ids = _action$payload2.ids,
          status = _action$payload2.status;
      state.entities = state.entities.map(function (entity) {
        if (ids.findIndex(function (id) {
          return id === entity.id;
        }) > -1) {
          entity.status = status;
        }

        return entity;
      });
    }
  }
});

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/_redux/products/productsActions.js":
/*!**************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/_redux/products/productsActions.js ***!
  \**************************************************************************************/
/*! exports provided: fetchProducts, fetchProduct, deleteProduct, createProduct, updateProduct, updateProductsStatus, deleteProducts */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchProducts", function() { return fetchProducts; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchProduct", function() { return fetchProduct; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteProduct", function() { return deleteProduct; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createProduct", function() { return createProduct; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateProduct", function() { return updateProduct; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateProductsStatus", function() { return updateProductsStatus; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteProducts", function() { return deleteProducts; });
/* harmony import */ var _productsCrud__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./productsCrud */ "./resources/demo2/src/app/modules/ECommerce/_redux/products/productsCrud.js");
/* harmony import */ var _productsSlice__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./productsSlice */ "./resources/demo2/src/app/modules/ECommerce/_redux/products/productsSlice.js");


var actions = _productsSlice__WEBPACK_IMPORTED_MODULE_1__["productsSlice"].actions;
var fetchProducts = function fetchProducts(queryParams) {
  return function (dispatch) {
    dispatch(actions.startCall({
      callType: _productsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].list
    }));
    return _productsCrud__WEBPACK_IMPORTED_MODULE_0__["findProducts"](queryParams).then(function (response) {
      var _response$data = response.data,
          totalCount = _response$data.totalCount,
          entities = _response$data.entities;
      dispatch(actions.productsFetched({
        totalCount: totalCount,
        entities: entities
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't find products";
      dispatch(actions.catchError({
        error: error,
        callType: _productsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].list
      }));
    });
  };
};
var fetchProduct = function fetchProduct(id) {
  return function (dispatch) {
    if (!id) {
      return dispatch(actions.productFetched({
        productForEdit: undefined
      }));
    }

    dispatch(actions.startCall({
      callType: _productsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
    }));
    return _productsCrud__WEBPACK_IMPORTED_MODULE_0__["getProductById"](id).then(function (response) {
      var product = response.data;
      dispatch(actions.productFetched({
        productForEdit: product
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't find product";
      dispatch(actions.catchError({
        error: error,
        callType: _productsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
      }));
    });
  };
};
var deleteProduct = function deleteProduct(id) {
  return function (dispatch) {
    dispatch(actions.startCall({
      callType: _productsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
    }));
    return _productsCrud__WEBPACK_IMPORTED_MODULE_0__["deleteProduct"](id).then(function (response) {
      dispatch(actions.productDeleted({
        id: id
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't delete product";
      dispatch(actions.catchError({
        error: error,
        callType: _productsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
      }));
    });
  };
};
var createProduct = function createProduct(productForCreation) {
  return function (dispatch) {
    dispatch(actions.startCall({
      callType: _productsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
    }));
    return _productsCrud__WEBPACK_IMPORTED_MODULE_0__["createProduct"](productForCreation).then(function (response) {
      var product = response.data.product;
      dispatch(actions.productCreated({
        product: product
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't create product";
      dispatch(actions.catchError({
        error: error,
        callType: _productsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
      }));
    });
  };
};
var updateProduct = function updateProduct(product) {
  return function (dispatch) {
    dispatch(actions.startCall({
      callType: _productsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
    }));
    return _productsCrud__WEBPACK_IMPORTED_MODULE_0__["updateProduct"](product).then(function () {
      dispatch(actions.productUpdated({
        product: product
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't update product";
      dispatch(actions.catchError({
        error: error,
        callType: _productsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
      }));
    });
  };
};
var updateProductsStatus = function updateProductsStatus(ids, status) {
  return function (dispatch) {
    dispatch(actions.startCall({
      callType: _productsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
    }));
    return _productsCrud__WEBPACK_IMPORTED_MODULE_0__["updateStatusForProducts"](ids, status).then(function () {
      dispatch(actions.productsStatusUpdated({
        ids: ids,
        status: status
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't update products status";
      dispatch(actions.catchError({
        error: error,
        callType: _productsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
      }));
    });
  };
};
var deleteProducts = function deleteProducts(ids) {
  return function (dispatch) {
    dispatch(actions.startCall({
      callType: _productsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
    }));
    return _productsCrud__WEBPACK_IMPORTED_MODULE_0__["deleteProducts"](ids).then(function () {
      dispatch(actions.productsDeleted({
        ids: ids
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't delete products";
      dispatch(actions.catchError({
        error: error,
        callType: _productsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
      }));
    });
  };
};

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/_redux/products/productsCrud.js":
/*!***********************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/_redux/products/productsCrud.js ***!
  \***********************************************************************************/
/*! exports provided: PRODUCTS_URL, createProduct, getAllProducts, getProductById, findProducts, updateProduct, updateStatusForProducts, deleteProduct, deleteProducts */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PRODUCTS_URL", function() { return PRODUCTS_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createProduct", function() { return createProduct; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAllProducts", function() { return getAllProducts; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getProductById", function() { return getProductById; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "findProducts", function() { return findProducts; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateProduct", function() { return updateProduct; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateStatusForProducts", function() { return updateStatusForProducts; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteProduct", function() { return deleteProduct; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteProducts", function() { return deleteProducts; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);

var PRODUCTS_URL = "api/products"; // CREATE =>  POST: add a new product to the server

function createProduct(product) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(PRODUCTS_URL, {
    product: product
  });
} // READ

function getAllProducts() {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(PRODUCTS_URL);
}
function getProductById(productId) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("".concat(PRODUCTS_URL, "/").concat(productId));
} // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result

function findProducts(queryParams) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("".concat(PRODUCTS_URL, "/find"), {
    queryParams: queryParams
  });
} // UPDATE => PUT: update the procuct on the server

function updateProduct(product) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.put("".concat(PRODUCTS_URL, "/").concat(product.id), {
    product: product
  });
} // UPDATE Status

function updateStatusForProducts(ids, status) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("".concat(PRODUCTS_URL, "/updateStatusForProducts"), {
    ids: ids,
    status: status
  });
} // DELETE => delete the product from the server

function deleteProduct(productId) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a["delete"]("".concat(PRODUCTS_URL, "/").concat(productId));
} // DELETE Products by ids

function deleteProducts(ids) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("".concat(PRODUCTS_URL, "/deleteProducts"), {
    ids: ids
  });
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/_redux/products/productsSlice.js":
/*!************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/_redux/products/productsSlice.js ***!
  \************************************************************************************/
/*! exports provided: callTypes, productsSlice */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "callTypes", function() { return callTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "productsSlice", function() { return productsSlice; });
!(function webpackMissingModule() { var e = new Error("Cannot find module '@reduxjs/toolkit'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());

var initialProductsState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  productForEdit: undefined,
  lastError: null
};
var callTypes = {
  list: "list",
  action: "action"
};
var productsSlice = !(function webpackMissingModule() { var e = new Error("Cannot find module '@reduxjs/toolkit'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())({
  name: "products",
  initialState: initialProductsState,
  reducers: {
    catchError: function catchError(state, action) {
      state.error = "".concat(action.type, ": ").concat(action.payload.error);

      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: function startCall(state, action) {
      state.error = null;

      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getProductById
    productFetched: function productFetched(state, action) {
      state.actionsLoading = false;
      state.productForEdit = action.payload.productForEdit;
      state.error = null;
    },
    // findProducts
    productsFetched: function productsFetched(state, action) {
      var _action$payload = action.payload,
          totalCount = _action$payload.totalCount,
          entities = _action$payload.entities;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createProduct
    productCreated: function productCreated(state, action) {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.product);
    },
    // updateProduct
    productUpdated: function productUpdated(state, action) {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(function (entity) {
        if (entity.id === action.payload.product.id) {
          return action.payload.product;
        }

        return entity;
      });
    },
    // deleteProduct
    productDeleted: function productDeleted(state, action) {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(function (el) {
        return el.id !== action.payload.id;
      });
    },
    // deleteProducts
    productsDeleted: function productsDeleted(state, action) {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(function (el) {
        return !action.payload.ids.includes(el.id);
      });
    },
    // productsUpdateState
    productsStatusUpdated: function productsStatusUpdated(state, action) {
      state.actionsLoading = false;
      state.error = null;
      var _action$payload2 = action.payload,
          ids = _action$payload2.ids,
          status = _action$payload2.status;
      state.entities = state.entities.map(function (entity) {
        if (ids.findIndex(function (id) {
          return id === entity.id;
        }) > -1) {
          entity.status = status;
        }

        return entity;
      });
    }
  }
});

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/_redux/remarks/remarksActions.js":
/*!************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/_redux/remarks/remarksActions.js ***!
  \************************************************************************************/
/*! exports provided: fetchRemarks, fetchRemark, deleteRemark, createRemark, updateRemark, deleteRemarks */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchRemarks", function() { return fetchRemarks; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchRemark", function() { return fetchRemark; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteRemark", function() { return deleteRemark; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createRemark", function() { return createRemark; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateRemark", function() { return updateRemark; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteRemarks", function() { return deleteRemarks; });
/* harmony import */ var _remarksCrud__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./remarksCrud */ "./resources/demo2/src/app/modules/ECommerce/_redux/remarks/remarksCrud.js");
/* harmony import */ var _remarksSlice__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./remarksSlice */ "./resources/demo2/src/app/modules/ECommerce/_redux/remarks/remarksSlice.js");


var actions = _remarksSlice__WEBPACK_IMPORTED_MODULE_1__["remarksSlice"].actions;
var fetchRemarks = function fetchRemarks(queryParams, productId) {
  return function (dispatch) {
    dispatch(actions.startCall({
      callType: _remarksSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].list
    }));

    if (!productId) {
      return dispatch(actions.remarksFetched({
        totalCount: 0,
        entities: null
      }));
    }

    return _remarksCrud__WEBPACK_IMPORTED_MODULE_0__["findRemarks"](queryParams, productId).then(function (response) {
      var _response$data = response.data,
          totalCount = _response$data.totalCount,
          entities = _response$data.entities;
      dispatch(actions.remarksFetched({
        totalCount: totalCount,
        entities: entities
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't find remarks";
      dispatch(actions.catchError({
        error: error,
        callType: _remarksSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].list
      }));
    });
  };
};
var fetchRemark = function fetchRemark(id) {
  return function (dispatch) {
    if (!id) {
      return dispatch(actions.remarkFetched({
        remarkForEdit: undefined
      }));
    }

    dispatch(actions.startCall({
      callType: _remarksSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
    }));
    return _remarksCrud__WEBPACK_IMPORTED_MODULE_0__["getRemarkById"](id).then(function (response) {
      var remark = response.data;
      dispatch(actions.remarkFetched({
        remarkForEdit: remark
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't find remark";
      dispatch(actions.catchError({
        error: error,
        callType: _remarksSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
      }));
    });
  };
};
var deleteRemark = function deleteRemark(id) {
  return function (dispatch) {
    dispatch(actions.startCall({
      callType: _remarksSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
    }));
    return _remarksCrud__WEBPACK_IMPORTED_MODULE_0__["deleteRemark"](id).then(function (response) {
      dispatch(actions.remarkDeleted({
        id: id
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't delete remark";
      dispatch(actions.catchError({
        error: error,
        callType: _remarksSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
      }));
    });
  };
};
var createRemark = function createRemark(remarkForCreation) {
  return function (dispatch) {
    dispatch(actions.startCall({
      callType: _remarksSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
    }));
    return _remarksCrud__WEBPACK_IMPORTED_MODULE_0__["createRemark"](remarkForCreation).then(function (response) {
      var remark = response.data.remark;
      dispatch(actions.remarkCreated({
        remark: remark
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't create remark";
      dispatch(actions.catchError({
        error: error,
        callType: _remarksSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
      }));
    });
  };
};
var updateRemark = function updateRemark(remark) {
  return function (dispatch) {
    dispatch(actions.startCall({
      callType: _remarksSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
    }));
    return _remarksCrud__WEBPACK_IMPORTED_MODULE_0__["updateRemark"](remark).then(function () {
      dispatch(actions.remarkUpdated({
        remark: remark
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't update remark";
      dispatch(actions.catchError({
        error: error,
        callType: _remarksSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
      }));
    });
  };
};
var deleteRemarks = function deleteRemarks(ids) {
  return function (dispatch) {
    dispatch(actions.startCall({
      callType: _remarksSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
    }));
    return _remarksCrud__WEBPACK_IMPORTED_MODULE_0__["deleteRemarks"](ids).then(function () {
      console.log("delete return");
      dispatch(actions.remarksDeleted({
        ids: ids
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't delete remarks";
      dispatch(actions.catchError({
        error: error,
        callType: _remarksSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
      }));
    });
  };
};

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/_redux/remarks/remarksCrud.js":
/*!*********************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/_redux/remarks/remarksCrud.js ***!
  \*********************************************************************************/
/*! exports provided: REMARKS_URL, createRemark, getAllProductRemarksByProductId, getRemarkById, findRemarks, updateRemark, deleteRemark, deleteRemarks */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "REMARKS_URL", function() { return REMARKS_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createRemark", function() { return createRemark; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAllProductRemarksByProductId", function() { return getAllProductRemarksByProductId; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getRemarkById", function() { return getRemarkById; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "findRemarks", function() { return findRemarks; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateRemark", function() { return updateRemark; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteRemark", function() { return deleteRemark; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteRemarks", function() { return deleteRemarks; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);

var REMARKS_URL = "api/remarks"; // CREATE =>  POST: add a new remark to the server

function createRemark(remark) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(REMARKS_URL, {
    remark: remark
  });
} // READ
// Server should return filtered remarks by productId

function getAllProductRemarksByProductId(productId) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("".concat(REMARKS_URL, "?productId=").concat(productId));
}
function getRemarkById(remarkId) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("".concat(REMARKS_URL, "/").concat(remarkId));
} // Server should return sorted/filtered remarks and merge with items from state
// TODO: Change your URL to REAL API, right now URL is 'api/remarksfind/{productId}'. Should be 'api/remarks/find/{productId}'!!!

function findRemarks(queryParams, productId) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("".concat(REMARKS_URL, "find/").concat(productId), {
    queryParams: queryParams
  });
} // UPDATE => PUT: update the remark

function updateRemark(remark) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.put("".concat(REMARKS_URL, "/").concat(remark.id), {
    remark: remark
  });
} // DELETE => delete the remark

function deleteRemark(remarkId) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a["delete"]("".concat(REMARKS_URL, "/").concat(remarkId));
} // DELETE Remarks by ids

function deleteRemarks(ids) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("".concat(REMARKS_URL, "/deleteRemarks"), {
    ids: ids
  });
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/_redux/remarks/remarksSlice.js":
/*!**********************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/_redux/remarks/remarksSlice.js ***!
  \**********************************************************************************/
/*! exports provided: callTypes, remarksSlice */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "callTypes", function() { return callTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "remarksSlice", function() { return remarksSlice; });
!(function webpackMissingModule() { var e = new Error("Cannot find module '@reduxjs/toolkit'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());

var initialRemarksState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  remarkForEdit: undefined,
  lastError: null
};
var callTypes = {
  list: "list",
  action: "action"
};
var remarksSlice = !(function webpackMissingModule() { var e = new Error("Cannot find module '@reduxjs/toolkit'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())({
  name: "remarks",
  initialState: initialRemarksState,
  reducers: {
    catchError: function catchError(state, action) {
      state.error = "".concat(action.type, ": ").concat(action.payload.error);

      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: function startCall(state, action) {
      state.error = null;

      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getRemarkById
    remarkFetched: function remarkFetched(state, action) {
      state.actionsLoading = false;
      state.remarkForEdit = action.payload.remarkForEdit;
      state.error = null;
    },
    // findRemarks
    remarksFetched: function remarksFetched(state, action) {
      var _action$payload = action.payload,
          totalCount = _action$payload.totalCount,
          entities = _action$payload.entities;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createRemark
    remarkCreated: function remarkCreated(state, action) {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.remark);
    },
    // updateRemark
    remarkUpdated: function remarkUpdated(state, action) {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(function (entity) {
        if (entity.id === action.payload.remark.id) {
          return action.payload.remark;
        }

        return entity;
      });
    },
    // deleteRemark
    remarkDeleted: function remarkDeleted(state, action) {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(function (el) {
        return el.id !== action.payload.id;
      });
    },
    // deleteRemarks
    remarksDeleted: function remarksDeleted(state, action) {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(function (el) {
        return !action.payload.ids.includes(el.id);
      });
    },
    // remarksUpdateState
    remarksStatusUpdated: function remarksStatusUpdated(state, action) {
      state.actionsLoading = false;
      state.error = null;
      var _action$payload2 = action.payload,
          ids = _action$payload2.ids,
          status = _action$payload2.status;
      state.entities = state.entities.map(function (entity) {
        if (ids.findIndex(function (id) {
          return id === entity.id;
        }) > -1) {
          entity.status = status;
        }

        return entity;
      });
    }
  }
});

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/_redux/specifications/specificationsActions.js":
/*!**************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/_redux/specifications/specificationsActions.js ***!
  \**************************************************************************************************/
/*! exports provided: fetchSpecifications, fetchSpecification, deleteSpecification, createSpecification, updateSpecification, deleteSpecifications */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchSpecifications", function() { return fetchSpecifications; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchSpecification", function() { return fetchSpecification; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteSpecification", function() { return deleteSpecification; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createSpecification", function() { return createSpecification; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateSpecification", function() { return updateSpecification; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteSpecifications", function() { return deleteSpecifications; });
/* harmony import */ var _specificationsCrud__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./specificationsCrud */ "./resources/demo2/src/app/modules/ECommerce/_redux/specifications/specificationsCrud.js");
/* harmony import */ var _specificationsSlice__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./specificationsSlice */ "./resources/demo2/src/app/modules/ECommerce/_redux/specifications/specificationsSlice.js");


var actions = _specificationsSlice__WEBPACK_IMPORTED_MODULE_1__["specificationsSlice"].actions;
var fetchSpecifications = function fetchSpecifications(queryParams, productId) {
  return function (dispatch) {
    dispatch(actions.startCall({
      callType: _specificationsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].list
    }));

    if (!productId) {
      return dispatch(actions.specificationsFetched({
        totalCount: 0,
        entities: null
      }));
    }

    return _specificationsCrud__WEBPACK_IMPORTED_MODULE_0__["findSpecifications"](queryParams, productId).then(function (response) {
      var _response$data = response.data,
          totalCount = _response$data.totalCount,
          entities = _response$data.entities;
      dispatch(actions.specificationsFetched({
        totalCount: totalCount,
        entities: entities
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't find specifications";
      dispatch(actions.catchError({
        error: error,
        callType: _specificationsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].list
      }));
    });
  };
};
var fetchSpecification = function fetchSpecification(id) {
  return function (dispatch) {
    if (!id) {
      return dispatch(actions.specificationFetched({
        specificationForEdit: undefined
      }));
    }

    dispatch(actions.startCall({
      callType: _specificationsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
    }));
    return _specificationsCrud__WEBPACK_IMPORTED_MODULE_0__["getSpecificationById"](id).then(function (response) {
      var specification = response.data;
      dispatch(actions.specificationFetched({
        specificationForEdit: specification
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't find specification";
      dispatch(actions.catchError({
        error: error,
        callType: _specificationsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
      }));
    });
  };
};
var deleteSpecification = function deleteSpecification(id) {
  return function (dispatch) {
    dispatch(actions.startCall({
      callType: _specificationsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
    }));
    return _specificationsCrud__WEBPACK_IMPORTED_MODULE_0__["deleteSpecification"](id).then(function (response) {
      dispatch(actions.specificationDeleted({
        id: id
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't delete specification";
      dispatch(actions.catchError({
        error: error,
        callType: _specificationsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
      }));
    });
  };
};
var createSpecification = function createSpecification(specificationForCreation) {
  return function (dispatch) {
    dispatch(actions.startCall({
      callType: _specificationsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
    }));
    return _specificationsCrud__WEBPACK_IMPORTED_MODULE_0__["createSpecification"](specificationForCreation).then(function (response) {
      var specification = response.data.specification;
      dispatch(actions.specificationCreated({
        specification: specification
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't create specification";
      dispatch(actions.catchError({
        error: error,
        callType: _specificationsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
      }));
    });
  };
};
var updateSpecification = function updateSpecification(specification) {
  return function (dispatch) {
    dispatch(actions.startCall({
      callType: _specificationsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
    }));
    return _specificationsCrud__WEBPACK_IMPORTED_MODULE_0__["updateSpecification"](specification).then(function () {
      dispatch(actions.specificationUpdated({
        specification: specification
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't update specification";
      dispatch(actions.catchError({
        error: error,
        callType: _specificationsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
      }));
    });
  };
};
var deleteSpecifications = function deleteSpecifications(ids) {
  return function (dispatch) {
    dispatch(actions.startCall({
      callType: _specificationsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
    }));
    return _specificationsCrud__WEBPACK_IMPORTED_MODULE_0__["deleteSpecifications"](ids).then(function () {
      dispatch(actions.specificationsDeleted({
        ids: ids
      }));
    })["catch"](function (error) {
      error.clientMessage = "Can't delete specifications";
      dispatch(actions.catchError({
        error: error,
        callType: _specificationsSlice__WEBPACK_IMPORTED_MODULE_1__["callTypes"].action
      }));
    });
  };
};

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/_redux/specifications/specificationsCrud.js":
/*!***********************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/_redux/specifications/specificationsCrud.js ***!
  \***********************************************************************************************/
/*! exports provided: SPECIFICATIONS_URL, createSpecification, getAllProductSpecificationsByProductId, getSpecificationById, findSpecifications, updateSpecification, deleteSpecification, deleteSpecifications */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SPECIFICATIONS_URL", function() { return SPECIFICATIONS_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createSpecification", function() { return createSpecification; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAllProductSpecificationsByProductId", function() { return getAllProductSpecificationsByProductId; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getSpecificationById", function() { return getSpecificationById; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "findSpecifications", function() { return findSpecifications; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateSpecification", function() { return updateSpecification; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteSpecification", function() { return deleteSpecification; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteSpecifications", function() { return deleteSpecifications; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);

var SPECIFICATIONS_URL = "api/specifications"; // CREATE =>  POST: add a new specifications to the server

function createSpecification(specification) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(SPECIFICATIONS_URL, {
    specification: specification
  });
} // READ
// Server should return filtered specifications by productId

function getAllProductSpecificationsByProductId(productId) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("".concat(SPECIFICATIONS_URL, "?productId=").concat(productId));
}
function getSpecificationById(specificationId) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("".concat(SPECIFICATIONS_URL, "/").concat(specificationId));
} // Server should return sorted/filtered specifications and merge with items from state
// TODO: Change your URL to REAL API, right now URL is 'api/specificationsfind/{productId}'. Should be 'api/specifications/find/{productId}'!!!

function findSpecifications(queryParams, productId) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("".concat(SPECIFICATIONS_URL, "find/").concat(productId), {
    queryParams: queryParams
  });
} // UPDATE => PUT: update the specification

function updateSpecification(specification) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.put("".concat(SPECIFICATIONS_URL, "/").concat(specification.id), {
    specification: specification
  });
} // DELETE => delete the specification

function deleteSpecification(specificationId) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a["delete"]("".concat(SPECIFICATIONS_URL, "/").concat(specificationId));
} // DELETE specifications by ids

function deleteSpecifications(ids) {
  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("".concat(SPECIFICATIONS_URL, "/deleteSpecifications"), {
    ids: ids
  });
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/_redux/specifications/specificationsSlice.js":
/*!************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/_redux/specifications/specificationsSlice.js ***!
  \************************************************************************************************/
/*! exports provided: callTypes, specificationsSlice */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "callTypes", function() { return callTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "specificationsSlice", function() { return specificationsSlice; });
!(function webpackMissingModule() { var e = new Error("Cannot find module '@reduxjs/toolkit'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());

var initialSpecificationsState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  specificationForEdit: undefined,
  lastError: null
};
var callTypes = {
  list: "list",
  action: "action"
};
var specificationsSlice = !(function webpackMissingModule() { var e = new Error("Cannot find module '@reduxjs/toolkit'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())({
  name: "specifications",
  initialState: initialSpecificationsState,
  reducers: {
    catchError: function catchError(state, action) {
      state.error = "".concat(action.type, ": ").concat(action.payload.error);

      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: function startCall(state, action) {
      state.error = null;

      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getSpecificationById
    specificationFetched: function specificationFetched(state, action) {
      state.actionsLoading = false;
      state.specificationForEdit = action.payload.specificationForEdit;
      state.error = null;
    },
    // findSpecifications
    specificationsFetched: function specificationsFetched(state, action) {
      var _action$payload = action.payload,
          totalCount = _action$payload.totalCount,
          entities = _action$payload.entities;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createSpecification
    specificationCreated: function specificationCreated(state, action) {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.specification);
    },
    // updateSpecification
    specificationUpdated: function specificationUpdated(state, action) {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(function (entity) {
        if (entity.id === action.payload.specification.id) {
          return action.payload.specification;
        }

        return entity;
      });
    },
    // deleteSpecification
    specificationDeleted: function specificationDeleted(state, action) {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(function (el) {
        return el.id !== action.payload.id;
      });
    },
    // deleteSpecifications
    specificationsDeleted: function specificationsDeleted(state, action) {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(function (el) {
        return !action.payload.ids.includes(el.id);
      });
    }
  }
});

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersCard.js":
/*!************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersCard.js ***!
  \************************************************************************************/
/*! exports provided: CustomersCard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomersCard", function() { return CustomersCard; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");
/* harmony import */ var _customers_filter_CustomersFilter__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./customers-filter/CustomersFilter */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-filter/CustomersFilter.js");
/* harmony import */ var _customers_table_CustomersTable__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./customers-table/CustomersTable */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-table/CustomersTable.js");
/* harmony import */ var _customers_grouping_CustomersGrouping__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./customers-grouping/CustomersGrouping */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-grouping/CustomersGrouping.js");
/* harmony import */ var _CustomersUIContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./CustomersUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersUIContext.js");






function CustomersCard() {
  var customersUIContext = Object(_CustomersUIContext__WEBPACK_IMPORTED_MODULE_5__["useCustomersUIContext"])();
  var customersUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      ids: customersUIContext.ids,
      newCustomerButtonClick: customersUIContext.newCustomerButtonClick
    };
  }, [customersUIContext]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_1__["Card"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_1__["CardHeader"], {
    title: "Customers list"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_1__["CardHeaderToolbar"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    className: "btn btn-primary",
    onClick: customersUIProps.newCustomerButtonClick
  }, "New Customer"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_1__["CardBody"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_customers_filter_CustomersFilter__WEBPACK_IMPORTED_MODULE_2__["CustomersFilter"], null), customersUIProps.ids.length > 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_customers_grouping_CustomersGrouping__WEBPACK_IMPORTED_MODULE_4__["CustomersGrouping"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_customers_table_CustomersTable__WEBPACK_IMPORTED_MODULE_3__["CustomersTable"], null)));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersPage.js":
/*!************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersPage.js ***!
  \************************************************************************************/
/*! exports provided: CustomersPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomersPage", function() { return CustomersPage; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-router-dom'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _customers_loading_dialog_CustomersLoadingDialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./customers-loading-dialog/CustomersLoadingDialog */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-loading-dialog/CustomersLoadingDialog.js");
/* harmony import */ var _customer_edit_dialog_CustomerEditDialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./customer-edit-dialog/CustomerEditDialog */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customer-edit-dialog/CustomerEditDialog.js");
/* harmony import */ var _customer_delete_dialog_CustomerDeleteDialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./customer-delete-dialog/CustomerDeleteDialog */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customer-delete-dialog/CustomerDeleteDialog.js");
/* harmony import */ var _customers_delete_dialog_CustomersDeleteDialog__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./customers-delete-dialog/CustomersDeleteDialog */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-delete-dialog/CustomersDeleteDialog.js");
/* harmony import */ var _customers_fetch_dialog_CustomersFetchDialog__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./customers-fetch-dialog/CustomersFetchDialog */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-fetch-dialog/CustomersFetchDialog.js");
/* harmony import */ var _customers_update_status_dialog_CustomersUpdateStateDialog__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./customers-update-status-dialog/CustomersUpdateStateDialog */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-update-status-dialog/CustomersUpdateStateDialog.js");
/* harmony import */ var _CustomersUIContext__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./CustomersUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersUIContext.js");
/* harmony import */ var _CustomersCard__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./CustomersCard */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersCard.js");










function CustomersPage(_ref) {
  var history = _ref.history;
  var customersUIEvents = {
    newCustomerButtonClick: function newCustomerButtonClick() {
      history.push("/e-commerce/customers/new");
    },
    openEditCustomerDialog: function openEditCustomerDialog(id) {
      history.push("/e-commerce/customers/".concat(id, "/edit"));
    },
    openDeleteCustomerDialog: function openDeleteCustomerDialog(id) {
      history.push("/e-commerce/customers/".concat(id, "/delete"));
    },
    openDeleteCustomersDialog: function openDeleteCustomersDialog() {
      history.push("/e-commerce/customers/deleteCustomers");
    },
    openFetchCustomersDialog: function openFetchCustomersDialog() {
      history.push("/e-commerce/customers/fetch");
    },
    openUpdateCustomersStatusDialog: function openUpdateCustomersStatusDialog() {
      history.push("/e-commerce/customers/updateStatus");
    }
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_CustomersUIContext__WEBPACK_IMPORTED_MODULE_8__["CustomersUIProvider"], {
    customersUIEvents: customersUIEvents
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_customers_loading_dialog_CustomersLoadingDialog__WEBPACK_IMPORTED_MODULE_2__["CustomersLoadingDialog"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-router-dom'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    path: "/e-commerce/customers/new"
  }, function (_ref2) {
    var history = _ref2.history,
        match = _ref2.match;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_customer_edit_dialog_CustomerEditDialog__WEBPACK_IMPORTED_MODULE_3__["CustomerEditDialog"], {
      show: match != null,
      onHide: function onHide() {
        history.push("/e-commerce/customers");
      }
    });
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-router-dom'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    path: "/e-commerce/customers/:id/edit"
  }, function (_ref3) {
    var history = _ref3.history,
        match = _ref3.match;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_customer_edit_dialog_CustomerEditDialog__WEBPACK_IMPORTED_MODULE_3__["CustomerEditDialog"], {
      show: match != null,
      id: match && match.params.id,
      onHide: function onHide() {
        history.push("/e-commerce/customers");
      }
    });
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-router-dom'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    path: "/e-commerce/customers/deleteCustomers"
  }, function (_ref4) {
    var history = _ref4.history,
        match = _ref4.match;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_customers_delete_dialog_CustomersDeleteDialog__WEBPACK_IMPORTED_MODULE_5__["CustomersDeleteDialog"], {
      show: match != null,
      onHide: function onHide() {
        history.push("/e-commerce/customers");
      }
    });
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-router-dom'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    path: "/e-commerce/customers/:id/delete"
  }, function (_ref5) {
    var history = _ref5.history,
        match = _ref5.match;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_customer_delete_dialog_CustomerDeleteDialog__WEBPACK_IMPORTED_MODULE_4__["CustomerDeleteDialog"], {
      show: match != null,
      id: match && match.params.id,
      onHide: function onHide() {
        history.push("/e-commerce/customers");
      }
    });
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-router-dom'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    path: "/e-commerce/customers/fetch"
  }, function (_ref6) {
    var history = _ref6.history,
        match = _ref6.match;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_customers_fetch_dialog_CustomersFetchDialog__WEBPACK_IMPORTED_MODULE_6__["CustomersFetchDialog"], {
      show: match != null,
      onHide: function onHide() {
        history.push("/e-commerce/customers");
      }
    });
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-router-dom'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    path: "/e-commerce/customers/updateStatus"
  }, function (_ref7) {
    var history = _ref7.history,
        match = _ref7.match;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_customers_update_status_dialog_CustomersUpdateStateDialog__WEBPACK_IMPORTED_MODULE_7__["CustomersUpdateStateDialog"], {
      show: match != null,
      onHide: function onHide() {
        history.push("/e-commerce/customers");
      }
    });
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_CustomersCard__WEBPACK_IMPORTED_MODULE_9__["CustomersCard"], null));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersUIContext.js":
/*!*****************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersUIContext.js ***!
  \*****************************************************************************************/
/*! exports provided: useCustomersUIContext, CustomersUIConsumer, CustomersUIProvider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useCustomersUIContext", function() { return useCustomersUIContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomersUIConsumer", function() { return CustomersUIConsumer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomersUIProvider", function() { return CustomersUIProvider; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _CustomersUIHelpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./CustomersUIHelpers */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersUIHelpers.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }




var CustomersUIContext = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_0__["createContext"])();
function useCustomersUIContext() {
  return Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(CustomersUIContext);
}
var CustomersUIConsumer = CustomersUIContext.Consumer;
function CustomersUIProvider(_ref) {
  var customersUIEvents = _ref.customersUIEvents,
      children = _ref.children;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(_CustomersUIHelpers__WEBPACK_IMPORTED_MODULE_2__["initialFilter"]),
      _useState2 = _slicedToArray(_useState, 2),
      queryParams = _useState2[0],
      setQueryParamsBase = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState4 = _slicedToArray(_useState3, 2),
      ids = _useState4[0],
      setIds = _useState4[1];

  var setQueryParams = Object(react__WEBPACK_IMPORTED_MODULE_0__["useCallback"])(function (nextQueryParams) {
    setQueryParamsBase(function (prevQueryParams) {
      if (Object(lodash__WEBPACK_IMPORTED_MODULE_1__["isFunction"])(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (Object(lodash__WEBPACK_IMPORTED_MODULE_1__["isEqual"])(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);
  var initCustomer = {
    id: undefined,
    firstName: "",
    lastName: "",
    email: "",
    userName: "",
    gender: "Female",
    status: 0,
    dateOfBbirth: "",
    ipAddress: "",
    type: 1
  };
  var value = {
    queryParams: queryParams,
    setQueryParamsBase: setQueryParamsBase,
    ids: ids,
    setIds: setIds,
    setQueryParams: setQueryParams,
    initCustomer: initCustomer,
    newCustomerButtonClick: customersUIEvents.newCustomerButtonClick,
    openEditCustomerDialog: customersUIEvents.openEditCustomerDialog,
    openDeleteCustomerDialog: customersUIEvents.openDeleteCustomerDialog,
    openDeleteCustomersDialog: customersUIEvents.openDeleteCustomersDialog,
    openFetchCustomersDialog: customersUIEvents.openFetchCustomersDialog,
    openUpdateCustomersStatusDialog: customersUIEvents.openUpdateCustomersStatusDialog
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(CustomersUIContext.Provider, {
    value: value
  }, children);
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersUIHelpers.js":
/*!*****************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersUIHelpers.js ***!
  \*****************************************************************************************/
/*! exports provided: CustomerStatusCssClasses, CustomerStatusTitles, CustomerTypeCssClasses, CustomerTypeTitles, defaultSorted, sizePerPageList, initialFilter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerStatusCssClasses", function() { return CustomerStatusCssClasses; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerStatusTitles", function() { return CustomerStatusTitles; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerTypeCssClasses", function() { return CustomerTypeCssClasses; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerTypeTitles", function() { return CustomerTypeTitles; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "defaultSorted", function() { return defaultSorted; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sizePerPageList", function() { return sizePerPageList; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initialFilter", function() { return initialFilter; });
var CustomerStatusCssClasses = ["danger", "success", "info", ""];
var CustomerStatusTitles = ["Suspended", "Active", "Pending", ""];
var CustomerTypeCssClasses = ["success", "primary", ""];
var CustomerTypeTitles = ["Business", "Individual", ""];
var defaultSorted = [{
  dataField: "id",
  order: "asc"
}];
var sizePerPageList = [{
  text: "3",
  value: 3
}, {
  text: "5",
  value: 5
}, {
  text: "10",
  value: 10
}];
var initialFilter = {
  filter: {
    lastName: "",
    firstName: "",
    email: "",
    ipAddress: ""
  },
  sortOrder: "asc",
  // asc||desc
  sortField: "id",
  pageNumber: 1,
  pageSize: 10
};

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customer-delete-dialog/CustomerDeleteDialog.js":
/*!******************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/customers/customer-delete-dialog/CustomerDeleteDialog.js ***!
  \******************************************************************************************************************/
/*! exports provided: CustomerDeleteDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerDeleteDialog", function() { return CustomerDeleteDialog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");
/* harmony import */ var _redux_customers_customersActions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../_redux/customers/customersActions */ "./resources/demo2/src/app/modules/ECommerce/_redux/customers/customersActions.js");
/* harmony import */ var _CustomersUIContext__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../CustomersUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersUIContext.js");






function CustomerDeleteDialog(_ref) {
  var id = _ref.id,
      show = _ref.show,
      onHide = _ref.onHide;
  // Customers UI Context
  var customersUIContext = Object(_CustomersUIContext__WEBPACK_IMPORTED_MODULE_4__["useCustomersUIContext"])();
  var customersUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      setIds: customersUIContext.setIds,
      queryParams: customersUIContext.queryParams
    };
  }, [customersUIContext]); // Customers Redux state

  var dispatch = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())();

  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      isLoading: state.customers.actionsLoading
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      isLoading = _useSelector.isLoading; // if !id we should close modal


  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (!id) {
      onHide();
    } // eslint-disable-next-line react-hooks/exhaustive-deps

  }, [id]); // looking for loading/dispatch

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {}, [isLoading, dispatch]);

  var deleteCustomer = function deleteCustomer() {
    // server request for deleting customer by id
    dispatch(_redux_customers_customersActions__WEBPACK_IMPORTED_MODULE_3__["deleteCustomer"](id)).then(function () {
      // refresh list after deletion
      dispatch(_redux_customers_customersActions__WEBPACK_IMPORTED_MODULE_3__["fetchCustomers"](customersUIProps.queryParams)); // clear selections list

      customersUIProps.setIds([]); // closing delete modal

      onHide();
    });
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    show: show,
    onHide: onHide,
    "aria-labelledby": "example-modal-sizes-title-lg"
  }, isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["ModalProgressBar"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Header, {
    closeButton: true
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Title, {
    id: "example-modal-sizes-title-lg"
  }, "Customer Delete")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Body, null, !isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Are you sure to permanently delete this customer?"), isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Customer is deleting...")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Footer, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: onHide,
    className: "btn btn-light btn-elevate"
  }, "Cancel"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: deleteCustomer,
    className: "btn btn-primary btn-elevate"
  }, "Delete"))));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customer-edit-dialog/CustomerEditDialog.js":
/*!**************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/customers/customer-edit-dialog/CustomerEditDialog.js ***!
  \**************************************************************************************************************/
/*! exports provided: CustomerEditDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerEditDialog", function() { return CustomerEditDialog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _redux_customers_customersActions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../_redux/customers/customersActions */ "./resources/demo2/src/app/modules/ECommerce/_redux/customers/customersActions.js");
/* harmony import */ var _CustomerEditDialogHeader__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./CustomerEditDialogHeader */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customer-edit-dialog/CustomerEditDialogHeader.js");
/* harmony import */ var _CustomerEditForm__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./CustomerEditForm */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customer-edit-dialog/CustomerEditForm.js");
/* harmony import */ var _CustomersUIContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../CustomersUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersUIContext.js");







function CustomerEditDialog(_ref) {
  var id = _ref.id,
      show = _ref.show,
      onHide = _ref.onHide;
  // Customers UI Context
  var customersUIContext = Object(_CustomersUIContext__WEBPACK_IMPORTED_MODULE_5__["useCustomersUIContext"])();
  var customersUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      initCustomer: customersUIContext.initCustomer
    };
  }, [customersUIContext]); // Customers Redux state

  var dispatch = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())();

  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      actionsLoading: state.customers.actionsLoading,
      customerForEdit: state.customers.customerForEdit
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      actionsLoading = _useSelector.actionsLoading,
      customerForEdit = _useSelector.customerForEdit;

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    // server call for getting Customer by id
    dispatch(_redux_customers_customersActions__WEBPACK_IMPORTED_MODULE_2__["fetchCustomer"](id));
  }, [id, dispatch]); // server request for saving customer

  var saveCustomer = function saveCustomer(customer) {
    if (!id) {
      // server request for creating customer
      dispatch(_redux_customers_customersActions__WEBPACK_IMPORTED_MODULE_2__["createCustomer"](customer)).then(function () {
        return onHide();
      });
    } else {
      // server request for updating customer
      dispatch(_redux_customers_customersActions__WEBPACK_IMPORTED_MODULE_2__["updateCustomer"](customer)).then(function () {
        return onHide();
      });
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    size: "lg",
    show: show,
    onHide: onHide,
    "aria-labelledby": "example-modal-sizes-title-lg"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_CustomerEditDialogHeader__WEBPACK_IMPORTED_MODULE_3__["CustomerEditDialogHeader"], {
    id: id
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_CustomerEditForm__WEBPACK_IMPORTED_MODULE_4__["CustomerEditForm"], {
    saveCustomer: saveCustomer,
    actionsLoading: actionsLoading,
    customer: customerForEdit || customersUIProps.initCustomer,
    onHide: onHide
  }));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customer-edit-dialog/CustomerEditDialogHeader.js":
/*!********************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/customers/customer-edit-dialog/CustomerEditDialogHeader.js ***!
  \********************************************************************************************************************/
/*! exports provided: CustomerEditDialogHeader */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerEditDialogHeader", function() { return CustomerEditDialogHeader; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }





function CustomerEditDialogHeader(_ref) {
  var id = _ref.id;

  // Customers Redux state
  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      customerForEdit: state.customers.customerForEdit,
      actionsLoading: state.customers.actionsLoading
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      customerForEdit = _useSelector.customerForEdit,
      actionsLoading = _useSelector.actionsLoading;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState2 = _slicedToArray(_useState, 2),
      title = _useState2[0],
      setTitle = _useState2[1]; // Title couting


  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    var _title = id ? "" : "New Customer";

    if (customerForEdit && id) {
      _title = "Edit customer '".concat(customerForEdit.firstName, " ").concat(customerForEdit.lastName, "'");
    }

    setTitle(_title); // eslint-disable-next-line
  }, [customerForEdit, actionsLoading]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, actionsLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["ModalProgressBar"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Header, {
    closeButton: true
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Title, {
    id: "example-modal-sizes-title-lg"
  }, title)));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customer-edit-dialog/CustomerEditForm.js":
/*!************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/customers/customer-edit-dialog/CustomerEditForm.js ***!
  \************************************************************************************************************/
/*! exports provided: CustomerEditForm */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerEditForm", function() { return CustomerEditForm; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");
// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10




 // Validation schema

var CustomerEditSchema = !(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())().shape({
  firstName: !(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())().min(3, "Minimum 3 symbols").max(50, "Maximum 50 symbols").required("Firstname is required"),
  lastName: !(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())().min(3, "Minimum 3 symbols").max(50, "Maximum 50 symbols").required("Lastname is required"),
  email: !(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())().email("Invalid email").required("Email is required"),
  userName: !(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())().required("Username is required"),
  dateOfBbirth: !(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())().nullable(false).required("Date of Birth is required"),
  ipAddress: !(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())().required("IP Address is required")
});
function CustomerEditForm(_ref) {
  var saveCustomer = _ref.saveCustomer,
      customer = _ref.customer,
      actionsLoading = _ref.actionsLoading,
      onHide = _ref.onHide;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    enableReinitialize: true,
    initialValues: customer,
    validationSchema: CustomerEditSchema,
    onSubmit: function onSubmit(values) {
      saveCustomer(values);
    }
  }, function (_ref2) {
    var handleSubmit = _ref2.handleSubmit;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Body, {
      className: "overlay overlay-block cursor-default"
    }, actionsLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "overlay-layer bg-transparent"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "spinner spinner-lg spinner-success"
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
      className: "form form-label-right"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "form-group row"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-4"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
      name: "firstName",
      component: _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["Input"],
      placeholder: "First Name",
      label: "First Name"
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-4"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
      name: "lastName",
      component: _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["Input"],
      placeholder: "Last Name",
      label: "Last Name"
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-4"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
      name: "userName",
      component: _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["Input"],
      placeholder: "Login",
      label: "Login"
    }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "form-group row"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-4"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
      type: "email",
      name: "email",
      component: _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["Input"],
      placeholder: "Email",
      label: "Email"
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-4"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["DatePickerField"], {
      name: "dateOfBbirth",
      label: "Date of Birth"
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-4"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
      name: "ipAddress",
      component: _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["Input"],
      placeholder: "IP Address",
      label: "IP Address",
      customFeedbackLabel: "We'll never share customer IP Address with anyone else"
    }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "form-group row"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-4"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["Select"], {
      name: "Gender",
      label: "Gender"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      value: "Female"
    }, "Female"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      value: "Male"
    }, "Male"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-4"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["Select"], {
      name: "type",
      label: "Type"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      value: "0"
    }, "Business"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      value: "1"
    }, "Individual")))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Footer, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      type: "button",
      onClick: onHide,
      className: "btn btn-light btn-elevate"
    }, "Cancel"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      type: "submit",
      onClick: function onClick() {
        return handleSubmit();
      },
      className: "btn btn-primary btn-elevate"
    }, "Save")));
  }));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-delete-dialog/CustomersDeleteDialog.js":
/*!********************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-delete-dialog/CustomersDeleteDialog.js ***!
  \********************************************************************************************************************/
/*! exports provided: CustomersDeleteDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomersDeleteDialog", function() { return CustomersDeleteDialog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _redux_customers_customersActions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../_redux/customers/customersActions */ "./resources/demo2/src/app/modules/ECommerce/_redux/customers/customersActions.js");
/* harmony import */ var _CustomersUIContext__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../CustomersUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersUIContext.js");
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");






function CustomersDeleteDialog(_ref) {
  var show = _ref.show,
      onHide = _ref.onHide;
  // Customers UI Context
  var customersUIContext = Object(_CustomersUIContext__WEBPACK_IMPORTED_MODULE_3__["useCustomersUIContext"])();
  var customersUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      ids: customersUIContext.ids,
      setIds: customersUIContext.setIds,
      queryParams: customersUIContext.queryParams
    };
  }, [customersUIContext]); // Customers Redux state

  var dispatch = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())();

  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      isLoading: state.customers.actionsLoading
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      isLoading = _useSelector.isLoading; // if customers weren't selected we should close modal


  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (!customersUIProps.ids || customersUIProps.ids.length === 0) {
      onHide();
    } // eslint-disable-next-line react-hooks/exhaustive-deps

  }, [customersUIProps.ids]); // looking for loading/dispatch

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {}, [isLoading, dispatch]);

  var deleteCustomers = function deleteCustomers() {
    // server request for deleting customer by selected ids
    dispatch(_redux_customers_customersActions__WEBPACK_IMPORTED_MODULE_2__["deleteCustomers"](customersUIProps.ids)).then(function () {
      // refresh list after deletion
      dispatch(_redux_customers_customersActions__WEBPACK_IMPORTED_MODULE_2__["fetchCustomers"](customersUIProps.queryParams)).then(function () {
        // clear selections list
        customersUIProps.setIds([]); // closing delete modal

        onHide();
      });
    });
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    show: show,
    onHide: onHide,
    "aria-labelledby": "example-modal-sizes-title-lg"
  }, isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_4__["ModalProgressBar"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Header, {
    closeButton: true
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Title, {
    id: "example-modal-sizes-title-lg"
  }, "Customers Delete")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Body, null, !isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Are you sure to permanently delete selected customers?"), isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Customer are deleting...")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Footer, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: onHide,
    className: "btn btn-light btn-elevate"
  }, "Cancel"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: deleteCustomers,
    className: "btn btn-primary btn-elevate"
  }, "Delete"))));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-fetch-dialog/CustomersFetchDialog.js":
/*!******************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-fetch-dialog/CustomersFetchDialog.js ***!
  \******************************************************************************************************************/
/*! exports provided: CustomersFetchDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomersFetchDialog", function() { return CustomersFetchDialog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _CustomersUIHelpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../CustomersUIHelpers */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersUIHelpers.js");
/* harmony import */ var _CustomersUIContext__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../CustomersUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersUIContext.js");






var selectedCustomers = function selectedCustomers(entities, ids) {
  var _customers = [];
  ids.forEach(function (id) {
    var customer = entities.find(function (el) {
      return el.id === id;
    });

    if (customer) {
      _customers.push(customer);
    }
  });
  return _customers;
};

function CustomersFetchDialog(_ref) {
  var show = _ref.show,
      onHide = _ref.onHide;
  // Customers UI Context
  var customersUIContext = Object(_CustomersUIContext__WEBPACK_IMPORTED_MODULE_3__["useCustomersUIContext"])();
  var customersUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      ids: customersUIContext.ids
    };
  }, [customersUIContext]); // Customers Redux state

  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      customers: selectedCustomers(state.customers.entities, customersUIProps.ids)
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      customers = _useSelector.customers; // if customers weren't selected we should close modal


  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (!customersUIProps.ids || customersUIProps.ids.length === 0) {
      onHide();
    } // eslint-disable-next-line react-hooks/exhaustive-deps

  }, [customersUIProps.ids]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    show: show,
    onHide: onHide,
    "aria-labelledby": "example-modal-sizes-title-lg"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Header, {
    closeButton: true
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Title, {
    id: "example-modal-sizes-title-lg"
  }, "Fetch selected elements")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Body, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "timeline timeline-5 mt-3"
  }, customers.map(function (customer) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "timeline-item align-items-start",
      key: "id".concat(customer.id)
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "timeline-label font-weight-bolder text-dark-75 font-size-lg text-right pr-3"
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "timeline-badge"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
      className: "fa fa-genderless text-".concat(_CustomersUIHelpers__WEBPACK_IMPORTED_MODULE_2__["CustomerStatusCssClasses"][customer.status], " icon-xxl")
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "timeline-content text-dark-50 mr-5"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "label label-lg label-light-".concat(_CustomersUIHelpers__WEBPACK_IMPORTED_MODULE_2__["CustomerStatusCssClasses"][customer.status], " label-inline")
    }, "ID: ", customer.id), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "ml-3"
    }, customer.lastName, ", ", customer.firstName)));
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Footer, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: onHide,
    className: "btn btn-light btn-elevate"
  }, "Cancel"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: onHide,
    className: "btn btn-primary btn-elevate"
  }, "Ok"))));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-filter/CustomersFilter.js":
/*!*******************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-filter/CustomersFilter.js ***!
  \*******************************************************************************************************/
/*! exports provided: CustomersFilter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomersFilter", function() { return CustomersFilter; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _CustomersUIContext__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../CustomersUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersUIContext.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






var prepareFilter = function prepareFilter(queryParams, values) {
  var status = values.status,
      type = values.type,
      searchText = values.searchText;

  var newQueryParams = _objectSpread({}, queryParams);

  var filter = {}; // Filter by status

  filter.status = status !== "" ? +status : undefined; // Filter by type

  filter.type = type !== "" ? +type : undefined; // Filter by all fields

  filter.lastName = searchText;

  if (searchText) {
    filter.firstName = searchText;
    filter.email = searchText;
    filter.ipAddress = searchText;
  }

  newQueryParams.filter = filter;
  return newQueryParams;
};

function CustomersFilter(_ref) {
  var listLoading = _ref.listLoading;
  // Customers UI Context
  var customersUIContext = Object(_CustomersUIContext__WEBPACK_IMPORTED_MODULE_3__["useCustomersUIContext"])();
  var customersUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      queryParams: customersUIContext.queryParams,
      setQueryParams: customersUIContext.setQueryParams
    };
  }, [customersUIContext]); // queryParams, setQueryParams,

  var applyFilter = function applyFilter(values) {
    var newQueryParams = prepareFilter(customersUIProps.queryParams, values);

    if (!Object(lodash__WEBPACK_IMPORTED_MODULE_2__["isEqual"])(newQueryParams, customersUIProps.queryParams)) {
      newQueryParams.pageNumber = 1; // update list by queryParams

      customersUIProps.setQueryParams(newQueryParams);
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    initialValues: {
      status: "",
      // values => All=""/Susspended=0/Active=1/Pending=2
      type: "",
      // values => All=""/Business=0/Individual=1
      searchText: ""
    },
    onSubmit: function onSubmit(values) {
      applyFilter(values);
    }
  }, function (_ref2) {
    var values = _ref2.values,
        handleSubmit = _ref2.handleSubmit,
        handleBlur = _ref2.handleBlur,
        handleChange = _ref2.handleChange,
        setFieldValue = _ref2.setFieldValue;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
      onSubmit: handleSubmit,
      className: "form form-label-right"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "form-group row"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-2"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("select", {
      className: "form-control",
      name: "status",
      placeholder: "Filter by Status" // TODO: Change this code
      ,
      onChange: function onChange(e) {
        setFieldValue("status", e.target.value);
        handleSubmit();
      },
      onBlur: handleBlur,
      value: values.status
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      value: ""
    }, "All"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      value: "0"
    }, "Susspended"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      value: "1"
    }, "Active"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      value: "2"
    }, "Pending")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("small", {
      className: "form-text text-muted"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("b", null, "Filter"), " by Status")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-2"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("select", {
      className: "form-control",
      placeholder: "Filter by Type",
      name: "type",
      onBlur: handleBlur,
      onChange: function onChange(e) {
        setFieldValue("type", e.target.value);
        handleSubmit();
      },
      value: values.type
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      value: ""
    }, "All"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      value: "0"
    }, "Business"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      value: "1"
    }, "Individual")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("small", {
      className: "form-text text-muted"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("b", null, "Filter"), " by Type")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-2"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      type: "text",
      className: "form-control",
      name: "searchText",
      placeholder: "Search",
      onBlur: handleBlur,
      value: values.searchText,
      onChange: function onChange(e) {
        setFieldValue("searchText", e.target.value);
        handleSubmit();
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("small", {
      className: "form-text text-muted"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("b", null, "Search"), " in all fields"))));
  }));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-grouping/CustomersGrouping.js":
/*!***********************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-grouping/CustomersGrouping.js ***!
  \***********************************************************************************************************/
/*! exports provided: CustomersGrouping */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomersGrouping", function() { return CustomersGrouping; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _CustomersUIContext__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../CustomersUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersUIContext.js");


function CustomersGrouping() {
  // Customers UI Context
  var customersUIContext = Object(_CustomersUIContext__WEBPACK_IMPORTED_MODULE_1__["useCustomersUIContext"])();
  var customersUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      ids: customersUIContext.ids,
      setIds: customersUIContext.setIds,
      openDeleteCustomersDialog: customersUIContext.openDeleteCustomersDialog,
      openFetchCustomersDialog: customersUIContext.openFetchCustomersDialog,
      openUpdateCustomersStatusDialog: customersUIContext.openUpdateCustomersStatusDialog
    };
  }, [customersUIContext]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "row align-items-center form-group-actions margin-top-20 margin-bottom-20"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-xl-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form-group form-group-inline"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form-label form-label-no-wrap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "font-bold font-danger"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Selected records count: ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("b", null, customersUIProps.ids.length)))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    className: "btn btn-danger font-weight-bolder font-size-sm",
    onClick: customersUIProps.openDeleteCustomersDialog
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-trash"
  }), " Delete All"), "\xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    className: "btn btn-light-primary font-weight-bolder font-size-sm",
    onClick: customersUIProps.openFetchCustomersDialog
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-stream"
  }), " Fetch Selected"), "\xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    className: "btn btn-light-primary font-weight-bolder font-size-sm",
    onClick: customersUIProps.openUpdateCustomersStatusDialog
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-sync-alt"
  }), " Update Status"))))));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-loading-dialog/CustomersLoadingDialog.js":
/*!**********************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-loading-dialog/CustomersLoadingDialog.js ***!
  \**********************************************************************************************************************/
/*! exports provided: CustomersLoadingDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomersLoadingDialog", function() { return CustomersLoadingDialog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");



function CustomersLoadingDialog() {
  // Customers Redux state
  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      isLoading: state.customers.listLoading
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      isLoading = _useSelector.isLoading; // looking for loading/dispatch


  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {}, [isLoading]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["LoadingDialog"], {
    isLoading: isLoading,
    text: "Loading ..."
  });
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-table/CustomersTable.js":
/*!*****************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-table/CustomersTable.js ***!
  \*****************************************************************************************************/
/*! exports provided: CustomersTable */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomersTable", function() { return CustomersTable; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap-table-next'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap-table2-paginator'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _redux_customers_customersActions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../_redux/customers/customersActions */ "./resources/demo2/src/app/modules/ECommerce/_redux/customers/customersActions.js");
/* harmony import */ var _metronic_helpers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../_metronic/_helpers */ "./resources/demo2/src/_metronic/_helpers/index.js");
/* harmony import */ var _CustomersUIHelpers__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../CustomersUIHelpers */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersUIHelpers.js");
/* harmony import */ var _column_formatters__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./column-formatters */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-table/column-formatters/index.js");
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");
/* harmony import */ var _CustomersUIContext__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../CustomersUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersUIContext.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html










function CustomersTable() {
  // Customers UI Context
  var customersUIContext = Object(_CustomersUIContext__WEBPACK_IMPORTED_MODULE_7__["useCustomersUIContext"])();
  var customersUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      ids: customersUIContext.ids,
      setIds: customersUIContext.setIds,
      queryParams: customersUIContext.queryParams,
      setQueryParams: customersUIContext.setQueryParams,
      openEditCustomerDialog: customersUIContext.openEditCustomerDialog,
      openDeleteCustomerDialog: customersUIContext.openDeleteCustomerDialog
    };
  }, [customersUIContext]); // Getting curret state of customers list from store (Redux)

  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      currentState: state.customers
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      currentState = _useSelector.currentState;

  var totalCount = currentState.totalCount,
      entities = currentState.entities,
      listLoading = currentState.listLoading; // Customers Redux state

  var dispatch = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())();
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    // clear selections list
    customersUIProps.setIds([]); // server call by queryParams

    dispatch(_redux_customers_customersActions__WEBPACK_IMPORTED_MODULE_2__["fetchCustomers"](customersUIProps.queryParams)); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [customersUIProps.queryParams, dispatch]); // Table columns

  var columns = [{
    dataField: "id",
    text: "ID",
    sort: true,
    sortCaret: _metronic_helpers__WEBPACK_IMPORTED_MODULE_3__["sortCaret"],
    headerSortingClasses: _metronic_helpers__WEBPACK_IMPORTED_MODULE_3__["headerSortingClasses"]
  }, {
    dataField: "firstName",
    text: "Firstname",
    sort: true,
    sortCaret: _metronic_helpers__WEBPACK_IMPORTED_MODULE_3__["sortCaret"],
    headerSortingClasses: _metronic_helpers__WEBPACK_IMPORTED_MODULE_3__["headerSortingClasses"]
  }, {
    dataField: "lastName",
    text: "Lastname",
    sort: true,
    sortCaret: _metronic_helpers__WEBPACK_IMPORTED_MODULE_3__["sortCaret"],
    headerSortingClasses: _metronic_helpers__WEBPACK_IMPORTED_MODULE_3__["headerSortingClasses"]
  }, {
    dataField: "email",
    text: "Email",
    sort: true,
    sortCaret: _metronic_helpers__WEBPACK_IMPORTED_MODULE_3__["sortCaret"],
    headerSortingClasses: _metronic_helpers__WEBPACK_IMPORTED_MODULE_3__["headerSortingClasses"]
  }, {
    dataField: "gender",
    text: "Gender",
    sort: false,
    sortCaret: _metronic_helpers__WEBPACK_IMPORTED_MODULE_3__["sortCaret"]
  }, {
    dataField: "status",
    text: "Status",
    sort: true,
    sortCaret: _metronic_helpers__WEBPACK_IMPORTED_MODULE_3__["sortCaret"],
    formatter: _column_formatters__WEBPACK_IMPORTED_MODULE_5__["StatusColumnFormatter"],
    headerSortingClasses: _metronic_helpers__WEBPACK_IMPORTED_MODULE_3__["headerSortingClasses"]
  }, {
    dataField: "type",
    text: "Type",
    sort: true,
    sortCaret: _metronic_helpers__WEBPACK_IMPORTED_MODULE_3__["sortCaret"],
    formatter: _column_formatters__WEBPACK_IMPORTED_MODULE_5__["TypeColumnFormatter"]
  }, {
    dataField: "action",
    text: "Actions",
    formatter: _column_formatters__WEBPACK_IMPORTED_MODULE_5__["ActionsColumnFormatter"],
    formatExtraData: {
      openEditCustomerDialog: customersUIProps.openEditCustomerDialog,
      openDeleteCustomerDialog: customersUIProps.openDeleteCustomerDialog
    },
    classes: "text-right pr-0",
    headerClasses: "text-right pr-3",
    style: {
      minWidth: "100px"
    }
  }]; // Table pagination properties

  var paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: _CustomersUIHelpers__WEBPACK_IMPORTED_MODULE_4__["sizePerPageList"],
    sizePerPage: customersUIProps.queryParams.pageSize,
    page: customersUIProps.queryParams.pageNumber
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap-table2-paginator'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    pagination: !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap-table2-paginator'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(paginationOptions)
  }, function (_ref) {
    var paginationProps = _ref.paginationProps,
        paginationTableProps = _ref.paginationTableProps;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_6__["Pagination"], {
      isLoading: listLoading,
      paginationProps: paginationProps
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap-table-next'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), _extends({
      wrapperClasses: "table-responsive",
      bordered: false,
      classes: "table table-head-custom table-vertical-center overflow-hidden",
      bootstrap4: true,
      remote: true,
      keyField: "id",
      data: entities === null ? [] : entities,
      columns: columns,
      defaultSorted: _CustomersUIHelpers__WEBPACK_IMPORTED_MODULE_4__["defaultSorted"],
      onTableChange: Object(_metronic_helpers__WEBPACK_IMPORTED_MODULE_3__["getHandlerTableChange"])(customersUIProps.setQueryParams),
      selectRow: Object(_metronic_helpers__WEBPACK_IMPORTED_MODULE_3__["getSelectRow"])({
        entities: entities,
        ids: customersUIProps.ids,
        setIds: customersUIProps.setIds
      })
    }, paginationTableProps), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_helpers__WEBPACK_IMPORTED_MODULE_3__["PleaseWaitMessage"], {
      entities: entities
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_helpers__WEBPACK_IMPORTED_MODULE_3__["NoRecordsFoundMessage"], {
      entities: entities
    })));
  }));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-table/column-formatters/ActionsColumnFormatter.js":
/*!*******************************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-table/column-formatters/ActionsColumnFormatter.js ***!
  \*******************************************************************************************************************************/
/*! exports provided: ActionsColumnFormatter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionsColumnFormatter", function() { return ActionsColumnFormatter; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-inlinesvg'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _metronic_helpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../_metronic/_helpers */ "./resources/demo2/src/_metronic/_helpers/index.js");
// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel

/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */



function ActionsColumnFormatter(cellContent, row, rowIndex, _ref) {
  var openEditCustomerDialog = _ref.openEditCustomerDialog,
      openDeleteCustomerDialog = _ref.openDeleteCustomerDialog;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    title: "Edit customer",
    className: "btn btn-icon btn-light btn-hover-primary btn-sm mx-3",
    onClick: function onClick() {
      return openEditCustomerDialog(row.id);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "svg-icon svg-icon-md svg-icon-primary"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-inlinesvg'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    src: Object(_metronic_helpers__WEBPACK_IMPORTED_MODULE_2__["toAbsoluteUrl"])("/media/svg/icons/Communication/Write.svg")
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    title: "Delete customer",
    className: "btn btn-icon btn-light btn-hover-danger btn-sm",
    onClick: function onClick() {
      return openDeleteCustomerDialog(row.id);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "svg-icon svg-icon-md svg-icon-danger"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-inlinesvg'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    src: Object(_metronic_helpers__WEBPACK_IMPORTED_MODULE_2__["toAbsoluteUrl"])("/media/svg/icons/General/Trash.svg")
  }))));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-table/column-formatters/StatusColumnFormatter.js":
/*!******************************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-table/column-formatters/StatusColumnFormatter.js ***!
  \******************************************************************************************************************************/
/*! exports provided: StatusColumnFormatter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatusColumnFormatter", function() { return StatusColumnFormatter; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _CustomersUIHelpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../CustomersUIHelpers */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersUIHelpers.js");
// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel


function StatusColumnFormatter(cellContent, row) {
  var getLabelCssClasses = function getLabelCssClasses() {
    return "label label-lg label-light-".concat(_CustomersUIHelpers__WEBPACK_IMPORTED_MODULE_1__["CustomerStatusCssClasses"][row.status], " label-inline");
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: getLabelCssClasses()
  }, _CustomersUIHelpers__WEBPACK_IMPORTED_MODULE_1__["CustomerStatusTitles"][row.status]);
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-table/column-formatters/TypeColumnFormatter.js":
/*!****************************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-table/column-formatters/TypeColumnFormatter.js ***!
  \****************************************************************************************************************************/
/*! exports provided: TypeColumnFormatter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TypeColumnFormatter", function() { return TypeColumnFormatter; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _CustomersUIHelpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../CustomersUIHelpers */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersUIHelpers.js");
// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel


function TypeColumnFormatter(cellContent, row) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "label label-dot label-".concat(_CustomersUIHelpers__WEBPACK_IMPORTED_MODULE_1__["CustomerTypeCssClasses"][row.type], " mr-2")
  }), "\xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "font-bold font-".concat(_CustomersUIHelpers__WEBPACK_IMPORTED_MODULE_1__["CustomerTypeCssClasses"][row.type])
  }, _CustomersUIHelpers__WEBPACK_IMPORTED_MODULE_1__["CustomerTypeTitles"][row.type]));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-table/column-formatters/index.js":
/*!**************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-table/column-formatters/index.js ***!
  \**************************************************************************************************************/
/*! exports provided: StatusColumnFormatter, TypeColumnFormatter, ActionsColumnFormatter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _StatusColumnFormatter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./StatusColumnFormatter */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-table/column-formatters/StatusColumnFormatter.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "StatusColumnFormatter", function() { return _StatusColumnFormatter__WEBPACK_IMPORTED_MODULE_0__["StatusColumnFormatter"]; });

/* harmony import */ var _TypeColumnFormatter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TypeColumnFormatter */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-table/column-formatters/TypeColumnFormatter.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TypeColumnFormatter", function() { return _TypeColumnFormatter__WEBPACK_IMPORTED_MODULE_1__["TypeColumnFormatter"]; });

/* harmony import */ var _ActionsColumnFormatter__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ActionsColumnFormatter */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-table/column-formatters/ActionsColumnFormatter.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ActionsColumnFormatter", function() { return _ActionsColumnFormatter__WEBPACK_IMPORTED_MODULE_2__["ActionsColumnFormatter"]; });

// TODO: Rename all formatters




/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-update-status-dialog/CustomersUpdateStateDialog.js":
/*!********************************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/customers/customers-update-status-dialog/CustomersUpdateStateDialog.js ***!
  \********************************************************************************************************************************/
/*! exports provided: CustomersUpdateStateDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomersUpdateStateDialog", function() { return CustomersUpdateStateDialog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _CustomersUIHelpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../CustomersUIHelpers */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersUIHelpers.js");
/* harmony import */ var _redux_customers_customersActions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../_redux/customers/customersActions */ "./resources/demo2/src/app/modules/ECommerce/_redux/customers/customersActions.js");
/* harmony import */ var _CustomersUIContext__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../CustomersUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersUIContext.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }








var selectedCustomers = function selectedCustomers(entities, ids) {
  var _customers = [];
  ids.forEach(function (id) {
    var customer = entities.find(function (el) {
      return el.id === id;
    });

    if (customer) {
      _customers.push(customer);
    }
  });
  return _customers;
};

function CustomersUpdateStateDialog(_ref) {
  var show = _ref.show,
      onHide = _ref.onHide;
  // Customers UI Context
  var customersUIContext = Object(_CustomersUIContext__WEBPACK_IMPORTED_MODULE_4__["useCustomersUIContext"])();
  var customersUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      ids: customersUIContext.ids,
      setIds: customersUIContext.setIds,
      queryParams: customersUIContext.queryParams
    };
  }, [customersUIContext]); // Customers Redux state

  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      customers: selectedCustomers(state.customers.entities, customersUIProps.ids),
      isLoading: state.customers.actionsLoading
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      customers = _useSelector.customers,
      isLoading = _useSelector.isLoading; // if !id we should close modal


  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (!customersUIProps.ids || customersUIProps.ids.length === 0) {
      onHide();
    } // eslint-disable-next-line react-hooks/exhaustive-deps

  }, [customersUIProps.ids]);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(0),
      _useState2 = _slicedToArray(_useState, 2),
      status = _useState2[0],
      setStatus = _useState2[1];

  var dispatch = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())();

  var updateStatus = function updateStatus() {
    // server request for update customers status by selected ids
    dispatch(_redux_customers_customersActions__WEBPACK_IMPORTED_MODULE_3__["updateCustomersStatus"](customersUIProps.ids, status)).then(function () {
      // refresh list after deletion
      dispatch(_redux_customers_customersActions__WEBPACK_IMPORTED_MODULE_3__["fetchCustomers"](customersUIProps.queryParams)).then(function () {
        // clear selections list
        customersUIProps.setIds([]); // closing delete modal

        onHide();
      });
    });
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    show: show,
    onHide: onHide,
    "aria-labelledby": "example-modal-sizes-title-lg"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Header, {
    closeButton: true
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Title, {
    id: "example-modal-sizes-title-lg"
  }, "Status has been updated for selected customers")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Body, {
    className: "overlay overlay-block cursor-default"
  }, isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "overlay-layer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "spinner spinner-lg spinner-primary"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "timeline timeline-5 mt-3"
  }, customers.map(function (customer) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "timeline-item align-items-start",
      key: "customersUpdate".concat(customer.id)
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "timeline-label font-weight-bolder text-dark-75 font-size-lg text-right pr-3"
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "timeline-badge"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
      className: "fa fa-genderless text-".concat(_CustomersUIHelpers__WEBPACK_IMPORTED_MODULE_2__["CustomerStatusCssClasses"][customer.status], " icon-xxl")
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "timeline-content text-dark-50 mr-5"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "label label-lg label-light-".concat(_CustomersUIHelpers__WEBPACK_IMPORTED_MODULE_2__["CustomerStatusCssClasses"][customer.status], " label-inline")
    }, "ID: ", customer.id), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "ml-3"
    }, customer.lastName, ", ", customer.firstName)));
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Footer, {
    className: "form"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form-group"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("select", {
    className: "form-control",
    value: status,
    onChange: function onChange(e) {
      return setStatus(+e.target.value);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
    value: "0"
  }, "Suspended"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
    value: "1"
  }, "Active"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
    value: "2"
  }, "Pending"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form-group"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: onHide,
    className: "btn btn-light btn-elevate mr-3"
  }, "Cancel"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: updateStatus,
    className: "btn btn-primary btn-elevate"
  }, "Update Status"))));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/eCommercePage.js":
/*!**************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/eCommercePage.js ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return eCommercePage; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-router-dom'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _customers_CustomersPage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./customers/CustomersPage */ "./resources/demo2/src/app/modules/ECommerce/pages/customers/CustomersPage.js");
/* harmony import */ var _products_ProductsPage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./products/ProductsPage */ "./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsPage.js");
/* harmony import */ var _products_product_edit_ProductEdit__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./products/product-edit/ProductEdit */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-edit/ProductEdit.js");
/* harmony import */ var _metronic_layout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../_metronic/layout */ "./resources/demo2/src/_metronic/layout/index.js");






function eCommercePage() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Suspense"], {
    fallback: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_layout__WEBPACK_IMPORTED_MODULE_5__["LayoutSplashScreen"], null)
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-router-dom'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), null,
  /*#__PURE__*/

  /* Redirect from eCommerce root URL to /customers */
  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-router-dom'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    exact: true,
    from: "/e-commerce",
    to: "/e-commerce/customers"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_layout__WEBPACK_IMPORTED_MODULE_5__["ContentRoute"], {
    path: "/e-commerce/customers",
    component: _customers_CustomersPage__WEBPACK_IMPORTED_MODULE_2__["CustomersPage"]
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_layout__WEBPACK_IMPORTED_MODULE_5__["ContentRoute"], {
    path: "/e-commerce/products/new",
    component: _products_product_edit_ProductEdit__WEBPACK_IMPORTED_MODULE_4__["ProductEdit"]
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_layout__WEBPACK_IMPORTED_MODULE_5__["ContentRoute"], {
    path: "/e-commerce/products/:id/edit",
    component: _products_product_edit_ProductEdit__WEBPACK_IMPORTED_MODULE_4__["ProductEdit"]
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_layout__WEBPACK_IMPORTED_MODULE_5__["ContentRoute"], {
    path: "/e-commerce/products",
    component: _products_ProductsPage__WEBPACK_IMPORTED_MODULE_3__["ProductsPage"]
  })));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsCard.js":
/*!**********************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsCard.js ***!
  \**********************************************************************************/
/*! exports provided: ProductsCard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsCard", function() { return ProductsCard; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");
/* harmony import */ var _products_filter_ProductsFilter__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./products-filter/ProductsFilter */ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-filter/ProductsFilter.js");
/* harmony import */ var _products_table_ProductsTable__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./products-table/ProductsTable */ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-table/ProductsTable.js");
/* harmony import */ var _products_grouping_ProductsGrouping__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./products-grouping/ProductsGrouping */ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-grouping/ProductsGrouping.js");
/* harmony import */ var _ProductsUIContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ProductsUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsUIContext.js");






function ProductsCard() {
  var productsUIContext = Object(_ProductsUIContext__WEBPACK_IMPORTED_MODULE_5__["useProductsUIContext"])();
  var productsUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      ids: productsUIContext.ids,
      queryParams: productsUIContext.queryParams,
      setQueryParams: productsUIContext.setQueryParams,
      newProductButtonClick: productsUIContext.newProductButtonClick,
      openDeleteProductsDialog: productsUIContext.openDeleteProductsDialog,
      openEditProductPage: productsUIContext.openEditProductPage,
      openUpdateProductsStatusDialog: productsUIContext.openUpdateProductsStatusDialog,
      openFetchProductsDialog: productsUIContext.openFetchProductsDialog
    };
  }, [productsUIContext]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_1__["Card"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_1__["CardHeader"], {
    title: "Products list"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_1__["CardHeaderToolbar"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    className: "btn btn-primary",
    onClick: productsUIProps.newProductButtonClick
  }, "New Product"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_1__["CardBody"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_products_filter_ProductsFilter__WEBPACK_IMPORTED_MODULE_2__["ProductsFilter"], null), productsUIProps.ids.length > 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_products_grouping_ProductsGrouping__WEBPACK_IMPORTED_MODULE_4__["ProductsGrouping"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_products_table_ProductsTable__WEBPACK_IMPORTED_MODULE_3__["ProductsTable"], null)));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsPage.js":
/*!**********************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsPage.js ***!
  \**********************************************************************************/
/*! exports provided: ProductsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsPage", function() { return ProductsPage; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-router-dom'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _products_loading_dialog_ProductsLoadingDialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./products-loading-dialog/ProductsLoadingDialog */ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-loading-dialog/ProductsLoadingDialog.js");
/* harmony import */ var _product_delete_dialog_ProductDeleteDialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./product-delete-dialog/ProductDeleteDialog */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-delete-dialog/ProductDeleteDialog.js");
/* harmony import */ var _products_delete_dialog_ProductsDeleteDialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./products-delete-dialog/ProductsDeleteDialog */ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-delete-dialog/ProductsDeleteDialog.js");
/* harmony import */ var _products_fetch_dialog_ProductsFetchDialog__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./products-fetch-dialog/ProductsFetchDialog */ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-fetch-dialog/ProductsFetchDialog.js");
/* harmony import */ var _products_update_status_dialog_ProductsUpdateStatusDialog__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./products-update-status-dialog/ProductsUpdateStatusDialog */ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-update-status-dialog/ProductsUpdateStatusDialog.js");
/* harmony import */ var _ProductsCard__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./ProductsCard */ "./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsCard.js");
/* harmony import */ var _ProductsUIContext__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./ProductsUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsUIContext.js");









function ProductsPage(_ref) {
  var history = _ref.history;
  var productsUIEvents = {
    newProductButtonClick: function newProductButtonClick() {
      history.push("/e-commerce/products/new");
    },
    openEditProductPage: function openEditProductPage(id) {
      history.push("/e-commerce/products/".concat(id, "/edit"));
    },
    openDeleteProductDialog: function openDeleteProductDialog(id) {
      history.push("/e-commerce/products/".concat(id, "/delete"));
    },
    openDeleteProductsDialog: function openDeleteProductsDialog() {
      history.push("/e-commerce/products/deleteProducts");
    },
    openFetchProductsDialog: function openFetchProductsDialog() {
      history.push("/e-commerce/products/fetch");
    },
    openUpdateProductsStatusDialog: function openUpdateProductsStatusDialog() {
      history.push("/e-commerce/products/updateStatus");
    }
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ProductsUIContext__WEBPACK_IMPORTED_MODULE_8__["ProductsUIProvider"], {
    productsUIEvents: productsUIEvents
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_products_loading_dialog_ProductsLoadingDialog__WEBPACK_IMPORTED_MODULE_2__["ProductsLoadingDialog"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-router-dom'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    path: "/e-commerce/products/deleteProducts"
  }, function (_ref2) {
    var history = _ref2.history,
        match = _ref2.match;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_products_delete_dialog_ProductsDeleteDialog__WEBPACK_IMPORTED_MODULE_4__["ProductsDeleteDialog"], {
      show: match != null,
      onHide: function onHide() {
        history.push("/e-commerce/products");
      }
    });
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-router-dom'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    path: "/e-commerce/products/:id/delete"
  }, function (_ref3) {
    var history = _ref3.history,
        match = _ref3.match;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_product_delete_dialog_ProductDeleteDialog__WEBPACK_IMPORTED_MODULE_3__["ProductDeleteDialog"], {
      show: match != null,
      id: match && match.params.id,
      onHide: function onHide() {
        history.push("/e-commerce/products");
      }
    });
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-router-dom'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    path: "/e-commerce/products/fetch"
  }, function (_ref4) {
    var history = _ref4.history,
        match = _ref4.match;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_products_fetch_dialog_ProductsFetchDialog__WEBPACK_IMPORTED_MODULE_5__["ProductsFetchDialog"], {
      show: match != null,
      onHide: function onHide() {
        history.push("/e-commerce/products");
      }
    });
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-router-dom'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    path: "/e-commerce/products/updateStatus"
  }, function (_ref5) {
    var history = _ref5.history,
        match = _ref5.match;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_products_update_status_dialog_ProductsUpdateStatusDialog__WEBPACK_IMPORTED_MODULE_6__["ProductsUpdateStatusDialog"], {
      show: match != null,
      onHide: function onHide() {
        history.push("/e-commerce/products");
      }
    });
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ProductsCard__WEBPACK_IMPORTED_MODULE_7__["ProductsCard"], null));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsUIContext.js":
/*!***************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsUIContext.js ***!
  \***************************************************************************************/
/*! exports provided: useProductsUIContext, ProductsUIConsumer, ProductsUIProvider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useProductsUIContext", function() { return useProductsUIContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsUIConsumer", function() { return ProductsUIConsumer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsUIProvider", function() { return ProductsUIProvider; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ProductsUIHelpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ProductsUIHelpers */ "./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsUIHelpers.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }




var ProductsUIContext = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_0__["createContext"])();
function useProductsUIContext() {
  return Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(ProductsUIContext);
}
var ProductsUIConsumer = ProductsUIContext.Consumer;
function ProductsUIProvider(_ref) {
  var productsUIEvents = _ref.productsUIEvents,
      children = _ref.children;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(_ProductsUIHelpers__WEBPACK_IMPORTED_MODULE_2__["initialFilter"]),
      _useState2 = _slicedToArray(_useState, 2),
      queryParams = _useState2[0],
      setQueryParamsBase = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState4 = _slicedToArray(_useState3, 2),
      ids = _useState4[0],
      setIds = _useState4[1];

  var setQueryParams = Object(react__WEBPACK_IMPORTED_MODULE_0__["useCallback"])(function (nextQueryParams) {
    setQueryParamsBase(function (prevQueryParams) {
      if (Object(lodash__WEBPACK_IMPORTED_MODULE_1__["isFunction"])(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (Object(lodash__WEBPACK_IMPORTED_MODULE_1__["isEqual"])(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);
  var value = {
    queryParams: queryParams,
    setQueryParamsBase: setQueryParamsBase,
    ids: ids,
    setIds: setIds,
    setQueryParams: setQueryParams,
    newProductButtonClick: productsUIEvents.newProductButtonClick,
    openEditProductPage: productsUIEvents.openEditProductPage,
    openDeleteProductDialog: productsUIEvents.openDeleteProductDialog,
    openDeleteProductsDialog: productsUIEvents.openDeleteProductsDialog,
    openFetchProductsDialog: productsUIEvents.openFetchProductsDialog,
    openUpdateProductsStatusDialog: productsUIEvents.openUpdateProductsStatusDialog
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(ProductsUIContext.Provider, {
    value: value
  }, children);
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsUIHelpers.js":
/*!***************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsUIHelpers.js ***!
  \***************************************************************************************/
/*! exports provided: ProductStatusCssClasses, ProductStatusTitles, ProductConditionCssClasses, ProductConditionTitles, defaultSorted, sizePerPageList, initialFilter, AVAILABLE_COLORS, AVAILABLE_MANUFACTURES */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductStatusCssClasses", function() { return ProductStatusCssClasses; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductStatusTitles", function() { return ProductStatusTitles; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductConditionCssClasses", function() { return ProductConditionCssClasses; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductConditionTitles", function() { return ProductConditionTitles; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "defaultSorted", function() { return defaultSorted; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sizePerPageList", function() { return sizePerPageList; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initialFilter", function() { return initialFilter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AVAILABLE_COLORS", function() { return AVAILABLE_COLORS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AVAILABLE_MANUFACTURES", function() { return AVAILABLE_MANUFACTURES; });
var ProductStatusCssClasses = ["success", "info", ""];
var ProductStatusTitles = ["Selling", "Sold"];
var ProductConditionCssClasses = ["success", "danger", ""];
var ProductConditionTitles = ["New", "Used"];
var defaultSorted = [{
  dataField: "id",
  order: "asc"
}];
var sizePerPageList = [{
  text: "3",
  value: 3
}, {
  text: "5",
  value: 5
}, {
  text: "10",
  value: 10
}];
var initialFilter = {
  filter: {
    model: "",
    manufacture: "",
    VINCode: ""
  },
  sortOrder: "asc",
  // asc||desc
  sortField: "VINCode",
  pageNumber: 1,
  pageSize: 10
};
var AVAILABLE_COLORS = ["Red", "CadetBlue", "Eagle", "Gold", "LightSlateGrey", "RoyalBlue", "Crimson", "Blue", "Sienna", "Indigo", "Green", "Violet", "GoldenRod", "OrangeRed", "Khaki", "Teal", "Purple", "Orange", "Pink", "Black", "DarkTurquoise"];
var AVAILABLE_MANUFACTURES = ["Pontiac", "Kia", "Lotus", "Subaru", "Jeep", "Isuzu", "Mitsubishi", "Oldsmobile", "Chevrolet", "Chrysler", "Audi", "Suzuki", "GMC", "Cadillac", "Infinity", "Mercury", "Dodge", "Ram", "Lexus", "Lamborghini", "Honda", "Nissan", "Ford", "Hyundai", "Saab", "Toyota"];

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-delete-dialog/ProductDeleteDialog.js":
/*!***************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-delete-dialog/ProductDeleteDialog.js ***!
  \***************************************************************************************************************/
/*! exports provided: ProductDeleteDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDeleteDialog", function() { return ProductDeleteDialog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");
/* harmony import */ var _redux_products_productsActions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../_redux/products/productsActions */ "./resources/demo2/src/app/modules/ECommerce/_redux/products/productsActions.js");
/* harmony import */ var _ProductsUIContext__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../ProductsUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsUIContext.js");
/* eslint-disable no-restricted-imports */






function ProductDeleteDialog(_ref) {
  var id = _ref.id,
      show = _ref.show,
      onHide = _ref.onHide;
  // Products UI Context
  var productsUIContext = Object(_ProductsUIContext__WEBPACK_IMPORTED_MODULE_4__["useProductsUIContext"])();
  var productsUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      setIds: productsUIContext.setIds,
      queryParams: productsUIContext.queryParams
    };
  }, [productsUIContext]); // Products Redux state

  var dispatch = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())();

  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      isLoading: state.products.actionsLoading
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      isLoading = _useSelector.isLoading; // if !id we should close modal


  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (!id) {
      onHide();
    } // eslint-disable-next-line react-hooks/exhaustive-deps

  }, [id]); // looking for loading/dispatch

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {}, [isLoading, dispatch]);

  var deleteProduct = function deleteProduct() {
    // server request for deleting product by id
    dispatch(_redux_products_productsActions__WEBPACK_IMPORTED_MODULE_3__["deleteProduct"](id)).then(function () {
      // refresh list after deletion
      dispatch(_redux_products_productsActions__WEBPACK_IMPORTED_MODULE_3__["fetchProducts"](productsUIProps.queryParams)); // clear selections list

      productsUIProps.setIds([]); // closing delete modal

      onHide();
    });
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    show: show,
    onHide: onHide,
    "aria-labelledby": "example-modal-sizes-title-lg"
  }, isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["ModalProgressBar"], {
    variant: "query"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Header, {
    closeButton: true
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Title, {
    id: "example-modal-sizes-title-lg"
  }, "Product Delete")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Body, null, !isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Are you sure to permanently delete this product?"), isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Product is deleting...")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Footer, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: onHide,
    className: "btn btn-light btn-elevate"
  }, "Cancel"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: deleteProduct,
    className: "btn btn-delete btn-elevate"
  }, "Delete"))));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-edit/ProductEdit.js":
/*!**********************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-edit/ProductEdit.js ***!
  \**********************************************************************************************/
/*! exports provided: ProductEdit */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductEdit", function() { return ProductEdit; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _redux_products_productsActions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../_redux/products/productsActions */ "./resources/demo2/src/app/modules/ECommerce/_redux/products/productsActions.js");
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");
/* harmony import */ var _ProductEditForm__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./ProductEditForm */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-edit/ProductEditForm.js");
/* harmony import */ var _product_specifications_Specifications__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../product-specifications/Specifications */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/Specifications.js");
/* harmony import */ var _product_specifications_SpecificationsUIContext__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../product-specifications/SpecificationsUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsUIContext.js");
/* harmony import */ var _metronic_layout__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../../_metronic/layout */ "./resources/demo2/src/_metronic/layout/index.js");
/* harmony import */ var _product_remarks_RemarksUIContext__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../product-remarks/RemarksUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksUIContext.js");
/* harmony import */ var _product_remarks_Remarks__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../product-remarks/Remarks */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/Remarks.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */












var initProduct = {
  id: undefined,
  model: "",
  manufacture: "Pontiac",
  modelYear: 2020,
  mileage: 0,
  description: "",
  color: "Red",
  price: 10000,
  condition: 1,
  status: 0,
  VINCode: ""
};
function ProductEdit(_ref) {
  var history = _ref.history,
      id = _ref.match.params.id;
  // Subheader
  var suhbeader = Object(_metronic_layout__WEBPACK_IMPORTED_MODULE_7__["useSubheader"])(); // Tabs

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("basic"),
      _useState2 = _slicedToArray(_useState, 2),
      tab = _useState2[0],
      setTab = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState4 = _slicedToArray(_useState3, 2),
      title = _useState4[0],
      setTitle = _useState4[1];

  var dispatch = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(); // const layoutDispatch = useContext(LayoutContext.Dispatch);

  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      actionsLoading: state.products.actionsLoading,
      productForEdit: state.products.productForEdit
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      actionsLoading = _useSelector.actionsLoading,
      productForEdit = _useSelector.productForEdit;

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    dispatch(_redux_products_productsActions__WEBPACK_IMPORTED_MODULE_2__["fetchProduct"](id));
  }, [id, dispatch]);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    var _title = id ? "" : "New Product";

    if (productForEdit && id) {
      _title = "Edit product '".concat(productForEdit.manufacture, " ").concat(productForEdit.model, " - ").concat(productForEdit.modelYear, "'");
    }

    setTitle(_title);
    suhbeader.setTitle(_title); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [productForEdit, id]);

  var saveProduct = function saveProduct(values) {
    if (!id) {
      dispatch(_redux_products_productsActions__WEBPACK_IMPORTED_MODULE_2__["createProduct"](values)).then(function () {
        return backToProductsList();
      });
    } else {
      dispatch(_redux_products_productsActions__WEBPACK_IMPORTED_MODULE_2__["updateProduct"](values)).then(function () {
        return backToProductsList();
      });
    }
  };

  var btnRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])();

  var saveProductClick = function saveProductClick() {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
    }
  };

  var backToProductsList = function backToProductsList() {
    history.push("/e-commerce/products");
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_3__["Card"], null, actionsLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_3__["ModalProgressBar"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_3__["CardHeader"], {
    title: title
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_3__["CardHeaderToolbar"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: backToProductsList,
    className: "btn btn-light"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-arrow-left"
  }), "Back"), "  ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: "btn btn-light ml-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-redo"
  }), "Reset"), "  ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "submit",
    className: "btn btn-primary ml-2",
    onClick: saveProductClick
  }, "Save"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_3__["CardBody"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "nav nav-tabs nav-tabs-line ",
    role: "tablist"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "nav-item",
    onClick: function onClick() {
      return setTab("basic");
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: "nav-link ".concat(tab === "basic" && "active"),
    "data-toggle": "tab",
    role: "tab",
    "aria-selected": (tab === "basic").toString()
  }, "Basic info")), id && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "nav-item",
    onClick: function onClick() {
      return setTab("remarks");
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: "nav-link ".concat(tab === "remarks" && "active"),
    "data-toggle": "tab",
    role: "button",
    "aria-selected": (tab === "remarks").toString()
  }, "Product remarks")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "nav-item",
    onClick: function onClick() {
      return setTab("specs");
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: "nav-link ".concat(tab === "specs" && "active"),
    "data-toggle": "tab",
    role: "tab",
    "aria-selected": (tab === "specs").toString()
  }, "Product specifications")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mt-5"
  }, tab === "basic" && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ProductEditForm__WEBPACK_IMPORTED_MODULE_4__["ProductEditForm"], {
    actionsLoading: actionsLoading,
    product: productForEdit || initProduct,
    btnRef: btnRef,
    saveProduct: saveProduct
  }), tab === "remarks" && id && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_product_remarks_RemarksUIContext__WEBPACK_IMPORTED_MODULE_8__["RemarksUIProvider"], {
    currentProductId: id
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_product_remarks_Remarks__WEBPACK_IMPORTED_MODULE_9__["Remarks"], null)), tab === "specs" && id && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_product_specifications_SpecificationsUIContext__WEBPACK_IMPORTED_MODULE_6__["SpecificationsUIProvider"], {
    currentProductId: id
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_product_specifications_Specifications__WEBPACK_IMPORTED_MODULE_5__["Specifications"], null)))));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-edit/ProductEditForm.js":
/*!**************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-edit/ProductEditForm.js ***!
  \**************************************************************************************************/
/*! exports provided: ProductEditForm */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductEditForm", function() { return ProductEditForm; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");
/* harmony import */ var _ProductsUIHelpers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../ProductsUIHelpers */ "./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsUIHelpers.js");
// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10




 // Validation schema

var ProductEditSchema = !(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())().shape({
  model: !(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())().min(2, "Minimum 2 symbols").max(50, "Maximum 50 symbols").required("Model is required"),
  manufacture: !(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())().min(2, "Minimum 2 symbols").max(50, "Maximum 50 symbols").required("Manufacture is required"),
  modelYear: !(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())().min(1950, "1950 is minimum").max(2020, "2020 is maximum").required("Model year is required"),
  mileage: !(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())().min(0, "0 is minimum").max(1000000, "1000000 is maximum").required("Mileage is required"),
  color: !(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())().required("Color is required"),
  price: !(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())().min(1, "$1 is minimum").max(1000000, "$1000000 is maximum").required("Price is required"),
  VINCode: !(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())().required("VINCode is required")
});
function ProductEditForm(_ref) {
  var product = _ref.product,
      btnRef = _ref.btnRef,
      saveProduct = _ref.saveProduct;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    enableReinitialize: true,
    initialValues: product,
    validationSchema: ProductEditSchema,
    onSubmit: function onSubmit(values) {
      saveProduct(values);
    }
  }, function (_ref2) {
    var handleSubmit = _ref2.handleSubmit;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
      className: "form form-label-right"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "form-group row"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-4"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
      name: "model",
      component: _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["Input"],
      placeholder: "Model",
      label: "Model"
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-4"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["Select"], {
      name: "manufacture",
      label: "Color"
    }, _ProductsUIHelpers__WEBPACK_IMPORTED_MODULE_3__["AVAILABLE_MANUFACTURES"].map(function (manufacture) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
        key: manufacture,
        value: manufacture
      }, manufacture);
    }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-4"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
      type: "number",
      name: "modelYear",
      component: _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["Input"],
      placeholder: "Model year",
      label: "Model year"
    }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "form-group row"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-4"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
      type: "number",
      name: "mileage",
      component: _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["Input"],
      placeholder: "Mileage",
      label: "Mileage"
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-4"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["Select"], {
      name: "color",
      label: "Color"
    }, _ProductsUIHelpers__WEBPACK_IMPORTED_MODULE_3__["AVAILABLE_COLORS"].map(function (color) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
        key: color,
        value: color
      }, color);
    }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-4"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
      type: "number",
      name: "price",
      component: _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["Input"],
      placeholder: "Price",
      label: "Price ($)",
      customFeedbackLabel: "Please enter Price"
    }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "form-group row"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-4"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
      name: "VINCode",
      component: _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["Input"],
      placeholder: "VIN code",
      label: "VIN code"
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-4"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["Select"], {
      name: "status",
      label: "Status"
    }, _ProductsUIHelpers__WEBPACK_IMPORTED_MODULE_3__["ProductStatusTitles"].map(function (status, index) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
        key: status,
        value: index
      }, status);
    }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-4"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["Select"], {
      name: "condition",
      label: "Condition"
    }, _ProductsUIHelpers__WEBPACK_IMPORTED_MODULE_3__["ProductConditionTitles"].map(function (condition, index) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
        key: condition,
        value: index
      }, condition);
    })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "form-group"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", null, "Description"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
      name: "description",
      as: "textarea",
      className: "form-control"
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      type: "submit",
      style: {
        display: "none"
      },
      ref: btnRef,
      onSubmit: function onSubmit() {
        return handleSubmit();
      }
    })));
  }));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarkDeleteDialog.js":
/*!********************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarkDeleteDialog.js ***!
  \********************************************************************************************************/
/*! exports provided: RemarkDeleteDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RemarkDeleteDialog", function() { return RemarkDeleteDialog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");
/* harmony import */ var _redux_remarks_remarksActions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../_redux/remarks/remarksActions */ "./resources/demo2/src/app/modules/ECommerce/_redux/remarks/remarksActions.js");
/* harmony import */ var _RemarksUIContext__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./RemarksUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksUIContext.js");
/* eslint-disable no-restricted-imports */






function RemarkDeleteDialog() {
  // Remarks UI Context
  var remarksUIContext = Object(_RemarksUIContext__WEBPACK_IMPORTED_MODULE_4__["useRemarksUIContext"])();
  var remarksUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      id: remarksUIContext.selectedId,
      setIds: remarksUIContext.setIds,
      productId: remarksUIContext.productId,
      queryParams: remarksUIContext.queryParams,
      showDeleteRemarkDialog: remarksUIContext.showDeleteRemarkDialog,
      closeDeleteRemarkDialog: remarksUIContext.closeDeleteRemarkDialog
    };
  }, [remarksUIContext]); // Remarks Redux state

  var dispatch = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())();

  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      isLoading: state.remarks.actionsLoading
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      isLoading = _useSelector.isLoading; // if !id we should close modal


  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (!remarksUIProps.id) {
      remarksUIProps.closeDeleteRemarkDialog();
    } // eslint-disable-next-line react-hooks/exhaustive-deps

  }, [remarksUIProps.id]); // looking for loading/dispatch

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {}, [isLoading, dispatch]);

  var deleteRemark = function deleteRemark() {
    // server request for deleting remark by id
    dispatch(_redux_remarks_remarksActions__WEBPACK_IMPORTED_MODULE_3__["deleteRemark"](remarksUIProps.id)).then(function () {
      // refresh list after deletion
      dispatch(_redux_remarks_remarksActions__WEBPACK_IMPORTED_MODULE_3__["fetchRemarks"](remarksUIProps.queryParams, remarksUIProps.productId)); // clear selections list

      remarksUIProps.setIds([]); // closing delete modal

      remarksUIProps.closeDeleteRemarkDialog();
    });
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    show: remarksUIProps.showDeleteRemarkDialog,
    onHide: remarksUIProps.closeDeleteRemarkDialog,
    "aria-labelledby": "example-modal-sizes-title-lg"
  }, isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["ModalProgressBar"], {
    variant: "query"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Header, {
    closeButton: true
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Title, {
    id: "example-modal-sizes-title-lg"
  }, "Remark Delete")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Body, null, !isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Are you sure to permanently delete this remark?"), isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Remark is deleting...")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Footer, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: remarksUIProps.closeDeleteRemarkDialog,
    className: "btn btn-light btn-elevate"
  }, "Cancel"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: deleteRemark,
    className: "btn btn-primary btn-elevate"
  }, "Delete"))));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/Remarks.js":
/*!*********************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/Remarks.js ***!
  \*********************************************************************************************/
/*! exports provided: Remarks */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Remarks", function() { return Remarks; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _RemarksFilter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RemarksFilter */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksFilter.js");
/* harmony import */ var _RemarksTable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./RemarksTable */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksTable.js");
/* harmony import */ var _RemarksLoadingDialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./RemarksLoadingDialog */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksLoadingDialog.js");
/* harmony import */ var _RemarksDeleteDialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./RemarksDeleteDialog */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksDeleteDialog.js");
/* harmony import */ var _RemarkDeleteDialog__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./RemarkDeleteDialog */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarkDeleteDialog.js");
/* harmony import */ var _RemarksFetchDialog__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./RemarksFetchDialog */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksFetchDialog.js");
/* harmony import */ var _RemarksGrouping__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./RemarksGrouping */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksGrouping.js");
/* harmony import */ var _remark_edit_dialog_RemarkEditDialog__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./remark-edit-dialog/RemarkEditDialog */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/remark-edit-dialog/RemarkEditDialog.js");
/* harmony import */ var _RemarksUIContext__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./RemarksUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksUIContext.js");










function Remarks() {
  // Remarks UI Context
  var remarksUIContext = Object(_RemarksUIContext__WEBPACK_IMPORTED_MODULE_9__["useRemarksUIContext"])();
  var remarksUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      ids: remarksUIContext.ids
    };
  }, [remarksUIContext]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_RemarksLoadingDialog__WEBPACK_IMPORTED_MODULE_3__["RemarksLoadingDialog"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_remark_edit_dialog_RemarkEditDialog__WEBPACK_IMPORTED_MODULE_8__["RemarkEditDialog"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_RemarkDeleteDialog__WEBPACK_IMPORTED_MODULE_5__["RemarkDeleteDialog"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_RemarksDeleteDialog__WEBPACK_IMPORTED_MODULE_4__["RemarksDeleteDialog"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_RemarksFetchDialog__WEBPACK_IMPORTED_MODULE_6__["RemarksFetchDialog"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form margin-b-30"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_RemarksFilter__WEBPACK_IMPORTED_MODULE_1__["RemarksFilter"], null), remarksUIProps.ids.length > 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_RemarksGrouping__WEBPACK_IMPORTED_MODULE_7__["RemarksGrouping"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_RemarksTable__WEBPACK_IMPORTED_MODULE_2__["RemarksTable"], null));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksDeleteDialog.js":
/*!*********************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksDeleteDialog.js ***!
  \*********************************************************************************************************/
/*! exports provided: RemarksDeleteDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RemarksDeleteDialog", function() { return RemarksDeleteDialog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");
/* harmony import */ var _redux_remarks_remarksActions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../_redux/remarks/remarksActions */ "./resources/demo2/src/app/modules/ECommerce/_redux/remarks/remarksActions.js");
/* harmony import */ var _RemarksUIContext__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./RemarksUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksUIContext.js");
/* eslint-disable no-restricted-imports */






function RemarksDeleteDialog() {
  // Remarks UI Context
  var remarksUIContext = Object(_RemarksUIContext__WEBPACK_IMPORTED_MODULE_4__["useRemarksUIContext"])();
  var remarksUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      ids: remarksUIContext.ids,
      setIds: remarksUIContext.setIds,
      productId: remarksUIContext.productId,
      queryParams: remarksUIContext.queryParams,
      showDeleteRemarksDialog: remarksUIContext.showDeleteRemarksDialog,
      closeDeleteRemarksDialog: remarksUIContext.closeDeleteRemarksDialog
    };
  }, [remarksUIContext]);
  var dispatch = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())();

  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      isLoading: state.remarks.actionsLoading
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      isLoading = _useSelector.isLoading;

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {}, [isLoading, dispatch]);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (!remarksUIProps.ids || remarksUIProps.ids.length === 0) {
      remarksUIProps.closeDeleteRemarksDialog();
    } // eslint-disable-next-line react-hooks/exhaustive-deps

  }, [remarksUIProps.ids]);

  var deleteRemarks = function deleteRemarks() {
    dispatch(_redux_remarks_remarksActions__WEBPACK_IMPORTED_MODULE_3__["deleteRemarks"](remarksUIProps.ids)).then(function () {
      dispatch(_redux_remarks_remarksActions__WEBPACK_IMPORTED_MODULE_3__["fetchRemarks"](remarksUIProps.queryParams, remarksUIProps.productId)).then(function () {
        remarksUIProps.setIds([]);
        remarksUIProps.closeDeleteRemarksDialog();
      });
    });
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    show: remarksUIProps.showDeleteRemarksDialog,
    onHide: remarksUIProps.closeDeleteRemarksDialog,
    "aria-labelledby": "example-modal-sizes-title-lg"
  }, isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["ModalProgressBar"], {
    variant: "query"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Header, {
    closeButton: true
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Title, {
    id: "example-modal-sizes-title-lg"
  }, "Remarks Delete")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Body, null, !isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Are you sure to permanently delete selected remarks?"), isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Remarks are deleting...")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Footer, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: remarksUIProps.closeDeleteRemarksDialog,
    className: "btn btn-light btn-elevate"
  }, "Cancel"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: deleteRemarks,
    className: "btn btn-primary btn-elevate"
  }, "Delete"))));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksFetchDialog.js":
/*!********************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksFetchDialog.js ***!
  \********************************************************************************************************/
/*! exports provided: RemarksFetchDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RemarksFetchDialog", function() { return RemarksFetchDialog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _RemarksUIContext__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./RemarksUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksUIContext.js");





var selectedRemarks = function selectedRemarks(entities, ids) {
  var _remarks = [];
  ids.forEach(function (id) {
    var remark = entities.find(function (el) {
      return el.id === id;
    });

    if (remark) {
      _remarks.push(remark);
    }
  });
  return _remarks;
};

function RemarksFetchDialog() {
  // Remarks UI Context
  var remarksUIContext = Object(_RemarksUIContext__WEBPACK_IMPORTED_MODULE_2__["useRemarksUIContext"])();
  var remarksUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      ids: remarksUIContext.ids,
      queryParams: remarksUIContext.queryParams,
      showFetchRemarksDialog: remarksUIContext.showFetchRemarksDialog,
      closeFetchRemarksDialog: remarksUIContext.closeFetchRemarksDialog
    };
  }, [remarksUIContext]);

  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      remarks: selectedRemarks(state.remarks.entities, remarksUIProps.ids)
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      remarks = _useSelector.remarks;

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (!remarksUIProps.ids || remarksUIProps.ids.length === 0) {
      remarksUIProps.closeFetchRemarksDialog();
    } // eslint-disable-next-line react-hooks/exhaustive-deps

  }, [remarksUIProps.ids]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    show: remarksUIProps.showFetchRemarksDialog,
    onHide: remarksUIProps.closeFetchRemarksDialog,
    "aria-labelledby": "example-modal-sizes-title-lg"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Header, {
    closeButton: true
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Title, {
    id: "example-modal-sizes-title-lg"
  }, "Fetch selected elements")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Body, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "list-timeline list-timeline-skin-light padding-30"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "list-timeline-items"
  }, remarks.map(function (remark) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "list-timeline-item mb-3",
      key: remark.id
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "list-timeline-text"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "label label-lg label-light-success label-inline",
      style: {
        width: "60px"
      }
    }, "ID: ", remark.id), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "ml-5"
    }, remark.text, " ")));
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Footer, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: remarksUIProps.closeFetchRemarksDialog,
    className: "btn btn-light btn-elevate"
  }, "Cancel"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: remarksUIProps.closeFetchRemarksDialog,
    className: "btn btn-primary btn-elevate"
  }, "Ok"))));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksFilter.js":
/*!***************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksFilter.js ***!
  \***************************************************************************************************/
/*! exports provided: RemarksFilter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RemarksFilter", function() { return RemarksFilter; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _RemarksUIContext__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./RemarksUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksUIContext.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






var prepareFilter = function prepareFilter(queryParams, values) {
  var searchText = values.searchText;

  var newQueryParams = _objectSpread({}, queryParams);

  var filter = {};
  filter.text = searchText;
  newQueryParams.filter = filter;
  return newQueryParams;
};

function RemarksFilter() {
  // Remarks UI Context
  var remarksUIContext = Object(_RemarksUIContext__WEBPACK_IMPORTED_MODULE_3__["useRemarksUIContext"])();
  var remarksUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      setQueryParams: remarksUIContext.setQueryParams,
      openNewRemarkDialog: remarksUIContext.openNewRemarkDialog,
      queryParams: remarksUIContext.queryParams
    };
  }, [remarksUIContext]);

  var applyFilter = function applyFilter(values) {
    var newQueryParams = prepareFilter(remarksUIProps.queryParams, values);

    if (!Object(lodash__WEBPACK_IMPORTED_MODULE_2__["isEqual"])(newQueryParams, remarksUIProps.queryParams)) {
      newQueryParams.pageNumber = 1;
      remarksUIProps.setQueryParams(newQueryParams);
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form-filtration"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "row align-items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-md-2 margin-bottom-10-mobile"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    initialValues: {
      searchText: ""
    },
    onSubmit: function onSubmit(values) {
      applyFilter(values);
    }
  }, function (_ref) {
    var values = _ref.values,
        handleSubmit = _ref.handleSubmit,
        handleBlur = _ref.handleBlur,
        handleChange = _ref.handleChange,
        setFieldValue = _ref.setFieldValue;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
      onSubmit: handleSubmit
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      type: "text",
      className: "form-control",
      name: "searchText",
      placeholder: "Search",
      onBlur: handleBlur,
      value: values.searchText,
      onChange: function onChange(e) {
        setFieldValue("searchText", e.target.value);
        handleSubmit();
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("small", {
      className: "form-text text-muted"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("b", null, "Search"), " in all fields")));
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-md-8 margin-bottom-10-mobile"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-md-2 text-right margin-bottom-10-mobile"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    className: "btn btn-primary",
    onClick: function onClick() {
      return remarksUIProps.openNewRemarkDialog();
    }
  }, "Create new remark")))));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksGrouping.js":
/*!*****************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksGrouping.js ***!
  \*****************************************************************************************************/
/*! exports provided: RemarksGrouping */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RemarksGrouping", function() { return RemarksGrouping; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _RemarksUIContext__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RemarksUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksUIContext.js");


function RemarksGrouping() {
  // Remarks UI Context
  var remarksUIContext = Object(_RemarksUIContext__WEBPACK_IMPORTED_MODULE_1__["useRemarksUIContext"])();
  var remarksUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      ids: remarksUIContext.ids,
      openDeleteRemarksDialog: remarksUIContext.openDeleteRemarksDialog,
      openFetchRemarksDialog: remarksUIContext.openFetchRemarksDialog
    };
  }, [remarksUIContext]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "row align-items-center form-group-actions margin-top-20"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-xl-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form-group form-group-inline"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form-label form-label-no-wrap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "font-bold font-danger mt-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Selected records count: ", remarksUIProps.ids.length))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form-group-inline"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    className: "btn btn-danger font-weight-bolder font-size-sm",
    onClick: remarksUIProps.openDeleteRemarksDialog
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-trash"
  }), " Delete All"), "\xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    className: "btn btn-light-primary font-weight-bolder font-size-sm",
    onClick: remarksUIProps.openFetchRemarksDialog
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-stream"
  }), " Fetch Selected"))))));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksLoadingDialog.js":
/*!**********************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksLoadingDialog.js ***!
  \**********************************************************************************************************/
/*! exports provided: RemarksLoadingDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RemarksLoadingDialog", function() { return RemarksLoadingDialog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");



function RemarksLoadingDialog() {
  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      isLoading: state.remarks.listLoading
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      isLoading = _useSelector.isLoading;

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {}, [isLoading]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["LoadingDialog"], {
    isLoading: isLoading,
    text: "Loading ..."
  });
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksTable.js":
/*!**************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksTable.js ***!
  \**************************************************************************************************/
/*! exports provided: RemarksTable */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RemarksTable", function() { return RemarksTable; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap-table-next'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap-table2-paginator'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _redux_remarks_remarksActions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../_redux/remarks/remarksActions */ "./resources/demo2/src/app/modules/ECommerce/_redux/remarks/remarksActions.js");
/* harmony import */ var _column_formatters_ActionsColumnFormatter__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./column-formatters/ActionsColumnFormatter */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/column-formatters/ActionsColumnFormatter.js");
/* harmony import */ var _RemarksUIHelper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./RemarksUIHelper */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksUIHelper.js");
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");
/* harmony import */ var _metronic_helpers__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../../_metronic/_helpers */ "./resources/demo2/src/_metronic/_helpers/index.js");
/* harmony import */ var _RemarksUIContext__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./RemarksUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksUIContext.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html










function RemarksTable() {
  // Remarks UI Context
  var remarksUIContext = Object(_RemarksUIContext__WEBPACK_IMPORTED_MODULE_7__["useRemarksUIContext"])();
  var remarksUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      ids: remarksUIContext.ids,
      setIds: remarksUIContext.setIds,
      queryParams: remarksUIContext.queryParams,
      setQueryParams: remarksUIContext.setQueryParams,
      productId: remarksUIContext.productId,
      openEditRemarkDialog: remarksUIContext.openEditRemarkDialog,
      openDeleteRemarkDialog: remarksUIContext.openDeleteRemarkDialog
    };
  }, [remarksUIContext]); // Getting curret state of products list from store (Redux)

  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      currentState: state.remarks
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      currentState = _useSelector.currentState;

  var totalCount = currentState.totalCount,
      entities = currentState.entities,
      listLoading = currentState.listLoading;
  var dispatch = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())();
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    remarksUIProps.setIds([]);
    dispatch(_redux_remarks_remarksActions__WEBPACK_IMPORTED_MODULE_2__["fetchRemarks"](remarksUIProps.queryParams, remarksUIProps.productId)); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [remarksUIProps.queryParams, dispatch, remarksUIProps.productId]);
  var columns = [{
    dataField: "id",
    text: "ID",
    sort: true,
    sortCaret: _metronic_helpers__WEBPACK_IMPORTED_MODULE_6__["sortCaret"]
  }, {
    dataField: "text",
    text: "Text",
    sort: true,
    sortCaret: _metronic_helpers__WEBPACK_IMPORTED_MODULE_6__["sortCaret"]
  }, {
    dataField: "dueDate",
    text: "Due date",
    sort: false
  }, {
    dataField: "action",
    text: "Actions",
    formatter: _column_formatters_ActionsColumnFormatter__WEBPACK_IMPORTED_MODULE_3__["ActionsColumnFormatter"],
    formatExtraData: {
      openEditRemarkDialog: remarksUIProps.openEditRemarkDialog,
      openDeleteRemarkDialog: remarksUIProps.openDeleteRemarkDialog
    },
    classes: "text-right pr-0",
    headerClasses: "text-right pr-3",
    style: {
      minWidth: "100px"
    }
  }];
  var paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: _RemarksUIHelper__WEBPACK_IMPORTED_MODULE_4__["sizePerPageList"],
    sizePerPage: remarksUIProps.queryParams.pageSize,
    page: remarksUIProps.queryParams.pageNumber
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap-table2-paginator'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    pagination: !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap-table2-paginator'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(paginationOptions)
  }, function (_ref) {
    var paginationProps = _ref.paginationProps,
        paginationTableProps = _ref.paginationTableProps;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_5__["Pagination"], {
      isLoading: listLoading,
      paginationProps: paginationProps
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap-table-next'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), _extends({
      wrapperClasses: "table-responsive",
      classes: "table table-head-custom table-vertical-center overflow-hidden",
      bordered: false,
      bootstrap4: true,
      remote: true,
      keyField: "id",
      data: entities === null ? [] : entities,
      columns: columns,
      defaultSorted: _RemarksUIHelper__WEBPACK_IMPORTED_MODULE_4__["defaultSorted"],
      onTableChange: Object(_metronic_helpers__WEBPACK_IMPORTED_MODULE_6__["getHandlerTableChange"])(remarksUIProps.setQueryParams),
      selectRow: Object(_metronic_helpers__WEBPACK_IMPORTED_MODULE_6__["getSelectRow"])({
        entities: entities,
        ids: remarksUIProps.ids,
        setIds: remarksUIProps.setIds
      })
    }, paginationTableProps), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_helpers__WEBPACK_IMPORTED_MODULE_6__["PleaseWaitMessage"], {
      entities: entities
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_helpers__WEBPACK_IMPORTED_MODULE_6__["NoRecordsFoundMessage"], {
      entities: entities
    })));
  }));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksUIContext.js":
/*!******************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksUIContext.js ***!
  \******************************************************************************************************/
/*! exports provided: useRemarksUIContext, RemarksUIConsumer, RemarksUIProvider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useRemarksUIContext", function() { return useRemarksUIContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RemarksUIConsumer", function() { return RemarksUIConsumer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RemarksUIProvider", function() { return RemarksUIProvider; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _RemarksUIHelper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./RemarksUIHelper */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksUIHelper.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

/* eslint-disable no-unused-vars */



var RemarksUIContext = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_0__["createContext"])();
function useRemarksUIContext() {
  return Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(RemarksUIContext);
}
var RemarksUIConsumer = RemarksUIContext.Consumer;
function RemarksUIProvider(_ref) {
  var currentProductId = _ref.currentProductId,
      children = _ref.children;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(currentProductId),
      _useState2 = _slicedToArray(_useState, 2),
      productId = _useState2[0],
      setProductId = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(_RemarksUIHelper__WEBPACK_IMPORTED_MODULE_2__["initialFilter"]),
      _useState4 = _slicedToArray(_useState3, 2),
      queryParams = _useState4[0],
      setQueryParamsBase = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState6 = _slicedToArray(_useState5, 2),
      ids = _useState6[0],
      setIds = _useState6[1];

  var setQueryParams = Object(react__WEBPACK_IMPORTED_MODULE_0__["useCallback"])(function (nextQueryParams) {
    setQueryParamsBase(function (prevQueryParams) {
      if (Object(lodash__WEBPACK_IMPORTED_MODULE_1__["isFunction"])(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (Object(lodash__WEBPACK_IMPORTED_MODULE_1__["isEqual"])(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null),
      _useState8 = _slicedToArray(_useState7, 2),
      selectedId = _useState8[0],
      setSelectedId = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState10 = _slicedToArray(_useState9, 2),
      showEditRemarkDialog = _useState10[0],
      setShowEditRemarkDialog = _useState10[1];

  var initRemark = {
    id: undefined,
    text: "",
    type: 0,
    dueDate: "01/07/2020",
    carId: productId
  };
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    initRemark.productId = currentProductId;
    initRemark.carId = currentProductId;
    setProductId(currentProductId); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentProductId]);

  var openNewRemarkDialog = function openNewRemarkDialog() {
    setSelectedId(undefined);
    setShowEditRemarkDialog(true);
  };

  var openEditRemarkDialog = function openEditRemarkDialog(id) {
    setSelectedId(id);
    setShowEditRemarkDialog(true);
  };

  var closeEditRemarkDialog = function closeEditRemarkDialog() {
    setSelectedId(undefined);
    setShowEditRemarkDialog(false);
  };

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState12 = _slicedToArray(_useState11, 2),
      showDeleteRemarkDialog = _useState12[0],
      setShowDeleteRemarkDialog = _useState12[1];

  var openDeleteRemarkDialog = function openDeleteRemarkDialog(id) {
    setSelectedId(id);
    setShowDeleteRemarkDialog(true);
  };

  var closeDeleteRemarkDialog = function closeDeleteRemarkDialog() {
    setSelectedId(undefined);
    setShowDeleteRemarkDialog(false);
  };

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState14 = _slicedToArray(_useState13, 2),
      showDeleteRemarksDialog = _useState14[0],
      setShowDeleteRemarksDialog = _useState14[1];

  var openDeleteRemarksDialog = function openDeleteRemarksDialog() {
    setShowDeleteRemarksDialog(true);
  };

  var closeDeleteRemarksDialog = function closeDeleteRemarksDialog() {
    setShowDeleteRemarksDialog(false);
  };

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState16 = _slicedToArray(_useState15, 2),
      showFetchRemarksDialog = _useState16[0],
      setShowFetchRemarksDialog = _useState16[1];

  var openFetchRemarksDialog = function openFetchRemarksDialog() {
    setShowFetchRemarksDialog(true);
  };

  var closeFetchRemarksDialog = function closeFetchRemarksDialog() {
    setShowFetchRemarksDialog(false);
  };

  var value = {
    ids: ids,
    setIds: setIds,
    productId: productId,
    setProductId: setProductId,
    queryParams: queryParams,
    setQueryParams: setQueryParams,
    initRemark: initRemark,
    selectedId: selectedId,
    showEditRemarkDialog: showEditRemarkDialog,
    openNewRemarkDialog: openNewRemarkDialog,
    openEditRemarkDialog: openEditRemarkDialog,
    closeEditRemarkDialog: closeEditRemarkDialog,
    showDeleteRemarkDialog: showDeleteRemarkDialog,
    openDeleteRemarkDialog: openDeleteRemarkDialog,
    closeDeleteRemarkDialog: closeDeleteRemarkDialog,
    showDeleteRemarksDialog: showDeleteRemarksDialog,
    openDeleteRemarksDialog: openDeleteRemarksDialog,
    closeDeleteRemarksDialog: closeDeleteRemarksDialog,
    openFetchRemarksDialog: openFetchRemarksDialog,
    closeFetchRemarksDialog: closeFetchRemarksDialog,
    showFetchRemarksDialog: showFetchRemarksDialog
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(RemarksUIContext.Provider, {
    value: value
  }, children);
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksUIHelper.js":
/*!*****************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksUIHelper.js ***!
  \*****************************************************************************************************/
/*! exports provided: defaultSorted, sizePerPageList, initialFilter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "defaultSorted", function() { return defaultSorted; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sizePerPageList", function() { return sizePerPageList; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initialFilter", function() { return initialFilter; });
var defaultSorted = [{
  dataField: "id",
  order: "asc"
}];
var sizePerPageList = [{
  text: "1",
  value: 1
}, {
  text: "3",
  value: 3
}, {
  text: "5",
  value: 5
}];
var initialFilter = {
  filter: {
    text: ""
  },
  sortOrder: "asc",
  // asc||desc
  sortField: "id",
  pageNumber: 1,
  pageSize: 5
};

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/column-formatters/ActionsColumnFormatter.js":
/*!******************************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/column-formatters/ActionsColumnFormatter.js ***!
  \******************************************************************************************************************************/
/*! exports provided: ActionsColumnFormatter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionsColumnFormatter", function() { return ActionsColumnFormatter; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-inlinesvg'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _metronic_helpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../_metronic/_helpers */ "./resources/demo2/src/_metronic/_helpers/index.js");
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */




var ActionsColumnFormatter = function ActionsColumnFormatter(cellContent, row, rowIndex, _ref) {
  var openEditRemarkDialog = _ref.openEditRemarkDialog,
      openDeleteRemarkDialog = _ref.openDeleteRemarkDialog;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    overlay: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
      id: "customers-edit-tooltip"
    }, "Edit remark")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: "btn btn-icon btn-light btn-hover-primary btn-sm mx-3",
    onClick: function onClick() {
      return openEditRemarkDialog(row.id);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "svg-icon svg-icon-md svg-icon-primary"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-inlinesvg'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    src: Object(_metronic_helpers__WEBPACK_IMPORTED_MODULE_2__["toAbsoluteUrl"])("/media/svg/icons/Communication/Write.svg")
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    overlay: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
      id: "customers-delete-tooltip"
    }, "Delete remark")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: "btn btn-icon btn-light btn-hover-danger btn-sm",
    onClick: function onClick() {
      return openDeleteRemarkDialog(row.id);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "svg-icon svg-icon-md svg-icon-danger"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-inlinesvg'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    src: Object(_metronic_helpers__WEBPACK_IMPORTED_MODULE_2__["toAbsoluteUrl"])("/media/svg/icons/General/Trash.svg")
  })))));
};

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/remark-edit-dialog/RemarkEditDialog.js":
/*!*************************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/remark-edit-dialog/RemarkEditDialog.js ***!
  \*************************************************************************************************************************/
/*! exports provided: RemarkEditDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RemarkEditDialog", function() { return RemarkEditDialog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _redux_remarks_remarksActions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../_redux/remarks/remarksActions */ "./resources/demo2/src/app/modules/ECommerce/_redux/remarks/remarksActions.js");
/* harmony import */ var _RemarkEditDialogHeader__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./RemarkEditDialogHeader */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/remark-edit-dialog/RemarkEditDialogHeader.js");
/* harmony import */ var _RemarkEditForm__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./RemarkEditForm */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/remark-edit-dialog/RemarkEditForm.js");
/* harmony import */ var _RemarksUIContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../RemarksUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/RemarksUIContext.js");








function getFormattedDate(date) {
  if (typeof date === "string") {
    return date;
  }

  var year = date.getFullYear();
  var month = (1 + date.getMonth()).toString();
  month = month.length > 1 ? month : "0" + month;
  var day = date.getDate().toString();
  day = day.length > 1 ? day : "0" + day;
  return month + "/" + day + "/" + year;
}

function RemarkEditDialog() {
  // Remarks UI Context
  var remarksUIContext = Object(_RemarksUIContext__WEBPACK_IMPORTED_MODULE_5__["useRemarksUIContext"])();
  var remarksUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      id: remarksUIContext.selectedId,
      setIds: remarksUIContext.setIds,
      productId: remarksUIContext.productId,
      queryParams: remarksUIContext.queryParams,
      showEditRemarkDialog: remarksUIContext.showEditRemarkDialog,
      closeEditRemarkDialog: remarksUIContext.closeEditRemarkDialog,
      initRemark: remarksUIContext.initRemark
    };
  }, [remarksUIContext]); // Remarks Redux state

  var dispatch = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())();

  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      actionsLoading: state.remarks.actionsLoading,
      remarkForEdit: state.remarks.remarkForEdit
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      actionsLoading = _useSelector.actionsLoading,
      remarkForEdit = _useSelector.remarkForEdit;

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    // server request for getting remark by seleted id
    dispatch(_redux_remarks_remarksActions__WEBPACK_IMPORTED_MODULE_2__["fetchRemark"](remarksUIProps.id));
  }, [remarksUIProps.id, dispatch]);

  var saveRemark = function saveRemark(remark) {
    remark.dueDate = getFormattedDate(remark.dueDate);

    if (!remarksUIProps.id) {
      // server request for creating remarks
      dispatch(_redux_remarks_remarksActions__WEBPACK_IMPORTED_MODULE_2__["createRemark"](remark)).then(function () {
        // refresh list after deletion
        dispatch(_redux_remarks_remarksActions__WEBPACK_IMPORTED_MODULE_2__["fetchRemarks"](remarksUIProps.queryParams, remarksUIProps.productId)).then(function () {
          // clear selections list
          remarksUIProps.setIds([]); // closing edit modal

          remarksUIProps.closeEditRemarkDialog();
        });
      });
    } else {
      // server request for updating remarks
      dispatch(_redux_remarks_remarksActions__WEBPACK_IMPORTED_MODULE_2__["updateRemark"](remark)).then(function () {
        // refresh list after deletion
        dispatch( // refresh list after deletion
        _redux_remarks_remarksActions__WEBPACK_IMPORTED_MODULE_2__["fetchRemarks"](remarksUIProps.queryParams, remarksUIProps.productId)).then(function () {
          // clear selections list
          remarksUIProps.setIds([]); // closing edit modal

          remarksUIProps.closeEditRemarkDialog();
        });
      });
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    show: remarksUIProps.showEditRemarkDialog,
    onHide: remarksUIProps.closeEditRemarkDialog,
    "aria-labelledby": "example-modal-sizes-title-lg"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_RemarkEditDialogHeader__WEBPACK_IMPORTED_MODULE_3__["RemarkEditDialogHeader"], {
    id: remarksUIProps.id
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_RemarkEditForm__WEBPACK_IMPORTED_MODULE_4__["RemarkEditForm"], {
    saveRemark: saveRemark,
    actionsLoading: actionsLoading,
    remark: remarkForEdit || remarksUIProps.initRemark,
    onHide: remarksUIProps.closeEditRemarkDialog
  }));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/remark-edit-dialog/RemarkEditDialogHeader.js":
/*!*******************************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/remark-edit-dialog/RemarkEditDialogHeader.js ***!
  \*******************************************************************************************************************************/
/*! exports provided: RemarkEditDialogHeader */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RemarkEditDialogHeader", function() { return RemarkEditDialogHeader; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

/* eslint-disable no-restricted-imports */




function RemarkEditDialogHeader(_ref) {
  var id = _ref.id;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState2 = _slicedToArray(_useState, 2),
      title = _useState2[0],
      setTitle = _useState2[1]; // Remarks Redux state


  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      remarkForEdit: state.remarks.remarkForEdit,
      actionsLoading: state.remarks.actionsLoading
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      remarkForEdit = _useSelector.remarkForEdit,
      actionsLoading = _useSelector.actionsLoading;

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    var _title = id ? "" : "New Remark";

    if (remarkForEdit && id) {
      _title = "Edit remark";
    }

    setTitle(_title); // eslint-disable-next-line
  }, [remarkForEdit, actionsLoading]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, actionsLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["ModalProgressBar"], {
    variant: "query"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Header, {
    closeButton: true
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Title, {
    id: "example-modal-sizes-title-lg"
  }, title)));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/remark-edit-dialog/RemarkEditForm.js":
/*!***********************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-remarks/remark-edit-dialog/RemarkEditForm.js ***!
  \***********************************************************************************************************************/
/*! exports provided: RemarkEditForm */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RemarkEditForm", function() { return RemarkEditForm; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");
// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10




 // Validation schema

var RemarkEditSchema = !(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())().shape({
  text: !(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())().min(2, "Minimum 2 symbols").max(50, "Maximum 50 symbols").required("Text is required"),
  type: !(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())().required("Type is required"),
  dueDate: !(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())().nullable(false).required("Due date is required")
});
function RemarkEditForm(_ref) {
  var saveRemark = _ref.saveRemark,
      remark = _ref.remark,
      actionsLoading = _ref.actionsLoading,
      onHide = _ref.onHide;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    enableReinitialize: true,
    initialValues: remark,
    validationSchema: RemarkEditSchema,
    onSubmit: function onSubmit(values) {
      return saveRemark(values);
    }
  }, function (_ref2) {
    var handleSubmit = _ref2.handleSubmit;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Body, {
      className: "overlay overlay-block cursor-default"
    }, actionsLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "overlay-layer bg-transparent"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "spinner spinner-lg spinner-success"
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
      className: "form form-label-right"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "form-group row"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-12"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
      name: "text",
      component: _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["Input"],
      placeholder: "Text",
      label: "Text"
    }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "form-group row"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-12"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["DatePickerField"], {
      name: "dueDate",
      label: "Due date"
    }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "form-group row"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-12"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["Select"], {
      name: "type",
      label: "Type"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      value: "0"
    }, "Info"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      value: "1"
    }, "Note"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      value: "2"
    }, "Reminder")))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Footer, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      type: "button",
      onClick: onHide,
      className: "btn btn-light btn-elevate"
    }, "Cancel"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      type: "submit",
      onClick: function onClick() {
        return handleSubmit();
      },
      className: "btn btn-primary btn-elevate"
    }, "Save")));
  }));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationDeleteDialog.js":
/*!**********************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationDeleteDialog.js ***!
  \**********************************************************************************************************************/
/*! exports provided: SpecificationDeleteDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpecificationDeleteDialog", function() { return SpecificationDeleteDialog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");
/* harmony import */ var _redux_specifications_specificationsActions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../_redux/specifications/specificationsActions */ "./resources/demo2/src/app/modules/ECommerce/_redux/specifications/specificationsActions.js");
/* harmony import */ var _SpecificationsUIContext__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./SpecificationsUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsUIContext.js");
/* eslint-disable no-restricted-imports */






function SpecificationDeleteDialog() {
  // Specifications UI Context
  var specsUIContext = Object(_SpecificationsUIContext__WEBPACK_IMPORTED_MODULE_4__["useSpecificationsUIContext"])();
  var specsUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      id: specsUIContext.selectedId,
      productId: specsUIContext.productId,
      show: specsUIContext.showDeleteSpecificationDialog,
      onHide: specsUIContext.closeDeleteSpecificationDialog,
      queryParams: specsUIContext.queryParams,
      setIds: specsUIContext.setIds
    };
  }, [specsUIContext]); // Specs Redux state

  var dispatch = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())();

  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      isLoading: state.specifications.actionsLoading
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      isLoading = _useSelector.isLoading; // if !id we should close modal


  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (!specsUIProps.id) {
      specsUIProps.onHide();
    } // eslint-disable-next-line react-hooks/exhaustive-deps

  }, [specsUIProps.id]); // looking for loading/dispatch

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {}, [isLoading, dispatch]);

  var deleteSpecification = function deleteSpecification() {
    // server request for deleting spec by id
    dispatch(_redux_specifications_specificationsActions__WEBPACK_IMPORTED_MODULE_3__["deleteSpecification"](specsUIProps.id)).then(function () {
      // refresh list after deletion
      dispatch(_redux_specifications_specificationsActions__WEBPACK_IMPORTED_MODULE_3__["fetchSpecifications"](specsUIProps.queryParams, specsUIProps.productId));
      specsUIProps.setIds([]);
      specsUIProps.onHide();
    });
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    show: specsUIProps.show,
    onHide: specsUIProps.onHide,
    "aria-labelledby": "example-modal-sizes-title-lg"
  }, isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["ModalProgressBar"], {
    variant: "query"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Header, {
    closeButton: true
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Title, {
    id: "example-modal-sizes-title-lg"
  }, "Specification Delete")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Body, null, !isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Are you sure to permanently delete this specification?"), isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Specification is deleting...")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Footer, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: specsUIProps.onHide,
    className: "btn btn-light btn-elevate"
  }, "Cancel"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: deleteSpecification,
    className: "btn btn-primary btn-elevate"
  }, "Delete"))));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/Specifications.js":
/*!***********************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/Specifications.js ***!
  \***********************************************************************************************************/
/*! exports provided: Specifications */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Specifications", function() { return Specifications; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _SpecificationsFilter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SpecificationsFilter */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsFilter.js");
/* harmony import */ var _SpecificationsTable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./SpecificationsTable */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsTable.js");
/* harmony import */ var _SpecificationsLoadingDialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./SpecificationsLoadingDialog */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsLoadingDialog.js");
/* harmony import */ var _SpecificationsDeleteDialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./SpecificationsDeleteDialog */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsDeleteDialog.js");
/* harmony import */ var _SpecificationDeleteDialog__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./SpecificationDeleteDialog */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationDeleteDialog.js");
/* harmony import */ var _SpecificationsFetchDialog__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./SpecificationsFetchDialog */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsFetchDialog.js");
/* harmony import */ var _SpecificationsGrouping__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./SpecificationsGrouping */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsGrouping.js");
/* harmony import */ var _specification_edit_dialog_SpecificationEditDialog__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./specification-edit-dialog/SpecificationEditDialog */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/specification-edit-dialog/SpecificationEditDialog.js");
/* harmony import */ var _SpecificationsUIContext__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./SpecificationsUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsUIContext.js");










function Specifications() {
  // Specifications UI Context
  var specsUIContext = Object(_SpecificationsUIContext__WEBPACK_IMPORTED_MODULE_9__["useSpecificationsUIContext"])();
  var specsUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      ids: specsUIContext.ids
    };
  }, [specsUIContext]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_SpecificationsLoadingDialog__WEBPACK_IMPORTED_MODULE_3__["SpecificationsLoadingDialog"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_specification_edit_dialog_SpecificationEditDialog__WEBPACK_IMPORTED_MODULE_8__["SpecificationEditDialog"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_SpecificationDeleteDialog__WEBPACK_IMPORTED_MODULE_5__["SpecificationDeleteDialog"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_SpecificationsDeleteDialog__WEBPACK_IMPORTED_MODULE_4__["SpecificationsDeleteDialog"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_SpecificationsFetchDialog__WEBPACK_IMPORTED_MODULE_6__["SpecificationsFetchDialog"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form margin-b-30"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_SpecificationsFilter__WEBPACK_IMPORTED_MODULE_1__["SpecificationsFilter"], null), specsUIProps.ids.length > 0 && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_SpecificationsGrouping__WEBPACK_IMPORTED_MODULE_7__["SpecificationsGrouping"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_SpecificationsTable__WEBPACK_IMPORTED_MODULE_2__["SpecificationsTable"], null));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsDeleteDialog.js":
/*!***********************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsDeleteDialog.js ***!
  \***********************************************************************************************************************/
/*! exports provided: SpecificationsDeleteDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpecificationsDeleteDialog", function() { return SpecificationsDeleteDialog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");
/* harmony import */ var _redux_specifications_specificationsActions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../_redux/specifications/specificationsActions */ "./resources/demo2/src/app/modules/ECommerce/_redux/specifications/specificationsActions.js");
/* harmony import */ var _SpecificationsUIContext__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./SpecificationsUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsUIContext.js");
/* eslint-disable no-restricted-imports */






function SpecificationsDeleteDialog() {
  // Specs UI Context
  var specsUIContext = Object(_SpecificationsUIContext__WEBPACK_IMPORTED_MODULE_4__["useSpecificationsUIContext"])();
  var specsUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      productId: specsUIContext.productId,
      ids: specsUIContext.ids,
      show: specsUIContext.showDeleteSpecificationsDialog,
      onHide: specsUIContext.closeDeleteSpecificationsDialog,
      setIds: specsUIContext.setIds,
      queryParams: specsUIContext.queryParams
    };
  }, [specsUIContext]); // Specs Redux state

  var dispatch = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())();

  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      isLoading: state.specifications.actionsLoading
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      isLoading = _useSelector.isLoading; // looking for loading/dispatch


  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {}, [isLoading, dispatch]); // if there weren't selected specs we should close modal

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (!specsUIProps.ids || specsUIProps.ids.length === 0) {
      specsUIProps.onHide();
    } // eslint-disable-next-line react-hooks/exhaustive-deps

  }, [specsUIProps.ids]);

  var deleteSpecifications = function deleteSpecifications() {
    // server request for selected deleting specs
    dispatch(_redux_specifications_specificationsActions__WEBPACK_IMPORTED_MODULE_3__["deleteSpecifications"](specsUIProps.ids)).then(function () {
      // refresh list after deletion
      dispatch(_redux_specifications_specificationsActions__WEBPACK_IMPORTED_MODULE_3__["fetchSpecifications"](specsUIProps.queryParams, specsUIProps.productId)).then(function () {
        specsUIProps.setIds([]);
        specsUIProps.onHide();
      });
    });
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    show: specsUIProps.show,
    onHide: specsUIProps.onHide,
    "aria-labelledby": "example-modal-sizes-title-lg"
  }, isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["ModalProgressBar"], {
    variant: "query"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Header, {
    closeButton: true
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Title, {
    id: "example-modal-sizes-title-lg"
  }, "Specifications Delete")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Body, null, !isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Are you sure to permanently delete selected specifications?"), isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Specifications are deleting...")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Footer, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: specsUIProps.onHide,
    className: "btn btn-light btn-elevate"
  }, "Cancel"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: deleteSpecifications,
    className: "btn btn-primary btn-elevate"
  }, "Delete"))));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsFetchDialog.js":
/*!**********************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsFetchDialog.js ***!
  \**********************************************************************************************************************/
/*! exports provided: SpecificationsFetchDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpecificationsFetchDialog", function() { return SpecificationsFetchDialog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _SpecificationsUIContext__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./SpecificationsUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsUIContext.js");





var selectedSpecifications = function selectedSpecifications(entities, ids) {
  var _specifications = [];
  ids.forEach(function (id) {
    var specification = entities.find(function (el) {
      return el.id === id;
    });

    if (specification) {
      _specifications.push(specification);
    }
  });
  return _specifications;
};

function SpecificationsFetchDialog() {
  // Specs UI Context
  var specsUIContext = Object(_SpecificationsUIContext__WEBPACK_IMPORTED_MODULE_2__["useSpecificationsUIContext"])();
  var specsUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      ids: specsUIContext.ids,
      show: specsUIContext.showFetchSpecificationsDialog,
      onHide: specsUIContext.closeFetchSpecificationsDialog,
      queryParams: specsUIContext.queryParams
    };
  }, [specsUIContext]); // Specs Redux state

  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      specifications: selectedSpecifications(state.specifications.entities, specsUIProps.ids)
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      specifications = _useSelector.specifications;

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (!specsUIProps.ids || specsUIProps.ids.length === 0) {
      specsUIProps.onHide();
    } // eslint-disable-next-line react-hooks/exhaustive-deps

  }, [specsUIProps.ids]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    show: specsUIProps.show,
    onHide: specsUIProps.onHide,
    "aria-labelledby": "example-modal-sizes-title-lg"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Header, {
    closeButton: true
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Title, {
    id: "example-modal-sizes-title-lg"
  }, "Fetch selected elements")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Body, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "list-timeline list-timeline-skin-light padding-30"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "list-timeline-items"
  }, specifications.map(function (specification) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "list-timeline-item mb-3",
      key: specification.id
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "list-timeline-text"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "label label-lg label-light-success label-inline",
      style: {
        width: "60px"
      }
    }, "ID: ", specification.id), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "ml-5"
    }, specification.name, ": ", specification.value, " ")));
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Footer, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: specsUIProps.onHide,
    className: "btn btn-light btn-elevate"
  }, "Cancel"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: specsUIProps.onHide,
    className: "btn btn-primary btn-elevate"
  }, "Ok"))));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsFilter.js":
/*!*****************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsFilter.js ***!
  \*****************************************************************************************************************/
/*! exports provided: SpecificationsFilter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpecificationsFilter", function() { return SpecificationsFilter; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _SpecificationsUIContext__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./SpecificationsUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsUIContext.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






var prepareFilter = function prepareFilter(queryParams, values) {
  var searchText = values.searchText;

  var newQueryParams = _objectSpread({}, queryParams);

  var filter = {};
  filter.value = searchText;

  if (searchText) {
    filter.name = searchText;
  }

  newQueryParams.filter = filter;
  return newQueryParams;
};

function SpecificationsFilter() {
  // Specs UI Context
  var specsUIContext = Object(_SpecificationsUIContext__WEBPACK_IMPORTED_MODULE_3__["useSpecificationsUIContext"])();
  var specsUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      openNewSpecificationDialog: specsUIContext.openNewSpecificationDialog,
      setQueryParams: specsUIContext.setQueryParams,
      queryParams: specsUIContext.queryParams
    };
  }, [specsUIContext]);

  var applyFilter = function applyFilter(values) {
    var newQueryParams = prepareFilter(specsUIProps.queryParams, values);

    if (!Object(lodash__WEBPACK_IMPORTED_MODULE_2__["isEqual"])(newQueryParams, specsUIProps.queryParams)) {
      newQueryParams.pageNumber = 1;
      specsUIProps.setQueryParams(newQueryParams);
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form-filtration"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "row align-items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-md-2 margin-bottom-10-mobile"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    initialValues: {
      searchText: ""
    },
    onSubmit: function onSubmit(values) {
      applyFilter(values);
    }
  }, function (_ref) {
    var values = _ref.values,
        handleSubmit = _ref.handleSubmit,
        handleBlur = _ref.handleBlur,
        handleChange = _ref.handleChange,
        setFieldValue = _ref.setFieldValue;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
      onSubmit: handleSubmit
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      type: "text",
      className: "form-control",
      name: "searchText",
      placeholder: "Search",
      onBlur: handleBlur,
      value: values.searchText,
      onChange: function onChange(e) {
        setFieldValue("searchText", e.target.value);
        handleSubmit();
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("small", {
      className: "form-text text-muted"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("b", null, "Search"), " in all fields")));
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-md-7 margin-bottom-10-mobile"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-md-3 text-right margin-bottom-10-mobile"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    className: "btn btn-primary",
    onClick: function onClick() {
      return specsUIProps.openNewSpecificationDialog();
    }
  }, "Create new specification")))));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsGrouping.js":
/*!*******************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsGrouping.js ***!
  \*******************************************************************************************************************/
/*! exports provided: SpecificationsGrouping */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpecificationsGrouping", function() { return SpecificationsGrouping; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _SpecificationsUIContext__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SpecificationsUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsUIContext.js");


function SpecificationsGrouping() {
  // Specs UI Context
  var specsUIContext = Object(_SpecificationsUIContext__WEBPACK_IMPORTED_MODULE_1__["useSpecificationsUIContext"])();
  var specsUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      ids: specsUIContext.ids,
      openDeleteSpecificationsDialog: specsUIContext.openDeleteSpecificationsDialog,
      openFetchSpecificationsDialog: specsUIContext.openFetchSpecificationsDialog
    };
  }, [specsUIContext]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "row align-items-center form-group-actions margin-top-20"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-xl-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form-group form-group-inline"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form-label form-label-no-wrap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "font-bold font-danger mt-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Selected records count: ", specsUIProps.ids.length))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form-group-inline"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    className: "btn btn-danger font-weight-bolder font-size-sm",
    onClick: specsUIProps.openDeleteSpecificationsDialog
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-trash"
  }), " Delete All"), "\xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    className: "btn btn-light-primary font-weight-bolder font-size-sm",
    onClick: specsUIProps.openFetchSpecificationsDialog
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-stream"
  }), " Fetch Selected"))))));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsLoadingDialog.js":
/*!************************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsLoadingDialog.js ***!
  \************************************************************************************************************************/
/*! exports provided: SpecificationsLoadingDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpecificationsLoadingDialog", function() { return SpecificationsLoadingDialog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");



function SpecificationsLoadingDialog() {
  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      isLoading: state.specifications.listLoading
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      isLoading = _useSelector.isLoading;

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {}, [isLoading]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["LoadingDialog"], {
    isLoading: isLoading,
    text: "Loading ..."
  });
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsTable.js":
/*!****************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsTable.js ***!
  \****************************************************************************************************************/
/*! exports provided: SpecificationsTable */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpecificationsTable", function() { return SpecificationsTable; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap-table-next'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap-table2-paginator'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _redux_specifications_specificationsActions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../_redux/specifications/specificationsActions */ "./resources/demo2/src/app/modules/ECommerce/_redux/specifications/specificationsActions.js");
/* harmony import */ var _column_formatters_ActionsColumnFormatter__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./column-formatters/ActionsColumnFormatter */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/column-formatters/ActionsColumnFormatter.js");
/* harmony import */ var _SpecificationsUIHelper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./SpecificationsUIHelper */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsUIHelper.js");
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");
/* harmony import */ var _metronic_helpers__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../../_metronic/_helpers */ "./resources/demo2/src/_metronic/_helpers/index.js");
/* harmony import */ var _SpecificationsUIContext__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./SpecificationsUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsUIContext.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html










function SpecificationsTable() {
  // Specs UI Context
  var specsUIContext = Object(_SpecificationsUIContext__WEBPACK_IMPORTED_MODULE_7__["useSpecificationsUIContext"])();
  var specsUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      queryParams: specsUIContext.queryParams,
      setQueryParams: specsUIContext.setQueryParams,
      openEditSpecificationDialog: specsUIContext.openEditSpecificationDialog,
      openDeleteSpecificationDialog: specsUIContext.openDeleteSpecificationDialog,
      ids: specsUIContext.ids,
      setIds: specsUIContext.setIds,
      productId: specsUIContext.productId
    };
  }, [specsUIContext]); // Specs Redux state
  // Getting curret state of products list from store (Redux)

  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      currentState: state.specifications
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      currentState = _useSelector.currentState;

  var totalCount = currentState.totalCount,
      entities = currentState.entities,
      listLoading = currentState.listLoading;
  var dispatch = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())();
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    specsUIProps.setIds([]);
    dispatch(_redux_specifications_specificationsActions__WEBPACK_IMPORTED_MODULE_2__["fetchSpecifications"](specsUIProps.queryParams, specsUIProps.productId)); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [specsUIProps.queryParams, dispatch, specsUIProps.productId]);
  var columns = [{
    dataField: "name",
    text: "Specification Type",
    sort: true,
    sortCaret: _metronic_helpers__WEBPACK_IMPORTED_MODULE_6__["sortCaret"]
  }, {
    dataField: "value",
    text: "Value",
    sort: true,
    sortCaret: _metronic_helpers__WEBPACK_IMPORTED_MODULE_6__["sortCaret"]
  }, {
    dataField: "action",
    text: "Actions",
    formatter: _column_formatters_ActionsColumnFormatter__WEBPACK_IMPORTED_MODULE_3__["ActionsColumnFormatter"],
    formatExtraData: {
      openEditSpecificationDialog: specsUIProps.openEditSpecificationDialog,
      openDeleteSpecificationDialog: specsUIProps.openDeleteSpecificationDialog
    },
    classes: "text-right pr-0",
    headerClasses: "text-right pr-3",
    style: {
      minWidth: "100px"
    }
  }]; // Table pagination properties

  var paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: _SpecificationsUIHelper__WEBPACK_IMPORTED_MODULE_4__["sizePerPageList"],
    sizePerPage: specsUIProps.queryParams.pageSize,
    page: specsUIProps.queryParams.pageNumber
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap-table2-paginator'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    pagination: !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap-table2-paginator'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(paginationOptions)
  }, function (_ref) {
    var paginationProps = _ref.paginationProps,
        paginationTableProps = _ref.paginationTableProps;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_5__["Pagination"], {
      isLoading: listLoading,
      paginationProps: paginationProps
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap-table-next'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), _extends({
      wrapperClasses: "table-responsive",
      classes: "table table-head-custom table-vertical-center overflow-hidden",
      bordered: false,
      bootstrap4: true,
      remote: true,
      keyField: "id",
      data: entities === null ? [] : entities,
      columns: columns,
      defaultSorted: _SpecificationsUIHelper__WEBPACK_IMPORTED_MODULE_4__["defaultSorted"],
      onTableChange: Object(_metronic_helpers__WEBPACK_IMPORTED_MODULE_6__["getHandlerTableChange"])(specsUIProps.setQueryParams),
      selectRow: Object(_metronic_helpers__WEBPACK_IMPORTED_MODULE_6__["getSelectRow"])({
        entities: entities,
        ids: specsUIProps.ids,
        setIds: specsUIProps.setIds
      })
    }, paginationTableProps), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_helpers__WEBPACK_IMPORTED_MODULE_6__["PleaseWaitMessage"], {
      entities: entities
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_helpers__WEBPACK_IMPORTED_MODULE_6__["NoRecordsFoundMessage"], {
      entities: entities
    })));
  }));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsUIContext.js":
/*!********************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsUIContext.js ***!
  \********************************************************************************************************************/
/*! exports provided: useSpecificationsUIContext, SpecificationsUIConsumer, SpecificationsUIProvider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useSpecificationsUIContext", function() { return useSpecificationsUIContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpecificationsUIConsumer", function() { return SpecificationsUIConsumer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpecificationsUIProvider", function() { return SpecificationsUIProvider; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _SpecificationsUIHelper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./SpecificationsUIHelper */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsUIHelper.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

/* eslint-disable no-unused-vars */



var SpecificationsUIContext = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_0__["createContext"])();
function useSpecificationsUIContext() {
  return Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(SpecificationsUIContext);
}
var SpecificationsUIConsumer = SpecificationsUIContext.Consumer;
function SpecificationsUIProvider(_ref) {
  var currentProductId = _ref.currentProductId,
      children = _ref.children;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(currentProductId),
      _useState2 = _slicedToArray(_useState, 2),
      productId = _useState2[0],
      setProductId = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(_SpecificationsUIHelper__WEBPACK_IMPORTED_MODULE_2__["initialFilter"]),
      _useState4 = _slicedToArray(_useState3, 2),
      queryParams = _useState4[0],
      setQueryParamsBase = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      _useState6 = _slicedToArray(_useState5, 2),
      ids = _useState6[0],
      setIds = _useState6[1];

  var setQueryParams = Object(react__WEBPACK_IMPORTED_MODULE_0__["useCallback"])(function (nextQueryParams) {
    setQueryParamsBase(function (prevQueryParams) {
      if (Object(lodash__WEBPACK_IMPORTED_MODULE_1__["isFunction"])(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (Object(lodash__WEBPACK_IMPORTED_MODULE_1__["isEqual"])(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null),
      _useState8 = _slicedToArray(_useState7, 2),
      selectedId = _useState8[0],
      setSelectedId = _useState8[1];

  var initSpecification = {
    id: undefined,
    value: "",
    specId: 0,
    carId: productId
  };
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    initSpecification.carId = currentProductId;
    initSpecification.productId = currentProductId;
    setProductId(currentProductId); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentProductId]);

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState10 = _slicedToArray(_useState9, 2),
      showEditSpecificationDialog = _useState10[0],
      setShowEditSpecificationDialog = _useState10[1];

  var openNewSpecificationDialog = function openNewSpecificationDialog() {
    setSelectedId(undefined);
    setShowEditSpecificationDialog(true);
  };

  var openEditSpecificationDialog = function openEditSpecificationDialog(id) {
    setSelectedId(id);
    setShowEditSpecificationDialog(true);
  };

  var closeEditSpecificationDialog = function closeEditSpecificationDialog() {
    setSelectedId(undefined);
    setShowEditSpecificationDialog(false);
  };

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState12 = _slicedToArray(_useState11, 2),
      showDeleteSpecificationDialog = _useState12[0],
      setShowDeleteSpecificationDialog = _useState12[1];

  var openDeleteSpecificationDialog = function openDeleteSpecificationDialog(id) {
    setSelectedId(id);
    setShowDeleteSpecificationDialog(true);
  };

  var closeDeleteSpecificationDialog = function closeDeleteSpecificationDialog() {
    setSelectedId(undefined);
    setShowDeleteSpecificationDialog(false);
  };

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState14 = _slicedToArray(_useState13, 2),
      showDeleteSpecificationsDialog = _useState14[0],
      setShowDeleteSpecificationsDialog = _useState14[1];

  var openDeleteSpecificationsDialog = function openDeleteSpecificationsDialog() {
    setShowDeleteSpecificationsDialog(true);
  };

  var closeDeleteSpecificationsDialog = function closeDeleteSpecificationsDialog() {
    setShowDeleteSpecificationsDialog(false);
  };

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      _useState16 = _slicedToArray(_useState15, 2),
      showFetchSpecificationsDialog = _useState16[0],
      setShowFetchSpecificationsDialog = _useState16[1];

  var openFetchSpecificationsDialog = function openFetchSpecificationsDialog() {
    setShowFetchSpecificationsDialog(true);
  };

  var closeFetchSpecificationsDialog = function closeFetchSpecificationsDialog() {
    setShowFetchSpecificationsDialog(false);
  };

  var value = {
    ids: ids,
    setIds: setIds,
    productId: productId,
    setProductId: setProductId,
    queryParams: queryParams,
    setQueryParams: setQueryParams,
    initSpecification: initSpecification,
    selectedId: selectedId,
    showEditSpecificationDialog: showEditSpecificationDialog,
    openEditSpecificationDialog: openEditSpecificationDialog,
    openNewSpecificationDialog: openNewSpecificationDialog,
    closeEditSpecificationDialog: closeEditSpecificationDialog,
    showDeleteSpecificationDialog: showDeleteSpecificationDialog,
    openDeleteSpecificationDialog: openDeleteSpecificationDialog,
    closeDeleteSpecificationDialog: closeDeleteSpecificationDialog,
    showDeleteSpecificationsDialog: showDeleteSpecificationsDialog,
    openDeleteSpecificationsDialog: openDeleteSpecificationsDialog,
    closeDeleteSpecificationsDialog: closeDeleteSpecificationsDialog,
    showFetchSpecificationsDialog: showFetchSpecificationsDialog,
    openFetchSpecificationsDialog: openFetchSpecificationsDialog,
    closeFetchSpecificationsDialog: closeFetchSpecificationsDialog
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(SpecificationsUIContext.Provider, {
    value: value
  }, children);
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsUIHelper.js":
/*!*******************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsUIHelper.js ***!
  \*******************************************************************************************************************/
/*! exports provided: defaultSorted, sizePerPageList, initialFilter, SPECIFICATIONS_DICTIONARY */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "defaultSorted", function() { return defaultSorted; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sizePerPageList", function() { return sizePerPageList; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initialFilter", function() { return initialFilter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SPECIFICATIONS_DICTIONARY", function() { return SPECIFICATIONS_DICTIONARY; });
var defaultSorted = [{
  dataField: "id",
  order: "asc"
}];
var sizePerPageList = [{
  text: "1",
  value: 1
}, {
  text: "3",
  value: 3
}, {
  text: "5",
  value: 5
}];
var initialFilter = {
  filter: {
    value: ""
  },
  sortOrder: "asc",
  // asc||desc
  sortField: "name",
  pageNumber: 1,
  pageSize: 5
};
var SPECIFICATIONS_DICTIONARY = [{
  id: 0,
  name: "Seats"
}, {
  id: 1,
  name: "Fuel Type"
}, {
  id: 2,
  name: "Stock"
}, {
  id: 3,
  name: "Door count"
}, {
  id: 4,
  name: "Engine"
}, {
  id: 5,
  name: "Transmission"
}, {
  id: 6,
  name: "Drivetrain"
}, {
  id: 7,
  name: "Combined MPG"
}, {
  id: 8,
  name: "Warranty"
}, {
  id: 9,
  name: "Wheels"
}];

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/column-formatters/ActionsColumnFormatter.js":
/*!*************************************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/column-formatters/ActionsColumnFormatter.js ***!
  \*************************************************************************************************************************************/
/*! exports provided: ActionsColumnFormatter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionsColumnFormatter", function() { return ActionsColumnFormatter; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-inlinesvg'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _metronic_helpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../_metronic/_helpers */ "./resources/demo2/src/_metronic/_helpers/index.js");
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */




function ActionsColumnFormatter(cellContent, row, rowIndex, _ref) {
  var openEditSpecificationDialog = _ref.openEditSpecificationDialog,
      openDeleteSpecificationDialog = _ref.openDeleteSpecificationDialog;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    overlay: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
      id: "specs-edit-tooltip"
    }, "Edit specification")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: "btn btn-icon btn-light btn-hover-primary btn-sm mx-3",
    onClick: function onClick() {
      return openEditSpecificationDialog(row.id);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "svg-icon svg-icon-md svg-icon-primary"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-inlinesvg'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    src: Object(_metronic_helpers__WEBPACK_IMPORTED_MODULE_2__["toAbsoluteUrl"])("/media/svg/icons/Communication/Write.svg")
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    overlay: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
      id: "spec-delete-tooltip"
    }, "Delete specification")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: "btn btn-icon btn-light btn-hover-danger btn-sm",
    onClick: function onClick() {
      return openDeleteSpecificationDialog(row.id);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "svg-icon svg-icon-md svg-icon-danger"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-inlinesvg'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    src: Object(_metronic_helpers__WEBPACK_IMPORTED_MODULE_2__["toAbsoluteUrl"])("/media/svg/icons/General/Trash.svg")
  })))));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/specification-edit-dialog/SpecificationEditDialog.js":
/*!**********************************************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/specification-edit-dialog/SpecificationEditDialog.js ***!
  \**********************************************************************************************************************************************/
/*! exports provided: SpecificationEditDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpecificationEditDialog", function() { return SpecificationEditDialog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _redux_specifications_specificationsActions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../_redux/specifications/specificationsActions */ "./resources/demo2/src/app/modules/ECommerce/_redux/specifications/specificationsActions.js");
/* harmony import */ var _SpecificationEditDialogHeader__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./SpecificationEditDialogHeader */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/specification-edit-dialog/SpecificationEditDialogHeader.js");
/* harmony import */ var _SpecificationEditForm__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./SpecificationEditForm */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/specification-edit-dialog/SpecificationEditForm.js");
/* harmony import */ var _SpecificationsUIContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../SpecificationsUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsUIContext.js");







function SpecificationEditDialog() {
  // Specifications UI Context
  var specsUIContext = Object(_SpecificationsUIContext__WEBPACK_IMPORTED_MODULE_5__["useSpecificationsUIContext"])();
  var specsUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      id: specsUIContext.selectedId,
      show: specsUIContext.showEditSpecificationDialog,
      onHide: specsUIContext.closeEditSpecificationDialog,
      productId: specsUIContext.productId,
      queryParams: specsUIContext.queryParams,
      initSpecification: specsUIContext.initSpecification
    };
  }, [specsUIContext]); // Specifications Redux state

  var dispatch = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())();

  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      actionsLoading: state.specifications.actionsLoading,
      specificationForEdit: state.specifications.specificationForEdit
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      actionsLoading = _useSelector.actionsLoading,
      specificationForEdit = _useSelector.specificationForEdit;

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    // server request for getting spec by seleted id
    dispatch(_redux_specifications_specificationsActions__WEBPACK_IMPORTED_MODULE_2__["fetchSpecification"](specsUIProps.id));
  }, [specsUIProps.id, dispatch]);

  var saveSpecification = function saveSpecification(specification) {
    if (!specsUIProps.id) {
      dispatch(_redux_specifications_specificationsActions__WEBPACK_IMPORTED_MODULE_2__["createSpecification"](specification)).then(function () {
        dispatch(_redux_specifications_specificationsActions__WEBPACK_IMPORTED_MODULE_2__["fetchSpecifications"](specsUIProps.queryParams, specsUIProps.productId)).then(function () {
          return specsUIProps.onHide();
        });
      });
    } else {
      dispatch(_redux_specifications_specificationsActions__WEBPACK_IMPORTED_MODULE_2__["updateSpecification"](specification)).then(function () {
        dispatch(_redux_specifications_specificationsActions__WEBPACK_IMPORTED_MODULE_2__["fetchSpecifications"](specsUIProps.queryParams, specsUIProps.productId)).then(function () {
          return specsUIProps.onHide();
        });
      });
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    show: specsUIProps.show,
    onHide: specsUIProps.onHide,
    "aria-labelledby": "example-modal-sizes-title-lg"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_SpecificationEditDialogHeader__WEBPACK_IMPORTED_MODULE_3__["SpecificationEditDialogHeader"], {
    id: specsUIProps.id
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_SpecificationEditForm__WEBPACK_IMPORTED_MODULE_4__["SpecificationEditForm"], {
    saveSpecification: saveSpecification,
    actionsLoading: actionsLoading,
    specification: specificationForEdit || specsUIProps.initSpecification,
    onHide: specsUIProps.onHide
  }));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/specification-edit-dialog/SpecificationEditDialogHeader.js":
/*!****************************************************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/specification-edit-dialog/SpecificationEditDialogHeader.js ***!
  \****************************************************************************************************************************************************/
/*! exports provided: SpecificationEditDialogHeader */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpecificationEditDialogHeader", function() { return SpecificationEditDialogHeader; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

/* eslint-disable no-restricted-imports */




function SpecificationEditDialogHeader(_ref) {
  var id = _ref.id;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""),
      _useState2 = _slicedToArray(_useState, 2),
      title = _useState2[0],
      setTitle = _useState2[1]; // Specs Redux state


  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      specificationForEdit: state.specifications.specificationForEdit,
      actionsLoading: state.specifications.actionsLoading
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      specificationForEdit = _useSelector.specificationForEdit,
      actionsLoading = _useSelector.actionsLoading;

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    var _title = id ? "" : "New Specification";

    if (specificationForEdit && id) {
      _title = "Edit Specification";
    }

    setTitle(_title); // eslint-disable-next-line
  }, [specificationForEdit, actionsLoading]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, actionsLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["ModalProgressBar"], {
    variant: "query"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Header, {
    closeButton: true
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Title, {
    id: "example-modal-sizes-title-lg"
  }, title)));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/specification-edit-dialog/SpecificationEditForm.js":
/*!********************************************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/specification-edit-dialog/SpecificationEditForm.js ***!
  \********************************************************************************************************************************************/
/*! exports provided: SpecificationEditForm */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpecificationEditForm", function() { return SpecificationEditForm; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");
/* harmony import */ var _SpecificationsUIHelper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../SpecificationsUIHelper */ "./resources/demo2/src/app/modules/ECommerce/pages/products/product-specifications/SpecificationsUIHelper.js");
// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10





 // Validation schema

var SpecificationEditSchema = !(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())().shape({
  value: !(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())().min(2, "Minimum 2 symbols").max(50, "Maximum 50 symbols").required("Value is required"),
  specId: !(function webpackMissingModule() { var e = new Error("Cannot find module 'yup'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())().required("Specification type is required")
});
function SpecificationEditForm(_ref) {
  var saveSpecification = _ref.saveSpecification,
      specification = _ref.specification,
      actionsLoading = _ref.actionsLoading,
      onHide = _ref.onHide;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    enableReinitialize: true,
    initialValues: specification,
    validationSchema: SpecificationEditSchema,
    onSubmit: function onSubmit(values) {
      saveSpecification(values);
    }
  }, function (_ref2) {
    var handleSubmit = _ref2.handleSubmit;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Body, {
      className: "overlay overlay-block cursor-default"
    }, actionsLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "overlay-layer bg-transparent"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "spinner spinner-lg spinner-success"
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
      className: "form form-label-right"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "form-group row"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-12"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["Select"], {
      name: "specId",
      label: "Specification Type"
    }, _SpecificationsUIHelper__WEBPACK_IMPORTED_MODULE_3__["SPECIFICATIONS_DICTIONARY"].map(function (spec) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
        key: spec.id,
        value: spec.id
      }, spec.name);
    })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "form-group row"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-12"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
      name: "value",
      component: _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["Input"],
      placeholder: "Value",
      label: "Value"
    }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Footer, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      type: "button",
      onClick: onHide,
      className: "btn btn-light btn-elevate"
    }, "Cancel"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      type: "submit",
      onClick: function onClick() {
        return handleSubmit();
      },
      className: "btn btn-primary btn-elevate"
    }, "Save")));
  }));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-delete-dialog/ProductsDeleteDialog.js":
/*!*****************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/products-delete-dialog/ProductsDeleteDialog.js ***!
  \*****************************************************************************************************************/
/*! exports provided: ProductsDeleteDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsDeleteDialog", function() { return ProductsDeleteDialog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");
/* harmony import */ var _redux_products_productsActions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../_redux/products/productsActions */ "./resources/demo2/src/app/modules/ECommerce/_redux/products/productsActions.js");
/* harmony import */ var _ProductsUIContext__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../ProductsUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsUIContext.js");
/* eslint-disable no-restricted-imports */






function ProductsDeleteDialog(_ref) {
  var show = _ref.show,
      onHide = _ref.onHide;
  // Products UI Context
  var productsUIContext = Object(_ProductsUIContext__WEBPACK_IMPORTED_MODULE_4__["useProductsUIContext"])();
  var productsUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      ids: productsUIContext.ids,
      setIds: productsUIContext.setIds,
      queryParams: productsUIContext.queryParams
    };
  }, [productsUIContext]); // Products Redux state

  var dispatch = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())();

  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      isLoading: state.products.actionsLoading
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      isLoading = _useSelector.isLoading; // looking for loading/dispatch


  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {}, [isLoading, dispatch]); // if there weren't selected products we should close modal

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (!productsUIProps.ids || productsUIProps.ids.length === 0) {
      onHide();
    } // eslint-disable-next-line react-hooks/exhaustive-deps

  }, [productsUIProps.ids]);

  var deleteProducts = function deleteProducts() {
    // server request for deleting product by seleted ids
    dispatch(_redux_products_productsActions__WEBPACK_IMPORTED_MODULE_3__["deleteProducts"](productsUIProps.ids)).then(function () {
      // refresh list after deletion
      dispatch(_redux_products_productsActions__WEBPACK_IMPORTED_MODULE_3__["fetchProducts"](productsUIProps.queryParams)).then(function () {
        // clear selections list
        productsUIProps.setIds([]); // closing delete modal

        onHide();
      });
    });
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    show: show,
    onHide: onHide,
    "aria-labelledby": "example-modal-sizes-title-lg"
  }, isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["ModalProgressBar"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Header, {
    closeButton: true
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Title, {
    id: "example-modal-sizes-title-lg"
  }, "Products Delete")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Body, null, !isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Are you sure to permanently delete selected products?"), isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Products are deleting...")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Footer, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: onHide,
    className: "btn btn-light btn-elevate"
  }, "Cancel"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: deleteProducts,
    className: "btn btn-primary btn-elevate"
  }, "Delete"))));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-fetch-dialog/ProductsFetchDialog.js":
/*!***************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/products-fetch-dialog/ProductsFetchDialog.js ***!
  \***************************************************************************************************************/
/*! exports provided: ProductsFetchDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsFetchDialog", function() { return ProductsFetchDialog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _ProductsUIHelpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../ProductsUIHelpers */ "./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsUIHelpers.js");
/* harmony import */ var _ProductsUIContext__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../ProductsUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsUIContext.js");






var selectedProducts = function selectedProducts(entities, ids) {
  var _products = [];
  ids.forEach(function (id) {
    var product = entities.find(function (el) {
      return el.id === id;
    });

    if (product) {
      _products.push(product);
    }
  });
  return _products;
};

function ProductsFetchDialog(_ref) {
  var show = _ref.show,
      onHide = _ref.onHide;
  // Products UI Context
  var productsUIContext = Object(_ProductsUIContext__WEBPACK_IMPORTED_MODULE_3__["useProductsUIContext"])();
  var productsUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      ids: productsUIContext.ids,
      queryParams: productsUIContext.queryParams
    };
  }, [productsUIContext]); // Products Redux state

  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      products: selectedProducts(state.products.entities, productsUIProps.ids)
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      products = _useSelector.products; // if there weren't selected ids we should close modal


  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (!productsUIProps.ids || productsUIProps.ids.length === 0) {
      onHide();
    } // eslint-disable-next-line react-hooks/exhaustive-deps

  }, [productsUIProps.ids]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    show: show,
    onHide: onHide,
    "aria-labelledby": "example-modal-sizes-title-lg"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Header, {
    closeButton: true
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Title, {
    id: "example-modal-sizes-title-lg"
  }, "Fetch selected elements")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Body, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "list-timeline list-timeline-skin-light padding-30"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "list-timeline-items"
  }, products.map(function (product) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "list-timeline-item mb-3",
      key: product.id
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "list-timeline-text"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "label label-lg label-light-".concat(_ProductsUIHelpers__WEBPACK_IMPORTED_MODULE_2__["ProductStatusCssClasses"][product.status], " label-inline"),
      style: {
        width: "60px"
      }
    }, "ID: ", product.id), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "ml-5"
    }, product.manufacture, ", ", product.model)));
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Footer, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: onHide,
    className: "btn btn-light btn-elevate"
  }, "Cancel"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: onHide,
    className: "btn btn-primary btn-elevate"
  }, "Ok"))));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-filter/ProductsFilter.js":
/*!****************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/products-filter/ProductsFilter.js ***!
  \****************************************************************************************************/
/*! exports provided: ProductsFilter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsFilter", function() { return ProductsFilter; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _ProductsUIContext__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../ProductsUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsUIContext.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






var prepareFilter = function prepareFilter(queryParams, values) {
  var status = values.status,
      condition = values.condition,
      searchText = values.searchText;

  var newQueryParams = _objectSpread({}, queryParams);

  var filter = {}; // Filter by status

  filter.status = status !== "" ? +status : undefined; // Filter by condition

  filter.condition = condition !== "" ? +condition : undefined; // Filter by all fields

  filter.model = searchText;

  if (searchText) {
    filter.manufacture = searchText;
    filter.VINCode = searchText;
  }

  newQueryParams.filter = filter;
  return newQueryParams;
};

function ProductsFilter(_ref) {
  var listLoading = _ref.listLoading;
  // Products UI Context
  var productsUIContext = Object(_ProductsUIContext__WEBPACK_IMPORTED_MODULE_3__["useProductsUIContext"])();
  var productsUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      setQueryParams: productsUIContext.setQueryParams,
      queryParams: productsUIContext.queryParams
    };
  }, [productsUIContext]);

  var applyFilter = function applyFilter(values) {
    var newQueryParams = prepareFilter(productsUIProps.queryParams, values);

    if (!Object(lodash__WEBPACK_IMPORTED_MODULE_2__["isEqual"])(newQueryParams, productsUIProps.queryParams)) {
      newQueryParams.pageNumber = 1;
      productsUIProps.setQueryParams(newQueryParams);
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'formik'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    initialValues: {
      status: "",
      // values => All=""/Selling=0/Sold=1
      condition: "",
      // values => All=""/New=0/Used=1
      searchText: ""
    },
    onSubmit: function onSubmit(values) {
      applyFilter(values);
    }
  }, function (_ref2) {
    var values = _ref2.values,
        handleSubmit = _ref2.handleSubmit,
        handleBlur = _ref2.handleBlur,
        handleChange = _ref2.handleChange,
        setFieldValue = _ref2.setFieldValue;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
      onSubmit: handleSubmit,
      className: "form form-label-right"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "form-group row"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-2"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("select", {
      className: "form-control",
      name: "status",
      placeholder: "Filter by Status",
      onChange: function onChange(e) {
        setFieldValue("status", e.target.value);
        handleSubmit();
      },
      onBlur: handleBlur,
      value: values.status
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      value: ""
    }, "All"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      value: "0"
    }, "Selling"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      value: "1"
    }, "Sold")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("small", {
      className: "form-text text-muted"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("b", null, "Filter"), " by Status")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-2"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("select", {
      className: "form-control",
      placeholder: "Filter by Type",
      name: "condition",
      onBlur: handleBlur,
      onChange: function onChange(e) {
        setFieldValue("condition", e.target.value);
        handleSubmit();
      },
      value: values.condition
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      value: ""
    }, "All"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      value: "0"
    }, "New"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
      value: "1"
    }, "Used")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("small", {
      className: "form-text text-muted"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("b", null, "Filter"), " by Condition")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-lg-2"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      type: "text",
      className: "form-control",
      name: "searchText",
      placeholder: "Search",
      onBlur: handleBlur,
      value: values.searchText,
      onChange: function onChange(e) {
        setFieldValue("searchText", e.target.value);
        handleSubmit();
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("small", {
      className: "form-text text-muted"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("b", null, "Search"), " in all fields"))));
  }));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-grouping/ProductsGrouping.js":
/*!********************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/products-grouping/ProductsGrouping.js ***!
  \********************************************************************************************************/
/*! exports provided: ProductsGrouping */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsGrouping", function() { return ProductsGrouping; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ProductsUIContext__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../ProductsUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsUIContext.js");


function ProductsGrouping() {
  // Products UI Context
  var productsUIContext = Object(_ProductsUIContext__WEBPACK_IMPORTED_MODULE_1__["useProductsUIContext"])();
  var productsUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      ids: productsUIContext.ids,
      setIds: productsUIContext.setIds,
      openDeleteProductsDialog: productsUIContext.openDeleteProductsDialog,
      openFetchProductsDialog: productsUIContext.openFetchProductsDialog,
      openUpdateProductsStatusDialog: productsUIContext.openUpdateProductsStatusDialog
    };
  }, [productsUIContext]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "row align-items-center form-group-actions margin-top-20 margin-bottom-20"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-xl-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form-group form-group-inline"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form-label form-label-no-wrap"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    className: "-font-bold font-danger-"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Selected records count: ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("b", null, productsUIProps.ids.length)))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    className: "btn btn-danger font-weight-bolder font-size-sm",
    onClick: productsUIProps.openDeleteProductsDialog
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-trash"
  }), " Delete All"), "\xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    className: "btn btn-light-primary font-weight-bolder font-size-sm",
    onClick: productsUIProps.openFetchProductsDialog
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-stream"
  }), " Fetch Selected"), "\xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    className: "btn btn-light-primary font-weight-bolder font-size-sm",
    onClick: productsUIProps.openUpdateProductsStatusDialog
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-sync-alt"
  }), " Update Status"))))));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-loading-dialog/ProductsLoadingDialog.js":
/*!*******************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/products-loading-dialog/ProductsLoadingDialog.js ***!
  \*******************************************************************************************************************/
/*! exports provided: ProductsLoadingDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsLoadingDialog", function() { return ProductsLoadingDialog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");



function ProductsLoadingDialog() {
  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      isLoading: state.products.listLoading
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      isLoading = _useSelector.isLoading;

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {}, [isLoading]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_2__["LoadingDialog"], {
    isLoading: isLoading,
    text: "Loading ..."
  });
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-table/ProductsTable.js":
/*!**************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/products-table/ProductsTable.js ***!
  \**************************************************************************************************/
/*! exports provided: ProductsTable */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsTable", function() { return ProductsTable; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap-table-next'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap-table2-paginator'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _redux_products_productsActions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../_redux/products/productsActions */ "./resources/demo2/src/app/modules/ECommerce/_redux/products/productsActions.js");
/* harmony import */ var _ProductsUIHelpers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../ProductsUIHelpers */ "./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsUIHelpers.js");
/* harmony import */ var _metronic_helpers__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../../_metronic/_helpers */ "./resources/demo2/src/_metronic/_helpers/index.js");
/* harmony import */ var _column_formatters__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./column-formatters */ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-table/column-formatters/index.js");
/* harmony import */ var _metronic_partials_controls__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../../_metronic/_partials/controls */ "./resources/demo2/src/_metronic/_partials/controls/index.js");
/* harmony import */ var _ProductsUIContext__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../ProductsUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsUIContext.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html










function ProductsTable() {
  // Products UI Context
  var productsUIContext = Object(_ProductsUIContext__WEBPACK_IMPORTED_MODULE_7__["useProductsUIContext"])();
  var productsUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      ids: productsUIContext.ids,
      setIds: productsUIContext.setIds,
      queryParams: productsUIContext.queryParams,
      setQueryParams: productsUIContext.setQueryParams,
      openEditProductPage: productsUIContext.openEditProductPage,
      openDeleteProductDialog: productsUIContext.openDeleteProductDialog
    };
  }, [productsUIContext]); // Getting curret state of products list from store (Redux)

  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      currentState: state.products
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      currentState = _useSelector.currentState;

  var totalCount = currentState.totalCount,
      entities = currentState.entities,
      listLoading = currentState.listLoading; // Products Redux state

  var dispatch = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())();
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    // clear selections list
    productsUIProps.setIds([]); // server call by queryParams

    dispatch(_redux_products_productsActions__WEBPACK_IMPORTED_MODULE_2__["fetchProducts"](productsUIProps.queryParams)); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [productsUIProps.queryParams, dispatch]); // Table columns

  var columns = [{
    dataField: "VINCode",
    text: "VIN Code (ID)",
    sort: true,
    sortCaret: _metronic_helpers__WEBPACK_IMPORTED_MODULE_4__["sortCaret"]
  }, {
    dataField: "manufacture",
    text: "Manufacture",
    sort: true,
    sortCaret: _metronic_helpers__WEBPACK_IMPORTED_MODULE_4__["sortCaret"]
  }, {
    dataField: "model",
    text: "Model",
    sort: true,
    sortCaret: _metronic_helpers__WEBPACK_IMPORTED_MODULE_4__["sortCaret"]
  }, {
    dataField: "modelYear",
    text: "Model Year",
    sort: true,
    sortCaret: _metronic_helpers__WEBPACK_IMPORTED_MODULE_4__["sortCaret"]
  }, {
    dataField: "color",
    text: "Color",
    sort: true,
    sortCaret: _metronic_helpers__WEBPACK_IMPORTED_MODULE_4__["sortCaret"],
    formatter: _column_formatters__WEBPACK_IMPORTED_MODULE_5__["ColorColumnFormatter"]
  }, {
    dataField: "price",
    text: "Price",
    sort: true,
    sortCaret: _metronic_helpers__WEBPACK_IMPORTED_MODULE_4__["sortCaret"],
    formatter: _column_formatters__WEBPACK_IMPORTED_MODULE_5__["PriceColumnFormatter"]
  }, {
    dataField: "status",
    text: "Status",
    sort: true,
    sortCaret: _metronic_helpers__WEBPACK_IMPORTED_MODULE_4__["sortCaret"],
    formatter: _column_formatters__WEBPACK_IMPORTED_MODULE_5__["StatusColumnFormatter"]
  }, {
    dataField: "condition",
    text: "Condition",
    sort: true,
    sortCaret: _metronic_helpers__WEBPACK_IMPORTED_MODULE_4__["sortCaret"],
    formatter: _column_formatters__WEBPACK_IMPORTED_MODULE_5__["ConditionColumnFormatter"]
  }, {
    dataField: "action",
    text: "Actions",
    formatter: _column_formatters__WEBPACK_IMPORTED_MODULE_5__["ActionsColumnFormatter"],
    formatExtraData: {
      openEditProductPage: productsUIProps.openEditProductPage,
      openDeleteProductDialog: productsUIProps.openDeleteProductDialog
    },
    classes: "text-right pr-0",
    headerClasses: "text-right pr-3",
    style: {
      minWidth: "100px"
    }
  }]; // Table pagination properties

  var paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: _ProductsUIHelpers__WEBPACK_IMPORTED_MODULE_3__["sizePerPageList"],
    sizePerPage: productsUIProps.queryParams.pageSize,
    page: productsUIProps.queryParams.pageNumber
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap-table2-paginator'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    pagination: !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap-table2-paginator'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(paginationOptions)
  }, function (_ref) {
    var paginationProps = _ref.paginationProps,
        paginationTableProps = _ref.paginationTableProps;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_partials_controls__WEBPACK_IMPORTED_MODULE_6__["Pagination"], {
      isLoading: listLoading,
      paginationProps: paginationProps
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap-table-next'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), _extends({
      wrapperClasses: "table-responsive",
      classes: "table table-head-custom table-vertical-center overflow-hidden",
      bootstrap4: true,
      bordered: false,
      remote: true,
      keyField: "id",
      data: entities === null ? [] : entities,
      columns: columns,
      defaultSorted: _ProductsUIHelpers__WEBPACK_IMPORTED_MODULE_3__["defaultSorted"],
      onTableChange: Object(_metronic_helpers__WEBPACK_IMPORTED_MODULE_4__["getHandlerTableChange"])(productsUIProps.setQueryParams),
      selectRow: Object(_metronic_helpers__WEBPACK_IMPORTED_MODULE_4__["getSelectRow"])({
        entities: entities,
        ids: productsUIProps.ids,
        setIds: productsUIProps.setIds
      })
    }, paginationTableProps), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_helpers__WEBPACK_IMPORTED_MODULE_4__["PleaseWaitMessage"], {
      entities: entities
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_metronic_helpers__WEBPACK_IMPORTED_MODULE_4__["NoRecordsFoundMessage"], {
      entities: entities
    })));
  }));
}

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-table/column-formatters/ActionsColumnFormatter.js":
/*!*****************************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/products-table/column-formatters/ActionsColumnFormatter.js ***!
  \*****************************************************************************************************************************/
/*! exports provided: ActionsColumnFormatter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionsColumnFormatter", function() { return ActionsColumnFormatter; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-inlinesvg'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _metronic_helpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../_metronic/_helpers */ "./resources/demo2/src/_metronic/_helpers/index.js");
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */




var ActionsColumnFormatter = function ActionsColumnFormatter(cellContent, row, rowIndex, _ref) {
  var openEditProductPage = _ref.openEditProductPage,
      openDeleteProductDialog = _ref.openDeleteProductDialog;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    overlay: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
      id: "products-edit-tooltip"
    }, "Edit product")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: "btn btn-icon btn-light btn-hover-primary btn-sm mx-3",
    onClick: function onClick() {
      return openEditProductPage(row.id);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "svg-icon svg-icon-md svg-icon-primary"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-inlinesvg'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    src: Object(_metronic_helpers__WEBPACK_IMPORTED_MODULE_2__["toAbsoluteUrl"])("/media/svg/icons/Communication/Write.svg")
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    overlay: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
      id: "products-delete-tooltip"
    }, "Delete product")
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: "btn btn-icon btn-light btn-hover-danger btn-sm",
    onClick: function onClick() {
      return openDeleteProductDialog(row.id);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "svg-icon svg-icon-md svg-icon-danger"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-inlinesvg'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    src: Object(_metronic_helpers__WEBPACK_IMPORTED_MODULE_2__["toAbsoluteUrl"])("/media/svg/icons/General/Trash.svg")
  })))));
};

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-table/column-formatters/ColorColumnFormatter.js":
/*!***************************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/products-table/column-formatters/ColorColumnFormatter.js ***!
  \***************************************************************************************************************************/
/*! exports provided: ColorColumnFormatter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ColorColumnFormatter", function() { return ColorColumnFormatter; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

var ColorColumnFormatter = function ColorColumnFormatter(cellContent, row) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    style: {
      color: row.color
    }
  }, row.color);
};

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-table/column-formatters/ConditionColumnFormatter.js":
/*!*******************************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/products-table/column-formatters/ConditionColumnFormatter.js ***!
  \*******************************************************************************************************************************/
/*! exports provided: ConditionColumnFormatter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConditionColumnFormatter", function() { return ConditionColumnFormatter; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ProductsUIHelpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../ProductsUIHelpers */ "./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsUIHelpers.js");


var ConditionColumnFormatter = function ConditionColumnFormatter(cellContent, row) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "badge badge-".concat(_ProductsUIHelpers__WEBPACK_IMPORTED_MODULE_1__["ProductConditionCssClasses"][row.condition], " badge-dot")
  }), "\xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "font-bold font-".concat(_ProductsUIHelpers__WEBPACK_IMPORTED_MODULE_1__["ProductConditionCssClasses"][row.condition])
  }, _ProductsUIHelpers__WEBPACK_IMPORTED_MODULE_1__["ProductConditionTitles"][row.condition]));
};

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-table/column-formatters/PriceColumnFormatter.js":
/*!***************************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/products-table/column-formatters/PriceColumnFormatter.js ***!
  \***************************************************************************************************************************/
/*! exports provided: PriceColumnFormatter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PriceColumnFormatter", function() { return PriceColumnFormatter; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

var PriceColumnFormatter = function PriceColumnFormatter(cellContent, row) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, "$", row.price);
};

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-table/column-formatters/StatusColumnFormatter.js":
/*!****************************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/products-table/column-formatters/StatusColumnFormatter.js ***!
  \****************************************************************************************************************************/
/*! exports provided: StatusColumnFormatter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatusColumnFormatter", function() { return StatusColumnFormatter; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ProductsUIHelpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../ProductsUIHelpers */ "./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsUIHelpers.js");


var StatusColumnFormatter = function StatusColumnFormatter(cellContent, row) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "label label-lg label-light-".concat(_ProductsUIHelpers__WEBPACK_IMPORTED_MODULE_1__["ProductStatusCssClasses"][row.status], " label-inline")
  }, _ProductsUIHelpers__WEBPACK_IMPORTED_MODULE_1__["ProductStatusTitles"][row.status]);
};

/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-table/column-formatters/index.js":
/*!************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/products-table/column-formatters/index.js ***!
  \************************************************************************************************************/
/*! exports provided: StatusColumnFormatter, ConditionColumnFormatter, ColorColumnFormatter, PriceColumnFormatter, ActionsColumnFormatter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _StatusColumnFormatter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./StatusColumnFormatter */ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-table/column-formatters/StatusColumnFormatter.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "StatusColumnFormatter", function() { return _StatusColumnFormatter__WEBPACK_IMPORTED_MODULE_0__["StatusColumnFormatter"]; });

/* harmony import */ var _ConditionColumnFormatter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ConditionColumnFormatter */ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-table/column-formatters/ConditionColumnFormatter.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ConditionColumnFormatter", function() { return _ConditionColumnFormatter__WEBPACK_IMPORTED_MODULE_1__["ConditionColumnFormatter"]; });

/* harmony import */ var _ColorColumnFormatter__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ColorColumnFormatter */ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-table/column-formatters/ColorColumnFormatter.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ColorColumnFormatter", function() { return _ColorColumnFormatter__WEBPACK_IMPORTED_MODULE_2__["ColorColumnFormatter"]; });

/* harmony import */ var _PriceColumnFormatter__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./PriceColumnFormatter */ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-table/column-formatters/PriceColumnFormatter.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PriceColumnFormatter", function() { return _PriceColumnFormatter__WEBPACK_IMPORTED_MODULE_3__["PriceColumnFormatter"]; });

/* harmony import */ var _ActionsColumnFormatter__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./ActionsColumnFormatter */ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-table/column-formatters/ActionsColumnFormatter.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ActionsColumnFormatter", function() { return _ActionsColumnFormatter__WEBPACK_IMPORTED_MODULE_4__["ActionsColumnFormatter"]; });

// TODO: Rename all formatters






/***/ }),

/***/ "./resources/demo2/src/app/modules/ECommerce/pages/products/products-update-status-dialog/ProductsUpdateStatusDialog.js":
/*!******************************************************************************************************************************!*\
  !*** ./resources/demo2/src/app/modules/ECommerce/pages/products/products-update-status-dialog/ProductsUpdateStatusDialog.js ***!
  \******************************************************************************************************************************/
/*! exports provided: ProductsUpdateStatusDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsUpdateStatusDialog", function() { return ProductsUpdateStatusDialog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _ProductsUIHelpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../ProductsUIHelpers */ "./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsUIHelpers.js");
/* harmony import */ var _redux_products_productsActions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../_redux/products/productsActions */ "./resources/demo2/src/app/modules/ECommerce/_redux/products/productsActions.js");
/* harmony import */ var _ProductsUIContext__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../ProductsUIContext */ "./resources/demo2/src/app/modules/ECommerce/pages/products/ProductsUIContext.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }








var selectedProducts = function selectedProducts(entities, ids) {
  var _products = [];
  ids.forEach(function (id) {
    var product = entities.find(function (el) {
      return el.id === id;
    });

    if (product) {
      _products.push(product);
    }
  });
  return _products;
};

function ProductsUpdateStatusDialog(_ref) {
  var show = _ref.show,
      onHide = _ref.onHide;
  // Products UI Context
  var productsUIContext = Object(_ProductsUIContext__WEBPACK_IMPORTED_MODULE_4__["useProductsUIContext"])();
  var productsUIProps = Object(react__WEBPACK_IMPORTED_MODULE_0__["useMemo"])(function () {
    return {
      ids: productsUIContext.ids,
      setIds: productsUIContext.setIds,
      queryParams: productsUIContext.queryParams
    };
  }, [productsUIContext]); // Products Redux state

  var _useSelector = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())(function (state) {
    return {
      products: selectedProducts(state.products.entities, productsUIProps.ids),
      isLoading: state.products.actionsLoading
    };
  }, !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())),
      products = _useSelector.products,
      isLoading = _useSelector.isLoading; // if there weren't selected products we should close modal


  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (productsUIProps.ids || productsUIProps.ids.length === 0) {
      onHide();
    } // eslint-disable-next-line react-hooks/exhaustive-deps

  }, [productsUIProps.ids]);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(0),
      _useState2 = _slicedToArray(_useState, 2),
      status = _useState2[0],
      setStatus = _useState2[1];

  var dispatch = !(function webpackMissingModule() { var e = new Error("Cannot find module 'react-redux'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())();

  var updateStatus = function updateStatus() {
    // server request for updateing product by ids
    dispatch(_redux_products_productsActions__WEBPACK_IMPORTED_MODULE_3__["updateProductsStatus"](productsUIProps.ids, status)).then(function () {
      // refresh list after deletion
      dispatch(_redux_products_productsActions__WEBPACK_IMPORTED_MODULE_3__["fetchProducts"](productsUIProps.queryParams)).then(function () {
        // clear selections list
        productsUIProps.setIds([]); // closing delete modal

        onHide();
      });
    });
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    show: show,
    onHide: onHide,
    "aria-labelledby": "example-modal-sizes-title-lg"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Header, {
    closeButton: true
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Title, {
    id: "example-modal-sizes-title-lg"
  }, "Status has been updated for selected products")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Body, {
    className: "overlay overlay-block cursor-default"
  }, isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "overlay-layer bg-transparent"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "spinner spinner-lg spinner-warning"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "list-timeline list-timeline-skin-light padding-30"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "list-timeline-items"
  }, products.map(function (product) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "list-timeline-item mb-3",
      key: product.id
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "list-timeline-text"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "label label-lg label-light-".concat(_ProductsUIHelpers__WEBPACK_IMPORTED_MODULE_2__["ProductStatusCssClasses"][product.status], " label-inline"),
      style: {
        width: "60px"
      }
    }, "ID: ", product.id), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "ml-5"
    }, product.manufacture, ", ", product.model)));
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(!(function webpackMissingModule() { var e = new Error("Cannot find module 'react-bootstrap'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()).Footer, {
    className: "form"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form-group"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("select", {
    className: "form-control ".concat(_ProductsUIHelpers__WEBPACK_IMPORTED_MODULE_2__["ProductStatusCssClasses"][status]),
    value: status,
    onChange: function onChange(e) {
      return setStatus(+e.target.value);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
    value: "0"
  }, "Selling"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
    value: "1"
  }, "Sold"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form-group"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: onHide,
    className: "btn btn-light btn-elevate"
  }, "Cancel"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    onClick: updateStatus,
    className: "btn btn-primary btn-elevate"
  }, "Update Status"))));
}

/***/ })

}]);