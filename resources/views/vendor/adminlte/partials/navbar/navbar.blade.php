<nav class="main-header navbar
    {{ config('adminlte.classes_topnav_nav', 'navbar-expand') }}
    {{ config('adminlte.classes_topnav', 'navbar-white navbar-light') }}">

    {{-- Navbar left links --}}
    <ul class="navbar-nav">
        {{-- Left sidebar toggler link --}}
        @include('adminlte::partials.navbar.menu-item-left-sidebar-toggler')

        {{-- Configured left links --}}
        @each('adminlte::partials.navbar.menu-item', $adminlte->menu('navbar-left'), 'item')

        {{-- Custom left links --}}
        @yield('content_top_nav_left')
    </ul>

    {{-- Navbar right links --}}
    <ul class="navbar-nav ml-auto">
        {{-- Custom right links --}}
        @yield('content_top_nav_right')

        {{-- Configured right links --}}
        @each('adminlte::partials.navbar.menu-item', $adminlte->menu('navbar-right'), 'item')

        {{-- User menu link --}}
        @if(Auth::user())
            @if(!Auth::user()->isAdmin() && Auth::user()->countItems() > 0)
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="fa fa-shopping-cart"></i>
                <span class="badge badge-success navbar-badge">
                    {{ Auth::user()->countItems() }}
                </span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <span class="dropdown-item dropdown-header">Carrito</span>
                    @foreach (Auth::user()->items() as $item)
                        <span class="dropdown-item" style="white-space: normal; font-variant: all-small-caps;">
                            <i class="fas fa-list mr-2"></i> 
                            {{$item->codigo}} - {{$item->nombre}} (Cant: {{$item->cantidad}})
                            <a class="ml-3 float-right text-muted text-sm" href="{{ route('productos.deletecarrito',$item->id_item_carrito) }}" title="Eliminar">
                                <i class="fas fa-trash mr-2"></i>
                            </a>
                        </span>
                    @endforeach
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('productos.carrito') }}" class="dropdown-item dropdown-footer" >Ver carrito</a>
                </div>
            </li>
            @endif
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="far fa-bell"></i>
                    @if(count(Auth::user()->unreadNotifications))
                    <span class="badge badge-warning navbar-badge">
                        {{ count(Auth::user()->unreadNotifications) }}
                    </span>
                    @endif
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <span class="dropdown-item dropdown-header">Notificaciones sin leer</span>
                    @forelse (Auth::user()->unreadNotifications as $notificacion)
                        <a href="/marcarleida/{{$notificacion->id}}" class="dropdown-item" style="background-color: lightgray;white-space: normal; font-variant: all-small-caps;">
                            <i class="fas fa-bell mr-2"></i> 
                            Stock bajo: {{$notificacion->data['codigo']}} - {{$notificacion->data['nombre']}}
                            <span class="float-right text-muted text-sm">{{$notificacion->created_at->diffForHumans()}}</span>
                        </a>
                        @empty
                        <span class="ml-3 pull-right text-muted text-sm">Sin notificaciones por leer</span>
                    @endforelse
                    <div class="dropdown-divider"></div>
                    <span class="dropdown-item dropdown-header">Notificaciones leidas</span>
                    @forelse (Auth::user()->readNotifications as $notificacion)
                        <a href="#" class="dropdown-item" style="white-space: normal; font-variant: all-small-caps;">
                            <i class="fas fa-bell mr-2"></i> 
                            Stock bajo: {{$notificacion->data['codigo']}} - {{$notificacion->data['nombre']}}
                            <span class="ml-3 float-right text-muted text-sm">{{$notificacion->created_at->diffForHumans()}}</span>
                        </a>
                    @empty
                    <span class="ml-3 pull-right text-muted text-sm">Sin notificaciones leidas</span>
                    @endforelse
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('notificacion.marcarleidas') }}" class="dropdown-item dropdown-footer" >Marcar todas como leidas</a>
                </div>
            </li>

            @if(config('adminlte.usermenu_enabled'))
                @include('adminlte::partials.navbar.menu-item-dropdown-user-menu')
            @else
                @include('adminlte::partials.navbar.menu-item-logout-link')
            @endif
        @else
            @if(env('CREAR_CUENTA_USUARIO', false))
            <li class="nav-item">
                <a class="nav-link" href="/register">Crear cuenta</a>
            </li>
            @endif
            <li class="nav-item">
                <a class="nav-link" href="/login">Ingresar</a>
            </li>
        @endif

        {{-- Right sidebar toggler link --}}
        @if(config('adminlte.right_sidebar'))
            @include('adminlte::partials.navbar.menu-item-right-sidebar-toggler')
        @endif
    </ul>

</nav>
