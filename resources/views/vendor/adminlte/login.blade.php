@extends('adminlte::auth.auth-page', ['auth_type' => 'login'])



@php( $login_url = View::getSection('login_url') ?? config('adminlte.login_url', 'login') )
@php( $register_url = View::getSection('register_url') ?? config('adminlte.register_url', 'register') )
@php( $password_reset_url = View::getSection('password_reset_url') ?? config('adminlte.password_reset_url', 'password/reset') )

@if (config('adminlte.use_route_url', false))
    @php( $login_url = $login_url ? route($login_url) : '' )
    @php( $register_url = $register_url ? route($register_url) : '' )
    @php( $password_reset_url = $password_reset_url ? route($password_reset_url) : '' )
@else
    @php( $login_url = $login_url ? url($login_url) : '' )
    @php( $register_url = $register_url ? url($register_url) : '' )
    @php( $password_reset_url = $password_reset_url ? url($password_reset_url) : '' )
@endif

@section('auth_header', 'Ingrese sus datos para iniciar sesión')

@section('auth_body')
@if((env('APP_ENV') == 'production'))
    {!! RecaptchaV3::initJs() !!}
@endif  
    <form action="{{ $login_url }}" method="post">
        {{ csrf_field() }}
        @if((env('APP_ENV') == 'production'))
            {!! RecaptchaV3::field('contacto') !!}
        @endif
        @if($message = session('message'))
            <div class="alert alert-{{ session('type') }}"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>{!! $message !!}</div>
        @endif
        {{-- Email field --}}
        <div class="input-group mb-3">
            <input type="text" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                   value="{{ old('email') }}" placeholder="Email o Usuario" autofocus>
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-envelope {{ config('adminlte.classes_auth_icon', '') }}"></span>
                </div>
            </div>
            @if($errors->has('email'))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </div>
            @endif
        </div>

        {{-- Password field --}}
        <div class="input-group mb-3">
            <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}"
                   placeholder="Contraseña">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-lock {{ config('adminlte.classes_auth_icon', '') }}"></span>
                </div>
            </div>
            @if($errors->has('password'))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('password') }}</strong>
                </div>
            @endif
        </div>

        {{-- Login field --}}
        <div class="row">
            <div class="col-7">
                <!-- <div class="icheck-primary">
                    <input type="checkbox" name="remember" id="remember">
                    <label for="remember">Recuerdame</label>
                </div> -->
            </div>
            <div class="col-5">
                <button type=submit class="btn btn-block {{ config('adminlte.classes_auth_btn', 'btn-flat btn-primary') }}">
                    <span class="fas fa-sign-in-alt"></span>
                    Ingresar
                </button>
            </div>
        </div>

    </form>
@stop

@section('auth_footer')
    {{-- Password reset link --}}
    @if($password_reset_url)
        <p class="my-0">
            <a href="{{ $password_reset_url }}">
                Olvidó su contraseña?
            </a>
        </p>
    @endif

    {{-- Register link --}}
    @if($register_url && env('CREAR_CUENTA_USUARIO', false))
        <p class="my-0">
            <a href="{{ $register_url }}">
                Crear cuenta
            </a>
        </p>
    @endif 
@stop
