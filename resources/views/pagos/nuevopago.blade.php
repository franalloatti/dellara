@extends('adminlte::page')

@section('content')
  {{-- <head>

  <!-- Meta -->
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">


  <!-- CSS -->
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

  <!-- Script -->
  <script src="/js/jquery-3.5.1.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>

  </head> --}}
  <body>
    <script type="text/javascript">
      $(document).ready(function(){
        $('.js-example-basic-single').select2({matcher: matcher});
      });
    </script>
    <style>
    .select2-container .select2-selection--single{
      height: auto !important;
    }
    .select2-container {
      width: 100% !important;
    }
    </style>
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- jquery validation -->
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">Pago <small>Crear</small></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" id="quickForm" method="post" action="{{ route('pagos.addpago') }}" enctype="multipart/form-data">
              @csrf
              <div class="card-body">
                <div class="row">
                  <div class="form-group col-sm-6">
                    <label for="medio_id_medio">Medio de pago</label>
                    <select name="medio_id_medio" class="form-control" id="medio_id_medio" required>
                      <option value="">Seleccionar</option>
                      @foreach($medios as $medio)
                      <option value="{{$medio->id_medio}}">{{$medio->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group col-sm-6">
                    <label for="monto">Monto</label>
                    <input type="number" step="0.01" name="monto" class="form-control" id="monto" placeholder="Ingresar monto">
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-sm-6">
                    <label for="descripcion">Descripcion</label>
                    <textarea name="descripcion" class="form-control" id="descripcion" placeholder="Ingresar descripcion"></textarea>
                  </div>
                  <div class="form-group col-sm-6">
                    <label for="comprobante">Comprobante</label><br>
                    <input type="file" name="comprobante" class="" id="comprobante">
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-success">Crear</button>
              </div>
            </form>
          </div>
          <!-- /.card -->
          </div>
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-6">

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </body>
</html>
@stop
