@extends('adminlte::page')

@section('content')
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Mis pagos</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(!count($pagos))
                <h3>NO TIENES PAGOS REALIZADOS</h3>
                @else
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th>Fecha</th>
                      <th>Descripción</th>
                      <th>Monto</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($pagos as $pago)
                    <tr>
                      <td>{{$pago->created_at}}</td>
                      <td>{{$pago->descripcion}}</td>
                      <td>$ {{number_format($pago->monto,2,",",".")}}</td>
                      <td>
                      @if($pago->comprobante != '')
                          <a class="btn btn-sm btn-info" target="_blank" href="/images/comprobantes/{{$pago->comprobante}}"><i class="fa fa-file"></i>&nbsp;&nbsp;&nbsp;Ver comprobante</a>&nbsp;&nbsp;&nbsp;
                        @endif
                        </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                @endif
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                    {{ $pagos->links() }}
              </div>
            </div>
            <!-- /.card -->
            </div>
          </div>
        </div>
        @stop
