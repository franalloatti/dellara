<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Reporte ventas desde <?php date('d/m/Y', strtotime($fecha_desde));?> hasta <?php date('d/m/Y', strtotime($fecha_hasta));?></title>

        <style>
            #table2 {
              border-collapse: collapse;
              border-spacing: 10px;  
              font-size: 10px;  
              border: 1px solid black;
            }
            table, th, td {
                border: 1px solid black;
            }
            h3 {
                font-size: 20px;
            }
        </style>
    </head>
    <body>
        <table id="table2">
            <thead>                  
                <tr>
                    <th>Fecha</th>
                    <th>Tipo de movimiento</th>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Cuenta</th>
                    <th>Monto</th>
                </tr>
            </thead>
            <tbody>
                @foreach($movimientos as $movimiento)
                <tr>
                    <td>{{date('d/m/Y', strtotime($movimiento->fecha != '' ? $movimiento->fecha : $movimiento->created_at))}}</td>
                    <td>{{$movimiento->tipo}}</td>
                    <td>{{($movimiento->tipo_movimiento == 'Compra productos' || $movimiento->tipo_movimiento == 'Venta') ? $movimiento->name : $movimiento->nombre}}</td>
                    <td>{{($movimiento->tipo_movimiento == 'Compra productos' || $movimiento->tipo_movimiento == 'Venta') ? $movimiento->id_pago : $movimiento->descripcion}}</td>
                    <td>{{$movimiento->cuenta}}</td>
                    <td>{{$movimiento->monto}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <br>
    </body>
</html>