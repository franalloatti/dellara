@extends('adminlte::page')

@section('content')
  <script>
    function cambiarSeleccion(){
      var tipo_movimiento_id = $("#tipo_mov_id option:selected").attr('tipo');
      if(tipo_movimiento_id == ''){
        document.getElementById("div_movimiento").hidden = true;
        document.getElementById("div_nombre").hidden = true;
      }else{
        document.getElementById("div_movimiento").hidden = false;
        document.getElementById("div_nombre").hidden = false;
      }
      if(tipo_movimiento_id == 'Transferencia'){
        document.getElementById("div_cuenta_destino").hidden = false;
        document.getElementById("cuenta_destino_id").required = true;
        document.getElementById("lbl_cuenta_origen").innerHTML = 'Cuenta origen';
      }else{
        document.getElementById("div_cuenta_destino").hidden = true;
        document.getElementById("cuenta_destino_id").required = false;
        document.getElementById("lbl_cuenta_origen").innerHTML = 'Cuenta';
      }
    }
    function cargarDatosMovimiento(pago){
      console.log(pago);
      $('#id_movimiento').val(pago.id_pago);
      $('#text_tipo_movimiento').html(pago.tipo_movimiento);
      $('#monto_edit').val(-1*pago.monto);
      $('#descripcion_edit').val(pago.descripcion);
      $('#nombre_edit').val(pago.nombre);
      $('#fecha_edit').val(pago.fecha);
      $('#cuenta_origen_id_edit').val(pago.medio_id_medio);
    }
  </script>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Movimientos</h3>
            <div class="card-tools">
              <form role="form" action="{{ route('movimientos.index') }}" method="POST">
                @csrf
                <div class="input-group input-group-sm">
                  <a class="btn btn-sm btn-success" data-toggle="modal" data-target="#addmovimiento"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Nuevo</a>&nbsp;&nbsp;&nbsp;
                  <input type="date" id="fecha_desde" title="Fecha desde" name="fecha_desde" value="{{$fecha_desde ? $fecha_desde : ''}}" class="form-control pull-right" placeholder="Buscar">
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="date" id="fecha_hasta" title="Fecha hasta" name="fecha_hasta" value="{{$fecha_hasta ? $fecha_hasta : ''}}" class="form-control pull-right" placeholder="Buscar">
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <select name="tipo_movimiento_id" class="form-control" id="tipo_movimiento_id">
                    <option value="">Tipo movimiento</option>
                    @foreach($tipo_movimientos as $tipo_mov)
                    @if($tipo_mov->id_tipo_movimiento == $tipoMovSelected)
                        <option value="{{$tipo_mov->id_tipo_movimiento}}" selected>{{$tipo_mov->nombre}}</option>
                    @else
                        <option value="{{$tipo_mov->id_tipo_movimiento}}">{{$tipo_mov->nombre}}</option>
                    @endif
                    @endforeach
                  </select>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <select name="cuenta_id" class="form-control" id="cuenta_id">
                    <option value="">Cuenta</option>
                    @foreach($cuentas as $cuenta)
                    @if($cuenta->id_medio == $cuentaSelected)
                        <option value="{{$cuenta->id_medio}}" selected>{{$cuenta->nombre}}</option>
                    @else
                        <option value="{{$cuenta->id_medio}}">{{$cuenta->nombre}}</option>
                    @endif
                    @endforeach
                  </select>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="text" id="search" name="search" value="{{$search ? $search : ''}}" class="form-control pull-right" placeholder="Buscar">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered">
              <thead>                  
                <tr>
                  <th>Tipo movimiento</th>
                  <th>Nombre</th>
                  <th>Descripción</th>
                  <th>Cuenta</th>
                  <th>Monto</th>
                  <th>Fecha</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>
                @foreach($pagos as $pago)
                <tr>
                  <td>{{$pago->tipo}}</td>
                  <td>{{$pago->nombre}}</td>
                  <td>{{$pago->descripcion}}</td>
                  <td>{{$pago->cuenta}}</td>
                  <td align="right">$ {{number_format($pago->monto,2,",",".")}}</td>
                  <td>{{date('d/m/Y', strtotime($pago->fecha != '' ? $pago->fecha : $pago->created_at))}}</td>
                  <td>
                  @if($pago->tipo <> 'Transferencia' && $pago->consolidado <> 'S')
                    <a data-toggle="modal" data-target="#editmovimiento" class="btn btn-sm btn-warning" onclick="cargarDatosMovimiento({{json_encode($pago)}})">
                      <i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Editar
                    </a>&nbsp;&nbsp;&nbsp;
                    <a href="{{route('movimientos.borrar', $pago->id_pago)}}" class="btn btn-sm btn-danger" onclick="return confirmar('borrar el movimiento',this,event);">
                      <i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Borrar
                    </a>&nbsp;&nbsp;&nbsp;
                  @endif
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
          <div class="card-footer clearfix">
                {{ $pagos->appends(['fecha_desde' => $fecha_desde, 'search' => $search,
                'fecha_hasta' => $fecha_hasta, 'tipo_movimiento_id' => $tipoMovSelected, 'cuenta_id' => $cuentaSelected])->links() }}
          </div>
        </div>
        <!-- /.card -->
        </div>
      </div>
    </div>
    <!-- Modal agregar movimiento-->
    <div class="modal fade" id="addmovimiento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <form role="form" id="quickForm" method="post" action="{{ route('movimientos.guardar') }}">
        @csrf
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Agregar movimiento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="form-group col-sm-6">
                  <label for="tipo_mov_id">Tipo movimiento</label>
                  <select name="tipo_mov_id" class="form-control" id="tipo_mov_id" onchange="cambiarSeleccion()" required>
                    <option tipo="" value="">Seleccione</option>
                    @foreach($tipo_movimientos as $tipo_mov)
                      <option tipo="{{$tipo_mov->tipo}}" value="{{$tipo_mov->id_tipo_movimiento}}">{{$tipo_mov->nombre}}</option>
                    @endforeach
                  </select>
                </div>
                <div id="div_nombre" hidden class="form-group col-sm-6">
                  <label for="nombre">Nombre</label>
                  <input type="text" name="nombre" class="form-control" id="nombre" required>
                </div>
              </div>
              <div id="div_movimiento" hidden>
                <div class="row">
                  <div class="form-group col-sm-6">
                    <label for="cuenta_origen_id" id="lbl_cuenta_origen">Cuenta origen</label>
                    <select name="cuenta_origen_id" class="form-control" id="cuenta_origen_id" required>
                      <option fecha_arqueo='1970-01-01' value="">Seleccione</option>
                      @foreach($cuentas as $cuenta)
                        <option fecha_arqueo="{{$cuenta->fecha_max ? $cuenta->fecha_max : '1970-01-01'}}" value="{{$cuenta->id_medio}}">{{$cuenta->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div id="div_cuenta_destino" class="form-group col-sm-6">
                    <label for="cuenta_destino_id">Cuenta destino</label>
                    <select name="cuenta_destino_id" class="form-control" id="cuenta_destino_id" required>
                      <option fecha_arqueo='1970-01-01' value="">Seleccione</option>
                      @foreach($cuentas as $cuenta)
                        <option fecha_arqueo="{{$cuenta->fecha_max ? $cuenta->fecha_max : '1970-01-01'}}" value="{{$cuenta->id_medio}}">{{$cuenta->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-sm-6">
                    <label for="monto">Monto</label>
                    <input type="number" step="0.01" min="0" name="monto" class="form-control" id="monto" required>
                  </div>
                  <div class="form-group col-sm-6">
                    <label for="fecha">Fecha</label>
                    <?php 
                    $fecha_min = date("Y-m-d",strtotime(date("Y-m-d")."- ".env('MES_RESTAR_MOVIMIENTO', 1)." month"));
                    ?>
                    <input type="date" min="<?php echo $fecha_min;?>" name="fecha" class="form-control" id="fecha" required>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-sm-12">
                    <label for="descripcion">Descripción</label>
                    <textarea name="descripcion" class="form-control" id="descripcion"></textarea>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-sm btn-success">Aceptar</button>
            </div>
          </div>
        </div>
      </form>
    </div>
    <!-- Modal editar movimiento-->
    <div class="modal fade" id="editmovimiento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <form role="form" id="quickForm" method="post" action="{{ route('movimientos.actualizar') }}">
        @csrf
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Editar movimiento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <input type="hidden" name="id_movimiento" class="form-control" id="id_movimiento" required>
              <div class="row">
                <div class="form-group col-sm-6">
                  <label for="text_tipo_movimiento">Tipo movimiento</label>
                  <p id="text_tipo_movimiento" name="text_tipo_movimiento"></p>
                </div>
                <div id="div_nombre_edit" class="form-group col-sm-6">
                  <label for="nombre_edit">Nombre</label>
                  <input type="text" name="nombre_edit" class="form-control" id="nombre_edit" required>
                </div>
              </div>
              <div id="div_movimiento_edit">
                <div class="row">
                  <div class="form-group col-sm-6">
                    <label for="cuenta_origen_id_edit" id="lbl_cuenta_origen_edit">Cuenta</label>
                    <select name="cuenta_origen_id_edit" class="form-control" id="cuenta_origen_id_edit" required>
                      <option fecha_arqueo='1970-01-01' value="">Seleccione</option>
                      @foreach($cuentas as $cuenta)
                        <option fecha_arqueo="{{$cuenta->fecha_max ? $cuenta->fecha_max : '1970-01-01'}}" value="{{$cuenta->id_medio}}">{{$cuenta->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-sm-6">
                    <label for="monto_edit">Monto</label>
                    <input type="number" step="0.01" min="0" name="monto_edit" class="form-control" id="monto_edit" required>
                  </div>
                  <div class="form-group col-sm-6">
                    <label for="fecha">Fecha</label>
                    <?php 
                    $fecha_min = date("Y-m-d",strtotime(date("Y-m-d")."- ".env('MES_RESTAR_MOVIMIENTO', 1)." month"));
                    ?>
                    <input type="date" min="<?php echo $fecha_min;?>" name="fecha_edit" class="form-control" id="fecha_edit" required>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-sm-12">
                    <label for="descripcion_edit">Descripción</label>
                    <textarea name="descripcion_edit" class="form-control" id="descripcion_edit"></textarea>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-sm btn-success">Aceptar</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
@stop
