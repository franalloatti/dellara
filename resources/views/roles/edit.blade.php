@extends('adminlte::page')

@section('content')
<div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Rol <small>Editar</small></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="quickForm" method="post" action="{{ route('roles.actualizar', [$role->id_role]) }}">
                @csrf
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="nombre">Nombre</label>
                      <input type="text" name="nombre" class="form-control" id="nombre" value="{{$role->name}}" placeholder="Ingresar nombre" required>
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="discount">Descuento</label>
                      <input type="number" step="0.01" name="discount" min="0" max="100" class="form-control" id="discount" value="{{$role->discount}}" placeholder="Ingresar descuento en %" required>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Editar</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
@stop
