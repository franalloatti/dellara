@extends('adminlte::page')

@section('content')
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Marcas</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th>Marca</th>
                      <th>Descripcion</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($marcas as $marca)
                    <tr>
                      <td>{{$marca->nombre}}</td>
                      <td>{{$marca->descripcion}}</td>
                      <td>
                        <a href="{{ route('marcas.editar', [$marca->id_marca]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Editar</a>&nbsp;&nbsp;&nbsp;
                        <a href="{{ route('marcas.borrar', [$marca->id_marca]) }}" class="btn btn-sm btn-danger" onclick="return confirmar('borrar la marca',this,event);"><i class="fa fa-trash-alt "></i>&nbsp;&nbsp;&nbsp;Borrar</a>&nbsp;&nbsp;&nbsp;
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                    {{ $marcas->links() }}
              </div>
            </div>
            <!-- /.card -->
            </div>
          </div>
        </div>
        @stop
