@extends('adminlte::page')

@section('content')
    <script>
        function cargarDatosDireccion(direccion){
            console.log(direccion);
            $('#id_direccion').val(direccion.id_direccion);
            $('#direccion').val(direccion.direccion);
            $('#observaciones').val(direccion.observaciones);
            $('#provincia').val(direccion.provincia);
            $('#localidad').val(direccion.localidad);
            $('#codigo_postal').val(direccion.codigo_postal);
            $('#telefono').val(direccion.telefono);
        }
    </script>

@if($errors->any())
    <div class="alert alert-secondary alert-dismissible fade show" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Proveedor</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="float-right">
                            <a href="#" class="btn btn-sm btn-info"><i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Reiniciar contraseña</a>&nbsp;&nbsp;&nbsp;
                            <a href="{{ route('proveedores.editar', [$user->id_user]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Editar</a>&nbsp;&nbsp;&nbsp;
                            @if(Auth::user()->hasPermiso('Facturas', 'crear'))
                            <div class="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button" class="btn btn-sm btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-map-pin"></i>&nbsp;&nbsp;&nbsp;Agregar direccion&nbsp;&nbsp;&nbsp;
                                </button>
                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                  <a class="dropdown-item" href="{{ route('direcciones.crearAutomaticamente', [$user->id_user]) }}">Traer datos</a>
                                  <a class="dropdown-item" href="{{ route('direcciones.crear', [$user->id_user]) }}">Manual</a>
                                </div>
                            </div>&nbsp;&nbsp;&nbsp;
                            <a href="#" class="btn btn-sm btn-secondary" {{$user->role == 'consumidor final' ? 'hidden' : ''}}><i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Ver cuenta corriente</a>&nbsp;&nbsp;&nbsp;
                            @else
                            <a href="{{ route('direcciones.crear', [$user->id_user]) }}" class="btn btn-sm btn-success"><i class="fa fa-map-pin"></i>&nbsp;&nbsp;&nbsp;Agregar direccion</a>&nbsp;&nbsp;&nbsp;
                            @endif 
                            <a href="{{ route('proveedores.cuentaampliado', [$user->id_user]) }}" class="btn btn-sm btn-secondary" {{$user->role == 'consumidor final' ? 'hidden' : ''}}><i class="fa fa-book "></i>&nbsp;&nbsp;&nbsp;Ver cuenta corriente</a>&nbsp;&nbsp;&nbsp;
                        </div>
                        <br><br>
                        <form class="form-horizontal">
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Tipo de proveedor</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$user->role}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">{{$user->role == 'Consumidor final' ? "DNI" : "CUIL/CUIT"}}</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$user->role == 'Consumidor final' ? $user->documento : $user->cuit}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">{{$user->role == 'Consumidor final' ? "Nombre" : "Nombre AFIP"}}</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$user->name}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">{{$user->role == 'Consumidor final' ? "Apellido" : "Razón social"}}</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$user->surname}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row" {{$user->role == 'Consumidor final' ? "hidden" : ""}}>
                                        <div class="col-sm-3">
                                            <label for="inputName">Nombre fantasia</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$user->nombre_fantasia}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row" {{$user->role == 'Consumidor final' ? "hidden" : ""}}>
                                        <div class="col-sm-3">
                                            <label for="inputName">Número IIBB</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$user->numero_iibb}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Email</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$user->email}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Nombre de usuario</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$user->username}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Teléfono fijo</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$user->telefono}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Celular</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$user->telefono_movil}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Direcciones</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <table class="table table-bordered">
                                        <thead>                  
                                            <tr>
                                            <th>Direccion</th>
                                            <th>Provincia</th>
                                            <th>Localidad</th>
                                            <th>Codigo postal</th>
                                            <th>Teléfono</th>
                                            <th>Estado</th>
                                            <th>Dir. facturación</th>
                                            <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($direcciones as $direccion)
                                            <tr>
                                            <td>{{$direccion->direccion}}</td>
                                            <td>{{$direccion->provincia}}</td>
                                            <td>{{$direccion->localidad}}</td>
                                            <td>{{$direccion->codigo_postal}}</td>
                                            <td>{{$direccion->telefono}}</td>
                                            <td>{{$direccion->estado ? "HABILITADA" : "DESHABILITADA"}}</td>
                                            <td>{{$direccion->es_direccion_fact ? "SI" : "NO"}}</td>
                                            <td>
                                                <a href="{{ route('direcciones.ver', [$direccion->id_direccion]) }}" class="btn btn-sm btn-success"><i class="fa fa-file"></i>&nbsp;&nbsp;&nbsp;Ver</a>&nbsp;&nbsp;&nbsp;
                                                <button type="button" class="btn btn-sm btn-warning" data-toggle="modal" onclick="cargarDatosDireccion({{json_encode($direccion)}})" data-target="#editDireccion"><i class="fa fa-edit"></i>&nbsp;&nbsp;&nbsp;Editar</button>&nbsp;&nbsp;&nbsp;
                                                <a href="{{ route('direcciones.seleccionardireccionfact', [$direccion->id_direccion, $user->id_user]) }}" class="btn btn-sm btn-primary" {{$direccion->es_direccion_fact? "hidden":""}}><i class="fa fa-hand-pointer"></i>&nbsp;&nbsp;&nbsp;Dir. facturación</a>&nbsp;&nbsp;&nbsp;
                                                <a href="{{ route('direcciones.cambiarestado', [$direccion->id_direccion, $user->id_user, $direccion->estado]) }}" class="btn btn-sm btn-danger" {{$direccion->estado? "":"hidden"}}><i class="fa fa-minus-square"></i>&nbsp;&nbsp;&nbsp;Deshabilitar</a>&nbsp;&nbsp;&nbsp;
                                                <a href="{{ route('direcciones.cambiarestado', [$direccion->id_direccion, $user->id_user, $direccion->estado]) }}" class="btn btn-sm btn-warning" {{$direccion->estado? "hidden":""}}><i class="fa fa-check-square"></i>&nbsp;&nbsp;&nbsp;Habilitar</a>&nbsp;&nbsp;&nbsp;
                                            </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.tab-pane -->
    <div class="modal fade" id="editDireccion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form role="form" id="quickForm" method="post" action="{{ route('direcciones.actualizar', $user->id_user) }}">
                    @csrf
                    <div class="modal-header">
                        <h3 class="modal-title">Editar dirección</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id_direccion" class="form-control" id="id_direccion" required>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="direccion">Dirección</label>
                                <input type="text" name="direccion" class="form-control" id="direccion" placeholder="Ingresar dirección" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="observaciones">Observaciones</label>
                                <input type="text" name="observaciones" class="form-control" id="observaciones" placeholder="Ingresar observaciones">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="provincia">Provincia</label>
                                <input type="text" name="provincia" class="form-control" id="provincia" placeholder="Ingresar provincia" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="localidad">Localidad</label>
                                <input type="text" name="localidad" class="form-control" id="localidad" placeholder="Ingresar localidad" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="codigo_postal">Código postal</label>
                                <input type="text" name="codigo_postal" class="form-control" id="codigo_postal" placeholder="Ingresar código postal" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="telefono">Teléfono</label>
                                <input type="text" name="telefono" class="form-control" id="telefono" placeholder="Ingresar teléfono">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-sm btn-success">Agregar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop