@extends('adminlte::page')

@section('content')
<head>
</head>
<body>
    <script>
      function aceptar(e){
        e.preventDefault();
        var bandera = 0;
        var confirmacion = false;
        if($('input[name*="checkbox"]:checked').length == 0){
          toastr.error('Se debe seleccionar al menos un remito');
          return false;
        }else{
            swal({
                title: 'Esta seguro/a?',
                text: 'Esta seguro/a que desea asignar el pago?',
                icon: 'warning',
                buttons: ["Cancelar", "Confirmar"],
            }).then(function(value) {
                if (value) {
                    $('#quickForm').submit();
                }
            });
        }
      };
    </script>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Asignar pago: {{$pago->descripcion}}</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="float-right">
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <h4>Monto total del pago: <strong>$ {{number_format(($pago->monto/$pago->conversion_dolar),2,",",".")}}</strong></h4>
                            </div>
                            <div class="col-sm-4">
                                <h4>Monto asignado del pago: <strong>$ {{number_format($pago->pagado,2,",",".")}}</strong></h4>
                            </div>
                            <div class="col-sm-4">
                                <h4>Excedente del pago: <strong>$ {{number_format((($pago->monto/$pago->conversion_dolar) - $pago->pagado),2,",",".")}}</strong></h4>
                            </div>
                        </div>
                        <br>
                        <form id="quickForm" class="form-horizontal" method="post" action="{{ route('proveedores.asignarpagosuccess', [$pago->id_pago]) }}">
                            @csrf
                            <input name="monto_pago" value="{{($pago->monto/$pago->conversion_dolar) - $pago->pagado}}" type="hidden" class="form-control" id="monto_pago">
                            <input name="id_proveedor" value="{{$id_proveedor}}" type="hidden" class="form-control" id="id_proveedor">
                            <div class="form-group row">
                                <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Remitos impagos</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <table class="table table-bordered">
                                            <thead>                  
                                                <tr>
                                                    <th>
                                                    </th>
                                                    <th>Fecha</th>
                                                    <th>Nro remito</th>
                                                    <th>Nro pedido</th>
                                                    <th>Monto</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tablaremitos">
                                                @php 
                                                $total = 0;
                                                foreach($remitos as $remito){
                                                $total = $total + $remito->falta_pagar;
                                                @endphp
                                                <tr id="fila{{$remito->id_remito}}">
                                                    <input name="items[{{$remito->id_remito}}][id_remito]" value="{{$remito->id_remito}}" type="hidden" class="form-control" id="items[{{$remito->id_remito}}][id_remito]">
                                                    <input name="items[{{$remito->id_remito}}][monto]" value="{{$remito->falta_pagar}}" type="hidden" class="form-control" id="items[{{$remito->id_remito}}][monto]">
                                                    <td><input name="items[{{$remito->id_remito}}][checkbox]" type="checkbox" id="items[{{$remito->id_remito}}][checkbox]"></td>
                                                    <td>{{$remito->created_at}}</td>
                                                    <td>01-{{substr('00000'.$remito->id_remito, -5)}}</td>
                                                    <td>{{$remito->pedido_id_pedido}}</td>
                                                    <td>$ {{number_format($remito->falta_pagar,2,",",".")}}</td>
                                                </tr>
                                                @php 
                                                }
                                                @endphp
                                            </tbody>
                                            <tbody>
                                                <tr>
                                                    <td><strong>TOTALES</strong></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td><strong id="total_con_descuento">$ {{number_format($total,2,",",".")}}</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-success" onclick="return aceptar(event);">Continuar</button>
                                    </div>
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.tab-pane -->
    </body>
@stop