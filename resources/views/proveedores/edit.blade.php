@extends('adminlte::page')

@section('content')
  <body>
    <script>
      function aceptar(){
        if($("#password").val() != $("#password_confirmation").val()) { 
          toastr.error('Las contraseñas no coinciden');
          return false;
        }else{
          return true;
        }
      }
    </script>
    <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Proveedor <small>Editar</small></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="quickForm" method="post" action="{{ route('proveedores.actualizar', [$user->id_user]) }}">
                @csrf
                <input type="hidden" id="categoriaAFIP" value="{{$user->responsabilidad}}">
                <div class="card-body">
                <div class="row">
                    <div class="form-group col-sm-6">
                      <div class="form-group">
                        <label for="role_id">Rol</label>
                        <input type="hidden" name="role_id" class="form-control" id="role_id" value="{{$roles[0]->id_role}}">
                        <p>{{$roles[0]->name}}</p>
                        {{-- <select name="role_id" class="form-control" id="role_id" required>
                          <option value="">Seleccionar</option>
                          @foreach($roles as $role)
                          <option value="{{$role->id_role}}">{{$role->name}}</option>
                          @endforeach
                        </select> --}}
                      </div>
                    </div>
                    @if(env('MANEJO_DOLARES', false))
                    <div class="form-group col-sm-6">
                        <label for="valor_dolar">Valor dolar</label>
                        <input type="number" step="0.01" name="valor_dolar" class="form-control" id="valor_dolar" value="{{$user->valor_dolar}}" step="0.01" placeholder="Ingresar valor dolar">
                    </div>
                    @endif
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-6">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control" id="email" value="{{$user->email}}" placeholder="Ingresar email" >
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="cuit">Cuil/Cuit</label>
                        <input type="text" name="cuit" class="form-control" id="cuit" value="{{$user->cuit}}" placeholder="Ingresar cuil/cuit" required>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="name">Nombre afip</label>
                      <input type="text" name="name" class="form-control" id="name" value="{{$user->name}}" placeholder="Ingresar nombre de afip" required>
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="nombre_fantasia">Nombre de fantasia</label>
                      <input type="text" name="nombre_fantasia" class="form-control" id="nombre_fantasia" value="{{$user->nombre_fantasia}}" placeholder="Ingresar nombre de fantasia" >
                    </div>
                  </div>
                  <div class="row">
                    <!-- <div class="form-group col-sm-6">
                      <label for="surname">Razón social</label>
                      <input type="text" name="surname" class="form-control" id="surname" value="{{$user->surname}}" placeholder="Ingresar razón social" >
                    </div> -->
                    <div class="form-group col-sm-6">
                      <label for="selectResponsabilidad">Responsabilidad AFIP</label>
                      <select class="form-control" id="selectResponsabilidad" name="responsabilidad" required>
                        <option value=""></option>
                        <option value="1">IVA Responsable Inscripto</option>
                        <option value="2">IVA Responsable no Inscripto</option>
                        <option value="3">IVA no Responsable</option>
                        <option value="4">IVA Sujeto Exento</option>
                        <option value="5">Consumidor Final</option>
                        <option value="6">Responsable Monotributo</option>
                        <option value="7">Sujeto no Categorizado</option>
                        <option value="8">Proveedor del Exterior</option>
                        <option value="9">Cliente Exterior</option>
                        <option value="10">IVA Liberado</option>
                        <option value="11">IVA responsable Inscripto – Agente de Percepción</option>
                        <option value="12">Pequeño Contribuyente Eventual</option>
                        <option value="13">Monotributista Social</option>
                        <option value="14">Pequeño Contribuyente Eventual Social</option>
                      </select>
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="numero_iibb">Número IIBB</label>
                      <input type="text" name="numero_iibb" class="form-control" id="numero_iibb" value="{{$user->numero_iibb}}" placeholder="Ingresar numero IIBB" >
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="telefono">Teléfono fijo</label>
                      <input type="text" name="telefono" class="form-control" id="telefono" value="{{$user->telefono}}" placeholder="Ingresar teléfono fijo" >
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="telefono_movil">Teléfono celular</label>
                      <input type="text" name="telefono_movil" class="form-control" id="telefono_movil" value="{{$user->telefono_movil}}" placeholder="Ingresar teléfono celular" >
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="password">Contraseña</label>
                      <input type="password" name="password" class="form-control" id="password" placeholder="Ingresar contraseña">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="password">Confirmar contraseña</label>
                        <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Confirmar contraseña">
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-success" onclick="return aceptar(event);">Editar</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </body>
@stop

@section('js')
    <script>
      $(document).ready(function(){
        var categoria = $('#categoriaAFIP').val();
        $("#selectResponsabilidad option[value="+categoria+"]").attr("selected", true);
      });
    </script>
@endsection