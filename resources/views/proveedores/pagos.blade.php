@extends('adminlte::page')

@section('content')
    <head>
    </head>
    <body>
      <script type="text/javascript">
        function cargarDatosPago(pago){
          $('#txt_nro_pago').html(pago.id_pago);
          var monto = (parseFloat(pago.monto)/parseFloat(pago.conversion_dolar)).toFixed(2);
          $('#txt_monto').html('$ '+(monto.toString()).replace(".",","));
          var html = '';
          var remitos = pago.remitos.split(',');
          var monto_remitos = pago.monto_remitos.split(',');
          for (let index = 0; index < remitos.length; index++) {
            const remito = remitos[index];
            const monto_remito = monto_remitos[index];
            html = html+"<tr><td><a href='/admin/remitos/ver/"+remito+"'>Remito "+remito+"</a></td>"
                  +"<td>$ "+monto_remito.replace(".",",")+"</td></tr>";
          }
          $('#tablaremitos').html(html);
        }
      </script>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Pagos</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th>Proveedor</th>
                      <th>Id</th>
                      <th>Monto Total {{env('MANEJO_DOLARES', false) ? 'Pesos' : ''}}</th>
                      @if(env('MANEJO_DOLARES', false))
                      <th>Monto Total Dolares</th>
                      @endif
                      <th>Monto Asignado</th>
                      <th>Excedente</th>
                      <th>Estado</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($pagos as $pago)
                    <tr>
                      <td>{{$pago->name}}</td>
                      <td>{{$pago->id_pago}}</td>
                      <td align="right">$ {{number_format($pago->monto,2,",",".")}}</td>
                      @if(env('MANEJO_DOLARES', false))
                      <td align="right">$ {{number_format(($pago->monto/$pago->conversion_dolar),2,",",".")}}</td>
                      @endif
                      <td align="right">$ {{number_format($pago->pagado,2,",",".")}}</td>
                      <td align="right">$ {{number_format(($pago->monto/$pago->conversion_dolar) - $pago->pagado,2,",",".")}}</td>
                      <td>{{$pago->estado}}</td>
                      <td>
                        @if($pago->estado != 'ASIGNADO')
                        <a href="{{ route('proveedores.asignarpago', [$pago->id_pago]) }}" class="btn btn-sm btn-success"><i class="fa fa-hand-pointer"></i>&nbsp;&nbsp;&nbsp;Asignar</a>&nbsp;&nbsp;&nbsp;
                        @endif
                        @if($pago->estado == 'PENDIENTE')
                        <a href="{{ route('proveedores.editarpago', [$pago->id_pago]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Editar</a>&nbsp;&nbsp;&nbsp;
                        <a href="{{ route('proveedores.borrarpago', [$pago->id_pago]) }}" class="btn btn-sm btn-danger" onclick="return confirmar('borrar el pago',this,event);"><i class="fa fa-trash-alt "></i>&nbsp;&nbsp;&nbsp;Borrar</a>&nbsp;&nbsp;&nbsp;
                        @endif
                        @if($pago->comprobante != '')
                          <a class="btn btn-sm btn-info" target="_blank" href="/images/comprobantes/{{$pago->comprobante}}"><i class="fa fa-file"></i>&nbsp;&nbsp;&nbsp;Ver comprobante</a>&nbsp;&nbsp;&nbsp;
                        @endif
                        @if($pago->remitos != '')
                        <a href='#' data-toggle="modal" data-target="#verPago" onclick="cargarDatosPago({{json_encode($pago)}})" class="btn btn-sm btn-secondary"><i class="fa fa-eye "></i>&nbsp;&nbsp;&nbsp;Ver</a>&nbsp;&nbsp;&nbsp;
                        @endif
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                    {{ $pagos->links() }}
              </div>
            </div>
            <!-- /.card -->
            </div>
          </div>
        </div>
        <!-- Modal ver orden pago-->
        <div class="modal fade" id="verPago" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Ver pago</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <div class="row">
                      <div class="form-group col-sm-6">
                          <label for="txt_tipo">Numero de pago</label>
                          <p id="txt_nro_pago"></p>
                      </div>
                      <div class="form-group col-sm-6">
                          <label for="txt_monto">Monto pagado</label>
                          <p id="txt_monto"></p>
                      </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-12">
                      <label for="txt_remitos">Remitos a los que fue imputado con su correspondiente monto: </label>
                      <table class="table table-bordered">
                        <thead>
                          <th>Nro remito</th>
                          <th>Monto</th>
                        </thead>
                        <tbody id="tablaremitos">
                        </tbody>
                      </table>
                    </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cerrar</button>
              </div>
              </div>
          </div>
        </div>
      </body>
        @stop
