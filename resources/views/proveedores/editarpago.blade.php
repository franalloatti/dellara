@extends('adminlte::page')

@section('content')
  {{-- <head>

  <!-- Meta -->
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">


  <!-- CSS -->
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

  <!-- Script -->
  <script src="/js/jquery-3.5.1.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>

  </head> --}}
  <body>
    <script type="text/javascript">
      $(document).ready(function(){
        $('.js-example-basic-single').select2({matcher: matcher});
      });
    </script>
    <style>
    .select2-container .select2-selection--single{
      height: auto !important;
    }
    .select2-container {
      width: 100% !important;
    }
    </style>
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- jquery validation -->
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">Pago <small>Editar</small></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" id="quickForm" method="post" action="{{ route('proveedores.actualizarpago', [$pago->id_pago]) }}" enctype="multipart/form-data">
              @csrf
              <div class="card-body">
                <div class="row">
                  <div class="form-group col-sm-6">
                    <label for="user_id_user">Proveedor</label>
                    <select name="user_id_user" class="form-control js-example-basic-single" id="user_id_user" required>
                      <option value="" disabled selected>Seleccionar</option>
                      @foreach($proveedores as $proveedor)
                        @if($proveedor->id_user == $pago->user_id_user)
                          <option value="{{$proveedor->id_user}}" selected>{{$proveedor->name}}</option>
                        @else
                          <option value="{{$proveedor->id_user}}">{{$proveedor->name}}</option>
                        @endif
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group col-sm-6">
                    <label for="medio_id_medio">Medio de pago</label>
                    <select name="medio_id_medio" class="form-control" id="medio_id_medio" required>
                      <option value="">Seleccionar</option>
                      @foreach($medios as $medio)
                        @if($medio->id_medio == $pago->medio_id_medio)
                          <option value="{{$medio->id_medio}}" selected>{{$medio->nombre}}</option>
                        @else
                          <option value="{{$medio->id_medio}}">{{$medio->nombre}}</option>
                        @endif
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-sm-6">
                    <label for="monto">Monto</label>
                    <input type="number" step="0.01" name="monto" value="{{$pago->monto}}" class="form-control" id="monto" placeholder="Ingresar monto">
                  </div>
                  @if(env('MANEJO_DOLARES',false))
                  <div class="form-group col-sm-6">
                    <label for="conversion_dolar">Conversión dolar</label>
                    <input type="number" step="0.01" name="conversion_dolar" value="{{$pago->conversion_dolar}}" class="form-control" id="conversion_dolar" placeholder="Ingresar conversión dolar">
                  </div>
                  @endif
                </div>
                <div class="row">
                  <div class="form-group col-sm-6">
                    <label for="descripcion">Descripcion</label>
                    <textarea name="descripcion" class="form-control" id="descripcion" placeholder="Ingresar descripcion">{{$pago->descripcion}}</textarea>
                  </div>
                  <div class="form-group col-sm-6">
                    <label for="comprobante">Comprobante</label><br>
                    <input type="file" name="comprobante" class="" id="comprobante">
                    @if($pago->comprobante != '')
                      <a href="/images/comprobantes/{{$pago->comprobante}}" target="_blank">Ver comprobante</a>
                    @endif
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-success">Editar</button>
              </div>
            </form>
          </div>
          <!-- /.card -->
          </div>
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-6">

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </body>
</html>
@stop
