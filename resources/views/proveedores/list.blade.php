@extends('adminlte::page')

@section('content')
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Proveedores</h3>
                <div class="card-tools">
                  <form role="form" action="/admin/proveedores/index" method="POST">
                    @csrf
                    <div class="input-group input-group-sm">
                      <input type="text" id="search" name="search" value="{{$search ? $search : ''}}" class="form-control pull-right" placeholder="Buscar">
                      <div class="input-group-btn">
                        <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th>Nombre fantasía</th>
                      <th>Nombre Afip</th>
                      <th>Cuil/Cuit</th>
                      <th>Estado de cuenta</th>
                      <th>Estado usuario</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($users as $user)
                    <tr>
                      <td>{{$user->nombre_fantasia}}</td>
                      <td>{{$user->name}}</td>
                      <td>{{$user->cuit}}</td>
                      <td align="right">{{(env('MANEJO_DOLARES', false) ? 'USD ' : '$ ') . number_format($user->monto,2,",",".")}}</td>
                      <td>{{$user->status ? "Activo" : "Suspendido"}}</td>
                      <td>
                        <a href="{{ route('proveedores.ver', [$user->id_user]) }}" class="btn btn-sm btn-success"><i class="fa fa-file"></i>&nbsp;&nbsp;&nbsp;Ver</a>&nbsp;&nbsp;&nbsp;
                        <a href="{{ route('proveedores.editar', [$user->id_user]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Editar</a>&nbsp;&nbsp;&nbsp;
                        @if ($user->id_user != Auth::user()->id_user)
                        <a href="{{ route('proveedores.borrar', [$user->id_user]) }}" class="btn btn-sm btn-danger" onclick="return confirmar('borrar el proveedor',this,event);"><i class="fa fa-trash-alt "></i>&nbsp;&nbsp;&nbsp;Borrar</a>&nbsp;&nbsp;&nbsp;
                        <a href="{{ route('proveedores.suspender', [$user->id_user]) }}" class="btn btn-sm btn-primary" onclick="return confirmar('{{$user->status ? 'suspender':'activar'}} al proveedor',this,event);"><i class="fa fa-hand-pointer"></i>&nbsp;&nbsp;&nbsp;{{$user->status? "Suspender":"Activar"}}</a>&nbsp;&nbsp;&nbsp;
                        <a href="{{ route('proveedores.cuenta', [$user->id_user]) }}" class="btn btn-sm btn-info"><i class="fas fa-balance-scale"></i>&nbsp;&nbsp;&nbsp;Estado de cuenta</a>&nbsp;&nbsp;&nbsp;
                        @endif
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                    {{ $users->appends(['search' => $search])->links() }}
              </div>
            </div>
            <!-- /.card -->
            </div>
          </div>
        </div>
        @stop
