@extends('adminlte::page')

@section('content')
<body>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Actualizar precio dolar</h3>
            <div class="card-tools">
            </div>
          </div>
          <!-- /.card-header -->
          <form role="form" action="/admin/proveedores/actualizar_dol" method="POST">
            @csrf
            <div class="card-body">
              <table class="table table-bordered">
                <thead>                  
                  <tr>
                    <th>Nombre del proveedor</th>
                    <th>Valor actual</th>
                    <th>Valor editable</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($users as $user)
                  <tr>
                    <input name="items[{{$user->id_user}}][id_user]" value="{{$user->id_user}}" type="hidden" class="form-control" id="items[{{$user->id_user}}][id_user]">
                    <td>{{$user->nombre_fantasia}}</td>
                    <td>{{$user->valor_dolar}}</td>
                    <td><input name="items[{{$user->id_user}}][valor_dolar]" step="0.01" type="number" value="{{$user->valor_dolar}}" class="form-control" id="items[{{$user->id_user}}][valor_dolar]"></td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <button type="submit" class="btn btn-success">Actualizar</button>
            </div>
          </form>
        </div>
      <!-- /.card -->
      </div>
    </div>
  </div>
</body>
@stop
