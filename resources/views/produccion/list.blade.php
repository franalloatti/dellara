@extends('adminlte::page')

@section('content')
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Planes de producción</h3>
                <div class="card-tools">
                  <form role="form" action="{{ route('produccion.index') }}" method="POST">
                    @csrf
                    <div class="input-group input-group-sm">
                      <input type="date" id="fecha_desde" title="Fecha desde" name="fecha_desde" value="{{$fecha_desde ? $fecha_desde : ''}}" class="form-control pull-right" placeholder="Buscar">
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <input type="date" id="fecha_hasta" title="Fecha hasta" name="fecha_hasta" value="{{$fecha_hasta ? $fecha_hasta : ''}}" class="form-control pull-right" placeholder="Buscar">
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <select name="estado_id" class="form-control" id="estado_id">
                        <option value="">Estado</option>
                        @foreach($estados as $estado)
                        @if($estado->id_estado_produccion == $estadoSelected)
                            <option value="{{$estado->id_estado_produccion}}" selected>{{$estado->nombre}}</option>
                        @else
                            <option value="{{$estado->id_estado_produccion}}">{{$estado->nombre}}</option>
                        @endif
                        @endforeach
                      </select>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <input type="text" id="search" name="search" value="{{$search ? $search : ''}}" class="form-control pull-right" placeholder="Buscar">
                      <div class="input-group-btn">
                        <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th>Producto</th>
                      <th>Cantidad</th>
                      <th>Nro. lote</th>
                      <th>Fecha producción</th>
                      <th>Fecha vencimiento</th>
                      <th>Responsable</th>
                      <th>Estado</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($producciones as $produccion)
                    <tr>
                      <td>{{$produccion->codigo}} - {{$produccion->nombre}}</td>
                      <td>{{$produccion->cantidad_final != '' ? $produccion->cantidad_final : $produccion->cantidad_planificada}}</td>
                      <td>{{$produccion->nro_lote}}</td>
                      <td>{{$produccion->fecha_produccion}}</td>
                      <td>{{$produccion->fecha_vencimiento}}</td>
                      <td>{{$produccion->responsable}}</td>
                      <td>{{$produccion->estado}}</td>
                      <td>
                        <a href="{{ route('produccion.ver', [$produccion->id_plan_produccion]) }}" class="btn btn-sm btn-success"><i class="fa fa-file "></i>&nbsp;&nbsp;&nbsp;Ver</a>&nbsp;&nbsp;&nbsp;
                        {{-- <a href="{{ route('produccion.editar', [$produccion->id_plan_produccion]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Editar</a>&nbsp;&nbsp;&nbsp; --}}
                        @if($produccion->estado != 'Finalizado')
                        <a href="{{ route('produccion.cancelado', [$produccion->id_plan_produccion]) }}" class="btn btn-sm btn-danger" onclick="return confirmar('cancelar el plan de producción',this,event);"><i class="fa fa-trash-alt "></i>&nbsp;&nbsp;&nbsp;Cancelar</a>&nbsp;&nbsp;&nbsp;
                        @endif
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                    {{ $producciones->appends(['search' => $search,'fecha_desde' => $fecha_desde,
                    'fecha_hasta' => $fecha_hasta, 'limit' => $limit, 'estado_id' => $estadoSelected])->links() }}
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    @stop
