@extends('adminlte::page')

@section('content')

<style>
    .form-group{
        margin-bottom: 0rem !important;
    }
</style>
<script>
        function actualizar(cantidad_producto, tabla){
            console.log(tabla);
            $("#"+tabla+" tr").each(function(index, tr) { 
                var cantidad_insumo = parseFloat(cantidad_producto) * parseFloat($(this).find("input[name*='[cantidad_unitario]']").val());
                console.log(cantidad_insumo);
                $(this).find("input[name*='[cantidad]']").val(cantidad_insumo.toFixed(2));
            });
        };
        function aceptar(boton,event){
            event.preventDefault();
            var bandera = 0;
            $("#tabla_insumos tr").each(function(index, tr) { 
                if($(this).find("input[name='nohaystock']").val()){
                    bandera = 1;
                    toastr.error('Falta stock de uno o mas insumos');
                    return false;
                }
            }).promise().done( function() {
                if(!bandera){
                    swal({
                        title: 'Esta seguro/a?',
                        text: 'Esta seguro/a que desea pasar el plan a producción?',
                        icon: 'warning',
                        buttons: ["Cancelar", "Confirmar"],
                    }).then(function(value) {
                        if (value) {
                            window.location.href = boton.href;
                        }
                    });
                }
            });
        }
        function finalizar(e){
            e.preventDefault();
            var bandera = 0;
            var confirmacion = false;
            if($("#tablaproductos tr").length == 0){
            toastr.error('No se puede generar una compra vacía');
            return false;
            }else{
            $("#tablaproductos tr").each(function(index, tr) { 
                if($(this).find("input[name*='[cantidad]']").val() <= 0){
                    bandera = 1;
                    toastr.error('No se puede generar una compra con un item con cantidad 0 (cero)');
                    return false;
                }
            }).promise().done( function() {
                if(!bandera){
                    swal({
                    title: 'Esta seguro/a?',
                    text: 'Esta seguro/a que desea confirmar la compra?',
                    icon: 'warning',
                    buttons: ["Cancelar", "Confirmar"],
                    }).then(function(value) {
                        if (value) {
                        $('#quickForm').submit();
                        }
                    });
                }
            });
            }
        };
</script>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Plan de producción</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="float-right">
                            @if($produccion->estado == 'Planificado')
                                <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#confirmarproduccion"><i class="fa fa-check "></i>&nbsp;&nbsp;&nbsp;Marcar como confirmado</button>&nbsp;&nbsp;&nbsp;
                            @endif
                            @if($produccion->estado == 'Confirmado')
                                <button class="btn btn-sm btn-warning" data-toggle="modal" data-target="#editarproduccion"><i class="fa fa-edit"></i>&nbsp;&nbsp;&nbsp;Editar</button>&nbsp;&nbsp;&nbsp;
                                <a href="{{ route('produccion.producido', [$produccion->id_plan_produccion]) }}" onclick="return aceptar(this,event);" class="btn btn-sm btn-success">
                                    <i class="fa fa-check"></i>&nbsp;&nbsp;&nbsp;Pasar a producción
                                </a>&nbsp;&nbsp;&nbsp;
                            @endif
                            @if($produccion->estado == 'En producción')
                                <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#finalizarproduccion"><i class="fa fa-check"></i>&nbsp;&nbsp;&nbsp;Marcar como Finalizado</button>&nbsp;&nbsp;&nbsp;
                            @endif
                            @if($produccion->estado == 'Finalizado' && $produccion->estado == 'Cancelado')
                                <a href="{{ route('produccion.cancelado', [$produccion->id_plan_produccion]) }}" class="btn btn-sm btn-danger"><i class="fa fa-times"></i>&nbsp;&nbsp;&nbsp;Cancelar</a>&nbsp;&nbsp;&nbsp;
                            @endif
                        </div>
                        <br><br>
                        <form class="form-horizontal">
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label for="inputName">Producto</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <p>{{$produccion->codigo}} - {{$produccion->nombre}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label for="inputName">Estado</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <p>{{$produccion->estado}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label for="inputName">Cantidad planificada</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <p>{{$produccion->cantidad_planificada}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label for="inputName">Cantidad final</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <p>{{$produccion->cantidad_final}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label for="inputName">Fecha producción</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <p>{{date('d/m/Y', strtotime($produccion->fecha_produccion))}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label for="inputName">Fecha vencimiento</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <p>{{date('d/m/Y', strtotime($produccion->fecha_vencimiento))}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label for="inputName">Nro lote</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <p>{{$produccion->nro_lote}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label for="inputName">Responsable</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <p>{{$produccion->responsable}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if($produccion->descripcion != '')
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label for="inputName">Otros datos:</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <p>{{$produccion->descripcion}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if(count($receta) > 0 && $produccion->estado_id <= 1)
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="card-title">Receta</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <table class="table table-bordered">
                                                <thead>                  
                                                    <tr>
                                                    <th>Insumo</th>
                                                    <th>Cantidad</th>
                                                    <th>Stock en almacen</th>
                                                    <!-- <th>Estado</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($receta as $insumo)
                                                    @php
                                                    $stock = $insumo->stock + $insumo->mov_destino + $insumo->mov_origen + $insumo->venta + $insumo->devolucion_venta + $insumo->compra + $insumo->devolucion_compra;
                                                    $cantidad = $insumo->cantidad_receta * $produccion->cantidad_planificada;
                                                    @endphp
                                                    <tr>
                                                    <td>{{$insumo->nombre}}</td>
                                                    <td>{{number_format($cantidad,2,",",".")}} {{$insumo->unidad}}</td>
                                                    <td>{{number_format($stock*$insumo->conversion,2,",",".")}} {{$insumo->unidad}}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>
                            @endif
                            @if(count($produccion_productos) > 0 && $produccion->estado_id > 1)
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="card-title">Insumos necesarios</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <table class="table table-bordered">
                                                <thead>                  
                                                    <tr>
                                                    <th>Insumo</th>
                                                    <th>Cantidad</th>
                                                    <th>Stock en almacen</th>
                                                    <!-- <th>Estado</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody id="tabla_insumos">
                                                @foreach($produccion_productos as $insumo)
                                                    @php
                                                    $stock = $insumo->stock + $insumo->mov_destino + $insumo->mov_origen + $insumo->venta + $insumo->devolucion_venta + $insumo->compra + $insumo->devolucion_compra;
                                                    $cantidad = $insumo->cantidad_planificada;
                                                    @endphp
                                                    <tr>
                                                    <input name="nohaystock" value="{{$cantidad > $stock}}" type="hidden" class="form-control" id="nohaystock">
                                                    <td>{{$insumo->nombre}}</td>
                                                    <td>{{number_format($cantidad,2,",",".")}} {{$insumo->unidad}}</td>
                                                    <td>{{number_format($stock*$insumo->conversion,2,",",".")}} {{$insumo->unidad}}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.tab-pane --> 
    <!-- Modal confirmar producción-->
    <div class="modal fade" id="confirmarproduccion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <form role="form" id="quickForm" method="post" action="{{ route('produccion.confirmado', [$produccion->id_plan_produccion]) }}">
            @csrf
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Confirmar plan de producción</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="producto_id" id="producto_id" value="{{$produccion->producto_id}}">
                        <div class="form-group col-sm-12">
                            <label for="nro_lote">Nro lote</label>
                            <input type="text" name="nro_lote" class="form-control" id="nro_lote" value="{{$produccion->nro_lote}}" placeholder="Ingresar número de lote">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="cantidad">Cantidad</label>
                            <input type="number" name="cantidad" class="form-control" onchange="return actualizar(this.value, 'tablainsumos')" id="cantidad" value="{{number_format($produccion->cantidad_planificada,0)}}">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="fecha_produccion">Fecha de producción</label>
                            <input type="date" name="fecha_produccion" class="form-control" id="fecha_produccion" value="{{$produccion->fecha_produccion}}">
                        </div>
                        <hr>
                        <div class="form-group col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Insumos</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <table class="table table-bordered">
                                        <thead>                  
                                            <tr>
                                                <th>Insumo</th>
                                                <th>Cantidad</th>
                                                <th>Unidad</th>
                                                <th>Stock en almacen</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tablainsumos">
                                        @foreach($receta as $insumo)
                                            @php
                                            $stock = $insumo->stock + $insumo->mov_destino + $insumo->mov_origen + $insumo->venta + $insumo->devolucion_venta + $insumo->compra + $insumo->devolucion_compra;
                                            $cantidad = $insumo->cantidad_receta * $produccion->cantidad_planificada;
                                            @endphp
                                            <tr>
                                                <input name="items[{{$insumo->id_producto}}][id]" value="{{$insumo->id_producto}}" type="hidden" class="form-control" id="items[{{$insumo->id_producto}}][id]">
                                                <input name="items[{{$insumo->id_producto}}][cantidad_unitario]" value="{{$insumo->cantidad_receta}}" type="hidden" class="form-control" id="items[{{$insumo->id_producto}}][cantidad_unitario]">
                                                <td>{{$insumo->nombre}}</td>
                                                <td><input name="items[{{$insumo->id_producto}}][cantidad]" type="number" step="0.01" value="{{number_format($cantidad,2,".","")}}" class="form-control" id="items[{{$insumo->id_producto}}][cantidad]"></td>
                                                <td>{{$insumo->unidad}}</td>
                                                <td>{{number_format($stock,2,",",".")}} {{$insumo->unidad}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-sm btn-success">Agregar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- Modal editar producción-->
    <div class="modal fade" id="editarproduccion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <form role="form" id="quickForm" method="post" action="{{ route('produccion.actualizar', [$produccion->id_plan_produccion]) }}">
            @csrf
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Editar plan de producción</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group col-sm-12">
                            <label for="nro_lote">Nro lote</label>
                            <input type="text" name="nro_lote" class="form-control" id="nro_lote" value="{{$produccion->nro_lote}}" placeholder="Ingresar número de lote">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="cantidad">Cantidad</label>
                            <input type="number" name="cantidad" class="form-control" id="cantidad" value="{{number_format($produccion->cantidad_planificada,0)}}">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="fecha_produccion">Fecha de producción</label>
                            <input type="date" name="fecha_produccion" class="form-control" id="fecha_produccion" value="{{$produccion->fecha_produccion}}">
                        </div>
                        <hr>
                        <div class="form-group col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Insumos</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <table class="table table-bordered">
                                        <thead>                  
                                            <tr>
                                                <th>Insumo</th>
                                                <th>Cantidad</th>
                                                <th>Unidad</th>
                                                <th>Stock en almacen</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tabla_produccion_producto">
                                        @foreach($produccion_productos as $insumo)
                                            @php
                                            $stock = $insumo->stock + $insumo->mov_destino + $insumo->mov_origen + $insumo->venta + $insumo->devolucion_venta + $insumo->compra + $insumo->devolucion_compra;
                                            $cantidad = $insumo->cantidad_planificada;
                                            @endphp
                                            <tr>
                                                <input name="items[{{$insumo->id_producto}}][id]" value="{{$insumo->id_producto}}" type="hidden" class="form-control" id="items[{{$insumo->id_producto}}][id]">
                                                <input name="items[{{$insumo->id_producto}}][id_produccion_producto]" value="{{$insumo->id_produccion_producto}}" type="hidden" class="form-control" id="items[{{$insumo->id_producto}}][id_produccion_producto]">
                                                <td>{{$insumo->nombre}}</td>
                                                <td><input name="items[{{$insumo->id_producto}}][cantidad]" type="number" step="0.01" value="{{number_format($cantidad,2,".","")}}" class="form-control" id="items[{{$insumo->id_producto}}][cantidad]"></td>
                                                <td>{{$insumo->unidad}}</td>
                                                <td>{{number_format($stock*$insumo->conversion,2,",",".")}} {{$insumo->unidad}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-sm btn-success">Aceptar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- Modal finalizar producción-->
    <div class="modal fade" id="finalizarproduccion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <form role="form" id="quickForm" method="post" action="{{ route('produccion.finalizado', [$produccion->id_plan_produccion]) }}">
            @csrf
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Finalizar plan de producción</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="producto_id" id="producto_id" value="{{$produccion->producto_id}}">
                        <div class="form-group col-sm-12">
                            <label for="cantidad_final">Cantidad final</label>
                            <input type="number" name="cantidad_final" class="form-control" id="cantidad_final" value="{{number_format($produccion->cantidad_final,0)}}" required>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="fecha_vencimiento">Fecha de vencimiento</label>
                            <input type="date" name="fecha_vencimiento" class="form-control" id="fecha_vencimiento" value="{{$produccion->fecha_vencimiento}}" required>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="responsable">Responsable</label>
                            <input type="text" name="responsable" class="form-control" id="responsable" value="{{$produccion->responsable}}" placeholder="Ingresar responsable" required>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="descripcion">Otros datos</label>
                            <textarea name="descripcion" class="form-control" id="descripcion"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-sm btn-success">Aceptar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop