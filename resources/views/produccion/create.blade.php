@extends('adminlte::page')

@section('content')
  <head>
   
  </head>

  <body>
    <script type="text/javascript">
      $(document).ready(function(){
        // $(window).keydown(function(event){
        //   if(event.keyCode == 13) {
        //     event.preventDefault();
        //     return false;
        //   }
        // });
        $('.js-example-basic-single').select2({matcher: matcher});
      });
      function cargarFaltante(){
        var producto_seleccionado = $('#producto_id').val();
        var cantidad = 0;
        $("#producto_id option").each(function(){
            if($(this).attr('value') != ''){
              if(producto_seleccionado == $(this).attr('value')){
                cantidad = $(this).attr('cantidad');
              }
            }
          });
        $('#cantidad').val(cantidad);
      }
      function calcular_cant_producto(){
        var producto_seleccionado = $('#producto_id').val();
        var cantidad_insumo = 0;
        $("#producto_id option").each(function(){
            if($(this).attr('value') != ''){
              if(producto_seleccionado == $(this).attr('value')){
                cantidad_insumo = $(this).attr('cantidad_insumo');
              }
            }
          });
        var cantidad_total = $('#cantidad_insumo').val();
        var nro_baches = $('#nro_baches').val() == '' ? 1 : $('#nro_baches').val();
        $('#cantidad').val(parseInt((cantidad_total/cantidad_insumo)*nro_baches));
      }
      function aceptar(e){
        e.preventDefault();
        var bandera = 0;
        var confirmacion = false;
        // console.log($("#fecha_produccion").val());
        if($("#producto_id").val() == null){
          toastr.error('Debe seleccionar un producto');
          return false;
        }else if($("#cantidad").val() <= 0){
          toastr.error('La cantidad debe ser mayor a 0 (cero)');
          return false;
        }else if($("#fecha_produccion").val() == ''){
          toastr.error('Debe seleccionar una fecha de producción');
          return false;
        }else{
          swal({
            title: 'Esta seguro/a?',
            text: 'Esta seguro/a que desea confirmar el plan de producción?',
            icon: 'warning',
            buttons: ["Cancelar", "Confirmar"],
          }).then(function(value) {
              if (value) {
                $('#quickForm').submit();
              }
          });
        }
      };
    </script>
    <style>
    .select2-container .select2-selection--single{
      height: auto !important;
    }
    .select2-container {
      width: 100% !important;
    }
    </style>
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- jquery validation -->
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">Plan de producción <small>Crear</small></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" id="quickForm" method="post" action="{{ route('produccion.guardar') }}">
              @csrf
              <div class="card-body">
                <div class="row">
                  <div class="form-group col-sm-2">
                    <label for="producto_id">Producto</label>
                  </div>
                  <div class="form-group col-sm-6">
                    <select name="producto_id" class="form-control js-example-basic-single" id="producto_id" onchange="cargarFaltante()" required>
                      <option cantidad_insumo="0" cantidad="0" value="" disabled selected>Seleccionar</option>
                      @foreach($productos as $producto)
                      <option cantidad_insumo="{{$producto->cantidad_insumo}}" cantidad="{{$producto->cantidad}}" value="{{$producto->id_producto}}">{{$producto->codigo . " - " . $producto->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group col-sm-2">

                  </div>
                </div>
                <div class="row">
                  @if(env('CALCULO_PRODUCCION_POR_INSUMO',false))
                  <div class="form-group col-sm-6">
                    <label for="cantidad_insumo">Cantidad de Kg de Harina</label>
                    <input type="text" name="cantidad_insumo" class="form-control" onchange="calcular_cant_producto()" id="cantidad_insumo" placeholder="Ingresar cantidad del insumo" required>
                  </div>
                  <div class="form-group col-sm-6">
                    <label for="nro_baches">Nro. de baches</label>
                    <input type="text" name="nro_baches" class="form-control" value="1" onchange="calcular_cant_producto()" id="nro_baches" placeholder="Ingresar número de baches" required>
                  </div>
                  @endif
                  <div class="form-group col-sm-6">
                    <label for="cantidad">Cantidad de producto final</label>
                    <input type="text" name="cantidad" {{env('CALCULO_PRODUCCION_POR_INSUMO',false) ? 'readonly' : ''}} class="form-control" id="cantidad" placeholder="Ingresar cantidad" required>
                  </div>
                  <div class="form-group col-sm-6">
                    <label for="fecha_produccion">Fecha de producción</label>
                    <input type="date" name="fecha_produccion" min='<?php echo date('Y-m-d'); ?>' class="form-control" id="fecha_produccion" required>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-success" onclick="aceptar(event);">Crear</button>
              </div>
            </form>
          </div>
          <!-- /.card -->
          </div>
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-6">

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
    <style>
        .pac-container {
            z-index: 10000 !important;
        }
    </style>
  </body>
</html>
@stop
