@extends('adminlte::page')

@section('content')
  <script>
    function cargarDatosArquear(cuenta){
      console.log(cuenta);
      $('#id_cuenta').val(cuenta.id_medio);
      var monto = parseFloat(cuenta.monto_pagos) + parseFloat(cuenta.monto_arqueo);
      $('#monto_sistema').val(monto.toFixed(2));
      $('#monto_arqueo').val(monto.toFixed(2));
    };
    function aceptar(e){
      e.preventDefault();
      if($("#monto_arqueo").val() == ''){
          $("#monto_arqueo").focus();
          toastr.error('Se debe ingresar un monto de arqueo');
          return false;
      }else{
          swal({
              title: 'Esta seguro/a?',
              text: 'Esta seguro/a que desea realizar un arqueo de esta cuenta?',
              icon: 'warning',
              buttons: ["Cancelar", "Confirmar"],
          }).then(function(value) {
              if (value) {
                  $('#quickForm').submit();
              }
          });
      }
    }
  </script>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Cuentas</h3>
            <div class="card-tools">
              <a href="{{ route('medios.crear') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Nueva</a>&nbsp;&nbsp;&nbsp;
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered">
              <thead>                  
                <tr>
                  <th>Cuenta</th>
                  <th>Descripción</th>
                  <th>Disponible</th>
                  <th>Fecha último arqueo</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>
                @php
                    $total = 0;
                @endphp
                @foreach($medios as $medio)
                @php
                    $total = $total + ($medio->monto_pagos + $medio->monto_arqueo);
                @endphp
                <tr>
                  <td>{{$medio->nombre}}</td>
                  <td>{{$medio->descripcion}}</td>
                  <td align="right">$ {{number_format($medio->monto_pagos + $medio->monto_arqueo,2,",",".")}}</td>
                  <td>{{$medio->fecha_arqueo <> '' ? date('d/m/Y H:s:i', strtotime($medio->fecha_arqueo)) : '-'}}</td>
                  <td>
                    <a href="{{ route('medios.editar', [$medio->id_medio]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Editar</a>&nbsp;&nbsp;&nbsp;
                    <a href="{{ route('medios.borrar', [$medio->id_medio]) }}" class="btn btn-sm btn-danger" onclick="return confirmar('borrar el medio de pago',this,event);"><i class="fa fa-trash-alt "></i>&nbsp;&nbsp;&nbsp;Borrar</a>&nbsp;&nbsp;&nbsp;
                    <a href="{{ route('medios.arqueos', [$medio->id_medio]) }}" class="btn btn-sm btn-info"><i class="fa fa-book"></i>&nbsp;&nbsp;&nbsp;Arqueos</a>&nbsp;&nbsp;&nbsp;
                    <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#modalArquear" onclick="cargarDatosArquear({{json_encode($medio)}})"><i class="fa fa-plus "></i>&nbsp;&nbsp;&nbsp;Nuevo arqueo</button>&nbsp;&nbsp;&nbsp;
                    <a href="{{ route('medios.movimientos', [$medio->id_medio]) }}" class="btn btn-sm btn-secondary"><i class="fa fa-list "></i>&nbsp;&nbsp;&nbsp;Ver movimientos</a>&nbsp;&nbsp;&nbsp;
                  </td>
                </tr>
                @endforeach
                <tr>
                  <td colspan="2">Total de cuentas</td>
                  <td align="right">$ {{number_format($total,2,",",".")}}</td>
                  <td colspan="2"></td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
          <div class="card-footer clearfix">
                {{ $medios->links() }}
          </div>
        </div>
        <!-- /.card -->
      </div>
    </div>
  </div>
  <!-- Modal agregar movimiento-->
  <div class="modal fade" id="modalArquear" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <form role="form" id="quickForm" method="post" action="{{ route('medios.arquear') }}">
      @csrf
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Agregar movimiento</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
            <input type="hidden" name="id_cuenta" class="form-control" id="id_cuenta" required>
            <div class="row">
              <div class="form-group col-sm-12">
                <label for="monto_sistema">Monto sistema</label>
                <input type="number" readonly name="monto_sistema" class="form-control" id="monto_sistema" required>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-sm-12">
                <label for="monto_arqueo">Monto arqueo</label>
                <input type="number" name="monto_arqueo" class="form-control" id="monto_arqueo" required>
              </div>
            </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-sm btn-success" onclick="aceptar(event);">Aceptar</button>
          </div>
        </div>
      </div>
    </form>
  </div>
@stop
