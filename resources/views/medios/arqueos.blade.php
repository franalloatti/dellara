@extends('adminlte::page')

@section('content')
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Arqueos cuenta: {{$cuenta->nombre}}</h3>
                <div class="card-tools">
                  <a href="{{ route('medios.index') }}" class="btn btn-sm btn-secondary"><i class="fa fa-reply"></i>&nbsp;&nbsp;&nbsp;Volver a listado</a>&nbsp;&nbsp;&nbsp;
                  <a href="{{ route('medios.arquear', $cuenta->id_medio) }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Nuevo arqueo</a>&nbsp;&nbsp;&nbsp;
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th>Fecha</th>
                      <th>Monto</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($arqueos as $arqueo)
                    <tr>
                      <td>{{date('d/m/Y H:s:i', strtotime($arqueo->created_at))}}</td>
                      <td align="right">$ {{number_format($arqueo->monto,2,",",".")}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                    {{ $arqueos->links() }}
              </div>
            </div>
            <!-- /.card -->
            </div>
          </div>
        </div>
        @stop
