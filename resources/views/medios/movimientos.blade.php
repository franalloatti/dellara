@extends('adminlte::page')

@section('content')
  <script>
  </script>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Movimientos (Cuenta: {{$cuenta->nombre}})</h3>
            <div class="card-tools">
              <form role="form" action="{{ route('medios.movimientos', array($cuenta->id_medio)) }}" method="POST">
                @csrf
                <div class="input-group input-group-sm">
                  <select name="tipo_movimiento_id" class="form-control" id="tipo_movimiento_id">
                    <option value="">Tipo movimiento</option>
                    @foreach($tipo_movimientos as $tipo_mov)
                    @if($tipo_mov->id_tipo_movimiento == $tipoMovSelected)
                        <option value="{{$tipo_mov->id_tipo_movimiento}}" selected>{{$tipo_mov->nombre}}</option>
                    @else
                        <option value="{{$tipo_mov->id_tipo_movimiento}}">{{$tipo_mov->nombre}}</option>
                    @endif
                    @endforeach
                  </select>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="text" id="search" name="search" value="{{$search ? $search : ''}}" class="form-control pull-right" placeholder="Buscar">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered">
              <thead>                  
                <tr>
                  <th>Tipo movimiento</th>
                  <th>Nombre</th>
                  <th>Descripción</th>
                  <th>Cuenta</th>
                  <th>Monto</th>
                  <th>Fecha</th>
                  <th>Saldo en cuenta</th>
                </tr>
              </thead>
              <tbody>
                @foreach($pagos as $pago)
                <tr>
                  <td>{{$pago->tipo_movimiento}}</td>
                  <td>{{($pago->tipo_movimiento == 'Compra productos' || $pago->tipo_movimiento == 'Venta') ? $pago->name : $pago->nombre}}</td>
                  <td>{{($pago->tipo_movimiento == 'Compra productos' || $pago->tipo_movimiento == 'Venta') ? $pago->id_pago : $pago->descripcion}}</td>
                  <td>{{$pago->cuenta}}</td>
                  <td align="right">$ {{number_format($pago->monto,2,",",".")}}</td>
                  <td>{{date('d/m/Y', strtotime($pago->fecha != '' ? $pago->fecha : $pago->created_at))}}</td>
                  <td align="right">$ {{number_format($total,2,",",".")}}</td>
                  @php
                    $total = $total - $pago->monto;
                  @endphp
                </tr>
                @endforeach
                <tr>
                  <td colspan="6"></td>
                  <td align="right">$ {{number_format($monto,2,",",".")}}</td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
          <div class="card-footer clearfix">
          </div>
        </div>
        <!-- /.card -->
        </div>
      </div>
    </div>
  </div>
@stop
