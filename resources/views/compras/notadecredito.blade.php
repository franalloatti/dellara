@extends('adminlte::page')

@section('content')
<head>
</head>
<body>
    <script type="text/javascript">
      function aceptar(e){
        e.preventDefault();
        var bandera = 0;
        var confirmacion = false;
        if($("#tablaproductos tr").length == 0){
          toastr.error('No se puede generar una solicitud de devolución vacía');
          return false;
        }else{
            $("#tablaproductos tr").each(function(index, tr) { 
                if($(this).find("input[name*='[cantidad]']").val() <= 0){
                    bandera = 1;
                    toastr.error('No se puede generar una solicitud de devolución con un item con cantidad 0 (cero)');
                    return confirmacion;
                }
            }).promise().done( function() {
                if(!bandera){
                    swal({
                    title: 'Esta seguro/a?',
                    text: 'Esta seguro/a que desea confirmar la solicitud de devolución?',
                    icon: 'warning',
                    buttons: ["Cancelar", "Confirmar"],
                    }).then(function(value) {
                        if (value) {
                        $('#quickForm').submit();
                        }
                    });
                }
            });   
        } 
      };
      function actualizarTotales(){
        var cantidadItems = 0;
        var precio_total = 0;
        var descuento = 0;
        var precio_final = 0;
        $("#tablaproductos tr").each(function(index, tr) { 
            // console.log($(this).find("input[name*='[checkbox]']").is(':checked'));
            // if($(this).find("input[name*='[checkbox]']").is(':checked')){
                cantidadItems = cantidadItems + 1;
                precio_total = parseFloat(precio_total) + parseFloat($(this).find("input[name*='[precio_total]']").val());
                descuento = parseFloat(descuento) + parseFloat($(this).find("input[name*='[descuento]']").val());
                precio_final = parseFloat(precio_final) + parseFloat($(this).find("input[name*='[precio_final]']").val());
            // }
        });
        document.getElementById('total_sin_descuento').innerHTML = "$ " + parseFloat(precio_total).toFixed(2);
        console.log("desc"+descuento);
        console.log("cant"+cantidadItems);
        // document.getElementById('descuento_final').innerHTML = cantidadItems ? parseFloat(descuento / cantidadItems).toFixed(2) : parseFloat(0).toFixed(2);
        document.getElementById('total_con_descuento').innerHTML = "$ " + parseFloat(precio_final).toFixed(2);
      };
      function actualizar(id_producto, value){
        var costo_unitario = document.getElementById('items['+id_producto+'][costo_unitario]').value;
        var descuento = document.getElementById('items['+id_producto+'][descuento]').value;
        document.getElementById('items['+id_producto+'][precio_total]').value = parseFloat(costo_unitario*value).toFixed(2);
        document.getElementById('items['+id_producto+'][precio_final]').value = parseFloat(costo_unitario*value*(1-descuento/100)).toFixed(2);
        actualizarTotales();
      };
      function actualizarDescuento(id_producto, descuento){
        var precio_total = document.getElementById('items['+id_producto+'][precio_total]').value;
        document.getElementById('items['+id_producto+'][precio_final]').value = parseFloat(precio_total*(1-descuento/100)).toFixed(2);
        actualizarTotales();
      };
    </script>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Generar remito</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="float-right">
                        </div>
                        <br><br>
                        <form class="form-horizontal" id="quickForm" method="post" action="{{ route('remitos.guardar') }}">
                            @csrf
                            <input name="id_pedido" value="{{$pedido->id_pedido}}" type="hidden" class="form-control" id="id_pedido">
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Cliente</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input name="user_id_user" value="{{$pedido->user_id_user}}" type="hidden" class="form-control" id="user_id_user">
                                            <p>{{$pedido->name}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                <label for="direccion_id_direccion">Dirección de entrega</label>
                                <select name="direccion_id_direccion" class="form-control js-example-basic-single" id="direccion_id_direccion" required>
                                    <!-- <option value="" disabled selected>Seleccionar</option> -->
                                    @foreach($direcciones as $direccion)
                                    @php 
                                    if($direccion->id_direccion == $pedido->id_direccion){
                                    @endphp 
                                        <option value="{{$direccion->id_direccion}}" selected>{{$direccion->direccion}}</option>    
                                    @php 
                                    }else{
                                    @endphp
                                        <option value="{{$direccion->id_direccion}}">{{$direccion->direccion}}</option>
                                    @php    
                                    }
                                    @endphp
                                    @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Productos</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <table class="table table-bordered">
                                            <thead>                  
                                                <tr>
                                                {{-- <th>
                                                <input name="items[todos][checkbox]" type="checkbox" onclick="return marcarTodos(this.checked);" id="items['{{$pedido->id_pedido}}'][todos]">
                                                </th> --}}
                                                <th>Codigo</th>
                                                <th>Nombre</th>
                                                <th>Costo unitario</th>
                                                <th>Cantidad</th>
                                                <th>Costo total</th>
                                                <th>Descuento</th>
                                                <th>Costo final</th>
                                                <th>Unidades disponibles</th>
                                                <!-- <th></th> -->
                                                </tr>
                                            </thead>
                                            <tbody id="tablaproductos">
                                                @php 
                                                $total_sin_descuento = 0.00;
                                                $total_con_descuento = 0.00;
                                                foreach($productos as $producto){
                                                $total_item_sin_desc = $producto->cantidad * $producto->costo_unitario;
                                                $total_item_con_desc = $total_item_sin_desc * (1-($producto->descuento/100));
                                                $total_sin_descuento = $total_sin_descuento + $total_item_sin_desc;
                                                $total_con_descuento = $total_con_descuento + $total_item_con_desc;
                                                @endphp
                                                <tr id="fila{{$producto->id_pedido_producto}}">
                                                    <input name="items[{{$producto->id_pedido_producto}}][id]" value="{{$producto->id_pedido_producto}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][id]">
                                                    <input name="items[{{$producto->id_pedido_producto}}][id_producto]" value="{{$producto->id_producto}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][id_producto]">
                                                    <input name="items[{{$producto->id_pedido_producto}}][costo_unitario]" value="{{$producto->valor_dolar * $producto->costo_dolares}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][costo_unitario]">
                                                    <input name="items[{{$producto->id_pedido_producto}}][cantidad_pedido]" value="{{$producto->cantidad}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][cantidad_pedido]">
                                                    <td>{{$producto->codigo}}</td>
                                                    <td>{{$producto->nombre}}</td>
                                                    <td><input name="items[{{$producto->id_pedido_producto}}][costo_unitario]" value="{{($producto->valor_dolar * $producto->costo_dolares)*($producto->margen / 100.00)}}" type="number" class="form-control" id="items[{{$producto->id_pedido_producto}}][costo_unitario]"></td>
                                                    <td><input name="items[{{$producto->id_pedido_producto}}][cantidad]" type="number" max="{{$producto->stock}}" oninvalid="this.setCustomValidity('No hay stock suficiente')" value="{{$producto->cantidad}}" onchange="return actualizar({{$producto->id_pedido_producto}},this.value)" class="form-control" id="items[{{$producto->id_pedido_producto}}][cantidad]"></td>
                                                    <td><input name="items[{{$producto->id_pedido_producto}}][precio_total]" readonly value="{{$total_item_sin_desc}}" type="number" class="form-control" id="items[{{$producto->id_pedido_producto}}][precio_total]"></td>
                                                    <td><input name="items[{{$producto->id_pedido_producto}}][descuento]" type="number" value="{{$producto->descuento}}" onchange="return actualizarDescuento({{$producto->id_pedido_producto}},this.value)" class="form-control" id="items[{{$producto->id_pedido_producto}}][descuento]"></td>
                                                    <td><input name="items[{{$producto->id_pedido_producto}}][precio_final]" readonly value="{{$total_item_con_desc}}" type="number" class="form-control" id="items[{{$producto->id_pedido_producto}}][precio_final]"></td>
                                                    <td>{{$producto->stock}}</td>
                                                    <!-- <td><button onclick="return borrar('{{$producto->id_pedido_producto}}')" class="btn btn-danger">Eliminar</button></td> -->
                                                </tr>
                                                @php 
                                                }
                                                @endphp
                                            </tbody>
                                            <tbody>
                                                <tr>
                                                    <td colspan="4"><strong>TOTALES</strong></td>
                                                    <td><strong id="total_sin_descuento">$ {{number_format($total_sin_descuento,2,",",".")}}</strong></td>
                                                    <td></td>
                                                    <td><strong id="total_con_descuento">$ {{number_format($total_con_descuento,2,",",".")}}</strong></td>
                                                    <td colspan="2"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-success" onclick="aceptar(event);">Generar remito</button>
                                    </div>
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.tab-pane -->
    </body>
    <style>
        .table{
            font-size: 13px;
        }
        .form-control{
            font-size: 13px;
        }
        .form-control{
            font-size: 13px;
        }
    </style>
@stop