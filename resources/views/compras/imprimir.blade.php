<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Laravel PDF</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <style>
            #table2 {
              border-collapse: collapse;
              border-spacing: 10px;  
            }
        </style>
    </head>
    <body>
        <h3 style="text-align:center">Orden de compra N {{substr('00000'.$pedido->id_pedido, -5)}}</h3>
        <h6 class="mb-3" style="text-align:center">Documento no valido como factura</h6>
        @if(env('DATOS_REMITO_COMPRA', false))
        <h6 class="m-0">==========================================================================</h6>
        @if(env('RAZON_SOCIAL', '') != '')
        <p class="m-0">RAZON SOCIAL: {{env('RAZON_SOCIAL', '')}}</p>
        @endif
        @if(env('CUIT', '') != '')
        <p class="m-0">CUIT: {{env('CUIT', '')}}</p>
        @endif
        @if(env('DIRECCION', '') != '')
        <p class="m-0">{{env('DIRECCION', '')}}</p>
        @endif
        @if(env('EMAIL', '') != '')
        <p class="m-0">{{env('EMAIL', '')}}</p>
        @endif
        @if(env('WEB', '') != '')
        <p class="m-0">{{env('WEB', '')}}</p>
        @endif
        <h6 class="m-0">==========================================================================</h6>
        @endif
        <table id="table2">
            <thead>                  
                <tr>
                    <th style="width:550px;"></th>
                    <th style="text-align: right; "></th>
                </tr>
            </thead>
            <tbody>                  
                <tr>
                    <td>Proveedor: {{$pedido->name}}</td>
                    <td style="text-align: right;">Fecha: {{date('d/m/Y', strtotime($pedido->created_at))}}</td>
                </tr>
                <tr>
                    <td>Dirección: {{$pedido->direccion}} - ({{$pedido->localidad}}, {{$pedido->provincia}})</td>
                    <td style="text-align: right;"></td>
                </tr>
                {{-- <tr>
                    <td>Condicion de iva: </td>
                    <td></td>
                </tr> --}}
                {{-- <tr>
                    <td>Domicilio: {{$pedido->direccion}} - {{$pedido->localidad}} ({{$pedido->provincia}})</td>
                    <td></td>
                </tr> --}}
                {{-- <tr>
                    <td>Condicion de venta: Efectivo</td>
                    <td></td>
                </tr> --}}
            </tbody>
        </table>
        <br>
        <table class="table table-bordered" style="border: solid 4px;font-size: 13px">
            <thead>                  
                <tr>
                    <th>Código proveedor</th>
                    <th>Nombre producto</th>
                    <th>Iva</th>
                    <th>Costo unitario</th>
                    <th>Cantidad</th>
                    <th>Costo total</th>
                    <th>Costo con iva</th>
                </tr>
            </thead>
            <tbody>
            @php 
            $total_sin_iva = 0.00;
            $total_sin_descuento = 0.00;
            $total_con_descuento = 0.00;
            @endphp
            @foreach($productos as $producto)
            @php 
            switch (intval($producto->iva)) {
                case 3:
                $el_iva = '0';
                break;
                case 4:
                $el_iva = '10.5';
                break;
                case 5:
                $el_iva = '21';
                break;
                case 6:
                $el_iva = '27';
                break;
                case 8:
                $el_iva = '5';
                break;
                case 9:
                $el_iva = '2.5';
                break;
                default:
                $el_iva = '0';
                break;
            } 
            if($producto->tipo == 'Insumo'){
                $renglon_sin_iva = $producto->cantidad * $producto->costo_unitario;
                $renglon_sin_descuento = $renglon_sin_iva * (1.0+(floatval($el_iva)/100.0));
                $total_sin_iva = $total_sin_iva + $renglon_sin_iva;
                $total_sin_descuento = $total_sin_descuento + $renglon_sin_descuento;
                $total_con_descuento = $total_con_descuento + ($renglon_sin_descuento*(1-($producto->descuento/100)));
            }else{
                $renglon_sin_iva = $producto->cantidad * $producto->costo_unitario;
                $renglon_sin_descuento = $renglon_sin_iva * (1.0+(floatval($el_iva)/100.0));
                $total_sin_iva = $total_sin_iva + $renglon_sin_iva;
                $total_sin_descuento = $total_sin_descuento + $renglon_sin_descuento;
                $total_con_descuento = $total_con_descuento + ($renglon_sin_descuento*(1-($producto->descuento/100)));
            }
            @endphp
                <tr>
                    <td>{{$producto->codigo_proveedor}}</td>
                    <td>{{$producto->nombre}}</td>
                    <td>{{$el_iva}}%</td>
                    <td>$ {{number_format(($producto->costo_unitario)*(1-($producto->descuento/100)),2,",",".")}}</td>
                    <td>{{$producto->cantidad*$producto->conversion}} {{$producto->tipo == 'Insumo' ? $producto->unidad . " (". $producto->cantidad. "u.)" : ''}}</td>
                    <td>$ {{number_format($renglon_sin_iva,2,",",".")}}</td>
                    <td>$ {{number_format($renglon_sin_descuento,2,",",".")}}</td>
                    </tr>
                    @endforeach
                    <tr>
                    <td colspan="5"><strong>TOTALES</strong></td>
                    <td><strong>$ {{number_format(($total_sin_iva),2,",",".")}}</strong></td>
                    <td><strong>$ {{number_format(($total_con_descuento),2,",",".")}}</strong></td>
                </tr>
            </tbody>
        </table>
        @if($pedido->observaciones_publicas != '')
        <table id="table2">
            <tbody>                  
                <tr>
                    <td>Observaciones: </td>
                    <td>{{$pedido->observaciones_publicas}}</td>
                </tr>
            </tbody>
        </table>
        @endif
    </body>
</html>