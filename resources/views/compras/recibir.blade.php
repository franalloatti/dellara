@extends('adminlte::page')

@section('content')
<head>
</head>
<body>
    <script type="text/javascript">
      function aceptar(e){
        e.preventDefault();
        var bandera = 0;
        $("#tablaproductos tr").each(function(index, tr) { 
            if($(this).find("input[name*='[cantidad]']").val() > 0){
                bandera = 1;
            }
        }).promise().done( function() {
            if(!bandera){
                toastr.error('No se puede recibir una compra vacía');
                return false;
            }else{
                swal({
                    title: 'Esta seguro/a?',
                    text: 'Esta seguro/a que desea recibir la compra?',
                    icon: 'warning',
                    buttons: ["Cancelar", "Confirmar"],
                }).then(function(value) {
                    if (value) {
                        $('#quickForm').submit();
                    }
                });
            }
        });
      }
      function actualizarTotales(){
        var cantidadItems = 0;
        var precio_final_sin_iva = 0;
        var precio_final = 0;
        $("#tablaproductos tr").each(function(index, tr) { 
                cantidadItems = cantidadItems + 1;
                precio_final_sin_iva = parseFloat(precio_final_sin_iva) + parseFloat($(this).find("input[name*='[precio_final_sin_iva]']").val());
                precio_final = parseFloat(precio_final) + parseFloat($(this).find("input[name*='[precio_final]']").val());
        });
        document.getElementById('total_sin_iva').innerHTML = "$ " + parseFloat(precio_final_sin_iva).toFixed(2);
        document.getElementById('total_con_descuento').innerHTML = "$ " + parseFloat(precio_final).toFixed(2);
      };
      function actualizar(id_producto){
        var el_iva = document.getElementById('items['+id_producto+'][iva]').value;
        var costo_unitario = document.getElementById('items['+id_producto+'][costo_unitario]').value;
        var cantidad = document.getElementById('items['+id_producto+'][cantidad]').value;
        document.getElementById('items['+id_producto+'][precio_final_sin_iva]').value = parseFloat(costo_unitario*cantidad).toFixed(2);
        document.getElementById('items['+id_producto+'][precio_final]').value = parseFloat(costo_unitario*cantidad*(1.0+(el_iva/100.0))).toFixed(2);
        actualizarTotales();
      };
    </script>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" id="quickForm" method="post" action="{{ route('compras.recibirsuccess', $pedido->id_pedido) }}">
                    @csrf
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Recibir compra (OC: {{substr('00000'.$pedido->id_pedido, -5)}})</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <input name="id_pedido" value="{{$pedido->id_pedido}}" type="hidden" class="form-control" id="id_pedido">
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Proveedor</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input name="user_id_user" value="{{$pedido->user_id_user}}" type="hidden" class="form-control" id="user_id_user">
                                            <p>{{$pedido->name}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="card-title">Productos</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <table class="table table-bordered">
                                                <thead>                  
                                                    <tr>
                                                    {{-- <th>
                                                    <input name="items[todos][checkbox]" type="checkbox" onclick="return marcarTodos(this.checked);" id="items['{{$pedido->id_pedido}}'][todos]">
                                                    </th> --}}
                                                    <th>Nombre</th>
                                                    <th>Iva</th>
                                                    <th>Costo unitario</th>
                                                    <th>Cantidad</th>
                                                    <th>Costo sin iva</th>
                                                    <th>Costo</th>
                                                    <th>Almacen</th>
                                                    <!-- <th></th> -->
                                                    </tr>
                                                </thead>
                                                <tbody id="tablaproductos">
                                                    @php 
                                                    $total_sin_iva = 0.00;
                                                    $total_sin_descuento = 0.00;
                                                    $cantidad_item = 0;
                                                    foreach($productos as $producto){
                                                    // switch ($producto->iva) {
                                                    //     case '3':
                                                    //     $el_iva = '0';
                                                    //     break;
                                                    //     case '4':
                                                    //     $el_iva = '10.5';
                                                    //     break;
                                                    //     case '5':
                                                    //     $el_iva = '21';
                                                    //     break;
                                                    //     case '6':
                                                    //     $el_iva = '27';
                                                    //     break;
                                                    //     case '8':
                                                    //     $el_iva = '5';
                                                    //     break;
                                                    //     case '9':
                                                    //     $el_iva = '2.5';
                                                    //     break;
                                                    //     default:
                                                    //     $el_iva = '0';
                                                    //     break;
                                                    // } 
                                                    $el_iva = $producto->iva;
                                                    if($producto->tipo == 'Insumo'){
                                                        $cantidad_item = $producto->cantidad;
                                                        $renglon_sin_iva = $cantidad_item * $producto->costo_unitario;
                                                        $renglon_sin_descuento = $renglon_sin_iva * (1.0+(floatval($el_iva)/100.0));
                                                        $total_sin_iva = $total_sin_iva + $renglon_sin_iva;
                                                        $total_sin_descuento = $total_sin_descuento + $renglon_sin_descuento;
                                                    }else{
                                                        $cantidad_item = $producto->cantidad;
                                                        $renglon_sin_iva = $cantidad_item * $producto->costo_unitario;
                                                        $renglon_sin_descuento = $renglon_sin_iva * (1.0+(floatval($el_iva)/100.0));
                                                        $total_sin_iva = $total_sin_iva + $renglon_sin_iva;
                                                        $total_sin_descuento = $total_sin_descuento + $renglon_sin_descuento;
                                                    }
                                                    @endphp
                                                    <tr id="fila{{$producto->id_pedido_producto}}">
                                                        <input name="items[{{$producto->id_pedido_producto}}][id]" value="{{$producto->id_pedido_producto}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][id]">
                                                        <input name="items[{{$producto->id_pedido_producto}}][id_producto]" value="{{$producto->id_producto}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][id_producto]">
                                                        <input name="items[{{$producto->id_pedido_producto}}][cantidad_pedido]" value="{{$cantidad_item}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][cantidad_pedido]">
                                                        <input name="items[{{$producto->id_pedido_producto}}][conversion]" value="{{$producto->conversion}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][conversion]">
                                                        <td>{{$producto->nombre}}</td>
                                                        <td>
                                                            <select class="form-control" name="items[{{$producto->id_pedido_producto}}][iva]" id="items[{{$producto->id_pedido_producto}}][iva]" onchange="return actualizar({{$producto->id_pedido_producto}})">
                                                                <option {{$producto->iva == 21 ? 'selected' : ''}} value="21">21%</option>
                                                                <option {{$producto->iva == 10.5 ? 'selected' : ''}} value="10.5">10.5%</option>
                                                                <option {{$producto->iva == 27 ? 'selected' : ''}} value="27">27%</option>
                                                                <option {{$producto->iva == 5 ? 'selected' : ''}} value="5">5%</option>
                                                                <option {{$producto->iva == 2.5 ? 'selected' : ''}} value="2.5">2.5%</option>
                                                                <option {{$producto->iva == 0 ? 'selected' : ''}} value="0">0%</option>
                                                            </select>
                                                        </td>
                                                        <td><input name="items[{{$producto->id_pedido_producto}}][costo_unitario]" step="0.01" value="{{$producto->costo_unitario}}" onchange="return actualizar({{$producto->id_pedido_producto}})" type="number" class="form-control" id="items[{{$producto->id_pedido_producto}}][costo_unitario]"></td>
                                                        <td><input name="items[{{$producto->id_pedido_producto}}][cantidad]" type="number" value="{{$cantidad_item}}" onchange="return actualizar({{$producto->id_pedido_producto}})" class="form-control" id="items[{{$producto->id_pedido_producto}}][cantidad]"></td>
                                                        <td><input name="items[{{$producto->id_pedido_producto}}][precio_final_sin_iva]" readonly value="{{$renglon_sin_iva}}" type="number" class="form-control" id="items[{{$producto->id_pedido_producto}}][precio_final_sin_iva]"></td>
                                                        <td><input name="items[{{$producto->id_pedido_producto}}][precio_final]" readonly value="{{$renglon_sin_descuento}}" type="number" class="form-control" id="items[{{$producto->id_pedido_producto}}][precio_final]"></td>
                                                        <td>
                                                            <select class="form-control" name="items[{{$producto->id_pedido_producto}}][almacen_id]" id="items[{{$producto->id_pedido_producto}}][almacen_id]">
                                                            @php
                                                            foreach ($almacenes as $almacen) {
                                                                echo "<option value='".$almacen->id_almacen."'>".$almacen->nombre."</option>";
                                                            }
                                                            @endphp
                                                            </select>
                                                        </td>
                                                        <!-- <td><button onclick="return borrar('{{$producto->id_pedido_producto}}')" class="btn btn-danger">Eliminar</button></td> -->
                                                    </tr>
                                                    @php 
                                                    }
                                                    @endphp
                                                </tbody>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="4"><strong>TOTALES</strong></td>
                                                        <td><strong id="total_sin_iva">$ {{number_format($total_sin_iva,2,",",".")}}</strong></td>
                                                        <td><strong id="total_con_descuento">$ {{number_format($total_sin_descuento,2,",",".")}}</strong></td>
                                                        <td></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <label for="inputName">Descripción</label>
                                        <textarea name="descripcion" id="descripcion" rows="5" class="form-control" maxlength="300"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success" onclick="aceptar(event);">Marcar como recibida</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.tab-pane -->
    </body>
    <style>
        .table{
            font-size: 13px;
        }
        .form-control{
            font-size: 13px;
        }
        .form-control{
            font-size: 13px;
        }
    </style>
@stop