@extends('adminlte::page')

@section('content')
  <head>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
  </head>
  <body>
    <script type="text/javascript">
      $(document).ready(function(){
        // $(window).keydown(function(event){
        //   if(event.keyCode == 13) {
        //     event.preventDefault();
        //     return false;
        //   }
        // });
        $('.js-example-basic-single').select2({matcher: matcher});
      });
      function borrar(id_producto){
        $("#fila" + id_producto).remove();
        $('#producto_id_producto option').each(function(i,elem){
          var elid = parseInt($(elem).attr('value'));
          if(id_producto == elid){
            $(elem).attr('disabled',false); 
          }
        });
        actualizarTotales();
      };
      function funcion(){
        var producto_seleccionado = $('#producto_id_producto').val();
        if(document.getElementById('items['+producto_seleccionado+'][id_producto]') != null){
          toastr.error("El producto ya se encuentra en la compra");
          return false;
        }else{
          $("#producto_id_producto option").each(function(){
            if($(this).attr('value') != ''){
              if(producto_seleccionado == $(this).attr('value')){
                var el_Costo = 0;
                var check_iva = $('#check_iva').prop('checked');
                var el_iva = parseInt($(this).attr('iva'));
                switch (el_iva) {
                  case 3:
                    el_iva = '0';
                    break;
                  case 4:
                    el_iva = '10.5';
                    break;
                  case 5:
                    el_iva = '21';
                    break;
                  case 6:
                    el_iva = '27';
                    break;
                  case 8:
                    el_iva = '5';
                    break;
                  case 9:
                    el_iva = '2.5';
                    break;
                  default:
                    el_iva = '0';
                    break;
                }
                var iva_actual = '';
                if(check_iva){
                  iva_actual = el_iva;
                }else{
                  iva_actual = 0;
                }
                el_Costo = $(this).attr('costo_dolares');
                var texto = '<tr id="fila'+$(this).attr('value')+'">'
                +'  <input name="items['+$(this).attr('value')+'][id_pedido_producto]" value="0" type="hidden" class="form-control" id="items['+$(this).attr('value')+'][id_pedido_producto]">'
                +'  <input name="items['+$(this).attr('value')+'][id_producto]" value="'+$(this).attr('value')+'" type="hidden" class="form-control" id="items['+$(this).attr('value')+'][id_producto]">'
                +'  <input name="items['+$(this).attr('value')+'][iva_producto]" value="'+el_iva+'" type="hidden" class="form-control" id="items['+$(this).attr('value')+'][iva_producto]">'
                +'  <input name="items['+$(this).attr('value')+'][iva]" value="'+iva_actual+'" type="hidden" class="form-control" id="items['+$(this).attr('value')+'][iva]">'
                +'  <td>'+$(this).attr('nombre')+'</td>'
                +'  <td id="iva_txt">'+iva_actual+'%</td>'
                +'  <td><input name="items['+$(this).attr('value')+'][costo_unitario]" onchange="return actualizar('+$(this).attr('value')+')" value="'+parseFloat($(this).attr('costo_dolares')).toFixed(2)+'" type="number" class="form-control" id="items['+$(this).attr('value')+'][costo_unitario]"></td>'
                +'  <td><input name="items['+$(this).attr('value')+'][cantidad]" onchange="return actualizar('+$(this).attr('value')+')" type="number" value="0" class="form-control" id="items['+$(this).attr('value')+'][cantidad]"></td>'
                +'  <td><input name="items['+$(this).attr('value')+'][precio_final_sin_iva]" readonly value="0" type="number" class="form-control" id="items['+$(this).attr('value')+'][precio_final_sin_iva]"></td>'
                +'  <td><input name="items['+$(this).attr('value')+'][precio_final]" readonly value="0" type="number" class="form-control" id="items['+$(this).attr('value')+'][precio_final]"></td>'
                +'  <td><button onclick="return borrar('+$(this).attr('value')+');" class="btn btn-danger">Eliminar</button></td>'
                +'</tr>'
                $('#tablaproductos').append(texto);
                return;
              }
            }
          });
          producto_seleccionado.disabled = true;
          $('#producto_id_producto').val($('#producto_id_producto option:eq(0)').val()).trigger('change');
          return false;
        }
      };
      function aceptar(e){
        e.preventDefault();
        var bandera = 0;
        var confirmacion = false;
        if($("#tablaproductos tr").length == 0){
          toastr.error('No se puede generar una compra vacía');
          return false;
        }else{
          $("#tablaproductos tr").each(function(index, tr) { 
              if($(this).find("input[name*='[cantidad]']").val() <= 0){
                bandera = 1;
                toastr.error('No se puede generar una compra con un item con cantidad 0 (cero)');
                return false;
              }
          }).promise().done( function() {
              if(!bandera){
                swal({
                  title: 'Esta seguro/a?',
                  text: 'Esta seguro/a que desea confirmar la compra?',
                  icon: 'warning',
                  buttons: ["Cancelar", "Confirmar"],
                }).then(function(value) {
                    if (value) {
                      $('#quickForm').submit();
                    }
                });
              }
          });
        }
      };
      function actualizarTotales(){
        var cantidadItems = 0;
        var precio_final = 0;
        var precio_final_sin_iva = 0;
        $("#tablaproductos tr").each(function(index, tr) { 
          cantidadItems = cantidadItems + 1;
          precio_final_sin_iva = parseFloat(precio_final_sin_iva) + parseFloat($(this).find("input[name*='[precio_final_sin_iva]']").val());
          precio_final = parseFloat(precio_final) + parseFloat($(this).find("input[name*='[precio_final]']").val());
        });
        document.getElementById('precio_final_final_sin_iva').innerHTML = '$ ' + parseFloat(precio_final_sin_iva).toFixed(2);
        document.getElementById('precio_final_final').innerHTML = '$ ' + parseFloat(precio_final).toFixed(2);
      };
      function actualizar(id_producto){
        var el_iva = document.getElementById('items['+id_producto+'][iva]').value;
        var costo_unitario = document.getElementById('items['+id_producto+'][costo_unitario]').value;
        var cantidad = document.getElementById('items['+id_producto+'][cantidad]').value;
        document.getElementById('items['+id_producto+'][precio_final_sin_iva]').value = parseFloat(costo_unitario*cantidad).toFixed(2);
        document.getElementById('items['+id_producto+'][precio_final]').value = parseFloat(costo_unitario*cantidad*(1.0+(parseFloat(el_iva)/100.0))).toFixed(2);
        actualizarTotales();
      };
      function cambiarIva(checked){
        $("#tablaproductos tr").each(function(index, tr) { 
          if(checked){
            var iva_producto = $(this).find("input[name*='[iva_producto]']").val();
            $(this).find("input[name*='[iva]']").val(iva_producto);
            $(this).find("#iva_txt").html(iva_producto+'%');
          }else{
            var el_iva = '0';
            $(this).find("input[name*='[iva]']").val(0);
            var txt = $(this).find("#iva_txt");
            txt.html(el_iva+'%');
          }
          actualizar($(this).find("input[name*='[id_producto]']").val());
        });
      }
    </script>
    <style>
    .select2-container .select2-selection--single{
      height: auto !important;
    }
    .select2-container {
      width: 100% !important;
    }
    </style>
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- jquery validation -->
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">Compra <small>Editar</small> &nbsp; - &nbsp; (OC: {{substr('00000'.$pedido->id_pedido, -5)}})</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" id="quickForm" method="post" action="{{ route('compras.actualizar', [$pedido->id_pedido]) }}">
              @csrf
              <input name="user_id_user" value="{{$pedido->user_id_user}}" type="hidden" class="form-control" id="user_id_user">
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-6">
                    <label for="inputName">Proveedor</label>
                    <p>{{$pedido->name}}</p>
                  </div>
                  <div class="form-group col-sm-6">
                    <label for="fecha_estimada_recepcion">Fecha estimada de recepción</label>
                    <input type="date" name="fecha_estimada_recepcion" class="form-control" id="fecha_estimada_recepcion" value="{{$pedido->fecha_estimada_recepcion}}" required>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="form-group col-sm-6">
                    <label for="producto_id_producto">Productos</label>
                    <select name="producto_id_producto" class="form-control js-example-basic-single" id="producto_id_producto">
                      <option value="" disabled selected>Seleccionar</option>
                      @foreach($productos as $producto)
                      <option value="{{$producto->id_producto}}" iva="{{$producto->iva}}" codigo="{{$producto->codigo}}" nombre="{{$producto->nombre}}" valor_dolar="{{$producto->valor_dolar}}" costo_dolares="{{$producto->costo_dolares}}">{{$producto->codigo}} - {{$producto->nombre}}</option>
                      @endforeach
                      </select>
                  </div>
                  <div class="form-group col-sm-2" style="align-self: flex-end;">
                    <button type="button" onclick="return funcion();" id="agregar" name="agregar" class="form-control btn btn-info">Agregar</button>
                  </div>
                  <div class="form-group col-sm-2" style="align-self: flex-end;">
                    <input id='check_iva' name='check_iva' type="checkbox" {{$productosPedido[0]->iva == 0 ? '' : 'checked'}} data-toggle="toggle" data-on="Con iva" data-off="Sin iva" onchange="cambiarIva(this.checked);">
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-sm-12">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Producto</th>
                          <th>Iva</th>
                          <th>Costo unitario</th>
                          <th>Cantidad</th>
                          <th>Costo sin iva</th>
                          <th>Costo</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody id="tablaproductos">
                        @php 
                        $total_sin_iva = 0.00;
                        $total_sin_descuento = 0.00;
                        @endphp
                        @foreach($productosPedido as $prod)
                        @php 
                        switch (intval($prod->iva_producto)) {
                            case 3:
                            $el_iva = '0';
                            break;
                            case 4:
                            $el_iva = '10.5';
                            break;
                            case 5:
                            $el_iva = '21';
                            break;
                            case 6:
                            $el_iva = '27';
                            break;
                            case 8:
                            $el_iva = '5';
                            break;
                            case 9:
                            $el_iva = '2.5';
                            break;
                            default:
                            $el_iva = '0';
                            break;
                        } 
                        $iva_actual = $prod->iva;
                        if($prod->tipo == 'Insumo'){
                            $renglon_sin_iva = $prod->cantidad * $prod->costo_unitario;
                            $renglon_sin_descuento = $renglon_sin_iva * (1.0+(floatval($iva_actual) / 100.0));
                            $total_sin_iva = $total_sin_iva + $renglon_sin_iva;
                            $total_sin_descuento = $total_sin_descuento + $renglon_sin_descuento;
                        }else{
                            $renglon_sin_iva = $prod->cantidad * $prod->costo_unitario;
                            $renglon_sin_descuento = $renglon_sin_iva * (1.0+(floatval($iva_actual) / 100.0));
                            $total_sin_iva = $total_sin_iva + $renglon_sin_iva;
                            $total_sin_descuento = $total_sin_descuento + $renglon_sin_descuento;
                        }
                        @endphp
                        <tr id="fila{{$prod->id_producto}}">
                          <input name="items[{{$prod->id_producto}}][id_pedido_producto]" value="{{$prod->id_pedido_producto}}" type="hidden" class="form-control" id="items[{{$prod->id_producto}}][id_pedido_producto]">
                          <input name="items[{{$prod->id_producto}}][id_producto]" value="{{$prod->id_producto}}" type="hidden" class="form-control" id="items[{{$prod->id_producto}}][id_producto]">
                          <input name="items[{{$prod->id_producto}}][iva_producto]" value="{{$el_iva}}" type="hidden" class="form-control" id="items[{{$prod->id_producto}}][iva_producto]">
                          <input name="items[{{$prod->id_producto}}][iva]" value="{{$iva_actual}}" type="hidden" class="form-control" id="items[{{$prod->id_producto}}][iva]">
                          <td>{{$prod->nombre}}</td>
                          <td id="iva_txt">{{$iva_actual}}%</td>
                          <td><input name="items[{{$prod->id_producto}}][costo_unitario]" step="0.01" type="number" onchange="return actualizar({{$prod->id_producto}})" value="{{$prod->costo_unitario}}" class="form-control" id="items[{{$prod->id_producto}}][costo_unitario]"></td>
                          <td><input name="items[{{$prod->id_producto}}][cantidad]" type="number" value="{{$prod->cantidad}}" onchange="return actualizar({{$prod->id_producto}})" class="form-control" id="items[{{$prod->id_producto}}][cantidad]"></td>
                          <td><input name="items[{{$prod->id_producto}}][precio_final_sin_iva]" type="number" readonly value="{{number_format($renglon_sin_iva,2)}}" class="form-control" id="items[{{$prod->id_producto}}][precio_final_sin_iva]"></td>
                          <td><input name="items[{{$prod->id_producto}}][precio_final]" type="number" readonly value="{{number_format($renglon_sin_descuento,2)}}" class="form-control" id="items[{{$prod->id_producto}}][precio_final]"></td>
                          <td><button onclick="return borrar({{$prod->id_producto}});" class="btn btn-danger">Eliminar</button></td>
                        </tr>
                        @endforeach
                      </tbody>
                      <tbody>
                          <th colspan="4">Totales</th>
                          <th id="precio_final_final_sin_iva" name="precio_final_final_sin_iva">$ {{number_format($total_sin_iva,2,",",".")}}</th>
                          <th id="precio_final_final" name="precio_final_final">$ {{number_format($total_sin_descuento,2,",",".")}}</th>
                          <th></th>
                        </tbody>
                    </table>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="form-group col-sm-6">
                    <div class="row">
                      <div class="col-md-12">
                          <label>Observaciones:</label>
                          <textarea name="descripcion" rows="5" class="form-control" id="descripcion" placeholder="Ingresar observaciones..."><?= $pedido->descripcion ?></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="form-group col-sm-6">
                    <div class="row">
                      <div class="col-md-12">
                          <label>Observaciones públicas:</label>
                          <textarea name="observaciones_publicas" rows="5" class="form-control" id="observaciones_publicas" placeholder="Ingresar observaciones públicas..."><?= $pedido->observaciones_publicas ?></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-success" onclick="aceptar(event);">Editar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
@stop
