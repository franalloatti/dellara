@extends('adminlte::page')

@section('content')
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Compras pendientes</h3>
                <div class="card-tools">
                  <form role="form" action="/admin/compras/pendientes" method="POST">
                    @csrf
                    <div class="input-group input-group-sm">
                      <select name="user_id" class="form-control" id="user_id">
                          <option value="">Proveedor</option>
                          @foreach($proveedores as $proveedor)
                          @if($proveedor->id_user == $proveedorSelected)
                              <option value="{{$proveedor->id_user}}" selected>{{$proveedor->name}}</option>
                          @else
                              <option value="{{$proveedor->id_user}}">{{$proveedor->name}}</option>
                          @endif
                          @endforeach
                      </select>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <input type="text" id="search" name="search" value="{{$search ? $search : ''}}" class="form-control pull-right" placeholder="Buscar">
                      <div class="input-group-btn">
                        <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th>OC</th>
                      <th>Proveedor</th>
                      <th>Usuario</th>
                      <th>Estado</th>
                      <th>Monto</th>
                      <th>Monto Iva</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($pedidos as $pedido)
                    <tr>
                      <td>{{substr('00000'.$pedido->id_pedido, -5)}}</td>
                      <td>{{$pedido->name}}</td>
                      <td>{{$pedido->creador}}</td>
                      <td>{{$pedido->estado ? $pedido->estado : 'Pendiente'}}</td>
                      <td align="right" style="white-space: nowrap;">$ {{number_format($pedido->monto_sin_iva,2,",",".")}}</td>
                      <td align="right" style="white-space: nowrap;">$ {{number_format($pedido->monto,2,",",".")}}</td>
                      <td>
                        <a href="{{ route('compras.ver', [$pedido->id_pedido]) }}" class="btn btn-sm btn-success"><i class="fa fa-file "></i>&nbsp;&nbsp;&nbsp;Ver</a>&nbsp;&nbsp;&nbsp;
                        @if($pedido->estado == null)
                        <a href="{{ route('compras.editar', [$pedido->id_pedido]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Editar</a>&nbsp;&nbsp;&nbsp;
                        @endif
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                    {{ $pedidos->appends(['search' => $search])->links() }}
              </div>
            </div>
            <!-- /.card -->
            </div>
          </div>
        </div>
        @stop
