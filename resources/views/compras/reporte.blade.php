<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Reporte compras desde <?php date('d/m/Y', strtotime($fecha_desde));?> hasta <?php date('d/m/Y', strtotime($fecha_hasta));?></title>

        <style>
            #table2 {
              border-collapse: collapse;
              border-spacing: 10px;  
              font-size: 10px;  
              border: 1px solid black;
            }
            table, th, td {
                border: 1px solid black;
            }
            h3 {
                font-size: 20px;
            }
        </style>
    </head>
    <body>
        <table id="table2">
            <thead>                  
                <tr>
                    <th>Fecha</th>
                    <th>Proveedor</th>
                    <th>Dirección</th>
                    <th>N° orden de compra</th>
                    <th>Insumo</th>
                    <th>Costo sin iva</th>
                    <th>Costo con iva</th>
                    <th>Cantidad</th>
                    <th>Descuento</th>
                    <th>Monto total sin iva</th>
                    <th>Monto total con iva</th>
                    <th>Fecha recepción</th>
                </tr>
            </thead>
            <tbody>
                @foreach($pedidos as $pedido)
                <tr>
                    <td>{{date('d/m/Y', strtotime($pedido->fecha_remito))}}</td>
                    <td>{{$pedido->name}}</td>
                    <td>{{$pedido->direccion}}</td>
                    <td>{{$pedido->id_remito}}</td>
                    <td>{{$pedido->codigo}} - {{$pedido->nombre}}</td>
                    <td>{{$pedido->costo_unitario}}</td>
                    
                    <td>{{($pedido->costo_unitario*(1.0+($pedido->iva/100.0)))}}</td>

                    <td>{{$pedido->cantidad}}</td>
                    <td>{{$pedido->descuento}}</td>
                    <td>{{($pedido->costo_unitario * $pedido->cantidad)}}</td>
                    <td>{{($pedido->costo_unitario * $pedido->cantidad * (1+($pedido->iva/100.0)))}}</td>
                    <td>{{$pedido->fecha_cobro ? date('d/m/Y', strtotime($pedido->fecha_cobro)) : ''}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <br>
    </body>
</html>