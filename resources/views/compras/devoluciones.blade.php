@extends('adminlte::page')

@section('content')
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Devoluciones de compras</h3>
                <div class="card-tools">
                  <form role="form" action="/admin/compras/devoluciones" method="POST">
                    @csrf
                    <div class="input-group input-group-sm">
                      <a href="{{ route('compras.createdevolucion') }}" class="btn btn-sm btn-success"><i class="fa fa-file "></i>&nbsp;&nbsp;&nbsp;Nueva</a>&nbsp;&nbsp;&nbsp;
                      <input type="text" id="search" name="search" value="{{$search ? $search : ''}}" class="form-control pull-right" placeholder="Buscar">
                      <div class="input-group-btn">
                        <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th>Id</th>
                      <th>Proveedor</th>
                      <th>Motivo</th>
                      <th>Estado</th>
                      <th>Monto</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($pedidos as $pedido)
                    <tr>
                      <td>{{$pedido->id_pedido}}</td>
                      <td>{{$pedido->name}}</td>
                      <td>{{$pedido->motivo_devolucion == 'defecto' ? 'Producto defectuoso' : 'Error en pedido'}}</td>
                      <td>{{$pedido->estado}}</td>
                      <td align="right" style="white-space: nowrap;">$ {{number_format($pedido->monto,2,",",".")}}</td>
                      <td>
                        <a href="{{ route('compras.verdevolucion', [$pedido->id_pedido]) }}" class="btn btn-sm btn-success"><i class="fa fa-file "></i>&nbsp;&nbsp;&nbsp;Ver</a>&nbsp;&nbsp;&nbsp;
                        @if($pedido->estado == 'Solicitada')
                        {{-- <a href="{{ route('compras.editardevolucion', [$pedido->id_pedido]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Editar</a>&nbsp;&nbsp;&nbsp; --}}
                        @else
                          @if($pedido->estado != 'Rechazada')
                            {{-- <a href="{{ route('notascredito.ver', [$pedido->id_remito]) }}" class="btn btn-sm btn-secondary"><i class="fa fa-file-pdf"></i>&nbsp;&nbsp;&nbsp;Nota de credito asociada</a>&nbsp;&nbsp;&nbsp; --}}
                          @endif
                        @endif
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                    {{ $pedidos->appends(['search' => $search])->links() }}
              </div>
            </div>
            <!-- /.card -->
            </div>
          </div>
        </div>
        @stop
