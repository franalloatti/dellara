@extends('adminlte::page')

@section('content')
  {{-- <head>

    <!-- Meta -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">


    <!-- CSS -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

    <!-- Script -->
    <script src="/js/jquery-3.5.1.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>

  </head> --}}
  <body onload="cargarDatos();">
    <script type="text/javascript">
      $(document).ready(function(){
        // $(window).keydown(function(event){
        //   if(event.keyCode == 13) {
        //     event.preventDefault();
        //     return false;
        //   }
        // });
      });
      function borrar(id_producto){
        $("#fila" + id_producto).remove();
        $('#producto_id_producto option').each(function(i,elem){
          var elid = parseInt($(elem).attr('value'));
          if(id_producto == elid){
            $(elem).attr('disabled',false); 
          }
        });
        actualizarTotales();
      };
      function actualizarTotales(){
        var cantidadItems = 0;
        var precio_final = 0;
        $("#tablaproductos tr").each(function(index, tr) { 
          cantidadItems = cantidadItems + 1;
          precio_final = parseFloat(precio_final) + parseFloat($(this).find("input[name*='[precio_final]']").val());
        });
        document.getElementById('precio_final_final').innerHTML = '$ ' + parseFloat(precio_final).toFixed(2);
      };
      function actualizar(id_producto){
        var costo_unitario = document.getElementById('items['+id_producto+'][costo_unitario]').value;
        var cantidad = document.getElementById('items['+id_producto+'][cantidad]').value;
        document.getElementById('items['+id_producto+'][precio_final]').value = parseFloat(costo_unitario*cantidad).toFixed(2);
        actualizarTotales();
      };
    </script>
    <style>
    .select2-container .select2-selection--single{
      height: auto !important;
    }
    .select2-container {
      width: 100% !important;
    }
    </style>
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- jquery validation -->
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">Orden de compra con faltante <small>Crear</small></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" id="quickForm" method="post" action="{{ route('compras.guardar', [$pedido->id_pedido]) }}">
              @csrf
              <input name="user_id_user" value="{{$pedido->user_id_user}}" type="hidden" class="form-control" id="user_id_user">
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-6">
                    <label for="inputName">Proveedor</label>
                    <p>{{$pedido->name}}</p>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="form-group col-sm-12">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Codigo</th>
                          <th>Producto</th>
                          <th>Costo unitario</th>
                          <th>Cantidad</th>
                          <th>Costo total</th>
                        </tr>
                      </thead>
                      <tbody id="tablaproductos">
                        @php 
                        $total_sin_descuento = 0.00;
                        @endphp
                        @foreach($productos as $prod)
                        @php 
                        $total_sin_descuento = $total_sin_descuento + ($prod->cantidad_faltante * $prod->costo_unitario);
                        @endphp
                        <tr id="fila{{$prod->id_producto}}">
                          <input name="items[{{$prod->id_producto}}][id]" value="{{$prod->id_producto}}" type="hidden" class="form-control" id="items[{{$prod->id_producto}}][id]">
                          <td>{{$prod->codigo}}</td>
                          <td>{{$prod->nombre}}</td>
                          <td><input name="items[{{$prod->id_producto}}][costo_unitario]" step="0.01" type="number" onchange="return actualizar({{$prod->id_producto}})" value="{{$prod->costo_unitario}}" class="form-control" id="items[{{$prod->id_producto}}][costo_unitario]"></td>
                          <td><input name="items[{{$prod->id_producto}}][cantidad]" type="number" value="{{$prod->cantidad_faltante}}" onchange="return actualizar({{$prod->id_producto}})" class="form-control" id="items[{{$prod->id_producto}}][cantidad]"></td>
                          <td><input name="items[{{$prod->id_producto}}][precio_final]" type="number" readonly value="{{$prod->costo_unitario * $prod->cantidad_faltante}}" class="form-control" id="items[{{$prod->id_producto}}][precio_final]"></td>
                        </tr>
                        @endforeach
                      </tbody>
                      <tbody>
                          <th colspan="4">Totales</th>
                          <th id="precio_final_final" name="precio_final_final">$ {{number_format($total_sin_descuento,2,",",".")}}</th>
                      </tbody>
                    </table>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="form-group col-sm-12">
                    <div class="row">
                      <div class="col-md-3">
                          <label>Observaciones:</label>
                      </div>
                      <div class="col-md-9">
                          <textarea name="descripcion" rows="5" class="form-control" id="descripcion" placeholder="Ingresar observaciones..."></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-success">Crear</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
@stop
