@extends('adminlte::page')

@section('content')
  {{-- <head>

    <!-- Meta -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">


    <!-- CSS -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

    <!-- Script -->
    <script src="/js/jquery-3.5.1.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>

  </head> --}}
  <body>
    <script type="text/javascript">
      $(document).ready(function(){
        // $(window).keydown(function(event){
        //   if(event.keyCode == 13) {
        //     event.preventDefault();
        //     return false;
        //   }
        // });
        $('.js-example-basic-single').select2({matcher: matcher});
      });
      function aceptar(e){
        e.preventDefault();
        var bandera = 0;
        var confirmacion = false;
        if($("#tablaproductos tr").length == 0){
          toastr.error('No se puede generar una solicitud de devolución vacía');
          return false;
        }else{
          $("#tablaproductos tr").each(function(index, tr) { 
              if($(this).find("input[name*='[cantidad]']").val() <= 0){
                bandera = 1;
                toastr.error('No se puede generar una solicitud de devolución con un item con cantidad 0 (cero)');
                return false;
              }
          }).promise().done( function() {
              if(!bandera){
                swal({
                  title: 'Esta seguro/a?',
                  text: 'Esta seguro/a que desea confirmar la solicitud de devolución?',
                  icon: 'warning',
                  buttons: ["Cancelar", "Confirmar"],
                }).then(function(value) {
                    if (value) {
                      $('#quickForm').submit();
                    }
                });
              }
          });
        }
      };
      function borrar(id_producto){
        $("#fila" + id_producto).remove();
        $('#producto_id_producto option').each(function(i,elem){
          var elid = parseInt($(elem).attr('value'));
          if(id_producto == elid){
            $(elem).attr('disabled',false); 
          }
        });   
        actualizarTotales();
      };
      function funcion(){
        var combo = document.getElementById("producto_id_producto");
        var producto_seleccionado = combo.options[combo.selectedIndex];
        if(producto_seleccionado.value != ''){
          if(document.getElementById('items['+producto_seleccionado.value+'][id]') != null){
            toastr.error("El producto ya se encuentra en el pedido");
            return false;
          }else{
            var texto = '<tr id="fila'+producto_seleccionado.value+'">'
            +'  <input name="items['+producto_seleccionado.value+'][id]" value="'+producto_seleccionado.value+'" type="hidden" class="form-control" id="items['+producto_seleccionado.value+'][id]">'
            +'  <input name="items['+producto_seleccionado.value+'][costo_unitario]" value="'+producto_seleccionado.getAttribute('costo_unitario')+'" type="hidden" class="form-control" id="items['+producto_seleccionado.value+'][costo_unitario]">'
            +'  <td>'+producto_seleccionado.getAttribute('codigo')+'</td>'
            +'  <td>'+producto_seleccionado.getAttribute('nombre')+'</td>'
            +'  <td><input name="items['+producto_seleccionado.value+'][costo_unitario]" step="0.01" readonly type="number" onchange="return actualizar('+producto_seleccionado.value+')" value="'+parseFloat(producto_seleccionado.getAttribute('costo_unitario')).toFixed(2)+'" class="form-control" id="items['+producto_seleccionado.value+'][costo_unitario]"></td>'
            +'  <td><input name="items['+producto_seleccionado.value+'][cantidad]" type="number" onchange="return actualizar('+producto_seleccionado.value+')" max="'+producto_seleccionado.getAttribute('cantidad')+'" value="'+producto_seleccionado.getAttribute('cantidad')+'" class="form-control" id="items['+producto_seleccionado.value+'][cantidad]"></td>'
            +'  <td><input name="items['+producto_seleccionado.value+'][precio_final]" type="number" readonly value="'+parseFloat(producto_seleccionado.getAttribute('cantidad') * parseFloat(producto_seleccionado.getAttribute('costo_unitario')).toFixed(2)).toFixed(2)+'" class="form-control" id="items['+producto_seleccionado.value+'][precio_final]"></td>'
            +'  <td><button onclick="return borrar('+producto_seleccionado.value+');" class="btn btn-danger">Eliminar</button></td>'
            +'</tr>'
            $('#tablaproductos').append(texto);
            producto_seleccionado.disabled = true;
            $('#producto_id_producto').val($('#producto_id_producto option:eq(0)').val()).trigger('change');
            actualizarTotales();
            return false;
          }
        }
      };
      function actualizarTotales(){
        var cantidadItems = 0;
        var precio_final = 0;
        $("#tablaproductos tr").each(function(index, tr) { 
          cantidadItems = cantidadItems + 1;
          precio_final = parseFloat(precio_final) + parseFloat($(this).find("input[name*='[precio_final]']").val());
        });
        document.getElementById('precio_final_final').innerHTML = '$ ' + parseFloat(precio_final).toFixed(2);
      };
      function actualizar(id_producto){
        var costo_unitario = document.getElementById('items['+id_producto+'][costo_unitario]').value;
        var cantidad = document.getElementById('items['+id_producto+'][cantidad]').value;
        document.getElementById('items['+id_producto+'][precio_final]').value = parseFloat(costo_unitario*cantidad).toFixed(2);
        actualizarTotales();
      };
    </script>
    <style>
    .select2-container .select2-selection--single{
      height: auto !important;
    }
    .select2-container {
      width: 100% !important;
    }
    </style>
<div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Solicitud de devolución <small>Crear</small></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="quickForm" method="post" action="{{ route('devoluciones.guardarcliente') }}">
                @csrf
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="name_client">Proveedor</label>
                      <p name="name_client" id="name_client" required>{{$cliente->name}}</p>
                    </div>
                    <input type="hidden" name="user_id_user" value="{{$cliente->id_user}}" class="form-control" id="user_id_user" required>
                    <div class="form-group col-sm-6">
                      <label for="motivo_devolucion">Motivo</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <select name="motivo_devolucion" class="form-control js-example-basic-single" id="motivo_devolucion" required>
                        <option value="" disabled selected>Seleccionar</option>
                        <option value="defecto">Producto defectuoso</option>
                        <option value="error">Error en pedido</option>
                      </select>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="producto_id_producto">Productos</label>
                      <select name="producto_id_producto" class="form-control js-example-basic-single" id="producto_id_producto">
                        <option value="" disabled selected>Seleccionar</option>
                        @foreach($productos as $producto)
                        <option value="{{$producto->id_producto}}" costo_unitario="{{$producto->costo_unitario}}" costo_unitario="{{$producto->costo_unitario}}" id_remito="{{$producto->id_remito}}" cantidad="{{$producto->cantidad}}" codigo="{{$producto->codigo}}" nombre="{{$producto->nombre}}">{{$producto->codigo}} - {{$producto->nombre}} (Cant: {{$producto->cantidad}} u.)</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="form-group col-sm-2" style="align-self: flex-end;">
                      <button type="button" onclick="return funcion();" id="agregar" name="agregar" class="form-control btn btn-info">Agregar</button>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-12">
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>Codigo</th>
                            <th>Producto</th>
                            <th>Costo unitario</th>
                            <th>Cantidad</th>
                            <th>Costo total</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody id="tablaproductos">
                        </tbody>
                        <tbody>
                            <th colspan="4">Totales</th>
                            <th id="precio_final_final" name="precio_final_final">0</th>
                            <th></th>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-success" onclick="aceptar(event);">Crear</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
      <div class="modal fade" id="addDireccion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Nueva dirección</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="direccion">Dirección</label>
                            <input type="text" name="direccion" class="form-control" id="direccion" placeholder="Ingresar dirección" required>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="observaciones">Observaciones</label>
                            <input type="text" name="observaciones" class="form-control" id="observaciones" placeholder="Ingresar observaciones">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="provincia">Provincia</label>
                            <input type="text" name="provincia" class="form-control" id="provincia" placeholder="Ingresar provincia" required>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="localidad">Localidad</label>
                            <input type="text" name="localidad" class="form-control" id="localidad" placeholder="Ingresar localidad" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="codigo_postal">Código postal</label>
                            <input type="text" name="codigo_postal" class="form-control" id="codigo_postal" placeholder="Ingresar código postal" required>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="telefono">Teléfono</label>
                            <input type="text" name="telefono" class="form-control" id="telefono" placeholder="Ingresar teléfono">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-sm btn-success" onclick="return functionAddDireccion();">Agregar</button>
                </div>
            </div>
        </div>
      </div>
  </body>
</html>
@stop
