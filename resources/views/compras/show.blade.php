@extends('adminlte::page')

@section('content')
    <head>
    </head>
    <body>
        <script type="text/javascript">
            function cargarDatosCompra(compra){
                @if(env('MANEJO_DOLARES',false))
                $('#monto').val(((compra.monto_con_desc - compra.monto_pagos)*compra.valor_dolar).toFixed(2));
                $('#monto_dolares').val((compra.monto_con_desc - compra.monto_pagos).toFixed(2));
                $('#conversion_dolar').val(compra.valor_dolar);
                $("#monto").attr({
                    "max" : ((compra.monto_con_desc - compra.monto_pagos)*compra.valor_dolar).toFixed(2)
                });
                @else
                $('#monto').val((compra.monto_con_desc - compra.monto_pagos).toFixed(2));
                $("#monto").attr({
                    "max" : (compra.monto_con_desc - compra.monto_pagos).toFixed(2)
                });
                @endif
            }
            function aceptar(e){
                e.preventDefault();
                var bandera = 0;
                var confirmacion = false;
                if($("#cuenta_id").val() == ''){
                    toastr.error('Se debe seleccionar una cuenta');
                    $("#cuenta_id").focus();
                    return false;
                }else{
                    if($("#monto").val() == ''){
                        toastr.error('Se debe ingrear un monto');
                        $("#monto").focus();
                        return false;
                    }else{
                        swal({
                        title: 'Esta seguro/a?',
                        text: 'Esta seguro/a que desea registrar el pago?',
                        icon: 'warning',
                        buttons: ["Cancelar", "Confirmar"],
                        }).then(function(value) {
                            if (value) {
                            $('#form_modal').submit();
                            }
                        });
                    }
                }
            };
            function actualizarMontos(){
                var monto_pesos = $("#monto").val();
                var conversion_dolar = $("#conversion_dolar").val();
                $("#monto_dolares").val((monto_pesos / conversion_dolar).toFixed(2));
            }
            function cambiarInfo(valor){
                if(valor == 'Cheque'){
                    document.getElementById('div_cheque').hidden = false;
                    document.getElementById('numero').required = true;
                }else{
                    document.getElementById('div_cheque').hidden = true;
                    document.getElementById('numero').required = false;
                }
            }
            function cargardatosorden(orden){
                $('#monto').val(orden.monto);
                $('#id_orden').val(orden.id_orden_pago);
                $('#id_remito').val(orden.remito_id_remito);
                $('#descripcion').val(orden.observaciones);
                console.log(orden);
            }
            function cargardatosverorden(orden){
                $('#txt_tipo').html(orden.tipo);
                $('#txt_monto').html(orden.monto);
                $('#txt_fecha_emision').html(orden.fecha_emision);
                $('#txt_fecha_posible_pago').html(orden.fecha_efectivo_pago != null ? orden.fecha_efectivo_pago : orden.fecha_posible_pago);
                $('#txt_observaciones').html(orden.observaciones);
                if(orden.tipo == 'Cheque'){
                    document.getElementById('div_cheque_ver').hidden = false;
                    $('#txt_numero').html(orden.numero);
                    if(orden.ruta_imagen != ''){
                        $('#txt_ruta_imagen').html("<a href='/images/ordenes_pago/"+orden.ruta_imagen+"' target='_blank'>Ver imagen</a>");
                    }
                }else{
                    document.getElementById('div_cheque_ver').hidden = true;
                }
                console.log(orden);
            }
        </script>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Compra (OC: {{substr('00000'.$pedido->id_pedido, -5)}})</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="float-right">
                                &nbsp;&nbsp;&nbsp;
                                @if($pedido->estado != null && $pedido->estado != 'Aprobada')
                                    @if($pedido->ruta_archivo == null)
                                        <a href="#" data-toggle="modal" data-target="#modalArchivo" class="btn btn-sm btn-primary"><i class="fa fa-file"></i>&nbsp;&nbsp;&nbsp;Adjuntar archivo</a>&nbsp;&nbsp;&nbsp;
                                    @else
                                        <a target="_blank" href="/images/compras/{{$pedido->ruta_archivo}}">&nbsp;&nbsp;&nbsp;Ver archivo</a>&nbsp;&nbsp;&nbsp;
                                        <a href="{{ route('compras.deletearchivo', [$pedido->id_pedido]) }}" onclick="return confirmar('eliminar el archivo',this,event);" class="btn btn-sm btn-danger"><i class="fa fa-times"></i>&nbsp;&nbsp;&nbsp;Eliminar archivo</a>&nbsp;&nbsp;&nbsp;
                                    @endif
                                @endif
                                @if($pedido->estado == null)
                                    <a href="{{ route('compras.aprobar', [$pedido->id_pedido]) }}" onclick="return confirmar('aprobar la compra',this,event);" class="btn btn-sm btn-success"><i class="fa fa-check"></i>&nbsp;&nbsp;&nbsp;Aprobar</a>&nbsp;&nbsp;&nbsp;
                                @else
                                    <a href="{{ route('compras.imprimir', [$pedido->id_pedido]) }}" class="btn btn-sm btn-secondary"><i class="fa fa-print"></i>&nbsp;&nbsp;&nbsp;Imprimir</a>&nbsp;&nbsp;&nbsp;
                                @endif
                                @if($pedido->estado == 'Aprobada')
                                    <a href="{{ route('compras.recibir', [$pedido->id_pedido]) }}" class="btn btn-sm btn-success"><i class="fa fa-check "></i>&nbsp;&nbsp;&nbsp;Recibir</a>&nbsp;&nbsp;&nbsp;
                                @endif
                                @if(($pedido->estado == 'Recibida' || $pedido->estado == 'Aprobada') && Auth::user()->hasPermiso('Ordenes de pago', 'crear'))
                                    <a href="#" data-toggle="modal" data-target="#ordenPago" class="btn btn-sm btn-primary"><i class="fa fa-plus "></i>&nbsp;&nbsp;&nbsp;Orden de pago</a>&nbsp;&nbsp;&nbsp;
                                @endif
                                @if($hay_faltante)
                                    <a href="{{ route('compras.faltante', [$pedido->id_pedido]) }}" class="btn btn-sm btn-info"><i class="fas fa-cart-arrow-down"></i>&nbsp;&nbsp;&nbsp;Generar OC con faltante</a>&nbsp;&nbsp;&nbsp;
                                @endif
                                @if($hay_excedente)
                                    <a href="{{ route('compras.excedente', [$pedido->id_pedido]) }}" class="btn btn-sm btn-warning"><i class="fas fa-fw fa-hand-paper"></i>&nbsp;&nbsp;&nbsp;Generar devolución con excedente</a>&nbsp;&nbsp;&nbsp;
                                @endif
                                @if($pedido->estado == 'Recibida')
                                    <a data-toggle="modal" data-target="#pagar" onclick="cargarDatosCompra({{json_encode($pedido)}})" href="#" class="btn btn-sm btn-success"><i class="fa fa-check "></i>&nbsp;&nbsp;&nbsp;Pagar</a>&nbsp;&nbsp;&nbsp;
                                @endif
                                @if($pedido->estado == null || $pedido->estado == 'Aprobada')
                                    <a href="{{ route('compras.editar', [$pedido->id_pedido]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Editar</a>&nbsp;&nbsp;&nbsp;                            
                                    <a href="{{ route('compras.cancelar', [$pedido->id_pedido]) }}" onclick="return confirmar('cancelar la compra',this,event);" class="btn btn-sm btn-danger"><i class="fa fa-times "></i>&nbsp;&nbsp;&nbsp;Cancelar</a>&nbsp;&nbsp;&nbsp;
                                @endif
                                @if($pedido->estado <> null && $pedido->estado <> 'Aprobada' && $pedido->estado <> 'Recibida')
                                    Estado: {{$pedido->estado ? $pedido->estado : 'Pendiente'}}
                                @endif
                            </div>
                            <br><br>
                            <form class="form-horizontal">
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <div class="col-sm-3">
                                                <label for="inputName">Proveedor</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <p>{{$pedido->name}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <div class="col-sm-3">
                                                <label for="inputName">Monto pagado</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <p>{{env('MANEJO_DOLARES', false) ? 'USD' : '$'}} {{number_format($pedido->monto_pagos,2,",",".")}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <div class="col-sm-3">
                                                <label for="inputName">Usuario</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <p>{{$pedido->creador}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <div class="col-sm-3">
                                                <label for="inputName">Falta pagar</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <p>{{env('MANEJO_DOLARES', false) ? 'USD' : '$'}} {{number_format($pedido->monto_con_desc - $pedido->monto_pagos,2,",",".")}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <div class="col-sm-3">
                                                <label for="inputName">Fecha emisión</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <p>{{date('d/m/Y H:i', strtotime($pedido->created_at))}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <div class="col-sm-3">
                                                <label for="inputName">Fecha estimada recepción</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <p>{{date('d/m/Y', strtotime($pedido->fecha_estimada_recepcion))}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <div class="col-sm-3">
                                                <label for="inputName">Observaciones internas</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <p>{{$pedido->descripcion}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <div class="col-sm-3">
                                                <label for="inputName">Observaciones públicas</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <p>{{$pedido->observaciones_publicas}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if($productos_remito !== null)
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="card-title">Productos remito</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <table class="table table-bordered">
                                            <thead>                  
                                                <tr>
                                                <th>Codigo</th>
                                                <th>Nombre</th>
                                                <th>Iva</th>
                                                <th>Cantidad</th>
                                                <th>Costo unitario</th>
                                                <th>Costo</th>
                                                <th>Costo con iva</th>
                                                <!-- <th>Estado</th> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @php 
                                            $total_sin_iva = 0.00;
                                            $total_sin_descuento = 0.00;
                                            @endphp
                                            @foreach($productos_remito as $producto_remito)
                                            @php 
                                            $el_iva = $producto_remito->iva;
                                            if($producto_remito->tipo == 'Insumo'){
                                                $renglon_sin_iva = $producto_remito->cantidad * $producto_remito->costo_unitario;
                                                $renglon_sin_descuento = $renglon_sin_iva * (1.0+(floatval($el_iva)/100.0));
                                                $total_sin_iva = $total_sin_iva + $renglon_sin_iva;
                                                $total_sin_descuento = $total_sin_descuento + $renglon_sin_descuento;
                                            }else{
                                                $renglon_sin_iva = $producto_remito->cantidad * $producto_remito->costo_unitario;
                                                $renglon_sin_descuento = $renglon_sin_iva * (1.0+(floatval($el_iva)/100.0));
                                                $total_sin_iva = $total_sin_iva + $renglon_sin_iva;
                                                $total_sin_descuento = $total_sin_descuento + $renglon_sin_descuento;
                                            }
                                            @endphp
                                                @if($producto_remito->cantidad > 0)
                                                <tr>
                                                <td>{{$producto_remito->codigo}}</td>
                                                <td>{{$producto_remito->nombre}}</td>
                                                <td>{{$el_iva}}%</td>
                                                <td>{{$producto_remito->cantidad*$producto_remito->conversion}} {{$producto_remito->tipo == 'Insumo' ? $producto_remito->unidad . " (". $producto_remito->cantidad. "u.)" : ''}}</td>
                                                <td style="white-space: nowrap;">$ {{number_format($producto_remito->costo_unitario,2,",",".")}}</td>
                                                <td style="white-space: nowrap;">$ {{number_format($renglon_sin_iva,2,",",".")}}</td>
                                                <td style="white-space: nowrap;">$ {{number_format($renglon_sin_descuento,2,",",".")}}</td>
                                                </tr>
                                                @endif
                                            @endforeach
                                                <tr>
                                                <td colspan="5"><strong>TOTALES</strong></td>
                                                <td style="white-space: nowrap;"><strong>$ {{number_format($total_sin_iva,2,",",".")}}</strong></td>
                                                <td style="white-space: nowrap;"><strong>$ {{number_format($total_sin_descuento,2,",",".")}}</strong></td>
                                                </tr>
                                            </tbody>
                                            </table>
                                        </div>
                                        <!-- /.card-body -->
                                        </div>
                                        <!-- /.card -->
                                    </div>
                                </div>
                                @endif
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h3 class="card-title">Productos compra</h3>
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <table class="table table-bordered">
                                                <thead>                  
                                                    <tr>
                                                    <th>Codigo</th>
                                                    <th>Nombre</th>
                                                    <th>Iva</th>
                                                    <th>Cantidad</th>
                                                    <th>Costo unitario</th>
                                                    <th>Costo</th>
                                                    <th>Costo con iva</th>
                                                    <!-- <th>Estado</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @php 
                                                $total_sin_iva = 0.00;
                                                $total_sin_descuento = 0.00;
                                                $renglon_sin_iva = 0.00;
                                                $renglon_sin_descuento = 0.00;
                                                @endphp
                                                @foreach($productos as $producto)
                                                @php
                                                // switch ($producto->iva) {
                                                //     case '3':
                                                //     $el_iva = '0';
                                                //     break;
                                                //     case '4':
                                                //     $el_iva = '10.5';
                                                //     break;
                                                //     case '5':
                                                //     $el_iva = '21';
                                                //     break;
                                                //     case '6':
                                                //     $el_iva = '27';
                                                //     break;
                                                //     case '8':
                                                //     $el_iva = '5';
                                                //     break;
                                                //     case '9':
                                                //     $el_iva = '2.5';
                                                //     break;
                                                //     default:
                                                //     $el_iva = '0';
                                                //     break;
                                                // } 
                                                $el_iva = $producto->iva;
                                                if($producto->tipo == 'Insumo'){
                                                    $renglon_sin_iva = $producto->cantidad * $producto->costo_unitario;
                                                    $renglon_sin_descuento = $renglon_sin_iva * (1.0+(floatval($el_iva)/100.0));
                                                    $total_sin_iva = $total_sin_iva + $renglon_sin_iva;
                                                    $total_sin_descuento = $total_sin_descuento + $renglon_sin_descuento;
                                                }else{
                                                    $renglon_sin_iva = $producto->cantidad * $producto->costo_unitario;
                                                    $renglon_sin_descuento = $renglon_sin_iva * (1.0+(floatval($el_iva)/100.0));
                                                    $total_sin_iva = $total_sin_iva + $renglon_sin_iva;
                                                    $total_sin_descuento = $total_sin_descuento + $renglon_sin_descuento;
                                                }
                                                @endphp
                                                    <tr>
                                                    <td>{{$producto->codigo}}</td>
                                                    <td>{{$producto->nombre}}</td>
                                                    <td>{{$el_iva}}%</td>
                                                    <td>{{$producto->cantidad*$producto->conversion}} {{$producto->tipo == 'Insumo' ? $producto->unidad . " (". $producto->cantidad. "u.)" : ''}}</td>
                                                    <td style="white-space: nowrap;">$ {{number_format($producto->costo_unitario,2,",",".")}}</td>
                                                    <td style="white-space: nowrap;">$ {{number_format($renglon_sin_iva,2,",",".")}}</td>
                                                    <td style="white-space: nowrap;">$ {{number_format($renglon_sin_descuento,2,",",".")}}</td>
                                                    </tr>
                                                    @endforeach
                                                    <tr>
                                                    <td colspan="5"><strong>TOTALES</strong></td>
                                                    <td style="white-space: nowrap;"><strong>$ {{number_format($total_sin_iva,2,",",".")}}</strong></td>
                                                    <td style="white-space: nowrap;"><strong>$ {{number_format($total_sin_descuento,2,",",".")}}</strong></td>
                                                    </tr>
                                                </tbody>
                                                </table>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <!-- /.card -->
                                    </div>
                                </div>
                                @if(Auth::user()->hasPermiso('Ordenes de pago', 'ver'))
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h3 class="card-title">Ordenes de pago</h3>
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <table class="table table-bordered">
                                                    <thead>                  
                                                      <tr>
                                                        <th>Fecha de pago</th>
                                                        <th>Tipo</th>
                                                        <th>Proveedor</th>
                                                        <th>Nro. orden</th>
                                                        <th>Monto</th>
                                                        <th>Acciones</th>
                                                      </tr>
                                                    </thead>
                                                    <tbody>
                                                      @foreach($ordenes as $orden)
                                                      <tr>
                                                        <td>{{date('d/m/Y', strtotime($orden->fecha_efectivo_pago ? $orden->fecha_efectivo_pago : $orden->fecha_posible_pago))}}</td>
                                                        <td>{{$orden->tipo}}</td>
                                                        <td>{{$orden->proveedor}}</td>
                                                        <td>{{substr('00000'.$orden->id_orden_pago, -5)}}</td>
                                                        <td align="right" style="white-space: nowrap;">$ {{number_format($orden->monto,2,",",".")}}</td>
                                                        <td>
                                                          @if(Auth::user()->hasPermiso('Ordenes de pago', 'ver'))
                                                          <a href='#' data-toggle="modal" data-target="#verOrdenPago" onclick="cargardatosverorden({{json_encode($orden)}})" class="btn btn-sm btn-success"><i class="fa fa-file "></i>&nbsp;&nbsp;&nbsp;Ver</a>&nbsp;&nbsp;&nbsp;
                                                          @endif
                                                          @if($orden->pagado == 0)
                                                            @if(Auth::user()->hasPermiso('Ordenes de pago', 'editar'))
                                                            <a href='#' data-toggle="modal" data-target="#pagarorden" onclick="cargardatosorden({{json_encode($orden)}})" class="btn btn-sm btn-secondary"><i class="fa fa-check"></i>&nbsp;&nbsp;&nbsp;Pagar</a>&nbsp;&nbsp;&nbsp;
                                                            <a href="{{ route('compras.deleteordenpago', [$orden->id_orden_pago, 'ver',$pedido->id_pedido]) }}" class="btn btn-sm btn-danger" onclick="return confirmar('borrar la orden de pago',this,event);"><i class="fa fa-trash-alt "></i>&nbsp;&nbsp;&nbsp;Borrar</a>&nbsp;&nbsp;&nbsp;
                                                            @endif
                                                          @else
                                                            PAGADA
                                                          @endif
                                                        </td>
                                                      </tr>
                                                      @endforeach
                                                    </tbody>
                                                  </table>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <!-- /.card -->
                                    </div>
                                </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal pagar orden-->
        <div class="modal fade" id="pagarorden" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <form role="form" id="form_modal" method="post" action="{{ route('compras.pagarorden') }}">
                @csrf
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Pagar</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="redirect_url" value="{{$_SERVER["REQUEST_URI"]}}" class="form-control" id="redirect_url" required>
                        <input type="hidden" name="id_remito" class="form-control" id="id_remito" required>
                        <input type="hidden" name="id_pedido" value="{{$pedido->id_pedido}}" class="form-control" id="id_pedido" required>
                        <input type="hidden" name="id_user" value="{{$pedido->user_id_user}}" class="form-control" id="id_user" required>
                        <input type="hidden" name="id_orden" class="form-control" id="id_orden" required>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="cuenta_id">Cuenta</label>
                                <select name="cuenta_id" class="form-control" id="cuenta_id" required>
                                <option value="">Seleccione</option>
                                @foreach($cuentas as $cuenta)
                                    <option value="{{$cuenta->id_medio}}">{{$cuenta->nombre}}</option>
                                @endforeach
                                </select>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="monto">Monto</label>
                                <input type="number" step="0.01" min="0" name="monto" class="form-control" id="monto" readonly>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="descripcion">Observaciones</label>
                                <textarea name="descripcion" class="form-control" id="descripcion"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-sm btn-success">Aceptar</button>
                    </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.tab-pane -->
        <!-- Modal orden pago-->
        <div class="modal fade" id="ordenPago" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <form role="form" id="form_modal" method="post" action="{{ route('compras.addordenpago') }}" enctype="multipart/form-data">
                @csrf
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Nueva orden de pago</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id_remito" value="{{$pedido->id_remito}}" class="form-control" id="id_remito" required>
                        <input type="hidden" name="id_pedido" value="{{$pedido->id_pedido}}" class="form-control" id="id_pedido" required>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="tipo">Medio de pago</label>
                                <select name="tipo" class="form-control" id="tipo" onchange="cambiarInfo(this.value)">
                                <option value="Efectivo">Efectivo</option>
                                <option value="Transferencia">Transferencia</option>
                                <option value="Cheque">Cheque</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="monto">Monto</label>
                                <input type="number" step="0.01" min="0" max="{{$pedido->monto_con_desc - $pedido->monto_pagos}}" name="monto" class="form-control" id="monto" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="fecha_emision">Fecha de generación</label>
                                <input type="date" value="<?php echo date("Y-m-d");?>" name="fecha_emision" class="form-control" id="fecha_emision" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="fecha_posible_pago">Fecha pago</label>
                                <input type="date" name="fecha_posible_pago" class="form-control" id="fecha_posible_pago" required>
                            </div>
                        </div>
                        <div class="row" id="div_cheque" hidden>
                            <div class="form-group col-sm-6">
                                <label for="numero">Número</label>
                                <input type="text" name="numero" class="form-control" id="numero">
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="ruta_imagen">Imagen</label><br>
                                <input type="file" name="ruta_imagen" class="" id="ruta_imagen">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="observaciones">Observaciones</label>
                                <textarea name="observaciones" class="form-control" id="observaciones"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-sm btn-success">Generar</button>
                    </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- Modal ver orden pago-->
        <div class="modal fade" id="verOrdenPago" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Ver orden de pago</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="txt_tipo">Medio de pago</label>
                            <p id="txt_tipo"></p>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="txt_monto">Monto</label>
                            <p id="txt_monto"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="txt_fecha_emision">Fecha de generación</label>
                            <p id="txt_fecha_emision"></p>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="txt_fecha_posible_pago">Fecha pago</label>
                            <p id="txt_fecha_posible_pago"></p>
                        </div>
                    </div>
                    <div class="row" id="div_cheque_ver" hidden>
                        <div class="form-group col-sm-6">
                            <label for="txt_numero">Número</label>
                            <p id="txt_numero"></p>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="txt_ruta_imagen">Imagen</label><br>
                            <p id="txt_ruta_imagen"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="txt_observaciones">Observaciones</label>
                            <p id="txt_observaciones"></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
                </div>
            </div>
        </div>
        <!-- Modal cobrar-->
        <div class="modal fade" id="pagar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <form role="form" id="form_modal" method="post" action="{{ route('compras.pagar') }}">
                @csrf
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Pagar</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="redirect_url" value="{{$_SERVER["REQUEST_URI"]}}" class="form-control" id="redirect_url" required>
                        <input type="hidden" name="id_pedido" value="{{$pedido->id_pedido}}" class="form-control" id="id_pedido" required>
                        <input type="hidden" name="id_remito" value="{{$pedido->id_remito}}" class="form-control" id="id_remito" required>
                        <input type="hidden" name="monto_total" value="{{$pedido->monto_con_desc - $pedido->monto_pagos}}" class="form-control" id="monto_total" required>
                        <input type="hidden" name="id_user" value="{{$pedido->user_id_user}}" class="form-control" id="id_user" required>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="cuenta_id">Cuenta</label>
                                <select name="cuenta_id" class="form-control" id="cuenta_id" required>
                                <option value="">Seleccione</option>
                                @foreach($cuentas as $cuenta)
                                    <option value="{{$cuenta->id_medio}}">{{$cuenta->nombre}}</option>
                                @endforeach
                                </select>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="monto">Monto {{env('MANEJO_DOLARES', false) ? 'en pesos': ''}}</label>
                                <input type="number" step="0.01" min="0" name="monto" onchange="actualizarMontos();" class="form-control" id="monto" required>
                            </div>
                        </div>
                        @if(env('MANEJO_DOLARES', false))
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="conversion_dolar">Conversion dolar</label>
                                <input type="number" step="0.01" min="0" name="conversion_dolar" {{env('MANEJO_DOLARES', false) ? "onchange=actualizarMontos();": ''}} class="form-control" id="conversion_dolar" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="monto_dolares">Monto en dolares</label>
                                <input type="number" step="0.01" min="0" readonly name="monto_dolares" class="form-control" id="monto_dolares" required>
                            </div>
                        </div>
                        @endif
                        <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="descripcion">Descripción</label>
                            <textarea name="descripcion" class="form-control" id="descripcion"></textarea>
                        </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-sm btn-success" onclick="return aceptar(event);">Aceptar</button>
                    </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- Modal archivo-->
        <div class="modal fade" id="modalArchivo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <form role="form" id="form_modal" method="post" action="{{ route('compras.addarchivo', $pedido->id_pedido) }}" enctype="multipart/form-data">
                @csrf
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Adjuntar archivo</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <input type="file" name="archivo" id="archivo" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-sm btn-success">Aceptar</button>
                    </div>
                    </div>
                </div>
            </form>
        </div>
    </body>
@stop