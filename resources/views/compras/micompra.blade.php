@extends('adminlte::page')

@section('content')
<head>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Solicitud de devolución</h3>
                        <div class="card-tools">
                            <div class="input-group input-group-sm">
                                <div class="input-group-btn">
                                    <a href="{{ route('devoluciones.misdevoluciones') }}" class="btn btn-sm btn-info"><i class="fa fa-reply"></i>&nbsp;&nbsp;&nbsp;Mis solicitudes de devolución</a>&nbsp;&nbsp;&nbsp;
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <form class="form-horizontal">
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Estado</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$pedido->estado}}</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Direccion de entrega</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$pedido->direccion}} - {{$pedido->localidad}} ({{$pedido->provincia}})</p>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Productos</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <table class="table table-bordered">
                                        <thead>                  
                                            <tr>
                                            <th>Codigo</th>
                                            <th>Nombre</th>
                                            <th>Cantidad</th>
                                            <th>Costo unitario</th>
                                            <!-- <th>Costo total</th> -->
                                            <!-- <th>Descuento</th> -->
                                            <th>Costo final</th>
                                            <!-- <th>Estado</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @php 
                                        $total_sin_descuento = 0.00;
                                        $total_con_descuento = 0.00;
                                        @endphp
                                        @foreach($productos as $producto)
                                        @php 
                                        $total_sin_descuento = $total_sin_descuento + ($producto->cantidad * $producto->costo_unitario);
                                        $total_con_descuento = $total_con_descuento + ($producto->cantidad * $producto->costo_unitario)*(1-($producto->descuento/100));
                                        @endphp
                                            <tr>
                                            <td>{{$producto->codigo}}</td>
                                            <td>{{$producto->nombre}}</td>
                                            <td>{{$producto->cantidad}}</td>
                                            <td style="white-space: nowrap;">$ {{number_format($producto->costo_unitario*(1-($producto->descuento/100)),2,",",".")}}</td>
                                            <td style="white-space: nowrap;">$ {{number_format(($producto->cantidad * $producto->costo_unitario)*(1-($producto->descuento/100)),2,",",".")}}</td>
                                            </tr>
                                            @endforeach
                                            <tr>
                                            <td colspan="4"><strong>TOTALES</strong></td>
                                            <td style="white-space: nowrap;"><strong>$ {{number_format(($total_con_descuento),2,",",".")}}</strong></td>
                                            </tr>
                                        </tbody>
                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.tab-pane -->
    </body>
@stop