@extends('adminlte::page')

@section('content')
<div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Reportes de compras</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="quickForm" method="POST" action="{{ route('compras.getreporte') }}">
                @csrf
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="fecha_desde">Fecha desde</label>
                      <input type="date" name="fecha_desde" class="form-control" id="fecha_desde" value="<?= date("Y-m-d",strtotime(date('Y-m-d')."- 2 month"))?>" required>
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="fecha_hasta">Fecha hasta</label>
                      <input type="date" name="fecha_hasta" class="form-control" id="fecha_hasta" value="<?= date("Y-m-d")?>" required>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Generar reporte</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
@stop
