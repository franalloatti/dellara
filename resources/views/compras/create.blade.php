@extends('adminlte::page')

@section('content')
  <head>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
  </head>
  <body>
    <script type="text/javascript">
      $(document).ready(function(){
        // $(window).keydown(function(event){
        //   if(event.keyCode == 13) {
        //     event.preventDefault();
        //     return false;
        //   }
        // });
        $('.js-example-basic-single').select2({matcher: matcher});
        var seleccion_anterior = '';
        $('#user_id_user').on('change', function(e){
          if(this.value != seleccion_anterior && $("#tablaproductos tr").length > 0){
            swal({
              title: 'Esta seguro/a de cambiar de proveedor?',
              text: 'Si lo hace se eliminarán los productos de la compra',
              icon: 'warning',
              buttons: ["Cancelar", "Confirmar"],
            }).then(function(value) {
                if (value) {
                  $("#tablaproductos").html('');
                  $('#user_id_user').val(e.target.value).trigger('change');
                }else{
                  $('#user_id_user').val(seleccion_anterior).trigger('change');
                }
            });
          }else{
            seleccion_anterior = e.target.value;
            var user_id_user = e.target.value;
            $.get('direccionesproveedor/'+user_id_user, function(data){
              var direccion_id_direccion = '<option value="" disabled selected>Seleccionar</option>'
              for (var i=0; i<data.length;i++)
              direccion_id_direccion+='<option value="'+data[i].id_direccion+'">'+data[i].direccion+'</option>';
              $("#direccion_id_direccion").html(direccion_id_direccion);
            }).fail(response => {
              toastr.error(response.responseJSON.message);
            });
            document.getElementById('producto_id_producto').disabled = true;
            $.get('getproductos/'+user_id_user, function(data){
              var producto_id_producto = '<option value="" disabled selected>Seleccionar</option>'
              for (var i=0; i<data.length;i++){
                var costo_unitario = data[i].costo_unitario;
                producto_id_producto+='<option value="'+data[i].id_producto+'" iva="'+data[i].iva+'" codigo="'+data[i].codigo+'" nombre="'+data[i].nombre+'" valor_dolar="'+data[i].valor_dolar+'" costo_dolares="'+data[i].costo_dolares+'">'+data[i].codigo+' - '+data[i].nombre+'</option>';
              }
              document.getElementById('producto_id_producto').disabled = false;
              $("#producto_id_producto").html(producto_id_producto);
            }).fail(response => {
              toastr.error(response.responseJSON.message);
            });
          }
        });
      });
      function aceptar(e){
        e.preventDefault();
        var bandera = 0;
        var confirmacion = false;
        if($("#tablaproductos tr").length == 0){
          toastr.error('No se puede generar una compra vacía');
          return false;
        }else{
          $("#tablaproductos tr").each(function(index, tr) { 
              if($(this).find("input[name*='[cantidad]']").val() <= 0){
                bandera = 1;
                toastr.error('No se puede generar una compra con un item con cantidad 0 (cero)');
                return false;
              }
          }).promise().done( function() {
              if(!bandera){
                swal({
                  title: 'Esta seguro/a?',
                  text: 'Esta seguro/a que desea confirmar la compra?',
                  icon: 'warning',
                  buttons: ["Cancelar", "Confirmar"],
                }).then(function(value) {
                    if (value) {
                      $('#quickForm').submit();
                    }
                });
              }
          });
        }
      };
      function borrar(id_producto){
        $("#fila" + id_producto).remove();
        $('#producto_id_producto option').each(function(i,elem){
          var elid = parseInt($(elem).attr('value'));
          if(id_producto == elid){
            $(elem).attr('disabled',false); 
          }
        });   
        actualizarTotales();
      };
      function funcion(){
        var producto_seleccionado = $('#producto_id_producto').val();
        if(document.getElementById('items['+producto_seleccionado+'][id]') != null){
          toastr.error("El producto ya se encuentra en la compra");
          return false;
        }else{
          $("#producto_id_producto option").each(function(){
            if($(this).attr('value') != ''){
              if(producto_seleccionado == $(this).attr('value')){
                var el_precio = 0;
                var check_iva = $('#check_iva').prop('checked');
                var el_iva = parseInt($(this).attr('iva'));
                switch (el_iva) {
                  case 3:
                    el_iva = '0';
                    break;
                  case 4:
                    el_iva = '10.5';
                    break;
                  case 5:
                    el_iva = '21';
                    break;
                  case 6:
                    el_iva = '27';
                    break;
                  case 8:
                    el_iva = '5';
                    break;
                  case 9:
                    el_iva = '2.5';
                    break;
                  default:
                    el_iva = '0';
                    break;
                }
                var iva_actual = '';
                if(check_iva){
                  iva_actual = el_iva;
                }else{
                  iva_actual = 0;
                }
                el_precio = $(this).attr('costo_dolares');
                var texto = '<tr id="fila'+$(this).attr('value')+'">'
                +'  <input name="items['+$(this).attr('value')+'][id]" value="'+$(this).attr('value')+'" type="hidden" class="form-control" id="items['+$(this).attr('value')+'][id]">'
                +'  <input name="items['+$(this).attr('value')+'][iva_producto]" value="'+el_iva+'" type="hidden" class="form-control" id="items['+$(this).attr('value')+'][iva_producto]">'
                +'  <input name="items['+$(this).attr('value')+'][iva]" value="'+iva_actual+'" type="hidden" class="form-control" id="items['+$(this).attr('value')+'][iva]">'
                +'  <td>'+$(this).attr('nombre')+'</td>'
                +'  <td id="iva_txt">'+iva_actual+'%</td>'
                +'  <td><input name="items['+$(this).attr('value')+'][costo_unitario]" value="'+parseFloat(el_precio).toFixed(2)+'" onchange="return actualizar('+$(this).attr('value')+')" type="number" class="form-control" id="items['+$(this).attr('value')+'][costo_unitario]"></td>'
                +'  <td><input name="items['+$(this).attr('value')+'][cantidad]" type="number" value="0" onchange="return actualizar('+$(this).attr('value')+')" class="form-control" id="items['+$(this).attr('value')+'][cantidad]"></td>'
                +'  <td><input name="items['+$(this).attr('value')+'][precio_final_sin_iva]" readonly value="0" type="number" class="form-control" id="items['+$(this).attr('value')+'][precio_final_sin_iva]"></td>'
                +'  <td><input name="items['+$(this).attr('value')+'][precio_final]" readonly value="0" type="number" class="form-control" id="items['+$(this).attr('value')+'][precio_final]"></td>'
                +'  <td><button onclick="return borrar('+$(this).attr('value')+');" class="btn btn-danger">Eliminar</button></td>'
                +'</tr>'
                $('#tablaproductos').append(texto);
                return;
              }
            }
          });
          $('#producto_id_producto').val($('#producto_id_producto option:eq(0)').val()).trigger('change');
          actualizarTotales();
          return false;
        }
      };
      function actualizarTotales(){
        var cantidadItems = 0;
        var precio_final = 0;
        $("#tablaproductos tr").each(function(index, tr) { 
          cantidadItems = cantidadItems + 1;
          precio_final = parseFloat(precio_final) + parseFloat($(this).find("input[name*='[precio_final]']").val());
        });
        document.getElementById('precio_final_final').innerHTML = '$ ' + parseFloat(precio_final).toFixed(2);
      };
      function actualizar(id_producto){
        var el_iva = document.getElementById('items['+id_producto+'][iva]').value;
        var costo_unitario = document.getElementById('items['+id_producto+'][costo_unitario]').value;
        var cantidad = document.getElementById('items['+id_producto+'][cantidad]').value;
        document.getElementById('items['+id_producto+'][precio_final_sin_iva]').value = parseFloat(costo_unitario*cantidad).toFixed(2);
        document.getElementById('items['+id_producto+'][precio_final]').value = parseFloat(costo_unitario*cantidad*(1.0+(parseFloat(el_iva)/100.0))).toFixed(2);
        actualizarTotales();
      };
      function cambiarIva(checked){
        $("#tablaproductos tr").each(function(index, tr) { 
          if(checked){
            var iva_producto = $(this).find("input[name*='[iva_producto]']").val();
            $(this).find("input[name*='[iva]']").val(iva_producto);
            $(this).find("#iva_txt").html(iva_producto+'%');
          }else{
            var el_iva = '0';
            $(this).find("input[name*='[iva]']").val(0);
            var txt = $(this).find("#iva_txt");
            txt.html(el_iva+'%');
          }
          actualizar($(this).find("input[name*='[id]']").val());
        });
      }
    </script>
    <style>
    .select2-container .select2-selection--single{
      height: auto !important;
    }
    .select2-container {
      width: 100% !important;
    }
    </style>
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Compra <small>Crear</small></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="quickForm" method="post" action="{{ route('compras.guardar') }}">
                @csrf
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="user_id_user">Proveedor</label>
                      <select name="user_id_user" class="form-control js-example-basic-single" id="user_id_user" required>
                        <option value="" disabled selected>Seleccionar</option>
                        @foreach($proveedores as $proveedor)
                        <option value="{{$proveedor->id_user}}">{{$proveedor->name}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="fecha_estimada_recepcion">Fecha estimada de recepción</label>
                      <input type="date" name="fecha_estimada_recepcion" class="form-control" id="fecha_estimada_recepcion" required>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="producto_id_producto">Productos</label>
                      <select name="producto_id_producto" disabled class="form-control js-example-basic-single" id="producto_id_producto">
                        <option value="" disabled selected>Seleccionar</option>
                      </select>
                    </div>
                    <div class="form-group col-sm-2" style="align-self: flex-end;">
                      <button type="button" onclick="return funcion();" id="agregar" name="agregar" class="form-control btn btn-info">Agregar</button>
                    </div>
                    <div class="form-group col-sm-2" style="align-self: flex-end;">
                      <input id='check_iva' name='check_iva' type="checkbox" data-toggle="toggle" data-on="Con iva" data-off="Sin iva" onchange="cambiarIva(this.checked);">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-12">
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>Insumo</th>
                            <th>Iva</th>
                            <th>Costo unitario</th>
                            <th>Cantidad</th>
                            <th>Costo</th>
                            <th>Costo con iva</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody id="tablaproductos">
                        </tbody>
                        <tbody>
                            <th colspan="5">Totales</th>
                            <th id="precio_final_final" name="precio_final_final">0</th>
                            <th></th>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <div class="row">
                        <div class="col-md-12">
                            <label>Observaciones:</label>
                            <textarea name="descripcion" rows="5" class="form-control" id="descripcion" placeholder="Ingresar observaciones internas..."></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="form-group col-sm-6">
                      <div class="row">
                        <div class="col-md-12">
                            <label>Observaciones públicas:</label>
                            <textarea name="observaciones_publicas" rows="5" class="form-control" id="observaciones_publicas" placeholder="Ingresar observaciones públicas..."></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-success" onclick="aceptar(event);">Crear</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
      <div class="modal fade" id="addDireccion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Nueva dirección</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="direccion">Dirección</label>
                            <input type="text" name="direccion" class="form-control" id="direccion" placeholder="Ingresar dirección" required>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="observaciones">Observaciones</label>
                            <input type="text" name="observaciones" class="form-control" id="observaciones" placeholder="Ingresar observaciones">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="provincia">Provincia</label>
                            <input type="text" name="provincia" class="form-control" id="provincia" placeholder="Ingresar provincia" required>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="localidad">Localidad</label>
                            <input type="text" name="localidad" class="form-control" id="localidad" placeholder="Ingresar localidad" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="codigo_postal">Código postal</label>
                            <input type="text" name="codigo_postal" class="form-control" id="codigo_postal" placeholder="Ingresar código postal" required>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="telefono">Teléfono</label>
                            <input type="text" name="telefono" class="form-control" id="telefono" placeholder="Ingresar teléfono">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-sm btn-success" onclick="return functionAddDireccion();">Agregar</button>
                </div>
            </div>
        </div>
      </div>
  </body>
</html>
@stop
