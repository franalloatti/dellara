@extends('adminlte::page')

@section('content')
  <head>
  </head>
  <body>
      <script>
        function cargardatosorden(orden){
          $('#monto').val(orden.monto);
          $('#id_orden').val(orden.id_orden_pago);id_pedido
          $('#id_remito').val(orden.remito_id_remito);
          $('#id_pedido').val(orden.id_pedido);
          $('#descripcion').val(orden.observaciones);
          $('#id_user').val(orden.id_user);
          console.log(orden);
        }
        function cargardatosverorden(orden){
            $('#txt_tipo').html(orden.tipo);
            $('#txt_monto').html(orden.monto);
            $('#txt_fecha_emision').html(orden.fecha_emision);
            $('#txt_fecha_posible_pago').html(orden.fecha_efectivo_pago != null ? orden.fecha_efectivo_pago : orden.fecha_posible_pago);
            $('#txt_observaciones').html(orden.observaciones);
            if(orden.tipo == 'Cheque'){
                document.getElementById('div_cheque_ver').hidden = false;
                $('#txt_numero').html(orden.numero);
                if(orden.ruta_imagen != ''){
                    $('#txt_ruta_imagen').html("<a href='/images/ordenes_pago/"+orden.ruta_imagen+"' target='_blank'>Ver imagen</a>");
                }
            }else{
                document.getElementById('div_cheque_ver').hidden = true;
            }
            console.log(orden);
        }
      </script>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Ordenes de pago</h3>
                <div class="card-tools">
                  <form role="form" action="/admin/compras/ordenespagos" method="POST">
                    @csrf
                    <div class="input-group input-group-sm">
                      <input type="date" id="fecha_desde" title="Fecha desde" name="fecha_desde" value="{{$fecha_desde ? $fecha_desde : ''}}" class="form-control pull-right" placeholder="Buscar">
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <input type="date" id="fecha_hasta" title="Fecha hasta" name="fecha_hasta" value="{{$fecha_hasta ? $fecha_hasta : ''}}" class="form-control pull-right" placeholder="Buscar">
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <select name="user_id" class="form-control" id="user_id">
                          <option value="">Proveedor</option>
                          @foreach($proveedores as $proveedor)
                          @if($proveedor->id_user == $proveedorSelected)
                              <option value="{{$proveedor->id_user}}" selected>{{$proveedor->name}}</option>
                          @else
                              <option value="{{$proveedor->id_user}}">{{$proveedor->name}}</option>
                          @endif
                          @endforeach
                      </select>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <select name="tipo" class="form-control" id="tipo">
                        <option value="">Tipo</option>
                        <option value="Efectivo" {{$tipoSelected == 'Efectivo' ? 'selected': ''}}>Efectivo</option>
                        <option value="Transferencia" {{$tipoSelected == 'Transferencia' ? 'selected': ''}}>Transferencia</option>
                        <option value="Cheque" {{$tipoSelected == 'Cheque' ? 'selected': ''}}>Cheque</option>
                      </select>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <select name="estado_id" class="form-control" id="estado_id">
                          <option value="0" {{$estadoSelected == 0 ? 'selected' : ''}}>Sin pagar</option>
                          <option value="1" {{$estadoSelected == 1 ? 'selected' : ''}}>Pagadas</option>
                          <option value="-" {{$estadoSelected == "-" ? 'selected' : ''}}>Todas</option>
                      </select>
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      <div class="input-group-btn">
                        <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <h3>Monto: $ {{number_format($monto_total->monto_total,2,",",".")}}</h3>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th>Fecha de pago</th>
                      <th>Tipo</th>
                      <th>Proveedor</th>
                      <th>Nro. orden</th>
                      <th>Nro. cheque</th>
                      <th>Monto</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($ordenes as $orden)
                    <tr>
                      <td>{{date('d/m/Y', strtotime($orden->fecha_efectivo_pago ? $orden->fecha_efectivo_pago : $orden->fecha_posible_pago))}}</td>
                      <td>{{$orden->tipo}}</td>
                      <td>{{$orden->proveedor}}</td>
                      <td>{{substr('00000'.$orden->id_orden_pago, -5)}}</td>
                      <td>{{$orden->numero}}</td>
                      <td align="right" style="white-space: nowrap;">$ {{number_format($orden->monto,2,",",".")}}</td>
                      <td>
                        <a href='#' data-toggle="modal" data-target="#verOrdenPago" onclick="cargardatosverorden({{json_encode($orden)}})" class="btn btn-sm btn-success"><i class="fa fa-file "></i>&nbsp;&nbsp;&nbsp;Ver</a>&nbsp;&nbsp;&nbsp;
                        @if($orden->pagado == 0)
                        <a href='#' data-toggle="modal" data-target="#pagarorden" onclick="cargardatosorden({{json_encode($orden)}})" class="btn btn-sm btn-secondary"><i class="fa fa-check"></i>&nbsp;&nbsp;&nbsp;Pagar</a>&nbsp;&nbsp;&nbsp;
                        <a href="{{ route('compras.deleteordenpago', [$orden->id_orden_pago, "ordenespagos",null]) }}" class="btn btn-sm btn-danger" onclick="return confirmar('borrar la orden de pago',this,event);"><i class="fa fa-trash-alt "></i>&nbsp;&nbsp;&nbsp;Borrar</a>&nbsp;&nbsp;&nbsp;
                        @else
                          PAGADA
                        @endif
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                    {{ $ordenes->appends(['fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta, 'tipoSelected' => $tipoSelected,
                          'estado_id' => $estadoSelected, 'user_id' => $proveedorSelected])->links() }}
              </div>
            </div>
            <!-- /.card -->
            </div>
          </div>
        </div>
        
        <!-- Modal ver orden pago-->
        <div class="modal fade" id="verOrdenPago" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Ver orden de pago</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <div class="row">
                      <div class="form-group col-sm-6">
                          <label for="txt_tipo">Medio de pago</label>
                          <p id="txt_tipo"></p>
                      </div>
                      <div class="form-group col-sm-6">
                          <label for="txt_monto">Monto</label>
                          <p id="txt_monto"></p>
                      </div>
                  </div>
                  <div class="row">
                      <div class="form-group col-sm-6">
                          <label for="txt_fecha_emision">Fecha de generación</label>
                          <p id="txt_fecha_emision"></p>
                      </div>
                      <div class="form-group col-sm-6">
                          <label for="txt_fecha_posible_pago">Fecha pago</label>
                          <p id="txt_fecha_posible_pago"></p>
                      </div>
                  </div>
                  <div class="row" id="div_cheque_ver" hidden>
                      <div class="form-group col-sm-6">
                          <label for="txt_numero">Número</label>
                          <p id="txt_numero"></p>
                      </div>
                      <div class="form-group col-sm-6">
                          <label for="txt_ruta_imagen">Imagen</label><br>
                          <p id="txt_ruta_imagen"></p>
                      </div>
                  </div>
                  <div class="row">
                      <div class="form-group col-sm-12">
                          <label for="txt_observaciones">Observaciones</label>
                          <p id="txt_observaciones"></p>
                      </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal pagar orden-->
        <div class="modal fade" id="pagarorden" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <form role="form" id="form_modal" method="post" action="{{ route('compras.pagarorden') }}">
                @csrf
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Pagar</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="redirect_url" value="{{$_SERVER["REQUEST_URI"]}}" class="form-control" id="redirect_url" required>
                        <input type="hidden" name="id_remito" class="form-control" id="id_remito" required>
                        <input type="hidden" name="id_pedido" class="form-control" id="id_pedido" required>
                        <input type="hidden" name="id_user" class="form-control" id="id_user" required>
                        <input type="hidden" name="id_orden" class="form-control" id="id_orden" required>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="cuenta_id">Cuenta</label>
                                <select name="cuenta_id" class="form-control" id="cuenta_id" required>
                                <option value="">Seleccione</option>
                                @foreach($cuentas as $cuenta)
                                    <option value="{{$cuenta->id_medio}}">{{$cuenta->nombre}}</option>
                                @endforeach
                                </select>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="monto">Monto</label>
                                <input type="number" step="0.01" min="0" name="monto" class="form-control" id="monto" readonly>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="descripcion">Observaciones</label>
                                <textarea name="descripcion" class="form-control" id="descripcion"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-sm btn-success">Aceptar</button>
                    </div>
                    </div>
                </div>
            </form>
        </div>
      </body>
        @stop
