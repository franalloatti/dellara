@extends('adminlte::page')

@section('content')
<div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Insumo <small>Editar</small></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="quickForm" method="post" action="{{ route('insumos.actualizar', [$insumo->id_producto]) }}">
                @csrf
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="codigo">Código</label>
                      <input type="text" name="codigo" class="form-control" id="codigo" value="{{$insumo->codigo}}" placeholder="Ingresar código" required>
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="nombre">Nombre</label>
                      <input type="text" name="nombre" class="form-control" id="nombre" value="{{$insumo->nombre}}" placeholder="Ingresar nombre" required>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="descripcion">Descripción</label>
                      <input type="text" name="descripcion" class="form-control" id="descripcion" value="{{$insumo->descripcion}}" placeholder="Ingresar descripción">
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="stock_seguridad">Nivel de Stock seguridad</label>
                      <input type="number" name="stock_seguridad" class="form-control" id="stock_seguridad" value="{{$insumo->stock_seguridad}}" placeholder="Ingresar nivel stock de seguridad">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="conversion">Conversión</label>
                      <input type="number" min="0.01" step="0.01" name="conversion" class="form-control" id="conversion" value="{{$insumo->conversion}}" placeholder="Ingresar conversión">
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="unidad">Unidad</label>
                      <input type="text" name="unidad" class="form-control" id="unidad" value="{{$insumo->unidad}}" placeholder="Ingresar unidad">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="rubro_id">Rubro</label>
                      <select name="rubro_id" class="form-control" id="rubro_id" required>
                        <option value="">Seleccionar</option>
                        @foreach($rubros as $rubro)
                          @if($rubro->id_rubro == $insumo->rubro)
                            <option value="{{$rubro->id_rubro}}" selected>{{$rubro->nombre}}</option>
                          @else
                            <option value="{{$rubro->id_rubro}}">{{$rubro->nombre}}</option>
                          @endif
                        @endforeach
                      </select>
                    </div>
                    <div class="form-group col-sm-6">
                      <div class="form-group">
                        <label for="iva">Iva</label>
                        <select name="iva" class="form-control" id="iva" required>
                          <option value="">Seleccionar</option>
                          <option value="3" {{$insumo->iva == 3 ? 'selected' : ""}}>0%</option>
                          <option value="4" {{$insumo->iva == 4 ? 'selected' : ""}}>10.5%</option>
                          <option value="5" {{$insumo->iva == 5 ? 'selected' : ""}}>21%</option>
                          <option value="6" {{$insumo->iva == 6 ? 'selected' : ""}}>27%</option>
                          <option value="8" {{$insumo->iva == 8 ? 'selected' : ""}}>5%</option>
                          <option value="9" {{$insumo->iva == 9 ? 'selected' : ""}}>2.5%</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Editar</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
@stop
