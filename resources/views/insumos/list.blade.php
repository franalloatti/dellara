@extends('adminlte::page')

@section('content')
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Insumos</h3>
                <div class="card-tools">
                  <form role="form" action="/admin/insumos/index" method="POST">
                    @csrf
                    <div class="input-group input-group-sm">
                      <select name="limit" class="form-control" id="limit">
                          <option value="20" <?php if($limit == 20) echo 'selected';?>>20 registros</option>
                          <option value="50" <?php if($limit == 50) echo 'selected';?>>50 registros</option>
                          <option value="100" <?php if($limit == 100) echo 'selected';?>>100 registros</option>
                      </select>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <input type="text" id="search" name="search" value="{{$search ? $search : ''}}" class="form-control pull-right" placeholder="Buscar">
                      <div class="input-group-btn">
                        <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th>Codigo</th>
                      <th>Nombre</th>
                      <th>Costo</th>
                      {{-- <th>Precio</th> --}}
                      <th>Stock</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($insumos as $insumo)
                    @php
                      $precio = ($insumo->costo * ($insumo->margen / 100) * ($insumo->valor_dolar));
                      $stock = $insumo->cantidad + $insumo->mov_destino + $insumo->mov_origen + $insumo->venta + $insumo->devolucion_venta + $insumo->compra + $insumo->devolucion_compra;
                    @endphp
                    <tr>
                      <td>{{$insumo->codigo}}</td>
                      <td>{{$insumo->nombre}}</td>
                      <td align="right" style="white-space: nowrap;">$ {{number_format($insumo->costo * ($insumo->valor_dolar),2,",",".")}}</td>
                      {{-- <td align="right" style="white-space: nowrap;">$ {{number_format($precio,2,",",".")}}</td> --}}
                      <td>{{number_format($stock*$insumo->conversion,2,",",".")}} {{$insumo->unidad}} ({{number_format($stock,2,",",".")}} u.)</td>
                      <td>
                        <a href="{{ route('insumos.ver', [$insumo->id_producto]) }}" class="btn btn-sm btn-success"><i class="fa fa-file "></i>&nbsp;&nbsp;&nbsp;Ver</a>&nbsp;&nbsp;&nbsp;
                        <a href="{{ route('insumos.editar', [$insumo->id_producto]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Editar</a>&nbsp;&nbsp;&nbsp;
                        <a href="{{ route('insumos.borrar', [$insumo->id_producto]) }}" class="btn btn-sm btn-danger" onclick="return confirmar('borrar el insumo',this,event);"><i class="fa fa-trash-alt "></i>&nbsp;&nbsp;&nbsp;Borrar</a>&nbsp;&nbsp;&nbsp;
                        <a href="{{ route('insumos.habilitarDeshabilitar', [$insumo->id_producto, $insumo->estado]) }}" class="btn btn-sm btn-{{$insumo->estado ? 'secondary' : 'primary'}}"><i class="fa {{$insumo->estado ? 'fa-times' : 'fa-check'}}"></i>&nbsp;&nbsp;&nbsp;{{$insumo->estado ? 'Deshabilitar' : 'Habilitar'}}</a>&nbsp;&nbsp;&nbsp;
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                    {{ $insumos->appends(['search' => $search, 'limit' => $limit])->links() }}
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    @stop
