@extends('adminlte::page')

@section('content')
<div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Insumo <small>Crear</small></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="quickForm" method="post" action="{{ route('insumos.guardar') }}">
                @csrf
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="codigo">Código</label>
                      <input type="text" name="codigo" class="form-control" id="codigo" placeholder="Ingresar código" required>
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="nombre">Nombre</label>
                      <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Ingresar nombre" required>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="descripcion">Descripción</label>
                      <input type="text" name="descripcion" class="form-control" id="descripcion" placeholder="Ingresar descripción">
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="stock_seguridad">Nivel de stock seguridad</label>
                      <input type="number" name="stock_seguridad" class="form-control" id="stock_seguridad" placeholder="Ingresar nivel stock de seguridad">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="conversion">Conversión</label>
                      <input type="number" min="0.01" step="0.01" name="conversion" class="form-control" id="conversion" placeholder="Ingresar conversión">
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="unidad">Unidad</label>
                      <input type="text" name="unidad" class="form-control" id="unidad" placeholder="Ingresar unidad">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="rubro_id">Rubro</label>
                      <select name="rubro_id" class="form-control" id="rubro_id" required>
                        <option value="">Seleccionar</option>
                        @foreach($rubros as $rubro)
                        <option value="{{$rubro->id_rubro}}">{{$rubro->nombre}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="form-group col-sm-6">
                      <div class="form-group">
                        <label for="iva">IVA</label>
                        <select name="iva" class="form-control" id="iva" requir ed>
                          <option value="">Seleccionar</option>
                          <option value="5">21%</option>
                          <option value="4">10.5%</option>
                          <option value="6">27%</option>
                          <option value="8">5%</option>
                          <option value="9">2.5%</option>
                          <option value="3">0%</option>
                          <option value="2">No gravado</option>
                          <option value="1">Exento</option>
                        </select>
                      </div>
                    </div>    
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Crear</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
@stop
