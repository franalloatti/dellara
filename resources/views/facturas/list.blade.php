@extends('adminlte::page')

@section('content')
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Facturas</h3>
              </div>
              <!-- /.card-header -->
              
              <div class="card-body">
                <button class="btn btn-secondary mb-2" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    Filtrar por fecha
                  </button>
                
                <div class="collapse pt-2" id="collapseExample">
                  <div class="card card-body">
                      <div class="row">
                          <div class="col">
                              Desde: <input class="form-control" type="date" id="txtMin" /><br />
                          </div>
                          <div class="col">
                              Hasta: <input class="form-control" type="date" id="txtMax" /><br />
                          </div>
                          <div class="col mt-4 text-center">
                              <button id="btnGo" class="btn btn-primary" type="button">Filtrar</button>
                          </div>
                      </div>
                  </div>
                </div>



                <table id="facturasTable" class="table table-bordered">
                  <thead>                  
                    <tr>
                        <th>Fecha</th>
                        <th>Cliente</th>
                        <th>N°</th>
                        <th>Monto $</th>
                        <td></td>
                        <td></td>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
            </div>
          </div>
        </div>
    </div>
@endsection



@section('css')
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css">

@endsection
@section('js')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js"></script>
    
   

    <script>
        $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {                
                var valid = true;
                var min = moment($("#txtMin").val());
                if (!min.isValid()) { min = null; }

                var max = moment($("#txtMax").val());
                if (!max.isValid()) { max = null; }

                if (min === null && max === null) {
                    // no filter applied or no date columns
                    valid = true;
                }
                else {

                    $.each(settings.aoColumns, function (i, col) {
                        if (col.type == "date") {
                            var cDate = moment(data[i]);
                        
                            if (cDate.isValid()) {
                                if (max !== null && max.isBefore(cDate)) {
                                    valid = false;
                                }
                                if (min !== null && cDate.isBefore(min)) {
                                    valid = false;
                                }
                            }
                            else {
                                valid = false;
                            }
                        }
                    });
                }
                return valid;
        });
        $(document).ready(function() {

            $("#btnGo").click(function () {
                $('#facturasTable').DataTable().draw();
            });


            var table = $('#facturasTable').DataTable({
                "ajax": "factura/getFacturas",
                "columnDefs": [ 
                    {
                        "targets": -1,
                        "data": null,
                        "className": "text-center",
                        "width": "4%",
                        "defaultContent": "<button class='btn btn-link button-print'><i class='fas fa-print'></i></button>",
                        "searchable": false,
                        "orderable": false
                    }, 
                    {
                        "targets": -2,
                        "data": null,
                        "className": "text-center",
                        "width": "4%",
                        "defaultContent": "<button class='btn btn-link button-eye'><i class='fas fa-eye'></i></button>",
                        "searchable": false,
                        "orderable": false
                    } 
                ],
                "columns": [
                    {data: 'fecha', type:"date"},
                    {data: 'cliente'},
                    {data: 'numero'},
                    {data: 'monto'},
                    {},
                    {}
                ],
                responsive: true,
                autoWidth: false,
               "language": {
                "lengthMenu": "Ver: "
                               +`<select class="custom-select custom-select-sm form-control form-control-sm">
                                <option value = '10'>10</option>
                                <option value = '25'>25</option>
                                <option value = '50'>50</option>
                                <option value = '100'>100</option>
                                <option value = '-1'>All</option>
                                </select>`
                               +" resultados",
                "zeroRecords": "No se encontraron resultados",
                "info": "Mostrando _PAGE_ de _PAGES_",
                "infoEmpty": "No records available",
                "infoFiltered": "(Filtrado de _MAX_ registros totales)",
                "search": "Buscar:",
                "paginate":{
                    "next":"Siguiente",
                    "previous":"Anterior"
                }
               }
            });

        $('#facturasTable tbody').on( 'click', '.button-eye', function () {
            var data = table.row($(this).parents('tr')).data();
            window.open("/admin/factura/ver/" + data.id_factura, "_blank");
        } );

        $('#facturasTable tbody').on( 'click', '.button-print', function () {
            var data = table.row($(this).parents('tr')).data();
            window.open("/admin/factura/imprimir/" + data.id_factura, "_blank");
        } );
    });


        
    </script>
    




@endsection