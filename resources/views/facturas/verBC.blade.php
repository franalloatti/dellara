<!DOCTYPE html>
<html>
    <head>
        <title>Factura {{$factura->CbteDesde}}</title>
        <meta charset="UTF-8">
        <style>
            *
            {
            margin:0;
            padding:0;
            font-family:Arial;
            font-size:10pt;
            color:#000;
            }
            #wrapperbill
            {
            width:180mm;
            margin:0 15mm;
            }
            .lineSmall{
            border: 0;
            color: #345092;
            background-color: #345092;
            height: 2px;
            }
            .lineLarge{
            width: 180mm;
            border: 0;
            color: #345092;
            background-color: #345092;
            height: 2px;
            margin-top: 0px;
            margin-bottom: 0px;
            }
        </style>
        {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> --}}
        {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> --}}
        {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"> --}}
        {{-- <link href="css/bootstrap.min.css" rel="stylesheet"> --}}    
        <!-- Styles -->
        {{-- <link href="/css/app.css" rel="stylesheet"> --}}
        <link type="text/css" href="/css/app.css" rel="stylesheet" />
    </head>
    <body>
        <div id="wrapperbill">
            <hr class="lineLarge"><br>
            <div class="row h-100 justify-content-center align-items-center">
                <h4>NO VALIDO CÓMO FACTURA</h4>
            </div>
            <div class="row mt-2">
                <div class="col-5">
                    <div class="row ml-1">
                        <b>{{$empresa->nombre}}</b>
                    </div>
                    <div class="row ml-1">{{$empresa->direccion}}</div>
                    <div class="row ml-1">{{$empresa->ciudad}} - {{$empresa->provincia}} - {{$empresa->pais}}</div>
                    <div class="row ml-1">Tel: {{$empresa->telefono}}</div>
                    <div class="row ml-1">{{$empresa->email}}</div>
                    <div class="row ml-1"><b>I.V.A.: {{$empresa->categoriaIVA}}</b></div>
                </div>
                <div class="col-3 d-flex ">
                    <div class="row pt-0 pb-0 mb-0 ml-4 align-self-start text-center" style=" background-color: rgb(52, 80, 146); color: white; font-size: 42px;">
                            {{$letra}}
                    </div>
                    <div  class="row align-self-center" style=" width: 87px;"><small><small>Codigo Nº {{$factura->CbteTipo}}</small></small></div>
                </div>
                <div class="col-4">
                    <div class="d-flex align-content-stretch flex-wrap">
                        <div class="ml-5 p-1">
                            <div class="row">
                                <h2> FACTURA </h2>
                            </div>
                            <div class="row mt-3">Número: {{$puntoVenta}} - {{$facturaNumero}}</div>
                            <div class="row">Fecha : {{$factura->CbteFch}}</div>
                            <div class="row mt-3"><b>CUIT:&nbsp;</b>{{$empresa->cuit}}</div>
                            <div class="row"><b>Ing. Brutos:&nbsp;</b>{{$empresa->iibb}}</div>
                            <div class="row"><b>Inicio de Act.:&nbsp;</b>{{$empresa->inicioActividades}}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col mt-1">
                <div class="row">
                    <b>Cliente: {{$cliente->name}}</b>
                </div>
                <div class="row">
                    Domicilio: {{$direccion->direccion}} 
                </div>
                <div class="row">
                    Ciudad: {{$direccion->localidad}} - {{$direccion->provincia}}
                </div>
                <div class="row">
                    Cond IVA: &nbsp;<b>
                        @switch($cliente->responsabilidad)
                            @case(1)
                                IVA Responsable Inscripto
                                @break
                            @case(2)
                                IVA Responsable no Inscripto
                                @break
                            @case(3)
                                IVA no Responsable
                                @break
                            @case(4)
                                IVA Sujeto Exento
                                @break
                            @case(5)
                                Consumidor Final
                                @break
                            @case(6)
                                Responsable Monotributo
                                @break
                            @case(7)
                                Sujeto no Categorizado
                                @break
                            @case(8)
                                Proveedor del Exterior
                                @break
                            @case(9)
                                Cliente del Exterior
                                @break
                            @case(10)
                                IVA Liberado – Ley Nº 19.640
                                @break
                            @case(11)
                                IVA Responsable Inscripto – Agente de Percepción
                                @break
                            @case(12)
                                Pequeño Contribuyente Eventual
                                @break
                            @case(13)
                                Monotributista Social
                                @break
                            @case(14)
                                Pequeño Contribuyente Eventual Social
                                @break
                            @default
                                
                        @endswitch
                    </b>
                </div>
                <div class="row">
                    CUIT: {{$cliente->cuit}}
                </div>
            </div>
            
            <hr class="lineLarge mt-2"><br>
        
            <div id="invoice_body">
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <th><b>N°</b></th>
                        <th><b>Codigo</b></th>
                        <th><b>Detalle</b></th>
                        <th><b>Cantidad</b></th>
                        <th><b>Precio Unitario</b></th>
                        <th><b>%Bon</b></th>
                        <th><b>Subtotal</b></th>
                    </tr>
                    @foreach ($tabla as $item)
                        <tr>
                            <td><b>{{ $loop->iteration }}</b></td>
                            <td><b>{{$item->codigo}}</b></td>
                            <td><b>{{$item->nombre}}</b></td>
                            <td><b>{{$item->cantidad}}</b></td>
                            <td><b>{{$item->precio_unitario}}</b></td>
                            <td><b>{{$item->descuento}}</b></td>
                            <td><b>{{$item->subTotalIva}}</b></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <br>
        <table class="table" cellpadding="2" cellspacing="2">
            <tbody>
            <tr>
                <td style="vertical-align: top; text-align: right;">
                    <span style="font-weight: bold;"><b> TOTAL $&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{$factura->ImpTotal}}</b></span>
                </td>
            </tr>
            </tbody>
        </table>
        <br>
        </div>
        <hr class="lineLarge"><br>
    </body>
    <footer>
            <div class= "row" >
                <div class= "col-2" >
                    {{$qr}}
                </div>
                <!-- <div class= "col-7" >
                    {{-- <img src="{{asset('images/'.$empresa->rutaLogo)}}" height="100" width="291"> --}}
                    <img src="/images/{{$empresa->rutaLogo}}" height="100" width="291">
                </div> -->
                <div class="col-2">
                    <div class="row ml-4">
                        <b>CAE:</b> {{$factura->CAE}}
                    </div>
                    <div class="row ml-4">
                        <b>Vto. CAE:</b> {{$factura->fechaCAE}}
                    </div>
                </div>
            </div>
            <br>           
            <hr class="lineLarge"></div>
            </div>
            <p>&nbsp;</p>
            </div>
    </footer>
</html>