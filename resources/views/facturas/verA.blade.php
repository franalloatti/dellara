<!DOCTYPE html>
<html>
    <head>
        <title>Factura {{$factura->CbteDesde}}</title>
        <meta charset="UTF-8">
        <style>
            *
            {
            margin:0;
            padding:0;
            font-family:Arial;
            font-size:10pt;
            color:#000;
            }
            #wrapperbill
            {
            width:180mm;
            margin:0 15mm;
            }
            .lineSmall{
            border: 0;
            color: #345092;
            background-color: #345092;
            height: 2px;
            }
            .lineLarge{
            width: 180mm;
            border: 0;
            color: #345092;
            background-color: #345092;
            height: 2px;
            margin-top: 0px;
            margin-bottom: 0px;
            }
        </style>
        {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> --}}
        {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> --}}
        {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"> --}}
        {{-- <link href="css/bootstrap.min.css" rel="stylesheet"> --}}    
        <!-- Styles -->
        {{-- <link href="/css/app.css" rel="stylesheet"> --}}
        <link type="text/css" href="/css/app.css" rel="stylesheet" />
    </head>
    <body>
        <div id="wrapperbill">
            <hr class="lineLarge"><br>
            <div class="row h-100 justify-content-center align-items-center">
                <h4>NO VALIDO CÓMO FACTURA</h4>
            </div>
            <div class="row mt-2">
                <div class="col-5">
                    <div class="row ml-1">
                        <b>{{$empresa->nombre}}</b>
                    </div>
                    <div class="row ml-1">{{$empresa->direccion}}</div>
                    <div class="row ml-1">{{$empresa->ciudad}} - {{$empresa->provincia}} - {{$empresa->pais}}</div>
                    <div class="row ml-1">Tel: {{$empresa->telefono}}</div>
                    <div class="row ml-1">{{$empresa->email}}</div>
                    <div class="row ml-1"><b>I.V.A.: {{$empresa->categoriaIVA}}</b></div>
                </div>
                <div class="col-3 d-flex ">
                    <div class="row pt-0 pb-0 mb-0 ml-4 align-self-start text-center" style=" background-color: rgb(52, 80, 146); color: white; font-size: 42px;">
                            {{$letra}}
                    </div>
                    <div  class="row align-self-center" style=" width: 87px;"><small><small>Codigo Nº {{$factura->CbteTipo}}</small></small></div>
                </div>
                <div class="col-4">
                    <div class="d-flex align-content-stretch flex-wrap">
                        <div class="ml-5 p-1">
                            <div class="row">
                                <h2> FACTURA </h2>
                            </div>
                            <div class="row mt-3">Número: {{$puntoVenta}} - {{$facturaNumero}}</div>
                            <div class="row">Fecha : {{$factura->CbteFch}}</div>
                            <div class="row mt-3"><b>CUIT:&nbsp;</b>{{$empresa->cuit}}</div>
                            <div class="row"><b>Ing. Brutos:&nbsp;</b>{{$empresa->iibb}}</div>
                            <div class="row"><b>Inicio de Act.:&nbsp;</b>{{$empresa->inicioActividades}}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col mt-1">
                <div class="row">
                    <b>Cliente: {{$cliente->name}}</b>
                </div>
                <div class="row">
                    Domicilio: {{$direccion->direccion}} 
                </div>
                <div class="row">
                    Ciudad: {{$direccion->localidad}} - {{$direccion->provincia}}
                </div>
                <div class="row">
                    Cond IVA: &nbsp;<b>
                        @switch($cliente->responsabilidad)
                            @case(1)
                                IVA Responsable Inscripto
                                @break
                            @case(2)
                                IVA Responsable no Inscripto
                                @break
                            @case(3)
                                IVA no Responsable
                                @break
                            @case(4)
                                IVA Sujeto Exento
                                @break
                            @case(5)
                                Consumidor Final
                                @break
                            @case(6)
                                Responsable Monotributo
                                @break
                            @case(7)
                                Sujeto no Categorizado
                                @break
                            @case(8)
                                Proveedor del Exterior
                                @break
                            @case(9)
                                Cliente del Exterior
                                @break
                            @case(10)
                                IVA Liberado – Ley Nº 19.640
                                @break
                            @case(11)
                                IVA Responsable Inscripto – Agente de Percepción
                                @break
                            @case(12)
                                Pequeño Contribuyente Eventual
                                @break
                            @case(13)
                                Monotributista Social
                                @break
                            @case(14)
                                Pequeño Contribuyente Eventual Social
                                @break
                            @default
                                
                        @endswitch
                    </b>
                </div>
                <div class="row">
                    CUIT: {{$cliente->cuit}}
                </div>
            </div>
            
            <hr class="lineLarge mt-2"><br>
        
            <div id="invoice_body">
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <th style= "text-align: center;"><b>N°</b></th>
                        <th style= "text-align: center;"><b>Codigo</b></th>
                        <th style= "text-align: center;"><b>Detalle</b></th>
                        <th style= "text-align: center;"><b>Cantidad</b></th>
                        <th style= "text-align: center;"><b>Precio Unitario</b></th>
                        <th style= "text-align: center;"><b>%Bon</b></th>
                        <th style= "text-align: center;"><b>Subtotal</b></th>
                        <th style= "text-align: center;"><b>IVA</b></th>
                        <th style= "text-align: center;"><b>Subtotal c/IVA</b></th>
                    </tr>
                    @foreach ($tabla as $item)
                        <tr>
                            <td style= "text-align: center;"><b>{{$loop->iteration}}</b></td>
                            <td style= "text-align: center;"><b>{{$item->codigo}}</b></td>
                            <td style= "text-align: center;"><b>{{$item->nombre}}</b></td>
                            <td style= "text-align: center;"><b>{{$item->cantidad}}</b></td>
                            <td style= "text-align: center;"><b>{{$item->precio_unitario}}</b></td>
                            <td style= "text-align: center;"><b>{{$item->descuento}}</b></td>
                            <td style= "text-align: center;"><b>{{$item->subTotal}}</b></td>
                            <td style= "text-align: center; width: 8%;">
                                @switch($item->iva)
                                    @case(1)
                                        <b>Exento</b>
                                        @break
                                    @case(2)
                                        <b>No gravado</b>
                                        @break
                                    @case(3)
                                        <b>0%</b>
                                        @break
                                    @case(4)
                                        <b>10.5%</b>
                                        @break
                                    @case(5)
                                        <b>21%</b>
                                        @break
                                    @case(6)
                                        <b>27%</b>    
                                        @break
                                    @case(8)
                                        <b>5%</b>
                                        @break
                                    @case(9)
                                        <b>2.5%</b>
                                        @break
                                    @default
                                @endswitch
                            </td>
                            <td><b>{{$item->subTotalIva}}</b></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <br>

            <table style="width: 60%;"  align="right" border="1" >
                <tbody>
                <tr style="height: 18px;">
                    <td style="width: 59%; height: 18px;"><strong>Importe Neto Gravado $</strong></td>
                    <td style="width: 41%; height: 18px; text-align: right;"><b>{{$factura->ImpNeto}}</b></td>
                </tr>
                @foreach ($alicuotas as $alicuota)
                    <tr style="height: 18px;">
                        <td style="width: 56.1047%; height: 18px;"><strong>{{$alicuota->ivaCategoria}}. $</strong></td>
                        <td style="width: 43.8953%; height: 18px; text-align: right;"><b>{{$alicuota->Importe}}</b></td>
                    </tr>
                @endforeach
                <tr style="height: 18px;">
                <td style="width: 56.1047%; height: 18px;"><strong>Importe Total $</strong></td>
                <td style="width: 43.8953%; height: 18px; text-align: right;"><b>{{$factura->ImpTotal}}</b></td>
                </tr>
                </tbody>
            </table>
        {{-- <table class="table" cellpadding="2" cellspacing="2">
            <tbody>
            <tr>
                <td style="vertical-align: top; text-align: right;">
                    <span style="font-weight: bold;">SUBTOTAL $&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{$factura->ImpNeto}}</span><br>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top; text-align: right;">
                    <span style="font-weight: bold;"><b> TOTAL $&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{$factura->ImpTotal}}</b></span>
                </td>
            </tr>
            </tbody>
        </table> --}}
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <br>
        </div>
        @if($cliente->responsabilidad==6)
            <hr class="lineLarge">
            <p>
                <small>El crédito fiscal discriminado en el prresente comprobante, sólo podrá ser computado a efectos del Régimen de Sostenimiento e Inclusión Fiscal para Pequeños Contribuyentes de la Ley N° 27.618'</small>
            </p>
        @endif
        @if($empresa->tipoFactA=="A CBU informado")
            <hr class="lineLarge">
            <p>
                <small><b>Operación sujeta a retención.</b>  CBU: {{$empresa->CBU}} </small>
            </p>
        @endif
        <hr class="lineLarge"><br>
    </body>
    <footer>
        <div class= "row" >
            <div class= "col-2" >
                {{$qr}}
            </div>
            <!-- <div class= "col-7" >
                {{-- <img src="{{asset('images/'.$empresa->rutaLogo)}}" height="100" width="291"> --}}
                <img src="/images/{{$empresa->rutaLogo}}" height="100" width="291">
            </div> -->
            <div class="col-2">
                <div class="row ml-4">
                    <b>CAE:</b> {{$factura->CAE}}
                </div>
                <div class="row ml-4">
                    <b>Vto. CAE:</b> {{$factura->fechaCAE}}
                </div>
            </div>
        </div>
        <br>           
        <hr class="lineLarge"></div>
        </div>
        <p>&nbsp;</p>
        </div>
    </footer>
</html>