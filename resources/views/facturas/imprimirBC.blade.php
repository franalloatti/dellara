<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Factura {{$factura->CbteDesde}}</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <style>

            #table2 {
              border-collapse: collapse;
              border-spacing: 10px;  
            }
            footer {
                position: fixed;
                /* left: 0; */
                bottom: 120;
                /* width: 100%;
                text-align: center; */ 
            }
            .footer2 {
                position: fixed;
                /* left: 0; */
                bottom: 200;
                /* height: 100px; */
                /* width: 100%;
                text-align: center; */ 
            }
            header {
                position: fixed;
                height: 50px;
                flex-shrink: 0;
            }
            main {
                /* position: static; */
                position: relative;
                top: 320px;
                /* min-height: 50px; */
                margin-bottom: 50px;
                clear: both;
                min-height: 100%;
            }
            .page_break { page-break-after: always; }
        </style>
         @php 
            $it = 0;
            $sub = 0; 
        @endphp
    </head>
    <body>
    <header>     
        <table id="table2">
            <tbody>
                <tr>
                    <td style="font-size:14px"><h3>{{$empresa->nombre}}</h3></td>
                    <td style="text-align: center; width: 200px;"><h3>{{$letra}}</h3></td>
                    <td style="text-align: right; width: 240px;"><h3>FACTURA</h3></td>
                </tr>
                <tr>
                    <td style="font-size:14px">{{$empresa->direccion}}</td>
                    <td style="text-align: center; width: 200px;"><small><small>Código N° {{$factura->CbteTipo}}</small></small></td>
                    <td style="text-align: right; width: 200px; font-size:14px">Número: {{$puntoVenta}} - {{$facturaNumero}}</td>
                </tr>
                <tr>
                    <td style="font-size:14px">{{$empresa->ciudad}} - {{$empresa->provincia}} - {{$empresa->pais}}</td>
                    <td style="text-align: center; width: 200px;"></td>
                    <td style="text-align: right; width: 200px; font-size:14px">Fecha : {{$factura->CbteFch}}</td>
                </tr>
                <tr>
                    <td style="font-size:14px">Tel: {{$empresa->telefono}}</td>
                    <td style="text-align: center; width: 200px;"></td>
                    <td style="text-align: right; width: 200px;"></td>
                </tr>
                <tr>
                    <td style="font-size:14px">{{$empresa->email}}</td>
                    <td style="text-align: center; width: 200px;"></td>
                    <td style="text-align: right; width: 200px; font-size:14px"><b>CUIT:&nbsp;</b>{{$empresa->cuit}}</td>
                </tr>
                <tr>
                    <td style="font-size:14px"><b>I.V.A.: {{$empresa->categoriaIVA}}</b></td>
                    <td style="text-align: center; width: 200px;"></td>
                    <td style="text-align: right; width: 200px; font-size:14px"><b>Ing. Brutos:&nbsp;</b>{{$empresa->iibb}}</td>
                </tr>
                <tr>
                    <td></td>
                    <td style="text-align: center; width: 200px;"></td>
                    <td style="text-align: right; width: 200px; font-size:14px"><b>Inicio de Act.:&nbsp;</b>{{$empresa->inicioActividades}}</td>
                    
                </tr>
                <tr>
                    <td></td>
                    <td style="text-align: center; width: 200px;"></td>
                    <td style="text-align: right; width: 200px;"></td>
                </tr>
            </tbody>
        </table>
        <table  style="border: solid 1px; width: 100%; font-size: 12px; ">
            <tbody>
                <tr>
                    <td style="font-size:14px"><b>Cliente: {{$cliente->name}}</b></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="font-size:14px">Domicilio: {{$direccion->direccion}}</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="font-size:14px">Localidad: {{$direccion->localidad}} - {{$direccion->provincia}}</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="font-size:14px">
                        Cond IVA: &nbsp;<b>{{$responsabilidad}}</b>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="font-size:14px">CUIT: {{$cliente->cuit}}</td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </header>

    <main>
    <table style="width: 100%; font-size: 12px"  align="center" border="1"">
            <thead>                  
                <tr>
                    <th style= "text-align: center"><b>N°</b></th>
                    <th style= "text-align: center"><b>Codigo</b></th>
                    <th style= "text-align: center"><b>Detalle</b></th>
                    <th style= "text-align: center"><b>Cantidad</b></th>
                    <th style= "text-align: center"><b>Precio Unitario</b></th>
                    <th style= "text-align: center"><b>%Bon</b></th>
                    <th style= "text-align: center"><b>Subtotal</b></th>
                </tr>
            </thead>
            <body>
                
                @foreach ($tabla as $item)
                    <tr>
                        <td style= "text-align: center; width: 2%; font-size: 10px;"><b>{{ $loop->iteration }}</b></td>
                        <td style= "text-align: center; width: 10%; font-size: 10px;"><b>{{$item->codigo}}</b></td>
                        <td style= "text-align: center; width: 25%; font-size: 10px;"><b>{{$item->nombre}}</b></td>
                        <td style= "text-align: center; width: 6%; font-size: 10px;"><b>{{$item->cantidad}}</b></td>
                        <td style= "text-align: center; width: 8%; font-size: 10px;"><b>{{$item->precio_unitario}}</b></td>
                        <td style= "text-align: center; width: 8%; font-size: 10px;"><b>{{$item->descuento}}</b></td>
                        <td style= "text-align: center; width: 8%; font-size: 10px;"><b>{{$item->subTotalIva}}</b></td>
                    </tr>
                    @php $sub += $item->subTotalIva; @endphp
                    @if($loop->iteration % 11 == 0 && $loop->iteration<count($tabla))
                        @php $it += 1; @endphp

                        </table>
                        <table align="right">
                            <tr>
                            <td style= "text-align: right; width: 20%; font-size: 15px;">SUBTOTAL: $<b>{{$sub}}</b></td>
                            </tr>
                        </table>
                        <div class="footer2">    
                        <hr class="solid">
                        <table align="center">
                            <tr>
                                <td style= "text-align: center; width: 20%; font-size: 10px;">Transporte, continúa en página {{$it+1}}</td>
                            </tr>   
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td><img src="data:image/png;base64, {{ base64_encode($qr) }} "></td>
                                    <td style="text-align: center; width: 350px;">
                                        <!-- <img src="images/{{$empresa->rutaLogo}}" class="center" height="80" width="271"> -->
                                    </td>
                                    <td style="text-align: right; width: 240px; font-size:12px">
                                        <b>CAE:</b> {{$factura->CAE}}
                                        <br>
                                        <b>Vto. CAE:</b> {{$factura->fechaCAE}}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table align="right">
                            <tr>
                            <td style= "text-align: right; width: 20%; font-size: 15px;">Pag.{{$it}}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="page_break"></div>
                    <table style="width: 100%; font-size: 12px; page-break-inside: auto;"  align="center" border="1">
                    <thead>                  
                        <tr>
                            <th style= "text-align: center"><b>N°</b></th>
                            <th style= "text-align: center"><b>Codigo</b></th>
                            <th style= "text-align: center"><b>Detalle</b></th>
                            <th style= "text-align: center"><b>Cantidad</b></th>
                            <th style= "text-align: center"><b>Precio Unitario</b></th>
                            <th style= "text-align: center"><b>%Bon</b></th>
                            <th style= "text-align: center"><b>Subtotal</b></th>
                        </tr>
                    </thead>
                    <body>  
                @endif
                @endforeach
                @if($it>0)
                    </div>
                    </table>
                @endif
            </body>
        </table>
    </main>
    <div class="footer2">    
        <table class="table" cellpadding="2" cellspacing="2">
            <tbody>
            <tr>
                <td style="vertical-align: top; text-align: right;">
                    <span style="font-weight: bold; font-size:12px"><b> TOTAL $&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{$factura->ImpTotal}}</b></span>
                </td>
            </tr>
            </tbody>
        </table>
        <hr class="solid">
        <table>
            <tbody>
                <tr>
                    <td><img src="data:image/png;base64, {{ base64_encode($qr) }} "></td>
                    <td style="text-align: center; width: 350px;">
                        <!-- <img src="images/{{$empresa->rutaLogo}}" class="center" height="80" width="271"> -->
                    </td>
                    <td style="text-align: right; width: 240px; font-size:12px">
                        <b>CAE:</b> {{$factura->CAE}}
                        <br>
                        <b>Vto. CAE:</b> {{$factura->fechaCAE}}
                    </td>
                </tr>
            </tbody>
        </table>
        <table align="right">
            <tr>
            <td style= "text-align: right; width: 20%; font-size: 15px;">Pag.{{$it+1}}</td>
            </tr>
        </table>
    </div>
    </body>
</html>