<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Laravel PDF</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <style>
            #table2 {
              border-collapse: collapse;
              border-spacing: 10px;  
            }
        </style>
    </head>
    <body>
        <h3 style="text-align:center">Remito N {{substr('00000'.$remito->id_remito, -5)}}</h3>
        <h6 class="mb-3" style="text-align:center">Documento no valido como factura</h6>
        @if(env('DATOS_REMITO_VENTA', false))
        <h6 class="m-0">==========================================================================</h6>
        @if(env('RAZON_SOCIAL', '') != '')
        <p class="m-0">RAZON SOCIAL: {{env('RAZON_SOCIAL', '')}}</p>
        @endif
        @if(env('CUIT', '') != '')
        <p class="m-0">CUIT: {{env('CUIT', '')}}</p>
        @endif
        @if(env('DIRECCION', '') != '')
        <p class="m-0">{{env('DIRECCION', '')}}</p>
        @endif
        @if(env('EMAIL', '') != '')
        <p class="m-0">{{env('EMAIL', '')}}</p>
        @endif
        @if(env('WEB', '') != '')
        <p class="m-0">{{env('WEB', '')}}</p>
        @endif
        <h6 class="m-0">==========================================================================</h6>
        @endif
        <table id="table2">
            <thead>                  
                <tr>
                    <th></th>
                    <th style="text-align: right; width:400px;"></th>
                </tr>
            </thead>
            <tbody>                  
                <tr>
                    <td>Cuit: {{$remito->cuit}}</td>
                    {{-- <td style="text-align: right; width:300px;">Numero: 01-{{substr('00000'.$remito->id_remito, -5)}}</td> --}}
                    <td style="text-align: right;">Fecha: {{date('d/m/Y', strtotime($remito->created_at))}}</td>
                </tr>
                <?php
                    switch ($remito->responsabilidad) {
                        case 1:
                            $razonSocialEmpresa = 'IVA Responsable Inscripto';
                            break;
                        case 2:
                            $razonSocialEmpresa = 'IVA Responsable no Inscripto';
                            break;
                        case 3:
                            $razonSocialEmpresa = 'IVA no Responsable';
                            break;
                        case 4:
                            $razonSocialEmpresa = 'IVA Sujeto Exento';
                            break;
                        
                        case 5:
                            $razonSocialEmpresa = 'Consumidor Final';
                            break;
                        
                        case 6:
                            $razonSocialEmpresa = 'Responsable Monotributo';
                            break;

                        case 7:
                            $razonSocialEmpresa = 'Sujeto no Categorizado';
                            break;

                        case 8:
                            $razonSocialEmpresa = 'Proveedor del Exterior';
                            break;

                        case 9:
                            $razonSocialEmpresa = 'Cliente del Exterior';
                            break;
                            
                        case 10:
                            $razonSocialEmpresa = ' IVA Liberado – Ley Nº 19.640';
                            break;

                        case 11:
                            $razonSocialEmpresa = 'IVA Responsable Inscripto – Agente de Percepción';
                            break;

                        case 12:
                            $razonSocialEmpresa = 'Pequeño Contribuyente Eventual';
                            break;

                        case 13:
                            $razonSocialEmpresa = 'Monotributista Social';
                            break;

                        case 14:
                            $razonSocialEmpresa = 'Pequeño Contribuyente Eventual Social';
                            break;

                        default:
                            $razonSocialEmpresa = 'IVA Responsable Inscripto';
                            break;
                    }
                ?>
                <tr>
                    <td>Nombre/Razon social: {{$remito->name}}</td>
                    <td style="text-align: right;">Condicion de iva: {{$razonSocialEmpresa}}</td>
                    {{-- <td style="text-align: right; width:300px;">Fecha: {{date('d/m/Y', strtotime($remito->created_at))}}</td> --}}
                </tr>
                <tr>
                    <td>Nro. factura: {{$remito->nro_factura}}</td>
                    <td style="text-align: right;">Transporte: {{$remito->flete}}</td>
                </tr>
                <tr>
                    <td>Domicilio: {{$remito->direccion}} - {{$remito->localidad}} ({{$remito->provincia}})</td>
                    {{-- <td style="text-align: right;">Condicion de venta: Efectivo</td> --}}
                </tr>
                {{-- <tr>
                    <td>Condicion de venta: Efectivo</td>
                    <td></td>
                </tr> --}}
            </tbody>
        </table>
        <br>
        <table class="table table-bordered" style="border: solid 4px;font-size: 13px">
            <thead>                  
                <tr>
                    <th>Descripción</th>
                    {{-- <th>Iva</th> --}}
                    {{-- <th>Prec. unit.</th> --}}
                    <th>Cantidad</th>
                    {{-- <th>Dcto.</th> --}}
                    {{-- <th>Precio total</th> --}}
                    {{-- <th>Precio con iva</th> --}}
                </tr>
            </thead>
            <tbody>
            {{-- @php 
            $total_sin_descuento = 0.00;
            $total_con_descuento = 0.00;
            $total_con_descuento_con_iva = 0.00;
            @endphp --}}
            @foreach($productos as $producto)
            {{-- @php 
            $renglon_sin_descuento = ($producto->cantidad * $producto->precio_unitario);
            $renglon_con_descuento = $renglon_sin_descuento *(1.0-($producto->descuento/100.0));
            $renglon_con_descuento_con_iva = $renglon_con_descuento *(1.0+($producto->iva/100.0));
            $total_sin_descuento = $total_sin_descuento + $renglon_sin_descuento;
            $total_con_descuento = $total_con_descuento + $renglon_con_descuento;
            $total_con_descuento_con_iva = $total_con_descuento_con_iva + $renglon_con_descuento_con_iva;
            @endphp --}}
                <tr>
                    <td>{{$producto->codigo}} - {{$producto->nombre}}{{$producto->tipo_item != 'venta' ? ' - '.($producto->tipo_item == 'devolucion' ? 'DEVOLUCIÓN' : 'PROMOCIÓN') : ''}}</td>
                    {{-- <td>{{$producto->iva}}%</td> --}}
                    {{-- <td>$ {{number_format($producto->precio_unitario,2,",",".")}}</td> --}}
                    <td>{{$producto->cantidad}}</td>
                    {{-- <td>{{$producto->descuento}}%</td> --}}
                    {{-- <td>$ {{number_format($renglon_con_descuento,2,",",".")}}</td> --}}
                    {{-- <td>$ {{number_format($renglon_con_descuento_con_iva,2,",",".")}}</td> --}}
                </tr>
            @endforeach
                {{-- <tr>
                    <td colspan="5"><strong>TOTALES</strong></td>
                    <td><strong>$ {{number_format(($total_con_descuento),2,",",".")}}</strong></td>
                    <td><strong>$ {{number_format(($total_con_descuento_con_iva),2,",",".")}}</strong></td>
                </tr> --}}
            </tbody>
        </table>
        @if($remito->observaciones_publicas != '')
        <table id="table2">
            <tbody>                  
                <tr>
                    <td>Observaciones: </td>
                    <td><?= $remito->observaciones_publicas ?></td>
                </tr>
            </tbody>
        </table>
        @endif
    </body>
</html>