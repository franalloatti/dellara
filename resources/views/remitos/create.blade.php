@extends('adminlte::page')

@section('content')
  {{-- <head>

    <!-- Meta -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">


    <!-- CSS -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

    <!-- Script -->
    <script src="/js/jquery-3.5.1.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>

  </head> --}}
  <body>
    <script type="text/javascript">
      $(document).ready(function(){
        $('.js-example-basic-single').select2({matcher: matcher});
        $('#user_id_user').on('change', function(e){
          var user_id_user = e.target.value;
          $.get('direccionescliente/'+user_id_user, function(data){
            var direccion_id_direccion = '<option value="" disabled selected>Seleccionar</option>'
            for (var i=0; i<data.length;i++)
            direccion_id_direccion+='<option value="'+data[i].id_direccion+'">'+data[i].direccion+'</option>';

            $("#direccion_id_direccion").html(direccion_id_direccion);

          }).fail(response => {
            toastr.error(response.responseJSON.message);
          });
        });
      });
      function funcion(){
        var producto_seleccionado = $('#producto_id').val();
        var descuentoCliente = $('#descuentoCliente').val();
        var productos = {!! $productos !!};
        productos.forEach(data => {
          if(producto_seleccionado == data.id_producto){
            var texto = '<tr id="fila'+data.id_producto+'">'
            +'  <input name="items['+data.id_producto+'][id]" value="'+data.id_producto+'" type="hidden" class="form-control" id="items['+data.id_producto+'][id]">'
            +'  <input name="items['+data.id_producto+'][costo_unitario]" value="'+parseFloat(data.valor_dolar * data.costo_dolares).toFixed(2)+'" type="hidden" class="form-control" id="items['+data.id_producto+'][id]">'
            +'  <td>'+data.nombre+'</td>'
            +'  <td><input name="items['+data.id_producto+'][precio_unitario]" readonly value="'+parseFloat((data.valor_dolar * data.costo_dolares)*(data.margen / 100.00)).toFixed(2)+'" type="number" class="form-control" id="items['+data.id_producto+'][precio_unitario]"></td>'
            +'  <td><input name="items['+data.id_producto+'][cantidad]" type="number" value="0" onchange="return actualizar('+data.id_producto+',this.value)" class="form-control" id="items['+data.id_producto+'][cantidad]"></td>'
            +'  <td><input name="items['+data.id_producto+'][precio_total]" readonly value="0" type="number" class="form-control" id="items['+data.id_producto+'][precio_total]"></td>'
            +'  <td><input name="items['+data.id_producto+'][descuento]" type="number" value="'+descuentoCliente+'" onchange="return actualizarDescuento('+data.id_producto+',this.value)" class="form-control" id="items['+data.id_producto+'][descuento]"></td>'
            +'  <td><input name="items['+data.id_producto+'][precio_final]" readonly value="0" type="number" class="form-control" id="items['+data.id_producto+'][precio_final]"></td>'
            +'  <td><button onclick="return borrar('+data.id_producto+')" class="btn btn-danger">Eliminar</button></td>'
            +'</tr>'
            $('#tablaproductos').append(texto);
            return;
          }
        });
        $('#producto_id').val("");
        actualizarTotales();
        return false;
      };
      function setDescuento(id_cliente){
        var clientes = {!! $clientes !!};
        clientes.forEach(data => {
          if(id_cliente == data.id_user){
            document.getElementById('descuentoCliente').value = data.descuento;
            $("#tablaproductos tr").each(function(index, tr) { 
              var precio_total = $(this).find("input[name*='[precio_total]']").val();
              $(this).find("input[name*='[descuento]']").val(data.descuento);
              $(this).find("input[name*='[precio_final]']").val(parseFloat(precio_total *(1 - data.descuento/100)).toFixed(2));
            });
            // $( "input[name*='[descuento]']" ).val(data.descuento);
            return;
          }
        });
        actualizarTotales();
      };
      function actualizarTotales(){
        var cantidadItems = 0;
        var precio_total = 0;
        var descuento = 0;
        var precio_final = 0;
        $("#tablaproductos tr").each(function(index, tr) { 
          cantidadItems = cantidadItems + 1;
          precio_total = parseFloat(precio_total) + parseFloat($(this).find("input[name*='[precio_total]']").val());
          descuento = parseFloat(descuento) + parseFloat($(this).find("input[name*='[descuento]']").val());
          precio_final = parseFloat(precio_final) + parseFloat($(this).find("input[name*='[precio_final]']").val());
        });
        document.getElementById('precio_total_final').innerHTML = parseFloat(precio_total).toFixed(2);
        console.log("desc"+descuento);
        console.log("cant"+cantidadItems);
        document.getElementById('descuento_final').innerHTML = cantidadItems ? parseFloat(descuento / cantidadItems).toFixed(2) : parseFloat(0).toFixed(2);
        document.getElementById('precio_final_final').innerHTML = parseFloat(precio_final).toFixed(2);
      };
      function actualizar(id_producto, value){
        var precio_unitario = document.getElementById('items['+id_producto+'][precio_unitario]').value;
        var descuento = document.getElementById('items['+id_producto+'][descuento]').value;
        document.getElementById('items['+id_producto+'][precio_total]').value = parseFloat(precio_unitario*value).toFixed(2);
        document.getElementById('items['+id_producto+'][precio_final]').value = parseFloat(precio_unitario*value*(1-descuento/100)).toFixed(2);
        actualizarTotales();
      };
      function actualizarDescuento(id_producto, descuento){
        var precio_total = document.getElementById('items['+id_producto+'][precio_total]').value;
        document.getElementById('items['+id_producto+'][precio_final]').value = parseFloat(precio_total*(1-descuento/100)).toFixed(2);
        actualizarTotales();
      };
      function borrar(id_producto){
        $("#fila" + id_producto).remove();
        actualizarTotales();
      }
    </script>
    <style>
    .select2-container .select2-selection--single{
      height: auto !important;
    }
    .select2-container {
      width: 100% !important;
    }
    </style>
<div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Pedido <small>Crear</small></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="quickForm" method="post" action="{{ route('pedidos.guardar') }}">
                @csrf
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="user_id_user">Cliente</label>
                      <select name="user_id_user" onchange="return setDescuento(this.value)" class="form-control js-example-basic-single" id="user_id_user" required>
                        <option value="" disabled selected>Seleccionar</option>
                        @foreach($clientes as $cliente)
                        <option value="{{$cliente->id_user}}">{{$cliente->name}}</option>
                        @endforeach
                      </select>
                    </div>
                    <input name="descuentoCliente" value="0" type="hidden" class="form-control" id="descuentoCliente">
                    <div class="form-group col-sm-6">
                      <label for="direccion_id_direccion">Dirección de entrega</label>
                      <select name="direccion_id_direccion" class="form-control js-example-basic-single" id="direccion_id_direccion" required>
                        <option value="" disabled selected>Seleccionar</option>
                      </select>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="producto_id">Producto</label>
                      <select name="producto_id" class="form-control" id="producto_id">
                        <option value="" disabled selected>Seleccionar</option>
                        @foreach($productos as $producto)
                        <option value="{{$producto->id_producto}}">{{$producto->codigo . " - " . $producto->nombre}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="form-group col-sm-2" style="align-self: flex-end;">
                      <button type="button" onclick="return funcion();" id="agregar" name="agregar" class="form-control btn btn-info">Agregar</button>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-12">
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>Producto</th>
                            <th>Precio Unitario</th>
                            <th>Cantidad</th>
                            <th>Precio Total</th>
                            <th>Descuento</th>
                            <th>Precio Final</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody id="tablaproductos">
                        </tbody>
                        <tbody>
                            <th colspan="3">Totales</th>
                            <th id="precio_total_final" name="precio_total_final">0</th>
                            <th id="descuento_final" name="descuento_final">0</th>
                            <th id="precio_final_final" name="precio_final_final">0</th>
                            <th></th>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Crear</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
  </body>
</html>
@stop
