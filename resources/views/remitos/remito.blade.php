@extends('adminlte::page')

@section('content')
<head>
</head>
<body>
    <script type="text/javascript">
      function marcarTodos(checked){
          $("#tablaproductos tr").each(function(index, tr) { 
              $(this).find("input[name*='[checkbox]']").prop("checked", checked);
          });
      }
      function desmarcarElTodos(){
          $("#tablaproductos tr").each(function(index, tr) { 
              if(!$(this).find("input[name*='[checkbox]']").is(':checked')){
                  $("input[name*='[todos][checkbox]']").prop("checked", false);
                  return;
              }
          })
      }
      function actualizarTotales(){
        var cantidadItems = 0;
        var precio_total = 0;
        var descuento = 0;
        var precio_final = 0;
        $("#tablaproductos tr").each(function(index, tr) { 
          cantidadItems = cantidadItems + 1;
          precio_total = parseFloat(precio_total) + parseFloat($(this).find("input[name*='[precio_total]']").val());
          descuento = parseFloat(descuento) + parseFloat($(this).find("input[name*='[descuento]']").val());
          precio_final = parseFloat(precio_final) + parseFloat($(this).find("input[name*='[precio_final]']").val());
        });
        document.getElementById('precio_total_final').innerHTML = parseFloat(precio_total).toFixed(2);
        console.log("desc"+descuento);
        console.log("cant"+cantidadItems);
        document.getElementById('descuento_final').innerHTML = cantidadItems ? parseFloat(descuento / cantidadItems).toFixed(2) : parseFloat(0).toFixed(2);
        document.getElementById('precio_final_final').innerHTML = parseFloat(precio_final).toFixed(2);
      };
      function actualizar(id_producto, value){
        var precio_unitario = document.getElementById('items['+id_producto+'][precio_unitario]').value;
        var descuento = document.getElementById('items['+id_producto+'][descuento]').value;
        document.getElementById('items['+id_producto+'][precio_total]').value = parseFloat(precio_unitario*value).toFixed(2);
        document.getElementById('items['+id_producto+'][precio_final]').value = parseFloat(precio_unitario*value*(1-descuento/100)).toFixed(2);
        actualizarTotales();
      };
      function actualizarDescuento(id_producto, descuento){
        var precio_total = document.getElementById('items['+id_producto+'][precio_total]').value;
        document.getElementById('items['+id_producto+'][precio_final]').value = parseFloat(precio_total*(1-descuento/100)).toFixed(2);
        actualizarTotales();
      };
    </script>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Generar remito</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="float-right">
                        </div>
                        <br><br>
                        <form class="form-horizontal" method="post" action="{{ route('remitos.guardar') }}">
                            @csrf
                            <input name="id_pedido" value="{{$pedido->id_pedido}}" type="hidden" class="form-control" id="id_pedido">
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Cliente</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input name="user_id_user" value="{{$pedido->user_id_user}}" type="hidden" class="form-control" id="user_id_user">
                                            <p>{{$pedido->name}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                <label for="direccion_id_direccion">Dirección de entrega</label>
                                <select name="direccion_id_direccion" class="form-control js-example-basic-single" id="direccion_id_direccion" required>
                                    <!-- <option value="" disabled selected>Seleccionar</option> -->
                                    @foreach($direcciones as $direccion)
                                    @php 
                                    if($direccion->id_direccion == $pedido->id_direccion){
                                    @endphp 
                                        <option value="{{$direccion->id_direccion}}" selected>{{$direccion->direccion}}</option>    
                                    @php 
                                    }else{
                                    @endphp
                                        <option value="{{$direccion->id_direccion}}">{{$direccion->direccion}}</option>
                                    @php    
                                    }
                                    @endphp
                                    @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Productos</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <table class="table table-bordered">
                                        <thead>                  
                                            <tr>
                                            <th>
                                            <input name="items[todos][checkbox]" type="checkbox" onclick="return marcarTodos(this.checked);" id="items['{{$pedido->id_pedido}}'][todos]">
                                            </th>
                                            <th>Codigo</th>
                                            <th>Nombre</th>
                                            <th>Precio unitario</th>
                                            <th>Cantidad</th>
                                            <th>Precio total</th>
                                            <th>Descuento</th>
                                            <th>Precio final</th>
                                            <th>Unidades disponibles</th>
                                            <!-- <th></th> -->
                                            </tr>
                                        </thead>
                                        <tbody id="tablaproductos">
                                        @php 
                                        $total_sin_descuento = 0.00;
                                        $total_con_descuento = 0.00;
                                        @endphp
                                        @foreach($productos as $producto)
                                        @php 
                                        $total_sin_descuento = $total_sin_descuento + ($producto->cantidad * $producto->precio_unitario);
                                        $total_con_descuento = $total_con_descuento + ($producto->cantidad * $producto->precio_unitario)*(1-($producto->descuento/100));
                                        @endphp
                                        <tr id="fila{{$producto->id_pedido_producto}}">
                                            <input name="items[{{$producto->id_pedido_producto}}][id]" value="{{$producto->id_pedido_producto}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][id]">
                                            <input name="items[{{$producto->id_pedido_producto}}][costo_unitario]" value="{{$producto->valor_dolar * $producto->costo_dolares}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][id]">
                                            <td><input name="items[{{$producto->id_pedido_producto}}][checkbox]" onclick="return desmarcarElTodos();" type="checkbox" id="items[{{$producto->id_pedido_producto}}][checkbox]"></td>
                                            <td>{{$producto->codigo}}</td>
                                            <td>{{$producto->nombre}}</td>
                                            <td><input name="items[{{$producto->id_pedido_producto}}][precio_unitario]" value="{{($producto->valor_dolar * $producto->costo_dolares)*($producto->margen / 100.00)}}" type="number" class="form-control" id="items[{{$producto->id_pedido_producto}}][precio_unitario]"></td>
                                            <td><input name="items[{{$producto->id_pedido_producto}}][cantidad]" type="number" value="{{$producto->cantidad}}" onchange="return actualizar({{$producto->id_pedido_producto}},this.value)" class="form-control" id="items[{{$producto->id_pedido_producto}}][cantidad]"></td>
                                            <td><input name="items[{{$producto->id_pedido_producto}}][precio_total]" readonly value="{{$total_sin_descuento}}" type="number" class="form-control" id="items[{{$producto->id_pedido_producto}}][precio_total]"></td>
                                            <td><input name="items[{{$producto->id_pedido_producto}}][descuento]" type="number" value="{{$producto->descuento}}" onchange="return actualizarDescuento({{$producto->id_pedido_producto}},this.value)" class="form-control" id="items[{{$producto->id_pedido_producto}}][descuento]"></td>
                                            <td><input name="items[{{$producto->id_pedido_producto}}][precio_final]" readonly value="{{$total_con_descuento}}" type="number" class="form-control" id="items[{{$producto->id_pedido_producto}}][precio_final]"></td>
                                            <td>{{$producto->stock}}</td>
                                            <!-- <td><button onclick="return borrar('{{$producto->id_pedido_producto}}')" class="btn btn-danger">Eliminar</button></td> -->
                                        </tr>
                                            @endforeach
                                            <tr>
                                            <td colspan="4"><strong>TOTALES</strong></td>
                                            <td><strong>$ {{number_format($total_sin_descuento,2,",",".")}}</strong></td>
                                            <td></td>
                                            <td><strong>$ {{number_format(($total_con_descuento),2,",",".")}}</strong></td>
                                            <td colspan="2"></td>
                                            </tr>
                                        </tbody>
                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-success" onclick="return confirmar('generar el remito',this,event);">Generar remito</button>
                                    </div>
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.tab-pane -->
    </body>
@stop