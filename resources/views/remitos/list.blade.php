@extends('adminlte::page')

{{-- @include('remitos.modalDomicilioFactura') --}}
@section('content')
  <script>
    function cargarDatosRemito(remito){
      console.log(remito);
      $('#id_remito').val(remito.id_remito);
      $('#id_user').val(remito.id_user);
      $('#monto').val(remito.monto - remito.monto_pagos);
      $("#monto").attr({
        "max" : remito.monto - remito.monto_pagos
      });
    }
  </script>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Remitos</h3>
            <div class="card-tools">
              <form role="form" action="/admin/remitos/index" method="POST">
                @csrf
                <div class="input-group input-group-sm">
                  <input type="date" id="fecha_desde" title="Fecha desde" name="fecha_desde" value="{{$fecha_desde ? $fecha_desde : ''}}" class="form-control pull-right" placeholder="Buscar">
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="date" id="fecha_hasta" title="Fecha hasta" name="fecha_hasta" value="{{$fecha_hasta ? $fecha_hasta : ''}}" class="form-control pull-right" placeholder="Buscar">
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <select name="user_id" class="form-control" id="user_id">
                      <option value="">Cliente</option>
                      @foreach($clientes as $cliente)
                      @if($cliente->id_user == $clienteSelected)
                          <option value="{{$cliente->id_user}}" selected>{{$cliente->name}}</option>
                      @else
                          <option value="{{$cliente->id_user}}">{{$cliente->name}}</option>
                      @endif
                      @endforeach
                  </select>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <select name="estado_id" class="form-control" id="estado_id">
                      <option value="">Estado</option>
                      @foreach($estados as $estado)
                      @if($estado->id_estado == $estadoSelected)
                          <option value="{{$estado->id_estado}}" selected>{{$estado->nombre}}</option>
                      @else
                          <option value="{{$estado->id_estado}}">{{$estado->nombre}}</option>
                      @endif
                      @endforeach
                  </select>
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  {{-- <input type="text" id="search" name="search" value="{{$search ? $search : ''}}" class="form-control pull-right" placeholder="Buscar"> --}}
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered">
              <thead>                  
                <tr>
                  <th>Cliente</th>
                  <th>Nro remito</th>
                  <th>Fecha remito</th>
                  <th>NP</th>
                  <th>Estado pedido</th>
                  <th>Nro. factura</th>
                  <th>Dirección</th>
                  <th>Flete</th>
                  <th>Monto total</th>
                  <th>Monto saldado</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>
                @foreach($remitos as $remito)
                <tr>
                  <td>{{$remito->name}}</td>
                  <td>01-{{substr('00000'.$remito->id_remito, -5)}}</td>
                  <td>{{date('d/m/Y', strtotime($remito->created_at))}}</td>
                  <td><a href="/admin/pedidos/ver/{{$remito->pedido_id_pedido}}">{{$remito->pedido_id_pedido}}</a></td>
                  <td>{{$remito->estado == 'En preparación' ? ($remito->id_remito === null ? 'Solicitado' : $remito->estado) : $remito->estado}}</td>
                  <td>{{$remito->nro_factura}}</td>
                  <td>{{$remito->direccion}}</td>
                  <td>{{$remito->flete}}</td>
                  <td align="right" style="white-space: nowrap;">$ {{number_format($remito->monto,2,",",".")}}</td>
                  <td align="right" style="white-space: nowrap;">$ {{number_format($remito->monto_pagos,2,",",".")}}</td>
                  <td>
                    <a href="{{ route('remitos.ver', [$remito->id_remito]) }}" title="Ver" class="btn btn-success"><i class="fa fa-eye"></i></a>
                    @if($remito->estado != 'Cancelado')
                      <a href="{{ route('remitos.imprimir', [$remito->id_remito]) }}" title="Imprimir" class="btn btn-secondary"><i class="fa fa-print"></i></a>
                      <a href="{{ route('remitos.imprimirtransporte', [$remito->id_remito]) }}" title="Imprimir" class="btn btn-warning"><i class="fa fa-truck"></i></a>
                      @if($remito->monto > $remito->monto_pagos)
                      <a href="#" class="btn btn-sm btn-info" title="Cobrar" style="font-family: auto;font-weight: bold;font-size: 19px;padding: 0px 7px 0px 7px;" 
                        data-toggle="modal" data-target="#cobrar" onclick="cargarDatosRemito({{json_encode($remito)}})">$
                      </a>
                      @endif
                      @if($remito->Facturado)
                            <button onclick="imprimir({{$remito->numero}})" class="btn btn-sm btn-primary"><i class="fa fa-print"></i>&nbsp;&nbsp;&nbsp;Imprimir Factura</button>&nbsp;&nbsp;&nbsp;
                      @else
                            {{-- <button onclick="generar({{$remito->id_remito}})" class="btn btn-sm btn-danger"><i class="fa fa-file"></i>&nbsp;&nbsp;&nbsp;Facturar</button>&nbsp;&nbsp;&nbsp; --}}
                            @if(Auth::user()->hasPermiso('Facturas', 'crear'))
                            <button onclick="cargarModal({{$remito->id_remito}},{{$remito->id_user}},{{$remito->id_direccion}},'{{$remito->direccion}}')" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-file"></i>&nbsp;&nbsp;&nbsp;Facturar</button>&nbsp;&nbsp;&nbsp;  
                            @endif 
                      @endif 
                    @endif 
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
          <div class="card-footer clearfix">
                {{ $remitos->appends(['search' => $search])->links() }}
          </div>
        </div>
      <!-- /.card -->
      </div>
    </div>
  </div>
  <div class="modal fade" id="cobrar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <form role="form" id="quickForm" method="post" action="{{ route('remitos.cobrar') }}">
      @csrf
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Cobrar</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
            <input type="hidden" name="redirect_url" value="{{$_SERVER["REQUEST_URI"]}}" class="form-control" id="redirect_url" required>
            <input type="hidden" name="id_remito" class="form-control" id="id_remito" required>
            <input type="hidden" name="id_user" class="form-control" id="id_user" required>
            <div class="row">
              <div class="form-group col-sm-6">
                <label for="cuenta_id">Cuenta</label>
                <select name="cuenta_id" class="form-control" id="cuenta_id" required>
                  <option value="">Seleccione</option>
                  @foreach($cuentas as $cuenta)
                    <option value="{{$cuenta->id_medio}}">{{$cuenta->nombre}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group col-sm-6">
                <label for="monto">Monto</label>
                <input type="number" step="0.01" min="0" name="monto" class="form-control" id="monto" required>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-sm-12">
                <label for="descripcion">Descripción</label>
                <textarea name="descripcion" class="form-control" id="descripcion"></textarea>
              </div>
            </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-sm btn-success">Aceptar</button>
          </div>
        </div>
      </div>
    </form>
  </div>
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">DOMICILIO DE FACTURACIÓN</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <input type="hidden" id="idRemitoModal" value="">
          <select name="domicilioFacturacion" id="domicilioFacturacion" class="custom-select">
              {{-- Completamos con Js --}}
          </select>
          <hr>
          <div class= "row ml-auto mt-2">
            El remito fue realizado con dirreción:  &nbsp;<strong><p id="direccion"></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
          <button onclick="validarModal()" type="button" class="btn btn-primary">Generar Factura</button>
        </div>
      </div>
    </div>
  </div>
        @stop


@section('js')
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    
<script>
  function generar(idRemito,idDomicilio){
    let ruta = '/admin/factura/generar/' + idRemito+'/'+idDomicilio
    axios.get(ruta)
          .then(function (response) {
              var idFactura = response.data
              var conf = confirm("¿Está seguro de validar factura?");
              if(conf == true){
                validar(idFactura);
              }
          })
          .catch(function (error) {
              // handle error
              console.log(error);
          });
  };

  function validar(idFactura){
    let ruta = '/admin/factura/validar/' + idFactura
    axios.get(ruta)
          .then(function (response) {
              console.log(response.data)
              location.reload();
          })
          .catch(function (error) {
               console.log(error.response.data);
               alert("Se produjo un error, con la siguiente descripción: "+error.response.data.message)
          });
  }
  function imprimir(numero){
      window.open("/admin/factura/imprimir/" + numero, "_blank");
  }

  function cargarModal(idRemito,idUsuario,idDireccion,direccion) {
    console.log(idDireccion)
    // Limpiamos campos
    $('#domicilioFacturacion').find('option').remove().end()
    $('#idRemitoModal').val('');
    $('#idRemitoModal').val(idRemito);
    var p = document.getElementById('direccion');
    p.innerHTML = "";
    p.innerHTML = direccion;
    // Buscamos los datos de facturación
    let ruta = '/admin/factura/domicilioFiscal/' + idUsuario
    axios.get(ruta)
          .then(function (response) {
              // console.log(response.data.length)
              domicilios = response.data;
              select = document.getElementById("domicilioFacturacion");

              for(i = 0; i <= domicilios.length; i++){
                  option = document.createElement("option");
                  option.value = domicilios[i]["id_direccion"];
                  option.text = domicilios[i]["direccion"]+" - "+ domicilios[i]["localidad"] +"("+domicilios[i]["codigo_postal"]+") ";
                  if(idDireccion == domicilios[i]["id_direccion"]){
                    option.selected = true;
                  }
                  select.appendChild(option);
              }
          })
          .catch(function (error) {
               console.log(error);
          });
  }

  function validarModal() {
    idRemito = $('#idRemitoModal').val()
    idDomicilio = $('#domicilioFacturacion').val()
    generar(idRemito,idDomicilio);
  }
</script>
@endsection
