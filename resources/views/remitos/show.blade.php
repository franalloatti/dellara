@extends('adminlte::page')

@section('content')
    <head>
    </head>
    <body>
        <script type="text/javascript">
        function cambiarestado(id_pedido, id_estado){
            $.get('../cambiarestado/'+id_pedido+"/"+id_estado, function(data){
                location.reload();
            }).fail(response => {
                toastr.error(response.responseJSON.message);
            });
        }
        function cargarDatosRemito(remito){
            console.log(remito);
            $('#monto').val(remito.monto - remito.monto_pagos);
            $("#monto").attr({
                "max" : remito.monto - remito.monto_pagos
            });
        }
        </script>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Remito</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="float-right">
                                @if($remito->estado != 'Cancelado')
                                    @if($remito->monto > $remito->monto_pagos)
                                    <a href="#" class="btn btn-sm btn-info" data-toggle="modal" data-target="#cobrar" onclick="cargarDatosRemito({{json_encode($remito)}})">
                                        <i class="fa fa-hand-paper"></i>&nbsp;&nbsp;&nbsp;Cobrar
                                    </a>&nbsp;&nbsp;&nbsp;
                                    @else
                                    <p>Cobrado</p>
                                    @endif
                                @else
                                    <p>Pedido cancelado</p>
                                @endif
                            </div>
                            <br><br>
                            <form class="form-horizontal">
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <div class="col-sm-3">
                                                <label for="inputName">Cliente</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <p>{{$remito->name}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <div class="col-sm-3">
                                                <label for="inputName">Direccion de entrega</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <p>{{$remito->direccion}} - {{$remito->localidad}} ({{$remito->provincia}})</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <div class="col-sm-3">
                                                <label for="inputName">Monto saldado</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <p>$ {{number_format($remito->monto_pagos,2,",",".")}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <div class="col-sm-3">
                                                <label for="inputName">Observaciones</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <p><?= $remito->observaciones_privadas ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <div class="col-sm-3">
                                                <label for="inputName">Observaciones públicas</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <p><?= $remito->observaciones_publicas ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="card-title">Productos</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <table class="table table-bordered">
                                            <thead>                  
                                                <tr>
                                                <th>Codigo</th>
                                                <th>Nombre</th>
                                                <th>Iva</th>
                                                <th>Cantidad</th>
                                                <th>Precio unitario</th>
                                                <th>Precio total</th>
                                                <th>Descuento</th>
                                                <th>Precio sin iva</th>
                                                <th>Precio con iva</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @php 
                                            $total_sin_descuento = 0.00;
                                            $total_con_descuento = 0.00;
                                            $total_con_descuento_con_iva = 0.00;
                                            @endphp
                                            @foreach($productos as $producto)
                                            @php 
                                            $renglon_sin_descuento = ($producto->cantidad * $producto->precio_unitario);
                                            $renglon_con_descuento = $renglon_sin_descuento *(1.0-($producto->descuento/100.0));
                                            $renglon_con_descuento_con_iva = $renglon_con_descuento *(1.0+($producto->iva/100.0));
                                            $total_sin_descuento = $total_sin_descuento + $renglon_sin_descuento;
                                            $total_con_descuento = $total_con_descuento + $renglon_con_descuento;
                                            $total_con_descuento_con_iva = $total_con_descuento_con_iva + $renglon_con_descuento_con_iva;
                                            @endphp
                                                <tr>
                                                <td>{{$producto->codigo}}</td>
                                                <td>{{$producto->nombre}} - {{$producto->tipo_item != 'venta' ? $producto->tipo_item : ''}}</td>
                                                <td>{{$producto->iva}}%</td>
                                                <td>{{$producto->cantidad}}</td>
                                                <td style="white-space: nowrap;">$ {{number_format($producto->precio_unitario,2,",",".")}}</td>
                                                <td style="white-space: nowrap;">$ {{number_format($renglon_sin_descuento,2,",",".")}}</td>
                                                <td>{{$producto->descuento}}%</td>
                                                <td style="white-space: nowrap;">$ {{number_format($renglon_con_descuento,2,",",".")}}</td>
                                                <td style="white-space: nowrap;">$ {{number_format($renglon_con_descuento_con_iva,2,",",".")}}</td>
                                                </tr>
                                                @endforeach
                                                <tr>
                                                <td colspan="5"><strong>TOTALES</strong></td>
                                                <td style="white-space: nowrap;"><strong>$ {{number_format($total_sin_descuento,2,",",".")}}</strong></td>
                                                <td></td>
                                                <td style="white-space: nowrap;"><strong>$ {{number_format(($total_con_descuento),2,",",".")}}</strong></td>
                                                <td style="white-space: nowrap;"><strong>$ {{number_format(($total_con_descuento_con_iva),2,",",".")}}</strong></td>
                                                </tr>
                                            </tbody>
                                            </table>
                                        </div>
                                        <!-- /.card-body -->
                                        </div>
                                        <!-- /.card -->
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.tab-pane -->
        <!-- Modal cobrar-->
        <div class="modal fade" id="cobrar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <form role="form" id="quickForm" method="post" action="{{ route('remitos.cobrar') }}">
            @csrf
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Cobrar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="redirect_url" value="{{$_SERVER["REQUEST_URI"]}}" class="form-control" id="redirect_url" required>
                    <input type="hidden" name="id_remito" value="{{$remito->id_remito}}" class="form-control" id="id_remito" required>
                    <input type="hidden" name="id_user" value="{{$remito->id_user}}" class="form-control" id="id_user" required>
                    <div class="row">
                    <div class="form-group col-sm-6">
                        <label for="cuenta_id">Cuenta</label>
                        <select name="cuenta_id" class="form-control" id="cuenta_id" required>
                        <option value="">Seleccione</option>
                        @foreach($cuentas as $cuenta)
                            <option value="{{$cuenta->id_medio}}">{{$cuenta->nombre}}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="monto">Monto</label>
                        <input type="number" step="0.01" min="0" name="monto" class="form-control" id="monto" required>
                    </div>
                    </div>
                    <div class="row">
                    <div class="form-group col-sm-12">
                        <label for="descripcion">Descripción</label>
                        <textarea name="descripcion" class="form-control" id="descripcion"></textarea>
                    </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-sm btn-success">Aceptar</button>
                </div>
                </div>
            </div>
            </form>
        </div>
</body>
@stop