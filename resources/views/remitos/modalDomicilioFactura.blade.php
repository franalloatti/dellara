<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">DOMICILIO DE FACTURACIÓN</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <input type="hidden" id="idRemitoModal" value="">
          <select name="domicilioFacturacion" id="domicilioFacturacion" class="custom-select">
              {{-- Completamos con Js --}}
          </select>
          <hr>
          <div class= "row ml-auto mt-2">
            El remito fue realizado con dirreción:  &nbsp;<strong><p id="direccion"></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
          <button onclick="validarModal()" type="button" class="btn btn-primary">Generar Factura</button>
        </div>
      </div>
    </div>
  </div>