@extends('adminlte::page')

@section('content')
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Servicios</h3>
                <div class="card-tools">
                  <form role="form" action="/admin/servicios/index" method="POST">
                    @csrf
                    <div class="input-group input-group-sm">
                      <select name="limit" class="form-control" id="limit">
                          <option value="20" <?php if($limit == 20) echo 'selected';?>>20 registros</option>
                          <option value="50" <?php if($limit == 50) echo 'selected';?>>50 registros</option>
                          <option value="100" <?php if($limit == 100) echo 'selected';?>>100 registros</option>
                      </select>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <input type="text" id="search" name="search" value="{{$search ? $search : ''}}" class="form-control pull-right" placeholder="Buscar">
                      <div class="input-group-btn">
                        <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th>Nombre</th>
                      <th>Costo</th>
                      <th>Precio</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($servicios as $servicio)
                    @php
                      $precio = ($servicio->costo * ($servicio->margen / 100) * ($servicio->valor_dolar));
                    @endphp
                    <tr>
                      <td>{{$servicio->nombre}}</td>
                      <td align="right" style="white-space: nowrap;">$ {{number_format($servicio->costo * ($servicio->valor_dolar),2,",",".")}}</td>
                      <td align="right" style="white-space: nowrap;">$ {{number_format($precio,2,",",".")}}</td>
                      <td>
                        <a href="{{ route('servicios.ver', [$servicio->id_producto]) }}" class="btn btn-sm btn-success"><i class="fa fa-file "></i>&nbsp;&nbsp;&nbsp;Ver</a>&nbsp;&nbsp;&nbsp;
                        @if($servicio->nombre != 'Servicio logístico')
                        <a href="{{ route('servicios.editar', [$servicio->id_producto]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Editar</a>&nbsp;&nbsp;&nbsp;
                        <a href="{{ route('servicios.borrar', [$servicio->id_producto]) }}" class="btn btn-sm btn-danger" onclick="return confirmar('borrar el servicio',this,event);"><i class="fa fa-trash-alt "></i>&nbsp;&nbsp;&nbsp;Borrar</a>&nbsp;&nbsp;&nbsp;
                        @endif
                        <a href="{{ route('servicios.habilitarDeshabilitar', [$servicio->id_producto, $servicio->estado]) }}" class="btn btn-sm btn-{{$servicio->estado ? 'secondary' : 'primary'}}"><i class="fa {{$servicio->estado ? 'fa-times' : 'fa-check'}}"></i>&nbsp;&nbsp;&nbsp;{{$servicio->estado ? 'Deshabilitar' : 'Habilitar'}}</a>&nbsp;&nbsp;&nbsp;
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                    {{ $servicios->appends(['search' => $search, 'limit' => $limit])->links() }}
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    @stop
