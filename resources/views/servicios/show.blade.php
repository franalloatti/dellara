@extends('adminlte::page')

@section('content')
<script>
function editarStock(id_stock, stock, nombre_almacen){
    document.getElementById('id_stock_modal').value = id_stock;
    document.getElementById('stock_modal').value = stock;
    document.getElementById('titulo_editar_stock').innerHTML = "Editar stock: "+ nombre_almacen;
}
function editarCosto(id_costo, costo, nombre_proveedor, codigo_proveedor){
    document.getElementById('id_costo_modal').value = id_costo;
    document.getElementById('costo_modal').value = costo;
    document.getElementById('codigo_proveedor_modal').value = codigo_proveedor;
    document.getElementById('titulo_editar_costo').innerHTML = "Editar costo: " + codigo_proveedor + " - " + nombre_proveedor;
}
</script>
<style>
    .form-group{
        margin-bottom: 0rem !important;
    }
</style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Servicios</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="float-right">
                            @if($servicio->nombre != 'Servicio logístico')
                                <a href="{{ route('servicios.editar', [$servicio->id_producto]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Editar</a>&nbsp;&nbsp;&nbsp;
                            @endif
                        </div>
                        <br><br>
                        <form class="form-horizontal">
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Nombre</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$servicio->nombre}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Descripción</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$servicio->descripcion}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?php if(env('MANEJO_DOLARES')){ ?>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Precio en dolares</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$servicio->costo_dolares * ($servicio->margen / 100)}}</p>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Precio en pesos</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$servicio->costo_dolares * ($servicio->margen / 100) * $servicio->valor_dolar}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="card-title">Proveedores</h3>
                                            <div class="float-right">
                                                <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#agregarproveedor">
                                                Agregar
                                                </button>
                                            </div>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <table class="table table-bordered">
                                            <thead>                  
                                                <tr>
                                                    <th>Proveedor</th>
                                                    <th>Costo</th>
                                                    <th>Seleccionado</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @php
                                            $idSusProveedores = array();
                                            @endphp
                                            @foreach($susproveedores as $proveedor)
                                            @php
                                            array_push($idSusProveedores, $proveedor->id_user);
                                            @endphp
                                                <tr>
                                                    <td title="{{$proveedor->codigo_proveedor}}">{{$proveedor->name}}</td>
                                                    <td>{{$proveedor->costo}}</td>
                                                    <td>{{$proveedor->seleccionado ? "SI" : "NO"}}</td>
                                                    <td>
                                                    <a href="#" class="btn btn-sm btn-warning" onclick="return editarCosto('{{$proveedor->id_costo}}','{{$proveedor->costo}}','{{$proveedor->name}}','{{$proveedor->codigo_proveedor}}')" data-toggle="modal" data-target="#editarcosto"><i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Editar</a>&nbsp;&nbsp;&nbsp;
                                                        <a href="{{ route('servicios.eliminarproveedor', [$servicio->id_producto, $proveedor->id_costo]) }}" class="btn btn-sm btn-danger" onclick="return confirmar('quitar al proveedor',this,event);"><i class="fa fa-trash-alt "></i>&nbsp;&nbsp;&nbsp;Borrar</a>&nbsp;&nbsp;&nbsp;
                                                        <a href="{{ route('servicios.seleccionarproveedor', [$servicio->id_producto, $proveedor->id_user]) }}" class="btn btn-sm btn-primary" {{$proveedor->seleccionado? "hidden":""}}><i class="fa fa-hand-pointer"></i>&nbsp;&nbsp;&nbsp;Seleccionar</a>&nbsp;&nbsp;&nbsp;
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                            </table>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.tab-pane -->
    <!-- Modal agregar proveedor-->
<div class="modal fade" id="agregarproveedor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <form role="form" id="quickForm" method="post" action="{{ route('servicios.agregarproveedor', [$servicio->id_producto]) }}">
        @csrf
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Agregar proveedor</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group col-sm-12">
                        <div class="form-group">
                        <label for="proveedor_id">Proveedor</label>
                        <select name="proveedor_id" class="form-control" id="proveedor_id" required>
                            <option value="">Seleccionar</option>
                            @foreach($proveedores as $el_proveedor)
                            @if(!in_array($el_proveedor->id_user, $idSusProveedores))
                            <option value="{{$el_proveedor->id_user}}">{{$el_proveedor->name}}</option>
                            @endif
                            @endforeach
                        </select>
                        </div>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="codigo_proveedor">Codigo servicio-proveedor</label>
                        <input type="text" name="codigo_proveedor" class="form-control" id="codigo_proveedor" placeholder="Ingresar codigo servicio-proveedor">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="costo_dolares">Costo{{env('MANEJO_DOLARES', false) ? ' en dolares': ''}}</label>
                        <input type="number" step="0.01" name="costo_dolares" class="form-control" id="costo_dolares" placeholder="Ingresar costo{{env('MANEJO_DOLARES', false) ? ' en dolares': ''}}" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-sm btn-success">Agregar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- Modal editar costo -->
<div class="modal fade" id="editarcosto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <form role="form" id="quickForm" method="post" action="{{ route('servicios.editarcosto', [$servicio->id_producto]) }}">
        @csrf
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="titulo_editar_costo">Editar costo: </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group col-sm-12">
                        <div class="form-group">
                        <input type="hidden" name="id_costo_modal" class="form-control" id="id_costo_modal" required>
                        </div>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="codigo_proveedor_modal">Codigo servicio-proveedor</label>
                        <input type="text" name="codigo_proveedor_modal" class="form-control" id="codigo_proveedor_modal" placeholder="Ingresar codigo servicio-proveedor">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="stock_modal">Costo{{env('MANEJO_DOLARES', false) ? ' en dolares': ''}}</label>
                        <input type="number" step="0.01" name="costo_modal" class="form-control" id="costo_modal" placeholder="Ingresar costo{{env('MANEJO_DOLARES', false) ? ' en dolares': ''}}" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                  <button type="submit" class="btn btn-sm btn-success">Editar</button>
                </div>
            </div>
        </div>
    </form>
</div>
@stop