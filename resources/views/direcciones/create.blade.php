@extends('adminlte::page')

@section('content')
<head>    
  <!-- Meta -->
  {{-- <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8"> --}}

  <!-- Script -->
  {{-- <script src="/js/jquery-3.5.1.min.js" type="text/javascript"></script> --}}

  {{-- javascript code --}}
  @if(env('AUTOCOMPLETE_DIRECCION', false))
  <script async
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCSScLFBA4mU-Gc4CD1hwahrjABPV-6b08&libraries=places&callback=initAutocomplete">
  </script>
  @endif
</head>
<body>
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Dirección <small>Crear</small></h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" id="quickForm" method="post" action="{{ route('direcciones.guardar', [$id_user]) }}">
            @csrf
            <div class="card-body">
              <div class="row">
                <div class="form-group col-sm-6">
                  <label for="direccion">Dirección</label>
                  <input type="text" name="direccion" class="form-control" id="direccion" placeholder="Ingresar dirección" required>
                </div>
                <div class="form-group col-sm-6">
                  <label for="observaciones">Observaciones</label>
                  <input type="text" name="observaciones" class="form-control" id="observaciones" placeholder="Ingresar observaciones">
                </div>
              </div>
              <div class="row">
                <div class="form-group col-sm-6">
                  <label for="provincia">Provincia</label>
                  <input type="text" name="provincia" class="form-control" id="provincia" placeholder="Ingresar provincia" required>
                </div>
                <div class="form-group col-sm-6">
                  <label for="localidad">Localidad</label>
                  <input type="text" name="localidad" class="form-control" id="localidad" placeholder="Ingresar localidad" required>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-sm-6">
                  <label for="codigo_postal">Código postal</label>
                  <input type="text" name="codigo_postal" class="form-control" id="codigo_postal" placeholder="Ingresar código postal" required>
                </div>
                <div class="form-group col-sm-6">
                  <label for="telefono">Teléfono</label>
                  <input type="text" name="telefono" class="form-control" id="telefono" placeholder="Ingresar teléfono">
                </div>
              </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <button type="submit" class="btn btn-success">Crear</button>
            </div>
          </form>
        </div>
        <!-- /.card -->
        </div>
      <!--/.col (left) -->
      <!-- right column -->
      <div class="col-md-6">

      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
  <script>
      $(document).ready(function() {
              $("#lat_area").addClass("d-none");
              $("#long_area").addClass("d-none");
      });
      </script>
      <script>
      
      let autocomplete;
      let direccionField;
      let codigo_postalField;

      function initAutocomplete() {
          direccionField = document.querySelector("#direccion");
          codigo_postalField = document.querySelector("#codigo_postal");
          autocomplete = new google.maps.places.Autocomplete(direccionField, {
              componentRestrictions: { country: "ar"},
              fields: ["address_components", "geometry"],
              types: ["address"],
          });
          direccionField.focus();
          autocomplete.addListener("place_changed", fillInAddress);
      }
      function fillInAddress() {
          
          const place = autocomplete.getPlace();
          let address1 = "";
          let postcode = "";

          for (const component of place.address_components) {
              
              const componentType = component.types[0];

              switch (componentType) {
                  case "street_number": {
                      address1 = `${component.long_name} ${address1}`;
                      break;
                  }

                  case "route": {
                      address1 += component.short_name;
                      break;
                  }

                  case "postal_code": {
                      postcode = `${component.long_name}${postcode}`;
                      break;
                  }

                  case "postal_code_suffix": {
                      postcode = `${postcode}-${component.long_name}`;
                      break;
                  }

                  case "locality":
                      (document.querySelector("#localidad")).value =
                      component.long_name;
                      break;

                  case "administrative_area_level_1": {
                      (document.querySelector("#provincia")).value =
                      component.short_name;
                      break;
                  }
              }
          }
          direccionField.value = address1;
          codigo_postalField.value = postcode;
      }
  </script>
  <style>
      .pac-container {
          z-index: 10000 !important;
      }
  </style>
</body>
@stop
