@extends('adminlte::page')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Dirección</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <form class="form-horizontal">
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Direccion</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$direccion->direccion}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Observaciones</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$direccion->observaciones}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Provincia</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$direccion->provincia}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Localidad</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$direccion->localidad}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Código postal</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$direccion->codigo_postal}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Teléfono</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$direccion->telefono}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Estado</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$direccion->estado ? "HABILITADA" : "DESHABILITADA"}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Dirección de facturación</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$direccion->es_direccion_fact ? "SI" : "NO"}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                            @php
                                if($roleUser == 'Proveedor'){
                            @endphp
                                    <a href="{{ route('proveedores.ver', [$direccion->user_id]) }}" class="btn btn-sm btn-success"><i class="fa fa-reply"></i>&nbsp;&nbsp;&nbsp;Ver proveedor</a>&nbsp;&nbsp;&nbsp;
                            @php
                                }else{
                            @endphp
                                    <a href="{{ route('clientes.ver', [$direccion->user_id]) }}" class="btn btn-sm btn-success"><i class="fa fa-reply"></i>&nbsp;&nbsp;&nbsp;Ver cliente</a>&nbsp;&nbsp;&nbsp;
                            @php
                                }
                            @endphp
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.tab-pane -->
@stop