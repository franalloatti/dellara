@extends('adminlte::page')

@section('content')
<head>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Devolución</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="float-right">
                            &nbsp;&nbsp;&nbsp;
                            @if($pedido->estado == 'Solicitada')
                            <a href="{{ route('devoluciones.aceptar', [$pedido->id_pedido]) }}" onclick="return confirmar('aceptar la solicitud',this,event);" class="btn btn-sm btn-success"><i class="fa fa-check"></i>&nbsp;&nbsp;&nbsp;Aceptar</a>&nbsp;&nbsp;&nbsp;
                            <a href="{{ route('devoluciones.rechazar', [$pedido->id_pedido]) }}" onclick="return confirmar('rechazar la solicitud',this,event);" class="btn btn-sm btn-danger"><i class="fa fa-times "></i>&nbsp;&nbsp;&nbsp;Rechazar</a>&nbsp;&nbsp;&nbsp;
                            <a href="{{ route('devoluciones.editar', [$pedido->id_pedido]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Editar</a>&nbsp;&nbsp;&nbsp;
                            @else
                            Estado: {{$pedido->estado}}
                            @endif
                        </div>
                        <br><br>
                        <form class="form-horizontal">
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Cliente</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$pedido->name}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Motivo</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$pedido->motivo_devolucion == 'defecto' ? 'Producto defectuoso' : 'Error en pedido'}}</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Direccion de entrega</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$pedido->direccion}} - {{$pedido->localidad}} ({{$pedido->provincia}})</p>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Productos</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <table class="table table-bordered">
                                        <thead>                  
                                            <tr>
                                            <th>Codigo</th>
                                            <th>Nombre</th>
                                            <th>Cantidad</th>
                                            <th>Precio unitario</th>
                                            <th>Precio total</th>
                                            <!-- <th>Estado</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @php 
                                        $total_sin_descuento = 0.00;
                                        @endphp
                                        @foreach($productos as $producto)
                                        @php 
                                        $total_sin_descuento = $total_sin_descuento + ($producto->cantidad * $producto->precio_unitario);
                                        @endphp
                                            <tr>
                                            <td>{{$producto->codigo}}</td>
                                            <td>{{$producto->nombre}}</td>
                                            <td>{{$producto->cantidad}}</td>
                                            <td style="white-space: nowrap;">$ {{number_format($producto->precio_unitario,2,",",".")}}</td>
                                            <td style="white-space: nowrap;">$ {{number_format($producto->cantidad * $producto->precio_unitario,2,",",".")}}</td>
                                            </tr>
                                            @endforeach
                                            <tr>
                                            <td colspan="4"><strong>TOTALES</strong></td>
                                            <td style="white-space: nowrap;"><strong>$ {{number_format($total_sin_descuento,2,",",".")}}</strong></td>
                                            </tr>
                                        </tbody>
                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.tab-pane -->
    </body>
@stop