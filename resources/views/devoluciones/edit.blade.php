@extends('adminlte::page')

@section('content')
  {{-- <head>

    <!-- Meta -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">


    <!-- CSS -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

    <!-- Script -->
    <script src="/js/jquery-3.5.1.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>

  </head> --}}
  <body onload="cargarDatos();">
    <script type="text/javascript">
      $(document).ready(function(){
        // $(window).keydown(function(event){
        //   if(event.keyCode == 13) {
        //     event.preventDefault();
        //     return false;
        //   }
        // });
        $('.js-example-basic-single').select2({matcher: matcher});
        $('#user_id_user').on('change', function(e){
          document.getElementById('producto_id_producto').disabled = true;
          var user_id_user = e.target.value;
          $.get('../getproductos/'+user_id_user, function(data){
          var producto_id_producto = '<option value="" disabled selected>Seleccionar</option>'
            for (var i=0; i<data.length;i++)
            producto_id_producto+='<option value="'+data[i].id_pedido_producto+'" cantidad="'+data[i].cantidad+'" codigo="'+data[i].codigo+'" nombre="'+data[i].nombre+'">'+data[i].codigo+' - '+data[i].nombre+' (Cant: '+data[i].cantidad+' u.)</option>';
            document.getElementById('producto_id_producto').disabled = false;
            $("#producto_id_producto").html(producto_id_producto);
          }).fail(response => {
            toastr.error(response.responseJSON.message);
          });
        });
      });
      function borrar(id_producto){
        $("#fila" + id_producto).remove();
        $('#producto_id_producto option').each(function(i,elem){
          var elid = parseInt($(elem).attr('value'));
          if(id_producto == elid){
            $(elem).attr('disabled',false); 
          }
        });
        actualizarTotales();
      };
      function funcion(){
        var combo = document.getElementById("producto_id_producto");
        var producto_seleccionado = combo.options[combo.selectedIndex];
        var texto = '<tr id="fila'+producto_seleccionado.value+'">'
        +'  <input name="items['+producto_seleccionado.value+'][id]" value="'+producto_seleccionado.value+'" type="hidden" class="form-control" id="items['+producto_seleccionado.value+'][id]">'
        +'  <td>'+producto_seleccionado.getAttribute('codigo')+'</td>'
        +'  <td>'+producto_seleccionado.getAttribute('nombre')+'</td>'
        +'  <td><input name="items['+producto_seleccionado.value+'][cantidad]" type="number" max="'+producto_seleccionado.getAttribute('cantidad')+'" value="'+producto_seleccionado.getAttribute('cantidad')+'" class="form-control" id="items['+producto_seleccionado.value+'][cantidad]"></td>'
        +'  <td><button onclick="return borrar('+producto_seleccionado.value+');" class="btn btn-danger">Eliminar</button></td>'
        +'</tr>'
        $('#tablaproductos').append(texto);
        producto_seleccionado.disabled = true;
        $('#producto_id_producto').val($('#producto_id_producto option:eq(0)').val()).trigger('change');
        return false;
      };
      function actualizarTotales(){
        var cantidadItems = 0;
        var precio_final = 0;
        $("#tablaproductos tr").each(function(index, tr) { 
          cantidadItems = cantidadItems + 1;
          precio_final = parseFloat(precio_final) + parseFloat($(this).find("input[name*='[precio_final]']").val());
        });
        document.getElementById('precio_final_final').innerHTML = '$ ' + parseFloat(precio_final).toFixed(2);
      };
      function actualizar(id_producto){
        var precio_unitario = document.getElementById('items['+id_producto+'][precio_unitario]').value;
        var cantidad = document.getElementById('items['+id_producto+'][cantidad]').value;
        document.getElementById('items['+id_producto+'][precio_final]').value = parseFloat(precio_unitario*cantidad).toFixed(2);
        actualizarTotales();
      };
    </script>
    <style>
    .select2-container .select2-selection--single{
      height: auto !important;
    }
    .select2-container {
      width: 100% !important;
    }
    </style>
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- jquery validation -->
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">Devolución <small>Editar</small></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" id="quickForm" method="post" action="{{ route('devoluciones.actualizar', [$pedido->id_pedido]) }}">
              @csrf
              <input name="user_id_user" value="{{$pedido->user_id_user}}" type="hidden" class="form-control" id="user_id_user">
              <input name="descuentoCliente" value="{{$pedido->discount}}" type="hidden" class="form-control" id="descuentoCliente">
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-6">
                    <label for="inputName">Cliente</label>
                    <p>{{$pedido->name}}</p>
                  </div>
                  <div class="form-group col-sm-6">
                    <label for="motivo_devolucion">Motivo</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <select name="motivo_devolucion" class="form-control js-example-basic-single" id="motivo_devolucion" required>
                      <option value="" disabled>Seleccionar</option>
                      <option value="defecto" {{$pedido->motivo_devolucion == 'defecto' ? 'selected' : ''}}>Producto defectuoso</option>
                      <option value="error" {{$pedido->motivo_devolucion == 'error' ? 'selected' : ''}}>Error en pedido</option>
                    </select>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="form-group col-sm-12">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Codigo</th>
                          <th>Producto</th>
                          <th>Precio unitario</th>
                          <th>Cantidad</th>
                          <th>Precio total</th>
                        </tr>
                      </thead>
                      <tbody id="tablaproductos">
                        @php 
                        $total_sin_descuento = 0.00;
                        @endphp
                        @foreach($productosPedido as $prod)
                        @php 
                        $total_sin_descuento = $total_sin_descuento + ($prod->cantidad * $prod->precio_unitario);
                        @endphp
                        <tr id="fila{{$prod->id_pedido_producto}}">
                          <input name="items[{{$prod->id_pedido_producto}}][id]" value="{{$prod->id_pedido_producto}}" type="hidden" class="form-control" id="items[{{$prod->id_pedido_producto}}][id]">
                          <td>{{$prod->codigo}}</td>
                          <td>{{$prod->nombre}}</td>
                          <td><input name="items[{{$prod->id_pedido_producto}}][precio_unitario]" step="0.01" type="number" onchange="return actualizar({{$prod->id_pedido_producto}})" value="{{$prod->precio_unitario}}" class="form-control" id="items[{{$prod->id_pedido_producto}}][precio_unitario]"></td>
                          <td><input name="items[{{$prod->id_pedido_producto}}][cantidad]" type="number" value="{{$prod->cantidad}}" onchange="return actualizar({{$prod->id_pedido_producto}})" class="form-control" id="items[{{$prod->id_pedido_producto}}][cantidad]"></td>
                          <td><input name="items[{{$prod->id_pedido_producto}}][precio_final]" type="number" readonly value="{{$prod->precio_unitario * $prod->cantidad}}" class="form-control" id="items[{{$prod->id_pedido_producto}}][precio_final]"></td>
                        </tr>
                        @endforeach
                      </tbody>
                      <tbody>
                          <th colspan="4">Totales</th>
                          <th id="precio_final_final" name="precio_final_final">$ {{number_format($total_sin_descuento,2,",",".")}}</th>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-success">Editar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
@stop
