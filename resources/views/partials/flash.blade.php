@if($message = session('message'))
    <div class="alert alert-{{ session('type') }}">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {!! $message !!}
    </div>
@endif