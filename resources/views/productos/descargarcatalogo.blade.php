@extends('adminlte::page')

@section('content')
{{-- <head>    
<!-- Meta -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">

<!-- Script -->
<script src="/js/jquery-3.5.1.min.js" type="text/javascript"></script>
</head> --}}
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info">
                    <form role="form" id="quickForm" method="post" action="{{ route('productos.lista_precios') }}">
                    @csrf
                    <div class="card-header">
                        <h3 class="card-title">Formato de descarga</h3>
                        <button type="button" class="close" data-dismiss="card" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="observaciones">Formato</label>
                                <select id="formato" name="formato" class="form-control">
                                <option value="pdf">PDF</option>
                                <option value="excel">Excel</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="card">Cancelar</button>
                        <button type="submit" class="btn btn-sm btn-success">Descargar</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
@stop