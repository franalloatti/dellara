@extends('adminlte::page')

@section('content')
 <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Drag And Drop File Upload In Laravel 7 Using Dropzone Js</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.js"></script>
 </head>
 <body>
  <div class="container-fluid">
    <div>
        <a class="btn btn-info" href="/admin/productos/index"><i class="fa fa-reply"></i>&nbsp;&nbsp;&nbsp;Listar productos</a>
    </div>
    <h3 align="center">Agregar imagenes a producto</h3>
    <br />
        
      <div class="panel panel-default">
        <!-- <div class="panel-heading">
          <h3 class="panel-title">Seleccionar Imagenes</h3>
        </div> -->
        <div class="panel-body">
          <form id="dropzoneForm" class="dropzone" action="{{ route('productos.upload_image', ['id_producto' => $id_producto]) }}">
            @csrf
            <div class="dz-message" data-dz-message><span>Arrastre sus imagenes aquí</span></div>
          </form>
          <br>
          <div align="center">
            <button type="button" class="btn btn-success" id="submit-all">Cargar</button>
          </div>
        </div>
      </div>
      <br />
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 align="center" class="panel-title">Imagenes cargadas</h3>
          <br>
        </div>
        <div align="center" class="panel-body" id="uploaded_image"><br><br>
            <div class="loader"></div>
        </div>
      </div>
    </div>
 </body> 
 <style>
 .loader {
  border: 16px solid #f3f3f3; /* Light grey */
  border-top: 16px solid #3498db; /* Blue */
  border-radius: 50%;
  width: 120px;
  height: 120px;
  animation: spin 2s linear infinite;
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
<script type="text/javascript">
 
  Dropzone.options.dropzoneForm = {
    autoProcessQueue : false,
    acceptedFiles : ".png,.jpg,.gif,.bmp,.jpeg",
 
    init:function(){
      var submitButton = document.querySelector("#submit-all");
      myDropzone = this;
 
      submitButton.addEventListener('click', function(){
        myDropzone.processQueue();
      });
 
      this.on("complete", function(){
        if(this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0)
        {
          var _this = this;
          _this.removeAllFiles();
        }
        load_images();
      });
 
    }
 
  };
 
  load_images();
 
  function load_images()
  {
    $("body").css('cursor','wait'); 
    $.ajax({
      url:"{{ route('productos.fetch_image', ['id_producto' => $id_producto]) }}",
      success:function(data)
      {
        $('#uploaded_image').html(data);
        $("body").css('cursor','default');
      }
    })
  }
 
  $(document).on('click', '.remove_image', function(){
    $("body").css('cursor','wait');
    var name = $(this).attr('name');
    var id = $(this).attr('id');
    $.ajax({
      url:"{{ route('productos.delete_image', ['id_producto' => $id_producto]) }}",
      data:{name : name,
            id : id},
      success:function(data){
        load_images();
        $("body").css('cursor','default');
      }
    })
  });

  $(document).on('click', '.ppal_image', function(){
    $("body").css('cursor','wait');
    var id = $(this).attr('id');
    $.ajax({
      url:"{{ route('productos.ppal_image', ['id_producto' => $id_producto]) }}",
      data:{id : id},
      success:function(data){
        load_images();
        $("body").css('cursor','default');
      }
    })
  });
 
</script>
@stop