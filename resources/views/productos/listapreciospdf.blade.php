<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Lista de precios al <?php echo date('d-m-Y');?></title>

        <style>
            #table2 {
              border-collapse: collapse;
              border-spacing: 10px;  
              font-size: 10px;  
              border: 1px solid black;
            }
            table, th, td {
                border: 1px solid black;
            }
            h3 {
                font-size: 20px;
            }
        </style>
    </head>
    <body>
        <h3 class="mb-3" style="text-align:center">Lista de precios al <?php echo date('d-m-Y');?></h3>
        <table id="table2">
            <thead>                  
                <tr>
                    <th>Rubro</th>
                    <th>Marca</th>
                    <th>Codigo</th>
                    <th>Nombre</th>
                    <th>Precio</th>
                </tr>
            </thead>
            <tbody>
                @foreach($productos as $producto)
                @php
                    if($producto->tiene_fijado == 1){
                        $precio = $producto->precio_fijado;
                    }else{
                        $precio = ($producto->costo * ($producto->margen / 100) * ($producto->valor_dolar) * ( 1 - ($midescuento/100)));
                    }
                @endphp
                <tr>
                    <td>{{$producto->rubro}}</td>
                    <td>{{$producto->marca}}</td>
                    <td>{{$producto->codigo}}</td>
                    <td>{{$producto->nombre}}</td>
                    <td align="right" style="white-space: nowrap;">$ {{number_format($precio,2,",",".")}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <br>
    </body>
</html>