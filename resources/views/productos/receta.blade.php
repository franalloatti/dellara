@extends('adminlte::page')

@section('content')
{{-- <head>

    <!-- Meta -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">


    <!-- CSS -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

    <!-- Script -->
    <script src="/js/jquery-3.5.1.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>

  </head> --}}
  <body>
    <script type="text/javascript">
      $(document).ready(function(){
        // $(window).keydown(function(event){
        //   if(event.keyCode == 13) {
        //     event.preventDefault();
        //     return false;
        //   }
        // });
        $('.js-example-basic-single').select2({matcher: matcher});
      });
      function aceptar(e){
        e.preventDefault();
        var bandera = 0;
        var confirmacion = false;
        if($("#tablaproductos tr").length == 0){
          toastr.error('No se puede generar una receta vacía');
          return false;
        }else{
          $("#tablaproductos tr").each(function(index, tr) { 
              if($(this).find("input[name*='[cantidad]']").val() <= 0){
                bandera = 1;
                toastr.error('No se puede generar una receta con un item con cantidad 0 (cero)');
                return false;
              }
          }).promise().done( function() {
              if(!bandera){
                swal({
                  title: 'Esta seguro/a?',
                  text: 'Esta seguro/a que desea confirmar la receta?',
                  icon: 'warning',
                  buttons: ["Cancelar", "Confirmar"],
                }).then(function(value) {
                    if (value) {
                      $('#quickForm').submit();
                    }
                });
              }
          });
        }
      };
      function borrar(id_producto){
        $("#fila" + id_producto).remove();
        $('#producto_id option').each(function(i,elem){
          var elid = parseInt($(elem).attr('value'));
          if(id_producto == elid){
            $(elem).attr('disabled',false); 
          }
        });   
      };
      function funcion(){
        var producto_seleccionado = $('#producto_id').val();
        if(document.getElementById('items['+producto_seleccionado+'][id]') != null){
          toastr.error("El producto ya se encuentra en la receta");
          return false;
        }else{
          $("#producto_id option").each(function(){
            if($(this).attr('value') != ''){
              if(producto_seleccionado == $(this).attr('value')){
                var texto = '<tr id="fila'+$(this).attr('value')+'">'
                +'  <input name="items['+$(this).attr('value')+'][id]" value="'+$(this).attr('value')+'" type="hidden" class="form-control" id="items['+$(this).attr('value')+'][id]">'
                +'  <td>'+$(this).attr('nombre')+'</td>'
                +'  <td>'+$(this).attr('unidad')+'</td>'
                +'  <td><input name="items['+$(this).attr('value')+'][cantidad]" type="number" value="0" class="form-control" id="items['+$(this).attr('value')+'][cantidad]"></td>'
                +'  <td><button onclick="return borrar('+$(this).attr('value')+');" class="btn btn-danger">Eliminar</button></td>'
                +'</tr>'
                $('#tablaproductos').append(texto);
                return;
              }
            }
          });
          $('#producto_id').val($('#producto_id option:eq(0)').val()).trigger('change');
          return false;
        }
      };
    </script>
    <style>
    .select2-container .select2-selection--single{
      height: auto !important;
    }
    .select2-container {
      width: 100% !important;
    }
    </style>
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Receta</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="quickForm" method="post" action="{{ route('productos.configreceta', $producto->id_producto) }}">
                @csrf
                <div class="card-body">
                  <div class="form-group row">
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label for="inputName">Código</label>
                            </div>
                            <div class="col-sm-9">
                                <p>{{$producto->codigo}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label for="inputName">Nombre</label>
                            </div>
                            <div class="col-sm-9">
                                <p>{{$producto->nombre}}</p>
                            </div>
                        </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-2">
                      <label for="producto_id">Insumos</label>
                    </div>
                    <div class="form-group col-sm-6">
                      <select name="producto_id" class="form-control js-example-basic-single" id="producto_id">
                        <option value="" disabled selected>Seleccionar</option>
                        @foreach($insumos as $insumo)
                        <option conversion="{{$insumo->conversion}}" nombre="{{$insumo->nombre}}" unidad="{{$insumo->unidad}}" value="{{$insumo->id_producto}}">{{$insumo->nombre}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="form-group col-sm-2" style="align-self: flex-end;">
                      <button type="button" onclick="return funcion();" id="agregar" name="agregar" class="form-control btn btn-info">Agregar</button>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-12">
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>Insumo</th>
                            <th>Unidad de conversion</th>
                            <th>Cantidad</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody id="tablaproductos">
                        @foreach($susinsumos as $insumo)
                        <tr id="fila{{$insumo->id_producto}}">
                            <input name="items[{{$insumo->id_producto}}][id]" value="{{$insumo->id_producto}}" type="hidden" class="form-control" id="items[{{$insumo->id_producto}}][id]">
                            <td>{{$insumo->nombre}}</td>
                            <td>{{$insumo->unidad}}</td>
                            <td><input name="items[{{$insumo->id_producto}}][cantidad]" type="number" value="{{$insumo->cantidad}}" class="form-control" id="items[{{$insumo->id_producto}}][cantidad]"></td>
                            <td><button onclick="return borrar({{$insumo->id_producto}});" class="btn btn-danger">Eliminar</button></td>
                            </tr>
                        @endforeach
                      </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-success" onclick="aceptar(event);">Crear</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div>
  </body>
</html>
@stop
