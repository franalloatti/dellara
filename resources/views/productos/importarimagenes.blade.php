@extends('adminlte::page')

@section('content')
  {{-- <head>

    <!-- Meta -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">

    <!-- Script -->
    <script src="/js/jquery-3.5.1.min.js" type="text/javascript"></script>

  </head> --}}
  <body>
    <script type="text/javascript">
      function procesar(){
        document.getElementById('errores').value = 'Procesando...';
        document.getElementById('loader').hidden = false;
        $.get('processimages', function(data){
          if(data.errores.length > 1){
            document.getElementById('errores').value = data.errores.join("\n");
          }else{
            document.getElementById('errores').value = 'Se procesaron todas las imagenes con éxito';
          }
          document.getElementById('loader').hidden = true;
        }).fail(response => {
          toastr.error(response.responseJSON.message);
        });
      };
    </script>
    <style>
    .loader {
    border: 8px solid #f3f3f3; /* Light grey */
    border-top: 8px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 40px;
    height: 40px;
    animation: spin 1s linear infinite;
    }

    @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
    }
    </style>
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- jquery validation -->
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">Imagenes <small>Procesar</small></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <div class="card-body">
              <div class="row">
                <div class="col-md-2">
                  <button type="submit" class="btn btn-info" onclick="procesar();">Procesar archivo</button>
                </div>
                <div class="col-md-4" id="loader" hidden>
                  <div class="loader"></div>
                </div>
              </div>
              <br>
              <div class="row">
                <label>Log de procesamiento: </label>
                <textarea class="form-control" id="errores" rows="15"></textarea>
              </div>
            </div>
          </div>
          <!-- /.card -->
          </div>
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-6">

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </body>
@stop
