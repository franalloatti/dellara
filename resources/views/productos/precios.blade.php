@extends('adminlte::page')

@section('content')
  <script>
    function seleccionarproveedor(id_producto, id_proveedor, redirect){
      $("body").css('cursor','wait');
      $.get('seleccionarproveedor/'+id_producto+'/'+id_proveedor+'/'+redirect, function(data){
        $("body").css('cursor','default');  
        var cells = document.getElementsByName('div_proveedor_'+id_producto);
        for (var i = 0; i < cells.length; i++) {
          cells[i].hidden = false;
        }
        document.getElementById('div_proveedor_'+id_producto+'_'+id_proveedor).hidden = true;
        toastr.success('El proveedor se seleccionó con éxito!');
      }).fail(response => {
        toastr.error(response.responseJSON.message);
      });
    }
    function editarmargen(id_costo, margen, costo){
      $("body").css('cursor','wait'); 
      $("#td_costo_"+id_costo).html('<div class="loader"></div>');
      $("#td_margen_"+id_costo).html('<div class="loader"></div>');
      $("#td_precio_dolares_"+id_costo).html('<div class="loader"></div>');
      $("#td_precio_pesos_"+id_costo).html('<div class="loader"></div>');
      $.get('editarmargen',{id_costo, margen, costo}, function(data){
        // $precio_dolares = $producto->costo * ($producto->margen_proveedor/100.00);
        var valor_dolar = $('#td_valor_dolar_'+id_costo).html();
        var precio_dolares = parseFloat(costo) * (parseFloat(margen)/100.0);
        var precio_pesos = precio_dolares * parseFloat(valor_dolar);
        $('#td_margen_'+id_costo).html(margen.replace('.', ','));
        $('#td_costo_'+id_costo).html(costo.replace('.', ','));
        $("#td_precio_dolares_"+id_costo).html(precio_dolares.toFixed(2).replace('.', ','));
        $("#td_precio_pesos_"+id_costo).html(precio_pesos.toFixed(2).replace('.', ','));
        $("body").css('cursor','default'); 
        toastr.success('El costo se actualizó con éxito!');
      }).fail(response => {
        toastr.error(response.responseJSON.message);
      });
    }
    function cargarDatos(id_costo){
      var margen = $('#td_margen_'+id_costo).html();
      var costo = $('#td_costo_'+id_costo).html();
      console.log(margen);
      console.log(costo);
      document.getElementById('margen').value = margen.replace(',', '.');
      document.getElementById('costo').value = costo.replace(',', '.');
      document.getElementById('id_costo').value = id_costo;
    }
  </script>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Precios de productos</h3>
            <div class="card-tools">
              <form role="form" action="{{ route('productos.precios') }}" method="POST">
                @csrf
                <div class="input-group input-group-sm">
                  <select name="marca_id" class="form-control" id="marca_id">
                    <option value="">Marca</option>
                    @foreach($marcas as $marca)
                    @if($marca->id_marca == $marcaSelected)
                        <option value="{{$marca->id_marca}}" selected>{{$marca->nombre}}</option>
                    @else
                        <option value="{{$marca->id_marca}}">{{$marca->nombre}}</option>
                    @endif
                    @endforeach
                  </select>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <select name="rubro_id" class="form-control" id="rubro_id">
                    <option value="">Rubro</option>
                    @foreach($rubros as $rubro)
                    @if($rubro->id_rubro == $rubroSelected)
                        <option value="{{$rubro->id_rubro}}" selected>{{$rubro->nombre}}</option>
                    @else
                        <option value="{{$rubro->id_rubro}}">{{$rubro->nombre}}</option>
                    @endif
                    @endforeach
                  </select>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <select name="user_id" class="form-control" id="user_id">
                    <option value="">Proveedor</option>
                    @foreach($proveedores as $proveedor)
                    @if($proveedor->id_user == $proveedorSelected)
                        <option value="{{$proveedor->id_user}}" selected>{{$proveedor->name}}</option>
                    @else
                        <option value="{{$proveedor->id_user}}">{{$proveedor->name}}</option>
                    @endif
                    @endforeach
                  </select>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="text" id="search" name="search" value="{{$search ? $search : ''}}" class="form-control pull-right" placeholder="Buscar">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered">
              <thead>                  
                <tr> 				
                  <th></th>
                  <th>Producto</th>
                  {{-- <th>Nombre</th> --}}
                  <th>Marca</th>
                  <th>Rubro</th>
                  <th>Proveedor (Cod.)</th>
                  <th>Costo</th>
                  <th>Margen</th>
                  @if(env('MANEJO_DOLARES', false))
                  <th>Precio dolar</th>
                  @endif
                  <th>Precio pesos</th>
                  <th style="min-width: 100px;">Acciones</th>
                </tr>
              </thead>
              <tbody>
                @foreach($productos as $producto)
                @php
                  $precio_dolares = $producto->costo * ($producto->margen_proveedor/100.00);
                  $precio_pesos = $producto->costo * ($producto->margen_proveedor/100.00) * $producto->valor_dolar;
                @endphp
                <tr>
                  <td>
                      @php
                      if(!empty($producto->nombre_imagen)){
                      @endphp
                        <a href="/images/{{$producto->id_producto}}/{{$producto->nombre_imagen}}" target="_blank">
                          <img src="/images/{{$producto->id_producto}}/MIN{{$producto->nombre_imagen}}" 
                            class="img-thumbnail" width="50" height="50"
                          />
                        </a>
                      @php
                      }
                      @endphp
                  </td>
                  <td>{{$producto->codigo}} - {{$producto->nombre}}</td>
                  <td id="td_valor_dolar_{{$producto->id_costo}}" hidden>{{$producto->valor_dolar}}</td>
                  <td>{{$producto->marca}}</td>
                  <td>{{$producto->rubro}}</td>
                  <td>{{$producto->proveedor}} ({{$producto->codigo_proveedor}})</td>
                  <td id="td_costo_{{$producto->id_costo}}">{{number_format($producto->costo,2,",","")}}</td>
                  <td id="td_margen_{{$producto->id_costo}}">{{number_format($producto->margen_proveedor,2,",","")}}</td>
                  @if(env('MANEJO_DOLARES', false))
                  <td id="td_precio_dolares_{{$producto->id_costo}}">{{number_format($precio_dolares,2,",","")}}</td>
                  @endif
                  <td id="td_precio_pesos_{{$producto->id_costo}}">{{number_format($precio_pesos,2,",","")}}</td>
                  <td>
                    <div style="display: flex;">
                      <a title="Editar" data-toggle="modal" data-target="#editarMargen" class="btn btn-sm btn-warning"
                        onclick="cargarDatos('{{$producto->id_costo}}')">
                        <i class="fa fa-edit"></i>
                      </a>
                      <div id="div_proveedor_{{$producto->id_producto}}_{{$producto->id_user}}" name="div_proveedor_{{$producto->id_producto}}"
                        {{$producto->seleccionado? "hidden":""}}> 
                        <a title="Seleccionar" class="btn btn-sm btn-primary"
                          onclick="seleccionarproveedor('{{$producto->id_producto}}', '{{$producto->id_user}}', 0)">
                          <i class="fa fa-hand-pointer"></i>
                        </a>
                      </div>
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  
    <!-- Modal agregar proveedor-->
<div class="modal fade" id="editarMargen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  {{-- <form role="form" id="quickForm" method="post" action="{{ route('productos.editarmargen') }}"> --}}
      {{-- @csrf --}}
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Editar margen</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <input type="hidden" name="id_costo" class="form-control" id="id_costo" required>
                  <div class="form-group col-sm-12">
                      <label for="costo">Costo</label>
                      <input type="number" step="0.01" name="costo" class="form-control" id="costo" required>
                  </div>
                  <div class="form-group col-sm-12">
                      <label for="margen">Margen</label>
                      <input type="number" step="0.01" name="margen" class="form-control" id="margen" required>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                  <button type="submit" class="btn btn-sm btn-success" data-dismiss="modal" onclick="editarmargen($('#id_costo').val(),$('#margen').val(),$('#costo').val())">Editar</button>
              </div>
          </div>
      </div>
  {{-- </form> --}}
</div>

<style>
  .table{
      font-size: 13px;
  }
  .form-control{
      font-size: 13px;
  }
  .form-control{
      font-size: 13px;
  }
  .loader {
      border: 3px solid #f3f3f3; /* Light grey */
      border-top: 3px solid #3498db; /* Blue */
      border-radius: 50%;
      width: 25px;
      height: 25px;
      animation: spin 2s linear infinite;
  }

  @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
  }
</style>
@stop
