@extends('adminlte::page')

@section('content')
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Productos</h3>
                <div class="card-tools">
                  <form role="form" action="{{ route('productos.index') }}" method="POST">
                    @csrf
                    <div class="input-group input-group-sm">
                      <select name="limit" class="form-control" id="limit">
                          <option value="20" <?php if($limit == 20) echo 'selected';?>>20 registros</option>
                          <option value="50" <?php if($limit == 50) echo 'selected';?>>50 registros</option>
                          <option value="100" <?php if($limit == 100) echo 'selected';?>>100 registros</option>
                      </select>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <select name="marca_id" class="form-control" id="marca_id">
                        <option value="">Marca</option>
                        @foreach($marcas as $marca)
                        @if($marca->id_marca == $marcaSelected)
                            <option value="{{$marca->id_marca}}" selected>{{$marca->nombre}}</option>
                        @else
                            <option value="{{$marca->id_marca}}">{{$marca->nombre}}</option>
                        @endif
                        @endforeach
                      </select>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <select name="rubro_id" class="form-control" id="rubro_id">
                          <option value="">Rubro</option>
                          @foreach($rubros as $rubro)
                          @if($rubro->id_rubro == $rubroSelected)
                              <option value="{{$rubro->id_rubro}}" selected>{{$rubro->nombre}}</option>
                          @else
                              <option value="{{$rubro->id_rubro}}">{{$rubro->nombre}}</option>
                          @endif
                          @endforeach
                      </select>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <input type="text" id="search" name="search" value="{{$search ? $search : ''}}" class="form-control pull-right" placeholder="Buscar">
                      <div class="input-group-btn">
                        <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th>Imagen ppal</th>
                      <th>Codigo</th>
                      <th>Nombre</th>
                      <th>Costo</th>
                      <th>Precio</th>
                      <th>Stock</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($productos as $producto)
                    <tr>
                      <td>
                          @php
                          $stock = $producto->cantidad + $producto->mov_destino + $producto->mov_origen + $producto->venta + $producto->devolucion_venta + $producto->compra + $producto->devolucion_compra;
                          if(!empty($producto->nombre_imagen)){
                          @endphp
                            <img src="/images/{{$producto->id_producto}}/MIN{{$producto->nombre_imagen}}" class="img-thumbnail" width="50" height="50"/>
                          @php
                          }
                          if($producto->tiene_fijado == 1){
                              $precio = $producto->precio_fijado;
                          }else{
                              $precio = ($producto->costo * ($producto->margen / 100) * ($producto->valor_dolar));
                          }
                          @endphp
                      </td>
                      <td>{{$producto->codigo}}</td>
                      <td>{{$producto->nombre}}</td>
                      <td align="right" style="white-space: nowrap;">$ {{number_format($producto->costo * ($producto->valor_dolar),2,",",".")}}</td>
                      <td align="right" style="white-space: nowrap;">$ {{number_format($precio,2,",",".")}}</td>
                      <td>{{$stock}}</td>
                      <td>
                        <a href="{{ route('productos.ver', [$producto->id_producto]) }}" class="btn btn-sm btn-success"><i class="fa fa-file "></i>&nbsp;&nbsp;&nbsp;Ver</a>&nbsp;&nbsp;&nbsp;
                        <a href="{{ route('productos.editar', [$producto->id_producto]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Editar</a>&nbsp;&nbsp;&nbsp;
                        <a href="{{ route('productos.borrar', [$producto->id_producto]) }}" class="btn btn-sm btn-danger" onclick="return confirmar('borrar el producto',this,event);"><i class="fa fa-trash-alt "></i>&nbsp;&nbsp;&nbsp;Borrar</a>&nbsp;&nbsp;&nbsp;
                        <a href="{{ route('productos.imagenes', [$producto->id_producto]) }}" class="btn btn-sm btn-info"><i class="fa fa-file-image"></i>&nbsp;&nbsp;&nbsp;Imagenes</a>&nbsp;&nbsp;&nbsp;
                        <a href="{{ route('productos.habilitarDeshabilitar', [$producto->id_producto, $producto->estado]) }}" class="btn btn-sm btn-{{$producto->estado ? 'secondary' : 'primary'}}"><i class="fa {{$producto->estado ? 'fa-times' : 'fa-check'}}"></i>&nbsp;&nbsp;&nbsp;{{$producto->estado ? 'Deshabilitar' : 'Habilitar'}}</a>&nbsp;&nbsp;&nbsp;
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                    {{ $productos->appends(['search' => $search, 'limit' => $limit, 'rubro_id' => $rubroSelected, 'marca_id' => $marcaSelected])->links() }}
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    @stop
