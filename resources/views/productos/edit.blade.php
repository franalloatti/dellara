@extends('adminlte::page')

@section('content')
<div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Producto <small>Editar</small></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="quickForm" method="post" action="{{ route('productos.actualizar', [$producto->id_producto]) }}">
                @csrf
                <input type="hidden" id="conceptoAFIP" value="{{$producto->concepto}}">
                <input type="hidden" id="ivaAFIP" value="{{$producto->iva}}">
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="codigo">Código</label>
                      <input type="text" name="codigo" class="form-control" id="codigo" value="{{$producto->codigo}}" placeholder="Ingresar código" required>
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="nombre">Nombre</label>
                      <input type="text" name="nombre" class="form-control" id="nombre" value="{{$producto->nombre}}" placeholder="Ingresar nombre" required>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="descripcion">Descripción</label>
                      <input type="text" name="descripcion" class="form-control" id="descripcion" value="{{$producto->descripcion}}" placeholder="Ingresar descripción">
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="stock_verde">Nivel de Stock verde</label>
                      <input type="number" name="stock_verde" class="form-control" id="stock_verde" value="{{$producto->stock_verde}}" placeholder="Ingresar nivel stock en verde">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="stock_seguridad">Nivel de Stock seguridad</label>
                      <input type="number" name="stock_seguridad" class="form-control" id="stock_seguridad" value="{{$producto->stock_seguridad}}" placeholder="Ingresar nivel stock de seguridad">
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="stock_amarillo">Nivel de Stock amarillo</label>
                      <input type="number" name="stock_amarillo" class="form-control" id="stock_amarillo" value="{{$producto->stock_amarillo}}" placeholder="Ingresar nivel stock en amarillo">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="margen">Margen</label>
                      <input type="number" step="0.01" name="margen" class="form-control" id="margen" value="{{$producto->margen}}" placeholder="Ingresar margen">
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="tiene_fijado">Precio fijado</label>
                      <input type="checkbox" name="tiene_fijado" class="" {{$producto->tiene_fijado == 1 ? 'checked' : ""}} id="tiene_fijado">
                      <input type="number" step="0.01" name="precio_fijado" class="form-control" id="precio_fijado" value="{{$producto->precio_fijado}}" placeholder="Ingresar precio fijado">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="marca_id">Marca</label>
                      <select name="marca_id" class="form-control" id="marca_id" required>
                        <option value="">Seleccionar</option>
                        @foreach($marcas as $marca)
                          @if($marca->id_marca == $producto->marca)
                            <option value="{{$marca->id_marca}}" selected>{{$marca->nombre}}</option>
                          @else
                            <option value="{{$marca->id_marca}}">{{$marca->nombre}}</option>
                          @endif
                        @endforeach
                      </select>
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="rubro_id">Rubro</label>
                      <select name="rubro_id" class="form-control" id="rubro_id" required>
                        <option value="">Seleccionar</option>
                        @foreach($rubros as $rubro)
                          @if($rubro->id_rubro == $producto->rubro)
                            <option value="{{$rubro->id_rubro}}" selected>{{$rubro->nombre}}</option>
                          @else
                            <option value="{{$rubro->id_rubro}}">{{$rubro->nombre}}</option>
                          @endif
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <div class="form-group">
                        <label for="marca_id">Concepto</label>
                        <select name="concepto" class="form-control" id="concepto" required>
                          <option value="">Seleccionar</option>
                          <option value="1" {{$producto->concepto == 1 ? 'selected' : ""}}>Productos</option>
                          <option value="2" {{$producto->concepto == 2 ? 'selected' : ""}}>Servicios</option>
                          <option value="3" {{$producto->concepto == 3 ? 'selected' : ""}}>Productos y Servicios</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group col-sm-6">
                      <div class="form-group">
                        <label for="iva">IVA</label>
                        <select name="iva" class="form-control" id="iva" requir ed>
                          <option value="">Seleccionar</option>
                          <option value="5" {{$producto->iva == 5 ? 'selected' : ""}}>21%</option>
                          <option value="4" {{$producto->iva == 4 ? 'selected' : ""}}>10.5%</option>
                          <option value="6" {{$producto->iva == 6 ? 'selected' : ""}}>27%</option>
                          <option value="8" {{$producto->iva == 8 ? 'selected' : ""}}>5%</option>
                          <option value="9" {{$producto->iva == 9 ? 'selected' : ""}}>2.5%</option>
                          <option value="3" {{$producto->iva == 3 ? 'selected' : ""}}>0%</option>
                          <option value="2" {{$producto->iva == 2 ? 'selected' : ""}}>No gravado</option>
                          <option value="1" {{$producto->iva == 1 ? 'selected' : ""}}>Exento</option>
                        </select>
                      </div>
                    </div> 
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Editar</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
@stop
@section('js')
    <script>
      $(document).ready(function(){
        var concepto = $('#conceptoAFIP').val();
        var iva = $('#ivaAFIP').val();
        $("#concepto option[value="+concepto+"]").attr("selected", true);
        $("#iva option[value="+iva+"]").attr("selected", true);
      });
    </script>
@endsection