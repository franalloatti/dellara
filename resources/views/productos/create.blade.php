@extends('adminlte::page')

@section('content')
<div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Producto <small>Crear</small></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="quickForm" method="post" action="{{ route('productos.guardar') }}">
                @csrf
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="codigo">Código</label>
                      <input type="text" name="codigo" class="form-control" id="codigo" placeholder="Ingresar código" required>
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="nombre">Nombre</label>
                      <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Ingresar nombre" required>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="descripcion">Descripción</label>
                      <input type="text" name="descripcion" class="form-control" id="descripcion" placeholder="Ingresar descripción">
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="stock_verde">Nivel de stock verde</label>
                      <input type="number" name="stock_verde" class="form-control" id="stock_verde" placeholder="Ingresar nivel stock en verde">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="stock_seguridad">Nivel de stock seguridad</label>
                      <input type="number" name="stock_seguridad" class="form-control" id="stock_seguridad" placeholder="Ingresar nivel stock de seguridad">
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="stock_amarillo">Nivel de stock amarillo</label>
                      <input type="number" name="stock_amarillo" class="form-control" id="stock_amarillo" placeholder="Ingresar nivel stock en amarillo">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="margen">Margen</label>
                      <input type="number" step="0.01" name="margen" class="form-control" id="margen" placeholder="Ingresar margen">
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="tiene_fijado">Precio fijado</label>
                      <input type="checkbox" name="tiene_fijado" class="" id="tiene_fijado">
                      <input type="number" step="0.01" name="precio_fijado" class="form-control" id="precio_fijado" placeholder="Ingresar precio fijado">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="marca_id">Marca</label>
                      <select name="marca_id" class="form-control" id="marca_id" required>
                        <option value="">Seleccionar</option>
                        @foreach($marcas as $marca)
                        <option value="{{$marca->id_marca}}">{{$marca->nombre}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="rubro_id">Rubro</label>
                      <select name="rubro_id" class="form-control" id="rubro_id" required>
                        <option value="">Seleccionar</option>
                        @foreach($rubros as $rubro)
                        <option value="{{$rubro->id_rubro}}">{{$rubro->nombre}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="row">   
                    <div class="form-group col-sm-6">
                      <div class="form-group">
                        <label for="concepto">Concepto</label>
                        <select name="concepto" class="form-control" id="concepto" required>
                          <option value="">Seleccionar</option>
                          <option value="1">Producto</option>
                          <option value="2">Servicio</option>
                          <option value="3">Producto y servicios</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group col-sm-6">
                      <div class="form-group">
                        <label for="iva">IVA</label>
                        <select name="iva" class="form-control" id="iva" requir ed>
                          <option value="">Seleccionar</option>
                          <option value="5">21%</option>
                          <option value="4">10.5%</option>
                          <option value="6">27%</option>
                          <option value="8">5%</option>
                          <option value="9">2.5%</option>
                          <option value="3">0%</option>
                          <option value="2">No gravado</option>
                          <option value="1">Exento</option>
                        </select>
                      </div>
                    </div>                  
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Crear</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
@stop
