@extends('adminlte::page')

@section('content')
    <head>    
    <!-- Meta -->
    {{-- <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">

    <!-- Script -->
    <script src="/js/jquery-3.5.1.min.js" type="text/javascript"></script> --}}

    {{-- javascript code --}}
    @if(env('AUTOCOMPLETE_DIRECCION', false))
    <script async
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCSScLFBA4mU-Gc4CD1hwahrjABPV-6b08&libraries=places&callback=initAutocomplete">
    </script>
    @endif
    </head>
    <body>
        <script type="text/javascript">
            $(document).ready(function(){
                
            });
            function cambiarCantidad(id_item_carrito, cantidad){
                $("body").css('cursor','wait'); 
                $.get('cambiarcantidad', { id_item_carrito, cantidad}, function(data){
                    $('#cantidad_'+id_item_carrito).val(parseInt($('#cantidad_'+id_item_carrito).val()) + parseInt(cantidad));
                    $("body").css('cursor','default'); 
                    toastr.success('Se modificó la cantidad.');
                    actualizarTotales();
                }).fail(response => {
                    $('#cantidad_'+id_item_carrito).val(parseInt($('#cantidad_'+id_item_carrito).val()) - parseInt(cantidad));
                    toastr.error('Hubo un error, por favor reintenta.');
                    $("body").css('cursor','default'); 
                });
            };
            function actualizarTotales(){
                var cantidadItems = 0;
                var precio_final = 0;
                $("#tablaproductos tr").each(function(index, tr) { 
                    var id_item_carrito = parseInt($(this).find("input[id='id_item_carrito']").val());
                    var cantidad = parseInt($("#cantidad_"+id_item_carrito).val());
                    var precio_unitario = parseFloat($(this).find("input[id='precio_unitario']").val());
                    var precio_renglon = cantidad * precio_unitario;
                    precio_final = precio_final + precio_renglon;
                    $("#precio_renglon_"+id_item_carrito).html(precio_renglon.toLocaleString('es-ar', {
                        style: 'currency',
                        currency: 'ARS',
                        minimumFractionDigits: 2
                    }));
                });
                document.getElementById('precio_total').innerHTML = parseFloat(precio_final).toLocaleString('es-ar', {
                    style: 'currency',
                    currency: 'ARS',
                    minimumFractionDigits: 2
                });
            };
            function functionAddDireccion(){
                $("body").css('cursor','wait'); 
                var direccion = $('#direccion').val();
                var observaciones = $('#observaciones').val();
                var provincia = $('#provincia').val();
                var localidad = $('#localidad').val();
                var codigo_postal = $('#codigo_postal').val();
                var telefono = $('#telefono').val();
                $.get('adddireccion', { direccion, observaciones, provincia, localidad, codigo_postal, telefono}, function(data){
                    if(data.success != undefined){
                        var html_old = $("#id_direccion").html();
                        var html_new = '<option value="'+data.id+'">'+direccion+'</option>';
                        $("#id_direccion").html(html_old + html_new);
                        toastr.success(data.success);
                        $("body").css('cursor','default'); 
                        $("#addDireccion").modal("toggle");
                        $("#finalizarCompra").modal("show");
                    }else{
                        $("body").css('cursor','default'); 
                        toastr.error(data.error);
                    }
                }).fail(response => {
                    toastr.error(response.responseJSON.message);
                    $("body").css('cursor','default'); 
                });
            };
        </script>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Carrito</h3>
                            <div class="card-tools">
                                <div class="input-group input-group-sm">
                                    <div class="input-group-btn">
                                    @if(count($productos))
                                    <button data-toggle="modal" data-target="#finalizarCompra" type="button" class="btn btn-sm btn-success">Finalizar compra</button>
                                    @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            @if(count($productos))
                            <table class="table table-bordered">
                                <thead>                  
                                <tr>
                                    <th>Imagen</th>
                                    <th>Codigo</th>
                                    <th>Nombre</th>
                                    <th>Precio unitario</th>
                                    <th>Cantidad</th>
                                    <th>Precio Final</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody id="tablaproductos">
                                @php
                                    $precio_total = 0;
                                @endphp
                                @foreach($productos as $producto)
                                    <tr>
                                        <td>
                                            @php
                                            if($producto->tiene_fijado == 1){
                                                $precio = $producto->precio_fijado;
                                            }else{
                                                $precio = ($producto->costo * ($producto->margen / 100) * (1 - $mirol->discount / 100)  * ($producto->valor_dolar));
                                            }
                                            $precio_total = $precio_total + ($precio * $producto->cantidad);
                                            if(!empty($producto->nombre_imagen)){
                                            @endphp
                                            <a target="_blank" href="/images/{{$producto->id_producto}}/MIN{{$producto->nombre_imagen}}"><img src="/images/{{$producto->id_producto}}/MIN{{$producto->nombre_imagen}}" class="img-thumbnail" width="50" height="50"/></a>
                                            @php
                                            }
                                            @endphp
                                        </td>
                                        <input name="id_item_carrito" value="{{$producto->id_item_carrito}}" type="hidden" class="form-control" id="id_item_carrito">
                                        <input name="precio_unitario" value="{{$precio}}" type="hidden" class="form-control" id="precio_unitario">
                                        <td>{{$producto->codigo}}</td>
                                        <td>{{$producto->nombre}}</td>
                                        <td style="white-space: nowrap;">$ {{number_format($precio,2,",",".")}}</td>
                                        <td>
                                            <div class="input-group number-spinner">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" data-dir="dwn" onclick="cambiarCantidad({{$producto->id_item_carrito}}, -1)"><i class="fa fa-minus"></i></button>
                                                </span>
                                                <input type="text" class="form-control text-center" id="cantidad_{{$producto->id_item_carrito}}" readonly value="{{$producto->cantidad}}" style="width: 20px">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" data-dir="up" onclick="cambiarCantidad({{$producto->id_item_carrito}}, 1)"><i class="fa fa-plus"></i></button>
                                                </span>
                                            </div>
                                        </td>
                                        <td style="white-space: nowrap;" id="precio_renglon_{{$producto->id_item_carrito}}">$ {{number_format(($precio * $producto->cantidad),2,",",".")}}</td>
                                        <td>
                                        <a href="{{ route('productos.deletecarrito', [$producto->id_item_carrito]) }}" title="Eliminar" onclick="return confirmar('quitar del carrito',this,event);" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tbody>
                                    <tr>
                                        <td colspan="5">
                                            <strong>TOTAL</strong>
                                        </td>
                                        <td style="white-space: nowrap;" id="precio_total">$ {{number_format($precio_total,2,",",".")}}</td>
                                        <td>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            @else
                            <h2>Carrito vacío</h2>
                            @endif
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix">

                        </div>
                    </div>
            <!-- /.card -->
                </div>
            </div>
        </div>
        <div class="modal fade" id="finalizarCompra" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <form role="form" id="quickForm" method="post" action="{{ route('productos.comprar') }}">
                @csrf
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">Ingresa una dirección de envío y algunas observaciones</h3>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Direccion de envío:</label>
                                            </div>
                                            <div class="col-md-9">
                                            <select name="id_direccion" class="form-control js-example-basic-single" id="id_direccion" required>
                                                <option value="" disabled selected>Seleccionar</option>
                                                @foreach($direcciones as $direccion)
                                                    <option value="{{$direccion->id_direccion}}">{{$direccion->direccion}}</option>';
                                                @endforeach
                                            </select>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="card-tools">
                                                    <div class="input-group input-group-sm">
                                                        <div class="input-group-btn">
                                                        <button data-toggle="modal" data-target="#addDireccion" type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Agregar una nueva dirección</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Observaciones:</label>
                                            </div>
                                            <div class="col-md-9">
                                                <textarea name="observaciones" rows="5" class="form-control" id="observaciones" placeholder="Ingresar observaciones..."></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-sm btn-success">Continuar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal fade" id="addDireccion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title">Nueva dirección</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="direccion">Dirección</label>
                                <input type="text" name="direccion" class="form-control" id="direccion" placeholder="Ingresar dirección" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="observaciones">Observaciones</label>
                                <input type="text" name="observaciones" class="form-control" id="observaciones" placeholder="Ingresar observaciones">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="provincia">Provincia</label>
                                <input type="text" name="provincia" class="form-control" id="provincia" placeholder="Ingresar provincia" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="localidad">Localidad</label>
                                <input type="text" name="localidad" class="form-control" id="localidad" placeholder="Ingresar localidad" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="codigo_postal">Código postal</label>
                                <input type="text" name="codigo_postal" class="form-control" id="codigo_postal" placeholder="Ingresar código postal" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="telefono">Teléfono</label>
                                <input type="text" name="telefono" class="form-control" id="telefono" placeholder="Ingresar teléfono">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6" id="lat_area">
                                <label for="latitude"> Latitude </label>
                                <input type="text" name="latitude" id="latitude" class="form-control">
                            </div>
                            <div class="form-group col-sm-6" id="long_area">
                                <label for="latitude"> Longitude </label>
                                <input type="text" name="longitude" id="longitude" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-sm btn-success" onclick="return functionAddDireccion();">Agregar</button>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function() {
                    $("#lat_area").addClass("d-none");
                    $("#long_area").addClass("d-none");
            });
            </script>
            <script>
            
            let autocomplete;
            let direccionField;
            let codigo_postalField;

            function initAutocomplete() {
                direccionField = document.querySelector("#direccion");
                codigo_postalField = document.querySelector("#codigo_postal");
                autocomplete = new google.maps.places.Autocomplete(direccionField, {
                    componentRestrictions: { country: "ar"},
                    fields: ["address_components", "geometry"],
                    types: ["address"],
                });
                direccionField.focus();
                autocomplete.addListener("place_changed", fillInAddress);
            }
            function fillInAddress() {
                
                const place = autocomplete.getPlace();
                let address1 = "";
                let postcode = "";

                for (const component of place.address_components) {
                    
                    const componentType = component.types[0];

                    switch (componentType) {
                        case "street_number": {
                            address1 = `${component.long_name} ${address1}`;
                            break;
                        }

                        case "route": {
                            address1 += component.short_name;
                            break;
                        }

                        case "postal_code": {
                            postcode = `${component.long_name}${postcode}`;
                            break;
                        }

                        case "postal_code_suffix": {
                            postcode = `${postcode}-${component.long_name}`;
                            break;
                        }

                        case "locality":
                            (document.querySelector("#localidad")).value =
                            component.long_name;
                            break;

                        case "administrative_area_level_1": {
                            (document.querySelector("#provincia")).value =
                            component.short_name;
                            break;
                        }
                    }
                }
                direccionField.value = address1;
                codigo_postalField.value = postcode;
            }
        </script>
        <style>
            .pac-container {
                z-index: 10000 !important;
            }
        </style>
    </body>
@stop