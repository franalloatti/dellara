@extends('adminlte::page')

@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Ver stock</h3>
            <div class="card-tools">
              <form role="form" action="{{ route('productos.stock', [$id_producto]) }}" method="POST">
                @csrf
                <div class="input-group input-group-sm">
                  <label for="fecha_desde">Almacen: </label>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <select name="almacen_id" class="form-control" id="almacen_id">
                      {{-- <option value="">Seleccione</option> --}}
                      @foreach($almacenes as $almacen)
                      @if($almacen->id_almacen == $selectedAlmacen)
                          <option value="{{$almacen->id_almacen}}" selected>{{$almacen->nombre}}</option>
                      @else
                          <option value="{{$almacen->id_almacen}}">{{$almacen->nombre}}</option>
                      @endif
                      @endforeach
                  </select>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <label for="fecha_desde">Fecha desde: </label>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="date" id="fecha_desde" name="fecha_desde" value="{{$fecha_desde ? $fecha_desde : ''}}" class="form-control pull-right" placeholder="Buscar">
                    <div class="input-group-btn">
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                      <a href="{{ route('productos.stock', [$id_producto]) }}" class="btn btn-default" style="color:#2b2b2b"><i class="fa fa-undo"></i></a>
                    </div>
                </div>
              </form>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered">
              <thead>                  
                <tr> 				

                  <th>Descripción Movimiento</th>
                  <th>Detalle</th>
                  <th>Fecha</th>
                  <th>Movimiento</th>
                  <th>Stock final</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @foreach($movimientos as $movimiento)
                <tr>
                  <td>{{$movimiento->descripcion_movimiento}}</td>
                  <td>{{$movimiento->detalle}}</td>
                  <td>{{date('d/m/Y H:i', strtotime($movimiento->fecha))}}</td>
                  <td>{{$movimiento->cantidad}}</td>
                  <td>{{$movimiento->stock}}</td>
                  <td><a target="_blank" href="{{$movimiento->link}}" class="btn btn-sm btn-success">
                    <i class="fa fa-file"></i>&nbsp;&nbsp;&nbsp;Ver</a>&nbsp;&nbsp;&nbsp;
                  </td>
                </tr>
                @endforeach
                <tr>
                  <td colspan="4">Stock fecha desde</td>
                  <td>{{$stock_final}}</td>
                  <td></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
