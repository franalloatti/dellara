@extends('adminlte::page')

@section('content')
<script>
function editarStock(id_stock, stock, nombre_almacen){
    document.getElementById('id_stock_modal').value = id_stock;
    document.getElementById('stock_modal').value = stock;
    document.getElementById('titulo_editar_stock').innerHTML = "Editar stock: "+ nombre_almacen;
}
function editarCosto(id_costo, costo, nombre_proveedor, codigo_proveedor, margen){
    document.getElementById('id_costo_modal').value = id_costo;
    document.getElementById('costo_modal').value = costo;
    document.getElementById('margen_modal').value = margen;
    document.getElementById('codigo_proveedor_modal').value = codigo_proveedor;
    document.getElementById('titulo_editar_costo').innerHTML = "Editar costo: " + codigo_proveedor + " - " + nombre_proveedor;
}
function cambiarModal(valor){
    if(valor == 'Ajuste'){
        document.getElementById('lblAlmacenDesde').innerHTML = "Almacen";
        document.getElementById('divAlmacenHacia').hidden = true;
    }else if(valor == 'Transferencia'){
        document.getElementById('lblAlmacenDesde').innerHTML = "Almacen desde";
        document.getElementById('divAlmacenHacia').hidden = false;
    }
}
function validar(){
    if(document.getElementById('tipo_movimiento').value == 'Transferencia' && 
    document.getElementById('almacen_id_origen').value == document.getElementById('almacen_id_destino').value){
        toastr.error('No se puede realizar una transferencia al mismo deposito');
        return false;
    }
}
function cambiarMax(almacen_desde){
    var stock = almacen_desde.options[almacen_desde.selectedIndex].getAttribute('stock');
    console.log(stock);
    if(document.getElementById("cantidad").value > stock){
        document.getElementById("cantidad").value = stock;
    }
    document.getElementById("cantidad").max = stock;
}
</script>
<style>
    .form-group{
        margin-bottom: 0rem !important;
    }
</style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Producto</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="float-right">
                            @if(env('RECETA_PRODUCTO',false))
                            <a href="{{ route('productos.receta', [$producto->id_producto]) }}" class="btn btn-sm btn-success"><i class="fa fa-coffee "></i>&nbsp;&nbsp;&nbsp;Receta</a>&nbsp;&nbsp;&nbsp;
                            @endif
                            <a href="{{ route('productos.editar', [$producto->id_producto]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Editar</a>&nbsp;&nbsp;&nbsp;
                            <a href="{{ route('productos.imagenes', [$producto->id_producto]) }}" class="btn btn-sm btn-info"><i class="fa fa-file-image"></i>&nbsp;&nbsp;&nbsp;Imagenes</a>&nbsp;&nbsp;&nbsp;
                        </div>
                        <br><br>
                        <form class="form-horizontal">
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Código</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$producto->codigo}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Nombre</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$producto->nombre}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Descripción</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$producto->descripcion}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Nivel de Stock verde</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$producto->stock_verde}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Nivel de Stock seguridad</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$producto->stock_seguridad}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Nivel de Stock amarillo</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$producto->stock_amarillo}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Margen</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$producto->margen}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Precio fijado: {{$producto->tiene_fijado == 1 ? "SI" : "NO"}}</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$producto->tiene_fijado == 1 ? $producto->precio_fijado : "-"}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?php if(env('MANEJO_DOLARES')){ ?>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Precio en dolares</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$producto->costo_dolares * ($producto->margen / 100)}}</p>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Precio en pesos</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$producto->costo_dolares * ($producto->margen / 100) * $producto->valor_dolar}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Marca</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$producto->marca}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Rubro</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$producto->rubro}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Concepto</label>
                                        </div>
                                        <div class="col-sm-9">
                                            @php
                                                switch ($producto->concepto) {
                                                    case '1':
                                                       $concepto = "Productos";
                                                       break;
                                                    case '2':
                                                        $concepto = "Servicios";
                                                        break;
                                                    case '3':
                                                        $concepto = "Productos y Servicios";
                                                        break;
                                                   default:
                                                        $concepto = "-";
                                                        break;
                                                } 
                                            @endphp
                                            <p>{{$concepto}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Iva</label>
                                        </div>
                                        <div class="col-sm-9">
                                            @php
                                                switch (intval($producto->iva)) {
                                                    case 3:
                                                       $iva = "0%";
                                                       break;
                                                    case 4:
                                                        $iva = "10.5%";
                                                        break;
                                                    case 5:
                                                        $iva = "21%";
                                                        break;
                                                    case 6:
                                                        $iva = "27%";
                                                        break;
                                                    case 8:
                                                        $iva = "5%";
                                                        break;
                                                    case 9:
                                                        $iva = "2.5%";
                                                        break;
                                                   default:
                                                        $iva = "-";
                                                        break;
                                                } 
                                            @endphp
                                            <p>{{$iva}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="card-title">Proveedores</h3>
                                            <div class="float-right">
                                                <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#agregarproveedor">
                                                Agregar
                                                </button>
                                            </div>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <table class="table table-bordered">
                                            <thead>                  
                                                <tr>
                                                    <th>Proveedor</th>
                                                    <th>Costo</th>
                                                    @if(env('MARGEN_PROVEEDOR', false))
                                                    <th>Margen</th>
                                                    @endif
                                                    <th>Seleccionado</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @php
                                            $idSusProveedores = array();
                                            @endphp
                                            @foreach($susproveedores as $proveedor)
                                            @php
                                            array_push($idSusProveedores, $proveedor->id_user);
                                            @endphp
                                                <tr>
                                                    <td title="{{$proveedor->codigo_proveedor}}">{{$proveedor->name}}</td>
                                                    <td>{{$proveedor->costo}}</td>
                                                    @if(env('MARGEN_PROVEEDOR', false))
                                                    <td>{{$proveedor->margen}}</td>
                                                    @endif
                                                    <td>{{$proveedor->seleccionado ? "SI" : "NO"}}</td>
                                                    <td>
                                                    <a href="#" class="btn btn-sm btn-warning" onclick="return editarCosto('{{$proveedor->id_costo}}','{{$proveedor->costo}}','{{$proveedor->name}}','{{$proveedor->codigo_proveedor}}','{{$proveedor->margen}}')" data-toggle="modal" data-target="#editarcosto"><i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Editar</a>&nbsp;&nbsp;&nbsp;
                                                        <a href="{{ route('productos.eliminarproveedor', [$producto->id_producto, $proveedor->id_costo]) }}" class="btn btn-sm btn-danger" onclick="return confirmar('quitar al proveedor',this,event);"><i class="fa fa-trash-alt "></i>&nbsp;&nbsp;&nbsp;Borrar</a>&nbsp;&nbsp;&nbsp;
                                                        <a href="{{ route('productos.seleccionarproveedor', [$producto->id_producto, $proveedor->id_user]) }}" class="btn btn-sm btn-primary" {{$proveedor->seleccionado? "hidden":""}}><i class="fa fa-hand-pointer"></i>&nbsp;&nbsp;&nbsp;Seleccionar</a>&nbsp;&nbsp;&nbsp;
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                            </table>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="card-title">Stock en almacenes</h3>
                                            <div class="float-right">
                                                @if(env('GESTION_STOCK', false) && count($susalmacenes) > 0)
                                                <a href="{{ route('productos.stock', [$producto->id_producto]) }}" class="btn btn-sm btn-warning">
                                                Movimientos
                                                </a>
                                                <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#agregarmovimiento">
                                                Actualización
                                                </button>
                                                        @endif
                                                <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#agregaralmacen">
                                                Habilitar
                                                </button>
                                            </div>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <table class="table table-bordered">
                                            <thead>                  
                                                <tr>
                                                    <th>Almacen</th>
                                                    <th>Stock</th>
                                                    @if(!env('GESTION_STOCK', false))
                                                    <th>Acciones</th>
                                                    @endif
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @php 
                                            $stock_total = 0;
                                            $idSusAlmacenes = array();
                                            @endphp
                                            @foreach($susalmacenes as $almacen)
                                            @php 
                                            $stock = $almacen->stock + $almacen->mov_destino + $almacen->mov_origen + $almacen->venta + $almacen->devolucion_venta + $almacen->compra + $almacen->devolucion_compra;
                                            $stock_total = $stock_total + $stock;
                                            array_push($idSusAlmacenes, $almacen->id_almacen);
                                            @endphp
                                                <tr>
                                                    <td>{{$almacen->nombre}}</td>
                                                    <td>{{$stock}}</td>
                                                    @if(!env('GESTION_STOCK', false))
                                                    <td>
                                                        <a href="#" class="btn btn-sm btn-warning" onclick="return editarStock('{{$almacen->id_stock}}','{{$almacen->stock}}','{{$almacen->nombre}}')" data-toggle="modal" data-target="#editarstock"><i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Editar</a>&nbsp;&nbsp;&nbsp;
                                                        <a href="{{ route('productos.eliminaralmacen', [$producto->id_producto, $almacen->id_stock]) }}" class="btn btn-sm btn-danger" onclick="return confirmar('borrar el stock del almacen',this,event);"><i class="fa fa-trash-alt "></i>&nbsp;&nbsp;&nbsp;Borrar</a>&nbsp;&nbsp;&nbsp;
                                                    </td>
                                                    @endif
                                                </tr>
                                                @endforeach
                                                <tr>
                                                    <td>TOTAL</td>
                                                    <td>{{$stock_total}}</td>
                                                    @if(!env('GESTION_STOCK', false))
                                                    <td></td>
                                                    @endif
                                                </tr>
                                            </tbody>
                                            </table>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.tab-pane -->
    <!-- Modal agregar proveedor-->
<div class="modal fade" id="agregarproveedor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <form role="form" id="quickForm" method="post" action="{{ route('productos.agregarproveedor', [$producto->id_producto]) }}">
        @csrf
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Agregar proveedor</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group col-sm-12">
                        <div class="form-group">
                        <label for="proveedor_id">Proveedor</label>
                        <select name="proveedor_id" class="form-control" id="proveedor_id" required>
                            <option value="">Seleccionar</option>
                            @foreach($proveedores as $el_proveedor)
                            @if(!in_array($el_proveedor->id_user, $idSusProveedores))
                            <option value="{{$el_proveedor->id_user}}">{{$el_proveedor->name}}</option>
                            @endif
                            @endforeach
                        </select>
                        </div>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="codigo_proveedor">Codigo producto-proveedor</label>
                        <input type="text" name="codigo_proveedor" class="form-control" id="codigo_proveedor" placeholder="Ingresar codigo producto-proveedor">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="costo_dolares">Costo{{env('MANEJO_DOLARES', false) ? ' en dolares': ''}}</label>
                        <input type="number" step="0.01" name="costo_dolares" class="form-control" id="costo_dolares" placeholder="Ingresar costo{{env('MANEJO_DOLARES', false) ? ' en dolares': ''}}" required>
                    </div>
                    <div class="form-group col-sm-12" <?php if(!env('MARGEN_PROVEEDOR', false)){ echo "hidden";} ?>>
                        <label for="margen">Margen</label>
                        <input type="number" step="0.01" name="margen" class="form-control" id="margen" placeholder="Ingresar margen">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-sm btn-success">Agregar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- Modal agregar almacen-->
<div class="modal fade" id="agregaralmacen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <form role="form" id="quickForm" method="post" action="{{ route('productos.agregaralmacen', [$producto->id_producto]) }}">
        @csrf
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Habilitar almacen</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group col-sm-12">
                        <div class="form-group">
                        <label for="almacen_id">Almacen</label>
                        <select name="almacen_id" class="form-control" id="almacen_id" required>
                            <option value="">Seleccionar</option>
                            @foreach($almacenes as $almacen)
                            @if(!in_array($almacen->id_almacen, $idSusAlmacenes))
                            <option value="{{$almacen->id_almacen}}">{{$almacen->nombre}}</option>
                            @endif
                            @endforeach
                        </select>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        {{-- <label for="stock">Stock</label> --}}
                        <input type="hidden" name="stock" class="form-control" id="stock" placeholder="Ingresar stock" value="0" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                  <button type="submit" class="btn btn-sm btn-success">Agregar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- Modal agregar movimiento-->
<div class="modal fade" id="agregarmovimiento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <form role="form" id="quickForm" method="post" action="{{ route('productos.agregarmovimiento', [$producto->id_producto]) }}">
        @csrf
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Agregar movimiento</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="tipo_movimiento">Movimiento</label>
                            <select name="tipo_movimiento" class="form-control" id="tipo_movimiento" required onchange="cambiarModal(this.value)">
                                <option value="Transferencia">Transferencia</option>
                                <option value="Ajuste">Ajuste</option>
                            </select>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="cantidad">Cantidad</label>
                            <input type="number" name="cantidad" class="form-control" id="cantidad" step="0.01" placeholder="Ingresar cantidad" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label id="lblAlmacenDesde" for="almacen_id_origen">Almacen desde</label>
                            <select name="almacen_id_origen" class="form-control" id="almacen_id_origen" required onchange="cambiarMax(this)">
                                @foreach($susalmacenes as $almacen)
                                @php 
                                $el_stock = $almacen->stock + $almacen->mov_destino + $almacen->mov_origen + $almacen->venta + $almacen->devolucion_venta + $almacen->compra + $almacen->devolucion_compra
                                @endphp
                                <option stock="{{$el_stock}}" value="{{$almacen->id_stock}}">{{$almacen->nombre}} - Stock: {{$el_stock}}u.</option>
                                @endforeach
                            </select>
                        </div>
                        <div id="divAlmacenHacia" class="form-group col-sm-6">
                            <label for="almacen_id_destino">Almacen hacia</label>
                            <select name="almacen_id_destino" class="form-control" id="almacen_id_destino" required>
                                @foreach($susalmacenes as $almacen)
                                <option value="{{$almacen->id_stock}}">{{$almacen->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="descripcion">Descripcion</label>
                            <textarea name="descripcion" class="form-control" id="descripcion" placeholder="Ingresar descripción"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-sm btn-success" onclick="return validar()">Agregar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- Modal editar stock-->
<div class="modal fade" id="editarstock" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <form role="form" id="quickForm" method="post" action="{{ route('productos.editarstock', [$producto->id_producto]) }}">
        @csrf
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="titulo_editar_stock">Editar stock: </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group col-sm-12">
                        <div class="form-group">
                        <input type="hidden" name="id_stock_modal" class="form-control" id="id_stock_modal" required>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="stock_modal">Stock</label>
                        <input type="number" name="stock_modal" class="form-control" id="stock_modal" placeholder="Ingresar stock" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                  <button type="submit" class="btn btn-sm btn-success">Editar</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- Modal editar costo -->
<div class="modal fade" id="editarcosto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <form role="form" id="quickForm" method="post" action="{{ route('productos.editarcosto', [$producto->id_producto]) }}">
        @csrf
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="titulo_editar_costo">Editar costo: </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group col-sm-12">
                        <div class="form-group">
                        <input type="hidden" name="id_costo_modal" class="form-control" id="id_costo_modal" required>
                        </div>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="codigo_proveedor_modal">Codigo producto-proveedor</label>
                        <input type="text" name="codigo_proveedor_modal" class="form-control" id="codigo_proveedor_modal" placeholder="Ingresar codigo producto-proveedor">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="stock_modal">Costo{{env('MANEJO_DOLARES', false) ? ' en dolares': ''}}</label>
                        <input type="number" step="0.01" name="costo_modal" class="form-control" id="costo_modal" placeholder="Ingresar costo{{env('MANEJO_DOLARES', false) ? ' en dolares': ''}}" required>
                    </div>
                    
                    <div class="form-group col-sm-12" <?php if(!env('MARGEN_PROVEEDOR', false)){ echo "hidden";} ?>>
                        <label for="margen_modal">Margen</label>
                        <input type="number" step="0.01" name="margen_modal" class="form-control" id="margen_modal" placeholder="Ingresar margen">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                  <button type="submit" class="btn btn-sm btn-success">Editar</button>
                </div>
            </div>
        </div>
    </form>
</div>
@stop