@extends('adminlte::page')

@section('content')
    {{-- <head>    
    <!-- Meta -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">

    <!-- Script -->
    <script src="/js/jquery-3.5.1.min.js" type="text/javascript"></script>
    </head> --}}
    <body>
        <style>
            img {
                /* width: 50%; */
                margin: auto;
            }
            .form-group{
                margin-bottom: 0rem;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function(){
                $('.carousel').carousel({
                    interval: false,
                });
                $('#verproducto').on('hidden.bs.modal', function () {
                    var html = '<div class="spinner-border"></div>';
                    $("#imagenescarousel").html(html);
                    $("#nombreProducto").html('');
                    $("#marcaProducto").html('');
                    $("#rubroProducto").html('');
                    $("#precioPesosProducto").html('');
                })
            });
            function vaciarCampos(){
                var html = '<div class="spinner-border"></div>';
                $("#imagenescarousel").html(html);
                $("#nombreProducto").html('');
                $("#nombreProductoCarrito").html('');
                $("#marcaProducto").html('');
                $("#rubroProducto").html('');
                $("#precioPesosProducto").html('');
            }
            function openModalVer(id_producto){
                $("#idProducto").val(id_producto);
                $.get('detalleproducto/'+id_producto, function(data){
                    console.log(data);
                    var imagenes = data['imagenes'];
                    var producto = data['producto'];
                    var mirol = data['mirol'];
                    var marca = data['producto']['marcas'][0];
                    var rubro = data['producto']['rubros'][0];
                    const options = { 
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2 
                    };
                    // var precio_dolar = producto.costo_dolares * (producto.margen / 100);
                    if(mirol.discount){
                        var precio_pesos = producto.costo_dolares * (producto.margen / 100) * producto.valor_dolar * (1 - mirol.discount / 100);
                        $("#precioPesosProducto").html('<strong>Precio en pesos: </strong>$ ' + Number(precio_pesos).toLocaleString('de-DE', options));
                    }else{
                        $("#precioPesosProducto").html()
                    }
                    var html = '<br><h3 class="text-center">IMAGENES</h3><br>';
                    var bandera = 1;
                    if (imagenes.length === 0){
                        html = html + '<div class="carousel-item active"><img class="d-block" src="/images/sin_imagen.jpg" alt="First slide"></div>';
                    }else{
                        imagenes.forEach(imagen => {
                            if(bandera == 1){
                                html = html + '<div class="carousel-item active"><img class="d-block" src="/images/'+imagen.producto_id+"/"+imagen.nombre+'" alt="First slide"></div>';
                            }else{
                                html = html + '<div class="carousel-item"><img class="d-block" src="/images/'+imagen.producto_id+"/"+imagen.nombre+'" alt="First slide"></div>';
                            }
                            bandera = bandera + 1;
                        });
                    }
                    $("#imagenescarousel").html(html);
                    $("#nombreProducto").html(producto.nombre);
                    $("#nombreProductoCarrito").html(producto.nombre);
                    $("#marcaProducto").html('<strong>Marca: </strong>' + marca.nombre);
                    $("#rubroProducto").html('<strong>Rubro: </strong>' + rubro.nombre);
                    // $("#precioDolarProducto").html('<strong>Precio en dolares: </strong>USD ' + parseFloat(precio_dolar).toFixed(2));
                    
                }).fail(response => {
                    toastr.error(response.responseJSON.message);
                });
            };
            function openModalAddCarrito(id_producto, nombre_producto){
                $("#idProducto").val(id_producto);
                $("#nombreProductoCarrito").html(nombre_producto);
            };
            function addCarrito(){
                var idProducto = $("#idProducto").val();
                var cantidad = $("#cantidad").val();
                if(cantidad != '' && cantidad != 0){
                    $("body").css('cursor','wait'); 
                    $.get('addcarrito', { cantidad, idProducto}, function(data){
                        if(data.success != undefined){
                            $('#addCarrito').modal('hide');
                            vaciarCampos();
                            toastr.success(data.success);
                            $("body").css('cursor','default');
                            location.reload();
                        }else{
                            $("body").css('cursor','default'); 
                            toastr.error(data.error);
                        }
                    }).fail(response => {
                        toastr.error(response.responseJSON.message);
                        $("body").css('cursor','default'); 
                    });
                }else{
                    toastr.error('Debe ingresar una cantidad mayor a 0(cero)');
                    $("#cantidad").focus();
                }
            }
        </script>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Catálogo</h3>
                            <div class="card-tools">
                                <form role="form" action="catalogo" method="POST">
                                @csrf
                                <div class="input-group input-group-sm">
                                    <div class="form-group">
                                        <label for="con_descuento">Solo con descuento</label>&nbsp;&nbsp;&nbsp;
                                        <input type="checkbox" id="con_descuento" name="con_descuento" <?php if($con_descuento) echo 'checked';?>>
                                    </div>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <select name="limit" class="form-control" id="limit">
                                        <option value="20" <?php if($limit == 20) echo 'selected';?>>20 registros</option>
                                        <option value="50" <?php if($limit == 50) echo 'selected';?>>50 registros</option>
                                        <option value="100" <?php if($limit == 100) echo 'selected';?>>100 registros</option>
                                    </select>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <select name="marca_id" class="form-control" id="marca_id">
                                        <option value="">Marca</option>
                                        @foreach($marcas as $marca)
                                        @if($marca->id_marca == $marcaSelected)
                                            <option value="{{$marca->id_marca}}" selected>{{$marca->nombre}}</option>
                                        @else
                                            <option value="{{$marca->id_marca}}">{{$marca->nombre}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <select name="rubro_id" class="form-control" id="rubro_id">
                                        <option value="">Rubro</option>
                                        @foreach($rubros as $rubro)
                                        @if($rubro->id_rubro == $rubroSelected)
                                            <option value="{{$rubro->id_rubro}}" selected>{{$rubro->nombre}}</option>
                                        @else
                                            <option value="{{$rubro->id_rubro}}">{{$rubro->nombre}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="text" id="search" name="search" value="{{$search ? $search : ''}}" class="form-control pull-right" placeholder="Buscar">
                                    <div class="input-group-btn">
                                    <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered">
                                <thead>                  
                                <tr>
                                    <th>Imagen</th>
                                    <th>Codigo</th>
                                    <th>Nombre</th>
                                    <th>Marca</th>
                                    <th>Rubro</th>
                                    @if(Auth::user())
                                    <th>Precio</th>
                                    <th>Stock</th>
                                    @endif
                                    @php
                                        if(Auth::user()){
                                    @endphp
                                    <th>Acciones</th>
                                    @php
                                        }
                                    @endphp
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($productos as $producto)
                                <tr>
                                    <td>
                                        @php
                                        $stock = $producto->cantidad + $producto->mov_destino + $producto->mov_origen + $producto->venta + $producto->devolucion_venta + $producto->compra + $producto->devolucion_compra;
                                        if($stock >= $producto->stock_verde){
                                            $procentaje_stock = 70;
                                            $color_stock = "bg-green";
                                        }else if($stock < $producto->stock_verde && $stock >= $producto->stock_amarillo){
                                            $procentaje_stock = 40;
                                            $color_stock = "bg-warning";
                                        }else{
                                            $procentaje_stock = "?";
                                            $color_stock = "bg-danger";
                                        }
                                        if(!empty($producto->nombre_imagen)){
                                        @endphp
                                        <a data-toggle="modal" data-target="#verproducto" title="Ver" href="#" onclick="return openModalVer('{{$producto->id_producto}}')"><img src="/images/{{$producto->id_producto}}/MIN{{$producto->nombre_imagen}}" class="img-thumbnail" width="50" height="50"/></a>
                                        
                                        @php
                                        }
                                        if($producto->tiene_fijado == 1){
                                            $precio = $producto->precio_fijado;
                                        }else{
                                            $precio = ($producto->costo * ($producto->margen / 100) * (1 - $mirol->discount / 100)  * ($producto->valor_dolar));
                                        }
                                        @endphp
                                    </td>
                                    <td>{{$producto->codigo}}</td>
                                    <td>{{$producto->nombre}}</td>
                                    <td>{{$producto->marca}}</td>
                                    <td>{{$producto->rubro}}</td>
                                    @if(Auth::user())
                                    <td align="right" style="white-space: nowrap;">
                                        @if($producto->tiene_fijado == 1)
                                        <img width="30px" height="30px" src="images/discount.png">&nbsp;&nbsp;&nbsp;
                                        @endif
                                        $ {{number_format($precio,2,",",".")}}
                                    </td>
                                        @if($procentaje_stock == "?")
                                        <td title="preguntar">
                                            {{$procentaje_stock}}
                                        </td>
                                        @else
                                        <td class="project_progress">
                                            <div class="progress progress-sm">
                                                <div class="progress-bar {{$color_stock}}" role="progressbar" aria-valuenow="{{$procentaje_stock}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$procentaje_stock}}%">
                                                </div>
                                            </div>
                                        </td>
                                        @endif
                                    @endif
                                    @php
                                        if(Auth::user()){
                                    @endphp
                                    <td>
                                    <a data-toggle="modal" data-target="#verproducto" title="Ver" href="#" onclick="return openModalVer('{{$producto->id_producto}}')" class="btn btn-sm btn-primary"><i class="fa fa-file"></i></a>
                                    <a data-toggle="modal" data-target="#addCarrito" title="Agregar al carrito" href="#" onclick="return openModalAddCarrito('{{$producto->id_producto}}', '{{$producto->nombre}}')" class="btn btn-sm btn-success"><i class="fa fa-cart-plus "></i></a>
                                    </td>
                                    @php
                                        }
                                    @endphp
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix">
                            {{ $productos->appends(['search' => $search, 'limit' => $limit, 'con_descuento' => $con_descuento, 'rubro_id' => $rubroSelected, 'marca_id' => $marcaSelected])->links() }}
                        </div>
                    </div>
            <!-- /.card -->
                </div>
            </div>
        </div>
        <div class="modal fade" id="verproducto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="nombreProducto"></h5>
                        <div class="card-tools" style="margin-inline-start: auto;">
                            <button type="button" class="btn btn-sm btn-secondary" onclick="vaciarCampos();" data-dismiss="modal">Cancelar</button>
                            <button data-toggle="modal" data-target="#addCarrito" type="button" class="btn btn-sm btn-success" data-dismiss="modal">Agregar al carrito</button>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!-- /.col -->
                        <div class="col-md-12">
                            <div class="card">
                            <!-- <div class="card-header">
                                
                            </div> -->
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h6><div id="marcaProducto"></div></h6>
                                        <h6><div id="rubroProducto"></div></h6>
                                    </div>
                                    <div class="col-md-6">
                                        <h6><div id="precioPesosProducto"></div></h6>
                                    </div>
                                </div>
                                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                <div id="imagenescarousel" class="carousel-inner text-center">
                                    <div class="spinner-border"></div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                    <span class="carousel-control-custom-icon" aria-hidden="true" style="color: black;">
                                    <i class="fas fa-chevron-left"></i>
                                    </span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                    <span class="carousel-control-custom-icon" aria-hidden="true" style="color: black;">
                                    <i class="fas fa-chevron-right"></i>
                                    </span>
                                    <span class="sr-only">Next</span>
                                </a>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="addCarrito" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <input type="hidden" id="idProducto" name="idProducto"/>
                            <h5 class="modal-title" id="nombreProductoCarrito"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Cantidad:</label>
                                </div>
                                <div class="col-md-9">
                                    <input class="form-control" type="number" id="cantidad" name="cantidad" required/>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-sm btn-secondary" onclick="vaciarCampos();" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-sm btn-success" onclick="addCarrito();">Aceptar</button>
                        </div>
                    </div>
                </div>
        </div>
    </body>
@stop