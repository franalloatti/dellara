@extends('adminlte::page')

@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Ver stock</h3>
            <div class="card-tools">
              <form role="form" action="{{ route('productos.verstock') }}" method="POST">
                @csrf
                <div class="input-group input-group-sm">
                  <select name="marca_id" class="form-control" id="marca_id">
                      <option value="">Marca</option>
                      @foreach($marcas as $marca)
                      @if($marca->id_marca == $marcaSelected)
                          <option value="{{$marca->id_marca}}" selected>{{$marca->nombre}}</option>
                      @else
                          <option value="{{$marca->id_marca}}">{{$marca->nombre}}</option>
                      @endif
                      @endforeach
                  </select>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <select name="rubro_id" class="form-control" id="rubro_id">
                      <option value="">Rubro</option>
                      @foreach($rubros as $rubro)
                      @if($rubro->id_rubro == $rubroSelected)
                          <option value="{{$rubro->id_rubro}}" selected>{{$rubro->nombre}}</option>
                      @else
                          <option value="{{$rubro->id_rubro}}">{{$rubro->nombre}}</option>
                      @endif
                      @endforeach
                  </select>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="text" id="search" name="search" value="{{$search ? $search : ''}}" class="form-control pull-right" placeholder="Buscar">
                  <div class="input-group-btn">
                  <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered" style="font-size: 13px">
              <thead>                  
                <tr>
                  <th>Codigo</th>
                  <th>Nombre</th>
                  <th>Marca</th>
                  <th>Rubro</th>
                  @foreach ($almacenes as $almacen)
                  <th>{{$almacen->nombre}}</th>
                  @endforeach
                  <th>Total</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @php
                  $stock_anterior = 0;
                  $stock_actual = 0;
                  $id_almacen_anterior = 0;
                  $id_almacen_actual = 0;
                  $id_producto_anterior = 0;
                  $id_producto_actual = 0;
                  $index = 0;
                  $stock_total = 0;
                  $unidad_anterior = '';
                  $unidad_actual = '';
                  $tipo_anterior = '';
                  $tipo_actual = '';
                  $conversion_anterior = '';
                  $conversion_actual = '';
                  foreach($stocks as $stock){
                    if($id_producto_anterior == 0){
                      $unidad_actual = $stock->unidad ;
                      $tipo_actual = $stock->tipo ;
                      $conversion_actual = $stock->conversion ;
                      $stock_actual = $stock->cantidad + $stock->mov_destino + $stock->mov_origen + $stock->venta + $stock->devolucion_venta + $stock->compra + $stock->devolucion_compra;
                      $id_almacen_actual = $stock->id_almacen;
                      $id_producto_actual = $stock->id_producto;
                      // $stock_total = $stock->cantidad;
                      $stock_total = $stock->cantidad + $stock->mov_destino + $stock->mov_origen + $stock->venta + $stock->devolucion_venta + $stock->compra + $stock->devolucion_compra;
                    echo
                    '<tr>
                      <td>'.$stock->codigo.'</td>
                      <td>'.$stock->nombre.'</td>
                      <td>'.$stock->marca.'</td>
                      <td>'.$stock->rubro.'</td>';
                      $stock_anterior = $stock_actual;
                      $unidad_anterior = $unidad_actual;
                      $tipo_anterior = $tipo_actual;
                      $conversion_anterior = $conversion_actual;
                      $id_almacen_anterior = $id_almacen_actual;
                      $id_producto_anterior = $id_producto_actual;
                    }else{
                      $tipo_actual = $stock->tipo;
                      $conversion_actual = $stock->conversion;
                      $unidad_actual = $stock->unidad;
                      $stock_actual = $stock->cantidad + $stock->mov_destino + $stock->mov_origen + $stock->venta + $stock->devolucion_venta + $stock->compra + $stock->devolucion_compra;
                      $id_almacen_actual = $stock->id_almacen;
                      $id_producto_actual = $stock->id_producto;
                      if($id_producto_actual <> $id_producto_anterior){
                        while($index < count($almacenes)){
                          if($almacenes[$index]->id_almacen <> $id_almacen_anterior){
                            echo    '<td>0,00'.$unidad_anterior.'</td>';
                          }else{
                            echo    '<td>'.number_format($tipo_anterior == 'Insumo' ? $conversion_anterior*$stock_anterior : $stock_anterior,2,",",".").$unidad_anterior.'</td>';
                          }
                          $index++;
                        }
                        // if($id_almacen_anterior <> null){
                          echo    '<td>'.number_format($tipo_anterior == 'Insumo' ? $conversion_anterior*$stock_total : $stock_total,2,",",".").$unidad_anterior.'</td>';
                        // }
                        echo '<td><a href="'.route('productos.stock', [$id_producto_anterior]).'" class="btn btn-sm btn-success">Ver</a></td>';
                        echo '</tr>';     
                        $stock_total = $stock_actual;
                        echo  
                        '<tr>
                          <td>'.$stock->codigo.'</td>
                          <td>'.$stock->nombre.'</td>
                          <td>'.$stock->marca.'</td>
                          <td>'.$stock->rubro.'</td>';
                        $index = 0;
                        $stock_anterior = $stock_actual;
                        $unidad_anterior = $unidad_actual;
                        $tipo_anterior = $tipo_actual;
                        $conversion_anterior = $conversion_actual;
                        $id_almacen_anterior = $id_almacen_actual;
                        $id_producto_anterior = $id_producto_actual;
                      }else{
                        if($id_almacen_anterior <> $id_almacen_actual){
                          echo    '<td>'.number_format($tipo_anterior == 'Insumo' ? $conversion_anterior*$stock_anterior : $stock_anterior,2,",",".").$unidad_anterior.'</td>';
                          $index++;
                          while($index < count($almacenes) && $almacenes[$index]->id_almacen <> $id_almacen_actual){
                            echo    '<td>0,00'.$unidad_anterior.'</td>';
                            $index++;
                          }
                          echo    '<td>'.number_format($tipo_actual == 'Insumo' ? $conversion_actual*$stock_actual : $stock_actual,2,",",".").$unidad_anterior.'</td>';
                          $stock_total = $stock_total + $stock_actual;
                          $id_almacen_anterior = $id_almacen_actual;
                          $index++;
                        }else{
                          $stock_total = $stock_total + $stock_actual;
                        }
                      }
                    }
                  }
                  // foreach ($almacenes as $almacen) {
                  //   if($almacen->id_almacen == $id_almacen_actual){
                  //     echo    '<td>'.number_format($stock_actual,2,",",".").'</td>';
                  //   }else{
                  //     echo    '<td>0,04</td>';
                  //   }
                  // }
                  // echo    '<td>'.number_format($stock_actual,2,",",".").'</td>';
                  // while($index < count($almacenes) && $almacenes[$index]->id_almacen <> $id_almacen_actual){
                  //   echo    '<td>0,00</td>';
                  //   $index++;
                  // }
                  while($index < count($almacenes)){
                    if($almacenes[$index]->id_almacen <> $id_almacen_actual){
                      echo    '<td>0,00'.$unidad_anterior.'</td>';
                    }else{
                      echo    '<td>'.number_format($tipo_anterior == 'Insumo' ? $conversion_anterior*$stock_anterior : $stock_anterior,2,",",".").$unidad_anterior.'</td>';
                    }
                    $index++;
                  }
                  echo    '<td>'.number_format($tipo_anterior == 'Insumo' ? $conversion_anterior*$stock_total : $stock_total,2,",",".").$unidad_anterior.'</td>';
                  echo '<td><a href="'.route('productos.stock', [$id_producto_anterior]).'" class="btn btn-sm btn-success">Ver</a></td>';
                  echo '</tr>';
                @endphp
              </tbody>
            </table>
          </div>
        </div>
      <!-- /.card -->
      </div>
    </div>
  </div>
@stop
