@extends('adminlte::page')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Movimiento</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="float-right">
                        </div>
                        <br><br>
                        <form class="form-horizontal">
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Tipo movimiento</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$movimiento->tipo_movimiento}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Usuario</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$movimiento->usuario}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Producto</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$movimiento->codigo}} - {{$movimiento->producto}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Cantidad</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$movimiento->cantidad}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">{{$movimiento->tipo_movimiento == 'Ajuste' ? 'Almacen' : 'Desde'}}</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$movimiento->almacen_origen}}</p>
                                        </div>
                                    </div>
                                </div>
                                @if($movimiento->tipo_movimiento != 'Ajuste')
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Hacia</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$movimiento->almacen_destino}}</p>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Fecha</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{date('d/m/Y H:i', strtotime($movimiento->created_at))}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Descripción</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$movimiento->descripcion}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop