@extends('adminlte::page')

@section('content')
<div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Usuario <small>Crear</small></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="quickForm" method="post" action="{{ route('usuarios.guardar') }}">
                @csrf
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-sm-4">
                      <div class="form-group">
                        <label for="perfil_id">Perfil</label>
                        <select name="perfil_id" class="form-control" id="perfil_id" required>
                          <option value="">Seleccionar</option>
                          @foreach($perfiles as $perfil)
                          <option value="{{$perfil->id_perfil}}">{{$perfil->nombre}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="form-group col-sm-4">
                      <div class="form-group">
                        <label for="role_id">Rol</label>
                        <select name="role_id" class="form-control" id="role_id" required>
                          <option value="">Seleccionar</option>
                          @foreach($roles as $role)
                          <option value="{{$role->id_role}}">{{$role->name}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="form-group col-sm-4">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control" id="email" placeholder="Ingresar email" required>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="nombre">Nombre</label>
                      <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Ingresar nombre" required>
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="apellido">Apellido</label>
                      <input type="text" name="apellido" class="form-control" id="apellido" placeholder="Ingresar apellido" required>
                    </div>
                  </div>
                  <div class="row">
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="password">Contraseña</label>
                      <input type="password" name="password" class="form-control" id="password" placeholder="Ingresar contraseña">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="password">Confirmar contraseña</label>
                        <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Confirmar contraseña">
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Crear</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
@stop
