@extends('adminlte::page')

@section('content')
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Usuarios</h3>
                <div class="card-tools">
                  <form role="form" action="/admin/usuarios/index" method="POST">
                    @csrf
                    <div class="input-group input-group-sm">
                      <input type="text" id="search" name="search" value="{{$search ? $search : ''}}" class="form-control pull-right" placeholder="Buscar">
                      <div class="input-group-btn">
                        <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th>Nombre</th>
                      <th>Apellido</th>
                      <th>Email</th>
                      <th>Rol</th>
                      <th>Estado</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($users as $user)
                    <tr>
                      <td>{{$user->name}}</td>
                      <td>{{$user->surname}}</td>
                      <td>{{$user->email}}</td>
                      <td>{{$user->role}}</td>
                      <td>{{$user->status ? "Activo" : "Suspendido"}}</td>
                      <td>
                        <a href="{{ route('usuarios.editar', [$user->id_user]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Editar</a>&nbsp;&nbsp;&nbsp;
                        @if ($user->id_user != Auth::user()->id_user)
                        <a href="{{ route('usuarios.borrar', [$user->id_user]) }}" class="btn btn-sm btn-danger" onclick="return confirmar('borrar el usuario',this,event);"><i class="fa fa-trash-alt "></i>&nbsp;&nbsp;&nbsp;Borrar</a>&nbsp;&nbsp;&nbsp;
                        <a href="{{ route('usuarios.suspender', [$user->id_user]) }}" class="btn btn-sm btn-primary" onclick="return confirmar('{{$user->status? 'suspender':'activar'}} al usuario',this,event);"><i class="fa fa-hand-pointer"></i>&nbsp;&nbsp;&nbsp;{{$user->status? "Suspender":"Activar"}}</a>&nbsp;&nbsp;&nbsp;
                        @endif
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                    {{ $users->appends(['search' => $search])->links() }}
              </div>
            </div>
            <!-- /.card -->
            </div>
          </div>
        </div>
        @stop
