@extends('adminlte::page')

@section('content')
  {{-- <head>

    <!-- Meta -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">

    <!-- Script -->
    <script src="/js/jquery-3.5.1.min.js" type="text/javascript"></script>

  </head> --}}
  <body>
    <script type="text/javascript">
      $(document).ready(function(){
        var role = $('#role_id');
        var categoria = $('#categoriaAFIP').val();
        categoria != '' ? $("#selectResponsabilidad option[value="+categoria+"]").attr("selected", true) : '';
        if(role.val() == ""){
          document.getElementById('mostrar_al_elegir_1').hidden = true;
          document.getElementById('mostrar_al_elegir_2').hidden = true;
        }else{
          document.getElementById('mostrar_al_elegir_1').hidden = false;
          document.getElementById('mostrar_al_elegir_2').hidden = false;
        }
        if(role.find(":selected").text() == 'Consumidor final'){
          document.getElementById('lblnombre').innerHTML = "Nombre";
          document.getElementById('nombre').placeholder = "Ingrese nombre";
          $('#lblapellidoHide').show();
          document.getElementById('lblapellido').innerHTML = "Apellido";
          document.getElementById('apellido').placeholder = "Ingrese apellido";
          document.getElementById('lbldocumento').innerHTML = "DNI";
          document.getElementById('documento').placeholder = "Ingrese documento";
          document.getElementById('divmayorista').hidden = true;
          document.getElementById('nombre_fantasia').required = false;
          document.getElementById('apellido').required = true;
          document.getElementById('divResponsabilidad').hidden = true;
          document.getElementById('selectResponsabilidad').value = 5;
        }else{
          document.getElementById('divResponsabilidad').hidden = false;
          document.getElementById('selectResponsabilidad').value = '';
          document.getElementById('lblnombre').innerHTML = "Nombre de AFIP";
          document.getElementById('nombre').placeholder = "Ingrese nombre de AFIP";
          $('#lblapellidoHide').hide();
          // document.getElementById('lblapellido').innerHTML = "Razón social";
          document.getElementById('apellido').placeholder = "Ingrese razón social";
          document.getElementById('lbldocumento').innerHTML = "CUIL/CUIT";
          document.getElementById('documento').placeholder = "Ingrese CUIL/CUIT";
          document.getElementById('divmayorista').hidden = false;
          document.getElementById('nombre_fantasia').required = true;
          document.getElementById('apellido').required = false;
        }
        $('#role_id').on('change', function(e){
          if($(this).val() == ""){
            document.getElementById('mostrar_al_elegir_1').hidden = true;
            document.getElementById('mostrar_al_elegir_2').hidden = true;
          }else{
            document.getElementById('mostrar_al_elegir_1').hidden = false;
            document.getElementById('mostrar_al_elegir_2').hidden = false;
          }
          if($(this).find(":selected").text() == 'Consumidor final'){
            document.getElementById('lblnombre').innerHTML = "Nombre";
            document.getElementById('nombre').placeholder = "Ingrese nombre";
            document.getElementById('lblapellido').innerHTML = "Apellido";
            document.getElementById('apellido').placeholder = "Ingrese apellido";
            document.getElementById('lbldocumento').innerHTML = "DNI";
            document.getElementById('documento').placeholder = "Ingrese documento";
            document.getElementById('divmayorista').hidden = true;
            document.getElementById('nombre_fantasia').required = false;
            document.getElementById('apellido').required = true;
            document.getElementById('divResponsabilidad').hidden = true;
            document.getElementById('selectResponsabilidad').value = 5;
          }else{
            document.getElementById('divResponsabilidad').hidden = false;
            document.getElementById('selectResponsabilidad').value = '';
            document.getElementById('lblnombre').innerHTML = "Nombre de AFIP";
            document.getElementById('nombre').placeholder = "Ingrese nombre de AFIP";
            document.getElementById('lblapellido').innerHTML = "Razón social";
            document.getElementById('apellido').placeholder = "Ingrese razón social";
            document.getElementById('lbldocumento').innerHTML = "CUIL/CUIT";
            document.getElementById('documento').placeholder = "Ingrese CUIL/CUIT";
            document.getElementById('divmayorista').hidden = false;
            document.getElementById('nombre_fantasia').required = true;
            document.getElementById('apellido').required = false;
          }
        });
      });
      function aceptar(){
        if($("#password").val() != $("#password_confirmation").val()) { 
          toastr.error('Las contraseñas no coinciden');
          return false;
        }else{
          return true;
        }
      }
    </script>
    <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Cliente <small>Editar</small></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="quickForm" method="post" action="{{ route('clientes.actualizar', [$user->id_user]) }}">
                @csrf
                <input type="hidden" id="categoriaAFIP" value="{{$user->responsabilidad}}">
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <div class="form-group">
                        <label for="role_id">Rol</label>
                        <select name="role_id" class="form-control" id="role_id" required>
                          <option value="">Seleccionar</option>
                          @foreach($roles as $role)
                          @if($role->id_role == $user->id_role)
                                <option value="{{$role->id_role}}" selected>{{$role->name}}</option>
                            @else
                                <option value="{{$role->id_role}}">{{$role->name}}</option>
                            @endif
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div hidden id="mostrar_al_elegir_1" class="form-group col-sm-6">
                      <label id="lbldocumento" for="documento">DNI</label>
                      <input type="text" name="documento" class="form-control" id="documento" value="{{$user->documento == '' ? $user->cuit : $user->documento}}" placeholder="Ingresar documento" required>
                    </div>
                  </div>
                  <div hidden id="mostrar_al_elegir_2">
                    <div class="row">
                      <div class="form-group col-sm-6">
                        <label id="lblnombre" for="nombre">Nombre</label>
                        <input type="text" name="nombre" class="form-control" id="nombre" value="{{$user->name}}" placeholder="Ingresar nombre" required>
                      </div>
                      <div id="lblapellidoHide" class="form-group col-sm-6">
                        <label id="lblapellido" for="apellido">Apellido</label>
                        <input type="text" name="apellido" class="form-control" id="apellido" value="{{$user->surname}}" placeholder="Ingresar apellido" required>
                      </div>
                      <div class="form-group col-sm-6" id="divResponsabilidad">
                        <label for="selectResponsabilidad">Responsabilidad AFIP</label>
                        <select class="form-control" id="selectResponsabilidad" name="responsabilidad" required>
                          <option value=""></option>
                          <option value="1">IVA Responsable Inscripto</option>
                          <option value="2">IVA Responsable no Inscripto</option>
                          <option value="3">IVA no Responsable</option>
                          <option value="4">IVA Sujeto Exento</option>
                          <option value="5">Consumidor Final</option>
                          <option value="6">Responsable Monotributo</option>
                          <option value="7">Sujeto no Categorizado</option>
                          <option value="8">Proveedor del Exterior</option>
                          <option value="9">Cliente Exterior</option>
                          <option value="10">IVA Liberado</option>
                          <option value="11">IVA responsable Inscripto – Agente de Percepción</option>
                          <option value="12">Pequeño Contribuyente Eventual</option>
                          <option value="13">Monotributista Social</option>
                          <option value="14">Pequeño Contribuyente Eventual Social</option>
                        </select>
                      </div>
                    </div>
                    <div id="divmayorista" hidden class="row">
                      <div class="form-group col-sm-6">
                        <label for="nombre_fantasia">Nombre de fantasia</label>
                        <input type="text" name="nombre_fantasia" class="form-control" id="nombre_fantasia" value="{{$user->nombre_fantasia}}" placeholder="Ingresar nombre de fantasia">
                      </div>
                      <div class="form-group col-sm-6">
                        <label for="numero_iibb">Número IIBB</label>
                        <input type="text" name="numero_iibb" class="form-control" id="numero_iibb" value="{{$user->numero_iibb}}" placeholder="Ingresar número IIBB">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-sm-6">
                          <label for="email">Email</label>
                          <input type="email" name="email" class="form-control" id="email" value="{{$user->email}}" placeholder="Ingresar email">
                      </div>
                      <!-- <div class="form-group col-sm-6">
                        <label for="username">Nombre de usuario</label>
                        <input type="text" name="username" class="form-control" id="username" value="{{$user->username}}" placeholder="Ingresar nombre de usuario">
                      </div> -->
                    </div>
                    <div class="row">
                      <div class="form-group col-sm-6">
                        <label for="telefono">Teléfono fijo</label>
                        <input type="text" name="telefono" class="form-control" id="telefono" value="{{$user->telefono}}" placeholder="Ingresar teléfono fijo">
                      </div>
                      <div class="form-group col-sm-6">
                        <label for="telefono_movil">Teléfono celular</label>
                        <input type="text" name="telefono_movil" class="form-control" id="telefono_movil" value="{{$user->telefono_movil}}" placeholder="Ingresar teléfono celular">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-sm-6">
                        <label for="password">Contraseña</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Ingresar contraseña">
                      </div>
                      <div class="form-group col-sm-6">
                          <label for="password">Confirmar contraseña</label>
                          <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Confirmar contraseña">
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-success" onclick="return aceptar(event);">Editar</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-6">

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </body>
@stop
