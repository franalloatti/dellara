@extends('adminlte::page')

@section('content')
<head>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Cliente: {{$cliente->name}}</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="float-right">
                            <a href="{{ route('clientes.cuentaampliado', [$cliente->id_user]) }}" class="btn btn-sm btn-secondary" {{$cliente->role == 'Consumidor final' ? 'hidden' : ''}}><i class="fa fa-book"></i>&nbsp;&nbsp;&nbsp;Ver cuenta corriente</a>&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="row">
                            {{-- <div class="col-sm-4">
                                <h4>Ingresos: <strong>$ {{number_format($total_ingresos,2,",",".")}}</strong></h4>
                            </div>
                            <div class="col-sm-4">
                                <h4>Egresos: <strong>$ -{{number_format($total_egresos,2,",",".")}}</strong></h4>
                            </div> --}}
                            <div class="col-sm-4">
                                <h4>Saldo: <strong>$ {{number_format(($total_ingresos - $total_egresos),2,",",".")}}</strong></h4>
                            </div>
                        </div>
                        <br>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Estado de cuenta</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <table class="table table-bordered">
                                            <thead>                  
                                                <tr>
                                                    <th>Movimiento</th>
                                                    <th>Tipo</th>
                                                    <th>Fecha</th>
                                                    <th>Monto</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($movimientos as $movimiento)
                                                <tr>
                                                    <td>{{$movimiento->movimiento}}</td>
                                                    <td>{{$movimiento->tipo}}</td>
                                                    <td>{{$movimiento->created_at}}</td>
                                                    @php
                                                    $signo = '';
                                                    if($movimiento->tipo == "Ingreso"){
                                                        $signo = '';
                                                    }else{
                                                        $signo = '-';
                                                    }
                                                    @endphp
                                                    <td align="right">{{$signo . number_format($movimiento->monto_faltante,2,",",".")}}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.tab-pane -->
    </body>
@stop