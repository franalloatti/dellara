@extends('adminlte::page')

@section('content')
<div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Perfil <small>Crear</small></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="quickForm" method="post" action="{{ route('perfiles.guardar') }}">
                @csrf
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="nombre">Nombre</label>
                      <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Ingresar nombre" required>
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="descripcion">Descripcion</label>
                      <input type="text" name="descripcion" class="form-control" id="descripcion" placeholder="Ingresar descripcion">
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="form-group col-sm-12">
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>Permisos</th>
                            <th style="text-align-last: center;">Ver</th>
                            <th style="text-align-last: center;">Crear</th>
                            <th style="text-align-last: center;">Editar</th>
                            <th style="text-align-last: center;">Borrar</th>
                          </tr>
                        </thead>
                        <tbody id="tablaproductos">
                          @foreach($permisos as $permiso)
                          <tr>
                            <input name="items[{{$permiso->id_permiso}}][id_permiso]" value="{{$permiso->id_permiso}}" type="hidden" class="form-control" id="items[{{$permiso->id_permiso}}][id_permiso]">
                            <td>{{$permiso->nombre}}</td>
                            <td><input name="items[{{$permiso->id_permiso}}][ver]" type="checkbox" class="form-control" id="items[{{$permiso->id_permiso}}][ver]"></td>
                            <td><input name="items[{{$permiso->id_permiso}}][crear]" type="checkbox" class="form-control" id="items[{{$permiso->id_permiso}}][crear]"></td>
                            <td><input name="items[{{$permiso->id_permiso}}][editar]" type="checkbox" class="form-control" id="items[{{$permiso->id_permiso}}][editar]"></td>
                            <td><input name="items[{{$permiso->id_permiso}}][borrar]" type="checkbox" class="form-control" id="items[{{$permiso->id_permiso}}][borrar]"></td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Crear</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
@stop
