@extends('adminlte::page')

@section('content')
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Tipos de movimiento</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th>Tipo de movimiento</th>
                      <th>Nombre</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($tipo_movimientos as $tipo_movimiento)
                    <tr>
                      <td>{{$tipo_movimiento->tipo}}</td>
                      <td>{{$tipo_movimiento->nombre}}</td>
                      <td>
                        <a href="{{ route('tipo_movimientos.editar', [$tipo_movimiento->id_tipo_movimiento]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Editar</a>&nbsp;&nbsp;&nbsp;
                        <a href="{{ route('tipo_movimientos.borrar', [$tipo_movimiento->id_tipo_movimiento]) }}" class="btn btn-sm btn-danger" onclick="return confirmar('borrar el tipo de movimiento',this,event);"><i class="fa fa-trash-alt "></i>&nbsp;&nbsp;&nbsp;Borrar</a>&nbsp;&nbsp;&nbsp;
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                    {{ $tipo_movimientos->links() }}
              </div>
            </div>
            <!-- /.card -->
            </div>
          </div>
        </div>
        @stop
