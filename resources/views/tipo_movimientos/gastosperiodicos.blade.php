@extends('adminlte::page')

@section('content')
  <script>
    function cargarDatos(id_gasto, nombre, observaciones){
      $('#id_gasto').val(id_gasto);
      $('#nombre').val(nombre);
      $('#observaciones').val(observaciones);
    }
    function cargarGasto(gasto){
      console.log(gasto)
      $('#id_gasto_pagar').val(gasto.id_tipo_movimiento);
      $('#monto_gasto').val(-1*gasto.monto);
    }
  </script>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Gastos periódicos</h3>
            <div class="card-tools">
              <form role="form" action="/admin/gastos/periodicos" method="POST">
                @csrf
                <div class="input-group input-group-sm">
                  <a class="btn btn-sm btn-success" data-toggle="modal" data-target="#addgasto"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Nuevo</a>&nbsp;&nbsp;&nbsp;
                  <input type="text" id="search" name="search" value="{{$search ? $search : ''}}" class="form-control pull-right" placeholder="Buscar">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered">
              <thead>                  
                <tr>
                  <th>Gasto</th>
                  <th>Observaciones</th>
                  <th>Ultima vez pagado</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>
                @foreach($tipo_movimientos as $tipo_movimiento)
                <tr>
                  <td>{{$tipo_movimiento->nombre}}</td>
                  <td>{{$tipo_movimiento->observaciones}}</td>
                  <td>{{$tipo_movimiento->ultimo_pago == '' ? '-' : date('d/m/Y', strtotime($tipo_movimiento->ultimo_pago))}}</td>
                  <td>
                    <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#pagargasto" onclick="cargarGasto({{json_encode($tipo_movimiento)}})"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Pagar</button>&nbsp;&nbsp;&nbsp;
                    <button class="btn btn-sm btn-warning" data-toggle="modal" data-target="#editgasto"
                      onclick="cargarDatos({{$tipo_movimiento->id_tipo_movimiento}},'{{$tipo_movimiento->nombre}}','{{$tipo_movimiento->observaciones}}')">
                      <i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Editar
                    </button>&nbsp;&nbsp;&nbsp;
                    <a href="{{ route('gastos.borrar', [$tipo_movimiento->id_tipo_movimiento]) }}" class="btn btn-sm btn-danger" onclick="return confirmar('borrar el gasto periódico',this,event);"><i class="fa fa-trash-alt "></i>&nbsp;&nbsp;&nbsp;Borrar</a>&nbsp;&nbsp;&nbsp;
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
          <div class="card-footer clearfix">
                {{ $tipo_movimientos->appends(['search' => $search])->links() }}
          </div>
        </div>
        <!-- /.card -->
      </div>
    </div>
  </div>
  <!-- Modal agregar gasto-->
  <div class="modal fade" id="addgasto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <form role="form" id="quickForm" method="post" action="{{ route('gastos.guardar') }}">
      @csrf
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Crear gasto periódico</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
            <div class="form-group col-sm-12">
                <label for="nombre_add">Nombre</label>
                <input type="text" name="nombre_add" class="form-control" id="nombre_add" placeholder="Ingresar nombre" required>
            </div>
            <div class="form-group col-sm-12">
                <label for="observaciones_add">Observaciones</label>
                <input type="text" name="observaciones_add" class="form-control" id="observaciones_add" placeholder="Ingresar observaciones">
            </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-sm btn-success">Aceptar</button>
          </div>
        </div>
      </div>
    </form>
  </div>
  <!-- Modal editar gasto-->
  <div class="modal fade" id="editgasto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <form role="form" id="quickForm" method="post" action="{{ route('gastos.actualizar') }}">
      @csrf
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Editar gasto periódico</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <input type="hidden" name="id_gasto" class="form-control" id="id_gasto" required>
              <div class="form-group col-sm-12">
                  <label for="nombre">Nombre</label>
                  <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Ingresar nombre" required>
              </div>
              <div class="form-group col-sm-12">
                  <label for="observaciones">Observaciones</label>
                  <input type="text" name="observaciones" class="form-control" id="observaciones" placeholder="Ingresar observaciones">
              </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-sm btn-success">Aceptar</button>
          </div>
        </div>
      </div>
    </form>
  </div>
  <!-- Modal pagar gasto-->
  <div class="modal fade" id="pagargasto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <form role="form" id="quickForm" method="post" action="{{ route('gastos.pagar') }}">
      @csrf
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Pagar gasto periódico</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
            <input type="hidden" name="id_gasto_pagar" class="form-control" id="id_gasto_pagar" required>
            <div class="row">
              <div class="form-group col-sm-6">
                <label for="monto_gasto">Monto</label>
                <input type="number" name="monto_gasto" class="form-control" id="monto_gasto" required step="0.01">
              </div>
              <div class="form-group col-sm-6">
                <label for="medio_id_medio">Cuenta</label>
                <select name="medio_id_medio" class="form-control" id="medio_id_medio" required>
                  <option value="">Seleccionar</option>
                  @foreach($medios as $medio)
                  <option value="{{$medio->id_medio}}">{{$medio->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-sm-6">
                <label for="fecha">Monto</label>
                <input type="date" id="fecha" title="Fecha" name="fecha" value="{{date('Y-m-d')}}" class="form-control">
              </div>
            </div>
            <div class="row">
              <div class="form-group col-sm-12">
                <label for="descripcion">Descripción</label>
                <textarea name="descripcion" class="form-control" id="descripcion" placeholder="Ingresar descripcion"></textarea>
              </div>
            </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-sm btn-success">Aceptar</button>
          </div>
        </div>
      </div>
    </form>
  </div>
@stop
