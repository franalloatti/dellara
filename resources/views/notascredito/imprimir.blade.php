<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Laravel PDF</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <style>
            #table2 {
              border-collapse: collapse;
              border-spacing: 10px;  
            }
        </style>
    </head>
    <body>
        <h3 class="mb-3" style="text-align:center">NOTA DE CREDITO</h3>
        <table id="table2">
            <thead>                  
                <tr>
                    <th></th>
                    <th style="text-align: right; width:300px;"></th>
                </tr>
            </thead>
            <tbody>                  
                <tr>
                    <td>Cuit: {{$remito->cuit}}</td>
                    <td style="text-align: right; width:300px;">Numero: 01-{{substr('00000'.$remito->id_remito, -5)}}</td>
                </tr>
                <tr>
                    <td>Nombre/Razon social: {{$remito->name}}</td>
                    <td style="text-align: right; width:300px;">Fecha: {{date('d/m/Y', strtotime($remito->created_at))}}</td>
                </tr>
                <tr>
                    <td>Condicion de iva: </td>
                    <td></td>
                </tr>
                <tr>
                    <td>Domicilio: {{$remito->direccion}} - {{$remito->localidad}} ({{$remito->provincia}})</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Condicion de venta: Efectivo</td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <br>
        <table class="table table-bordered" style="border: solid 4px;font-size: 13px">
            <thead>                  
                <tr>
                    <th>Cantidad</th>
                    <th>Descripción</th>
                    <th>Precio unitario</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
            @php 
            $total_sin_descuento = 0.00;
            $total_con_descuento = 0.00;
            @endphp
            @foreach($productos as $producto)
            @php 
            $total_sin_descuento = $total_sin_descuento + ($producto->cantidad * $producto->precio_unitario);
            $total_con_descuento = $total_con_descuento + ($producto->cantidad * $producto->precio_unitario)*(1-($producto->descuento/100));
            @endphp
                <tr>
                    <td>{{$producto->cantidad}}</td>
                    <td>{{$producto->codigo}} - {{$producto->nombre}}</td>
                    <td>$ {{number_format(($producto->precio_unitario)*(1-($producto->descuento/100)),2,",",".")}}</td>
                    <td>$ {{number_format(($producto->cantidad * $producto->precio_unitario)*(1-($producto->descuento/100)),2,",",".")}}</td>
                    </tr>
                    @endforeach
                    <tr>
                    <td colspan="3"><strong>TOTALES</strong></td>
                    <td><strong>$ {{number_format(($total_con_descuento),2,",",".")}}</strong></td>
                </tr>
            </tbody>
        </table>
    </body>
</html>