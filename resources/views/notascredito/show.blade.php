@extends('adminlte::page')

@section('content')
<head>
</head>
<body>
    <script type="text/javascript">
    function cambiarestado(id_pedido, id_estado){
        $.get('../cambiarestado/'+id_pedido+"/"+id_estado, function(data){
            location.reload();
        }).fail(response => {
            toastr.error(response.responseJSON.message);
        });
    }
    </script>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Nota de credito</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="float-right">
                        </div>
                        <br><br>
                        <form class="form-horizontal">
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Cliente</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$remito->name}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Motivo</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$remito->motivo_devolucion == 'defecto' ? 'Producto defectuoso' : 'Error en pedido'}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Productos</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <table class="table table-bordered">
                                        <thead>                  
                                            <tr>
                                            <th>Codigo</th>
                                            <th>Nombre</th>
                                            <th>Cantidad</th>
                                            <th>Precio unitario</th>
                                            <th>Precio total</th>
                                            <th>Descuento</th>
                                            <th>Precio final</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @php 
                                        $total_sin_descuento = 0.00;
                                        $total_con_descuento = 0.00;
                                        @endphp
                                        @foreach($productos as $producto)
                                        @php 
                                        $total_sin_descuento = $total_sin_descuento + ($producto->cantidad * $producto->precio_unitario);
                                        $total_con_descuento = $total_con_descuento + ($producto->cantidad * $producto->precio_unitario)*(1-($producto->descuento/100));
                                        @endphp
                                            <tr>
                                            <td>{{$producto->codigo}}</td>
                                            <td>{{$producto->nombre}}</td>
                                            <td>{{$producto->cantidad}}</td>
                                            <td>$ {{number_format($producto->precio_unitario,2,",",".")}}</td>
                                            <td>$ {{number_format($producto->cantidad * $producto->precio_unitario,2,",",".")}}</td>
                                            <td>{{$producto->descuento}} %</td>
                                            <td>$ {{number_format(($producto->cantidad * $producto->precio_unitario)*(1-($producto->descuento/100)),2,",",".")}}</td>
                                            </tr>
                                            @endforeach
                                            <tr>
                                            <td colspan="4"><strong>TOTALES</strong></td>
                                            <td><strong>$ {{number_format($total_sin_descuento,2,",",".")}}</strong></td>
                                            <td></td>
                                            <td><strong>$ {{number_format(($total_con_descuento),2,",",".")}}</strong></td>
                                            </tr>
                                        </tbody>
                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.tab-pane -->
    </body>
@stop