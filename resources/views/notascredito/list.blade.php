@extends('adminlte::page')

@section('content')
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Remitos</h3>
                <div class="card-tools">
                  <form role="form" action="/admin/remitos/index" method="POST">
                    @csrf
                    <div class="input-group input-group-sm">
                      <input type="text" id="search" name="search" value="{{$search ? $search : ''}}" class="form-control pull-right" placeholder="Buscar">
                      <div class="input-group-btn">
                        <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th>Cliente</th>
                      <th>Nro remito</th>
                      <th>Nro pedido</th>
                      <th>Monto</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($remitos as $remito)
                    <tr>
                      <td>{{$remito->name}}</td>
                      <td>01-{{substr('00000'.$remito->id_remito, -5)}}</td>
                      <td><a href="/admin/pedidos/ver/{{$remito->pedido_id_pedido}}">{{$remito->pedido_id_pedido}}</a></td>
                      <td align="right">$ {{number_format($remito->monto,2,",",".")}}</td>
                      <td>
                        <a href="{{ route('remitos.ver', [$remito->id_remito]) }}" class="btn btn-sm btn-success"><i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Ver</a>&nbsp;&nbsp;&nbsp;
                        <a href="{{ route('remitos.imprimir', [$remito->id_remito]) }}" class="btn btn-sm btn-secondary"><i class="fa fa-print"></i>&nbsp;&nbsp;&nbsp;Imprimir</a>&nbsp;&nbsp;&nbsp;
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                    {{ $remitos->appends(['search' => $search])->links() }}
              </div>
            </div>
            <!-- /.card -->
            </div>
          </div>
        </div>
        @stop
