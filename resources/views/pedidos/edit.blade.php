@extends('adminlte::page')

@section('content')
  <head>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

  </head>
  <?php 
  if(env('PRECIO_EDITABLE_VENTA', false)){
    $readonly = '';
  }else{
    $readonly = 'readonly';
  }
  ?>
  <body onload="cargarDatos();">
    <script type="text/javascript">
      $(document).ready(function(){
        $('.js-example-basic-single').select2({matcher: matcher});
        $('#user_id_user').on('change', function(e){
          var user_id_user = e.target.value;
          $.get('../direccionescliente/'+user_id_user, function(data){
            var direccion_id_direccion = '<option value="" disabled selected>Seleccionar</option>'
            for (var i=0; i<data.length;i++)
            direccion_id_direccion+='<option value="'+data[i].id_direccion+'">'+data[i].direccion+'</option>';
            $("#direccion_id_direccion").html(direccion_id_direccion);
          }).fail(response => {
            toastr.error(response.responseJSON.message);
          });
        });
      });
      function cargarDatos(){
        var user_id_user = $('#user_id_user').val();
          $.get('../direccionescliente/'+user_id_user, function(data){
          var direccion_id_direccion = '<option value="" disabled selected>Seleccionar</option>'
            for (var i=0; i<data.length;i++)
            direccion_id_direccion+='<option value="'+data[i].id_direccion+'">'+data[i].direccion+'</option>';

            $("#direccion_id_direccion").html(direccion_id_direccion);
            var direccion_id = @php echo $pedido->direccion_id_direccion; @endphp;
            $('#direccion_id_direccion').val(direccion_id).trigger('change');
          }).fail(response => {
            toastr.error(response.responseJSON.message);
          });
      }
      function aceptar(e){
        e.preventDefault();
        var bandera = 0;
        var confirmacion = false;
        if($("#productos_venta tr").length == 0){
          toastr.error('No se puede generar una venta vacía');
          return false;
        }else{
          $("#productos_venta tr").each(function(index, tr) { 
              if($(this).find("input[name*='[cantidad]']").val() <= 0){
                bandera = 1;
                toastr.error('No se puede generar una venta con un item con cantidad 0 (cero)');
                return false;
              }
          }).promise().done( function() {
              if(!bandera){
                swal({
                  title: 'Esta seguro/a?',
                  text: 'Esta seguro/a que desea confirmar la venta?',
                  icon: 'warning',
                  buttons: ["Cancelar", "Confirmar"],
                }).then(function(value) {
                    if (value) {
                      $('#quickForm').submit();
                    }
                });
              }
          });
        }
      };
      function agregar_venta(){
        var readonly = '{!! $readonly !!}';
        var producto_seleccionado = $('#producto_id').val();
        if(document.getElementById('items['+producto_seleccionado+'][id]') != null){
          toastr.error("El producto ya se encuentra en el pedido");
          return false;
        }else{
          var descuentoCliente = $('#descuentoCliente').val();
          var productos = {!! $productos !!};
          productos.forEach(data => {
            if(producto_seleccionado == data.id_producto){
              var el_precio = 0;
              var check_iva = $('#check_iva').prop('checked');
              var el_iva = parseInt(data.iva);
              switch (el_iva) {
                case 3:
                  el_iva = '0';
                  break;
                case 4:
                  el_iva = '10.5';
                  break;
                case 5:
                  el_iva = '21';
                  break;
                case 6:
                  el_iva = '27';
                  break;
                case 8:
                  el_iva = '5';
                  break;
                case 9:
                  el_iva = '2.5';
                  break;
                default:
                  el_iva = '0';
                  break;
              }
              var iva_actual = '';
              if(check_iva){
                iva_actual = el_iva;
              }else{
                iva_actual = 0;
              }
              if(data.tiene_fijado == 1){
                el_precio = data.precio_fijado;
              }else{
                el_precio = (data.valor_dolar * data.costo_dolares)*(data.margen / 100.00);
              }
              var ultimo_precio = '';
              var user_id_user = $('#user_id_user').val();
              $.ajax({
                async: false,
                type: 'get',
                url: '../getultimoprecio/'+user_id_user+"/"+producto_seleccionado,
                success: function (data) {
                  console.log(data);
                  ultimo_precio = data;
                },
                error: function (request, status, error) {
                    toastr.error(request.responseJSON.message);
                }
              });
              var texto = '<tr id="fila'+data.id_producto+'">'
              +'  <input name="items['+data.id_producto+'][id]" value="'+data.id_producto+'" type="hidden" class="form-control" id="items['+data.id_producto+'][id]">'
              +'  <input name="items['+data.id_producto+'][costo_unitario]" value="'+parseFloat(data.valor_dolar * data.costo_dolares).toFixed(2)+'" type="hidden" class="form-control" id="items['+data.id_producto+'][costo_unitario]">'
              +'  <input name="items['+data.id_producto+'][iva_producto]" value="'+el_iva+'" type="hidden" class="form-control" id="items['+data.id_producto+'][iva_producto]">'
              +'  <input name="items['+data.id_producto+'][iva]" value="'+iva_actual+'" type="hidden" class="form-control" id="items['+data.id_producto+'][iva]">'
              +'  <td>'+data.nombre+'</td>'
              +'  <td>'+ultimo_precio+'</td>'
              +'  <td id="iva_txt">'+iva_actual+'%</td>'
              +'  <td><input name="items['+data.id_producto+'][precio_unitario]" '+readonly+' value="'+parseFloat(el_precio).toFixed(2)+'" type="number" class="form-control" id="items['+data.id_producto+'][precio_unitario]"></td>'
              +'  <td><input name="items['+data.id_producto+'][cantidad]" type="number" value="0" onchange="return actualizar('+data.id_producto+')" class="form-control" id="items['+data.id_producto+'][cantidad]"></td>'
              +'  <td><input name="items['+data.id_producto+'][precio_total]" readonly value="0" type="number" class="form-control" id="items['+data.id_producto+'][precio_total]"></td>'
              +'  <td><input name="items['+data.id_producto+'][descuento]" step="0.01" type="number" value="'+descuentoCliente+'" onchange="return actualizarDescuento('+data.id_producto+')" class="form-control" id="items['+data.id_producto+'][descuento]"></td>'
              +'  <td><input name="items['+data.id_producto+'][precio_final_sin_iva]" readonly value="0" type="number" class="form-control" id="items['+data.id_producto+'][precio_final_sin_iva]"></td>'
              +'  <td><input name="items['+data.id_producto+'][precio_final]" readonly value="0" type="number" class="form-control" id="items['+data.id_producto+'][precio_final]"></td>'
              +'  <td><button onclick="return borrar('+data.id_producto+')" class="btn btn-danger">Eliminar</button></td>'
              +'</tr>'
              $('#productos_venta').append(texto);
              return;
            }
          });
          $('#producto_id').val("");
          actualizarTotales();
          return false;
        }
      };
      function agregar_promocion(){
        var producto_seleccionado = $('#producto_id_prom').val();
        if(document.getElementById('items_prom['+producto_seleccionado+'][id]') != null){
          toastr.error("El producto ya se encuentra en el pedido");
          return false;
        }else{
          var productos = {!! $productos !!};
          productos.forEach(data => {
            if(producto_seleccionado == data.id_producto){
              var texto = '<tr id="fila_prom'+data.id_producto+'">'
              +'  <input name="items_prom['+data.id_producto+'][id]" value="'+data.id_producto+'" type="hidden" class="form-control" id="items_prom['+data.id_producto+'][id]">'
              +'  <input name="items_prom['+data.id_producto+'][costo_unitario]" value="'+parseFloat(data.valor_dolar * data.costo_dolares).toFixed(2)+'" type="hidden" class="form-control" id="items_prom['+data.id_producto+'][costo_unitario]">'
              +'  <td>'+data.nombre+'</td>'
              +'  <td><input name="items_prom['+data.id_producto+'][cantidad]" type="number" value="0" class="form-control" id="items_prom['+data.id_producto+'][cantidad]"></td>'
              +'  <td><button onclick="return borrar_prom('+data.id_producto+');" class="btn btn-danger">Eliminar</button></td>'
              +'</tr>'
              $('#productos_promocion').append(texto);
              return;
            }
          });
          $('#producto_id_prom').val($('#producto_id_prom option:eq(0)').val()).trigger('change');
          return false;
        }
      };
      function agregar_devolucion(){
        var producto_seleccionado = $('#producto_id_dev').val();
        if(document.getElementById('items_dev['+producto_seleccionado+'][id]') != null){
          toastr.error("El producto ya se encuentra en el pedido");
          return false;
        }else{
          var productos = {!! $productos !!};
          productos.forEach(data => {
            if(producto_seleccionado == data.id_producto){
              var texto = '<tr id="fila_dev'+data.id_producto+'">'
              +'  <input name="items_dev['+data.id_producto+'][id]" value="'+data.id_producto+'" type="hidden" class="form-control" id="items_dev['+data.id_producto+'][id]">'
              +'  <input name="items_dev['+data.id_producto+'][costo_unitario]" value="'+parseFloat(data.valor_dolar * data.costo_dolares).toFixed(2)+'" type="hidden" class="form-control" id="items_dev['+data.id_producto+'][costo_unitario]">'
              +'  <td>'+data.nombre+'</td>'
              +'  <td><input name="items_dev['+data.id_producto+'][cantidad]" type="number" value="0" class="form-control" id="items_dev['+data.id_producto+'][cantidad]"></td>'
              +'  <td><button onclick="return borrar_dev('+data.id_producto+');" class="btn btn-danger">Eliminar</button></td>'
              +'</tr>'
              $('#productos_devolucion').append(texto);
              return;
            }
          });
          $('#producto_id_dev').val($('#producto_id_dev option:eq(0)').val()).trigger('change');
          return false;
        }
      };
      function setDescuento(id_cliente){
        var clientes = {!! $clientes !!};
        clientes.forEach(data => {
          if(id_cliente == data.id_user){
            document.getElementById('descuentoCliente').value = data.descuento;
            $("#productos_venta tr").each(function(index, tr) { 
              var precio_total = $(this).find("input[name*='[precio_total]']").val();
              var el_iva = $(this).find("input[name*='[iva]']").val();
              console.log(el_iva)
              $(this).find("input[name*='[descuento]']").val(data.descuento);
              $(this).find("input[name*='[precio_final_sin_iva]']").val(parseFloat(precio_total *(1 - data.descuento/100)).toFixed(2));
              $(this).find("input[name*='[precio_final]']").val(parseFloat(precio_total *(1 - data.descuento/100)*(1.0+(parseFloat(el_iva)/100.0))).toFixed(2));
            });
            // $( "input[name*='[descuento]']" ).val(data.descuento);
            return;
          }
        });
        actualizarTotales();
      };
      function actualizarTotales(){
        var cantidadItems = 0;
        var precio_total = 0;
        var descuento = 0;
        var precio_final = 0;
        var precio_final_sin_iva = 0;
        $("#productos_venta tr").each(function(index, tr) { 
          cantidadItems = cantidadItems + 1;
          precio_total = parseFloat(precio_total) + parseFloat($(this).find("input[name*='[precio_total]']").val());
          descuento = parseFloat(descuento) + parseFloat($(this).find("input[name*='[descuento]']").val());
          precio_final_sin_iva = parseFloat(precio_final_sin_iva) + parseFloat($(this).find("input[name*='[precio_final_sin_iva]']").val());
          precio_final = parseFloat(precio_final) + parseFloat($(this).find("input[name*='[precio_final]']").val());
        });
        document.getElementById('precio_total_final').innerHTML = "$ " + parseFloat(precio_total).toFixed(2);
        // console.log("desc"+descuento);
        // console.log("cant"+cantidadItems);
        document.getElementById('descuento_final').innerHTML = cantidadItems ? parseFloat(descuento / cantidadItems).toFixed(2) : parseFloat(0).toFixed(2);
        document.getElementById('precio_final_final_sin_iva').innerHTML = parseFloat(precio_final_sin_iva).toFixed(2);
        document.getElementById('precio_final_final').innerHTML = "$ " + parseFloat(precio_final).toFixed(2);
      };
      function actualizar(id_producto, value){
        var el_iva = document.getElementById('items['+id_producto+'][iva]').value;
        var precio_unitario = document.getElementById('items['+id_producto+'][precio_unitario]').value;
        var descuento = document.getElementById('items['+id_producto+'][descuento]').value;
        var cantidad = document.getElementById('items['+id_producto+'][cantidad]').value;
        document.getElementById('items['+id_producto+'][precio_total]').value = parseFloat(precio_unitario*cantidad).toFixed(2);
        document.getElementById('items['+id_producto+'][precio_final_sin_iva]').value = parseFloat(precio_unitario*cantidad*(1-descuento/100)).toFixed(2);
        document.getElementById('items['+id_producto+'][precio_final]').value = parseFloat(precio_unitario*cantidad*(1-descuento/100)*(1.0+(parseFloat(el_iva)/100.0))).toFixed(2);
        actualizarTotales();
      };
      function actualizarDescuento(id_producto){
        var precio_total = document.getElementById('items['+id_producto+'][precio_total]').value;
        var descuento = document.getElementById('items['+id_producto+'][descuento]').value;
        var el_iva = document.getElementById('items['+id_producto+'][iva]').value;
        document.getElementById('items['+id_producto+'][precio_final_sin_iva]').value = parseFloat(precio_total*(1-descuento/100)).toFixed(2);
        document.getElementById('items['+id_producto+'][precio_final]').value = parseFloat(precio_total*(1-descuento/100)*(1.0+(parseFloat(el_iva)/100.0))).toFixed(2);
        actualizarTotales();
      };
      function cambiarIva(checked){
        $("#productos_venta tr").each(function(index, tr) { 
          if(checked){
            var iva_producto = $(this).find("input[name*='[iva_producto]']").val();
            $(this).find("input[name*='[iva]']").val(iva_producto);
            $(this).find("#iva_txt").html(iva_producto+'%');
          }else{
            var el_iva = '0';
            $(this).find("input[name*='[iva]']").val(0);
            var txt = $(this).find("#iva_txt");
            txt.html(el_iva+'%');
          }
          actualizar($(this).find("input[name*='[id]']").val());
        });
      }
      function borrar(id_producto){
        $("#fila" + id_producto).remove();
        actualizarTotales();
      }
      function borrar_prom(id_producto){
        $("#fila_prom" + id_producto).remove();
        actualizarTotales();
      }
      function borrar_dev(id_producto){
        $("#fila_dev" + id_producto).remove();
        actualizarTotales();
      }
    </script>
    <style>
    .select2-container .select2-selection--single{
      height: auto !important;
    }
    .select2-container {
      width: 100% !important;
    }
    </style>
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- jquery validation -->
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">Pedido <small>Editar</small></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" id="quickForm" method="post" action="{{ route('pedidos.actualizar', [$pedido->id_pedido]) }}">
              @csrf
              <input name="user_id_user" value="{{$pedido->user_id_user}}" type="hidden" class="form-control" id="user_id_user">
              <input name="descuentoCliente" value="{{$pedido->discount}}" type="hidden" class="form-control" id="descuentoCliente">
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-6">
                    <label for="inputName">Cliente</label>
                    <p>{{$pedido->name}}</p>
                  </div>
                  <div class="form-group col-sm-6">
                    <label for="direccion_id_direccion">Dirección de entrega</label>
                    <select name="direccion_id_direccion" class="form-control js-example-basic-single" id="direccion_id_direccion" required>
                      <option value="" disabled selected>Seleccionar</option>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-sm-6">
                    <label for="flete">Flete</label>
                    <input type="text" name="flete" value="{{$pedido->flete}}" class="form-control" id="flete" placeholder="Ingresar datos del flete">
                  </div>
                  <div class="form-group col-sm-6">
                    <label for="nro_factura">Nro. factura</label>
                    <input type="text" name="nro_factura" value="{{$pedido->nro_factura}}" class="form-control" id="nro_factura" placeholder="Ingresar número de factura">
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-sm-6">
                    <label for="fecha_estimada_recepcion">Fecha tentativa de cobro</label>
                    <input type="date" name="fecha_estimada_recepcion" value="{{$pedido->fecha_estimada_recepcion}}" class="form-control" id="fecha_estimada_recepcion" required>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="form-group col-sm-2">
                    <label for="producto_id">Producto venta</label>
                  </div>
                  <div class="form-group col-sm-6">
                    <select name="producto_id" class="form-control js-example-basic-single" id="producto_id">
                      <option value="" disabled selected>Seleccionar</option>
                      @foreach($productos as $producto)
                      <option value="{{$producto->id_producto}}">{{$producto->codigo . " - " . $producto->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group col-sm-2" style="align-self: flex-end;">
                    <button type="button" onclick="return agregar_venta();" id="agregar" name="agregar" class="form-control btn btn-info">Agregar</button>
                  </div>
                  <div class="form-group col-sm-2" style="align-self: flex-end;">
                    <input id='check_iva' name='check_iva' type="checkbox" {{$productosPedido[0]->iva == 0 ? '' : 'checked'}} data-toggle="toggle" data-on="Con iva" data-off="Sin iva" onchange="cambiarIva(this.checked);">
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-sm-12">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Producto</th>
                          <th>Último Precio</th>
                          <th>Iva</th>
                          <th>Precio Unitario</th>
                          <th>Cantidad</th>
                          <th>Precio Total</th>
                          <th>Descuento</th>
                          <th>Precio sin iva</th>
                          <th>Precio con iva</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody id="productos_venta">
                        @php
                        $cantidad_items = 0;
                        $precio_total = 0;
                        $descuento = 0;
                        $precio_final_sin_iva = 0;
                        $precio_final = 0;
                        @endphp
                        @foreach($productosPedido as $prod)
                        @php
                        switch (intval($prod->iva_producto)) {
                            case 3:
                            $el_iva = '0';
                            break;
                            case 4:
                            $el_iva = '10.5';
                            break;
                            case 5:
                            $el_iva = '21';
                            break;
                            case 6:
                            $el_iva = '27';
                            break;
                            case 8:
                            $el_iva = '5';
                            break;
                            case 9:
                            $el_iva = '2.5';
                            break;
                            default:
                            $el_iva = '0';
                            break;
                        } 
                        $iva_actual = $prod->iva;
                        $cantidad_items = $cantidad_items + 1;
                        $precio_sin_desc = $prod->precio_unitario * $prod->cantidad;
                        $precio_total = $precio_total + $precio_sin_desc;
                        $descuento = $descuento + $prod->descuento;
                        $precio_con_desc_sin_iva = $precio_sin_desc * (1 - $prod->descuento / 100.00);
                        $precio_con_desc = $precio_sin_desc * (1 - $prod->descuento / 100.00) * (1.0+(floatval($iva_actual) / 100.0));
                        $precio_final_sin_iva = $precio_final_sin_iva + $precio_con_desc_sin_iva;
                        $precio_final = $precio_final + $precio_con_desc;
                        @endphp
                        <tr id="fila{{$prod->id_producto}}">
                          <input name="items[{{$prod->id_producto}}][id]" value="{{$prod->id_producto}}" type="hidden" class="form-control" id="items[{{$prod->id_producto}}][id]">
                          <input name="items[{{$prod->id_producto}}][costo_unitario]" value="{{($prod->costo_unitario)}}" type="hidden" class="form-control" id="items[{{$prod->id_producto}}][costo_unitario]">
                          <input name="items[{{$prod->id_producto}}][iva_producto]" value="{{($el_iva)}}" type="hidden" class="form-control" id="items[{{$prod->id_producto}}][iva_producto]">
                          <input name="items[{{$prod->id_producto}}][iva]" value="{{($iva_actual)}}" type="hidden" class="form-control" id="items[{{$prod->id_producto}}][iva]">
                          <td>{{$prod->nombre}}</td>
                          <td>{{$prod->precio_unitario}}</td>
                          <td id="iva_txt">{{$iva_actual}}%</td>
                          <td><input name="items[{{$prod->id_producto}}][precio_unitario]" readonly value="{{$prod->precio_unitario}}" type="number" class="form-control" id="items[{{$prod->id_producto}}][precio_unitario]"></td>
                          <td><input name="items[{{$prod->id_producto}}][cantidad]" type="number" value="{{$prod->cantidad}}" onchange="return actualizar('{{$prod->id_producto}}')" class="form-control" id="items[{{$prod->id_producto}}][cantidad]"></td>
                          <td><input name="items[{{$prod->id_producto}}][precio_total]" readonly value="{{$precio_sin_desc}}" type="number" class="form-control" id="items[{{$prod->id_producto}}][precio_total]"></td>
                          <td><input name="items[{{$prod->id_producto}}][descuento]" step="0.01" type="number" value="{{$prod->descuento}}" onchange="return actualizarDescuento('{{$prod->id_producto}}')" class="form-control" id="items[{{$prod->id_producto}}][descuento]"></td>
                          <td><input name="items[{{$prod->id_producto}}][precio_final_sin_iva]" readonly value="{{$precio_con_desc_sin_iva}}" type="number" class="form-control" id="items[{{$prod->id_producto}}][precio_final_sin_iva]"></td>
                          <td><input name="items[{{$prod->id_producto}}][precio_final]" readonly value="{{$precio_con_desc}}" type="number" class="form-control" id="items[{{$prod->id_producto}}][precio_final]"></td>
                          <td><button onclick="return borrar('{{$prod->id_producto}}')" class="btn btn-danger">Eliminar</button></td>
                        </tr>
                        @endforeach
                      </tbody>
                      <tbody>
                          <th colspan="5">Totales</th>
                          <th id="precio_total_final" name="precio_total_final">$ {{number_format($precio_total,2,",",".")}}</th>
                          <th id="descuento_final" name="descuento_final">{{number_format($descuento / $cantidad_items,2,",",".")}}</th>
                          <th id="precio_final_final_sin_iva" name="precio_final_final_sin_iva">$ {{number_format($precio_final_sin_iva,2,",",".")}}</th>
                          <th id="precio_final_final" name="precio_final_final">$ {{number_format($precio_final,2,",",".")}}</th>
                          <th></th>
                      </tbody>
                    </table>
                  </div>
                </div>
                <hr>
                @if(env('DEVOLUCIONES_VENTA', false))
                <div class="row">
                  <div class="form-group col-sm-2">
                    <label for="producto_id_dev">Producto devolución</label>
                  </div>
                  <div class="form-group col-sm-6">
                    <select name="producto_id_dev" class="form-control js-example-basic-single" id="producto_id_dev">
                      <option value="" disabled selected>Seleccionar</option>
                      @foreach($productos as $producto)
                      <option value="{{$producto->id_producto}}">{{$producto->codigo . " - " . $producto->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group col-sm-2" style="align-self: flex-end;">
                    <button type="button" onclick="return agregar_devolucion();" id="agregar" name="agregar" class="form-control btn btn-info">Agregar</button>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-sm-12">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Producto</th>
                          <th>Cantidad</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody id="productos_devolucion">
                        @foreach($devolucionesPedido as $prod)
                        <tr id="fila_dev{{$prod->id_producto}}">
                          <input name="items_dev[{{$prod->id_producto}}][id]" value="{{$prod->id_producto}}" type="hidden" class="form-control" id="items_dev[{{$prod->id_producto}}][id]">
                          <input name="items_dev[{{$prod->id_producto}}][costo_unitario]" value="{{($prod->costo_unitario)}}" type="hidden" class="form-control" id="items_dev[{{$prod->id_producto}}][costo_unitario]">
                          <td>{{$prod->nombre}}</td>
                          <td><input name="items_dev[{{$prod->id_producto}}][cantidad]" type="number" value="{{$prod->cantidad}}" class="form-control" id="items_dev[{{$prod->id_producto}}][cantidad]"></td>
                          <td><button onclick="return borrar_dev('{{$prod->id_producto}}')" class="btn btn-danger">Eliminar</button></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
                <hr>
                @endif
                @if(env('PROMOCIONES_VENTA', false))
                <div class="row">
                  <div class="form-group col-sm-2">
                    <label for="producto_id_prom">Producto promoción</label>
                  </div>
                  <div class="form-group col-sm-6">
                    <select name="producto_id_prom" class="form-control js-example-basic-single" id="producto_id_prom">
                      <option value="" disabled selected>Seleccionar</option>
                      @foreach($productos as $producto)
                      <option value="{{$producto->id_producto}}">{{$producto->codigo . " - " . $producto->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group col-sm-2" style="align-self: flex-end;">
                    <button type="button" onclick="return agregar_promocion();" id="agregar" name="agregar" class="form-control btn btn-info">Agregar</button>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-sm-12">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Producto</th>
                          <th>Cantidad</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody id="productos_promocion">
                        @foreach($promocionesPedido as $prod)
                        <tr id="fila_prom{{$prod->id_producto}}">
                          <input name="items_prom[{{$prod->id_producto}}][id]" value="{{$prod->id_producto}}" type="hidden" class="form-control" id="items_prom[{{$prod->id_producto}}][id]">
                          <input name="items_prom[{{$prod->id_producto}}][costo_unitario]" value="{{($prod->costo_unitario)}}" type="hidden" class="form-control" id="items_prom[{{$prod->id_producto}}][costo_unitario]">
                          <td>{{$prod->nombre}}</td>
                          <td><input name="items_prom[{{$prod->id_producto}}][cantidad]" type="number" value="{{$prod->cantidad}}" class="form-control" id="items_prom[{{$prod->id_producto}}][cantidad]"></td>
                          <td><button onclick="return borrar_prom('{{$prod->id_producto}}')" class="btn btn-danger">Eliminar</button></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
                <hr>
                @endif
                <div class="row">
                  <div class="form-group col-sm-6">
                    <div class="row">
                      <div class="col-md-12">
                          <label>Observaciones:</label>
                          <textarea name="observaciones" rows="5" class="form-control" id="observaciones" placeholder="Ingresar observaciones internas..."><?= str_replace('<br />',"", $pedido->descripcion) ?></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="form-group col-sm-6">
                    <div class="row">
                      <div class="col-md-12">
                          <label>Observaciones públicas:</label>
                          <textarea name="observaciones_publicas" rows="5" class="form-control" id="observaciones_publicas" placeholder="Ingresar observaciones públicas..."><?= str_replace('<br />',"", $pedido->observaciones_publicas) ?></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-success" onclick="aceptar(event);">Editar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
@stop
