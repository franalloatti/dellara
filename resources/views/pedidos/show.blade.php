@extends('adminlte::page')

@section('content')
<head>
</head>
<body>
    <script type="text/javascript">
    function cambiarestado(id_pedido, id_estado){
        $.get('../cambiarestado/'+id_pedido+"/"+id_estado, function(data){
            location.reload();
        }).fail(response => {
            toastr.error(response.responseJSON.message);
        });
    }
    </script>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Pedido</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="float-right">
                            <div class="btn-group">
                                <button type="button" {{$pedido->estado == 'Cancelado' ? 'disabled' : ''}} class="btn btn-success btn-sm">Estado: {{$pedido->id_remito === null && $pedido->estado == 'En preparación' ? 'Solicitado' : $pedido->estado}}</button>
                                <button type="button" {{$pedido->estado == 'Cancelado' ? 'disabled' : ''}} class="btn btn-success btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown">                                <span class="sr-only">Toggle Dropdown</span>
                                <ul class="dropdown-menu dropdown-menu-success" role="menu">
                                    @foreach($estados as $estado)
                                    <li style="{{$estado->nombre == $pedido->estado ? 'background-color:lightgrey;' : ''}}">
                                        <a onclick="return cambiarestado({{$pedido->id_pedido}}, {{$estado->id_estado}})" class="btn btn-sm">
                                            &nbsp;&nbsp;&nbsp;{{$pedido->id_remito === null && $estado->nombre == 'En preparación' ? 'Solicitado' : $estado->nombre}}
                                        </a>&nbsp;&nbsp;&nbsp;  
                                    </li>
                                    @endforeach
                                </ul>
                                </button>
                            </div>&nbsp;&nbsp;&nbsp;
                            @if($pedido->estado != 'Cancelado')
                                @if(($pedido->monto_remito == null || $pedido->monto_remito == 0) && ($pedido->estado == 'Artículos pendientes' || $pedido->estado == 'En preparación'))
                                <a href="{{ route('pedidos.editar', [$pedido->id_pedido]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Editar</a>&nbsp;&nbsp;&nbsp;
                                @endif
                                @if($pedido->id_remito === null)
                                    <a href="{{ route('pedidos.remito', [$pedido->id_pedido]) }}" class="btn btn-sm btn-info"><i class="fa fa-file-pdf"></i>&nbsp;&nbsp;&nbsp;Generar remito</a>&nbsp;&nbsp;&nbsp;
                                @else
                                    <a href="{{ route('remitos.ver', [$pedido->id_remito]) }}" class="btn btn-sm btn-secondary"><i class="fa fa-file-pdf"></i>&nbsp;&nbsp;&nbsp;Remito asociado</a>&nbsp;&nbsp;&nbsp;
                                    <a href="#" data-toggle="modal" data-target="#editarDatosRemito" type="button" class="btn btn-sm btn-primary" ><i class="fa fa-edit"></i>&nbsp;&nbsp;&nbsp;Editar datos remito</a>&nbsp;&nbsp;&nbsp;
                                @endif
                            @endif
                        </div>
                        <br><br>
                        <form class="form-horizontal">
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Cliente</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$pedido->name}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Direccion de entrega</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$pedido->direccion}} - {{$pedido->localidad}} ({{$pedido->provincia}})</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Flete</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$pedido->flete}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Nro. factura</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$pedido->nro_factura}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Fecha tentativa de cobro</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p>{{$pedido->fecha_estimada_recepcion != '' ? date('d/m/Y', strtotime($pedido->fecha_estimada_recepcion)) : '-'}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Observaciones internas</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p><?= $pedido->descripcion ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Observaciones públicas</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <p><?= $pedido->observaciones_publicas ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Productos venta</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <table class="table table-bordered">
                                        <thead>                  
                                            <tr>
                                            <th>Codigo</th>
                                            <th>Nombre</th>
                                            <th>Iva</th>
                                            <th>Cantidad</th>
                                            <th>Precio unitario</th>
                                            <th>Precio total</th>
                                            <th>Descuento</th>
                                            <th>Precio final</th>
                                            <th>Precio final con iva</th>
                                            <th>Unidades disponibles</th>
                                            <!-- <th>Estado</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @php 
                                        $total_sin_descuento = 0.00;
                                        $total_con_descuento = 0.00;
                                        $total_con_descuento_con_iva = 0.00;
                                        @endphp
                                        @foreach($productos as $producto)
                                        @php 
                                        $el_iva = $producto->iva;
                                        $renglon_sin_descuento = ($producto->cantidad * $producto->precio_unitario);
                                        $renglon_con_descuento = $renglon_sin_descuento*(1-($producto->descuento/100));
                                        $renglon_con_descuento_con_iva = $renglon_con_descuento*(1+($producto->iva/100));
                                        $total_sin_descuento = $total_sin_descuento + $renglon_sin_descuento;
                                        $total_con_descuento = $total_con_descuento + $renglon_con_descuento;
                                        $total_con_descuento_con_iva = $total_con_descuento_con_iva + $renglon_con_descuento_con_iva;
                                        @endphp
                                            <tr>
                                            <td>{{$producto->codigo}}</td>
                                            <td>{{$producto->nombre}}</td>
                                            <td>{{$el_iva}}%</td>
                                            <td>{{$producto->cantidad}}</td>
                                            <td style="white-space: nowrap;">$ {{number_format($producto->precio_unitario,2,",",".")}}</td>
                                            <td style="white-space: nowrap;">$ {{number_format($renglon_sin_descuento,2,",",".")}}</td>
                                            <td>{{$producto->descuento}} %</td>
                                            <td style="white-space: nowrap;">$ {{number_format($renglon_con_descuento,2,",",".")}}</td>
                                            <td style="white-space: nowrap;">$ {{number_format($renglon_con_descuento_con_iva,2,",",".")}}</td>
                                            <td>{{$producto->stock}}</td>
                                            <!-- <td>{{$producto->descuento}}</td> -->
                                            </tr>
                                            @endforeach
                                            <tr>
                                            <td colspan="5"><strong>TOTALES</strong></td>
                                            <td style="white-space: nowrap;"><strong>$ {{number_format($total_sin_descuento,2,",",".")}}</strong></td>
                                            <td></td>
                                            <td style="white-space: nowrap;"><strong>$ {{number_format(($total_con_descuento),2,",",".")}}</strong></td>
                                            <td style="white-space: nowrap;"><strong>$ {{number_format(($total_con_descuento_con_iva),2,",",".")}}</strong></td>
                                            <td></td>
                                            </tr>
                                        </tbody>
                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                @if(env('DEVOLUCIONES_VENTA',false) && count($devoluciones) > 0)
                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="card-title">Productos devolución</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <table class="table table-bordered">
                                                <thead>                  
                                                    <tr>
                                                        <th>Codigo</th>
                                                        <th>Nombre</th>
                                                        <th>Cantidad</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($devoluciones as $producto)
                                                    <tr>
                                                        <td>{{$producto->codigo}}</td>
                                                        <td>{{$producto->nombre}}</td>
                                                        <td>{{$producto->cantidad}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                                @endif
                                @if(env('PROMOCIONES_VENTA',false) && count($promociones) > 0)
                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="card-title">Productos promoción</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <table class="table table-bordered">
                                                <thead>                  
                                                    <tr>
                                                        <th>Codigo</th>
                                                        <th>Nombre</th>
                                                        <th>Cantidad</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($promociones as $producto)
                                                    <tr>
                                                        <td>{{$producto->codigo}}</td>
                                                        <td>{{$producto->nombre}}</td>
                                                        <td>{{$producto->cantidad}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.tab-pane -->
    
    <!-- Modal agregar movimiento-->
    <div class="modal fade" id="editarDatosRemito" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <form role="form" id="quickForm" method="post" action="{{ route('pedidos.editarRemito', $pedido->id_pedido) }}">
          @csrf
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Editar datos</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="form-group col-sm-6">
                    <label for="flete">Flete</label>
                    <input type="text" name="flete" class="form-control" value="{{$pedido->flete}}" id="flete" required>
                  </div>
                  <div class="form-group col-sm-6">
                    <label for="nro_factura">Nro factura</label>
                    <input type="text" name="nro_factura" class="form-control" id="nro_factura" value="{{$pedido->nro_factura}}" required>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-sm-6">
                    <label for="fecha_estimada_recepcion">Fecha tentativa de cobro</label>
                    <input type="date" name="fecha_estimada_recepcion" class="form-control" value="{{$pedido->fecha_estimada_recepcion}}" id="fecha_estimada_recepcion" required>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-sm-6">
                      <div class="row">
                        <div class="col-md-12">
                            <label>Observaciones:</label>
                            <textarea name="descripcion" rows="5" class="form-control" id="descripcion" placeholder="Ingresar observaciones internas...">{{$pedido->descripcion}}</textarea>
                        </div>
                      </div>
                  </div>
                  <div class="form-group col-sm-6">
                      <div class="row">
                        <div class="col-md-12">
                            <label>Observaciones públicas:</label>
                            <textarea name="observaciones_publicas" rows="5" class="form-control" id="observaciones_publicas" placeholder="Ingresar observaciones públicas...">{{$pedido->observaciones_publicas}}</textarea>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                  <button type="submit" class="btn btn-sm btn-success">Aceptar</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </body>
@stop