@extends('adminlte::page')

@section('content')
  <head>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
   
    @if(env('AUTOCOMPLETE_DIRECCION', false))
    <script async
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCSScLFBA4mU-Gc4CD1hwahrjABPV-6b08&libraries=places&callback=initAutocomplete">
    </script>
    @endif
  </head>
  <?php 
  if(env('PRECIO_EDITABLE_VENTA', false)){
    $readonly = '';
  }else{
    $readonly = 'readonly';
  }
  ?>
  <body>
    <script type="text/javascript">
      $(document).ready(function(){
        // $(window).keydown(function(event){
        //   if(event.keyCode == 13) {
        //     event.preventDefault();
        //     return false;
        //   }
        // });
        $('.js-example-basic-single').select2({matcher: matcher});
        var seleccion_anterior = '';
        $('#user_id_user').on('change', function(e){
          if(this.value != seleccion_anterior && ($("#productos_venta tr").length > 0 || $("#productos_devolucion tr").length > 0 || $("#productos_promocion tr").length > 0)){
            swal({
              title: 'Esta seguro/a de cambiar de cliente?',
              text: 'Si lo hace se eliminarán los productos de la venta',
              icon: 'warning',
              buttons: ["Cancelar", "Confirmar"],
            }).then(function(value) {
                if (value) {
                  $("#productos_venta").html('');
                  $("#productos_devolucion").html('');
                  $("#productos_promocion").html('');
                  $('#btn_agregar_venta').prop('disabled', true);
                  $('#btn_agregar_devolucion').prop('disabled', true);
                  $('#btn_agregar_promocion').prop('disabled', true);
                  $('#user_id_user').val(e.target.value).trigger('change');
                }else{
                  $('#user_id_user').val(seleccion_anterior).trigger('change');
                }
            });
          }else{
            $('#btn_agregar_venta').prop('disabled', false);
            $('#btn_agregar_devolucion').prop('disabled', false);
            $('#btn_agregar_promocion').prop('disabled', false);
            seleccion_anterior = e.target.value;
            var user_id_user = e.target.value;
            $.get('direccionescliente/'+user_id_user, function(data){
              var direccion_id_direccion = '<option value="" disabled selected>Seleccionar</option>'
              for (var i=0; i<data.length;i++)
              direccion_id_direccion+='<option value="'+data[i].id_direccion+'">'+data[i].direccion+'</option>';
              $("#direccion_id_direccion").html(direccion_id_direccion);
            }).fail(response => {
              toastr.error(response.responseJSON.message);
            });
          }
        });
      });
      function aceptar(e){
        e.preventDefault();
        var bandera = 0;
        var confirmacion = false;
        if($("#productos_venta tr").length == 0){
          toastr.error('No se puede generar una venta vacía');
          return false;
        }else{
          $("#productos_venta tr").each(function(index, tr) { 
              if($(this).find("input[name*='[cantidad]']").val() <= 0){
                bandera = 1;
                toastr.error('No se puede generar una venta con un item con cantidad 0 (cero)');
                return false;
              }
          }).promise().done( function() {
              if(!bandera){
                swal({
                  title: 'Esta seguro/a?',
                  text: 'Esta seguro/a que desea confirmar la venta?',
                  icon: 'warning',
                  buttons: ["Cancelar", "Confirmar"],
                }).then(function(value) {
                    if (value) {
                      $('#quickForm').submit();
                    }
                });
              }
          });
        }
      };
      function agregar_venta(){
        var readonly = '{!! $readonly !!}';
        var producto_seleccionado = $('#producto_id').val();
        if(document.getElementById('items['+producto_seleccionado+'][id]') != null){
          toastr.error("El producto ya se encuentra en el pedido");
          return false;
        }else{
          var descuentoCliente = $('#descuentoCliente').val();
          var productos = {!! $productos !!};
          productos.forEach(data => {
            if(producto_seleccionado == data.id_producto){
              var el_precio = 0;
              var check_iva = $('#check_iva').prop('checked');
              var el_iva = parseInt(data.iva);
              switch (el_iva) {
                case 3:
                  el_iva = '0';
                  break;
                case 4:
                  el_iva = '10.5';
                  break;
                case 5:
                  el_iva = '21';
                  break;
                case 6:
                  el_iva = '27';
                  break;
                case 8:
                  el_iva = '5';
                  break;
                case 9:
                  el_iva = '2.5';
                  break;
                default:
                  el_iva = '0';
                  break;
              }
              var iva_actual = '';
              if(check_iva){
                iva_actual = el_iva;
              }else{
                iva_actual = 0;
              }
              if(data.tiene_fijado == 1){
                el_precio = data.precio_fijado;
              }else{
                el_precio = (data.valor_dolar * data.costo_dolares)*(data.margen / 100.00);
              }
              var ultimo_precio = '';
              var user_id_user = $('#user_id_user').val();
              $.ajax({
                async: false,
                type: 'get',
                url: 'getultimoprecio/'+user_id_user+"/"+producto_seleccionado,
                success: function (data) {
                  console.log(data);
                  ultimo_precio = data;
                },
                error: function (request, status, error) {
                    toastr.error(request.responseJSON.message);
                }
              });
              var texto = '<tr id="fila'+data.id_producto+'">'
              +'  <input name="items['+data.id_producto+'][id]" value="'+data.id_producto+'" type="hidden" class="form-control" id="items['+data.id_producto+'][id]">'
              +'  <input name="items['+data.id_producto+'][costo_unitario]" value="'+parseFloat(data.valor_dolar * data.costo_dolares).toFixed(2)+'" type="hidden" class="form-control" id="items['+data.id_producto+'][costo_unitario]">'
              +'  <input name="items['+data.id_producto+'][iva_producto]" value="'+el_iva+'" type="hidden" class="form-control" id="items['+data.id_producto+'][iva_producto]">'
              +'  <input name="items['+data.id_producto+'][iva]" value="'+iva_actual+'" type="hidden" class="form-control" id="items['+data.id_producto+'][iva]">'
              +'  <td>'+data.nombre+'</td>'
              +'  <td>'+ultimo_precio+'</td>'
              +'  <td id="iva_txt">'+iva_actual+'%</td>'
              +'  <td><input name="items['+data.id_producto+'][precio_unitario]" '+readonly+' value="'+parseFloat(el_precio).toFixed(2)+'" type="number" class="form-control" id="items['+data.id_producto+'][precio_unitario]"></td>'
              +'  <td><input name="items['+data.id_producto+'][cantidad]" type="number" value="0" onchange="return actualizar('+data.id_producto+')" class="form-control" id="items['+data.id_producto+'][cantidad]"></td>'
              +'  <td><input name="items['+data.id_producto+'][precio_total]" readonly value="0" type="number" class="form-control" id="items['+data.id_producto+'][precio_total]"></td>'
              +'  <td><input name="items['+data.id_producto+'][descuento]" step="0.01" type="number" value="'+descuentoCliente+'" onchange="return actualizarDescuento('+data.id_producto+')" class="form-control" id="items['+data.id_producto+'][descuento]"></td>'
              +'  <td><input name="items['+data.id_producto+'][precio_final_sin_iva]" readonly value="0" type="number" class="form-control" id="items['+data.id_producto+'][precio_final_sin_iva]"></td>'
              +'  <td><input name="items['+data.id_producto+'][precio_final]" readonly value="0" type="number" class="form-control" id="items['+data.id_producto+'][precio_final]"></td>'
              +'  <td><button onclick="return borrar('+data.id_producto+');" class="btn btn-danger">Eliminar</button></td>'
              +'</tr>'
              $('#productos_venta').append(texto);
              return;
            }
          });
          $('#producto_id').val($('#producto_id option:eq(0)').val()).trigger('change');
          actualizarTotales();
          return false;
        }
      };
      function agregar_promocion(){
        var producto_seleccionado = $('#producto_id_prom').val();
        if(document.getElementById('items_prom['+producto_seleccionado+'][id]') != null){
          toastr.error("El producto ya se encuentra en el pedido");
          return false;
        }else{
          var productos = {!! $productos !!};
          productos.forEach(data => {
            if(producto_seleccionado == data.id_producto){
              var texto = '<tr id="fila_prom'+data.id_producto+'">'
              +'  <input name="items_prom['+data.id_producto+'][id]" value="'+data.id_producto+'" type="hidden" class="form-control" id="items_prom['+data.id_producto+'][id]">'
              +'  <input name="items_prom['+data.id_producto+'][costo_unitario]" value="'+parseFloat(data.valor_dolar * data.costo_dolares).toFixed(2)+'" type="hidden" class="form-control" id="items_prom['+data.id_producto+'][costo_unitario]">'
              +'  <td>'+data.nombre+'</td>'
              +'  <td><input name="items_prom['+data.id_producto+'][cantidad]" type="number" value="0" class="form-control" id="items_prom['+data.id_producto+'][cantidad]"></td>'
              +'  <td><button onclick="return borrar_prom('+data.id_producto+');" class="btn btn-danger">Eliminar</button></td>'
              +'</tr>'
              $('#productos_promocion').append(texto);
              return;
            }
          });
          $('#producto_id_prom').val($('#producto_id_prom option:eq(0)').val()).trigger('change');
          return false;
        }
      };
      function agregar_devolucion(){
        var producto_seleccionado = $('#producto_id_dev').val();
        if(document.getElementById('items_dev['+producto_seleccionado+'][id]') != null){
          toastr.error("El producto ya se encuentra en el pedido");
          return false;
        }else{
          var productos = {!! $productos !!};
          productos.forEach(data => {
            if(producto_seleccionado == data.id_producto){
              var texto = '<tr id="fila_dev'+data.id_producto+'">'
              +'  <input name="items_dev['+data.id_producto+'][id]" value="'+data.id_producto+'" type="hidden" class="form-control" id="items_dev['+data.id_producto+'][id]">'
              +'  <input name="items_dev['+data.id_producto+'][costo_unitario]" value="'+parseFloat(data.valor_dolar * data.costo_dolares).toFixed(2)+'" type="hidden" class="form-control" id="items_dev['+data.id_producto+'][costo_unitario]">'
              +'  <td>'+data.nombre+'</td>'
              +'  <td><input name="items_dev['+data.id_producto+'][cantidad]" type="number" value="0" class="form-control" id="items_dev['+data.id_producto+'][cantidad]"></td>'
              +'  <td><button onclick="return borrar_dev('+data.id_producto+');" class="btn btn-danger">Eliminar</button></td>'
              +'</tr>'
              $('#productos_devolucion').append(texto);
              return;
            }
          });
          $('#producto_id_dev').val($('#producto_id_dev option:eq(0)').val()).trigger('change');
          return false;
        }
      };
      function setDescuento(id_cliente){
        var clientes = {!! $clientes !!};
        clientes.forEach(data => {
          if(id_cliente == data.id_user){
            document.getElementById('descuentoCliente').value = data.descuento;
            $("#productos_venta tr").each(function(index, tr) { 
              var precio_total = $(this).find("input[name*='[precio_total]']").val();
              var el_iva = $(this).find("input[name*='[iva]']").val();
              console.log(el_iva)
              $(this).find("input[name*='[descuento]']").val(data.descuento);
              $(this).find("input[name*='[precio_final_sin_iva]']").val(parseFloat(precio_total *(1 - data.descuento/100)).toFixed(2));
              $(this).find("input[name*='[precio_final]']").val(parseFloat(precio_total *(1 - data.descuento/100)*(1.0+(parseFloat(el_iva)/100.0))).toFixed(2));
            });
            // $( "input[name*='[descuento]']" ).val(data.descuento);
            return;
          }
        });
        if(id_cliente != ''){
          document.getElementById('btnAddDireccion').hidden = false;
        }else{
          document.getElementById('btnAddDireccion').hidden = true;
        }
        actualizarTotales();
      };
      function actualizarTotales(){
        var cantidadItems = 0;
        var precio_total = 0;
        var descuento = 0;
        var precio_final = 0;
        var precio_final_sin_iva = 0;
        $("#productos_venta tr").each(function(index, tr) { 
          cantidadItems = cantidadItems + 1;
          precio_total = parseFloat(precio_total) + parseFloat($(this).find("input[name*='[precio_total]']").val());
          descuento = parseFloat(descuento) + parseFloat($(this).find("input[name*='[descuento]']").val());
          precio_final_sin_iva = parseFloat(precio_final_sin_iva) + parseFloat($(this).find("input[name*='[precio_final_sin_iva]']").val());
          precio_final = parseFloat(precio_final) + parseFloat($(this).find("input[name*='[precio_final]']").val());
        });
        document.getElementById('precio_total_final').innerHTML = parseFloat(precio_total).toFixed(2);
        document.getElementById('descuento_final').innerHTML = cantidadItems ? parseFloat(descuento / cantidadItems).toFixed(2) : parseFloat(0).toFixed(2);
        document.getElementById('precio_final_final_sin_iva').innerHTML = parseFloat(precio_final_sin_iva).toFixed(2);
        document.getElementById('precio_final_final').innerHTML = parseFloat(precio_final).toFixed(2);
      };
      function actualizar(id_producto){
        var el_iva = document.getElementById('items['+id_producto+'][iva]').value;
        var precio_unitario = document.getElementById('items['+id_producto+'][precio_unitario]').value;
        var descuento = document.getElementById('items['+id_producto+'][descuento]').value;
        var cantidad = document.getElementById('items['+id_producto+'][cantidad]').value;
        document.getElementById('items['+id_producto+'][precio_total]').value = parseFloat(precio_unitario*cantidad).toFixed(2);
        document.getElementById('items['+id_producto+'][precio_final_sin_iva]').value = parseFloat(precio_unitario*cantidad*(1-descuento/100)).toFixed(2);
        document.getElementById('items['+id_producto+'][precio_final]').value = parseFloat(precio_unitario*cantidad*(1-descuento/100)*(1.0+(parseFloat(el_iva)/100.0))).toFixed(2);
        actualizarTotales();
      };
      function actualizarDescuento(id_producto){
        var precio_total = document.getElementById('items['+id_producto+'][precio_total]').value;
        var descuento = document.getElementById('items['+id_producto+'][descuento]').value;
        var el_iva = document.getElementById('items['+id_producto+'][iva]').value;
        document.getElementById('items['+id_producto+'][precio_final_sin_iva]').value = parseFloat(precio_total*(1-descuento/100)).toFixed(2);
        document.getElementById('items['+id_producto+'][precio_final]').value = parseFloat(precio_total*(1-descuento/100)*(1.0+(parseFloat(el_iva)/100.0))).toFixed(2);
        actualizarTotales();
      };
      function cambiarIva(checked){
        $("#productos_venta tr").each(function(index, tr) { 
          if(checked){
            var iva_producto = $(this).find("input[name*='[iva_producto]']").val();
            $(this).find("input[name*='[iva]']").val(iva_producto);
            $(this).find("#iva_txt").html(iva_producto+'%');
          }else{
            var el_iva = '0';
            $(this).find("input[name*='[iva]']").val(0);
            var txt = $(this).find("#iva_txt");
            txt.html(el_iva+'%');
          }
          actualizar($(this).find("input[name*='[id]']").val());
        });
      }
      function borrar(id_producto){
        $("#fila" + id_producto).remove();
        actualizarTotales();
      }
      function borrar_prom(id_producto){
        $("#fila_prom" + id_producto).remove();
        actualizarTotales();
      }
      function borrar_dev(id_producto){
        $("#fila_dev" + id_producto).remove();
        actualizarTotales();
      }
      function functionAddDireccion(){
          $("body").css('cursor','wait'); 
          var direccion = $('#direccion').val();
          var observaciones = $('#observaciones').val();
          var provincia = $('#provincia').val();
          var localidad = $('#localidad').val();
          var codigo_postal = $('#codigo_postal').val();
          var telefono = $('#telefono').val();
          var cliente = $('#user_id_user').val();
          $.get('/adddireccion', { direccion, observaciones, provincia, localidad, codigo_postal, telefono, cliente}, function(data){
              if(data.success != undefined){
                  var html_old = $("#direccion_id_direccion").html();
                  var html_new = '<option value="'+data.id+'">'+direccion+'</option>';
                  $("#direccion_id_direccion").html(html_old + html_new);
                  $('#direccion_id_direccion').val($('#direccion_id_direccion option:eq('+($('#direccion_id_direccion').find('option:not(:disabled)').length)+')').val()).trigger('change');
                  toastr.success(data.success);
                  $("body").css('cursor','default'); 
                  $("#addDireccion").modal("toggle");
              }else{
                  $("body").css('cursor','default'); 
                  toastr.error(data.error);
              }
          }).fail(response => {
            toastr.error(response.responseJSON.message);
            $("body").css('cursor','default'); 
          });
      };
    </script>
    <style>
    .select2-container .select2-selection--single{
      height: auto !important;
    }
    .select2-container {
      width: 100% !important;
    }
    </style>
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- jquery validation -->
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">Pedido <small>Crear</small></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" id="quickForm" method="post" action="{{ route('pedidos.guardar') }}">
              @csrf
              <div class="card-body">
                <div class="row">
                  <div class="form-group col-sm-6">
                    <label for="user_id_user">Cliente</label>
                    <select name="user_id_user" onchange="return setDescuento(this.value)" class="form-control js-example-basic-single" id="user_id_user" required>
                      <option value="" disabled selected>Seleccionar</option>
                      @foreach($clientes as $cliente)
                      <option value="{{$cliente->id_user}}">{{$cliente->name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <input name="descuentoCliente" value="0" type="hidden" class="form-control" id="descuentoCliente">
                  <div class="form-group col-sm-6">
                    <label for="direccion_id_direccion">Dirección de entrega</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <button id="btnAddDireccion" data-toggle="modal" data-target="#addDireccion" type="button" class="btn btn-sm btn-primary" data-dismiss="modal" hidden>+</button>
                    <select name="direccion_id_direccion" class="form-control js-example-basic-single" id="direccion_id_direccion" required>
                      <option value="" disabled selected>Seleccionar</option>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-sm-6">
                    <label for="flete">Flete</label>
                    <input type="text" name="flete" class="form-control" id="flete" placeholder="Ingresar datos del flete">
                  </div>
                  <div class="form-group col-sm-6">
                    <label for="nro_factura">Nro. factura</label>
                    <input type="text" name="nro_factura" class="form-control" id="nro_factura" placeholder="Ingresar número de factura">
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-sm-6">
                    <label for="fecha_estimada_recepcion">Fecha tentativa de cobro</label>
                    <input type="date" name="fecha_estimada_recepcion" class="form-control" id="fecha_estimada_recepcion" required>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="form-group col-sm-2">
                    <label for="producto_id">Producto venta</label>
                  </div>
                  <div class="form-group col-sm-6">
                    <select name="producto_id" class="form-control js-example-basic-single" id="producto_id">
                      <option value="" disabled selected>Seleccionar</option>
                      @foreach($productos as $producto)
                      <option value="{{$producto->id_producto}}">{{$producto->codigo . " - " . $producto->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group col-sm-2" style="align-self: flex-end;">
                    <button type="button" onclick="return agregar_venta();" id="btn_agregar_venta" name="btn_agregar_venta" disabled class="form-control btn btn-info">Agregar</button>
                  </div>
                  <div class="form-group col-sm-2" style="align-self: flex-end;">
                    <input id='check_iva' name='check_iva' type="checkbox" data-toggle="toggle" data-on="Con iva" data-off="Sin iva" onchange="cambiarIva(this.checked);">
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-sm-12">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Producto</th>
                          <th>Último Precio</th>
                          <th>Iva</th>
                          <th>Precio Unitario</th>
                          <th>Cantidad</th>
                          <th>Precio Total</th>
                          <th>Descuento</th>
                          <th>Precio sin iva</th>
                          <th>Precio con iva</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody id="productos_venta">
                      </tbody>
                      <tbody>
                          <th colspan="5">Totales</th>
                          <th id="precio_total_final" name="precio_total_final">0</th>
                          <th id="descuento_final" name="descuento_final">0</th>
                          <th id="precio_final_final_sin_iva" name="precio_final_final_sin_iva">0</th>
                          <th id="precio_final_final" name="precio_final_final">0</th>
                          <th></th>
                      </tbody>
                    </table>
                  </div>
                </div>
                <hr>
                @if(env('DEVOLUCIONES_VENTA', false))
                <div class="row">
                  <div class="form-group col-sm-2">
                    <label for="producto_id_dev">Producto devolución</label>
                  </div>
                  <div class="form-group col-sm-6">
                    <select name="producto_id_dev" class="form-control js-example-basic-single" id="producto_id_dev">
                      <option value="" disabled selected>Seleccionar</option>
                      @foreach($productos as $producto)
                      <option value="{{$producto->id_producto}}">{{$producto->codigo . " - " . $producto->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group col-sm-2" style="align-self: flex-end;">
                    <button type="button" onclick="return agregar_devolucion();" id="btn_agregar_devolucion" name="btn_agregar_devolucion" disabled class="form-control btn btn-info">Agregar</button>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-sm-12">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Producto</th>
                          <th>Cantidad</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody id="productos_devolucion">
                      </tbody>
                    </table>
                  </div>
                </div>
                <hr>
                @endif
                @if(env('PROMOCIONES_VENTA', false))
                <div class="row">
                  <div class="form-group col-sm-2">
                    <label for="producto_id_prom">Producto promoción</label>
                  </div>
                  <div class="form-group col-sm-6">
                    <select name="producto_id_prom" class="form-control js-example-basic-single" id="producto_id_prom">
                      <option value="" disabled selected>Seleccionar</option>
                      @foreach($productos as $producto)
                      <option value="{{$producto->id_producto}}">{{$producto->codigo . " - " . $producto->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group col-sm-2" style="align-self: flex-end;">
                    <button type="button" onclick="return agregar_promocion();" id="btn_agregar_promocion" name="btn_agregar_promocion" disabled class="form-control btn btn-info">Agregar</button>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-sm-12">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Producto</th>
                          <th>Cantidad</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody id="productos_promocion">
                      </tbody>
                    </table>
                  </div>
                </div>
                <hr>
                @endif
                <div class="row">
                  <div class="form-group col-sm-6">
                    <div class="row">
                      <div class="col-md-12">
                          <label>Observaciones:</label>
                          <textarea name="descripcion" rows="5" class="form-control" id="descripcion" placeholder="Ingresar observaciones internas..."></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="form-group col-sm-6">
                    <div class="row">
                      <div class="col-md-12">
                          <label>Observaciones públicas:</label>
                          <textarea name="observaciones_publicas" rows="5" class="form-control" id="observaciones_publicas" placeholder="Ingresar observaciones públicas..."></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-success" onclick="aceptar(event);">Crear</button>
              </div>
            </form>
          </div>
          <!-- /.card -->
          </div>
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-6">

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
    <div class="modal fade" id="addDireccion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h3 class="modal-title">Nueva dirección</h3>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <div class="row">
                      <div class="form-group col-sm-6">
                          <label for="direccion">Dirección</label>
                          <input type="text" name="direccion" class="form-control" id="direccion" placeholder="Ingresar dirección" required>
                      </div>
                      <div class="form-group col-sm-6">
                          <label for="observaciones">Observaciones</label>
                          <input type="text" name="observaciones" class="form-control" id="observaciones" placeholder="Ingresar observaciones">
                      </div>
                  </div>
                  <div class="row">
                      <div class="form-group col-sm-6">
                          <label for="provincia">Provincia</label>
                          <input type="text" name="provincia" class="form-control" id="provincia" placeholder="Ingresar provincia" required>
                      </div>
                      <div class="form-group col-sm-6">
                          <label for="localidad">Localidad</label>
                          <input type="text" name="localidad" class="form-control" id="localidad" placeholder="Ingresar localidad" required>
                      </div>
                  </div>
                  <div class="row">
                      <div class="form-group col-sm-6">
                          <label for="codigo_postal">Código postal</label>
                          <input type="text" name="codigo_postal" class="form-control" id="codigo_postal" placeholder="Ingresar código postal" required>
                      </div>
                      <div class="form-group col-sm-6">
                          <label for="telefono">Teléfono</label>
                          <input type="text" name="telefono" class="form-control" id="telefono" placeholder="Ingresar teléfono">
                      </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                  <button type="button" class="btn btn-sm btn-success" onclick="return functionAddDireccion();">Agregar</button>
              </div>
          </div>
      </div>
    </div>
    <script>
        $(document).ready(function() {
                $("#lat_area").addClass("d-none");
                $("#long_area").addClass("d-none");
        });
        </script>
        <script>
        
        let autocomplete;
        let direccionField;
        let codigo_postalField;

        function initAutocomplete() {
            direccionField = document.querySelector("#direccion");
            codigo_postalField = document.querySelector("#codigo_postal");
            autocomplete = new google.maps.places.Autocomplete(direccionField, {
                componentRestrictions: { country: "ar"},
                fields: ["address_components", "geometry"],
                types: ["address"],
            });
            direccionField.focus();
            autocomplete.addListener("place_changed", fillInAddress);
        }
        function fillInAddress() {
            
            const place = autocomplete.getPlace();
            let address1 = "";
            let postcode = "";

            for (const component of place.address_components) {
                
                const componentType = component.types[0];

                switch (componentType) {
                    case "street_number": {
                        address1 = `${component.long_name} ${address1}`;
                        break;
                    }

                    case "route": {
                        address1 += component.short_name;
                        break;
                    }

                    case "postal_code": {
                        postcode = `${component.long_name}${postcode}`;
                        break;
                    }

                    case "postal_code_suffix": {
                        postcode = `${postcode}-${component.long_name}`;
                        break;
                    }

                    case "locality":
                        (document.querySelector("#localidad")).value =
                        component.long_name;
                        break;

                    case "administrative_area_level_1": {
                        (document.querySelector("#provincia")).value =
                        component.short_name;
                        break;
                    }
                }
            }
            direccionField.value = address1;
            codigo_postalField.value = postcode;
        }
    </script>
    <style>
        .pac-container {
            z-index: 10000 !important;
        }
    </style>
  </body>
</html>
@stop
