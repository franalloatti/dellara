@extends('adminlte::page')

@section('content')
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Pedidos en preparación</h3>
                <div class="card-tools">
                  <form role="form" action="/admin/pedidos/enpreparacion" method="POST">
                    @csrf
                    <div class="input-group input-group-sm">
                      <input type="text" id="search" name="search" value="{{$search ? $search : ''}}" class="form-control pull-right" placeholder="Buscar">
                      <div class="input-group-btn">
                        <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th>Id</th>
                      <th>Cliente</th>
                      <th>Estado</th>
                      <th>Monto</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($pedidos as $pedido)
                    <tr>
                      <td>{{$pedido->id_pedido}}</td>
                      <td>{{$pedido->name}}</td>
                      <td>{{$pedido->estado == 'En preparación' ? ($pedido->id_remito === null ? 'Solicitado' : $pedido->estado) : $pedido->estado}}</td>
                      <td align="right" style="white-space: nowrap;">$ {{number_format($pedido->monto,2,",",".")}}</td>
                      <td>
                        <a href="{{ route('pedidos.ver', [$pedido->id_pedido]) }}" class="btn btn-sm btn-success"><i class="fa fa-file "></i>&nbsp;&nbsp;&nbsp;Ver</a>&nbsp;&nbsp;&nbsp;
                        @if(($pedido->monto_remito == null || $pedido->monto_remito == 0) && ($pedido->estado == 'Artículos pendientes' || $pedido->estado == 'En preparación'))
                        <a href="{{ route('pedidos.editar', [$pedido->id_pedido]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit "></i>&nbsp;&nbsp;&nbsp;Editar</a>&nbsp;&nbsp;&nbsp;
                        @endif
                        @if($pedido->estado != 'Entregado' && $pedido->id_remito === null)
                          <a href="{{ route('pedidos.remito', [$pedido->id_pedido]) }}" class="btn btn-sm btn-info"><i class="fa fa-file-pdf"></i>&nbsp;&nbsp;&nbsp;Generar remito</a>&nbsp;&nbsp;&nbsp;
                        @else
                          <a href="{{ route('remitos.ver', [$pedido->id_remito]) }}" class="btn btn-sm btn-secondary"><i class="fa fa-file-pdf"></i>&nbsp;&nbsp;&nbsp;Remito asociado</a>&nbsp;&nbsp;&nbsp;
                        @endif
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                    {{ $pedidos->appends(['search' => $search])->links() }}
              </div>
            </div>
            <!-- /.card -->
            </div>
          </div>
        </div>
        @stop
