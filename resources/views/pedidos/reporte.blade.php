<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Reporte ventas desde <?php date('d/m/Y', strtotime($fecha_desde));?> hasta <?php date('d/m/Y', strtotime($fecha_hasta));?></title>

        <style>
            #table2 {
              border-collapse: collapse;
              border-spacing: 10px;  
              font-size: 10px;  
              border: 1px solid black;
            }
            table, th, td {
                border: 1px solid black;
            }
            h3 {
                font-size: 20px;
            }
        </style>
    </head>
    <body>
        <table id="table2">
            <thead>                  
                <tr>
                    <th>Fecha</th>
                    <th>Cliente</th>
                    <th>Dirección</th>
                    <th>Flete</th>
                    <th>Id remito</th>
                    <th>N factura</th>
                    <th>Producto</th>
                    <th>Costo</th>
                    <th>Tipo</th>
                    <th>Cantidad</th>
                    <th>Precio sin iva</th>
                    <th>Precio con iva</th>
                    <th>Descuento</th>
                    <th>Monto total con iva</th>
                    <th>Fecha cobro</th>
                </tr>
            </thead>
            <tbody>
                @foreach($pedidos as $pedido)
                <tr>
                    <td>{{date('d/m/Y', strtotime($pedido->fecha_remito))}}</td>
                    <td>{{$pedido->name}}</td>
                    <td>{{$pedido->direccion}}</td>
                    <td>{{$pedido->flete}}</td>
                    <td>{{$pedido->id_remito}}</td>
                    <td>{{$pedido->nro_factura}}</td>
                    <td>{{$pedido->codigo}} - {{$pedido->nombre}}</td>
                    <td>{{$pedido->costo_unitario}}</td>
                    <td>{{$pedido->tipo_item}}</td>
                    <td>{{$pedido->cantidad}}</td>
                    <td>{{$pedido->precio_unitario}}</td>
                    <td>{{($pedido->precio_unitario * (1+($pedido->iva/100.0)))}}</td>
                    <td>{{$pedido->descuento}}</td>
                    <td>{{($pedido->precio_unitario * (1+($pedido->iva/100.0)) * $pedido->cantidad*(1.0-($pedido->descuento/100.0)))}}</td>
                    <td>{{$pedido->fecha_cobro ? date('d/m/Y', strtotime($pedido->fecha_cobro)) : ''}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <br>
    </body>
</html>