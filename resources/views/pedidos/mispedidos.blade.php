@extends('adminlte::page')

@section('content')
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Mis pedidos</h3>
                <div class="card-tools">
                  <!-- <form role="form" action="/mispedidos" method="POST">
                    @csrf
                    <div class="input-group input-group-sm">
                      <input type="text" id="search" name="search" value="{{$search ? $search : ''}}" class="form-control pull-right" placeholder="Buscar">
                      <div class="input-group-btn">
                        <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </form> -->
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(!count($pedidos))
                <h3>NO TIENES PEDIDOS</h3>
                @else
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th>Id</th>
                      <th>Estado</th>
                      <th>Fecha</th>
                      <th>Monto</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($pedidos as $pedido)
                    <tr>
                      <td>{{$pedido->id_pedido}}</td>
                      <td>{{$pedido->estado}}</td>
                      <td>{{$pedido->created_at}}</td>
                      <td>$ {{number_format($pedido->monto,2,",",".")}}</td>
                      <td>
                        <a href="{{ route('pedidos.mipedido', [$pedido->id_pedido]) }}" class="btn btn-sm btn-success"><i class="fa fa-file "></i>&nbsp;&nbsp;&nbsp;Ver</a>&nbsp;&nbsp;&nbsp;
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                @endif
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                    {{ $pedidos->links() }}
              </div>
            </div>
            <!-- /.card -->
            </div>
          </div>
        </div>
        @stop
