@extends('adminlte::page')

@section('content')
<head>
</head>
<body>
    <script type="text/javascript">
      function aceptar(e){
        e.preventDefault();
        var bandera = 0;
        if($("#tablaproductos tr").length == 0){
          toastr.error('No se puede generar un remito vacío');
          return false;
        }else{
            $("#tablaproductos tr").each(function(index, tr) { 
                if($(this).find("input[name*='[cantidad]']").val() <= 0){
                    bandera = 1;
                    toastr.error('No se puede generar un remito con un item con cantidad 0 (cero)');
                    return false;
                }
            }).promise().done( function() {
                if(!bandera){
                swal({
                    title: 'Esta seguro/a?',
                    text: 'Esta seguro/a que desea generar un remito?',
                    icon: 'warning',
                    buttons: ["Cancelar", "Confirmar"],
                }).then(function(value) {
                    if (value) {
                        $('#quickForm').submit();
                    }
                });
                }
            });
        }
      }
      function getStockAlmacen(id_almacen,id_producto){
        $("#td_stock_"+id_producto).html('<div class="loader"></div>');
        $.get('/getstockalmacen', {id_almacen: id_almacen, id_producto: id_producto}, function(data){
        // console.log(data);
            $("#td_stock_"+id_producto).html(data);
        }).fail(response => {
            toastr.error(response.responseJSON.message);
        });
      }
      function getStockAlmacenDev(id_almacen,id_producto){
        $("#td_stock_dev_"+id_producto).html('<div class="loader"></div>');
        $.get('/getstockalmacen', {id_almacen: id_almacen, id_producto: id_producto}, function(data){
        // console.log(data);
            $("#td_stock_dev_"+id_producto).html(data);
        }).fail(response => {
            toastr.error(response.responseJSON.message);
        });
      }
      function getStockAlmacenProm(id_almacen,id_producto){
        $("#td_stock_prom_"+id_producto).html('<div class="loader"></div>');
        $.get('/getstockalmacen', {id_almacen: id_almacen, id_producto: id_producto}, function(data){
        // console.log(data);
            $("#td_stock_prom_"+id_producto).html(data);
        }).fail(response => {
            toastr.error(response.responseJSON.message);
        });
      }
      function actualizarTotales(){
        var cantidadItems = 0;
        var precio_total = 0;
        var descuento = 0;
        var precio_final = 0;
        $("#tablaproductos tr").each(function(index, tr) { 
                cantidadItems = cantidadItems + 1;
                precio_total = parseFloat(precio_total) + parseFloat($(this).find("input[name*='[precio_total]']").val());
                descuento = parseFloat(descuento) + parseFloat($(this).find("input[name*='[descuento]']").val());
                precio_final = parseFloat(precio_final) + parseFloat($(this).find("input[name*='[precio_final]']").val());
        });
        document.getElementById('total_sin_descuento').innerHTML = "$ " + parseFloat(precio_total).toFixed(2).replace('.',',');
        document.getElementById('total_con_descuento').innerHTML = "$ " + parseFloat(precio_final).toFixed(2).replace('.',',');
      };
      function actualizar(id_producto){
        var precio_unitario = document.getElementById('items['+id_producto+'][precio_unitario]').value;
        var cantidad = document.getElementById('items['+id_producto+'][cantidad]').value;
        var descuento = document.getElementById('items['+id_producto+'][descuento]').value;
        document.getElementById('items['+id_producto+'][precio_total]').value = parseFloat(precio_unitario*cantidad).toFixed(2);
        document.getElementById('items['+id_producto+'][precio_final]').value = parseFloat(precio_unitario*cantidad*(1-descuento/100)).toFixed(2);
        actualizarTotales();
      };
      function actualizarDescuento(id_producto, descuento){
        var precio_total = document.getElementById('items['+id_producto+'][precio_total]').value;
        document.getElementById('items['+id_producto+'][precio_final]').value = parseFloat(precio_total*(1-descuento/100)).toFixed(2);
        actualizarTotales();
      };
    </script>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" id="quickForm" method="post" action="{{ route('remitos.guardar') }}">
                    @csrf
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Generar remito</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <input name="id_pedido" value="{{$pedido->id_pedido}}" type="hidden" class="form-control" id="id_pedido">
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label for="inputName">Cliente</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input name="user_id_user" value="{{$pedido->user_id_user}}" type="hidden" class="form-control" id="user_id_user">
                                            <p>{{$pedido->name}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                <label for="direccion_id_direccion">Dirección de entrega</label>
                                <select name="direccion_id_direccion" class="form-control js-example-basic-single" id="direccion_id_direccion" required>
                                    <!-- <option value="" disabled selected>Seleccionar</option> -->
                                    @foreach($direcciones as $direccion)
                                    @php 
                                    if($direccion->id_direccion == $pedido->id_direccion){
                                    @endphp 
                                        <option value="{{$direccion->id_direccion}}" selected>{{$direccion->direccion}}</option>    
                                    @php 
                                    }else{
                                    @endphp
                                        <option value="{{$direccion->id_direccion}}">{{$direccion->direccion}}</option>
                                    @php    
                                    }
                                    @endphp
                                    @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="card-title">Productos venta</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <table class="table table-bordered">
                                                <thead>                  
                                                    <tr>
                                                    {{-- <th>
                                                    <input name="items[todos][checkbox]" type="checkbox" onclick="return marcarTodos(this.checked);" id="items['{{$pedido->id_pedido}}'][todos]">
                                                    </th> --}}
                                                    <th>Codigo</th>
                                                    <th>Nombre</th>
                                                    <th>Iva</th>
                                                    <th>Precio unitario</th>
                                                    <th>Cantidad</th>
                                                    <th>Precio total</th>
                                                    <th>Descuento</th>
                                                    <th>Precio final</th>
                                                    <th>Almacen</th>
                                                    <th>Unidades disponibles</th>
                                                    <!-- <th></th> -->
                                                    </tr>
                                                </thead>
                                                <tbody id="tablaproductos">
                                                    @php 
                                                    $total_sin_descuento = 0.00;
                                                    $total_con_descuento = 0.00;
                                                    $total_con_descuento_con_iva = 0.00;
                                                    foreach($productos as $producto){
                                                    $el_iva = $producto->iva;
                                                    $total_item_sin_desc = $producto->cantidad * $producto->precio_unitario;
                                                    $total_item_con_desc = $total_item_sin_desc * (1-($producto->descuento/100));
                                                    $total_item_con_desc_con_iva = $total_item_con_desc * (1.0+($el_iva/100.0));
                                                    $total_sin_descuento = $total_sin_descuento + $total_item_sin_desc;
                                                    $total_con_descuento = $total_con_descuento + $total_item_con_desc;
                                                    $total_con_descuento_con_iva = $total_con_descuento_con_iva + $total_item_con_desc_con_iva;
                                                    $stock = $producto->stock + $producto->mov_destino + $producto->mov_origen + $producto->venta + $producto->devolucion_venta + $producto->compra + $producto->devolucion_compra;
                                                    @endphp
                                                    <tr id="fila{{$producto->id_pedido_producto}}">
                                                        <input name="items[{{$producto->id_pedido_producto}}][id]" value="{{$producto->id_pedido_producto}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][id]">
                                                        <input name="items[{{$producto->id_pedido_producto}}][id_producto]" value="{{$producto->id_producto}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][id_producto]">
                                                        <input name="items[{{$producto->id_pedido_producto}}][costo_unitario]" value="{{$producto->costo_unitario}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][costo_unitario]">
                                                        <input name="items[{{$producto->id_pedido_producto}}][cantidad_pedido]" value="{{$producto->cantidad}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][cantidad_pedido]">
                                                        <input name="items[{{$producto->id_pedido_producto}}][tipo_item]" value="{{$producto->tipo_item}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][tipo_item]">
                                                        <input name="items[{{$producto->id_pedido_producto}}][iva]" value="{{$el_iva}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][iva]">
                                                        <td>{{$producto->codigo}}</td>
                                                        <td>{{$producto->nombre}}</td>
                                                        <td>{{$producto->iva}}%</td>
                                                        <td><input name="items[{{$producto->id_pedido_producto}}][precio_unitario]" step="0.01" value="{{$producto->precio_unitario}}" type="number" class="form-control" id="items[{{$producto->id_pedido_producto}}][precio_unitario]" onchange="return actualizar({{$producto->id_pedido_producto}})"></td>
                                                        <td><input name="items[{{$producto->id_pedido_producto}}][cantidad]" type="number" max="{{$stock}}" oninvalid="this.setCustomValidity('No hay stock suficiente')" value="{{$producto->cantidad}}" onchange="return actualizar({{$producto->id_pedido_producto}})" class="form-control" id="items[{{$producto->id_pedido_producto}}][cantidad]"></td>
                                                        <td><input name="items[{{$producto->id_pedido_producto}}][precio_total]" readonly value="{{$total_item_sin_desc}}" type="number" class="form-control" id="items[{{$producto->id_pedido_producto}}][precio_total]"></td>
                                                        <td><input name="items[{{$producto->id_pedido_producto}}][descuento]" step="0.01" type="number" value="{{$producto->descuento}}" onchange="return actualizarDescuento({{$producto->id_pedido_producto}},this.value)" class="form-control" id="items[{{$producto->id_pedido_producto}}][descuento]"></td>
                                                        <td><input name="items[{{$producto->id_pedido_producto}}][precio_final]" readonly value="{{$total_item_con_desc}}" type="number" class="form-control" id="items[{{$producto->id_pedido_producto}}][precio_final]"></td>
                                                        <td><input name="items[{{$producto->id_pedido_producto}}][precio_final_con_iva]" readonly value="{{$total_item_con_desc_con_iva}}" type="number" class="form-control" id="items[{{$producto->id_pedido_producto}}][precio_final_con_iva]"></td>
                                                        <td>
                                                            <select class="form-control" onchange="getStockAlmacen(this.value, '{{$producto->id_producto}}')" name="items[{{$producto->id_pedido_producto}}][almacen_id]" id="items[{{$producto->id_pedido_producto}}][almacen_id]">
                                                            @php
                                                            foreach ($almacenes as $almacen) {
                                                                if($almacen->id_almacen == env('ALMACEN_PRINCIPAL', 1)){
                                                                    echo "<option selected value='".$almacen->id_almacen."'>".$almacen->nombre."</option>";
                                                                }else{
                                                                    echo "<option value='".$almacen->id_almacen."'>".$almacen->nombre."</option>";
                                                                }
                                                            }
                                                            @endphp
                                                            </select>
                                                        </td>
                                                        <td id="td_stock_{{$producto->id_producto}}">{{number_format($stock,2,",",".")}}</td>
                                                        <!-- <td><button onclick="return borrar('{{$producto->id_pedido_producto}}')" class="btn btn-danger">Eliminar</button></td> -->
                                                    </tr>
                                                    @php 
                                                    }
                                                    @endphp
                                                </tbody>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="5"><strong>TOTALES</strong></td>
                                                        <td><strong id="total_sin_descuento">$ {{number_format($total_sin_descuento,2,",","")}}</strong></td>
                                                        <td></td>
                                                        <td><strong id="total_con_descuento">$ {{number_format($total_con_descuento,2,",","")}}</strong></td>
                                                        <td><strong id="total_con_descuento">$ {{number_format($total_con_descuento_con_iva,2,",","")}}</strong></td>
                                                        <td colspan="2"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                @if(env('DEVOLUCIONES_VENTA',false) && count($devoluciones) > 0)
                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="card-title">Productos devolución</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <table class="table table-bordered">
                                                <thead>                  
                                                    <tr>
                                                    <th>Codigo</th>
                                                    <th>Nombre</th>
                                                    <th>Cantidad</th>
                                                    <th>Almacen</th>
                                                    <th>Unidades disponibles</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tablaproductos_dev">
                                                    @foreach($devoluciones as $producto)
                                                    @php 
                                                    $stock = $producto->stock + $producto->mov_destino + $producto->mov_origen + $producto->venta + $producto->devolucion_venta + $producto->compra + $producto->devolucion_compra;
                                                    @endphp
                                                    <tr id="fila{{$producto->id_pedido_producto}}">
                                                        <input name="items[{{$producto->id_pedido_producto}}][id]" value="{{$producto->id_pedido_producto}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][id]">
                                                        <input name="items[{{$producto->id_pedido_producto}}][id_producto]" value="{{$producto->id_producto}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][id_producto]">
                                                        <input name="items[{{$producto->id_pedido_producto}}][costo_unitario]" value="{{$producto->costo_unitario}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][costo_unitario]">
                                                        <input name="items[{{$producto->id_pedido_producto}}][cantidad_pedido]" value="{{$producto->cantidad}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][cantidad_pedido]">
                                                        <input name="items[{{$producto->id_pedido_producto}}][tipo_item]" value="{{$producto->tipo_item}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][tipo_item]">
                                                        <input name="items[{{$producto->id_pedido_producto}}][iva]" value="0" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][iva]">
                                                        <td>{{$producto->codigo}}</td>
                                                        <td>{{$producto->nombre}}</td>
                                                        <td><input name="items[{{$producto->id_pedido_producto}}][cantidad]" type="number" max="{{$stock}}" oninvalid="this.setCustomValidity('No hay stock suficiente')" value="{{$producto->cantidad}}" class="form-control" id="items[{{$producto->id_pedido_producto}}][cantidad]"></td>
                                                        <td>
                                                            <select class="form-control" onchange="getStockAlmacenDev(this.value, '{{$producto->id_producto}}')" name="items[{{$producto->id_pedido_producto}}][almacen_id]" id="items[{{$producto->id_pedido_producto}}][almacen_id]">
                                                            @php
                                                            foreach ($almacenes as $almacen) {
                                                                if($almacen->id_almacen == env('ALMACEN_PRINCIPAL', 1)){
                                                                    echo "<option selected value='".$almacen->id_almacen."'>".$almacen->nombre."</option>";
                                                                }else{
                                                                    echo "<option value='".$almacen->id_almacen."'>".$almacen->nombre."</option>";
                                                                }
                                                            }
                                                            @endphp
                                                            </select>
                                                        </td>
                                                        <td id="td_stock_dev_{{$producto->id_producto}}">{{number_format($stock,2,",",".")}}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                                @endif
                                @if(env('PROMOCIONES_VENTA',false) && count($promociones) > 0)
                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="card-title">Productos promoción</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <table class="table table-bordered">
                                                <thead>                  
                                                    <tr>
                                                    <th>Codigo</th>
                                                    <th>Nombre</th>
                                                    <th>Cantidad</th>
                                                    <th>Almacen</th>
                                                    <th>Unidades disponibles</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tablaproductos_prom">
                                                    @foreach($promociones as $producto)
                                                    @php 
                                                    $stock = $producto->stock + $producto->mov_destino + $producto->mov_origen + $producto->venta + $producto->devolucion_venta + $producto->compra + $producto->devolucion_compra;
                                                    @endphp
                                                    <tr id="fila{{$producto->id_pedido_producto}}">
                                                        <input name="items[{{$producto->id_pedido_producto}}][id]" value="{{$producto->id_pedido_producto}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][id]">
                                                        <input name="items[{{$producto->id_pedido_producto}}][id_producto]" value="{{$producto->id_producto}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][id_producto]">
                                                        <input name="items[{{$producto->id_pedido_producto}}][costo_unitario]" value="{{$producto->costo_unitario}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][costo_unitario]">
                                                        <input name="items[{{$producto->id_pedido_producto}}][cantidad_pedido]" value="{{$producto->cantidad}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][cantidad_pedido]">
                                                        <input name="items[{{$producto->id_pedido_producto}}][tipo_item]" value="{{$producto->tipo_item}}" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][tipo_item]">
                                                        <input name="items[{{$producto->id_pedido_producto}}][iva]" value="0" type="hidden" class="form-control" id="items[{{$producto->id_pedido_producto}}][iva]">
                                                        <td>{{$producto->codigo}}</td>
                                                        <td>{{$producto->nombre}}</td>
                                                        <td><input name="items[{{$producto->id_pedido_producto}}][cantidad]" type="number" max="{{$stock}}" oninvalid="this.setCustomValidity('No hay stock suficiente')" value="{{$producto->cantidad}}" class="form-control" id="items[{{$producto->id_pedido_producto}}][cantidad]"></td>
                                                        <td>
                                                            <select class="form-control" onchange="getStockAlmacenProm(this.value, '{{$producto->id_producto}}')" name="items[{{$producto->id_pedido_producto}}][almacen_id]" id="items[{{$producto->id_pedido_producto}}][almacen_id]">
                                                            @php
                                                            foreach ($almacenes as $almacen) {
                                                                if($almacen->id_almacen == env('ALMACEN_PRINCIPAL', 1)){
                                                                    echo "<option selected value='".$almacen->id_almacen."'>".$almacen->nombre."</option>";
                                                                }else{
                                                                    echo "<option value='".$almacen->id_almacen."'>".$almacen->nombre."</option>";
                                                                }
                                                            }
                                                            @endphp
                                                            </select>
                                                        </td>
                                                        <td id="td_stock_prom_{{$producto->id_producto}}">{{number_format($stock,2,",",".")}}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                                @endif
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                        <label for="descripcion">Observaciones</label>
                                        <textarea name="descripcion" rows="5" id="descripcion" class="form-control" maxlength="300">{{str_replace('<br />',"", $pedido->descripcion)}}</textarea>
                                </div>
                                <div class="col-sm-6">
                                        <label for="observaciones_publicas">Observaciones públicas</label>
                                        <textarea name="observaciones_publicas" rows="5" id="observaciones_publicas" class="form-control" maxlength="300">{{str_replace('<br />',"", $pedido->observaciones_publicas)}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success" onclick="aceptar(event);">Generar remito</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.tab-pane -->
    </body>
    <style>
        .table{
            font-size: 13px;
        }
        .form-control{
            font-size: 13px;
        }
        .form-control{
            font-size: 13px;
        }
        .loader {
            border: 3px solid #f3f3f3; /* Light grey */
            border-top: 3px solid #3498db; /* Blue */
            border-radius: 50%;
            width: 25px;
            height: 25px;
            animation: spin 2s linear infinite;
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>
    @stop