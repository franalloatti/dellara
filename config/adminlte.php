<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | Here you can change the default title of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#61-title
    |
    */

    'title' => 'Sistema '.env('APP_NAME', ''),
    'title_prefix' => '',
    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Favicon
    |--------------------------------------------------------------------------
    |
    | Here you can activate the favicon.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#62-favicon
    |
    */

    'use_ico_only' => false,
    'use_full_favicon' => false,

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | Here you can change the logo of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#63-logo
    |
    */

    'logo' => env('APP_LOGO_FULL', null),
    'logo_img' => env('APP_LOGO', null),
    'logo_img_class' => 'brand-image img-circle elevation-3',
    'logo_img_xl' => null,
    'logo_img_xl_class' => 'brand-image-xs',
    'logo_img_alt' => env('APP_NAME', 'Laravel'),

    /*
    |--------------------------------------------------------------------------
    | User Menu
    |--------------------------------------------------------------------------
    |
    | Here you can activate and change the user menu.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#64-user-menu
    |
    */

    'usermenu_enabled' => true,
    'usermenu_header' => false,
    'usermenu_header_class' => 'bg-primary',
    'usermenu_image' => false,
    'usermenu_desc' => false,
    'usermenu_profile_url' => false,

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Here we change the layout of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#65-layout
    |
    */

    'layout_topnav' => null,
    'layout_boxed' => null,
    'layout_fixed_sidebar' => null,
    'layout_fixed_navbar' => null,
    'layout_fixed_footer' => null,

    /*
    |--------------------------------------------------------------------------
    | Authentication Views Classes
    |--------------------------------------------------------------------------
    |
    | Here you can change the look and behavior of the authentication views.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#661-authentication-views-classes
    |
    */

    'classes_auth_card' => 'card-outline card-primary',
    'classes_auth_header' => '',
    'classes_auth_body' => '',
    'classes_auth_footer' => '',
    'classes_auth_icon' => '',
    'classes_auth_btn' => 'btn-flat btn-primary',

    /*
    |--------------------------------------------------------------------------
    | Admin Panel Classes
    |--------------------------------------------------------------------------
    |
    | Here you can change the look and behavior of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#662-admin-panel-classes
    |
    */

    'classes_body' => '',
    'classes_brand' => '',
    'classes_brand_text' => '',
    'classes_content_wrapper' => '',
    'classes_content_header' => '',
    'classes_content' => '',
    'classes_sidebar' => 'sidebar-dark-primary elevation-4',
    'classes_sidebar_nav' => '',
    'classes_topnav' => 'navbar-white navbar-light',
    'classes_topnav_nav' => 'navbar-expand',
    'classes_topnav_container' => 'container',

    /*
    |--------------------------------------------------------------------------
    | Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#67-sidebar
    |
    */

    'sidebar_mini' => true,
    'sidebar_collapse' => true,
    'sidebar_collapse_auto_size' => false,
    'sidebar_collapse_remember' => true,
    'sidebar_collapse_remember_no_transition' => true,
    'sidebar_scrollbar_theme' => 'os-theme-light',
    'sidebar_scrollbar_auto_hide' => 'l',
    'sidebar_nav_accordion' => true,
    'sidebar_nav_animation_speed' => 300,

    /*
    |--------------------------------------------------------------------------
    | Control Sidebar (Right Sidebar)
    |--------------------------------------------------------------------------
    |
    | Here we can modify the right sidebar aka control sidebar of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#68-control-sidebar-right-sidebar
    |
    */

    'right_sidebar' => false,
    'right_sidebar_icon' => 'fas fa-cogs',
    'right_sidebar_theme' => 'dark',
    'right_sidebar_slide' => true,
    'right_sidebar_push' => true,
    'right_sidebar_scrollbar_theme' => 'os-theme-light',
    'right_sidebar_scrollbar_auto_hide' => 'l',

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Here we can modify the url settings of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#69-urls
    |
    */

    'use_route_url' => false,

    'dashboard_url' => '/',

    'logout_url' => 'logout',

    'login_url' => 'login',

    'register_url' => 'register',

    'password_reset_url' => 'password/reset',

    'password_email_url' => 'password/email',

    'profile_url' => false,

    /*
    |--------------------------------------------------------------------------
    | Laravel Mix
    |--------------------------------------------------------------------------
    |
    | Here we can enable the Laravel Mix option for the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#610-laravel-mix
    |
    */

    'enabled_laravel_mix' => false,
    'laravel_mix_css_path' => 'css/app.css',
    'laravel_mix_js_path' => 'js/app.js',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar/top navigation of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#611-menu
    |
    */

    'menu' => [
        [
            'text'   => 'Actualizar USD',
            'url'    => 'admin/proveedores/actualizar_dol',
            'can'    => 'DOL-ADM-Proveedores-editar',
            'topnav' => true,
        ],
        [
            'text'   => 'Ver stock',
            'url'    => 'admin/productos/verstock',
            'can'    => 'STOCK-ADM-Productos-ver',
            'topnav' => true,
        ],
        [
            'text'   => 'Precio producto',
            'url'    => 'admin/productos/precios',
            'can'    => 'PRODUCTO-ADM-Productos-editar',
            'topnav' => true,
        ],
        [
            'text' => 'Descargar catálogo',
            'icon' => 'fas fa-arrow-circle-down',
            'url'  => 'descargarcatalogo',
            'can'  => 'Catalogo-ver',
        ],
        [
            'text' => 'Descargar catálogo',
            'icon' => 'fas fa-arrow-circle-down',
            'url'  => 'descargarcatalogo',
            'can'  => 'ADM-Catalogo-ver',
        ],
        [
            'text'    => 'Finanzas',
            'icon'    => 'fas fa-balance-scale',
            'can'     => 'ADM-Medios de pagos-alguno',
            'submenu' => [
                [
                    'icon' => 'fas fa-fw fa-list',
                    'text' => 'Cuentas',
                    'url'  => 'admin/medios/index',
                    'can'  => 'ADM-Medios de pagos-ver',
                ],
                [
                    'icon' => 'fas fa-fw fa-list',
                    'text' => 'Gastos periódicos',
                    'url'  => 'admin/gastos/periodicos',
                    'can'  => 'ADM-Pagos-ver',
                ],
                [
                    'icon' => 'fas fa-fw fa-list',
                    'text' => 'Ver movimientos',
                    'url'  => 'admin/movimientos/index',
                    'can'  => 'ADM-Pagos-ver',
                ],
                [
                    'icon' => 'fas fa-fw fa-file-excel',
                    'text' => 'Reportes movimientos',
                    'url'  => 'admin/movimientos/reportes',
                    'can'  => 'ADM-Pagos-ver',
                ],
                [
                    'icon' => 'fas fa-fw fa-list',
                    'text' => 'Pagos',
                    'url'  => 'admin/compras/ordenespagos',
                    'can'  => 'ADM-Ordenes de pago-ver',
                ],
            ],
        ],
        [
            'text'    => 'Productos',
            'icon'    => 'fas fa-barcode',
            'can'     => 'ADM-Productos-alguno',
            'submenu' => [
                [
                    'icon' => 'fas fa-fw fa-list',
                    'text' => 'Listar',
                    'url'  => 'admin/productos/index',
                    'can'  => 'ADM-Productos-ver',
                ],
                [
                    'icon' => 'fas fa-fw fa-plus',
                    'text' => 'Nuevo',
                    'url'  => 'admin/productos/crear',
                    'can'  => 'ADM-Productos-crear',
                ],
                [
                    'icon' => 'fas fa-fw fa-file-excel',
                    'text' => 'Importar',
                    'url'  => 'admin/productos/importar',
                    'can'  => 'ADM-Productos-crear',
                ],
                // [
                //     'icon' => 'fas fa-fw fa-file-excel',
                //     'text' => 'Importar imagenes',
                //     'url'  => 'admin/productos/importarimagenes',
                //     'can'  => 'ADM-Productos-editar',
                // ],
            ],
        ],
        [
            'text'    => 'Insumos',
            'icon'    => 'fa fa-info-circle',
            'can'     => 'ADM-Insumos-alguno',
            'submenu' => [
                [
                    'icon' => 'fas fa-fw fa-list',
                    'text' => 'Listar',
                    'url'  => 'admin/insumos/index',
                    'can'  => 'ADM-Insumos-ver',
                ],
                [
                    'icon' => 'fas fa-fw fa-plus',
                    'text' => 'Nuevo',
                    'url'  => 'admin/insumos/crear',
                    'can'  => 'ADM-Insumos-crear',
                ],
            ],
        ],
        [
            'text'    => 'Servicios',
            'icon'    => 'fa fa-arrow-circle-right',
            'can'     => 'ADM-Servicios-alguno',
            'submenu' => [
                [
                    'icon' => 'fas fa-fw fa-list',
                    'text' => 'Listar',
                    'url'  => 'admin/servicios/index',
                    'can'  => 'ADM-Servicios-ver',
                ],
                [
                    'icon' => 'fas fa-fw fa-plus',
                    'text' => 'Nuevo',
                    'url'  => 'admin/servicios/crear',
                    'can'  => 'ADM-Servicios-crear',
                ],
            ],
        ],
        [
            'text' => 'Catálogo',
            'icon' => 'fas fa-fw fa-book',
            'url'  => 'catalogo',
            'can'  => 'Catalogo-ver',
        ],
        [
            'text' => 'Catálogo',
            'icon' => 'fas fa-fw fa-book',
            'url'  => 'catalogo',
            'can'  => 'ADM-Catalogo-ver',
        ],
        [
            'text' => 'Mis pedidos',
            'icon' => 'fas fa-fw fa-list',
            'url'  => 'mispedidos',
            'can'  => 'Pedidos-ver',
        ],
        [
            'text' => 'Estado de cuenta',
            'icon' => 'fas fa-balance-scale',
            'url'  => 'cuenta',
            'can'  => 'Estados de cuentas-ver',
        ],
        [
            'text'    => 'Pagos',
            'icon'    => 'fas fa-fw fa-hand-paper',
            'can'     => 'Pagos-alguno',
            'submenu' => [
                [
                    'text' => 'Nuevo',
                    'icon' => 'fas fa-fw fa-plus',
                    'url'  => 'nuevopago',
                    'can'  => 'Pagos-crear',
                ],
                [
                    'text' => 'Mis pagos',
                    'icon' => 'fas fa-fw fa-file-powerpoint',
                    'url'  => 'mispagos',
                    'can'  => 'Pagos-ver',
                ],
            ],
        ],
        [
            'text'    => 'Devoluciones',
            'icon'    => 'fas fa-fw fa-hand-paper',
            'can'     => 'Devoluciones-alguno',
            'submenu' => [
                [
                    'icon' => 'fas fa-fw fa-plus',
                    'text' => 'Nueva',
                    'url'  => 'devoluciones/crear',
                    'can'  => 'Devoluciones-crear',
                ],
                [
                    'icon' => 'fas fa-fw fa-list',
                    'text' => 'Mis devoluciones',
                    'url'  => 'misdevoluciones',
                    'can'  => 'Devoluciones-ver',
                ],
            ],
        ],
        [
            'text' => 'Ventas en preparación',
            'icon' => 'fas fa-fw fa-list',
            'url'  => 'admin/pedidos/enpreparacion',
            'can'  => 'ADM-Pedidos-ver',
        ],
        [
            'text'    => 'Ventas',
            'icon'    => 'fas fa-cart-arrow-down',
            'can'     => 'ADM-Pedidos-alguno',
            'submenu' => [
                [
                    'icon' => 'fas fa-fw fa-plus',
                    'text' => 'Nueva',
                    'url'  => 'admin/pedidos/crear',
                    'can'  => 'ADM-Pedidos-crear',
                ],
                [
                    'text' => 'Listar todas',
                    'icon' => 'fas fa-fw fa-list',
                    'url'  => 'admin/pedidos/index',
                    'can'  => 'ADM-Pedidos-ver',
                ],
                [
                    'text' => 'Remitos ventas',
                    'icon' => 'fas fa-list-alt',
                    'url'  => 'admin/remitos/index',
                    'can'  => 'ADM-Remitos-ver',
                ],
                [
                    'icon' => 'fas fa-fw fa-file-excel',
                    'text' => 'Reportes',
                    'url'  => 'admin/pedidos/reportes',
                    'can'  => 'ADM-Pedidos-ver',
                ],
            ],
        ],
        // [
        //     'text' => 'En preparación',
        //     'icon' => 'fas fa-fw fa-list',
        //     'url'  => 'admin/pedidos/enpreparacion',
        //     'can'  => 'ADM-Pedidos-ver',
        // ],
        // [
        //     'text' => 'Listar Pedidos',
        //     'icon' => 'fas fa-fw fa-list',
        //     'url'  => 'admin/pedidos/index',
        //     'can'  => 'ADM-Pedidos-ver',
        // ],
        // [
        //     'text' => 'Nuevo Pedido',
        //     'icon' => 'fas fa-fw fa-plus',
        //     'url'  => 'admin/pedidos/crear',
        //     'can'  => 'ADM-Pedidos-crear',
        // ],
        // [
        //     'text' => 'Remitos',
        //     'icon' => 'fas fa-list-alt',
        //     'url'  => 'admin/remitos/index',
        //     'can'  => 'ADM-Remitos-ver',
        // ],
        [
            'text'    => 'Facturas',
            'icon'    => 'fas fa-file-invoice',
            'url'  => 'admin/factura',
            'can'  => 'ADM-Facturas-alguno',
        ],
        [
            'text'    => 'Devoluciones',
            'icon'    => 'fas fa-fw fa-hand-paper',
            'can'     => 'ADM-Devoluciones-alguno',
            'submenu' => [
                [
                    'icon' => 'fas fa-fw fa-plus',
                    'text' => 'Nueva',
                    'url'  => 'admin/devoluciones/crear',
                    'can'  => 'ADM-Devoluciones-crear',
                ],
                [
                    'icon' => 'fas fa-fw fa-list',
                    'text' => 'Pendientes',
                    'url'  => 'admin/devoluciones/pendientes',
                    'can'  => 'ADM-Devoluciones-ver',
                ],
                [
                    'icon' => 'fas fa-fw fa-list',
                    'text' => 'Todas',
                    'url'  => 'admin/devoluciones/todas',
                    'can'  => 'ADM-Devoluciones-ver',
                ],
            ],
        ],
        [
            'text'    => 'Clientes',
            'icon'    => 'fas fa-fw fa-user',
            'can'     => 'ADM-Clientes-alguno',
            'submenu' => [
                [
                    'icon' => 'fas fa-fw fa-list',
                    'text' => 'Mayoristas',
                    'url'  => 'admin/clientes/index',
                    'can'  => 'ADM-Clientes-ver',
                ],
                [
                    'icon' => 'fas fa-fw fa-list',
                    'text' => 'Consumidores finales',
                    'url'  => 'admin/clientes/indexconsumidor',
                    'can'  => 'ADM-Clientes-ver',
                ],
                [
                    'icon' => 'fas fa-fw fa-plus',
                    'text' => 'Nuevo',
                    'url'  => 'admin/clientes/crear',
                    'can'  => 'ADM-Clientes-crear',
                ],
                [
                    'icon'    => 'fas fa-fw fa-plus',
                    'text' => 'Cargar nuevo cobro',
                    'url'  => 'admin/pagos/crear',
                    'can'  => 'ADM-Pagos-crear',
                ],
                [
                    'icon'    => 'fas fa-fw fa-list',
                    'text' => 'Listar cobros',
                    'url'  => 'admin/pagos/index',
                    'can'  => 'ADM-Pagos-ver',
                ],
            ],
        ],
        // [
        //     'text'    => 'Pagos',
        //     'icon'    => 'fas fa-fw fa-file-powerpoint',
        //     'can'     => 'ADM-Pagos-alguno',
        //     'submenu' => [
                
        //     ],
        // ],
        [
            'text'    => 'Tipos de clientes',
            'icon'    => 'fas fa-fw fa-sitemap',
            'can'     => 'ADM-Tipos de clientes-alguno',
            'submenu' => [
                [
                    'icon' => 'fas fa-fw fa-plus',
                    'text' => 'Nuevo',
                    'url'  => 'admin/roles/crear',
                    'can'  => 'ADM-Tipos de clientes-crear',
                ],
                [
                    'icon' => 'fas fa-fw fa-list',
                    'text' => 'Listar',
                    'url'  => 'admin/roles/index',
                    'can'  => 'ADM-Tipos de clientes-ver',
                ],
            ],
        ],
        [
            'text'    => 'Compras',
            'icon'    => 'fas fa-cart-plus',
            'can'     => 'ADM-Compras-alguno',
            'submenu' => [
                [
                    'icon' => 'fas fa-fw fa-plus',
                    'text' => 'Nueva',
                    'url'  => 'admin/compras/crear',
                    'can'  => 'ADM-Compras-crear',
                ],
                [
                    'icon' => 'fas fa-fw fa-list',
                    'text' => 'Pendientes',
                    'url'  => 'admin/compras/pendientes',
                    'can'  => 'ADM-Compras-ver',
                ],
                [
                    'icon' => 'fas fa-fw fa-list',
                    'text' => 'Todas',
                    'url'  => 'admin/compras/todas',
                    'can'  => 'ADM-Compras-ver',
                ],
                [
                    'icon' => 'fas fa-fw fa-hand-paper',
                    'text' => 'Devoluciones compras',
                    'url'  => 'admin/compras/devoluciones',
                    'can'  => 'ADM-Devoluciones compras-crear',
                ],
                [
                    'icon' => 'fas fa-fw fa-file-excel',
                    'text' => 'Reportes',
                    'url'  => 'admin/compras/reportes',
                    'can'  => 'ADM-Compras-ver',
                ],
            ],
        ],
        [
            'text'    => 'Proveedores',
            'icon'    => 'fas fa-fw fa-user',
            'can'     => 'ADM-Proveedores-alguno',
            'submenu' => [
                [
                    'icon' => 'fas fa-fw fa-list',
                    'text' => 'Listar',
                    'url'  => 'admin/proveedores/index',
                    'can'  => 'ADM-Proveedores-ver',
                ],
                [
                    'icon' => 'fas fa-fw fa-plus',
                    'text' => 'Nuevo',
                    'url'  => 'admin/proveedores/crear',
                    'can'  => 'ADM-Proveedores-crear',
                ],
                [
                    'icon'    => 'fas fa-fw fa-list',
                    'text' => 'Listar pagos',
                    'url'  => 'admin/proveedores/pagos',
                    'can'  => 'ADM-Pagos-ver',
                ],
                [
                    'icon'    => 'fas fa-fw fa-plus',
                    'text' => 'Cargar nuevo pago',
                    'url'  => 'admin/proveedores/addpago',
                    'can'  => 'ADM-Pagos-crear',
                ],
            ],
        ],
        [
            'text'    => 'Planes de producción',
            'icon'    => 'fas fa-fw fa-cubes',
            'can'     => 'ADM-Produccion-alguno',
            'submenu' => [
                [
                    'icon' => 'fas fa-fw fa-list',
                    'text' => 'Listar',
                    'url'  => 'admin/produccion/index',
                    'can'  => 'ADM-Produccion-ver',
                ],
                [
                    'icon' => 'fas fa-fw fa-plus',
                    'text' => 'Nuevo',
                    'url'  => 'admin/produccion/crear',
                    'can'  => 'ADM-Produccion-crear',
                ],
            ],
        ],
        [
            'text'    => 'Planes de producción',
            'icon'    => 'fas fa-fw fa-cubes',
            'can'     => 'Produccion-alguno',
            'submenu' => [
                [
                    'icon' => 'fas fa-fw fa-list',
                    'text' => 'Listar',
                    'url'  => 'admin/produccion/index',
                    'can'  => 'Produccion-ver',
                ],
                [
                    'icon' => 'fas fa-fw fa-plus',
                    'text' => 'Nuevo',
                    'url'  => 'admin/produccion/crear',
                    'can'  => 'Produccion-crear',
                ],
            ],
        ],
        [
            'text'    => 'Perfiles',
            'icon'    => 'fas fa-user-secret',
            'can'     => 'ADM-Perfiles-alguno',
            'submenu' => [
                [
                    'icon' => 'fas fa-fw fa-list',
                    'text' => 'Listar',
                    'url'  => 'admin/perfiles/index',
                    'can'  => 'ADM-Perfiles-ver',
                ],
                [
                    'icon' => 'fas fa-fw fa-plus',
                    'text' => 'Nuevo',
                    'url'  => 'admin/perfiles/crear',
                    'can'  => 'ADM-Perfiles-crear',
                ],
            ],
        ],
        [
            'text'    => 'Usuarios',
            'icon'    => 'fas fa-fw fa-users',
            'can'     => 'ADM-Usuarios-alguno',
            'submenu' => [
                [
                    'icon' => 'fas fa-fw fa-list',
                    'text' => 'Listar',
                    'url'  => 'admin/usuarios/index',
                    'can'  => 'ADM-Usuarios-ver',
                ],
                [
                    'icon'    => 'fas fa-fw fa-plus',
                    'text' => 'Nuevo',
                    'url'  => 'admin/usuarios/crear',
                    'can'  => 'ADM-Usuarios-crear',
                ],
            ],
        ],
        [
            'text'    => 'Almacenes',
            'icon'    => 'fas fa-fw fa-truck',
            'can'     => 'ADM-Almacenes-alguno',
            'submenu' => [
                [
                    'icon'    => 'fas fa-fw fa-list',
                    'text' => 'Listar',
                    'url'  => 'admin/almacenes/index',
                    'can'  => 'ADM-Almacenes-ver',
                ],
                [
                    'icon'    => 'fas fa-fw fa-plus',
                    'text' => 'Nuevo',
                    'url'  => 'admin/almacenes/crear',
                    'can'  => 'ADM-Almacenes-crear',
                ],
            ],
        ],
        [
            'text'    => 'Marcas',
            'icon'    => 'fas fa-fw fa-bookmark',
            'can'     => 'ADM-Marcas-alguno',
            'submenu' => [
                [
                    'icon'    => 'fas fa-fw fa-list',
                    'text' => 'Listar',
                    'url'  => 'admin/marcas/index',
                    'can'  => 'ADM-Marcas-ver',
                ],
                [
                    'icon'    => 'fas fa-fw fa-plus',
                    'text' => 'Nuevo',
                    'url'  => 'admin/marcas/crear',
                    'can'  => 'ADM-Marcas-crear',
                ],
            ],
        ],
        [
            'text'    => 'Rubros',
            'icon'    => 'fas fa-fw fa-share-alt',
            'can'     => 'ADM-Rubros-alguno',
            'submenu' => [
                [
                    'icon'    => 'fas fa-fw fa-list',
                    'text' => 'Listar',
                    'url'  => 'admin/rubros/index',
                    'can'  => 'ADM-Rubros-ver',
                ],
                [
                    'icon'    => 'fas fa-fw fa-plus',
                    'text' => 'Nuevo',
                    'url'  => 'admin/rubros/crear',
                    'can'  => 'ADM-Rubros-crear',
                ],
            ],
        ],
        // [
        //     'text'    => 'Medios de pago',
        //     'icon'    => 'fas fa-fw fa-credit-card',
        //     'can'     => 'ADM-Medios de pagos-alguno',
        //     'submenu' => [
        //         [
        //             'icon'    => 'fas fa-fw fa-list',
        //             'text' => 'Listar',
        //             'url'  => 'admin/medios/index',
        //             'can'  => 'ADM-Medios de pagos-ver',
        //         ],
        //         [
        //             'icon'    => 'fas fa-fw fa-plus',
        //             'text' => 'Nuevo',
        //             'url'  => 'admin/medios/crear',
        //             'can'  => 'ADM-Medios de pagos-crear',
        //         ],
        //     ],
        // ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Here we can modify the menu filters of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#612-menu-filters
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SearchFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\LangFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\DataFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Here we can modify the plugins used inside the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#613-plugins
    |
    */

    'plugins' => [
        'Datatables' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css',
                ],
            ],
        ],
        'Select2' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css',
                ],
            ],
        ],
        'Chartjs' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js',
                ],
            ],
        ],
        'Sweetalert2' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.jsdelivr.net/npm/sweetalert2@8',
                ],
            ],
        ],
        'Pace' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/blue/pace-theme-center-radar.min.css',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js',
                ],
            ],
        ],
    ],
];
