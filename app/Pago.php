<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    protected $primaryKey = 'id_pago';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'monto', 'user_id_user', 'medio_id_medio', 'descripcion', 'estado', 'deleted', 
        'tipo_movimiento_id', 'consolidado', 'nombre', 'fecha', 'conversion_dolar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    public function user() {
        return $this
        ->belongsTo(User::class)
        ->withTimestamps();
    }

    public function remitos() {
        return $this
        ->belongsToMany(Remito::class)
        ->withTimestamps();
    }
    
    public function medio() {
        return $this
        ->belongsTo(Medio::class)
        ->withTimestamps();
    }
}
