<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemFactura extends Model
{
    protected $table = 'factura_item';
    protected $primaryKey = 'id_item_factura';
    protected $fillable = [
        'id_item_factura', 'factura_id', 'Id', 'BaseImp', 'Importe'
    ];
}
