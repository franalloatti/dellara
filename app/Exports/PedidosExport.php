<?php

namespace App\Exports;

use App\Pedido;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class PedidosExport implements WithColumnFormatting, FromView, WithEvents
{

    protected $tipo;
    protected $fecha_desde;
    protected $fecha_hasta;
    
     function __construct($tipo, $fecha_desde, $fecha_hasta) {
            $this->tipo = $tipo;
            $this->fecha_desde = $fecha_desde;
            $this->fecha_hasta = $fecha_hasta;
     }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $remitos = DB::table('remitos')->where('pedidos.tipo', '=', $this->tipo)
        ->whereRaw('(`estado_pedido`.`estado_id_estado` < 4 or `estado_pedido`.`estado_id_estado` is null)')
        ->whereRaw("cast(remitos.created_at as date) >= cast('".$this->fecha_desde."' as date)")
        ->whereRaw("cast(remitos.created_at as date) <= cast('".$this->fecha_hasta."' as date)")
        ->join('producto_remito', 'remitos.id_remito', '=', 'producto_remito.remito_id_remito')
        ->join('pedido_producto', 'pedido_producto.id_pedido_producto', '=', 'producto_remito.producto_id_producto')
        ->join('productos', 'pedido_producto.producto_id_producto', '=', 'productos.id_producto')
        ->join('pedidos', 'pedidos.id_pedido', '=', 'remitos.pedido_id_pedido')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin(DB::raw("(SELECT remito_id_remito, MAX(created_at) as fecha_cobro FROM pago_remito PR
        group by remito_id_remito) as TEMP"), 'remitos.id_remito', '=', 'TEMP.remito_id_remito')
        ->join('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->join('direccions', 'direccions.id_direccion', '=', 'pedidos.direccion_id_direccion')
        ->join('perfils', 'users.perfil_id', '=', 'perfils.id_perfil')
        ->leftJoin('pago_remito', 'pago_remito.remito_id_remito', '=', 'remitos.id_remito')
        ->select('producto_remito.*', 'pedidos.tipo', 'productos.codigo', 'productos.nombre', 'remitos.id_remito', 'remitos.created_at as fecha_remito', 
        'pedidos.flete', 'pedidos.nro_factura', 'users.surname', 'users.id_user', 'direccions.direccion',
        'direccions.id_direccion', 'TEMP.fecha_cobro')
        ->selectRaw("IF(perfils.nombre = 'Mayorista', users.name, concat(users.name, ' ', users.surname)) as name")
        ->orderBy('remitos.created_at', 'desc')
        ->get();
        // echo json_encode($remitos);die;
        if($this->tipo == 'Compra'){
            return view('compras.reporte', [
                'pedidos' => $remitos,
                'fecha_desde' => $this->fecha_desde,
                'fecha_hasta' => $this->fecha_hasta
            ]);
        }else{
            return view('pedidos.reporte', [
                'pedidos' => $remitos,
                'fecha_desde' => $this->fecha_desde,
                'fecha_hasta' => $this->fecha_hasta
            ]);
        }
    }
    public function collection()
    {
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                
                if($this->tipo == 'Compra'){
                    // $event->sheet->getStyle('A1')->getFont()->setSize(20)->setBold(true);
                    $event->sheet->getStyle('A1')->getFont()->setBold(true);
                    $event->sheet->getStyle('B1')->getFont()->setBold(true);
                    $event->sheet->getStyle('C1')->getFont()->setBold(true);
                    $event->sheet->getStyle('D1')->getFont()->setBold(true);
                    $event->sheet->getStyle('E1')->getFont()->setBold(true);
                    $event->sheet->getStyle('F1')->getFont()->setBold(true);
                    $event->sheet->getStyle('G1')->getFont()->setBold(true);
                    $event->sheet->getStyle('H1')->getFont()->setBold(true);
                    $event->sheet->getStyle('I1')->getFont()->setBold(true);
                    $event->sheet->getStyle('J1')->getFont()->setBold(true);
                    $event->sheet->getStyle('K1')->getFont()->setBold(true);
                    $event->sheet->getStyle('L1')->getFont()->setBold(true);
                    $event->sheet->getColumnDimension('A')->setWidth(18);
                    $event->sheet->getColumnDimension('B')->setWidth(30);
                    $event->sheet->getColumnDimension('C')->setWidth(30);
                    $event->sheet->getColumnDimension('D')->setWidth(20);
                    $event->sheet->getColumnDimension('E')->setWidth(50);
                    $event->sheet->getColumnDimension('F')->setWidth(13);
                    $event->sheet->getColumnDimension('G')->setWidth(13);
                    $event->sheet->getColumnDimension('H')->setWidth(10);
                    $event->sheet->getColumnDimension('I')->setWidth(12);
                    $event->sheet->getColumnDimension('J')->setWidth(18);
                    $event->sheet->getColumnDimension('K')->setWidth(18);
                    $event->sheet->getColumnDimension('L')->setWidth(16);
                }else{
                    // $event->sheet->getStyle('A1')->getFont()->setSize(20)->setBold(true);
                    $event->sheet->getStyle('A1')->getFont()->setBold(true);
                    $event->sheet->getStyle('B1')->getFont()->setBold(true);
                    $event->sheet->getStyle('C1')->getFont()->setBold(true);
                    $event->sheet->getStyle('D1')->getFont()->setBold(true);
                    $event->sheet->getStyle('E1')->getFont()->setBold(true);
                    $event->sheet->getStyle('F1')->getFont()->setBold(true);
                    $event->sheet->getStyle('G1')->getFont()->setBold(true);
                    $event->sheet->getStyle('H1')->getFont()->setBold(true);
                    $event->sheet->getStyle('I1')->getFont()->setBold(true);
                    $event->sheet->getStyle('J1')->getFont()->setBold(true);
                    $event->sheet->getStyle('K1')->getFont()->setBold(true);
                    $event->sheet->getStyle('L1')->getFont()->setBold(true);
                    $event->sheet->getStyle('M1')->getFont()->setBold(true);
                    $event->sheet->getStyle('N1')->getFont()->setBold(true);
                    $event->sheet->getStyle('O1')->getFont()->setBold(true);
                    $event->sheet->getColumnDimension('A')->setWidth(18);
                    $event->sheet->getColumnDimension('B')->setWidth(30);
                    $event->sheet->getColumnDimension('C')->setWidth(30);
                    $event->sheet->getColumnDimension('D')->setWidth(30);
                    $event->sheet->getColumnDimension('E')->setWidth(10);
                    $event->sheet->getColumnDimension('F')->setWidth(10);
                    $event->sheet->getColumnDimension('G')->setWidth(40);
                    $event->sheet->getColumnDimension('H')->setWidth(10);
                    $event->sheet->getColumnDimension('I')->setWidth(12);
                    $event->sheet->getColumnDimension('J')->setWidth(10);
                    $event->sheet->getColumnDimension('K')->setWidth(13);
                    $event->sheet->getColumnDimension('L')->setWidth(13);
                    $event->sheet->getColumnDimension('M')->setWidth(12);
                    $event->sheet->getColumnDimension('N')->setWidth(15);
                    $event->sheet->getColumnDimension('O')->setWidth(14);
                }
            }
        ];
    }
    public function columnFormats(): array
    {
        if($this->tipo == 'Compra'){
            return [
                'A' => NumberFormat::FORMAT_DATE_DDMMYYYY,
                'L' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            ];
        }else{
            return [
                'A' => NumberFormat::FORMAT_DATE_DDMMYYYY,
                'O' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            ];
        }
    }
}
