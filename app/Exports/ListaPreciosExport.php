<?php

namespace App\Exports;

use Auth;
use App\Producto;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class ListaPreciosExport implements FromView, WithEvents
{
    public function view(): View
    {
        $productos = DB::table('productos')->where('estado', 1);
        // ->leftJoin('producto_user', 'producto_user.producto_id_producto', '=', 'productos.id_producto')
        $productos = $productos->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('marca_producto', 'marca_producto.producto_id_producto', '=', 'productos.id_producto')
        ->leftJoin('marcas', 'marcas.id_marca', '=', 'marca_producto.marca_id_marca')
        ->leftJoin('producto_rubro', 'producto_rubro.producto_id_producto', '=', 'productos.id_producto')
        ->leftJoin('rubros', 'rubros.id_rubro', '=', 'producto_rubro.rubro_id_rubro')
        ->leftJoin('users', 'users.id_user', '=', 'producto_user.user_id_user')
        ->select('productos.*', 'marcas.nombre as marca', 'rubros.nombre as rubro', 'producto_user.costo_dolares as costo', 'users.valor_dolar')
        ->groupBy('productos.id_producto')->orderBy('marcas.nombre')->orderBy('productos.codigo')->get();
        $midescuento = 0;
        if(Auth::user()){
            $midescuento = DB::table('roles')->where('role_user.user_id_user', '=', Auth::user()->id_user)
            ->leftJoin('role_user', 'role_user.role_id_role', '=', 'roles.id_role')
            ->select('roles.discount')->get()->first()->discount;
        }
        return view('productos.listapreciospdf', [
            'productos' => $productos,
            'midescuento' => $midescuento
        ]);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getStyle('A1')->getFont()->setSize(20)->setBold(true);
                $event->sheet->getStyle('A2')->getFont()->setBold(true);
                $event->sheet->getStyle('B2')->getFont()->setBold(true);
                $event->sheet->getStyle('C2')->getFont()->setBold(true);
                $event->sheet->getStyle('D2')->getFont()->setBold(true);
                $event->sheet->getStyle('E2')->getFont()->setBold(true);
                $event->sheet->getColumnDimension('A')->setWidth(32);
                $event->sheet->getColumnDimension('B')->setWidth(32);
                $event->sheet->getColumnDimension('C')->setWidth(32);
                $event->sheet->getColumnDimension('D')->setWidth(80);
                $event->sheet->getColumnDimension('E')->setWidth(32);
            }
        ];
    }
}
