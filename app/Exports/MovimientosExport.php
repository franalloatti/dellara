<?php

namespace App\Exports;

use App\Pago;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class MovimientosExport implements WithColumnFormatting, FromView, WithEvents
{

    protected $fecha_desde;
    protected $fecha_hasta;
    
     function __construct($fecha_desde, $fecha_hasta) {
            $this->fecha_desde = $fecha_desde;
            $this->fecha_hasta = $fecha_hasta;
     }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $pagos = DB::table('pagos')->where('pagos.deleted', 0)
        ->whereRaw("cast(pagos.fecha as date) >= cast('".$this->fecha_desde."' as date)")
        ->whereRaw("cast(pagos.fecha as date) <= cast('".$this->fecha_hasta."' as date)")
        // ->whereRaw("tipo_movimientos.nombre <> 'Venta'")->whereRaw("tipo_movimientos.nombre <> 'Compra productos'")
        ->leftJoin('tipo_movimientos', 'tipo_movimientos.id_tipo_movimiento', '=', 'pagos.tipo_movimiento_id')
        ->leftJoin('medios', 'medios.id_medio', '=', 'pagos.medio_id_medio')->where('medios.deleted', 0)
        ->leftJoin('users', 'pagos.user_id_user', '=', 'users.id_user')
        ->leftJoin('perfils', 'perfils.id_perfil', '=', 'users.perfil_id')
        ->select('pagos.*', 'tipo_movimientos.nombre as tipo', 'tipo_movimientos.nombre as tipo_movimiento')
        ->selectRaw("medios.nombre as cuenta, IF(perfils.nombre = 'Mayorista', users.name, concat(users.name, ' ', users.surname)) as name")
        ->orderBy('pagos.created_at', 'desc')
        ->get();
        // echo json_encode($pagos);die;
        return view('movimientos.reporte', [
            'movimientos' => $pagos,
            'fecha_desde' => $this->fecha_desde,
            'fecha_hasta' => $this->fecha_hasta
        ]);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getStyle('A1')->getFont()->setBold(true);
                $event->sheet->getStyle('B1')->getFont()->setBold(true);
                $event->sheet->getStyle('C1')->getFont()->setBold(true);
                $event->sheet->getStyle('D1')->getFont()->setBold(true);
                $event->sheet->getStyle('E1')->getFont()->setBold(true);
                $event->sheet->getStyle('F1')->getFont()->setBold(true);
                // $event->sheet->getStyle('G1')->getFont()->setBold(true);
                // $event->sheet->getStyle('H1')->getFont()->setBold(true);
                // $event->sheet->getStyle('I1')->getFont()->setBold(true);
                // $event->sheet->getStyle('J1')->getFont()->setBold(true);
                // $event->sheet->getStyle('K1')->getFont()->setBold(true);
                // $event->sheet->getStyle('L1')->getFont()->setBold(true);
                // $event->sheet->getStyle('M1')->getFont()->setBold(true);
                // $event->sheet->getStyle('N1')->getFont()->setBold(true);
                // $event->sheet->getStyle('O1')->getFont()->setBold(true);
                $event->sheet->getColumnDimension('A')->setWidth(18);
                $event->sheet->getColumnDimension('B')->setWidth(30);
                $event->sheet->getColumnDimension('C')->setWidth(30);
                $event->sheet->getColumnDimension('D')->setWidth(30);
                $event->sheet->getColumnDimension('E')->setWidth(30);
                $event->sheet->getColumnDimension('F')->setWidth(20);
                // $event->sheet->getColumnDimension('G')->setWidth(40);
                // $event->sheet->getColumnDimension('H')->setWidth(10);
                // $event->sheet->getColumnDimension('I')->setWidth(12);
                // $event->sheet->getColumnDimension('J')->setWidth(10);
                // $event->sheet->getColumnDimension('K')->setWidth(13);
                // $event->sheet->getColumnDimension('L')->setWidth(13);
                // $event->sheet->getColumnDimension('M')->setWidth(12);
                // $event->sheet->getColumnDimension('N')->setWidth(15);
                // $event->sheet->getColumnDimension('O')->setWidth(14);
            }
        ];
    }
    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_DATE_DDMMYYYY
        ];
    }
}
