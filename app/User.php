<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $primaryKey = 'id_user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'email', 'nombre_fantasia', 'cuit', 'numero_iibb', 'documento', 'status', 
        'username', 'telefono', 'telefono_movil', 'description_status', 'password', 'valor_dolar', 'perfil_id', 'deleted','responsabilidad'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'last_login_at' => 'datetime',
    ];

    public function roles() {
        return $this->belongsToMany(Role::class)
        ->withTimestamps();
    }

    public function pagos() {
        return $this->belongsToMany(Pago::class)
        ->withTimestamps();
    }
    
    public function direcciones() {
        return $this->belongsToMany(Direccion::class)
        ->withTimestamps();
    }
    
    public function pedidos() {
        return $this
        ->belongsToMany(Pedido::class)
        ->withTimestamps();
    }

    public function perfil() {
        return $this
        ->belongsTo(Perfil::class)
        ->withTimestamps();
    }

    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first()) {
            return true;
        }
        return false;
    }

    public function isAdmin()
    {
        $perfil = DB::table('users')
        ->where('users.id_user', '=', $this->id_user)
        ->where(function($q){
            $q->where('perfils.nombre', 'Superadmin')
              ->orWhere('perfils.nombre', 'Admin');
         })
        ->leftJoin('perfils', 'perfils.id_perfil', '=', 'users.perfil_id')
        ->select('perfils.nombre as perfil')->get()->first();
        return $perfil;
    }

    public function hasPermiso($permiso, $accion)
    {
        $permiso = DB::table('users')->where('users.id_user', '=', $this->id_user)->where('permisos.nombre', $permiso)
            ->leftJoin('perfils', 'perfils.id_perfil', '=', 'users.perfil_id')
            ->leftJoin('perfil_permiso', 'perfils.id_perfil', '=', 'perfil_permiso.perfil_id_perfil')
            ->leftJoin('permisos', 'permisos.id_permiso', '=', 'perfil_permiso.permiso_id_permiso')
            ->select('perfil_permiso.*')->get()->first();
        if($permiso && $accion == 'alguno'){
            return $permiso->ver || $permiso->crear || $permiso->editar || $permiso->borrar;
        }else{
            return $permiso && $permiso->$accion;
        }
    }

    public function countItems()
    {
        $items = DB::table('item_carrito')->where('user_id_user', $this->id_user)->count();
        return $items;
    }

    public function items()
    {
        $items = DB::table('item_carrito')->where('user_id_user', $this->id_user)
        ->leftJoin('productos', 'productos.id_producto', '=', 'item_carrito.producto_id_producto')
        ->select('productos.nombre','productos.codigo','item_carrito.cantidad','item_carrito.id_item_carrito')->get();
        return $items;
    }

    public function redirect_login()
    {
        $perfil = DB::table('perfils')->where('id_perfil', $this->perfil_id)->get()->first();
        return $perfil->redirect_login;
    }
}
