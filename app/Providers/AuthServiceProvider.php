<?php

namespace App\Providers;

use App\Permiso;
use App\Policies\PermisoPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
         'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->registerPermisoPolicies();

        Passport::routes();
    }
    public function registerPermisoPolicies()
    {
        Gate::define('DOL-ADM-Proveedores-editar', function ($user) {
            return  env('MANEJO_DOLARES', false) && 
                    $user->isAdmin() && 
                    $user->hasPermiso('Proveedores', 'editar');
        });
        Gate::define('STOCK-ADM-Productos-ver', function ($user) {
            return  env('GESTION_STOCK', false) && 
                    $user->isAdmin() && 
                    $user->hasPermiso('Productos', 'ver');
        });
        Gate::define('PRODUCTO-ADM-Productos-editar', function ($user) {
            return  env('PRECIO_PRODUCTO', false) && 
                    $user->isAdmin() && 
                    $user->hasPermiso('Productos', 'editar');
        });
        $permisos = Permiso::all();
        foreach($permisos as $permiso){
            Gate::define('ADM-'.$permiso->nombre.'-crear', function ($user) use($permiso) {
                return $user->isAdmin() && $user->hasPermiso($permiso->nombre, 'crear');
            });
            Gate::define('ADM-'.$permiso->nombre.'-editar', function ($user) use($permiso){
                return $user->isAdmin() && $user->hasPermiso($permiso->nombre, 'editar');
            });
            Gate::define('ADM-'.$permiso->nombre.'-ver', function ($user) use($permiso){
                return $user->isAdmin() && $user->hasPermiso($permiso->nombre, 'ver');
            });
            Gate::define('ADM-'.$permiso->nombre.'-borrar', function ($user) use($permiso){
                return $user->isAdmin() && $user->hasPermiso($permiso->nombre, 'borrar');
            });
            Gate::define('ADM-'.$permiso->nombre.'-alguno', function ($user) use($permiso){
                return $user->isAdmin() && $user->hasPermiso($permiso->nombre, 'alguno');
            });
            Gate::define($permiso->nombre.'-crear', function ($user) use($permiso) {
                return !$user->isAdmin() && $user->hasPermiso($permiso->nombre, 'crear');
            });
            Gate::define($permiso->nombre.'-editar', function ($user) use($permiso){
                return !$user->isAdmin() && $user->hasPermiso($permiso->nombre, 'editar');
            });
            Gate::define($permiso->nombre.'-ver', function ($user) use($permiso){
                return !$user->isAdmin() && $user->hasPermiso($permiso->nombre, 'ver');
            });
            Gate::define($permiso->nombre.'-borrar', function ($user) use($permiso){
                return !$user->isAdmin() && $user->hasPermiso($permiso->nombre, 'borrar');
            });
            Gate::define($permiso->nombre.'-alguno', function ($user) use($permiso){
                return !$user->isAdmin() && $user->hasPermiso($permiso->nombre, 'alguno');
            });
        }
    }

}
