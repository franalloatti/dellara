<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Gate::define('admin', function ($user) {
            $perfil = DB::table('users')->where('users.id_user', '=', $user->id_user)
            ->leftJoin('perfils', 'perfils.id_perfil', '=', 'users.perfil_id')
            ->select('perfils.nombre as perfil')->get()->first();
            if ($perfil->perfil == 'Admin' || $perfil->perfil == 'Superadmin') {
                return true;
            }
            return false;
        });
        Gate::define('superadmin', function ($user) {
            $perfil = DB::table('users')->where('users.id_user', '=', $user->id_user)
            ->leftJoin('perfils', 'perfils.id_perfil', '=', 'users.perfil_id')
            ->select('perfils.nombre as perfil')->get()->first();
            if ($perfil->perfil == 'Superadmin') {
                return true;
            }
            return false;
        });
        Gate::define('client', function ($user) {
            $perfil = DB::table('users')->where('users.id_user', '=', $user->id_user)
            ->leftJoin('perfils', 'perfils.id_perfil', '=', 'users.perfil_id')
            ->select('perfils.nombre as perfil')->get()->first();
            if ($perfil->perfil != 'Superadmin' && $perfil->perfil != 'Admin' && $perfil->perfil != 'Proveedor') {
                return true;
            }
            return false;
        });
        Gate::define('dolares', function ($user) {
            if(env('MANEJO_DOLARES',false)){
                $perfil = DB::table('users')->where('users.id_user', '=', $user->id_user)
                ->leftJoin('perfils', 'perfils.id_perfil', '=', 'users.perfil_id')
                ->select('perfils.nombre as perfil')->get()->first();
                if ($perfil->perfil == 'Admin' || $perfil->perfil == 'Superadmin') {
                    return true;
                }
            }
            return false;
        });
    }
}
