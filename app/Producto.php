<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $primaryKey = 'id_producto';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'codigo', 'nombre', 'descripcion', 'margen', 'stock_verde', 'stock_amarillo', 'stock_seguridad', 
        'tiene_fijado', 'precio_fijado', 'estado', 'iva', 'concepto', 'tipo', 'unidad', 'conversion', 'deleted',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    public function marcas() {
        return $this
        ->belongsToMany(Marca::class)
        ->withTimestamps();
    }

    public function almacenes() {
        return $this
        ->belongsToMany(Almacen::class)
        ->withTimestamps();
    }

    public function proveedores() {
        return $this
        ->belongsToMany(User::class)
        ->withTimestamps();
    }

    public function rubros() {
        return $this
        ->belongsToMany(Rubro::class)
        ->withTimestamps();
    }

    public function imagenes() {
        return $this
        ->belongsToMany(Imagen::class)
        ->withTimestamps();
    }
    
    // public function remitos() {
    //     return $this
    //     ->belongsToMany(Remito::class)
    //     ->withTimestamps();
    // }
    
    public function pedidos() {
        return $this
        ->belongsToMany(Pedido::class)
        ->withTimestamps();
    }
}
