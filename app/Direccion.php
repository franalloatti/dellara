<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Direccion extends Model
{
    protected $primaryKey = 'id_direccion';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'identificacion_lugar', 'direccion', 'observaciones', 'localidad', 'provincia', 'codigo_postal', 'telefono', 'user_id', 'estado', 'es_direccion_fact', 'deleted'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    public function user() {
        return $this
        ->belongsTo(User::class)
        ->withTimestamps();
    }
    
    public function pedidos() {
        return $this
        ->belongsToMany(Pedido::class)
        ->withTimestamps();
    }
}
