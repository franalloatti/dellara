<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ActualizarStock extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stock:actualizar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Script de actualización de stock';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // CONSOLIDAR MOVIMIENTOS SALIENTES
        $movstock_origen = DB::table('almacen_producto as AP')->whereRaw("cast(M.created_at as date) = cast(DATE_SUB(NOW(), INTERVAL 1 DAY) as date)")
        ->join('movimiento_stock as M', 'M.almacen_id_origen', '=', 'AP.id_stock')
        ->join('productos as P', function($join){
            $join->on('P.id_producto', '=', 'AP.producto_id_producto')
            ->whereRaw("(P.tipo <> 'Producto' or M.tipo_movimiento <> 'Producción')");
        })
        ->select('M.tipo_movimiento', 'M.almacen_id_origen', 'M.cantidad')
        ->orderBy('M.created_at', 'desc')->get();
        foreach($movstock_origen as $movimiento){
            $stock = DB::table('almacen_producto')->where('id_stock', $movimiento->almacen_id_origen);
            if($movimiento->tipo_movimiento = 'Transferencia'){
                $stock->decrement('cantidad', $movimiento->cantidad);
            }else if($movimiento->tipo_movimiento = 'Ajuste'){
                if($movimiento->cantidad > 0){
                    $stock->increment('cantidad', $movimiento->cantidad);
                }else{
                    $stock->decrement('cantidad', $movimiento->cantidad);
                }
            }else if($movimiento->tipo_movimiento = 'Producción'){
                if($movimiento->cantidad > 0){
                    $stock->decrement('cantidad', $movimiento->cantidad);
                }else{
                    $stock->increment('cantidad', $movimiento->cantidad);
                }
            }
        }

        // CONSOLIDAR TRANSFERENCIAS ENTRANTES
        $movstock_destino = DB::table('almacen_producto as AP')->whereRaw("cast(M.created_at as date) = cast(DATE_SUB(NOW(), INTERVAL 1 DAY) as date)")
        ->whereRaw("M.tipo_movimiento <> 'Ajuste'")
        ->join('movimiento_stock as M', 'M.almacen_id_destino', '=', 'AP.id_stock')
        ->join('productos as P', function($join){
            $join->on('P.id_producto', '=', 'AP.producto_id_producto')
            ->whereRaw("(P.tipo <> 'Insumo' or M.tipo_movimiento <> 'Producción')");
        })
        ->select('M.tipo_movimiento', 'M.almacen_id_destino', 'M.cantidad')
        ->orderBy('M.created_at', 'desc')->get();
        foreach($movstock_destino as $movimiento){
            $stock = DB::table('almacen_producto')
            ->where('id_stock', $movimiento->almacen_id_destino)
            ->increment('cantidad', $movimiento->cantidad);
        }

        // CONSOLIDAR MOVIMIENTOS DE REMITOS DEL DIA
        $item_remito = DB::table('producto_remito as PR')->whereRaw("cast(PR.created_at as date) = cast(DATE_SUB(NOW(), INTERVAL 1 DAY) as date)")
        ->join('pedido_producto as PP', 'PR.producto_id_producto', '=', 'PP.id_pedido_producto')
        ->join('pedidos as P', 'P.id_pedido', '=', 'PP.pedido_id_pedido')
        ->select('P.tipo', 'P.motivo_devolucion', 'PR.almacen_id_almacen', 'PP.producto_id_producto')
        ->selectRaw('sum(PR.cantidad) as cantidad')
        ->groupBy('PR.almacen_id_almacen', 'PP.producto_id_producto', 'P.tipo')
        ->orderBy('PR.created_at', 'desc')
        ->get();
        foreach($item_remito as $movimiento){
            $stock = DB::table('almacen_producto')
            ->where('almacen_id_almacen', $movimiento->almacen_id_almacen)
            ->where('producto_id_producto', $movimiento->producto_id_producto);
            switch($movimiento->tipo){
                case 'Pedido':
                    $stock->decrement('cantidad', $movimiento->cantidad);
                    break;
                case 'Devolucion':
                    if($movimiento->motivo_devolucion == 'error'){
                        $stock->increment('cantidad', $movimiento->cantidad);
                    }
                    break;
                case 'Compra':
                    $stock->increment('cantidad', $movimiento->cantidad);
                    break;
                case 'Devolucion compra':
                    $stock->decrement('cantidad', $movimiento->cantidad);
                    break;
            }
        }
        $this->info('La actualización de stock se realizó correctamente');
    }
}
