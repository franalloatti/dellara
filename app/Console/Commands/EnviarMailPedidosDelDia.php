<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Notifications\MailNuevosPedidosXDiaNotificacion;
use App\User;

class EnviarMailPedidosDelDia extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pedidos:hoy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envio de mail con pedidos del dia';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if(env('MAIL_NUEVO_PEDIDO_X_DIA', false)){
            $pedidos = DB::table('pedidos')->where('pedidos.tipo', '=', 'Pedido')
            ->whereRaw('cast(pedidos.created_at as date) = cast(NOW() as date)')
            ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
            ->select('pedidos.*','users.name')
            ->get();
            // echo json_encode($pedidos);die;
            $user = User::where('email', '=', env('MAIL_TO_ADDRESS', '---'))->get()->first();
            if($user){
                $user->notify(new MailNuevosPedidosXDiaNotificacion($pedidos));
            }
            if(env('MAIL_TO_ADDRESS_COPY', '') != ''){
                $user = User::where('email', '=', env('MAIL_TO_ADDRESS_COPY', '---'))->get()->first();
                if($user){
                    $user->notify(new MailNuevosPedidosXDiaNotificacion($pedidos));
                }
            }
        }
        $this->info('Los mails se enviaron correctamente');
    }
}
