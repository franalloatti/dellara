<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Notifications\MailStocksBajosXDiaNotificacion;
use App\User;

class EnviarMailStocksBajosDelDia extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stocks:bajos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envio de mail con stocks bajos del dia';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if(env('MAIL_STOCK_FALTANTE_X_DIA', false)){
            $productos = DB::table('productos')->where('almacen_producto.almacen_id_almacen', env('ALMACEN_PRINCIPAL', 1))
            ->join('almacen_producto', 'almacen_producto.producto_id_producto', '=', 'productos.id_producto')
            ->leftJoin(DB::raw("(SELECT M.almacen_id_destino, M.cantidad FROM almacen_producto AP
            left join movimiento_stock M on AP.id_stock = M.almacen_id_destino
            where M.tipo_movimiento = 'Transferencia' and cast(M.created_at as date) = cast(NOW() as date)
            order by M.created_at desc) as MOV_DEST"), 'almacen_producto.id_stock', '=', 'MOV_DEST.almacen_id_destino')
            ->leftJoin(DB::raw("(SELECT M.almacen_id_origen, if(M.tipo_movimiento = 'Transferencia', -1*M.cantidad,M.cantidad) AS 'cantidad' FROM movimiento_stock M
            join almacen_producto AP on AP.id_stock = M.almacen_id_origen
            where cast(M.created_at as date) = cast(NOW() as date)
            order by M.created_at desc) as MOV_ORI"), 'almacen_producto.id_stock', 'MOV_ORI.almacen_id_origen')
            ->leftJoin(DB::raw("(SELECT P.tipo, P.motivo_devolucion, PP.producto_id_producto, PR.almacen_id_almacen, sum(IF(P.tipo = 'Pedido' or P.tipo = 'Devolucion compra', -1*PR.cantidad,PR.cantidad)) as 'cantidad' FROM producto_remito PR
            join pedido_producto PP on PR.producto_id_producto = PP.id_pedido_producto
            join pedidos P on PP.pedido_id_pedido = P.id_pedido
            where cast(PR.created_at as date) = cast(NOW() as date) and ((P.tipo <> 'Devolucion') or (P.tipo = 'Devolucion' and P.motivo_devolucion <> 'defecto'))
            group by P.tipo, PR.almacen_id_almacen, PP.producto_id_producto
            order by PR.created_at desc) as ITM_REM"), 
                function($join){
                    $join->on('almacen_producto.almacen_id_almacen', '=', 'ITM_REM.almacen_id_almacen')
                    ->on('almacen_producto.producto_id_producto', '=', 'ITM_REM.producto_id_producto');
                })
            ->select('productos.*')
            ->selectRaw("(ifnull(almacen_producto.cantidad,0.00) + ifnull(MOV_DEST.cantidad,0.00) + ifnull(MOV_ORI.cantidad,0.00) + 
            if(ITM_REM.tipo = 'Pedido', ITM_REM.cantidad,0.00) + if(ITM_REM.tipo = 'Devolucion', ITM_REM.cantidad,0.00) + 
            if(ITM_REM.tipo = 'Compra', ITM_REM.cantidad,0.00) + if(ITM_REM.tipo = 'Devolucion compra', ITM_REM.cantidad,0.00)) as stock")
            ->havingRaw('stock <= stock_seguridad')
            ->get();
            // echo json_encode($productos);
            $user = User::where('email', '=', env('MAIL_TO_ADDRESS', '---'))->get()->first();
            if($user){
                $user->notify(new MailStocksBajosXDiaNotificacion($productos));
            }
            if(env('MAIL_TO_ADDRESS_COPY', '') != ''){
                $user = User::where('email', '=', env('MAIL_TO_ADDRESS_COPY', '---'))->get()->first();
                if($user){
                    $user->notify(new MailStocksBajosXDiaNotificacion($productos));
                }
            }
        }
        $this->info('Los mails se enviaron correctamente');
    }
}
