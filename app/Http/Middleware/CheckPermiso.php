<?php

namespace App\Http\Middleware;

use Closure;
use App\Perfil;
use Illuminate\Support\Facades\Session;

class CheckPermiso
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$permiso,$accion)
    {
        // Pre-Middleware Action

        //$response = $next($request);

        // Post-Middleware Action

        //return $response;
        if($request->user() != null){
            $perfil = Perfil::where('id_perfil',$request->user()->perfil_id)->get()->first();
            
            if (!$perfil->hasPermiso($permiso,$accion) || $request->user()->status != 1) {
                //dd($request->user()->status);
                abort(403, 'No tienes permisos para realizar la acción solicitada.');
                //return redirect('login');
            }
            
        }
        return $next($request);
    }
}
