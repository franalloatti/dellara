<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$role)
    {
        // Pre-Middleware Action

        //$response = $next($request);

        // Post-Middleware Action

        //return $response;
        if($request->user() === null){
            Session::flash('message', 'Debe iniciar sesión para poder acceder al sistema.');
            Session::flash('type', 'danger');
            return redirect()->route('login');
        }

        if ((!$request->user()->hasRole($role) && !$request->user()->hasRole('admin')) || $request->user()->status != 1) {
            //dd($request->user()->status);
            abort(403, 'No tienes autorización para ingresar.');
            //return redirect('login');
        }

        return $next($request);
    }
}
