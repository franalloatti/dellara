<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Perfil;
use Validator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $fields = $request->all();
        $search = '';
        $users = DB::table('users')->where('users.deleted', 0)->where('roles.name', '<>', 'admin');
        if(isset($fields['search'])){
            $search = $fields['search'];
            $users = $users->where('roles.name', 'like', '%' . $search . '%')->orWhere('users.name', 'like', '%' . $search . '%')
            ->orWhere('users.surname', 'like', '%' . $search . '%')->orWhere('users.email', 'like', '%' . $search . '%');
        }
        $users = $users->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->select('users.*','roles.name as role')
        ->paginate(10);
        return view('usuarios.list', ['search' =>  $search, 'users' => $users]);
    }

    public function marcarleida($id){

        auth()->user()->unreadNotifications->when($id, function($query) use ($id){
            return $query->where('id', $id);
        })->markAsRead();
        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all()->where('roles.deleted', 0)->where('name', '<>', 'admin');
        $perfiles = Perfil::all()->where('nombre', '<>', 'Superadmin')->where('deleted', 0);
        return view('usuarios.create', ['roles' => $roles, 'perfiles' => $perfiles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'El :attribute es requerido',
            'unique'    => 'El :attribute ingresado ya esta en uso',
            'confirmed'    => 'Las contraseñas ingresadas no coinciden',
            'min'      => 'La :attribute debe tener como mínimo :min caracteres',
          ];
        $rules = array(
            'nombre'=>'required',
            'apellido'=>'required',
            'email'=>'required|unique:users',
            'password'=>'min:6|confirmed',
            'role_id'=>'required',
            'perfil_id'=>'required'
        );

        $validator = Validator::make($request->all(), $rules, $messages);


        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/usuarios/crear')
                ->withErrors($validator);
            // return view('usuarios.crear');
        } else {
            $user = new User([
                'name' => $request->get('nombre'),
                'surname' => $request->get('apellido'),
                'email' => $request->get('email'),
                'perfil_id' => $request->get('perfil_id'),
                'password' => Hash::make($request->get('password'))
            ]);
            $user->save();
            $user->roles()->sync([$request->get('role_id')]);
            // redirect
            Session::flash('message', 'El usuario se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/usuarios/index');
            // return view('usuarios.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    public function suspend($id)
    {
        $user = User::where('id_user',$id)->get()->first();
        if($user->status){
            $user->status = 0;
        }else{
            $user->status = 1;
        }
        $user->update();
        // redirect
        Session::flash('message', 'Se actualizó el estado con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/usuarios/index');
        // return view('usuarios.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::all()->where('roles.deleted', 0)->where('name', '<>', 'admin');
        $perfiles = Perfil::all()->where('nombre', '<>', 'Superadmin')->where('deleted', 0);
        $user = User::where('id_user',$id)
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->select('users.*','role_user.role_id_role as id_role')->get()->first();
        return view('usuarios.edit', ['user' => $user, 'roles' => $roles, 'perfiles' => $perfiles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::where('id_user',$id)->get()->first();
        $messages = [
            'required' => 'El :attribute es requerido',
            'unique'    => 'El :attribute ingresado ya esta en uso',
            'confirmed'    => 'Las contraseñas ingresadas no coinciden',
            'min'      => 'La contraseña debe tener como mínimo :min caracteres',
          ];
        $rules = array(
            'nombre'=>'required',
            'apellido'=>'required',
            'role_id'=>'required',
            'perfil_id'=>'required'
        );
        $fields = $request->all();
        if($fields['email'] != $user->email) {
            $rules['email'] = 'required|unique:users';
        }
        if(!empty($fields['password'])) {
            $rules['password'] = 'min:6|confirmed';
            $fields['password'] = Hash::make($request->get('password'));
        }else{
            unset($fields['password']); 
        }

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/usuarios/editar/'.$id)
                ->withErrors($validator);
            // return view('usuarios.editar', [$id]);
        } else {
            $user->name = $fields['nombre'];
            $user->surname = $fields['apellido'];
            if(isset($fields['password'])) {
                $user->password = $fields['password'];
            }
            $user->update($fields);
            $user->roles()->sync([$request->get('role_id')]);
            // redirect
            Session::flash('message', 'El usuario se actualizó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/usuarios/index');
            // return view('usuarios.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $user = User::find($id);
        $user->deleted = 1;
        $user->save();

        // redirect
        Session::flash('message', 'El usuario se eliminó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/usuarios/index');
        // return view('usuarios.index');
    }
}
