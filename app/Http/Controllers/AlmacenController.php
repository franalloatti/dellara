<?php

namespace App\Http\Controllers;

use App\Almacen;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class AlmacenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $almacenes = DB::table('almacens')->where('deleted', 0)->paginate(10);
        return view('almacenes.list', ['almacenes' => $almacenes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('almacenes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'nombre'=>'required'
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/almacenes/crear')
                ->withErrors($validator);
                // return view('almacenes.crear');
        } else {
            $almacen = new Almacen([
                'nombre' => $request->get('nombre'),
                'descripcion' => $request->get('descripcion')
            ]);
            $almacen->save();
            // redirect
            Session::flash('message', 'El almacen se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/almacenes/index');
            // return view('almacenes.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Almacen  $almacen
     * @return \Illuminate\Http\Response
     */
    public function show(Almacen $almacen)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Almacen  $almacen
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $almacen = Almacen::where('id_almacen',$id)->get()->first();
        return view('almacenes.edit', ['almacen' => $almacen]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Almacen  $almacen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $almacen = Almacen::where('id_almacen',$id)->get()->first();
        $messages = [
            'required' => 'El :attribute es requerido'
          ];
        $rules = array(
            'nombre'=>'required'
        );
        $fields = $request->all();
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/almacenes/editar/'.$id)
                ->withErrors($validator);
                // return view('almacenes.editar', [$id]);
        } else {
            $almacen->update($fields);
            // redirect
            Session::flash('message', 'El almacen se actualizó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/almacenes/index');
            // return view('almacenes.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Almacen  $almacen
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $almacen = Almacen::find($id);
        $almacen->deleted = 1;
        $almacen->save();

        // redirect
        Session::flash('message', 'El almacen se eliminó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/almacenes/index');
        // return view('almacenes.index');
    }
}
