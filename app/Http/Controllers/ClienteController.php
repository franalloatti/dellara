<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Perfil;
use App\User;
use App\Role;
use Auth;
use App\Direccion;
use Validator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $fields = $request->all();
        $search = '';
        $users = DB::table('users')->where('users.deleted', 0)->where('roles.name', '<>', 'admin')->where('roles.name', '<>', 'administrador')->where('roles.name', '<>', 'Proveedor')->where('roles.name', '<>', 'Consumidor final');
        $users = $users->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')->whereRaw('(`estado_pedido`.`estado_id_estado` < 4 or `estado_pedido`.`estado_id_estado` is null)')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->leftJoin('pedidos', 'pedidos.user_id_user', '=', 'users.id_user')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('remitos', 'pedidos.id_pedido', '=', 'remitos.pedido_id_pedido')
        ->leftJoin(DB::raw("(SELECT TEMPO.user_id_user, SUM(TEMPO.monto_pagos) AS 'monto_pagos' FROM (
            SELECT P.user_id_user, P.id_pedido, ifnull(SUM(PR.monto),0) - IF(P.tipo = 'Pedido', R.monto, (-1 * R.monto)) as 'monto_pagos' 
            FROM pedidos P
            JOIN estado_pedido ES ON P.id_pedido = ES.pedido_id_pedido 
            AND ES.created_at = (SELECT MAX(ES1.created_at) FROM estado_pedido ES1 WHERE ES1.pedido_id_pedido = P.id_pedido) AND ES.estado_id_estado < 4
            LEFT JOIN remitos R on P.id_pedido = R.pedido_id_pedido
            LEFT JOIN pago_remito PR on R.id_remito = PR.remito_id_remito
            GROUP BY P.user_id_user, R.id_remito
            ) TEMPO GROUP BY TEMPO.user_id_user) as TEMP "),function($join){
           $join->on('TEMP.user_id_user', 'pedidos.user_id_user');
        })
        ->select('users.*','roles.name as role')
        ->selectRaw("TEMP.monto_pagos as monto")
        ->groupBy('users.id_user');
        // echo json_encode($users->get());
        // die;
        $monto_total = 0;
        foreach($users->get() as $user){
            $monto_total = $monto_total + $user->monto;
        }
        if(isset($fields['search'])){
            $search = $fields['search'];
            $users = $users->whereRaw("(`users`.`name` like '%".$search."%' or `roles`.`name` like '%".$search."%' or `users`.`email` like '%".$search."%')");
        }
        $users = $users->paginate(10);
        // echo json_encode($users);
        // die;
        return view('clientes.list', ['search' =>  $search, 'users' => $users, 'monto_total' => $monto_total]);
    }

    public function indexconsumidor(Request $request)
    {
        $fields = $request->all();
        $search = '';
        $users = DB::table('users')->where('users.deleted', 0)->where('roles.name', '=', 'Consumidor final');
        if(isset($fields['search'])){
            $search = $fields['search'];
            $users = $users->whereRaw("(`users`.`name` like '%".$search."%' or `users`.`surname` 
            like '%".$search."%' or `users`.`telefono` like '%".$search."%' or `users`.`email` like '%".$search."%')");
        }
        $users = $users->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->select('users.*','roles.name as role')
        ->paginate(10);
        return view('clientes.listconsumidor', ['search' =>  $search, 'users' => $users]);
    }

    public function cuenta(Request $request, $id = null)
    {
        if(isset($id)){
            $id_cliente = $id;
        }else{
            $id_cliente = Auth::user()->id_user;
        }
        $cliente = DB::table('users')->where('users.id_user', '=', $id_cliente)
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->select('users.*', 'roles.name as role')->get()->first();
        $pagos = DB::table('pagos')->where('pagos.user_id_user', '=', $id_cliente)->where('pagos.deleted', 0)
        ->leftJoin('users', 'pagos.user_id_user', '=', 'users.id_user')
        ->leftJoin('pago_remito', 'pago_remito.pago_id_pago', '=', 'pagos.id_pago')
        ->select('pagos.created_at', 'pagos.monto')
        ->selectRaw("pagos.monto - sum(ifnull(`pago_remito`.`monto`,0)) as 'monto_faltante', CONCAT('Ingreso') as tipo, CONCAT('PAGO') as movimiento")
        ->groupBy('pagos.id_pago')->orderBy('pagos.created_at')
        ->havingRaw('monto_faltante > 0')->get();
        $remitos = DB::table('remitos')->where('users.id_user', $id_cliente)->whereRaw('(`estado_pedido`.`estado_id_estado` < 4 or `estado_pedido`.`estado_id_estado` is null)')
        ->leftJoin('pedidos', 'pedidos.id_pedido', '=', 'remitos.pedido_id_pedido')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('pago_remito', 'pago_remito.remito_id_remito', '=', 'remitos.id_remito')
        ->select('remitos.created_at', 'remitos.monto', 'pedidos.tipo as tipo_pedido')
        ->selectRaw("`remitos`.`monto` - sum(ifnull(`pago_remito`.`monto`,0)) as 'monto_faltante', CONCAT(if(`pedidos`.`tipo` = 'Pedido','REMITO ','NOTA DE CREDITO '), remitos.id_remito) as movimiento, CONCAT(if(`pedidos`.`tipo` = 'Pedido','Egreso','Ingreso')) as tipo")
        ->groupBy('remitos.id_remito')->orderBy('remitos.created_at')
        ->havingRaw('(`remitos`.`monto` - sum(ifnull(`pago_remito`.`monto`,0))) > 0')->get();
        $item_pagos = 0;
        $item_remitos = 0;
        $total_egresos = 0;
        $total_ingresos = 0;
        // echo json_encode($pagos);
        // echo json_encode($remitos);
        // die;
        $array = array();
        while($item_pagos < count($pagos) || $item_remitos < count($remitos)){
            if($item_pagos == count($pagos)){
                array_push($array, $remitos[$item_remitos]);
                if($remitos[$item_remitos]->tipo_pedido == 'Pedido'){
                    $total_egresos = $total_egresos + $remitos[$item_remitos]->monto_faltante;
                }else{
                    $total_ingresos = $total_ingresos + $remitos[$item_remitos]->monto_faltante;
                }
                $item_remitos++;
            }else if($item_remitos == count($remitos)){
                array_push($array, $pagos[$item_pagos]);
                $total_ingresos = $total_ingresos + $pagos[$item_pagos]->monto_faltante;
                $item_pagos++;
            }else{
                if($pagos[$item_pagos]->created_at < $remitos[$item_remitos]->created_at){
                    array_push($array, $pagos[$item_pagos]);
                    $total_ingresos = $total_ingresos + $pagos[$item_pagos]->monto_faltante;
                    $item_pagos++;
                }else{
                    array_push($array, $remitos[$item_remitos]);
                    if($remitos[$item_remitos]->tipo_pedido == 'Pedido'){
                        $total_egresos = $total_egresos + $remitos[$item_remitos]->monto_faltante;
                    }else{
                        $total_ingresos = $total_ingresos + $remitos[$item_remitos]->monto_faltante;
                    }
                    $item_remitos++;
                }
            }
        }
        // echo json_encode(['cliente' =>  $cliente]);
        // die;
        return view('clientes.cuenta', ['cliente' => $cliente, 'total_egresos' =>  $total_egresos, 'total_ingresos' =>  $total_ingresos, 'movimientos' =>  $array]);
    }

    public function cuentaampliado(Request $request, $id = null)
    {
        if(isset($id)){
            $id_cliente = $id;
        }else{
            $id_cliente = Auth::user()->id_user;
        }
        $cliente = DB::table('users')->where('users.id_user', '=', $id_cliente)->get()->first();
        $pagos = DB::table('pagos')->where('pagos.user_id_user', '=', $id_cliente)->where('pagos.deleted', 0)
        ->leftJoin('users', 'pagos.user_id_user', '=', 'users.id_user')
        ->leftJoin('pago_remito', 'pago_remito.pago_id_pago', '=', 'pagos.id_pago')
        ->select('pagos.created_at', 'pagos.monto')
        ->selectRaw("pagos.monto - sum(ifnull(`pago_remito`.`monto`,0)) as 'monto_faltante', CONCAT('Ingreso') as tipo, CONCAT('PAGO') as movimiento")
        ->groupBy('pagos.id_pago')->orderBy('pagos.created_at')
        // ->havingRaw('monto_faltante > 0')
        ->get();
        $remitos = DB::table('remitos')->where('users.id_user', $id_cliente)->where('remitos.monto', '>', 0.00)->whereRaw('(`estado_pedido`.`estado_id_estado` < 4 or `estado_pedido`.`estado_id_estado` is null)')
        ->leftJoin('pedidos', 'pedidos.id_pedido', '=', 'remitos.pedido_id_pedido')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('pago_remito', 'pago_remito.remito_id_remito', '=', 'remitos.id_remito')
        ->select('remitos.created_at', 'remitos.monto', 'pedidos.tipo as tipo_pedido')
        ->selectRaw("`remitos`.`monto` - sum(ifnull(`pago_remito`.`monto`,0)) as 'monto_faltante', CONCAT(if(`pedidos`.`tipo` = 'Pedido','REMITO ','NOTA DE CREDITO '), remitos.id_remito) as movimiento, CONCAT(if(`pedidos`.`tipo` = 'Pedido','Egreso','Ingreso')) as tipo")
        ->groupBy('remitos.id_remito')->orderBy('remitos.created_at')
        // ->havingRaw('(`remitos`.`monto` - sum(ifnull(`pago_remito`.`monto`,0))) > 0')
        ->get();
        $item_pagos = 0;
        $item_remitos = 0;
        $total_egresos = 0;
        $total_ingresos = 0;
        // echo json_encode($pagos);
        // echo json_encode($remitos);
        // die;
        $array = array();
        while($item_pagos < count($pagos) || $item_remitos < count($remitos)){
            if($item_pagos == count($pagos)){
                array_push($array, $remitos[$item_remitos]);
                if($remitos[$item_remitos]->tipo_pedido == 'Pedido'){
                    $total_egresos = $total_egresos + $remitos[$item_remitos]->monto;
                }else{
                    $total_ingresos = $total_ingresos + $remitos[$item_remitos]->monto;
                }
                $item_remitos++;
            }else if($item_remitos == count($remitos)){
                array_push($array, $pagos[$item_pagos]);
                $total_ingresos = $total_ingresos + $pagos[$item_pagos]->monto;
                $item_pagos++;
            }else{
                if($pagos[$item_pagos]->created_at < $remitos[$item_remitos]->created_at){
                    array_push($array, $pagos[$item_pagos]);
                    $total_ingresos = $total_ingresos + $pagos[$item_pagos]->monto;
                    $item_pagos++;
                }else{
                    array_push($array, $remitos[$item_remitos]);
                    if($remitos[$item_remitos]->tipo_pedido == 'Pedido'){
                        $total_egresos = $total_egresos + $remitos[$item_remitos]->monto;
                    }else{
                        $total_ingresos = $total_ingresos + $remitos[$item_remitos]->monto;
                    }
                    $item_remitos++;
                }
            }
        }
        // echo json_encode(['cliente' =>  $cliente]);
        // die;
        return view('clientes.cuentaampliado', ['cliente' => $cliente, 'total_egresos' =>  $total_egresos, 'total_ingresos' =>  $total_ingresos, 'movimientos' =>  $array]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = DB::table('roles')->where('roles.deleted', 0)->where('roles.name', '<>', 'admin')->where('roles.name', '<>', 'administrador')->where('roles.name', '<>', 'Proveedor')->get();
        return view('clientes.create', ['roles' => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'El :attribute es requerido',
            'unique'    => 'El :attribute ingresado ya esta en uso',
            'confirmed'    => 'Las contraseñas ingresadas no coinciden',
            'min'      => 'La contraseña debe tener como mínimo :min caracteres',
          ];
        $rules = array(
            'role_id'=>'required',
            'documento'=>'required',
            'nombre'=>'required',
            'responsabilidad'=>'required'
        );
        $rol_seleccionado = DB::table('roles')->where('roles.id_role', '=', $request->get('role_id'))->get()->first();
        if($rol_seleccionado->name != 'Consumidor final'){
            $rules['nombre_fantasia'] = 'required';
        }else{
            $rules['apellido'] = 'required';
        }
        if(!empty($request->get('email'))) {
            $rules['email'] = 'unique:users';
        }
        if(!empty($request->get('password'))) {
            $rules['password'] = 'min:6|confirmed';
        }
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/clientes/crear')
                ->withErrors($validator);
            // return view('clientes.crear');
        } else {
            $cliente = new User([
                'name' => $request->get('nombre'),
                'surname' => $request->get('apellido'),
                'email' => $request->get('email'),
                'telefono' => $request->get('telefono'),
                'telefono_movil' => $request->get('telefono_movil'),
                'responsabilidad' => $request->get('responsabilidad')
            ]);
            if($rol_seleccionado->name != 'Consumidor final'){
                $perfil = Perfil::where('nombre','Mayorista')->where('deleted', 0)->get()->first();
                $cliente->perfil_id = isset($perfil->id_perfil) ? $perfil->id_perfil : 4;
                $cliente->cuit = $request->get('documento');
                $cliente->numero_iibb = $request->get('numero_iibb');
                $cliente->nombre_fantasia = $request->get('nombre_fantasia');
                $cliente->surname = $request->get('responsabilidad');
            }else{
                $perfil = Perfil::where('nombre','Consumidor final')->where('deleted', 0)->get()->first();
                $cliente->perfil_id = isset($perfil->id_perfil) ? $perfil->id_perfil : 3;
                $cliente->documento = $request->get('documento');
            }
            if(!empty($request->get('password'))) {
                $cliente->password = Hash::make($request->get('password'));
            }
            // if(!empty($request->get('username'))) {
            //     $cliente->username = $request->get('username');
            // }
            // echo $request->get('password');die; 
            $cliente->save();
            // if($rol_seleccionado->name != 'Consumidor final'){
            $username['username'] = substr('000000'.$cliente->id_user, -6);
            if($request->get('password') === null) {
                $cliente->password = Hash::make(substr('000000'.$cliente->id_user, -6));
            }
            $cliente->update($username);
            // }
            $cliente->roles()->sync([$request->get('role_id')]);
            // redirect
            Session::flash('message', 'El cliente se creó con éxito!');
            Session::flash('type', 'success');
            if($rol_seleccionado->name != 'Consumidor final'){
                return Redirect::to('admin/clientes/ver/'.$cliente->id_user);
                // return view('clientes.index');
            }else{
                // return view('clientes.indexconsumidor');
                return Redirect::to('admin/clientes/ver/'.$cliente->id_user);
            }
        }
    }

    public function suspend($id)
    {
        $user = User::where('id_user',$id)->get()->first();
        if($user->status){
            $user->status = 0;
        }else{
            $user->status = 1;
        }
        $user->update();
        // redirect
        Session::flash('message', 'Se actualizó el estado con éxito!');
        Session::flash('type', 'success');
        $role_cliente = DB::table('users')->where('users.id_user', '=', $id)
            ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
            ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
            ->select('roles.name as role')->get()->first();
        if ($role_cliente->role == 'Consumidor final') {
            return Redirect::to('admin/clientes/indexconsumidor');
            // return view('clientes.indexconsumidor');
        }else{
            return Redirect::to('admin/clientes/index');
            // return view('clientes.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = DB::table('users')->where('users.id_user', '=', $id)
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->select('users.*','roles.name as role')->get()->first();
        $direcciones = DB::table('direccions')->where('direccions.user_id', '=', $id)->get();
        return view('clientes.show', ['user' => $user, 'direcciones' => $direcciones]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = DB::table('roles')->where('roles.deleted', 0)->where('roles.name', '<>', 'admin')->where('roles.name', '<>', 'administrador')->where('roles.name', '<>', 'Proveedor')->get();
        $user = User::where('id_user',$id)
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->select('users.*','role_user.role_id_role as id_role')->get()->first();
        return view('clientes.edit', ['user' => $user, 'roles' => $roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cliente = User::where('id_user',$id)->get()->first();
        $messages = [
            'required' => 'El :attribute es requerido',
            'unique'    => 'El :attribute ingresado ya esta en uso',
            'confirmed'    => 'Las contraseñas ingresadas no coinciden',
            'min'      => 'La contraseña debe tener como mínimo :min caracteres',
          ];
        $rules = array(
            'role_id'=>'required',
            'documento'=>'required',
            'nombre'=>'required',
            'responsabilidad'=>'required'
        );
        $rol_seleccionado = DB::table('roles')->where('roles.id_role', '=', $request->get('role_id'))->get()->first();
        if($rol_seleccionado->name != 'Consumidor final'){
            $rules['nombre_fantasia'] = 'required';
        }else{
            $rules['apellido'] = 'required';
        }
        if(!empty($request->get('email')) && $cliente->email != $request->get('email')) {
            $rules['email'] = 'required|unique:users';
        }
        if(!empty($request->get('password'))) {
            $rules['password'] = 'min:6|confirmed';
        }
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/clientes/editar/'.$id)
                ->withErrors($validator);
                // return view('clientes.crear');
        } else {
            $cliente->name = $request->get('nombre');
            $cliente->surname = $request->get('apellido');
            $cliente->email = $request->get('email');
            $cliente->telefono = $request->get('telefono');
            $cliente->telefono_movil = $request->get('telefono_movil');
            $cliente->responsabilidad = $request->get('responsabilidad');
            if($rol_seleccionado->name != 'Consumidor final'){
                $cliente->cuit = $request->get('documento');
                $cliente->numero_iibb = $request->get('numero_iibb');
                $cliente->nombre_fantasia = $request->get('nombre_fantasia');
                $cliente->surname = $this->mepeoCategoriaIVAInverso($request->get('responsabilidad'));
            }else{
                $cliente->documento = $request->get('documento');
            }
            if(!empty($request->get('password'))) {
                $cliente->password = Hash::make($request->get('password'));
            }
            // if(!empty($request->get('username'))) {
            //     $cliente->username = $request->get('username');
            // }
            $cliente->update();
            $cliente->roles()->sync([$request->get('role_id')]);
            // redirect
            Session::flash('message', 'El cliente se actualizó con éxito!');
            Session::flash('type', 'success');
            if($rol_seleccionado->name != 'Consumidor final'){
                return Redirect::to('admin/clientes/index');
                // return view('clientes.index');
            }else{
                // return view('clientes.indexconsumidor');
                return Redirect::to('admin/clientes/indexconsumidor');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role_cliente = DB::table('users')->where('users.id_user', '=', $id)
            ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
            ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
            ->select('roles.name as role')->get()->first();
        // delete
        $user = User::find($id);
        $user->deleted = 1;
        $user->save();

        // redirect
        Session::flash('message', 'El cliente se eliminó con éxito!');
        Session::flash('type', 'success');
        if ($role_cliente->role == 'Consumidor final') {
            return Redirect::to('admin/clientes/indexconsumidor');
            // return view('clientes.indexconsumidor');
        }else{
            // return view('clientes.index');
            return Redirect::to('admin/clientes/index');
        }
    }
    //Funnción que nos  convierte el entero de la razón social de la empresa en el string.
    public function mepeoCategoriaIVAInverso($categoriaIVA){
        switch ($categoriaIVA) {
            case 1:
                $razonSocialEmpresa = 'IVA Responsable Inscripto';
                break;
            case 2:
                $razonSocialEmpresa = 'IVA Responsable no Inscripto';
                break;
            case 3:
                $razonSocialEmpresa = 'IVA no Responsable';
                break;
            case 4:
                $razonSocialEmpresa = 'IVA Sujeto Exento';
                break;
            
            case 5:
                $razonSocialEmpresa = 'Consumidor Final';
                break;
            
            case 6:
                $razonSocialEmpresa = 'Responsable Monotributo';
                break;

            case 7:
                $razonSocialEmpresa = 'Sujeto no Categorizado';
                break;

            case 8:
                $razonSocialEmpresa = 'Proveedor del Exterior';
                break;

            case 9:
                $razonSocialEmpresa = 'Cliente del Exterior';
                break;
                
            case 10:
                $razonSocialEmpresa = ' IVA Liberado – Ley Nº 19.640';
                break;

            case 11:
                $razonSocialEmpresa = 'IVA Responsable Inscripto – Agente de Percepción';
                break;

            case 12:
                $razonSocialEmpresa = 'Pequeño Contribuyente Eventual';
                break;

            case 13:
                $razonSocialEmpresa = 'Monotributista Social';
                break;

            case 14:
                $razonSocialEmpresa = 'Pequeño Contribuyente Eventual Social';
                break;

            default:
                $razonSocialEmpresa = 'IVA Responsable Inscripto';
                break;
        }
        return $razonSocialEmpresa;
    }
}
