<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Perfil;
use App\User;
use App\TipoMovimiento;
use App\Pago;
use Validator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;

class ProveedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $fields = $request->all();
        $search = '';
        $users = DB::table('users')->where('users.deleted', 0)->where('roles.name', '=', 'Proveedor')->whereRaw('(`estado_pedido`.`estado_id_estado` < 4 or `estado_pedido`.`estado_id_estado` is null)');
        if(isset($fields['search'])){
            $search = $fields['search'];
            $users = $users->whereRaw("(`roles`.`name` like '%".$search."%' or `users`.`nombre_fantasia` 
            like '%".$search."%' or `users`.`cuit` like '%".$search."%' or `users`.`email` like '%".$search."%')");
        }
        $users = $users->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->leftJoin('pedidos', 'pedidos.user_id_user', '=', 'users.id_user')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('remitos', 'pedidos.id_pedido', '=', 'remitos.pedido_id_pedido')
        ->leftJoin(DB::raw("(SELECT P.user_id_user, PR.remito_id_remito, sum(PR.monto) as 'monto_pagos' 
        FROM pago_remito PR
        join remitos R on R.id_remito = PR.remito_id_remito
        join pedidos P on P.id_pedido = R.pedido_id_pedido
        group by P.user_id_user) as TEMP "),function($join){
           $join->on('TEMP.user_id_user', 'pedidos.user_id_user');
        })
        ->select('users.*','roles.name as role')
        ->selectRaw("ifnull(TEMP.monto_pagos,0) - sum(if(pedidos.tipo = 'Compra', remitos.monto, -1 *remitos.monto)) as monto")
        ->groupBy('users.id_user')
        ->paginate(10);
        // echo json_encode($users);die;
        return view('proveedores.list', ['search' =>  $search, 'users' => $users]);
    }

    public function actualizar_dol(Request $request)
    {
        $users = DB::table('users')->where('users.deleted', 0)->where('roles.name', '=', 'Proveedor');
        $users = $users->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->select('users.*','roles.name as role')
        ->paginate(10);
        return view('proveedores.actualizar_dol', ['users' => $users]);
    }

    public function actualizar_dol_success(Request $request)
    {
        $fields = $request->all();
        // echo json_encode($fields);die;
        foreach($fields['items'] as $item){
            DB::table('users')->where('id_user', $item['id_user'])->update(
                [
                'valor_dolar' => $item['valor_dolar'],
                'updated_at' => date('Y-m-d H:i:s'),
                ]
            );
        }
        Session::flash('message', 'Se actualizaron los valores del dolar de los proveedores con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/pedidos/index');
    }

    public function cuenta(Request $request, $id = null)
    {
        if(isset($id)){
            $id_proveedor = $id;
        }else{
            $id_proveedor = Auth::user()->id_user;
        }
        $proveedor = DB::table('users')->where('users.id_user', '=', $id_proveedor)
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->select('users.*', 'roles.name as role')->get()->first();
        $pagos = DB::table('pagos')->where('pagos.user_id_user', '=', $id_proveedor)->where('pagos.deleted', 0)
        ->leftJoin('users', 'pagos.user_id_user', '=', 'users.id_user')
        ->leftJoin('pago_remito', 'pago_remito.pago_id_pago', '=', 'pagos.id_pago')
        ->select('pagos.created_at', 'pagos.monto as monto_pesos')
        ->selectRaw("(pagos.monto/pagos.conversion_dolar) as monto, CONCAT('PAGO') as movimiento, CONCAT('Egreso') as tipo, 
        (pagos.monto/pagos.conversion_dolar) - sum(ifnull(`pago_remito`.`monto`,0)) as 'monto_faltante', 
        pagos.monto - sum(ifnull((`pago_remito`.`monto`),0))*pagos.conversion_dolar as 'monto_faltante_pesos'")
        ->groupBy('pagos.id_pago')->orderBy('pagos.created_at')
        ->havingRaw('monto_faltante > 0')->get();
        $remitos = DB::table('remitos')->where('users.id_user', $id_proveedor)->whereRaw('(`estado_pedido`.`estado_id_estado` < 4 or `estado_pedido`.`estado_id_estado` is null)')
        ->leftJoin('pedidos', 'pedidos.id_pedido', '=', 'remitos.pedido_id_pedido')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('pago_remito', 'pago_remito.remito_id_remito', '=', 'remitos.id_remito')
        ->select('remitos.created_at', 'remitos.monto', 'pedidos.tipo as tipo_pedido')
        ->selectRaw("`remitos`.`monto` - sum(ifnull(`pago_remito`.`monto`,0)) as 'monto_faltante', CONCAT(if(`pedidos`.`tipo` = 'Compra','REMITO ','NOTA DE CREDITO '), remitos.id_remito) as movimiento, CONCAT(if(`pedidos`.`tipo` = 'Compra','Ingreso','Egreso')) as tipo")
        ->groupBy('remitos.id_remito')->orderBy('remitos.created_at')
        ->havingRaw('(`remitos`.`monto` - sum(ifnull(`pago_remito`.`monto`,0))) > 0')->get();
        $item_pagos = 0;
        $item_remitos = 0;
        $total_egresos = 0;
        $total_ingresos = 0;
        // echo json_encode($pagos);
        // echo json_encode($remitos);
        // die;
        $array = array();
        while($item_pagos < count($pagos) || $item_remitos < count($remitos)){
            if($item_pagos == count($pagos)){
                array_push($array, $remitos[$item_remitos]);
                if($remitos[$item_remitos]->tipo_pedido == 'Compra'){
                    $total_egresos = $total_egresos + $remitos[$item_remitos]->monto_faltante;
                }else{
                    $total_ingresos = $total_ingresos + $remitos[$item_remitos]->monto_faltante;
                }
                $item_remitos++;
            }else if($item_remitos == count($remitos)){
                array_push($array, $pagos[$item_pagos]);
                $total_ingresos = $total_ingresos + $pagos[$item_pagos]->monto_faltante;
                $item_pagos++;
            }else{
                if($pagos[$item_pagos]->created_at < $remitos[$item_remitos]->created_at){
                    array_push($array, $pagos[$item_pagos]);
                    $total_ingresos = $total_ingresos + $pagos[$item_pagos]->monto_faltante;
                    $item_pagos++;
                }else{
                    array_push($array, $remitos[$item_remitos]);
                    if($remitos[$item_remitos]->tipo_pedido == 'Compra'){
                        $total_egresos = $total_egresos + $remitos[$item_remitos]->monto_faltante;
                    }else{
                        $total_ingresos = $total_ingresos + $remitos[$item_remitos]->monto_faltante;
                    }
                    $item_remitos++;
                }
            }
        }
        // echo json_encode(['array' =>  $array]);
        // die;
        return view('proveedores.cuenta', ['proveedor' => $proveedor, 'total_egresos' =>  $total_egresos, 'total_ingresos' =>  $total_ingresos, 'movimientos' =>  $array]);
    }

    public function cuentaampliado(Request $request, $id = null)
    {
        if(isset($id)){
            $id_proveedor = $id;
        }else{
            $id_proveedor = Auth::user()->id_user;
        }
        $proveedor = DB::table('users')->where('users.id_user', '=', $id_proveedor)->get()->first();
        $pagos = DB::table('pagos')->where('pagos.user_id_user', '=', $id_proveedor)->where('pagos.deleted', 0)
        ->leftJoin('users', 'pagos.user_id_user', '=', 'users.id_user')
        ->leftJoin('pago_remito', 'pago_remito.pago_id_pago', '=', 'pagos.id_pago')
        ->select('pagos.created_at', 'pagos.monto as monto_pesos')
        ->selectRaw("(-1*pagos.monto / pagos.conversion_dolar) as monto, (pagos.monto / pagos.conversion_dolar) + sum(ifnull(`pago_remito`.`monto`,0)) as 'monto_faltante', CONCAT('Egreso') as tipo, CONCAT('PAGO') as movimiento")
        ->groupBy('pagos.id_pago')->orderBy('pagos.created_at')
        // ->havingRaw('monto_faltante > 0')
        ->get();
        $remitos = DB::table('remitos')->where('users.id_user', $id_proveedor)->whereRaw('(`estado_pedido`.`estado_id_estado` < 4 or `estado_pedido`.`estado_id_estado` is null)')
        ->leftJoin('pedidos', 'pedidos.id_pedido', '=', 'remitos.pedido_id_pedido')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('pago_remito', 'pago_remito.remito_id_remito', '=', 'remitos.id_remito')
        ->select('remitos.created_at', 'remitos.monto', 'pedidos.tipo as tipo_pedido')
        ->selectRaw("`remitos`.`monto` - sum(ifnull(`pago_remito`.`monto`,0)) as 'monto_faltante', CONCAT(if(`pedidos`.`tipo` = 'Compra','REMITO ','NOTA DE CREDITO '), remitos.id_remito) as movimiento, CONCAT(if(`pedidos`.`tipo` = 'Compra','Ingreso','Egreso')) as tipo")
        ->groupBy('remitos.id_remito')->orderBy('remitos.created_at')
        // ->havingRaw('(`remitos`.`monto` - sum(ifnull(`pago_remito`.`monto`,0))) > 0')
        ->get();
        $item_pagos = 0;
        $item_remitos = 0;
        $total_egresos = 0;
        $total_ingresos = 0;
        // echo json_encode($pagos);
        // echo json_encode($remitos);
        // die;
        $array = array();
        while($item_pagos < count($pagos) || $item_remitos < count($remitos)){
            if($item_pagos == count($pagos)){
                array_push($array, $remitos[$item_remitos]);
                if($remitos[$item_remitos]->tipo_pedido == 'Compra'){
                    $total_egresos = $total_egresos + $remitos[$item_remitos]->monto;
                }else{
                    $total_ingresos = $total_ingresos + $remitos[$item_remitos]->monto;
                }
                $item_remitos++;
            }else if($item_remitos == count($remitos)){
                array_push($array, $pagos[$item_pagos]);
                $total_ingresos = $total_ingresos + $pagos[$item_pagos]->monto;
                $item_pagos++;
            }else{
                if($pagos[$item_pagos]->created_at < $remitos[$item_remitos]->created_at){
                    array_push($array, $pagos[$item_pagos]);
                    $total_ingresos = $total_ingresos + $pagos[$item_pagos]->monto;
                    $item_pagos++;
                }else{
                    array_push($array, $remitos[$item_remitos]);
                    if($remitos[$item_remitos]->tipo_pedido == 'Compra'){
                        $total_egresos = $total_egresos + $remitos[$item_remitos]->monto;
                    }else{
                        $total_ingresos = $total_ingresos + $remitos[$item_remitos]->monto;
                    }
                    $item_remitos++;
                }
            }
        }
        // echo json_encode(['proveedor' =>  $proveedor]);
        // die;
        return view('proveedores.cuentaampliado', ['proveedor' => $proveedor, 'total_egresos' =>  $total_egresos, 'total_ingresos' =>  $total_ingresos, 'movimientos' =>  $array]);
    }

    public function pagos()
    {
        $tipo_movimiento = TipoMovimiento::whereRaw("nombre = 'Compra productos'")->get()->first();
        $pagos = DB::table('pagos')->where('pagos.deleted', 0)
        ->where("tipo_movimiento_id", $tipo_movimiento ? $tipo_movimiento->id_tipo_movimiento : 2)
        ->leftJoin('users', 'pagos.user_id_user', '=', 'users.id_user')
        ->leftJoin('perfils', 'users.perfil_id', '=', 'perfils.id_perfil')
        ->leftJoin('pago_remito', 'pago_remito.pago_id_pago', '=', 'pagos.id_pago')
        ->select('pagos.*')
        ->selectRaw("GROUP_CONCAT(pago_remito.remito_id_remito separator ',') as remitos,GROUP_CONCAT(pago_remito.monto separator ',') as monto_remitos, users.name as name, sum(ifnull(`pago_remito`.`monto`,0)) as pagado")
        ->groupBy('pagos.id_pago')
        ->paginate(10);
        // echo json_encode($pagos);die;
        return view('proveedores.pagos', ['pagos' => $pagos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addpago()
    {
        $medios = DB::table('medios')->where('deleted', 0)->get();
        $proveedores = DB::table('users')->where('users.deleted', 0)->where('roles.name', '<>', 'admin')->where('roles.name', '<>', 'administrador')->where('roles.name', '=', 'Proveedor')
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->leftJoin('perfils', 'users.perfil_id', '=', 'perfils.id_perfil')
        ->select('users.id_user', 'roles.discount as descuento')
        ->selectRaw("users.name as name")->get();
        return view('proveedores.addpago', ['medios' => $medios, 'proveedores' => $proveedores]);
    }

    public function addpagosuccess(Request $request)
    {
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'user_id_user'=>'required',
            'medio_id_medio'=>'required',
            'monto'=>'required',
            'comprobante'=>'max:2048',
        );
        if(env('MANEJO_DOLARES', false)){
            $rules['conversion_dolar'] = 'required';
        }

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/proveedores/addpago')
                ->withErrors($validator);
                // return view('medios.crear');
        } else {
            $tipo_movimiento = TipoMovimiento::whereRaw("nombre = 'Compra productos'")->get()->first();
            $pago = new Pago([
                'user_id_user' => $request->get('user_id_user'),
                'medio_id_medio' => $request->get('medio_id_medio'),
                'monto' => -1 * $request->get('monto'),
                'descripcion' => $request->get('descripcion'),
                'estado' => 'PENDIENTE',
                'tipo_movimiento_id' => $tipo_movimiento ? $tipo_movimiento->id_tipo_movimiento : 2
            ]);
            if(env('MANEJO_DOLARES', false)){
                $pago->conversion_dolar = $request->get('conversion_dolar');
            }
            if($request->hasFile('comprobante')) {
                $fileUpload = $request->file('comprobante');
                $ext = $fileUpload->guessExtension();
                $filename = md5(date('Y-m-d H:i:s:u')).".".$ext;
                $pago->comprobante = $filename;
                $fileUpload->move('images/comprobantes/',$filename);
            }
            $pago->save();
            // redirect
            Session::flash('message', 'El pago se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/proveedores/pagos');
            // return view('marcas.index');
        }
    }

    public function asignarpago($id)
    {
        $pago = Pago::where('id_pago',$id)->where('pagos.deleted', 0)
        ->leftJoin('pago_remito', 'pago_remito.pago_id_pago', '=', 'pagos.id_pago')
        ->select('pagos.*')
        ->selectRaw('sum(ifnull(`pago_remito`.`monto`,0)) as pagado')
        ->groupBy('pagos.id_pago')
        ->get()->first();
        $remitos = DB::table('remitos')->where('users.id_user', $pago->user_id_user)->where('pedidos.tipo', '=', 'Compra')
        ->leftJoin('pedidos', 'pedidos.id_pedido', '=', 'remitos.pedido_id_pedido')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('pago_remito', 'pago_remito.remito_id_remito', '=', 'remitos.id_remito')
        ->select('remitos.*', 'users.name', 'users.surname')
        ->selectRaw('`remitos`.`monto` - sum(ifnull(`pago_remito`.`monto`,0)) as falta_pagar')
        ->groupBy('remitos.id_remito')
        ->orderBy('remitos.created_at')
        ->havingRaw('(`remitos`.`monto` - sum(ifnull(`pago_remito`.`monto`,0))) > 0')->get();
        return view('proveedores.asignarpago', ['pago' => $pago, 'remitos' => $remitos, 'id_proveedor' => $pago->user_id_user]);
    }

    public function asignarpagosuccess(Request $request, $id_pago)
    {
        $fields = $request->all();
        $monto_pago = $fields['monto_pago'];
        $id_proveedor = $fields['id_proveedor'];
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        // echo json_encode($fields);die;
        foreach($fields['items'] as $item){
            if($monto_pago > 0){
                if(isset($item['checkbox'])){
                    if($monto_pago >= $item['monto']){
                        $monto_a_pagar = $item['monto'];
                        $monto_pago = $monto_pago - $item['monto'];
                    }else{
                        $monto_a_pagar = $monto_pago;
                        $monto_pago = 0;
                    }
                    DB::table('pago_remito')->insert(
                        [
                        'pago_id_pago' => $id_pago, 
                        'remito_id_remito' => $item['id_remito'], 
                        'monto' => $monto_a_pagar, 
                        'created_at' =>  date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        ]
                    );
                }
            }else {
                break;
            }
        }
        $pago = Pago::where('id_pago',$id_pago)->get()->first();
        if($monto_pago > 0){
            $pago->estado = 'ASIGNADO PARCIAL';
        }else{
            $pago->estado = 'ASIGNADO';
        }
        $pago->save();
        Session::flash('message', 'El pago se asignó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/proveedores/cuentaampliado/'.$id_proveedor);
    }
    public function editarpago($id)
    {
        $pago = Pago::where('id_pago',$id)->get()->first();
        $medios = DB::table('medios')->where('deleted', 0)->get();
        $proveedores = DB::table('users')->where('users.deleted', 0)->where('roles.name', '<>', 'admin')->where('roles.name', '<>', 'administrador')->where('roles.name', '=', 'Proveedor')
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->leftJoin('perfils', 'users.perfil_id', '=', 'perfils.id_perfil')
        ->select('users.id_user', 'roles.discount as descuento')
        ->selectRaw("users.name as name")->get();
        return view('proveedores.editarpago', ['pago' => $pago, 'medios' => $medios, 'proveedores' => $proveedores]);
    }

    public function actualizarpago(Request $request, $id)
    {
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'user_id_user'=>'required',
            'medio_id_medio'=>'required',
            'monto'=>'required',
            'conversion_dolar'=>'required',
            'comprobante'=>'max:2048',
        );
        $fields = $request->all();
        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/proveedores/editarpago/'.$id)
                ->withErrors($validator);
                // return view('medios.crear');
        } else {
            $pago = Pago::where('id_pago',$id)->get()->first();
            $pago->user_id_user = $fields['user_id_user'];
            $pago->medio_id_medio = $fields['medio_id_medio'];
            $pago->monto = $fields['monto'];
            $pago->conversion_dolar = $fields['conversion_dolar'];
            $pago->descripcion = $fields['descripcion'];
            if($request->hasFile('comprobante')) {
                $fileUpload = $request->file('comprobante');
                $ext = $fileUpload->guessExtension();
                $filename = md5(date('Y-m-d H:i:s:u')).".".$ext;
                if($pago->comprobante != ''){
                    \File::delete(public_path('images/comprobantes/' . $pago->comprobante));
                }
                $pago->comprobante = $filename;
                $fileUpload->move('images/comprobantes/',$filename);
            }
            $pago->save();
            // redirect
            Session::flash('message', 'El pago se editó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/proveedores/pagos');
            // return view('marcas.index');
        }
    }


    public function borrarpago($id)
    {
        // delete
        $pago = Pago::find($id);
        $pago->deleted = 1;
        $pago->save();

        // redirect
        Session::flash('message', 'El pago se eliminó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/proveedores/pagos');
        // return view('pagos.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = DB::table('roles')->where('roles.deleted', 0)->where('roles.name', '=', 'Proveedor')->get();
        return view('proveedores.create', ['roles' => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // echo json_encode($request->all()['password']);die;
        $messages = [
            'required' => 'El :attribute es requerido',
            'unique'    => 'El :attribute ingresado ya esta en uso',
            'confirmed'    => 'Las contraseñas ingresadas no coinciden',
            'min'      => 'La :attribute debe tener como mínimo :min caracteres',
          ];
        $rules = array(
            'role_id'=>'required',
            'cuit'=>'required',
            'name'=>'required',
            'nombre_fantasia'=>'required',
            'responsabilidad'=>'required'
        );
        if(!empty($request->get('email'))) {
            $rules['email'] = 'unique:users';
        }
        if(!empty($request->get('password'))) {
            $rules['password'] = 'min:6|confirmed';
        }

        $validator = Validator::make($request->all(), $rules, $messages);


        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/proveedores/crear')
                ->withErrors($validator);
            // return view('proveedores.crear');
        } else {
            $id=DB::select("SHOW TABLE STATUS LIKE 'users'");
            $next_id=$id[0]->Auto_increment;
            $perfil = Perfil::where('nombre','Proveedor')->where('deleted', 0)->get()->first();
            $proveedor = new User([
                'name' => $request->get('name'),
                'surname' => $this->mepeoCategoriaIVAInverso($request->get('responsabilidad')),
                'nombre_fantasia' => $request->get('nombre_fantasia'),
                'cuit' => $request->get('cuit'),
                'numero_iibb' => $request->get('numero_iibb'),
                'telefono' => $request->get('telefono'),
                'telefono_movil' => $request->get('telefono_movil'),
                'email' => $request->get('email'),
                'perfil_id' => isset($perfil->id_perfil) ? $perfil->id_perfil : 5,
                'username' => substr('000000'.$next_id, -6),
                'responsabilidad' => $request->get('responsabilidad'),
                'password' => $request->get('password') === null ? Hash::make(substr('000000'.$next_id, -6)) : Hash::make($request->get('password'))
            ]);
            $proveedor->save();
            $proveedor->roles()->sync([$request->get('role_id')]);
            // redirect
            Session::flash('message', 'El proveedor se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/proveedores/index');
            // return view('proveedores.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = DB::table('users')->where('users.id_user', '=', $id)
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->select('users.*','roles.name as role')->get()->first();
        $direcciones = DB::table('direccions')->where('direccions.user_id', '=', $id)->get();
        return view('proveedores.show', ['user' => $user, 'direcciones' => $direcciones]);
    }
    
    public function suspend($id)
    {
        $user = User::where('id_user',$id)->get()->first();
        if($user->status){
            $user->status = 0;
        }else{
            $user->status = 1;
        }
        $user->update();
        // redirect
        Session::flash('message', 'Se actualizó el estado con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/proveedores/index');
        // return view('proveedores.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = DB::table('roles')->where('roles.deleted', 0)->where('roles.name', '=', 'Proveedor')->get();
        $user = User::where('id_user',$id)
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->select('users.*','role_user.role_id_role as id_role')->get()->first();
        return view('proveedores.edit', ['user' => $user, 'roles' => $roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $proveedor = User::where('id_user',$id)->get()->first();
        $messages = [
            'required' => 'El :attribute es requerido',
            'unique'    => 'El :attribute ingresado ya esta en uso',
            'confirmed'    => 'Las contraseñas ingresadas no coinciden',
            'min'      => 'La :attribute debe tener como mínimo :min caracteres',
          ];
        $rules = array(
            'role_id'=>'required',
            'cuit'=>'required',
            'name'=>'required',
            'nombre_fantasia'=>'required',
            'responsabilidad'=>'required'
        );
        $fields = $request->all();
        if(!empty($fields['email']) && $fields['email'] != $proveedor->email) {
            $rules['email'] = 'unique:users';
        }
        if(!empty($fields['password'])) {
            $rules['password'] = 'min:6|confirmed';
            $fields['password'] = Hash::make($request->get('password'));
        }else{
            $fields['password'] = Hash::make(substr('000000'.$proveedor->id_user, -6));
        }
        $fields['username'] = substr('000000'.$proveedor->id_user, -6);
        $validator = Validator::make($request->all(), $rules, $messages);


        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/proveedores/crear')
                ->withErrors($validator);
            // return view('proveedores.crear');
        } else {
            $fields['surname'] = $this->mepeoCategoriaIVAInverso($request->get('responsabilidad'));
            $proveedor->update($fields);
            $proveedor->roles()->sync([$request->get('role_id')]);
            // redirect
            Session::flash('message', 'El proveedor se actualizó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/proveedores/index');
            // return view('proveedores.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $user = User::find($id);
        $user->delete();
        $user->roles()->detach();

        // redirect
        Session::flash('message', 'El proveedor se eliminó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/proveedores/index');
        // return view('proveedores.index');
    }

    //Funnción que nos  convierte el entero de la razón social de la empresa en el string.
    public function mepeoCategoriaIVAInverso($categoriaIVA){
        switch ($categoriaIVA) {
            case 1:
                $razonSocialEmpresa = 'IVA Responsable Inscripto';
                break;
            case 2:
                $razonSocialEmpresa = 'IVA Responsable no Inscripto';
                break;
            case 3:
                $razonSocialEmpresa = 'IVA no Responsable';
                break;
            case 4:
                $razonSocialEmpresa = 'IVA Sujeto Exento';
                break;
            
            case 5:
                $razonSocialEmpresa = 'Consumidor Final';
                break;
            
            case 6:
                $razonSocialEmpresa = 'Responsable Monotributo';
                break;

            case 7:
                $razonSocialEmpresa = 'Sujeto no Categorizado';
                break;

            case 8:
                $razonSocialEmpresa = 'Proveedor del Exterior';
                break;

            case 9:
                $razonSocialEmpresa = 'Cliente del Exterior';
                break;
                
            case 10:
                $razonSocialEmpresa = ' IVA Liberado – Ley Nº 19.640';
                break;

            case 11:
                $razonSocialEmpresa = 'IVA Responsable Inscripto – Agente de Percepción';
                break;

            case 12:
                $razonSocialEmpresa = 'Pequeño Contribuyente Eventual';
                break;

            case 13:
                $razonSocialEmpresa = 'Monotributista Social';
                break;

            case 14:
                $razonSocialEmpresa = 'Pequeño Contribuyente Eventual Social';
                break;

            default:
                $razonSocialEmpresa = 'IVA Responsable Inscripto';
                break;
        }
        return $razonSocialEmpresa;
    }
}
