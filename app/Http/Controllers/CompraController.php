<?php

namespace App\Http\Controllers;

use PDF;
use Auth;
use App\Pedido;
use App\Remito;
use App\Estado;
use App\TipoMovimiento;
use App\Pago;
use App\Direccion;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PedidosExport;

class CompraController extends Controller
{
    public function getreporte(Request $request)
    {
        $fields = $request->all();
        $fecha_desde = $fields['fecha_desde'];
        $fecha_hasta = $fields['fecha_hasta'];
        return Excel::download(new PedidosExport('Compra', $fecha_desde, $fecha_hasta), 
        'Compras_'.date('d-m-Y', strtotime($fecha_desde)).'_'.date('d-m-Y', strtotime($fecha_hasta)).'.xlsx');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function todas(Request $request)
    {
        $fields = $request->all();
        $search = '';
        $fecha_desde = '';
        $fecha_hasta = '';
        $proveedorSelected = '';
        $estadoSelected = '';
        $proveedores = DB::table('users')->where('users.deleted', 0)->where('roles.name', '=', 'Proveedor')
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->select('users.*', 'roles.discount as descuento')->get();
        $estados = DB::table('estados')->get();
        $pedidos = DB::table('pedidos')->where('pedidos.tipo', '=', 'Compra');
        if(isset($fields['estado_id'])){
            $estadoSelected = $fields['estado_id'];
            if($estadoSelected == '-'){
                $pedidos = $pedidos->whereNull('estados.id_estado');
            }else{
                $pedidos = $pedidos->where('estados.id_estado', '=', $estadoSelected);
            }
        }
        if(isset($fields['user_id'])){
            $proveedorSelected = $fields['user_id'];
            $pedidos = $pedidos->where('users.id_user', '=', $proveedorSelected);
        }
        if(isset($fields['search'])){
            $search = $fields['search'];
            $pedidos = $pedidos->where('pedidos.id_pedido', 'like', '%' . $search . '%')
            ->orWhere('users.name', 'like', '%' . $search . '%')
            ->orWhere('users.surname', 'like', '%' . $search . '%');
        }
        if(isset($fields['fecha_desde'])){
            $fecha_desde = $fields['fecha_desde'];
            $pedidos = $pedidos->whereRaw("cast(pedidos.created_at as date) >= cast('".$fecha_desde."' as date)");
        }
        if(isset($fields['fecha_hasta'])){
            $fecha_hasta = $fields['fecha_hasta'];
            $pedidos = $pedidos->whereRaw("cast(pedidos.created_at as date) <= cast('".$fecha_hasta."' as date)");
        }
        $pedidos = $pedidos->leftJoin('pedido_producto', 'pedido_producto.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('remitos', 'remitos.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('users as proveedor', 'proveedor.id_user', '=', 'producto_user.user_id_user')
        ->leftJoin('users as creador', 'creador.id_user', '=', 'pedidos.usuario_crea_id')
        // ->leftJoin('estado_pedido', 'estado_pedido.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estado_pedido as R', function($join) {
            $join->on('pedidos.id_pedido', 'R.pedido_id_pedido')
                ->whereRaw('R.estado_id_estado = 2');
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->select('pedidos.*','remitos.id_remito','estados.descripcion_compra as estado', 'users.name', 'users.surname')
        ->selectRaw("ifnull(concat(creador.name, ' ', creador.surname), '') as creador, R.created_at as 'fecha_recibido',
        if(remitos.monto = '0.00' or remitos.id_remito is null, sum((pedido_producto.costo_unitario * pedido_producto.cantidad * 
        (1.00+((pedido_producto.iva)/100.00))) * (1-(pedido_producto.descuento/100))), remitos.monto) as monto, 
        if(remitos.monto_sin_iva = '0.00' or remitos.id_remito is null, sum((pedido_producto.costo_unitario * pedido_producto.cantidad) * (1-(pedido_producto.descuento/100))), remitos.monto_sin_iva) as monto_sin_iva")
        ->groupBy('pedidos.id_pedido')
        ->orderBy('pedidos.created_at', 'desc')
        ->paginate(10);
        // echo json_encode($pedidos);die;
        return view('compras.todas', ['search' =>  $search, 'fecha_desde' =>  $fecha_desde, 'fecha_hasta' =>  $fecha_hasta, 'pedidos' => $pedidos,
            'proveedores' => $proveedores,'proveedorSelected' => $proveedorSelected, 'estados' => $estados, 'estadoSelected' => $estadoSelected]);
    }

    public function pendientes(Request $request)
    {
        $fields = $request->all();
        $search = '';
        $proveedorSelected = '';
        $proveedores = DB::table('users')->where('users.deleted', 0)->where('roles.name', '=', 'Proveedor')
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->select('users.*', 'roles.discount as descuento')->get();
        $pedidos = DB::table('pedidos')->where('pedidos.tipo', '=', 'Compra');
        if(isset($fields['user_id'])){
            $proveedorSelected = $fields['user_id'];
            $pedidos = $pedidos->where('users.id_user', '=', $proveedorSelected);
        }
        if(isset($fields['search'])){
            $search = $fields['search'];
            $pedidos = $pedidos->where('pedidos.id_pedido', 'like', '%' . $search . '%')
            ->orWhere('users.name', 'like', '%' . $search . '%')
            ->orWhere('users.surname', 'like', '%' . $search . '%')
            ->orWhere('estados.descripcion_compra', 'like', '%' . $search . '%');
        }
        $pedidos = $pedidos->leftJoin('pedido_producto', 'pedido_producto.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('remitos', 'remitos.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('users as proveedor', 'proveedor.id_user', '=', 'producto_user.user_id_user')
        ->leftJoin('users as creador', 'creador.id_user', '=', 'pedidos.usuario_crea_id')
        // ->leftJoin('estado_pedido', 'estado_pedido.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->select('pedidos.*','remitos.id_remito','estados.descripcion_compra as estado','estado_pedido.estado_id_estado', 'users.name')
        ->selectRaw("ifnull(concat(creador.name, ' ', creador.surname), '') as creador,
        sum((pedido_producto.costo_unitario * pedido_producto.cantidad * (1.0+((pedido_producto.iva)/100.0))) 
        * (1-(pedido_producto.descuento/100))) as monto, 
        sum((pedido_producto.costo_unitario * pedido_producto.cantidad) * (1-(pedido_producto.descuento/100))) as monto_sin_iva")
        ->groupBy('pedidos.id_pedido')
        ->havingRaw('estado_id_estado is null or estado_id_estado = 1')
        ->paginate(10);
        return view('compras.pendientes', ['search' =>  $search, 'pedidos' => $pedidos, 'proveedores' => $proveedores, 'proveedorSelected' => $proveedorSelected]);
    }

    public function addordenpago(Request $request)
    {
        $fields = $request->all();
        // echo json_encode($fields);die;
        $id_pedido = $fields['id_pedido'];
        $id_remito = $fields['id_remito'];
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'id_pedido'=>'required',
            'id_remito'=>'required',
            'tipo'=>'required',
            'monto'=>'required',
            'fecha_emision'=>'required',
            'fecha_posible_pago'=>'required'
        );

        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/compras/ver/'.$id_pedido)
                ->withErrors($validator);
                // return view('medios.crear');
        } else {
            $ruta_imagen = '';
            if($request->hasFile('ruta_imagen')) {
                $fileUpload = $request->file('ruta_imagen');
                $ext = $fileUpload->guessExtension();
                $filename = md5(date('Y-m-d H:i:s:u')).".".$ext;
                $ruta_imagen = $filename;
                $fileUpload->move('images/ordenes_pago/',$filename);
            }
            DB::table('orden_pago')->insert(
                [
                'remito_id_remito' => $id_remito, 
                'tipo' => $fields['tipo'], 
                'monto' => $fields['monto'], 
                'fecha_emision' => $fields['fecha_emision'], 
                'fecha_posible_pago' => $fields['fecha_posible_pago'],
                'numero' => $fields['numero'],
                'observaciones' => $fields['observaciones'],
                'ruta_imagen' => $ruta_imagen,
                ]
            );
            // redirect
            Session::flash('message', 'La orden de pago se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/compras/ver/'.$id_pedido);
            // return view('marcas.index');
        }
    }

    public function deleteordenpago(Request $request, $id, $redirect,$id_pedido = null)
    {
         // delete
         DB::table('orden_pago')->where('id_orden_pago', $id)->delete();
 
         // redirect
         Session::flash('message', 'La orden de pago se eliminó con éxito!');
         Session::flash('type', 'success');
         return Redirect::to('admin/compras/'.$redirect."/".$id_pedido);
         // return view('pagos.index');
    }

    public function ordenespagos(Request $request)
    {
        $fields = $request->all();
        $fecha_desde = '';
        $fecha_hasta = '';
        $proveedorSelected = '';
        $estadoSelected = '';
        $tipoSelected = '';
        $proveedores = DB::table('users')->where('users.deleted', 0)->where('roles.name', '=', 'Proveedor')
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->select('users.*', 'roles.discount as descuento')->get();
        $ordenes = DB::table('orden_pago');
        if(isset($fields['estado_id'])){
            $estadoSelected = $fields['estado_id'];
            if($estadoSelected != '-'){
                $ordenes = $ordenes->where('orden_pago.pagado', $estadoSelected);
            }
        }else{
            $ordenes = $ordenes->where('orden_pago.pagado', 0);
        }
        if(isset($fields['user_id'])){
            $proveedorSelected = $fields['user_id'];
            $ordenes = $ordenes->where('users.id_user', '=', $proveedorSelected);
        }
        if(isset($fields['tipo']) && $fields['tipo'] != ''){
            $tipoSelected = $fields['tipo'];
            $ordenes = $ordenes->where('orden_pago.tipo', '=', $tipoSelected);
        }
        if(isset($fields['fecha_desde'])){
            $fecha_desde = $fields['fecha_desde'];
            $ordenes = $ordenes->whereRaw("cast(orden_pago.fecha_posible_pago as date) >= cast('".$fecha_desde."' as date)");
        }
        if(isset($fields['fecha_hasta'])){
            $fecha_hasta = $fields['fecha_hasta'];
            $ordenes = $ordenes->whereRaw("cast(orden_pago.fecha_posible_pago as date) <= cast('".$fecha_hasta."' as date)");
        }
        $ordenes = $ordenes
        ->join('remitos', 'remitos.id_remito', '=', 'orden_pago.remito_id_remito')
        ->join('pedidos', 'pedidos.id_pedido', '=', 'remitos.pedido_id_pedido')
        ->join('users', 'users.id_user', '=', 'pedidos.user_id_user');
        $monto_total = $ordenes->selectRaw('SUM(orden_pago.monto) as monto_total')->get()->first();
        $ordenes = $ordenes->select('orden_pago.*', 'users.name as proveedor', 'users.id_user', 'pedidos.id_pedido')
        ->orderBy('orden_pago.fecha_posible_pago')
        ->paginate(20);
        // echo json_encode($monto_total);die;
        $medios = DB::table('medios')->where('deleted', 0)->get();
        return view('compras.ordenespagos', ['monto_total' => $monto_total, 'cuentas' => $medios, 'fecha_desde' => $fecha_desde, 'fecha_hasta' =>  $fecha_hasta, 'ordenes' => $ordenes,
            'proveedores' => $proveedores,'proveedorSelected' => $proveedorSelected, 'estadoSelected' => $estadoSelected, 'tipoSelected' => $tipoSelected]);
    }

    public function pagarorden(Request $request)
    {
        $fields = $request->all();
        // echo json_encode($fields);die;
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'redirect_url'=>'required',
            'id_remito'=>'required',
            'id_pedido'=>'required',
            'id_user'=>'required',
            'id_orden'=>'required',
            'cuenta_id'=>'required',
            'monto'=>'required',
        );
        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to($fields['redirect_url'])
                ->withErrors($validator);
                // return view('marcas.crear');
        } else {
            $tipo_movimiento = TipoMovimiento::whereRaw("nombre = 'Compra productos'")->get()->first();
            $pago = new Pago([
                'user_id_user' => $fields['id_user'],
                'medio_id_medio' => $fields['cuenta_id'],
                'monto' => -1 * $fields['monto'],
                'descripcion' => $fields['descripcion'],
                'estado' => 'ASIGNADO',
                'tipo_movimiento_id' => $tipo_movimiento ? $tipo_movimiento->id_tipo_movimiento : 2, 
                'fecha' =>  date('Y-m-d')
            ]);
            $pago->save();
            DB::table('pago_remito')->insert(
                [
                'pago_id_pago' => $pago->id_pago, 
                'remito_id_remito' => $fields['id_remito'], 
                'monto' => $fields['monto'], 
                'created_at' =>  date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                ]
            );
            DB::table('orden_pago')->where('id_orden_pago', $fields['id_orden'])
            ->update(['fecha_efectivo_pago' => date('Y-m-d'), 'pagado' => 1]);
            Session::flash('message', 'La compra fue marcada como pagada!');
            // redirect
            Session::flash('type', 'success');
            return Redirect::to($fields['redirect_url']);
        }
    }

    public function miscompras(Request $request)
    {
        $fields = $request->all();
        $search = '';
        $pedidos = DB::table('pedidos')->where('pedidos.tipo', '=', 'Compra')->where('pedidos.user_id_user', '=', Auth::user()->id_user);
        if(isset($fields['search'])){
            $search = $fields['search'];
            $pedidos = $pedidos->where('pedidos.id_pedido', 'like', '%' . $search . '%')
            ->orWhere('users.name', 'like', '%' . $search . '%')
            ->orWhere('users.surname', 'like', '%' . $search . '%')
            ->orWhere('estados.descripcion_compra', 'like', '%' . $search . '%');
        }
        $pedidos = $pedidos->leftJoin('pedido_producto', 'pedido_producto.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        // ->leftJoin('estado_pedido', 'estado_pedido.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->select('pedidos.*', 'estados.descripcion_compra as estado', 'estados.id_estado', 'users.name', 'users.surname')
        ->selectRaw('sum((pedido_producto.costo_unitario * pedido_producto.cantidad) * (1-(pedido_producto.descuento/100))) as monto')
        ->groupBy('pedidos.id_pedido')
        // ->having('id_estado', '>', '1')
        ->paginate(10);
        return view('compras.miscompras', ['search' =>  $search, 'pedidos' => $pedidos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proveedores = DB::table('users')->where('users.deleted', 0)->where('roles.name', '=', 'Proveedor')
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->select('users.*', 'roles.discount as descuento')->get();
        return view('compras.create', ['proveedores' => $proveedores]);
    }

    public function createdevolucion()
    {
        $proveedores = DB::table('users')->where('users.deleted', 0)->where('roles.name', '=', 'Proveedor')
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->select('users.*', 'roles.discount as descuento')->get();
        return view('compras.createdevolucion', ['proveedores' => $proveedores]);
    }

    public function getproductoscomprados($user_id_user = null){
        $productos = DB::table('pedidos')->where('pedidos.user_id_user', $user_id_user)->where('pedidos.tipo', 'Compra')
        ->join('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->join('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->join('remitos', 'remitos.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->join('producto_remito', 'remitos.id_remito', '=', 'producto_remito.remito_id_remito')
        ->join('pedido_producto', 'pedido_producto.id_pedido_producto', '=', 'producto_remito.producto_id_producto')
        ->join('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->select('producto_remito.*', 'remitos.id_remito', 'pedidos.created_at as fecha_pedido', 'productos.id_producto', 'productos.nombre', 'productos.codigo', 'estado_pedido.estado_id_estado')
        ->selectRaw('sum(producto_remito.cantidad) as cantidad_total')
        ->havingRaw('estado_id_estado >= 2')
        ->groupBy('productos.id_producto')
        ->get();
        return $productos;
    }
    
    public function imprimir($id)
    {
        $pedido = DB::table('pedidos')->where('pedidos.id_pedido', $id)
        ->leftJoin('pedido_producto', 'pedido_producto.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('remitos', 'remitos.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('direccions', function($join){
            $join->on('direccions.user_id', '=', 'users.id_user')
            ->where('direccions.es_direccion_fact', '=', 1);
        })
        ->select('pedidos.*', 'remitos.id_remito', 'estados.descripcion_compra as estado', 'users.cuit', 'users.name', 'users.surname', 'direccions.direccion', 'direccions.localidad', 'direccions.provincia')
        ->selectRaw('sum(pedido_producto.costo_unitario * pedido_producto.cantidad) as monto_sin_desc, sum((pedido_producto.costo_unitario * pedido_producto.cantidad) * (1-(pedido_producto.descuento/100))) as monto_con_desc')
        ->groupBy('pedidos.id_pedido')
        ->get()->first();
        $productos = DB::table('pedido_producto')->where('pedido_producto.pedido_id_pedido', $id)
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('users', 'producto_user.user_id_user', '=', 'users.id_user')
        ->leftJoin(DB::raw("(SELECT `almacens`.`nombre`, `almacen_producto`.`producto_id_producto`, `almacen_producto`.`cantidad` FROM `almacen_producto`
        join `almacens` on `almacens`.`id_almacen` = `almacen_producto`.`almacen_id_almacen` and `almacens`.`nombre` <> 'Mercado Libre') as TEMP "),function($join){
           $join->on('productos.id_producto', '=', 'TEMP.producto_id_producto');
        })
        ->select('pedido_producto.*','productos.tipo','productos.unidad','productos.iva','productos.conversion','productos.nombre','productos.codigo', 'producto_user.codigo_proveedor')
        ->selectRaw('ifnull(sum(TEMP.cantidad),0) as stock')
        ->groupBy('pedido_producto.producto_id_producto')->get();
        // echo json_encode($productos);die;
        // return view('remitos.imprimir', ['remito' => $remito, 'productos' => $productos]);
        $pdf = PDF::loadView('compras.imprimir', ['pedido' => $pedido, 'productos' => $productos]);
        return $pdf->download('compra-'.$id.'.pdf');
    }

    public function faltante($id_pedido)
    {
        $pedido = DB::table('pedidos as P')->where('P.id_pedido', $id_pedido)
        ->leftJoin('users as U', 'U.id_user', '=', 'P.user_id_user')
        ->get()->first();
        $productos = DB::table('pedido_producto as PP')->where('PP.pedido_id_pedido', $id_pedido)
        ->leftJoin('producto_remito as PR', 'PP.id_pedido_producto', '=', 'PR.producto_id_producto')
        ->join('productos as P', 'P.id_producto', '=', 'PP.producto_id_producto')
        ->select('P.id_producto','P.codigo','P.nombre','PP.*')
        ->selectRaw('IF(ifnull(PP.cantidad,0) > ifnull(PR.cantidad,0), ifnull(PP.cantidad,0) - ifnull(PR.cantidad,0), 0) AS cantidad_faltante')
        ->havingRaw('cantidad_faltante > 0')->get();
        // echo json_encode($productos);die;
        return view('compras.faltante', ['productos' => $productos, 'pedido' => $pedido]);
    }

    public function excedente($id_pedido)
    {
        $pedido = DB::table('pedidos as P')->where('P.id_pedido', $id_pedido)
        ->leftJoin('users as U', 'U.id_user', '=', 'P.user_id_user')
        ->get()->first();
        $productos = DB::table('pedido_producto as PP')->where('PP.pedido_id_pedido', $id_pedido)
        ->leftJoin('producto_remito as PR', 'PP.id_pedido_producto', '=', 'PR.producto_id_producto')
        ->join('productos as P', 'P.id_producto', '=', 'PP.producto_id_producto')
        ->select('P.id_producto','P.codigo','P.nombre','PP.*')
        ->selectRaw('IF(ifnull(PP.cantidad,0) < ifnull(PR.cantidad,0), ifnull(PR.cantidad,0) - ifnull(PP.cantidad,0), 0) AS cantidad_excedente')
        ->havingRaw('cantidad_excedente > 0')->get();
        // echo json_encode($productos);die;
        return view('compras.excedente', ['productos' => $productos, 'pedido' => $pedido]);
    }

    public function generardevolucion(Request $request)
    {
        $fields = $request->all();
        // echo json_encode($fields);
        // die;
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'user_id_user'=>'required',
            'motivo_devolucion'=>'required',
            'items'=>'required'
        );

        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/devoluciones/crear')
                ->withErrors($validator);
                // return view('marcas.crear');
        } else {
            $direccion = DB::table('direccions')->where('user_id', $request->get('user_id_user'))->where('es_direccion_fact', 1)->get()->first();
            $pedido = new Pedido([
                'tipo' => 'Devolucion compra',
                'user_id_user' => $request->get('user_id_user'),
                'motivo_devolucion' => $request->get('motivo_devolucion'),
                'descripcion' => $request->get('descripcion'),
                'direccion_id_direccion' => $direccion ? $direccion->id_direccion : 1,
                'usuario_crea_id' => Auth::user()->id_user,
                'created_at' =>  date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
            $pedido->save();
            $pedido->estados()->sync(['estado_id_estado' => 2], false);
            foreach($request->get('items') as $item){
                DB::table('pedido_producto')->insert(
                    [
                    'pedido_id_pedido' => $pedido->id_pedido, 
                    'producto_id_producto' => $item['id'], 
                    'cantidad' => $item['cantidad'], 
                    'descuento' => 0, 
                    'costo_unitario' => $item['costo_unitario'],
                    'created_at' =>  date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    ]
                );
            }
            // redirect
            Session::flash('message', 'La solicitud de devolución de compra se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/compras/verdevolucion/'.$pedido->id_pedido);
        }
    }

    public function aceptar($id_pedido)
    {
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $pedido = DB::table('pedidos')->where('pedidos.tipo', '=', 'Devolucion compra')
        ->where('pedidos.id_pedido', $id_pedido)->get()->first();
        $remito = new Remito([
            'pedido_id_pedido' => $id_pedido
        ]);
        $productos = DB::table('pedido_producto')->where('pedido_producto.pedido_id_pedido', $id_pedido)
        ->join('productos', 'productos.id_producto', 'pedido_producto.producto_id_producto')
        ->select('pedido_producto.*','productos.tipo','productos.unidad','productos.conversion')->get();
        $remito->save();
        $totalMontoRemito = 0;
        foreach($productos as $producto){
            $totalMontoRemito = $totalMontoRemito + ($producto->cantidad * $producto->costo_unitario);
            DB::table('producto_remito')->insert(
                [
                'remito_id_remito' => $remito->id_remito, 
                'producto_id_producto' => $producto->id_pedido_producto, 
                'cantidad' => $producto->cantidad, 
                'descuento' => $producto->descuento, 
                'costo_unitario' => $producto->costo_unitario,
                'precio_unitario' => $producto->precio_unitario,
                'created_at' =>  date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                ]
            );
            // if($pedido->motivo_devolucion == 'error'){
            //     $almacen_producto = DB::table('almacen_producto')->where('producto_id_producto', $producto->producto_id_producto)->where('almacen_id_almacen', 1)
            //         ->increment('cantidad', $producto->cantidad);
            // }
        }
        $remito->update(['monto' => $totalMontoRemito]);
        DB::table('estado_pedido')->insert(
            [
            'pedido_id_pedido' => $id_pedido, 
            'estado_id_estado' => 3, 
            'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            ]
        );
        // redirect
        Session::flash('message', 'El pedido de devolución de compra fue marcado como aceptado!');
        Session::flash('type', 'success');
        return Redirect::to('admin/compras/verdevolucion/'.$id_pedido);
    }

    public function rechazar($id_pedido)
    {
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        DB::table('estado_pedido')->insert(
            [
            'pedido_id_pedido' => $id_pedido, 
            'estado_id_estado' => 4, 
            'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            ]
        );
        // redirect
        Session::flash('message', 'El pedido de devolución de compra fue marcado como rechazado!');
        Session::flash('type', 'success');
        return Redirect::to('admin/compras/verdevolucion/'.$id_pedido);
    }

    public function verdevolucion($id)
    {
        $pedido = DB::table('pedidos')->where('pedidos.id_pedido', $id)
        ->leftJoin('pedido_producto', 'pedido_producto.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('remitos', 'remitos.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('direccions', 'direccions.id_direccion', '=', 'pedidos.direccion_id_direccion')
        ->select('pedidos.*', 'remitos.id_remito', 'estados.descripcion as estado', 'users.name', 'users.surname', 'direccions.direccion', 'direccions.localidad', 'direccions.provincia')
        ->selectRaw('sum(pedido_producto.costo_unitario * pedido_producto.cantidad) as monto_sin_desc, sum((pedido_producto.costo_unitario * pedido_producto.cantidad) * (1-(pedido_producto.descuento/100))) as monto_con_desc')
        ->groupBy('pedidos.id_pedido')
        ->get()->first();
        $productos = DB::table('pedido_producto')->where('pedido_producto.pedido_id_pedido', $id)
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('users', 'producto_user.user_id_user', '=', 'users.id_user')
        ->leftJoin(DB::raw("(SELECT `almacens`.`nombre`, `almacen_producto`.`producto_id_producto`, `almacen_producto`.`cantidad` FROM `almacen_producto`
        join `almacens` on `almacens`.`id_almacen` = `almacen_producto`.`almacen_id_almacen` and `almacens`.`nombre` <> 'Mercado Libre') as TEMP "),function($join){
           $join->on('productos.id_producto', '=', 'TEMP.producto_id_producto');
        })
        ->select('pedido_producto.*','productos.nombre','productos.codigo', 'users.valor_dolar')
        ->selectRaw('ifnull(sum(TEMP.cantidad),0) as stock')
        ->groupBy('pedido_producto.producto_id_producto')->get();
        $estados = DB::table('estados')->get();
        return view('compras.verdevolucion', ['pedido' => $pedido, 'productos' => $productos, 'estados' => $estados]);
    }

    public function devoluciones(Request $request)
    {
        $fields = $request->all();
        $search = '';
        $pedidos = DB::table('pedidos')->where('pedidos.tipo', '=', 'Devolucion compra');
        if(isset($fields['search'])){
            $search = $fields['search'];
            $pedidos = $pedidos->where('pedidos.id_pedido', 'like', '%' . $search . '%')
            ->orWhere('users.name', 'like', '%' . $search . '%')
            ->orWhere('users.surname', 'like', '%' . $search . '%')
            ->orWhere('estados.descripcion', 'like', '%' . $search . '%');
        }
        $pedidos = $pedidos->leftJoin('pedido_producto', 'pedido_producto.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('remitos', 'remitos.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('users as proveedor', 'proveedor.id_user', '=', 'producto_user.user_id_user')
        // ->leftJoin('estado_pedido', 'estado_pedido.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->select('pedidos.*','remitos.id_remito','estados.descripcion as estado', 'users.name', 'users.surname')
        ->selectRaw('sum((pedido_producto.costo_unitario * pedido_producto.cantidad) * (1-(pedido_producto.descuento/100))) as monto')
        ->groupBy('pedidos.id_pedido')
        ->paginate(10);
        return view('compras.devoluciones', ['search' =>  $search, 'pedidos' => $pedidos]);
    }

    public function direccionesproveedor($user_id_user = null)
    {
        if(!empty($user_id_user)){
            $direcciones = Direccion::where('user_id',$user_id_user)->orderBy('es_direccion_fact', 'desc')->get();
        }else{
            $direcciones = Direccion::all();
        }
        return $direcciones;
    }

    public function createcliente()
    {
        $proveedor = Auth::user();
        $productos = DB::table('pedidos')->where('pedidos.user_id_user', $proveedor->id_user)->where('pedidos.tipo', 'Pedido')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('remitos', 'remitos.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('producto_remito', 'remitos.id_remito', '=', 'producto_remito.remito_id_remito')
        ->leftJoin('pedido_producto', 'pedido_producto.id_pedido_producto', '=', 'producto_remito.producto_id_producto')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->select('producto_remito.*', 'remitos.id_remito', 'pedidos.created_at as fecha_pedido', 'productos.id_producto', 'productos.nombre', 'productos.codigo', 'estado_pedido.estado_id_estado')
        ->havingRaw('estado_id_estado = 3')
        ->get();
        return view('compras.createcliente', ['productos' => $productos, 'proveedor' => $proveedor]);
    }

    public function getproductos($user_id_user = null){
        $productos = DB::table('productos')->where('productos.deleted', 0)->where('productos.estado', 1)->where('producto_user.user_id_user', $user_id_user)
        ->where(function($q){
            $q->where('productos.tipo', 'Producto')
                ->orWhere('productos.tipo', 'Insumo');
         })
        ->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto');
        })
        ->leftJoin('users', 'producto_user.user_id_user', '=', 'users.id_user')
        ->select('productos.*', 'producto_user.costo_dolares', 'users.valor_dolar')->get();
        return $productos;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->all();
        // echo json_encode($fields);
        // die;
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'user_id_user'=>'required',
            'items'=>'required'
        );

        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/compras/crear')
                ->withErrors($validator);
                // return view('marcas.crear');
        } else {
            $direccion = DB::table('direccions')->where('user_id', $request->get('user_id_user'))->where('es_direccion_fact', 1)->get()->first();
            $pedido = new Pedido([
                'tipo' => 'Compra',
                'user_id_user' => $request->get('user_id_user'),
                'direccion_id_direccion' => $direccion ? $direccion->id_direccion : 1,
                'descripcion' => $request->get('descripcion'),
                'fecha_estimada_recepcion' => $request->get('fecha_estimada_recepcion'),
                'observaciones_publicas' => $request->get('observaciones_publicas'),
                'usuario_crea_id' => Auth::user()->id_user,
                'created_at' =>  date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
            $pedido->save();
            foreach($request->get('items') as $item){
                $producto = DB::table('productos')->where('id_producto', $item['id'])->get()->first();
                if($producto->tipo == 'Insumo'){
                    $cantidad = $item['cantidad'];
                }else{
                    $cantidad = $item['cantidad'];
                }
                DB::table('pedido_producto')->insert(
                    [
                    'pedido_id_pedido' => $pedido->id_pedido, 
                    'producto_id_producto' => $item['id'], 
                    'cantidad' => $cantidad, 
                    'iva' => $item['iva'], 
                    'descuento' => 0, 
                    'costo_unitario' => $item['costo_unitario'],
                    'created_at' =>  date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    ]
                );
            }
            // redirect
            Session::flash('message', 'La compra se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/compras/ver/'.$pedido->id_pedido);
        }
    }

    public function storecliente(Request $request)
    {
        $fields = $request->all();
        // echo json_encode($fields);
        // die;
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'user_id_user'=>'required',
            'items'=>'required'
        );

        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('compras/createcliente')
                ->withErrors($validator);
                // return view('marcas.crear');
        } else {
            $direccion = DB::table('direccions')->where('user_id', $request->get('user_id_user'))->where('es_direccion_fact', 1)->get()->first();
            $pedido = new Pedido([
                'tipo' => 'Compra',
                'user_id_user' => $request->get('user_id_user'),
                'direccion_id_direccion' => $direccion ? $direccion->id_direccion : 1,
                'usuario_crea_id' => Auth::user()->id_user,
                'created_at' =>  date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
            $pedido->save();
            foreach($request->get('items') as $item){
                DB::table('pedido_producto')->insert(
                    [
                    'pedido_id_pedido' => $pedido->id_pedido, 
                    'producto_id_producto' => $item['id'], 
                    'cantidad' => $item['cantidad'], 
                    'descuento' => 0, 
                    'costo_unitario' => $item['costo_unitario'],
                    'created_at' =>  date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    ]
                );
            }
            // redirect
            Session::flash('message', 'La compra se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('miscompras');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pedido  $pedido
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pedido = DB::table('pedidos')->where('pedidos.id_pedido', $id)
        ->leftJoin('pedido_producto', 'pedido_producto.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('remitos', 'remitos.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin(DB::raw("(SELECT remito_id_remito, ifnull(sum(monto),0) as monto FROM pago_remito
                group by remito_id_remito) as PAGOS"), 'PAGOS.remito_id_remito', '=', 'remitos.id_remito')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('users as creador', 'creador.id_user', '=', 'pedidos.usuario_crea_id')
        ->leftJoin('direccions', 'direccions.id_direccion', '=', 'pedidos.direccion_id_direccion')
        ->select('pedidos.*', 'remitos.id_remito', 'estados.descripcion_compra as estado', 'users.valor_dolar', 'users.name', 'users.surname', 'direccions.direccion', 
        'direccions.localidad', 'direccions.provincia')
        ->selectRaw("ifnull(concat(creador.name, ' ', creador.surname), '') as creador, 
        if(remitos.monto = '0.00' or remitos.id_remito is null, sum((pedido_producto.costo_unitario * (1.0+((pedido_producto.iva)/100.0))
        * pedido_producto.cantidad) * (1-(pedido_producto.descuento/100))), remitos.monto) as monto_con_desc, 
        ifnull(PAGOS.monto,0) as monto_pagos")
        ->groupBy('pedidos.id_pedido')
        ->get()->first();
        // echo json_encode($pedido);die;
        $productos = DB::table('pedido_producto')->where('pedido_producto.pedido_id_pedido', $id)
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('users', 'producto_user.user_id_user', '=', 'users.id_user')
        ->leftJoin(DB::raw("(SELECT `almacens`.`nombre`, `almacen_producto`.`producto_id_producto`, `almacen_producto`.`cantidad` FROM `almacen_producto`
        join `almacens` on `almacens`.`id_almacen` = `almacen_producto`.`almacen_id_almacen` and `almacens`.`nombre` <> 'Mercado Libre') as TEMP "),function($join){
           $join->on('productos.id_producto', '=', 'TEMP.producto_id_producto');
        })
        ->select('pedido_producto.*','productos.tipo','productos.unidad','productos.conversion','productos.nombre','productos.codigo', 'users.valor_dolar')
        ->selectRaw('ifnull(sum(TEMP.cantidad),0) as stock')
        ->groupBy('pedido_producto.producto_id_producto')->get();
        // echo json_encode($productos);die;
        $hay_excedente = false;
        $hay_faltante = false;
        if($pedido->estado == 'Recibida' || $pedido->estado == 'Pagada'){
            $productos_remito = DB::table('remitos as R')->where('PP.pedido_id_pedido', $id)
            ->leftJoin('producto_remito as PR', 'PR.remito_id_remito', '=', 'R.id_remito')
            ->rightJoin('pedido_producto as PP', 'PR.producto_id_producto', '=', 'PP.id_pedido_producto')
            ->leftJoin('productos as P', 'P.id_producto', '=', 'PP.producto_id_producto')
            ->join('almacen_producto as AP', 'AP.producto_id_producto', '=', 'PP.producto_id_producto')
            ->select('PR.*','P.nombre','P.codigo','P.tipo','P.unidad','P.conversion')
            ->selectRaw("IF(ifnull(PP.cantidad,0) > ifnull(PR.cantidad,0), 'F', IF(ifnull(PP.cantidad,0) < ifnull(PR.cantidad,0), 'E', '-')) AS 'estado'")
            ->groupBy('PP.producto_id_producto')->get();
            foreach($productos_remito as $producto){
                switch($producto->estado){
                    case 'E':
                    $hay_excedente = true;
                    break;
                    case 'F':
                    $hay_faltante = true;
                    break;
                }
            }
            // echo json_encode($productos_remito);die;
        }else{
            $productos_remito = null;
        }
        $estados = DB::table('estados')->get();
        $medios = DB::table('medios')->where('deleted', 0)->get();
        $ordenes = DB::table('orden_pago')->where('remitos.pedido_id_pedido', $id)
        ->join('remitos', 'remitos.id_remito', '=', 'orden_pago.remito_id_remito')
        ->join('pedidos', 'pedidos.id_pedido', '=', 'remitos.pedido_id_pedido')
        ->join('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->select('orden_pago.*', 'users.name as proveedor')
        ->groupBy('orden_pago.fecha_posible_pago')->get();
        return view('compras.show', ['ordenes' => $ordenes, 'cuentas' => $medios, 'hay_excedente' => $hay_excedente, 
        'hay_faltante' => $hay_faltante, 'pedido' => $pedido, 'productos_remito' => $productos_remito, 'productos' => $productos, 
        'estados' => $estados]);
    }

    public function micompra($id)
    {
        $pedido = DB::table('pedidos')->where('pedidos.id_pedido', $id)
        ->leftJoin('pedido_producto', 'pedido_producto.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('direccions', 'direccions.id_direccion', '=', 'pedidos.direccion_id_direccion')
        ->select('pedidos.*','estados.descripcion_compra as estado', 'users.name', 'users.surname', 'direccions.direccion', 'direccions.localidad', 'direccions.provincia')
        ->selectRaw('sum(pedido_producto.costo_unitario * pedido_producto.cantidad) as monto_sin_desc, sum((pedido_producto.costo_unitario * pedido_producto.cantidad) * (1-(pedido_producto.descuento/100))) as monto_con_desc')
        ->groupBy('pedidos.id_pedido')
        ->get()->first();
        $productos = DB::table('pedido_producto')->where('pedido_producto.pedido_id_pedido', $id)
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin(DB::raw("(SELECT `almacens`.`nombre`, `almacen_producto`.`producto_id_producto`, `almacen_producto`.`cantidad` FROM `almacen_producto`
        join `almacens` on `almacens`.`id_almacen` = `almacen_producto`.`almacen_id_almacen` and `almacens`.`nombre` <> 'Mercado Libre') as TEMP "),function($join){
           $join->on('productos.id_producto', '=', 'TEMP.producto_id_producto');
        })
        // ->join('almacen_producto', 'almacen_producto.producto_id_producto', '=', 'productos.id_producto')
        // // ->leftJoin('almacens', 'almacens.id_almacen', '=', 'almacen_producto.almacen_id_almacen')
        // ->join('almacens', function($join) {
        //     $join->on('almacens.id_almacen', 'almacen_producto.almacen_id_almacen')
        //         ->where('almacens.nombre', '<>', "Mercado Libre");
        // })
        ->select('pedido_producto.*','productos.nombre','productos.codigo')
        ->selectRaw('ifnull(sum(TEMP.cantidad),0) as stock')
        ->groupBy('pedido_producto.producto_id_producto')->get();
        $estados = DB::table('estados')->get();
        return view('compras.micompra', ['pedido' => $pedido, 'productos' => $productos, 'estados' => $estados]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pedido  $pedido
     * @return \Illuminate\Http\Response
     */
    public function notadecredito($id)
    {
        $pedido = DB::table('pedidos')->where('pedidos.tipo', '=', 'Compra')->where('pedidos.id_pedido', $id)
        ->leftJoin('pedido_producto', 'pedido_producto.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('direccions', 'direccions.id_direccion', '=', 'pedidos.direccion_id_direccion')
        ->select('pedidos.*','estados.descripcion_compra as estado', 'users.name', 'users.surname', 'direccions.id_direccion', 'direccions.direccion', 'direccions.localidad', 'direccions.provincia')
        ->selectRaw('sum(pedido_producto.costo_unitario * pedido_producto.cantidad) as monto_sin_desc, sum((pedido_producto.costo_unitario * pedido_producto.cantidad) * (1-(pedido_producto.descuento/100))) as monto_con_desc')
        ->groupBy('pedidos.id_pedido')
        ->get()->first();
        $direcciones = DB::table('direccions')->where('direccions.user_id', $pedido->user_id_user)->get();
        $productos = DB::table('pedido_producto')->where('pedido_producto.pedido_id_pedido', $id)
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('users', 'producto_user.user_id_user', '=', 'users.id_user')
        ->join('almacen_producto', 'almacen_producto.producto_id_producto', '=', 'productos.id_producto')
        // ->leftJoin('almacens', 'almacens.id_almacen', '=', 'almacen_producto.almacen_id_almacen')
        ->join('almacens', function($join) {
            $join->on('almacens.id_almacen', 'almacen_producto.almacen_id_almacen')
                ->where('almacens.nombre', '<>', "Mercado Libre");
        })
        ->select('pedido_producto.*','productos.id_producto','productos.margen','productos.nombre','productos.codigo','users.valor_dolar', 'producto_user.costo_dolares')
        ->selectRaw('sum(almacen_producto.cantidad) as stock')
        ->groupBy('pedido_producto.producto_id_producto')->get();
        return view('compras.notadecredito', ['pedido' => $pedido, 'productos' => $productos, 'direcciones' => $direcciones]);
    }

    public function aprobar($id_pedido)
    {
        $compra = Pedido::find($id_pedido);
        $compra->created_at = date('Y-m-d H:i:s');
        $compra->save();
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        DB::table('estado_pedido')->insert(
            [
            'pedido_id_pedido' => $id_pedido, 
            'estado_id_estado' => 1, 
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            ]
        );
        $remito = new Remito([
            'pedido_id_pedido' => $id_pedido,
        ]);
        $remito->save();
        // redirect
        Session::flash('message', 'La compra fue aprobada!');
        Session::flash('type', 'success');
        return Redirect::to('admin/compras/ver/'.$id_pedido);
    }

    public function addarchivo($id_pedido, Request $request)
    {
        $fields = $request->all();
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'archivo'=>'max:2048|required',
        );

        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/compras/ver/'.$id_pedido)
                ->withErrors($validator);
                // return view('medios.crear');
        } else {
            $ruta_archivo = '';
            if($request->hasFile('archivo')) {
                $fileUpload = $request->file('archivo');
                $ext = $fileUpload->guessExtension();
                $filename = md5(date('Y-m-d H:i:s:u')).".".$ext;
                $ruta_archivo = $filename;
                $fileUpload->move('images/compras/',$filename);
            }
            DB::table('pedidos')->where('id_pedido', $id_pedido)
            ->update(['ruta_archivo' => $ruta_archivo]);
            // redirect
            Session::flash('message', 'El archivo se adjuntó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/compras/ver/'.$id_pedido);
            // return view('marcas.index');
        }
    }

    public function deletearchivo($id_pedido)
    {
        // echo public_path();die;
        $compra = Pedido::find($id_pedido);
        unlink(public_path()."/images/compras/" . $compra->ruta_archivo);
        $compra->ruta_archivo = null;
        $compra->save();

        // redirect
        Session::flash('message', 'El archivo se eliminó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/compras/ver/'.$id_pedido);
    }

    public function recibir($id)
    {
        $pedido = DB::table('pedidos')->where('pedidos.tipo', '=', 'Compra')->where('pedidos.id_pedido', $id)
        ->leftJoin('pedido_producto', 'pedido_producto.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('direccions', 'direccions.id_direccion', '=', 'pedidos.direccion_id_direccion')
        ->select('pedidos.*','estados.nombre as estado', 'users.name', 'users.surname', 'direccions.id_direccion', 'direccions.direccion', 'direccions.localidad', 'direccions.provincia')
        ->selectRaw('sum(pedido_producto.costo_unitario * pedido_producto.cantidad) as monto_sin_desc, sum((pedido_producto.costo_unitario * pedido_producto.cantidad) * (1-(pedido_producto.descuento/100))) as monto_con_desc')
        ->groupBy('pedidos.id_pedido')
        ->get()->first();
        $direcciones = DB::table('direccions')->where('direccions.user_id', $pedido->user_id_user)->get();
        $productos = DB::table('pedido_producto')->where('pedido_producto.pedido_id_pedido', $id)
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->join('almacen_producto', 'almacen_producto.producto_id_producto', '=', 'productos.id_producto')
        // ->leftJoin('almacens', 'almacens.id_almacen', '=', 'almacen_producto.almacen_id_almacen')
        ->join('almacens', function($join) {
            $join->on('almacens.id_almacen', 'almacen_producto.almacen_id_almacen')
                ->where('almacens.nombre', '<>', "Mercado Libre");
        })
        ->select('pedido_producto.*','productos.tipo','productos.unidad','productos.conversion','productos.id_producto','productos.nombre','productos.codigo')
        ->selectRaw('sum(almacen_producto.cantidad) as stock')
        ->groupBy('pedido_producto.producto_id_producto')->get();
        $almacenes = DB::table('almacens')->where('deleted', 0)->get();
        return view('compras.recibir', ['pedido' => $pedido, 'productos' => $productos, 'direcciones' => $direcciones, 'almacenes' => $almacenes]);
    }

    public function recibirsuccess($id_pedido, Request $request)
    {
        $fields = $request->all();
        // echo json_encode($request->all());
        // die;
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'id_pedido'=>'required',
            'items'=>'required',
            'user_id_user'=>'required'
        );
        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/pedidos/remito/'.$fields['id_pedido'])
                ->withErrors($validator);
                // return view('marcas.crear');
        } else {
            // $remito = new Remito([
            //     'pedido_id_pedido' => $id_pedido,
            //     'descripcion' => $fields['descripcion'],
            // ]);
            // $remito->save();
            $remito = Remito::where("pedido_id_pedido", $id_pedido)->get()->first();
            $remito->descripcion = $fields['descripcion'];
            $remito->save();
            $totalMontoRemito = 0;
            $totalMontoRemitoSinIva = 0;
            foreach($fields['items'] as $producto){
                $totalMontoRemito = $totalMontoRemito + ($producto['cantidad'] * $producto['costo_unitario'] * (1.0+($producto['iva']/100.0)));
                $totalMontoRemitoSinIva = $totalMontoRemitoSinIva + ($producto['cantidad'] * $producto['costo_unitario']);
                if($producto['cantidad'] > 0){
                    DB::table('producto_remito')->insert(
                        [
                        'remito_id_remito' => $remito->id_remito, 
                        'producto_id_producto' => $producto['id'], 
                        'cantidad' => $producto['cantidad'], 
                        'almacen_id_almacen' => $producto['almacen_id'], 
                        'costo_unitario' => $producto['costo_unitario'],
                        'iva' => $producto['iva'],
                        'created_at' =>  date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        ]
                    );
                }
                if(env('COSTO_VARIABLE_COMPRA', false)){
                    $precio = $producto['costo_unitario'];
                    DB::table('producto_user')->where('user_id_user', $fields['user_id_user'])
                    ->where('producto_id_producto', $producto['id_producto'])
                    ->update(['costo_dolares' => $precio]);
                }
            }
            $pagos = DB::table('pago_remito')->where('remito_id_remito', $remito->id_remito)
            ->selectRaw('sum(monto) as monto')->groupBy('remito_id_remito')->get()->first();
            $remito->update(['monto' => $totalMontoRemito, 'monto_sin_iva' => $totalMontoRemitoSinIva]);
            $monto_pagos = $pagos ? $pagos->monto : 0;
            if($remito->monto <= $monto_pagos){
                DB::table('estado_pedido')->insert(
                    [
                    'pedido_id_pedido' => $id_pedido, 
                    'estado_id_estado' => 3, 
                    'created_at' =>  date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    ]
                );
            }else{
                DB::table('estado_pedido')->insert(
                    [
                    'pedido_id_pedido' => $id_pedido, 
                    'estado_id_estado' => 2, 
                    'created_at' =>  date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    ]
                );
            }
            // redirect
            Session::flash('message', 'La compra fue marcada como recibida!');
            Session::flash('type', 'success');
            return Redirect::to('admin/compras/ver/'.$id_pedido);
        }
    }

    public function pagar(Request $request)
    {
        $fields = $request->all();
        // echo json_encode($fields);die;
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'redirect_url'=>'required',
            'id_pedido'=>'required',
            'id_remito'=>'required',
            'id_user'=>'required',
            'cuenta_id'=>'required',
            'monto'=>'required',
            'monto_total'=>'required',
        );
        if(env('MANEJO_DOLARES', false)){
            $rules['conversion_dolar'] = 'required';
            $rules['monto_dolares'] = 'required';
        }
        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to($fields['redirect_url'])
                ->withErrors($validator);
                // return view('marcas.crear');
        } else {
            $tipo_movimiento = TipoMovimiento::whereRaw("nombre = 'Compra productos'")->get()->first();
            $pago = new Pago([
                'user_id_user' => $fields['id_user'],
                'medio_id_medio' => $fields['cuenta_id'],
                'monto' => -1 * $fields['monto'],
                'descripcion' => $fields['descripcion'],
                'estado' => 'ASIGNADO',
                'tipo_movimiento_id' => $tipo_movimiento ? $tipo_movimiento->id_tipo_movimiento : 2, 
                'fecha' =>  date('Y-m-d')
            ]);
            if(env('MANEJO_DOLARES', false)){
                $pago->conversion_dolar = $fields['conversion_dolar'];
                $monto = 'monto_dolares';
            }else{
                $monto = 'monto';
            }
            $pago->save();
            DB::table('pago_remito')->insert(
                [
                'pago_id_pago' => $pago->id_pago, 
                'remito_id_remito' => $fields['id_remito'], 
                'monto' => $fields[$monto], 
                'created_at' =>  date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                ]
            );
            if($fields['monto'] == $fields['monto_total']){
                DB::table('estado_pedido')->insert(
                    [
                    'pedido_id_pedido' => $fields['id_pedido'], 
                    'estado_id_estado' => 3, 
                    'created_at' =>  date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    ]
                );
                Session::flash('message', 'La compra fue marcada como pagada!');
            }else{
                Session::flash('message', 'El pago fue registrado con éxito!');
            }
            // redirect
            Session::flash('type', 'success');
            return Redirect::to($fields['redirect_url']);
        }
    }

    public function cancelar($id_pedido)
    {
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        DB::table('estado_pedido')->insert(
            [
            'pedido_id_pedido' => $id_pedido, 
            'estado_id_estado' => 4, 
            'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            ]
        );
        // redirect
        Session::flash('message', 'La compra fue marcado como cancelada!');
        Session::flash('type', 'success');
        return Redirect::to('admin/compras/ver/'.$id_pedido);
    }

    public function edit($id)
    {
        $pedido = DB::table('pedidos')->where('pedidos.id_pedido', $id)
        ->leftJoin('pedido_producto', 'pedido_producto.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->leftJoin('direccions', 'direccions.id_direccion', '=', 'pedidos.direccion_id_direccion')
        ->select('pedidos.*','estados.descripcion_compra as estado', 'users.name', 'users.surname', 'roles.discount', 'direccions.id_direccion', 'direccions.direccion', 'direccions.localidad', 'direccions.provincia')
        ->selectRaw('sum(pedido_producto.costo_unitario * pedido_producto.cantidad) as monto_sin_desc, sum((pedido_producto.costo_unitario * pedido_producto.cantidad) * (1-(pedido_producto.descuento/100))) as monto_con_desc')
        ->groupBy('pedidos.id_pedido')
        ->get()->first();
        $direcciones = DB::table('direccions')->where('direccions.user_id', $pedido->user_id_user)->get();
        $productosPedido = DB::table('pedido_producto')->where('pedido_producto.pedido_id_pedido', $id)
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->join('almacen_producto', 'almacen_producto.producto_id_producto', '=', 'productos.id_producto')
        // ->leftJoin('almacens', 'almacens.id_almacen', '=', 'almacen_producto.almacen_id_almacen')
        ->join('almacens', function($join) {
            $join->on('almacens.id_almacen', 'almacen_producto.almacen_id_almacen')
                ->where('almacens.nombre', '<>', "Mercado Libre");
        })
        ->select('pedido_producto.*','productos.tipo','productos.unidad','productos.conversion','productos.id_producto','productos.nombre','productos.codigo')
        ->selectRaw('sum(almacen_producto.cantidad) as stock, productos.iva as iva_producto')
        ->groupBy('pedido_producto.producto_id_producto')->get();
        $proveedores = DB::table('users')->where('users.deleted', 0)->where('roles.name', '=', 'Proveedor')
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->select('users.*', 'roles.discount as descuento')->get();
        $productos = DB::table('productos')->where('productos.deleted', 0)->where('productos.estado', 1)
        ->where('producto_user.user_id_user', $pedido->user_id_user)->where(function($q){
            $q->where('productos.tipo', 'Producto')
                ->orWhere('productos.tipo', 'Insumo');
         })
        ->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('users', 'producto_user.user_id_user', '=', 'users.id_user')
        ->select('productos.*', 'producto_user.costo_dolares', 'users.valor_dolar')->get();
        return view('compras.edit', ['pedido' => $pedido, 'proveedores' => $proveedores, 'productos' => $productos, 'productosPedido' => $productosPedido, 'direcciones' => $direcciones]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pedido  $pedido
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fields = $request->all();
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        // echo json_encode($fields);die;
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'user_id_user'=>'required',
            'items'=>'required'
        );

        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/compras/editar/'.$id)
                ->withErrors($validator);
                // return view('marcas.crear');
        } else {
            $pedido = Pedido::where('id_pedido',$id)->get()->first();
            $pedido->descripcion = $fields['descripcion'];
            $pedido->observaciones_publicas = $fields['observaciones_publicas'];
            $pedido->fecha_estimada_recepcion = $fields['fecha_estimada_recepcion'];
            $pedido->update();
            DB::table('pedido_producto')->where('pedido_id_pedido', $id)->delete();
            foreach($request->get('items') as $item){
                $producto = DB::table('productos')->where('id_producto', $item['id_producto'])->get()->first();
                if($producto->tipo == 'Insumo'){
                    $cantidad = $item['cantidad'];
                }else{
                    $cantidad = $item['cantidad'];
                }
                DB::table('pedido_producto')->insert(
                    [
                    'pedido_id_pedido' => $id, 
                    'producto_id_producto' => $item['id_producto'], 
                    'iva' => $item['iva'], 
                    'cantidad' => $cantidad, 
                    'descuento' => 0, 
                    'costo_unitario' => $item['costo_unitario'],
                    'created_at' =>  date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    ]
                );
            }
            // redirect
            Session::flash('message', 'La compra se actualizó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/compras/ver/'.$pedido->id_pedido);
        }
    }
}
