<?php

namespace App\Http\Controllers;

use App\Perfil;
use App\Permiso;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class PerfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perfiles = DB::table('perfils')->where('deleted', 0)->paginate(10);
        return view('perfiles.list', ['perfiles' => $perfiles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permisos = Permiso::all();
        return view('perfiles.create', ['permisos' => $permisos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'nombre'=>'required'
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/perfiles/crear')
                ->withErrors($validator);
                // return view('perfiles.crear');
        } else {
            $perfil = new Perfil([
                'nombre' => $request->get('nombre'),
                'descripcion' => $request->get('descripcion')
            ]);
            $perfil->save();
            foreach($request->get('items') as $item){
                DB::table('perfil_permiso')->insert(
                    [
                    'perfil_id_perfil' => $perfil->id_perfil, 
                    'permiso_id_permiso' => $item['id_permiso'], 
                    'ver' => isset($item['ver']) ? 1 : 0, 
                    'crear' => isset($item['crear']) ? 1 : 0, 
                    'editar' => isset($item['editar']) ? 1 : 0,
                    'borrar' => isset($item['borrar']) ? 1 : 0,
                    'created_at' =>  date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    ]
                );
            }
            // redirect
            Session::flash('message', 'El perfil se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/perfiles/index');
            // return view('perfiles.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Perfil  $perfil
     * @return \Illuminate\Http\Response
     */
    public function show(Perfil $perfil)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Perfil  $perfil
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permisos = Permiso::leftJoin('perfil_permiso',
            DB::raw('perfil_permiso.perfil_id_perfil = '.$id.' and permisos.id_permiso'), '=','perfil_permiso.permiso_id_permiso')
        ->select('permisos.id_permiso', 'permisos.nombre', 'perfil_permiso.*')->orderBy('permisos.nombre')->get();
        $perfil = Perfil::where('id_perfil',$id)->get()->first();
        return view('perfiles.edit', ['perfil' => $perfil, 'permisos' => $permisos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Perfil  $perfil
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $perfil = Perfil::where('id_perfil',$id)->get()->first();
        $messages = [
            'required' => 'El :attribute es requerido'
          ];
        $rules = array(
            'nombre'=>'required'
        );
        $fields = $request->all();
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/perfiles/editar/'.$id)
                ->withErrors($validator);
                // return view('perfiles.editar', [$id]);
        } else {
            $perfil->update($fields);
            foreach($request->get('items') as $item){
                if($item['id_perfil_permiso'] == 0){
                    DB::table('perfil_permiso')->insert(
                        [
                        'perfil_id_perfil' => $perfil->id_perfil, 
                        'permiso_id_permiso' => $item['id_permiso'], 
                        'ver' => isset($item['ver']) ? 1 : 0, 
                        'crear' => isset($item['crear']) ? 1 : 0, 
                        'editar' => isset($item['editar']) ? 1 : 0,
                        'borrar' => isset($item['borrar']) ? 1 : 0,
                        'created_at' =>  date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        ]
                    );
                }else{
                    DB::table('perfil_permiso')->where('perfil_permiso.id_perfil_permiso',$item['id_perfil_permiso'])
                    ->update(
                        [
                        'perfil_id_perfil' => $perfil->id_perfil, 
                        'permiso_id_permiso' => $item['id_permiso'], 
                        'ver' => isset($item['ver']) ? 1 : 0, 
                        'crear' => isset($item['crear']) ? 1 : 0, 
                        'editar' => isset($item['editar']) ? 1 : 0,
                        'borrar' => isset($item['borrar']) ? 1 : 0,
                        'created_at' =>  date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        ]
                    );
                }
            }
            // redirect
            Session::flash('message', 'El perfil se actualizó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/perfiles/index');
            // return view('perfiles.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Perfil  $perfil
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $perfil = Perfil::find($id);
        $perfil->deleted = 1;
        $perfil->save();

        // redirect
        Session::flash('message', 'El perfil se eliminó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/perfiles/index');
        // return view('perfiles.index');
    }
}
