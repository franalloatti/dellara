<?php

namespace App\Http\Controllers;

use App\Role;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = DB::table('roles')->where('roles.deleted', 0)->where('roles.name', '<>', 'admin')->where('roles.name', '<>', 'administrador')->where('roles.name', '<>', 'Proveedor')
        ->leftJoin('role_user', 'roles.id_role', '=', 'role_user.role_id_role')
        ->groupBy('id_role')
        ->select('roles.id_role', 'roles.name', 'roles.discount', 'role_user.user_id_user', DB::raw('count(*) as total'))
        ->paginate(10);
        return view('roles.list', ['roles' => $roles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'El :attribute es requerido',
            'min'      => 'El descuento debe ser mayor o igual a 0',
            'max'      => 'El descuento debe ser menor o igual a 100',
          ];
        $rules = array(
            'nombre'=>'required',
            'discount'=>'min:0|max:100'
        );

        $validator = Validator::make($request->all(), $rules, $messages);


        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/roles/crear')
                ->withErrors($validator);
                // return view('roles.crear');
        } else {
            $role = new Role([
                'name' => $request->get('nombre'),
                'discount' => $request->get('discount')
            ]);
            $role->save();
            // redirect
            Session::flash('message', 'El rol se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/roles/index');
            // return view('roles.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::where('id_role',$id)->get()->first();
        return view('roles.edit', ['role' => $role]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::where('id_role',$id)->get()->first();
        $messages = [
            'required' => 'El :attribute es requerido',
            'min'      => 'El descuento debe ser mayor o igual a 0',
            'max'      => 'El descuento debe ser menor o igual a 100',
          ];
        $rules = array(
            'nombre'=>'required',
            'discount'=>'min:0|max:100'
        );
        $fields = $request->all();
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/roles/editar/'.$id)
                ->withErrors($validator);
            // return view('roles.editar', [$id]);
        } else {
            $role->name = $fields['nombre'];
            $role->update($fields);
            // redirect
            Session::flash('message', 'El rol se actualizó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/roles/index');
            // return view('roles.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         // delete
         $role = Role::find($id);
         $role->deleted = 1;
         $role->save();
 
         // redirect
         Session::flash('message', 'El rol se eliminó con éxito!');
         Session::flash('type', 'success');
         return Redirect::to('admin/roles/index');
        //  return view('roles.index');
        }
}
