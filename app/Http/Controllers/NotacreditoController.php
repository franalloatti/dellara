<?php

namespace App\Http\Controllers;

use PDF;
use \stdClass;
use App\Pedido;
use App\Remito;
use App\Estado;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class NotacreditoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $fields = $request->all();
        $search = '';
        $remitos = DB::table('remitos');
        if(isset($fields['search'])){
            $search = $fields['search'];
            $remitos = $remitos->where('remitos.id_remito', 'like', '%' . $search . '%')
            ->orWhere('remitos.pedido_id_pedido', 'like', '%' . $search . '%');
        }
        $remitos = $remitos->leftJoin('pedidos', 'pedidos.id_pedido', '=', 'remitos.pedido_id_pedido')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('perfils', 'users.perfil_id', '=', 'perfils.id_perfil')
        ->select('remitos.*')
        ->selectRaw("IF(perfils.nombre = 'Mayorista', users.name, concat(users.name, ' ', users.surname)) as name")
        ->paginate(10);
        return view('notascredito.list', ['search' =>  $search, 'remitos' => $remitos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->all();
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        // echo json_encode($fields);die;
        // $ped = Pedido::where('id_pedido',$fields['id_pedido'])->with('estados')->get()->first();
        // echo json_encode($ped->estados[count($ped->estados)-1]);
        // die;
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'id_pedido'=>'required',
            'user_id_user'=>'required',
            'direccion_id_direccion'=>'required',
            'items'=>'required'
        );

        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/pedidos/remito/'.$fields['id_pedido'])
                ->withErrors($validator);
                // return view('marcas.crear');
        } else {
            DB::table('pedidos')->where('id_pedido', $fields['id_pedido'])->update(['direccion_id_direccion' => $fields['direccion_id_direccion']]);
            $pedidoOriginal = Pedido::where('id_pedido',$fields['id_pedido'])->with('estados')->get()->first(); 
            $pedidoOriginal->estados()->sync(['estado_id_estado' => ($pedidoOriginal->estados[count($pedidoOriginal->estados)-1]->id_estado + 1)], false);
            $pedidoOriginal = Pedido::where('id_pedido',$fields['id_pedido'])->with('estados')->get()->first(); 
            sleep(1);
            $remito = new Remito([
                'pedido_id_pedido' => $fields['id_pedido']
            ]);
            $remito->save();
            $nuevoPedido = 0;
            $totalMontoRemito = 0;
            $newPedido = new stdClass();
            foreach($fields['items'] as $item){
                if($item['cantidad'] > 0){
                    if($item['cantidad'] < $item['cantidad_pedido']){
                        if($nuevoPedido == 0){
                            $newPedido = new Pedido([
                                'user_id_user' => $fields['user_id_user'],
                                'direccion_id_direccion' => $fields['direccion_id_direccion'],
                                'usuario_crea_id' => Auth::user()->id_user
                            ]);
                            $newPedido->save();
                            DB::table('pedido_pedido')->insert([
                                [
                                    'pedido_id_padre' => $fields['id_pedido'],
                                    'pedido_id_hijo' => $newPedido->id_pedido
                                ]
                            ]);
                            $nuevoPedido = 1;
                        }
                        DB::table('pedido_producto')->insert(
                            [
                            'pedido_id_pedido' => $newPedido->id_pedido, 
                            'producto_id_producto' => $item['id_producto'], 
                            'cantidad' => $item['cantidad_pedido'] - $item['cantidad'], 
                            'descuento' => $item['descuento'], 
                            'costo_unitario' => $item['costo_unitario'],
                            'precio_unitario' => $item['precio_unitario'],
                            'created_at' =>  date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                            ]
                        );
                    }
                    $totalMontoRemito = $totalMontoRemito + floatval($item['precio_final']);
                    DB::table('producto_remito')->insert(
                        [
                        'remito_id_remito' => $remito->id_remito, 
                        'producto_id_producto' => $item['id'], 
                        'cantidad' => $item['cantidad'], 
                        'descuento' => $item['descuento'], 
                        'costo_unitario' => $item['costo_unitario'],
                        'precio_unitario' => $item['precio_unitario'],
                        'created_at' =>  date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        ]
                    );
                    // $almacen_producto = DB::table('almacen_producto')->where('producto_id_producto', $item['id_producto'])->where('almacen_id_almacen', 1)
                    // ->decrement('cantidad', $item['cantidad']);
                }else{
                    if($nuevoPedido == 0){
                        $newPedido = new Pedido([
                            'user_id_user' => $fields['user_id_user'],
                            'direccion_id_direccion' => $fields['direccion_id_direccion'],
                            'usuario_crea_id' => Auth::user()->id_user,
                        ]);
                        $newPedido->save();
                        DB::table('pedido_pedido')->insert([
                            [
                                'pedido_id_padre' => $fields['id_pedido'],
                                'pedido_id_hijo' => $newPedido->id_pedido
                            ]
                        ]);
                        $nuevoPedido = 1;
                    }
                    DB::table('pedido_producto')->where('id_pedido_producto', $item['id'])->update(['pedido_id_pedido' => $newPedido->id_pedido]);
                }
            }           
            if($nuevoPedido){
                $newPedido->estados()->sync(['estado_id_estado' => $pedidoOriginal->estados[count($pedidoOriginal->estados)-1]->id_estado - 1], false);
            }
            $pedidoOriginal->estados()->sync(['estado_id_estado' => ($pedidoOriginal->estados[count($pedidoOriginal->estados)-1]->id_estado + 1)], false);
            $remito->update(['monto' => $totalMontoRemito]);
            // redirect
            Session::flash('message', 'El remito se generó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/notascredito/index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Remito  $remito
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $remito = DB::table('remitos')->where('remitos.id_remito', $id)
        ->leftJoin('producto_remito', 'producto_remito.remito_id_remito', '=', 'remitos.id_remito')
        ->leftJoin('pedido_producto', 'pedido_producto.producto_id_producto', '=', 'producto_remito.producto_id_producto')
        ->leftJoin('pedidos', 'pedidos.id_pedido', '=', 'remitos.pedido_id_pedido')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('perfils', 'users.perfil_id', '=', 'perfils.id_perfil')
        ->leftJoin('direccions', 'direccions.id_direccion', '=', 'pedidos.direccion_id_direccion')
        ->select('remitos.*', 'pedidos.motivo_devolucion', 'direccions.direccion', 'direccions.localidad', 'direccions.provincia')
        ->selectRaw("IF(perfils.nombre = 'Mayorista', users.name, concat(users.name, ' ', users.surname)) as name")
        ->groupBy('pedidos.id_pedido')
        ->get()->first();
        $productos = DB::table('producto_remito')->where('producto_remito.remito_id_remito', $id)
        ->leftJoin('pedido_producto', 'producto_remito.producto_id_producto', '=', 'pedido_producto.id_pedido_producto')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->join('almacen_producto', 'almacen_producto.producto_id_producto', '=', 'pedido_producto.producto_id_producto')
        ->select('producto_remito.*','productos.nombre','productos.codigo')
        ->groupBy('pedido_producto.producto_id_producto')->get();
        return view('notascredito.show', ['remito' => $remito, 'productos' => $productos]);
    }

    public function imprimir($id)
    {
        $remito = DB::table('remitos')->where('remitos.id_remito', $id)
        ->leftJoin('producto_remito', 'producto_remito.remito_id_remito', '=', 'remitos.id_remito')
        ->leftJoin('pedido_producto', 'pedido_producto.producto_id_producto', '=', 'producto_remito.producto_id_producto')
        ->leftJoin('pedidos', 'pedidos.id_pedido', '=', 'remitos.pedido_id_pedido')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('direccions', 'direccions.id_direccion', '=', 'pedidos.direccion_id_direccion')
        ->select('remitos.*', 'users.name', 'users.surname', 'users.cuit', 'direccions.direccion', 'direccions.localidad', 'direccions.provincia')
        ->groupBy('pedidos.id_pedido')
        ->get()->first();
        $productos = DB::table('producto_remito')->where('producto_remito.remito_id_remito', $id)
        ->leftJoin('pedido_producto', 'producto_remito.producto_id_producto', '=', 'pedido_producto.id_pedido_producto')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->join('almacen_producto', 'almacen_producto.producto_id_producto', '=', 'pedido_producto.producto_id_producto')
        ->select('producto_remito.*','productos.nombre','productos.codigo')
        ->groupBy('pedido_producto.producto_id_producto')->get();
        // return view('remitos.imprimir', ['remito' => $remito, 'productos' => $productos]);
        $pdf = PDF::loadView('notascredito.imprimir', ['remito' => $remito, 'productos' => $productos]);
        return $pdf->download('notadecredito-'.$id.'.pdf');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Remito  $remito
     * @return \Illuminate\Http\Response
     */
    public function edit(Remito $remito)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Remito  $remito
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Remito $remito)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Remito  $remito
     * @return \Illuminate\Http\Response
     */
    public function destroy(Remito $remito)
    {
        //
    }
}
