<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Perfil;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::CATALOGO;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    
    public function showRegistrationForm()
    {
        return view('vendor.adminlte.register');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        if (env('APP_ENV') == 'production'){
            $validator = Validator::make($data, [
                'name' => ['required', 'string', 'max:255'],
                'surname' => ['required', 'string', 'max:255'],
                'dni' => ['required', 'string', 'max:20'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'min:6', 'confirmed'],
                'g-recaptcha-response' => 'required|recaptchav3:contacto,0.5',
            ]);
        }else{
            $validator = Validator::make($data, [
                'name' => ['required', 'string', 'max:255'],
                'surname' => ['required', 'string', 'max:255'],
                'dni' => ['required', 'string', 'max:20'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'min:6', 'confirmed'],
            ]);
        }
        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $perfil = Perfil::where('nombre','Consumidor final')->where('deleted', 0)->get()->first();
        if(env('CREAR_CUENTA_USUARIO', false)){
            $id_perfil = env('ID_PERFIL_USUARIO', 3);
        }else{
            $id_perfil = isset($perfil->id_perfil) ? $perfil->id_perfil : 3;
        }
        $user = User::create([
            'name' => $data['name'],
            'surname' => $data['surname'],
            'nombre_fantasia' => $data['name'] . " " . $data['surname'],
            'dni' => $data['dni'],
            'email' => $data['email'],
            'perfil_id' => $id_perfil,
            'password' => Hash::make($data['password'])
        ]);
        $user->save();
        $username = substr('000000'.$user->id_user, -6);
        $user->update(['username' => $username]);
        $user->roles()->sync([4]);
        Session::flash('message', 'Su cuenta se creó con éxito!');
        Session::flash('type', 'success');
        return $user;
    }
}
