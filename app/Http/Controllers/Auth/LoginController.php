<?php

namespace App\Http\Controllers\Auth;

use App\Role;
use App\User;
use Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::CATALOGO;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect('/');
    }

    public function login(Request $request)
    {   
        $input = $request->all();
        $rules = array(
            'email' => 'required',
            'password' => 'required',
        );
        if (env('APP_ENV') == 'production'){
            $rules['g-recaptcha-response'] = 'required|recaptchav3:contacto,0.5';
        }
        $this->validate($request, $rules);
        $fieldType = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        if(auth()->attempt(array($fieldType => $input['email'], 'password' => $input['password'])))
        {
            $role_admin = DB::table('users')->where('users.id_user', '=', Auth::user()->id_user)
            ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
            ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
            ->select('roles.name as role', 'users.deleted')->get()->first();
            if($role_admin->deleted){
                $this->guard()->logout();
                $request->session()->flush();
                $request->session()->regenerate();
                Session::flash('message', 'Su cuenta se encuentra eliminada.');
                Session::flash('type', 'danger');
                return redirect()->route('login');
            }
            if ($role_admin->role == 'admin' || $role_admin->role == 'administrador') {
                $user = User::where('id_user',Auth::user()->id_user)->get()->first();
                date_default_timezone_set('America/Argentina/Buenos_Aires');
                $user->last_login_at = date('Y-m-d H:i:s');
                $user->update();
                return redirect()->route(Auth::user()->redirect_login());
            }else{
                $user = User::where('id_user',Auth::user()->id_user)->get()->first();
                date_default_timezone_set('America/Argentina/Buenos_Aires');
                $user->last_login_at = date('Y-m-d H:i:s');
                $user->update();
                return redirect()->route(Auth::user()->redirect_login());
                // $this->guard()->logout();
                // $request->session()->flush();
                // $request->session()->regenerate();
                // Session::flash('message', 'No está autorizado.');
                // Session::flash('type', 'danger');
                // return redirect()->route('login');
            }
        }else{
            Session::flash('message', 'Credenciales inválidas.');
            Session::flash('type', 'danger');
            return redirect()->route('login');
        }
    }

    protected function authenticated(Request $request, $user)
    {
        $role_admin = DB::table('users')->where('users.deleted', 0)->where('users.id_user', '=', $user->id_user)
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->select('roles.name as role')->get()->first();
        if ($role_admin->name == 'admin') {
            return redirect()->route('home');
        }
        Session::flash('message', 'No está autorizado.');
        Session::flash('type', 'danger');
        return redirect()->route('login');
    }
}
