<?php

namespace App\Http\Controllers;

use Auth;
use App\Pedido;
use App\Estado;
use App\Direccion;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PedidosExport;

class PedidoController extends Controller
{
    public function getReporte(Request $request){
        $fields = $request->all();
        $fecha_desde = $fields['fecha_desde'];
        $fecha_hasta = $fields['fecha_hasta'];
        return Excel::download(new PedidosExport('Pedido', $fecha_desde, $fecha_hasta), 
        'Ventas_'.date('d-m-Y', strtotime($fecha_desde)).'_'.date('d-m-Y', strtotime($fecha_hasta)).'.xls');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $fields = $request->all();
        $estados = DB::table('estados')->get();
        $estadoSelected = null;
        $search = '';
        $fecha_desde = '';
        $fecha_hasta = '';
        $pedidos = DB::table('pedidos')->where('pedidos.tipo', '=', 'Pedido');
        if(isset($fields['search'])){
            $search = $fields['search'];
            $pedidos = $pedidos->where('pedidos.id_pedido', 'like', '%' . $search . '%')
            ->orWhere('users.name', 'like', '%' . $search . '%')
            ->orWhere('users.surname', 'like', '%' . $search . '%')
            ->orWhere('estados.nombre', 'like', '%' . $search . '%');
        }
        if(isset($fields['fecha_desde'])){
            $fecha_desde = $fields['fecha_desde'];
            $pedidos = $pedidos->whereRaw("cast(pedidos.created_at as date) >= cast('".$fecha_desde."' as date)");
        }
        if(isset($fields['fecha_hasta'])){
            $fecha_hasta = $fields['fecha_hasta'];
            $pedidos = $pedidos->whereRaw("cast(pedidos.created_at as date) <= cast('".$fecha_hasta."' as date)");
        }
        if(isset($fields['estado_id']) && $fields['estado_id'] !== null){
            $estadoSelected = $fields['estado_id'];
            $pedidos = $pedidos->where('estados.id_estado', '=', $estadoSelected);
        }
        $pedidos = $pedidos->leftJoin('pedido_producto', 'pedido_producto.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('remitos', 'remitos.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('users as proveedor', 'proveedor.id_user', '=', 'producto_user.user_id_user')
        // ->leftJoin('estado_pedido', 'estado_pedido.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('perfils', 'users.perfil_id', '=', 'perfils.id_perfil')
        ->select('pedidos.*','remitos.id_remito', 'remitos.monto as monto_remito', 'estados.id_estado', 'estados.nombre as estado')
        ->selectRaw("IF(perfils.nombre = 'Mayorista', users.name, concat(users.name, ' ', users.surname)) as name, sum((pedido_producto.precio_unitario * pedido_producto.cantidad) * (1-(pedido_producto.descuento/100))) as monto")
        ->groupBy('pedidos.id_pedido')
        ->orderBy('pedidos.created_at', 'desc')
        ->paginate(10);
        // echo json_encode($pedidos);die;
        return view('pedidos.list', ['search' =>  $search, 'fecha_desde' =>  $fecha_desde, 'fecha_hasta' =>  $fecha_hasta, 'estados' =>  $estados, 
        'estadoSelected' =>  $estadoSelected, 'pedidos' => $pedidos]);
    }

    public function enpreparacion(Request $request)
    {
        $fields = $request->all();
        $search = '';
        $pedidos = DB::table('pedidos')->where('pedidos.tipo', '=', 'Pedido');
        if(isset($fields['search'])){
            $search = $fields['search'];
            $pedidos = $pedidos->where('pedidos.id_pedido', 'like', '%' . $search . '%')
            ->orWhere('users.name', 'like', '%' . $search . '%')
            ->orWhere('users.surname', 'like', '%' . $search . '%')
            ->orWhere('estados.nombre', 'like', '%' . $search . '%');
        }
        $pedidos = $pedidos->leftJoin('pedido_producto', 'pedido_producto.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('remitos', 'remitos.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('users as proveedor', 'proveedor.id_user', '=', 'producto_user.user_id_user')
        // ->leftJoin('estado_pedido', 'estado_pedido.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('perfils', 'users.perfil_id', '=', 'perfils.id_perfil')
        ->select('pedidos.*','remitos.id_remito', 'remitos.monto as monto_remito','estados.nombre as estado','estado_pedido.estado_id_estado')
        ->selectRaw("IF(perfils.nombre = 'Mayorista', users.name, concat(users.name, ' ', users.surname)) as name, sum((pedido_producto.precio_unitario * pedido_producto.cantidad) * (1-(pedido_producto.descuento/100))) as monto")
        ->groupBy('pedidos.id_pedido')
        ->orderBy('pedidos.id_pedido', 'desc')
        ->havingRaw('estado_id_estado < 3')
        ->paginate(10);
        return view('pedidos.enpreparacion', ['search' =>  $search, 'pedidos' => $pedidos]);
    }

    public function mispedidos(Request $request)
    {
        $fields = $request->all();
        $search = '';
        $pedidos = DB::table('pedidos')->where('pedidos.tipo', '=', 'Pedido')->where('pedidos.user_id_user', '=', Auth::user()->id_user);
        if(isset($fields['search'])){
            $search = $fields['search'];
            $pedidos = $pedidos->where('pedidos.id_pedido', 'like', '%' . $search . '%')
            ->orWhere('users.name', 'like', '%' . $search . '%')
            ->orWhere('users.surname', 'like', '%' . $search . '%')
            ->orWhere('estados.nombre', 'like', '%' . $search . '%');
        }
        $pedidos = $pedidos->leftJoin('pedido_producto', 'pedido_producto.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        // ->leftJoin('estado_pedido', 'estado_pedido.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->select('pedidos.*', 'estados.nombre as estado', 'estados.id_estado', 'users.name', 'users.surname')
        ->selectRaw('sum((pedido_producto.precio_unitario * pedido_producto.cantidad) * (1-(pedido_producto.descuento/100))) as monto')
        ->groupBy('pedidos.id_pedido')
        ->having('id_estado', '>', '1')
        ->paginate(10);
        return view('pedidos.mispedidos', ['search' =>  $search, 'pedidos' => $pedidos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientes = DB::table('users')->where('users.deleted', 0)->where('roles.name', '<>', 'admin')->where('roles.name', '<>', 'administrador')->where('roles.name', '<>', 'Proveedor')
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->leftJoin('perfils', 'users.perfil_id', '=', 'perfils.id_perfil')
        ->select('users.id_user', 'roles.discount as descuento')
        ->selectRaw("IF(perfils.nombre = 'Mayorista', users.name, concat(users.name, ' ', users.surname)) as name")->get();
        $productos = DB::table('productos')->where('productos.deleted', 0)->where('productos.estado', 1)->where('productos.tipo', 'Producto')
        ->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('pedido_producto', function($join) {
            $join->on('pedido_producto.producto_id_producto', '=', 'productos.id_producto')
            ->whereRaw("pedido_producto.tipo_item = 'venta'")
            ->on('pedido_producto.created_at', DB::raw("(SELECT MAX(PP.created_at) from pedido_producto as PP join pedidos as P on P.id_pedido = PP.pedido_id_pedido where P.tipo = 'Pedido' and PP.producto_id_producto = productos.id_producto)"));
        })
        ->leftJoin('users', 'producto_user.user_id_user', '=', 'users.id_user')
        ->select('productos.*', 'producto_user.costo_dolares', 'users.valor_dolar')
        ->selectRaw("ifnull(`pedido_producto`.`precio_unitario`,'sin definir') AS ultimo_precio")->get();
        // echo json_encode($productos);die;
        return view('pedidos.create', ['clientes' => $clientes, 'productos' => $productos]);
    }

    public function direccionescliente($user_id_user = null)
    {
        if(!empty($user_id_user)){
            $direcciones = Direccion::where('user_id',$user_id_user)->get();
        }else{
            $direcciones = Direccion::all();
        }
        return $direcciones;
    }

    public function getultimoprecio($id_user, $id_producto)
    {
        $producto = DB::table('productos')->where('productos.tipo', 'Producto')->where('pedidos.user_id_user', $id_user)->where('productos.id_producto', $id_producto)
        ->leftJoin('pedido_producto', function($join) use($id_user) {
            $join->on('pedido_producto.producto_id_producto', '=', 'productos.id_producto')
            ->whereRaw("pedido_producto.tipo_item = 'venta'")
            ->on('pedido_producto.created_at', DB::raw("(SELECT MAX(PP.created_at) from pedido_producto as PP join pedidos as P on P.id_pedido = PP.pedido_id_pedido where P.tipo = 'Pedido' and PP.producto_id_producto = productos.id_producto and P.user_id_user = ".$id_user.")"));
        })
        ->leftJoin('pedidos', 'pedidos.id_pedido', '=', 'pedido_producto.pedido_id_pedido')
        ->selectRaw("ifnull(`pedido_producto`.`precio_unitario`,'sin definir') AS ultimo_precio")->get()->first();
        // echo json_encode($producto);die;
        if(!empty($producto)){
            $ultimo_precio = $producto->ultimo_precio;
        }else{
            $ultimo_precio = 'sin definir';
        }
        return $ultimo_precio;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $fields = $request->all();
        // echo json_encode($fields);die;
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'user_id_user'=>'required',
            'direccion_id_direccion'=>'required',
            'items'=>'required'
        );

        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/pedidos/crear')
                ->withErrors($validator);
                // return view('marcas.crear');
        } else {
            $pedido = new Pedido([
                'user_id_user' => $request->get('user_id_user'),
                'direccion_id_direccion' => $request->get('direccion_id_direccion'),
                'descripcion' => nl2br($request->get('descripcion')),
                'observaciones_publicas' => nl2br($request->get('observaciones_publicas')),
                'fecha_estimada_recepcion' => $request->get('fecha_estimada_recepcion'),
                'flete' => $request->get('flete'),
                'nro_factura' => $request->get('nro_factura'),
                'created_at' =>  date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'usuario_crea_id' => Auth::user()->id_user,
            ]);
            $estado = Estado::where('id_estado',2)->get()->first();
            $pedido->save();
            $pedido->estados()->sync(['estado_id_estado' => $estado->id_estado]);
            foreach($request->get('items') as $item){
                DB::table('pedido_producto')->insert(
                    [
                    'pedido_id_pedido' => $pedido->id_pedido, 
                    'producto_id_producto' => $item['id'], 
                    'cantidad' => $item['cantidad'], 
                    'iva' => $item['iva'], 
                    'descuento' => $item['descuento'], 
                    'costo_unitario' => $item['costo_unitario'],
                    'precio_unitario' => $item['precio_unitario'],
                    'created_at' =>  date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    ]
                );
            }
            if($request->get('items_prom') !== null){
                foreach($request->get('items_prom') as $item){
                    DB::table('pedido_producto')->insert(
                        [
                        'tipo_item' => 'promocion', 
                        'pedido_id_pedido' => $pedido->id_pedido, 
                        'producto_id_producto' => $item['id'], 
                        'cantidad' => $item['cantidad'], 
                        'iva' => 0, 
                        'descuento' => 0, 
                        'costo_unitario' => $item['costo_unitario'],
                        'precio_unitario' => 0,
                        'created_at' =>  date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        ]
                    );
                }
            }
            if($request->get('items_dev') !== null){
                foreach($request->get('items_dev') as $item){
                    DB::table('pedido_producto')->insert(
                        [
                        'tipo_item' => 'devolucion', 
                        'pedido_id_pedido' => $pedido->id_pedido, 
                        'producto_id_producto' => $item['id'], 
                        'cantidad' => $item['cantidad'], 
                        'iva' => 0, 
                        'descuento' => 0, 
                        'costo_unitario' => $item['costo_unitario'],
                        'precio_unitario' => 0,
                        'created_at' =>  date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        ]
                    );
                }
            }
            // redirect
            Session::flash('message', 'El pedido se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/pedidos/ver/'.$pedido->id_pedido);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pedido  $pedido
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pedido = DB::table('pedidos')->where('pedidos.id_pedido', $id)
        ->leftJoin('pedido_producto', 'pedido_producto.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('remitos', 'remitos.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('perfils', 'users.perfil_id', '=', 'perfils.id_perfil')
        ->leftJoin('direccions', 'direccions.id_direccion', '=', 'pedidos.direccion_id_direccion')
        ->select('pedidos.*', 'remitos.id_remito', 'remitos.monto as monto_remito', 'estados.nombre as estado', 'direccions.direccion', 'direccions.localidad', 'direccions.provincia')
        ->selectRaw("IF(perfils.nombre = 'Mayorista', users.name, concat(users.name, ' ', users.surname)) as name, sum(pedido_producto.precio_unitario * pedido_producto.cantidad) as monto_sin_desc, sum((pedido_producto.precio_unitario * pedido_producto.cantidad) * (1-(pedido_producto.descuento/100))) as monto_con_desc")
        ->groupBy('pedidos.id_pedido')
        ->get()->first();
        // echo json_encode($pedido);die;
        $productos = DB::table('pedido_producto')->where('pedido_producto.pedido_id_pedido', $id)
        ->whereRaw("pedido_producto.tipo_item = 'venta'")
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->join('almacen_producto', 'almacen_producto.producto_id_producto', '=', 'productos.id_producto')
        ->join('almacens', function($join) {
            $join->on('almacens.id_almacen', 'almacen_producto.almacen_id_almacen')
                ->where('almacens.id_almacen', '=', DB::raw(env('ALMACEN_PRINCIPAL', 1)));
        })
        ->select('pedido_producto.*','productos.nombre','productos.codigo')
        ->selectRaw('ifnull(sum(almacen_producto.cantidad),0) as stock')
        ->groupBy('pedido_producto.producto_id_producto')->get();
        $devoluciones = DB::table('pedido_producto')->where('pedido_producto.pedido_id_pedido', $id)
        ->whereRaw("pedido_producto.tipo_item = 'devolucion'")
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->join('almacen_producto', 'almacen_producto.producto_id_producto', '=', 'productos.id_producto')
        ->join('almacens', function($join) {
            $join->on('almacens.id_almacen', 'almacen_producto.almacen_id_almacen')
                ->where('almacens.id_almacen', '=', DB::raw(env('ALMACEN_PRINCIPAL', 1)));
        })
        ->select('pedido_producto.*','productos.nombre','productos.codigo')
        ->selectRaw('ifnull(sum(almacen_producto.cantidad),0) as stock')
        ->groupBy('pedido_producto.producto_id_producto')->get();
        $promociones = DB::table('pedido_producto')->where('pedido_producto.pedido_id_pedido', $id)
        ->whereRaw("pedido_producto.tipo_item = 'promocion'")
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->join('almacen_producto', 'almacen_producto.producto_id_producto', '=', 'productos.id_producto')
        ->join('almacens', function($join) {
            $join->on('almacens.id_almacen', 'almacen_producto.almacen_id_almacen')
                ->where('almacens.id_almacen', '=', DB::raw(env('ALMACEN_PRINCIPAL', 1)));
        })
        ->select('pedido_producto.*','productos.nombre','productos.codigo')
        ->selectRaw('ifnull(sum(almacen_producto.cantidad),0) as stock')
        ->groupBy('pedido_producto.producto_id_producto')->get();
        $estados = DB::table('estados')->where('id_estado', '>', 1)->get();
        return view('pedidos.show', ['pedido' => $pedido, 'productos' => $productos, 'devoluciones' => $devoluciones,
                                     'promociones' => $promociones, 'estados' => $estados]);
    }

    public function mipedido($id)
    {
        $pedido = DB::table('pedidos')->where('pedidos.id_pedido', $id)
        ->leftJoin('pedido_producto', 'pedido_producto.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('direccions', 'direccions.id_direccion', '=', 'pedidos.direccion_id_direccion')
        ->select('pedidos.*','estados.nombre as estado', 'users.name', 'users.surname', 'direccions.direccion', 'direccions.localidad', 'direccions.provincia')
        ->selectRaw('sum(pedido_producto.precio_unitario * pedido_producto.cantidad) as monto_sin_desc, sum((pedido_producto.precio_unitario * pedido_producto.cantidad) * (1-(pedido_producto.descuento/100))) as monto_con_desc')
        ->groupBy('pedidos.id_pedido')
        ->get()->first();
        $productos = DB::table('pedido_producto')->where('pedido_producto.pedido_id_pedido', $id)
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        // ->leftJoin(DB::raw("(SELECT `almacens`.`nombre`, `almacen_producto`.`producto_id_producto`, `almacen_producto`.`cantidad` FROM `almacen_producto`
        // join `almacens` on `almacens`.`id_almacen` = `almacen_producto`.`almacen_id_almacen` and `almacens`.`nombre` <> 'Mercado Libre') as TEMP "),function($join){
        //    $join->on('productos.id_producto', '=', 'TEMP.producto_id_producto');
        // })
        ->join('almacen_producto', 'almacen_producto.producto_id_producto', '=', 'productos.id_producto')
        ->join('almacens', function($join) {
            $join->on('almacens.id_almacen', 'almacen_producto.almacen_id_almacen')
                ->where('almacens.id_almacen', '=', DB::raw(env('ALMACEN_PRINCIPAL', 1)));
        })
        // ->join('almacen_producto', 'almacen_producto.producto_id_producto', '=', 'productos.id_producto')
        // // ->leftJoin('almacens', 'almacens.id_almacen', '=', 'almacen_producto.almacen_id_almacen')
        // ->join('almacens', function($join) {
        //     $join->on('almacens.id_almacen', 'almacen_producto.almacen_id_almacen')
        //         ->where('almacens.nombre', '<>', "Mercado Libre");
        // })
        ->select('pedido_producto.*','productos.nombre','productos.codigo')
        ->selectRaw('ifnull(sum(almacen_producto.cantidad),0) as stock')
        ->groupBy('pedido_producto.producto_id_producto')->get();
        $estados = DB::table('estados')->get();
        return view('pedidos.mipedido', ['pedido' => $pedido, 'productos' => $productos, 'estados' => $estados]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pedido  $pedido
     * @return \Illuminate\Http\Response
     */
    public function cambiarestado($id_pedido, $id_estado)
    {
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        DB::table('estado_pedido')->insert(
            [
            'pedido_id_pedido' => $id_pedido, 
            'estado_id_estado' => $id_estado, 
            'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            ]
        );
        // redirect
        Session::flash('message', 'El estado se actualizó con éxito!');
        Session::flash('type', 'success');
        return null;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pedido  $pedido
     * @return \Illuminate\Http\Response
     */
    public function remito($id)
    {
        $pedido = DB::table('pedidos')->where('pedidos.tipo', '=', 'Pedido')->where('pedidos.id_pedido', $id)
        ->leftJoin('pedido_producto', 'pedido_producto.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('direccions', 'direccions.id_direccion', '=', 'pedidos.direccion_id_direccion')
        ->select('pedidos.*','estados.nombre as estado', 'users.name', 'users.surname', 'direccions.id_direccion', 'direccions.direccion', 'direccions.localidad', 'direccions.provincia')
        ->selectRaw('sum(pedido_producto.precio_unitario * pedido_producto.cantidad) as monto_sin_desc, sum((pedido_producto.precio_unitario * pedido_producto.cantidad) * (1-(pedido_producto.descuento/100))) as monto_con_desc')
        ->groupBy('pedidos.id_pedido')
        ->get()->first();
        $direcciones = DB::table('direccions')->where('direccions.user_id', $pedido->user_id_user)->get();
        $productos = DB::table('pedido_producto')->where('pedido_producto.pedido_id_pedido', $id)
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->join('almacen_producto', 'almacen_producto.producto_id_producto', '=', 'productos.id_producto')
        // ->leftJoin('almacens', 'almacens.id_almacen', '=', 'almacen_producto.almacen_id_almacen')
        ->join('almacens', function($join) {
            $join->on('almacens.id_almacen', 'almacen_producto.almacen_id_almacen')
                ->where('almacens.id_almacen', '=', DB::raw(env('ALMACEN_PRINCIPAL', 1)));
        })
        ->leftJoin(DB::raw("(SELECT M.almacen_id_destino, M.cantidad FROM almacen_producto AP
        left join movimiento_stock M on AP.id_stock = M.almacen_id_destino
        join productos P on P.id_producto = AP.producto_id_producto
        where M.tipo_movimiento <> 'Ajuste' and cast(M.created_at as date) = cast(NOW() as date) 
        and (P.tipo <> 'Insumo' or M.tipo_movimiento <> 'Producción') 
        order by M.created_at desc) as MOV_DEST"), 'almacen_producto.id_stock', '=', 'MOV_DEST.almacen_id_destino')
        ->leftJoin(DB::raw("(SELECT M.almacen_id_origen, if(M.tipo_movimiento = 'Ajuste', M.cantidad,-1*M.cantidad) AS 'cantidad' FROM movimiento_stock M
        join almacen_producto AP on AP.id_stock = M.almacen_id_origen
        join productos P on P.id_producto = AP.producto_id_producto
        where cast(M.created_at as date) = cast(NOW() as date) 
        and (P.tipo <> 'Producto' or M.tipo_movimiento <> 'Producción') 
        order by M.created_at desc) as MOV_ORI"), 'almacen_producto.id_stock', 'MOV_ORI.almacen_id_origen')
        ->leftJoin(DB::raw("(SELECT P.tipo, P.motivo_devolucion, PP.producto_id_producto, PR.almacen_id_almacen, sum(IF(P.tipo = 'Pedido' or P.tipo = 'Devolucion compra', -1*PR.cantidad,PR.cantidad)) as 'cantidad' FROM producto_remito PR
        join pedido_producto PP on PR.producto_id_producto = PP.id_pedido_producto
        join pedidos P on PP.pedido_id_pedido = P.id_pedido
        where cast(PR.created_at as date) = cast(NOW() as date) and ((P.tipo <> 'Devolucion') or (P.tipo = 'Devolucion' and P.motivo_devolucion <> 'defecto'))
        group by P.tipo, PR.almacen_id_almacen, PP.producto_id_producto
        order by PR.created_at desc) as ITM_REM"), 
            function($join){
                $join->on('almacen_producto.almacen_id_almacen', '=', 'ITM_REM.almacen_id_almacen')
                ->on('almacen_producto.producto_id_producto', '=', 'ITM_REM.producto_id_producto');
            })
        ->select('pedido_producto.*','productos.id_producto','productos.nombre','productos.codigo')
        // ->selectRaw('sum(almacen_producto.cantidad) as stock')
        ->selectRaw("ifnull(almacen_producto.cantidad,0.00) as stock, sum(ifnull(MOV_DEST.cantidad,0.00)) as mov_destino, sum(ifnull(MOV_ORI.cantidad,0.00)) as mov_origen, 
        sum(if(ITM_REM.tipo = 'Pedido', ITM_REM.cantidad,0.00)) as venta, sum(if(ITM_REM.tipo = 'Devolucion', ITM_REM.cantidad,0.00)) as devolucion_venta, 
        sum(if(ITM_REM.tipo = 'Compra', ITM_REM.cantidad,0.00)) as compra, sum(if(ITM_REM.tipo = 'Devolucion compra', ITM_REM.cantidad,0.00)) as devolucion_compra")
        ->groupBy('pedido_producto.producto_id_producto', 'pedido_producto.tipo_item')->get();
        // echo json_encode($productos);die;
        $productosPedido = array();
        $promocionesPedido = array();
        $devolucionesPedido = array();
        foreach($productos as $producto){
            switch($producto->tipo_item){
                case 'devolucion':
                    array_push($devolucionesPedido, $producto);
                    break;
                case 'promocion':
                    array_push($promocionesPedido, $producto);
                    break;
                default:
                    array_push($productosPedido, $producto);
                    break;
            }
        }
        $almacenes = DB::table('almacens')->where('deleted', 0)->get();
        return view('pedidos.remito', ['pedido' => $pedido, 'productos' => $productosPedido, 'direcciones' => $direcciones, 
                    'almacenes' => $almacenes, 'devoluciones' => $devolucionesPedido, 'promociones' => $promocionesPedido]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pedido  $pedido
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pedido = DB::table('pedidos')->where('pedidos.id_pedido', $id)
        ->leftJoin('pedido_producto', 'pedido_producto.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('perfils', 'users.perfil_id', '=', 'perfils.id_perfil')
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->leftJoin('direccions', 'direccions.id_direccion', '=', 'pedidos.direccion_id_direccion')
        ->select('pedidos.*','estados.nombre as estado', 'users.name', 'users.surname', 'roles.discount', 'direccions.id_direccion', 'direccions.direccion', 'direccions.localidad', 'direccions.provincia')
        ->selectRaw("IF(perfils.nombre = 'Mayorista', users.name, concat(users.name, ' ', users.surname)) as name, sum(pedido_producto.precio_unitario * pedido_producto.cantidad) as monto_sin_desc, sum((pedido_producto.precio_unitario * pedido_producto.cantidad) * (1-(pedido_producto.descuento/100))) as monto_con_desc")
        ->groupBy('pedidos.id_pedido')
        ->get()->first();
        $direcciones = DB::table('direccions')->where('direccions.user_id', $pedido->user_id_user)->get();
        $productosPedido = DB::table('pedido_producto')->where('pedido_producto.pedido_id_pedido', $id)
        ->whereRaw("pedido_producto.tipo_item = 'venta'")
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->join('almacen_producto', 'almacen_producto.producto_id_producto', '=', 'productos.id_producto')
        ->join('almacens', function($join) {
            $join->on('almacens.id_almacen', 'almacen_producto.almacen_id_almacen')
                ->where('almacens.id_almacen', '=', DB::raw(env('ALMACEN_PRINCIPAL', 1)));
        })
        ->select('pedido_producto.*','productos.id_producto','productos.nombre','productos.codigo')
        ->selectRaw('sum(almacen_producto.cantidad) as stock, productos.iva as iva_producto')
        ->groupBy('pedido_producto.producto_id_producto')->get();
        $devolucionesPedido = DB::table('pedido_producto')->where('pedido_producto.pedido_id_pedido', $id)
        ->whereRaw("pedido_producto.tipo_item = 'devolucion'")
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->join('almacen_producto', 'almacen_producto.producto_id_producto', '=', 'productos.id_producto')
        ->join('almacens', function($join) {
            $join->on('almacens.id_almacen', 'almacen_producto.almacen_id_almacen')
                ->where('almacens.id_almacen', '=', DB::raw(env('ALMACEN_PRINCIPAL', 1)));
        })
        ->select('pedido_producto.*','productos.id_producto','productos.nombre','productos.codigo')
        ->selectRaw('sum(almacen_producto.cantidad) as stock')
        ->groupBy('pedido_producto.producto_id_producto')->get();
        $promocionesPedido = DB::table('pedido_producto')->where('pedido_producto.pedido_id_pedido', $id)
        ->whereRaw("pedido_producto.tipo_item = 'promocion'")
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->join('almacen_producto', 'almacen_producto.producto_id_producto', '=', 'productos.id_producto')
        ->join('almacens', function($join) {
            $join->on('almacens.id_almacen', 'almacen_producto.almacen_id_almacen')
                ->where('almacens.id_almacen', '=', DB::raw(env('ALMACEN_PRINCIPAL', 1)));
        })
        ->select('pedido_producto.*','productos.id_producto','productos.nombre','productos.codigo')
        ->selectRaw('sum(almacen_producto.cantidad) as stock')
        ->groupBy('pedido_producto.producto_id_producto')->get();
        $clientes = DB::table('users')->where('users.deleted', 0)->where('roles.name', '<>', 'admin')->where('roles.name', '<>', 'administrador')->where('roles.name', '<>', 'Proveedor')
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->leftJoin('perfils', 'users.perfil_id', '=', 'perfils.id_perfil')
        ->select('users.id_user', 'roles.discount as descuento')
        ->selectRaw("IF(perfils.nombre = 'Mayorista', users.name, concat(users.name, ' ', users.surname)) as name")->get();
        $productos = DB::table('productos')->where('productos.deleted', 0)->where('productos.estado', 1)->where('productos.tipo', 'Producto')
        ->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('pedido_producto', function($join) use($pedido) {
            $join->on('pedido_producto.producto_id_producto', '=', 'productos.id_producto')
            ->whereRaw("pedido_producto.tipo_item = 'venta'")
            ->on('pedido_producto.created_at', DB::raw("(SELECT MAX(PP.created_at) from pedido_producto as PP join pedidos as P on P.id_pedido = PP.pedido_id_pedido 
                                        where P.tipo = 'Pedido' and PP.producto_id_producto = productos.id_producto and P.user_id_user = ".$pedido->user_id_user." 
                                        and P.id_pedido <> ".$pedido->id_pedido.")"));
        })
        ->leftJoin('users', 'producto_user.user_id_user', '=', 'users.id_user')
        ->select('productos.*', 'producto_user.costo_dolares', 'users.valor_dolar')
        ->selectRaw("ifnull(`pedido_producto`.`precio_unitario`,'sin definir') AS ultimo_precio")->get();
        return view('pedidos.edit', ['pedido' => $pedido, 'clientes' => $clientes, 'productos' => $productos, 'direcciones' => $direcciones,
            'productosPedido' => $productosPedido, 'devolucionesPedido' => $devolucionesPedido, 'promocionesPedido' => $promocionesPedido]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pedido  $pedido
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fields = $request->all();
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        // echo json_encode($fields);die;
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'user_id_user'=>'required',
            'direccion_id_direccion'=>'required',
            'items'=>'required'
        );

        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/pedidos/crear')
                ->withErrors($validator);
                // return view('marcas.crear');
        } else {
            $pedido = Pedido::where('id_pedido',$id)->get()->first();
            $pedido->user_id_user = $fields['user_id_user'];
            $pedido->direccion_id_direccion = $fields['direccion_id_direccion'];
            $pedido->descripcion = nl2br($fields['observaciones']);
            $pedido->observaciones_publicas = nl2br($fields['observaciones_publicas']);
            $pedido->fecha_estimada_recepcion = $fields['fecha_estimada_recepcion'];
            $pedido->flete = $fields['flete'];
            $pedido->nro_factura = $fields['nro_factura'];
            $pedido->update();
            DB::table('pedido_producto')->where('pedido_id_pedido', $id)->delete();
            foreach($request->get('items') as $item){
                DB::table('pedido_producto')->insert(
                    [
                    'pedido_id_pedido' => $id, 
                    'producto_id_producto' => $item['id'], 
                    'cantidad' => $item['cantidad'], 
                    'iva' => $item['iva'], 
                    'descuento' => $item['descuento'], 
                    'costo_unitario' => $item['costo_unitario'],
                    'precio_unitario' => $item['precio_unitario'],
                    'created_at' =>  date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    ]
                );
            }
            if($request->get('items_prom') !== null){
                foreach($request->get('items_prom') as $item){
                    DB::table('pedido_producto')->insert(
                        [
                        'tipo_item' => 'promocion', 
                        'pedido_id_pedido' => $pedido->id_pedido, 
                        'producto_id_producto' => $item['id'], 
                        'cantidad' => $item['cantidad'], 
                        'iva' => 0, 
                        'descuento' => 0, 
                        'costo_unitario' => $item['costo_unitario'],
                        'precio_unitario' => 0,
                        'created_at' =>  date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        ]
                    );
                }
            }
            if($request->get('items_dev') !== null){
                foreach($request->get('items_dev') as $item){
                    DB::table('pedido_producto')->insert(
                        [
                        'tipo_item' => 'devolucion', 
                        'pedido_id_pedido' => $pedido->id_pedido, 
                        'producto_id_producto' => $item['id'], 
                        'cantidad' => $item['cantidad'], 
                        'iva' => 0, 
                        'descuento' => 0, 
                        'costo_unitario' => $item['costo_unitario'],
                        'precio_unitario' => 0,
                        'created_at' =>  date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        ]
                    );
                }
            }
            // redirect
            Session::flash('message', 'El pedido se actualizó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/pedidos/ver/'.$pedido->id_pedido);
        }
    }

    public function editarRemito(Request $request, $id)
    {
        $fields = $request->all();
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
        );

        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/pedidos/ver/'.$id)
                ->withErrors($validator);
                // return view('marcas.crear');
        } else {
            $pedido = Pedido::where('id_pedido',$id)->get()->first();
            $pedido->descripcion = nl2br($fields['descripcion']);
            $pedido->observaciones_publicas = nl2br($fields['observaciones_publicas']);
            $pedido->fecha_estimada_recepcion = $fields['fecha_estimada_recepcion'];
            $pedido->flete = $fields['flete'];
            $pedido->nro_factura = $fields['nro_factura'];
            $pedido->update();
            // redirect
            Session::flash('message', 'El pedido se actualizó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/pedidos/ver/'.$pedido->id_pedido);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pedido  $pedido
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pedido $pedido)
    {
        //
    }
}
