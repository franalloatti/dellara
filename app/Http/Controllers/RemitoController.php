<?php

namespace App\Http\Controllers;

use Auth;
use PDF;
use \stdClass;
use App\Pedido;
use App\Pago;
use App\TipoMovimiento;
use App\Remito;
use App\Estado;
use App\Notifications\StockBajoNotificacion;
use App\Notifications\MailStockBajoProductoNotificacion;
use App\Producto;
use App\User;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class RemitoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $fields = $request->all();
        $estados = DB::table('estados')->get();
        $estadoSelected = null;
        $search = '';
        $fecha_desde = '';
        $fecha_hasta = '';
        $clienteSelected = '';
        $clientes = DB::table('users')->where('users.deleted', 0)->whereRaw("(perfils.nombre = 'Mayorista' or perfils.nombre = 'Consumidor final')")
        ->leftJoin('perfils', 'users.perfil_id', '=', 'perfils.id_perfil')
        ->selectRaw("users.id_user, IF(perfils.nombre = 'Mayorista', users.name, concat(users.name, ' ', users.surname)) as name")->get();
        $remitos = DB::table('remitos');
        if(isset($fields['search'])){
            $search = $fields['search'];
            $remitos = $remitos->where('remitos.id_remito', 'like', '%' . $search . '%')
            ->orWhere('remitos.pedido_id_pedido', 'like', '%' . $search . '%');
        }
        if(isset($fields['fecha_desde'])){
            $fecha_desde = $fields['fecha_desde'];
            $remitos = $remitos->whereRaw("cast(remitos.created_at as date) >= cast('".$fecha_desde."' as date)");
        }
        if(isset($fields['fecha_hasta'])){
            $fecha_hasta = $fields['fecha_hasta'];
            $remitos = $remitos->whereRaw("cast(remitos.created_at as date) <= cast('".$fecha_hasta."' as date)");
        }
        if(isset($fields['estado_id']) && $fields['estado_id'] !== null){
            $estadoSelected = $fields['estado_id'];
            $remitos = $remitos->where('estados.id_estado', '=', $estadoSelected);
        }
        if(isset($fields['user_id'])){
            $clienteSelected = $fields['user_id'];
            $remitos = $remitos->where('users.id_user', '=', $clienteSelected);
        }
        $remitos = $remitos->leftJoin('pedidos', 'pedidos.id_pedido', '=', 'remitos.pedido_id_pedido')->where('pedidos.tipo', '=', 'Pedido')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('factura', 'factura.id_remito', '=', 'remitos.id_remito')
        ->leftJoin('direccions', 'direccions.id_direccion', '=', 'pedidos.direccion_id_direccion')
        ->leftJoin('perfils', 'users.perfil_id', '=', 'perfils.id_perfil')
        ->leftJoin('pago_remito', 'pago_remito.remito_id_remito', '=', 'remitos.id_remito')
        ->select('remitos.*', 'users.surname', 'users.id_user', 'estados.nombre as estado','pedidos.flete','pedidos.nro_factura','factura.Facturado','factura.id_factura as numero', 'direccions.direccion', 'direccions.id_direccion')
        ->selectRaw("IF(perfils.nombre = 'Mayorista', users.name, concat(users.name, ' ', users.surname)) as name, ifnull(sum(pago_remito.monto),0) as monto_pagos")
        ->groupBy('remitos.id_remito')
        ->orderBy('remitos.created_at', 'desc')
        ->paginate(20);
        // echo json_encode($remitos);die;
        $medios = DB::table('medios')->where('deleted', 0)->get();
        return view('remitos.list', ['clienteSelected' => $clienteSelected, 'estadoSelected' => $estadoSelected, 
        'fecha_desde' =>  $fecha_desde, 'fecha_hasta' =>  $fecha_hasta, 'estados' =>  $estados,
        'clientes' => $clientes, 'search' =>  $search, 'remitos' => $remitos, 'cuentas' => $medios]);
    }

    public function cobrar(Request $request)
    {
        $fields = $request->all();
        // echo json_encode($fields);
        // die;
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'id_remito'=>'required',
            'id_user'=>'required',
            'cuenta_id'=>'required',
            'monto'=>'required',
        );

        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to($fields['redirect_url'])
                ->withErrors($validator);
                // return view('medios.crear');
        } else {
            $tipo_movimiento = TipoMovimiento::whereRaw("nombre = 'Venta'")->get()->first();
            $pago = new Pago([
                'user_id_user' => $fields['id_user'],
                'medio_id_medio' => $fields['cuenta_id'],
                'monto' => $fields['monto'],
                'descripcion' => $fields['descripcion'],
                'estado' => 'ASIGNADO',
                'tipo_movimiento_id' => $tipo_movimiento ? $tipo_movimiento->id_tipo_movimiento : 1, 
                'fecha' =>  date('Y-m-d')
            ]);
            $pago->save();
            DB::table('pago_remito')->insert(
                [
                'pago_id_pago' => $pago->id_pago, 
                'remito_id_remito' => $fields['id_remito'], 
                'monto' => $fields['monto'], 
                'created_at' =>  date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                ]
            );
            // redirect
            Session::flash('message', 'El cobro se registró con éxito!');
            Session::flash('type', 'success');
            return Redirect::to($fields['redirect_url']);
            // return view('marcas.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->all();
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        // $ped = Pedido::where('id_pedido',$fields['id_pedido'])->with('estados')->get()->first();
        // echo json_encode($fields);
        // die;
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'id_pedido'=>'required',
            'user_id_user'=>'required',
            'direccion_id_direccion'=>'required',
            'items'=>'required'
        );

        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/pedidos/remito/'.$fields['id_pedido'])
                ->withErrors($validator);
                // return view('marcas.crear');
        } else {
            $existe_remito = Remito::where('pedido_id_pedido',$fields['id_pedido'])->get()->first();
            if(!$existe_remito){
                DB::table('pedidos')->where('id_pedido', $fields['id_pedido'])->update(['direccion_id_direccion' => $fields['direccion_id_direccion'],
                'descripcion' => nl2br($fields['descripcion']), 'observaciones_publicas' => nl2br($fields['observaciones_publicas'])]);
                $pedidoOriginal = Pedido::where('id_pedido',$fields['id_pedido'])->with('estados')->get()->first(); 
                // $pedidoOriginal->estados()->sync(['estado_id_estado' => ($pedidoOriginal->estados[count($pedidoOriginal->estados)-1]->id_estado + 1)], false);
                // $pedidoOriginal = Pedido::where('id_pedido',$fields['id_pedido'])->with('estados')->get()->first(); 
                sleep(1);
                $remito = new Remito([
                    'pedido_id_pedido' => $fields['id_pedido'],
                    'descripcion' => $fields['descripcion'],
                    'created_at' =>  date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
                $remito->save();
                $nuevoPedido = 0;
                $nuevaDevolucion = 0;
                $totalMontoRemito = 0;
                $totalMontoRemitoDevolucion = 0;
                $newPedido = new stdClass();
                $newDevolucion = new stdClass();
                $newRemitoDevolucion = new stdClass();
                foreach($fields['items'] as $item){
                    if($item['cantidad'] > 0){
                        if($item['cantidad'] < $item['cantidad_pedido']){
                            if($nuevoPedido == 0){
                                $newPedido = new Pedido([
                                    'user_id_user' => $fields['user_id_user'],
                                    'direccion_id_direccion' => $fields['direccion_id_direccion'],
                                    'created_at' =>  date('Y-m-d H:i:s'),
                                    'updated_at' => date('Y-m-d H:i:s'),
                                    'usuario_crea_id' => Auth::user()->id_user,
                                ]);
                                $newPedido->save();
                                DB::table('pedido_pedido')->insert([
                                    [
                                        'pedido_id_padre' => $fields['id_pedido'],
                                        'pedido_id_hijo' => $newPedido->id_pedido
                                    ]
                                ]);
                                $nuevoPedido = 1;
                            }
                            DB::table('pedido_producto')->insert(
                                [
                                'pedido_id_pedido' => $newPedido->id_pedido, 
                                'producto_id_producto' => $item['id_producto'], 
                                'cantidad' => $item['cantidad_pedido'] - $item['cantidad'], 
                                'iva' => $item['iva'], 
                                'descuento' => $item['descuento'], 
                                'costo_unitario' => $item['costo_unitario'],
                                'precio_unitario' => $item['precio_unitario'],
                                'created_at' =>  date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s'),
                                ]
                            );
                        }
                        $totalMontoRemito = $totalMontoRemito + floatval(isset($item['precio_final_con_iva']) ? $item['precio_final_con_iva'] : 0);
                        DB::table('producto_remito')->insert(
                            [
                            'tipo_item' => $item['tipo_item'], 
                            'remito_id_remito' => $remito->id_remito, 
                            'producto_id_producto' => $item['id'], 
                            'cantidad' => $item['cantidad'], 
                            'almacen_id_almacen' => $item['almacen_id'], 
                            'iva' => $item['iva'], 
                            'descuento' => isset($item['descuento']) ? $item['descuento'] : 0, 
                            'costo_unitario' => $item['costo_unitario'],
                            'precio_unitario' => isset($item['precio_unitario']) ? $item['precio_unitario'] : 0,
                            'created_at' =>  date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                            ]
                        );
                        if($item['tipo_item'] == 'devolucion'){
                            if($nuevaDevolucion == 0){
                                $newDevolucion = new Pedido([
                                    'tipo' => 'Devolucion',
                                    'motivo_devolucion' => 'defecto',
                                    'user_id_user' => $fields['user_id_user'],
                                    'direccion_id_direccion' => $fields['direccion_id_direccion'],
                                    'created_at' =>  date('Y-m-d H:i:s'),
                                    'updated_at' => date('Y-m-d H:i:s'),
                                    'usuario_crea_id' => Auth::user()->id_user,
                                ]);
                                $estado = Estado::where('id_estado',3)->get()->first();
                                $newDevolucion->save();
                                $newDevolucion->estados()->sync(['estado_id_estado' => $estado->id_estado]);
                                $newRemitoDevolucion = new Remito([
                                    'pedido_id_pedido' => $newDevolucion->id_pedido
                                ]);
                                $newRemitoDevolucion->save();
                                $nuevaDevolucion = 1;
                            }
                            $id_pedido_producto = DB::table('pedido_producto')->insertGetId(
                                [
                                'tipo_item' => 'devolucion', 
                                'pedido_id_pedido' => $newDevolucion->id_pedido, 
                                'producto_id_producto' => $item['id_producto'], 
                                'cantidad' => $item['cantidad'], 
                                'iva' => 0, 
                                'descuento' => 0, 
                                'costo_unitario' => $item['costo_unitario'],
                                'precio_unitario' => 0,
                                'created_at' =>  date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s'),
                                ]
                            );
                            $totalMontoRemitoDevolucion = $totalMontoRemitoDevolucion + ($item['cantidad'] * $item['costo_unitario']);
                            DB::table('producto_remito')->insert(
                                [
                                'tipo_item' => 'devolucion', 
                                'remito_id_remito' => $newRemitoDevolucion->id_remito, 
                                'producto_id_producto' => $id_pedido_producto, 
                                'cantidad' => $item['cantidad'], 
                                'iva' => 0, 
                                'descuento' => 0, 
                                'costo_unitario' => $item['costo_unitario'],
                                'precio_unitario' => 0,
                                'created_at' =>  date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s'),
                                ]
                            );
                        }
                        // DB::table('almacen_producto')->where('producto_id_producto', $item['id_producto'])->where('almacen_id_almacen', $item['almacen_id'])->decrement('cantidad', $item['cantidad']);
                        $almacen_producto = DB::table('almacen_producto')->where('almacen_producto.producto_id_producto', $item['id_producto'])->where('almacen_producto.almacen_id_almacen', $item['almacen_id'])
                        ->leftJoin(DB::raw("(SELECT M.almacen_id_destino, M.cantidad FROM almacen_producto AP
                        left join movimiento_stock M on AP.id_stock = M.almacen_id_destino
                        join productos P on P.id_producto = AP.producto_id_producto
                        where M.tipo_movimiento <> 'Ajuste' and cast(M.created_at as date) = cast(NOW() as date) 
                        and (P.tipo <> 'Insumo' or M.tipo_movimiento <> 'Producción') 
                        order by M.created_at desc) as MOV_DEST"), 'almacen_producto.id_stock', '=', 'MOV_DEST.almacen_id_destino')
                        ->leftJoin(DB::raw("(SELECT M.almacen_id_origen, if(M.tipo_movimiento = 'Ajuste', M.cantidad,-1*M.cantidad) AS 'cantidad' FROM movimiento_stock M
                        join almacen_producto AP on AP.id_stock = M.almacen_id_origen
                        join productos P on P.id_producto = AP.producto_id_producto
                        where cast(M.created_at as date) = cast(NOW() as date) 
                        and (P.tipo <> 'Producto' or M.tipo_movimiento <> 'Producción') 
                        order by M.created_at desc) as MOV_ORI"), 'almacen_producto.id_stock', 'MOV_ORI.almacen_id_origen')
                        ->leftJoin(DB::raw("(SELECT P.tipo, P.motivo_devolucion, PP.producto_id_producto, PR.almacen_id_almacen, sum(IF(P.tipo = 'Pedido' or P.tipo = 'Devolucion compra', -1*PR.cantidad,PR.cantidad)) as 'cantidad' FROM producto_remito PR
                        join pedido_producto PP on PR.producto_id_producto = PP.id_pedido_producto
                        join pedidos P on PP.pedido_id_pedido = P.id_pedido
                        where cast(PR.created_at as date) = cast(NOW() as date) and ((P.tipo <> 'Devolucion') or (P.tipo = 'Devolucion' and P.motivo_devolucion <> 'defecto'))
                        group by P.tipo, PR.almacen_id_almacen, PP.producto_id_producto
                        order by PR.created_at desc) as ITM_REM"), 
                            function($join){
                                $join->on('almacen_producto.almacen_id_almacen', '=', 'ITM_REM.almacen_id_almacen')
                                ->on('almacen_producto.producto_id_producto', '=', 'ITM_REM.producto_id_producto');
                            })
                        ->selectRaw("(ifnull(almacen_producto.cantidad,0.00) + ifnull(MOV_DEST.cantidad,0.00) + ifnull(MOV_ORI.cantidad,0.00) + 
                        if(ITM_REM.tipo = 'Pedido', ITM_REM.cantidad,0.00) + if(ITM_REM.tipo = 'Devolucion', ITM_REM.cantidad,0.00) + 
                        if(ITM_REM.tipo = 'Compra', ITM_REM.cantidad,0.00) + if(ITM_REM.tipo = 'Devolucion compra', ITM_REM.cantidad,0.00)) as cantidad")
                        ->get()->first();
                        $producto = Producto::where('id_producto', $item['id_producto'])->get()->first();
                        if($item['tipo_item'] == 'venta' && $producto->tiene_fijado && env('PRECIO_FIJADO_VARIABLE_VENTA', false)){
                            $precio = $item['precio_unitario'];
                            $producto->precio_fijado = $precio;
                            $producto->save();
                        }
                        if($producto->stock_seguridad > $almacen_producto->cantidad){
                            $user = User::where('email', '=', env('MAIL_TO_ADDRESS', '---'))->get()->first();
                            if($user){
                                $user->notify(new StockBajoNotificacion($producto, $almacen_producto->cantidad));
                                if(env('MAIL_STOCK_FALTANTE_X_PRODUCTO', false)){
                                    $user->notify(new MailStockBajoProductoNotificacion($producto, $almacen_producto->cantidad));
                                }
                            }
                            if(env('MAIL_TO_ADDRESS_COPY', '') != ''){
                                $user = User::where('email', '=', env('MAIL_TO_ADDRESS_COPY', '---'))->get()->first();
                                if($user){
                                    $user->notify(new StockBajoNotificacion($producto, $almacen_producto->cantidad));
                                    if(env('MAIL_STOCK_FALTANTE_X_PRODUCTO', false)){
                                        $user->notify(new MailStockBajoProductoNotificacion($producto, $almacen_producto->cantidad));
                                    }
                                }
                            }
                        }
                    }else{
                        if($nuevoPedido == 0){
                            $newPedido = new Pedido([
                                'user_id_user' => $fields['user_id_user'],
                                'direccion_id_direccion' => $fields['direccion_id_direccion'],
                                'created_at' =>  date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s'),
                                'usuario_crea_id' => Auth::user()->id_user,
                            ]);
                            $newPedido->save();
                            DB::table('pedido_pedido')->insert([
                                [
                                    'pedido_id_padre' => $fields['id_pedido'],
                                    'pedido_id_hijo' => $newPedido->id_pedido
                                ]
                            ]);
                            $nuevoPedido = 1;
                        }
                        DB::table('pedido_producto')->where('id_pedido_producto', $item['id'])->update(['pedido_id_pedido' => $newPedido->id_pedido]);
                    }
                }           
                if($nuevoPedido){
                    $newPedido->estados()->sync(['estado_id_estado' => 1], false);
                }           
                if($nuevaDevolucion){
                    $newRemitoDevolucion->update(['monto' => $totalMontoRemitoDevolucion]);
                }
                // $pedidoOriginal->estados()->sync(['estado_id_estado' => 3], false);
                $remito->update(['monto' => $totalMontoRemito]);
                // redirect
                Session::flash('message', 'El remito se generó con éxito!');
                Session::flash('type', 'success');
                return Redirect::to('admin/remitos/index');
            }else{
                Session::flash('message', 'Ya existe un remito del pedido (Nro remito: '.$existe_remito->id_remito.')');
                Session::flash('type', 'danger');
                return Redirect::to('admin/remitos/ver/'.$existe_remito->id_remito);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Remito  $remito
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $remito = DB::table('remitos')->where('remitos.id_remito', $id)
        ->leftJoin('producto_remito', 'producto_remito.remito_id_remito', '=', 'remitos.id_remito')
        ->leftJoin('pedido_producto', 'pedido_producto.producto_id_producto', '=', 'producto_remito.producto_id_producto')
        ->leftJoin('pedidos', 'pedidos.id_pedido', '=', 'remitos.pedido_id_pedido')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('perfils', 'users.perfil_id', '=', 'perfils.id_perfil')
        ->leftJoin('direccions', 'direccions.id_direccion', '=', 'pedidos.direccion_id_direccion')
        ->leftJoin(DB::raw("(SELECT remito_id_remito, ifnull(sum(monto),0) as monto FROM pago_remito where remito_id_remito = ".$id." 
                group by remito_id_remito) as PAGOS"), 'PAGOS.remito_id_remito', '=', 'remitos.id_remito')
        ->select('remitos.*', 'direccions.direccion', 'direccions.localidad', 'direccions.provincia', 'users.id_user', 'pedidos.descripcion as observaciones_privadas', 'pedidos.observaciones_publicas', 'estados.nombre as estado')
        ->selectRaw("IF(perfils.nombre = 'Mayorista', users.name, concat(users.name, ' ', users.surname)) as name, ifnull(PAGOS.monto,0) as monto_pagos")
        ->groupBy('remitos.id_remito')
        ->get()->first();
        // echo json_encode($remito);die;
        $productos = DB::table('producto_remito')->where('producto_remito.remito_id_remito', $id)
        ->leftJoin('pedido_producto', 'producto_remito.producto_id_producto', '=', 'pedido_producto.id_pedido_producto')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->join('almacen_producto', 'almacen_producto.producto_id_producto', '=', 'pedido_producto.producto_id_producto')
        ->select('producto_remito.*','productos.nombre','productos.codigo')
        ->groupBy('pedido_producto.producto_id_producto', 'producto_remito.tipo_item')
        ->orderBy('productos.codigo')->orderBy('producto_remito.tipo_item', 'DESC')
        ->get();
        $medios = DB::table('medios')->where('deleted', 0)->get();
        return view('remitos.show', ['remito' => $remito, 'productos' => $productos, 'cuentas' => $medios]);
    }

    public function imprimir($id)
    {
        $remito = DB::table('remitos')->where('remitos.id_remito', $id)
        ->leftJoin('producto_remito', 'producto_remito.remito_id_remito', '=', 'remitos.id_remito')
        ->leftJoin('pedido_producto', 'pedido_producto.producto_id_producto', '=', 'producto_remito.producto_id_producto')
        ->leftJoin('pedidos', 'pedidos.id_pedido', '=', 'remitos.pedido_id_pedido')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('direccions', 'direccions.id_direccion', '=', 'pedidos.direccion_id_direccion')
        ->select('remitos.*', 'pedidos.observaciones_publicas', 'users.name', 'users.responsabilidad', 'users.surname', 'users.cuit', 
        'direccions.direccion', 'direccions.localidad', 'direccions.provincia', 'pedidos.flete', 'pedidos.nro_factura')
        ->groupBy('pedidos.id_pedido')
        ->get()->first();
        $productos = DB::table('producto_remito')->where('producto_remito.remito_id_remito', $id)
        ->leftJoin('pedido_producto', 'producto_remito.producto_id_producto', '=', 'pedido_producto.id_pedido_producto')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->join('almacen_producto', 'almacen_producto.producto_id_producto', '=', 'pedido_producto.producto_id_producto')
        ->select('producto_remito.*','productos.nombre','productos.codigo')
        ->get();
        // return view('remitos.imprimir', ['remito' => $remito, 'productos' => $productos]);
        $pdf = PDF::loadView('remitos.imprimir', ['remito' => $remito, 'productos' => $productos]);
        return $pdf->download('remito-'.$id.'.pdf');
    }

    public function imprimirtransporte($id)
    {
        $remito = DB::table('remitos')->where('remitos.id_remito', $id)
        ->leftJoin('producto_remito', 'producto_remito.remito_id_remito', '=', 'remitos.id_remito')
        ->leftJoin('pedido_producto', 'pedido_producto.producto_id_producto', '=', 'producto_remito.producto_id_producto')
        ->leftJoin('pedidos', 'pedidos.id_pedido', '=', 'remitos.pedido_id_pedido')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('direccions', 'direccions.id_direccion', '=', 'pedidos.direccion_id_direccion')
        ->select('remitos.*', 'pedidos.observaciones_publicas', 'users.name', 'users.responsabilidad', 'users.surname', 'users.cuit', 
        'direccions.direccion', 'direccions.localidad', 'direccions.provincia', 'pedidos.flete', 'pedidos.nro_factura')
        ->groupBy('pedidos.id_pedido')
        ->get()->first();
        $productos = DB::table('producto_remito')->where('producto_remito.remito_id_remito', $id)
        ->leftJoin('pedido_producto', 'producto_remito.producto_id_producto', '=', 'pedido_producto.id_pedido_producto')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->join('almacen_producto', 'almacen_producto.producto_id_producto', '=', 'pedido_producto.producto_id_producto')
        ->select('producto_remito.*','productos.nombre','productos.codigo')
        ->get();
        // return view('remitos.imprimir', ['remito' => $remito, 'productos' => $productos]);
        $pdf = PDF::loadView('remitos.imprimirtransporte', ['remito' => $remito, 'productos' => $productos]);
        return $pdf->download('remitoTransporte-'.$id.'.pdf');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Remito  $remito
     * @return \Illuminate\Http\Response
     */
    public function edit(Remito $remito)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Remito  $remito
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Remito $remito)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Remito  $remito
     * @return \Illuminate\Http\Response
     */
    public function destroy(Remito $remito)
    {
        //
    }
}
