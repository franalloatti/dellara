<?php

namespace App\Http\Controllers;

use Auth;
use App\Pago;
use App\TipoMovimiento;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\MovimientosExport;

class MovimientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $fields = $request->all();
        $tipo_movimientos = DB::table('tipo_movimientos')->whereRaw("tipo_movimientos.nombre <> 'Venta' and tipo_movimientos.periodo = 0")
        ->whereRaw("tipo_movimientos.nombre <> 'Compra productos'")->orderBy('nombre')->get();
        $cuentas = DB::table('medios')->where('medios.deleted', 0)
        ->leftJoin(DB::raw("(SELECT ARQ1.cuenta_id, ARQ1.monto as 'monto_arqueo', ARQ1.created_at as 'fecha_max' FROM arqueos as ARQ1 where ARQ1.created_at = 
        (select max(ARQ2.created_at) from arqueos as ARQ2 where ARQ1.cuenta_id = ARQ2.cuenta_id group by ARQ2.cuenta_id)) as ARQ") , 'ARQ.cuenta_id', '=', 'medios.id_medio')
        ->select('medios.*')->selectRaw("cast(ARQ.fecha_max as date) as fecha_max")->orderBy('medios.nombre')->get();
        $search = '';
        $fecha_desde = '';
        $fecha_hasta = '';
        $tipoMovSelected = null;
        $cuentaSelected = null;
        $pagos = DB::table('pagos')->where('pagos.deleted', 0)
        ->whereRaw("tipo_movimientos.nombre <> 'Venta'")->whereRaw("tipo_movimientos.nombre <> 'Compra productos'");
        if(isset($fields['tipo_movimiento_id'])){
            $tipoMovSelected = $fields['tipo_movimiento_id'];
            $pagos = $pagos->where('pagos.tipo_movimiento_id', $tipoMovSelected);
        }
        if(isset($fields['search'])){
            $search = $fields['search'];
            $pagos = $pagos->whereRaw("(pagos.nombre like '%" . $search . "%' or pagos.descripcion like '%" . $search . "%')");
        }
        if(isset($fields['cuenta_id'])){
            $cuentaSelected = $fields['cuenta_id'];
            $pagos = $pagos->where('medios.id_medio', $cuentaSelected);
        }
        if(isset($fields['fecha_desde'])){
            $fecha_desde = $fields['fecha_desde'];
            $pagos = $pagos->whereRaw("cast(pagos.fecha as date) >= cast('".$fecha_desde."' as date)");
        }
        if(isset($fields['fecha_hasta'])){
            $fecha_hasta = $fields['fecha_hasta'];
            $pagos = $pagos->whereRaw("cast(pagos.fecha as date) <= cast('".$fecha_hasta."' as date)");
        }
        $pagos = $pagos->leftJoin('tipo_movimientos', 'tipo_movimientos.id_tipo_movimiento', '=', 'pagos.tipo_movimiento_id')
        ->leftJoin('medios', 'medios.id_medio', '=', 'pagos.medio_id_medio')->where('medios.deleted', 0)
        ->select('pagos.*', 'tipo_movimientos.nombre as tipo', 'tipo_movimientos.nombre as tipo_movimiento')
        ->selectRaw('medios.nombre as cuenta')
        ->orderBy('pagos.created_at', 'desc')
        ->paginate(10);
        // echo json_encode($pagos);die;
        return view('movimientos.list', ['pagos' => $pagos, 'tipo_movimientos' => $tipo_movimientos, 'fecha_desde' => $fecha_desde, 'search' => $search,
        'fecha_hasta' => $fecha_hasta, 'tipoMovSelected' => $tipoMovSelected,'cuentaSelected' => $cuentaSelected, 'cuentas' => $cuentas]);
    }

    public function getreporte(Request $request)
    {
        $fields = $request->all();
        $fecha_desde = $fields['fecha_desde'];
        $fecha_hasta = $fields['fecha_hasta'];
        return Excel::download(new MovimientosExport($fecha_desde, $fecha_hasta), 
        'Movimientos_'.date('d-m-Y', strtotime($fecha_desde)).'_'.date('d-m-Y', strtotime($fecha_hasta)).'.xlsx');
    }

    public function mispagos()
    {
        $pagos = DB::table('pagos')->where('pagos.deleted', 0)->where('pagos.user_id_user', '=', Auth::user()->id_user)
        ->select('pagos.*')
        ->orderBy('pagos.created_at', 'desc')
        ->paginate(10);
        return view('movimientos.mispagos', ['pagos' => $pagos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $medios = DB::table('medios')->where('deleted', 0)->get();
        $clientes = DB::table('users')->where('users.deleted', 0)->where('roles.name', '<>', 'admin')->where('roles.name', '<>', 'administrador')->where('roles.name', '<>', 'Proveedor')
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->leftJoin('perfils', 'users.perfil_id', '=', 'perfils.id_perfil')
        ->select('users.id_user', 'roles.discount as descuento')
        ->selectRaw("IF(perfils.nombre = 'Mayorista', users.name, concat(users.name, ' ', users.surname)) as name")->get();
        return view('movimientos.create', ['medios' => $medios, 'clientes' => $clientes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->all();
        // echo json_encode($fields);die;
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'tipo_mov_id'=>'required',
            'nombre'=>'required',
            'cuenta_origen_id'=>'required',
            'monto'=>'required',
            'fecha'=>'required',
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/movimientos/index')
                ->withErrors($validator);
                // return view('medios.crear');
        } else {
            $tipo_movimiento = TipoMovimiento::where('id_tipo_movimiento',$fields['tipo_mov_id'])->get()->first();
            if($tipo_movimiento->tipo == 'Ingreso'){
                $signo = 1;
            }else{
                $signo = -1;
            }
            $pago = new Pago([
                'user_id_user' => Auth::user()->id_user,
                'medio_id_medio' => $request->get('cuenta_origen_id'),
                'monto' => $signo * $request->get('monto'),
                'descripcion' => $request->get('descripcion'),
                'estado' => 'PENDIENTE',
                'tipo_movimiento_id' => $request->get('tipo_mov_id'),
                'nombre' => $request->get('nombre'),
                'fecha' => $request->get('fecha')
            ]);
            $pago->save();
            if($tipo_movimiento->tipo == 'Transferencia') {
                $pago = new Pago([
                    'user_id_user' => Auth::user()->id_user,
                    'medio_id_medio' => $request->get('cuenta_destino_id'),
                    'monto' => $request->get('monto'),
                    'descripcion' => $request->get('descripcion'),
                    'estado' => 'PENDIENTE',
                    'tipo_movimiento_id' => $request->get('tipo_mov_id'),
                    'nombre' => $request->get('nombre'),
                    'fecha' => $request->get('fecha')
                ]);
                $pago->save();
            }
            // redirect
            Session::flash('message', 'El movimiento se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/movimientos/index');
            // return view('marcas.index');
        }
    }

    public function nuevopago()
    {
        $medios = DB::table('medios')->where('deleted', 0)->where('publica', 1)->get();
        return view('movimientos.nuevopago', ['medios' => $medios]);
    }

    public function addpago(Request $request)
    {
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'medio_id_medio'=>'required',
            'monto'=>'required',
            'comprobante'=>'max:2048',
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/movimientos/crear')
                ->withErrors($validator);
                // return view('medios.crear');
        } else {
            $pago = new Pago([
                'user_id_user' => Auth::user()->id_user,
                'medio_id_medio' => $request->get('medio_id_medio'),
                'monto' => $request->get('monto'),
                'descripcion' => $request->get('descripcion'),
                'estado' => 'PENDIENTE'
            ]);
            if($request->hasFile('comprobante')) {
                $fileUpload = $request->file('comprobante');
                $ext = $fileUpload->guessExtension();
                $filename = md5(date('Y-m-d H:i:s:u')).".".$ext;
                $pago->comprobante = $filename;
                $fileUpload->move('images/comprobantes/',$filename);
            }
            $pago->save();
            // redirect
            Session::flash('message', 'El movimiento se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('mispagos');
            // return view('marcas.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pago  $pago
     * @return \Illuminate\Http\Response
     */
    public function show(Pago $pago)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pago  $pago
     * @return \Illuminate\Http\Response
     */
    public function asignar($id)
    {
        $pago = Pago::where('id_pago',$id)->where('pagos.deleted', 0)
        ->leftJoin('pago_remito', 'pago_remito.pago_id_pago', '=', 'pagos.id_pago')
        ->select('pagos.*')
        ->selectRaw('sum(ifnull(`pago_remito`.`monto`,0)) as pagado')
        ->groupBy('pagos.id_pago')
        ->get()->first();
        $remitos = DB::table('remitos')->where('users.id_user', $pago->user_id_user)
        ->leftJoin('pedidos', 'pedidos.id_pedido', '=', 'remitos.pedido_id_pedido')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('pago_remito', 'pago_remito.remito_id_remito', '=', 'remitos.id_remito')
        ->select('remitos.*', 'users.name', 'users.surname')
        ->selectRaw('`remitos`.`monto` - sum(ifnull(`pago_remito`.`monto`,0)) as falta_pagar')
        ->groupBy('remitos.id_remito')
        ->orderBy('remitos.created_at')
        ->havingRaw('(`remitos`.`monto` - sum(ifnull(`pago_remito`.`monto`,0))) > 0')->get();
        return view('movimientos.asignar', ['pago' => $pago, 'remitos' => $remitos, 'id_cliente' => $pago->user_id_user]);
    }

    public function asignarsuccess(Request $request, $id_pago)
    {
        $fields = $request->all();
        $monto_pago = $fields['monto_pago'];
        $id_cliente = $fields['id_cliente'];
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        // echo json_encode($fields);die;
        foreach($fields['items'] as $item){
            if($monto_pago > 0){
                if(isset($item['checkbox'])){
                    if($monto_pago >= $item['monto']){
                        $monto_a_pagar = $item['monto'];
                        $monto_pago = $monto_pago - $item['monto'];
                    }else{
                        $monto_a_pagar = $monto_pago;
                        $monto_pago = 0;
                    }
                    DB::table('pago_remito')->insert(
                        [
                        'pago_id_pago' => $id_pago, 
                        'remito_id_remito' => $item['id_remito'], 
                        'monto' => $monto_a_pagar, 
                        'created_at' =>  date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        ]
                    );
                }
            }else {
                break;
            }
        }
        $pago = Pago::where('id_pago',$id_pago)->get()->first();
        if($monto_pago > 0){
            $pago->estado = 'ASIGNADO PARCIAL';
        }else{
            $pago->estado = 'ASIGNADO';
        }
        $pago->save();
        Session::flash('message', 'El movimiento se asignó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/clientes/cuentaampliado/'.$id_cliente);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pago  $pago
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pago = Pago::where('id_pago',$id)->get()->first();
        $medios = DB::table('medios')->where('deleted', 0)->get();
        $clientes = DB::table('users')->where('users.deleted', 0)->where('roles.name', '<>', 'admin')->where('roles.name', '<>', 'administrador')->where('roles.name', '<>', 'Proveedor')
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->leftJoin('perfils', 'users.perfil_id', '=', 'perfils.id_perfil')
        ->select('users.id_user', 'roles.discount as descuento')
        ->selectRaw("IF(perfils.nombre = 'Mayorista', users.name, concat(users.name, ' ', users.surname)) as name")->get();
        return view('movimientos.edit', ['pago' => $pago, 'medios' => $medios, 'clientes' => $clientes]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pago  $pago
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $fields = $request->all();
        // echo json_encode($fields);die;
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'id_movimiento'=>'required',
            'cuenta_origen_id_edit'=>'required',
            'nombre_edit'=>'required',
            'monto_edit'=>'required',
            'fecha_edit'=>'required',
        );
        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/movimientos/index')
                ->withErrors($validator);
                // return view('medios.crear');
        } else {
            $pago = Pago::where('id_pago',$fields['id_movimiento'])->get()->first();
            $pago->nombre = $fields['nombre_edit'];
            $pago->fecha = $fields['fecha_edit'];
            $pago->medio_id_medio = $fields['cuenta_origen_id_edit'];
            $pago->monto = -1*$fields['monto_edit'];
            $pago->descripcion = $fields['descripcion_edit'];
            $pago->save();
            // redirect
            Session::flash('message', 'El movimiento se actualizó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/movimientos/index');
            // return view('marcas.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pago  $pago
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $pago = Pago::find($id);
        $pago->deleted = 1;
        $pago->save();

        // redirect
        Session::flash('message', 'El movimiento se eliminó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/movimientos/index');
        // return view('movimientos.index');
    }
}
