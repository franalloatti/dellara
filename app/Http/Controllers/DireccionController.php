<?php

namespace App\Http\Controllers;

use Afip;
use App\Direccion;
use App\Empresa;
use App\User;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class DireccionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_user)
    {
        return view('direcciones.create', ['id_user' => $id_user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id_user)
    {
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'direccion'=>'required',
            'provincia'=>'required',
            'localidad'=>'required',
            'codigo_postal'=>'required',
            // 'telefono'=>'required',
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/direcciones/crear/'.$id_user)
                ->withErrors($validator);
                // return view('direcciones.crear');
        } else {
            $existeDireccionSeleccionado = DB::table('direccions')->where('user_id', $id_user)->where('es_direccion_fact', 1)->get()->first();
            if(empty($existeDireccionSeleccionado)){
                $es_facturacion = 1;
            }else{
                $es_facturacion = 0;
            }
            $direccion = new Direccion([
                'direccion' => $request->get('direccion'),
                'observaciones' => $request->get('observaciones'),
                'provincia' => $request->get('provincia'),
                'localidad' => $request->get('localidad'),
                'codigo_postal' => $request->get('codigo_postal'),
                'telefono' => $request->get('telefono'),
                'es_direccion_fact' => $es_facturacion,
                'user_id' => $id_user
            ]);
            $direccion->save();
            // redirect
            Session::flash('message', 'La dirección se creó con éxito!');
            Session::flash('type', 'success');
            $perfilUser = DB::table('users')->where('users.id_user', '=', $id_user)
            ->leftJoin('perfils', 'perfils.id_perfil', '=', 'users.perfil_id')
            ->select('perfils.nombre')->get()->first();
            if($perfilUser->nombre == 'Proveedor'){
                return Redirect::to('admin/proveedores/ver/'.$id_user);
            }else{
                return Redirect::to('admin/clientes/ver/'.$id_user);
            }
            // return view('clientes.ver', [$id_user]);
        }
    }
    
    public function crearAutomaticamente($id_user)
    {
        // Usuario al que le queremos buscar datos de afip
        $user = User::find($id_user);
        // Datos de la empresa para buscar la dirección
        $empresa = Empresa::find(1);
        // Documento ya sea DNI o CUIT a buscar datos
        $nroDocumento = $user->role == 'consumidor final' ? $user->documento : $user->cuit;
        //1) Con nroDocumento busco en afip los datos de las direcciones
        $afip = new Afip(array('CUIT' => $empresa->cuit, 'production' => $empresa->produccion ? true : false));
        $taxpayer_details = $afip->RegisterScopeThirteen->GetTaxpayerDetails($nroDocumento); //Devuelve los datos del contribuyente correspondiente al identificador    
        //2) Me fijo si el array NO está vacío, si es así, operamos
        if(!is_null($taxpayer_details)){
            //Seleccionamos los datos correspondientes a los domicilios
            $domicilios = $taxpayer_details->domicilio;
            //3) Sino, recorro  con un for los datos y los creo uno por uno
            $contador = 0;
            foreach($domicilios as $domicilio){
                // Consultamos que no exista ya esta dirección para este usuario
                $existeDireccion = DB::table('direccions')->where('user_id', $id_user)->where('direccion',$domicilio->direccion)->get()->first();
                if(empty($existeDireccion)){ //No existe, por lo tannto la creamos
                    $es_facturacion = $domicilio->tipoDomicilio == 'FISCAL' ? 1 : 0;
                    $direccion = new Direccion([
                        'direccion' => $domicilio->direccion,
                        'observaciones' => NULL,
                        'provincia' => $domicilio->descripcionProvincia,
                        'localidad' => array_key_exists('localidad',$domicilios) ? $domicilio->localidad : $domicilio->descripcionProvincia,
                        'codigo_postal' => $domicilio->codigoPostal,
                        'telefono' => $user->telefono,
                        'es_direccion_fact' => $es_facturacion,
                        'user_id' => $id_user
                    ]);
                    $direccion->save();
                    $contador++;
                }            }
            $message = "Se agregaron ".$contador." direcciones con éxito";
        }elseif(is_null($taxpayer_details)){ // si es NULL enotnces, está vació y AFIP anda
            // ERROR DOCUMENTO
            $message = "No se encontraron resultados de direcciones con el identificador:  ".$nroDocumento;
        }else{ //Si no se cumplen esas opciones AFIP, está caído...
            // ERROR AFIP
            $message = "No se a podido obtener conexión con  el servidor, por favor vuelvalo a intentar más tarde";
        }

        // Retornamos a la vista
        $roleUser = DB::table('users')->where('users.id_user', '=', $id_user)
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->select('roles.name')->get()->first();
        if($roleUser->name = 'proveedor'){
            return Redirect::to('admin/proveedores/ver/'.$id_user)->withErrors([$message]);
        }else{
            return Redirect::to('admin/clientes/ver/'.$id_user)->withErrors([$message]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Direccion  $direccion
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $direccion = DB::table('direccions')->where('direccions.id_direccion', '=', $id)->get()->first();
        $roleUser = DB::table('direccions')->where('direccions.id_direccion', '=', $id)
        ->leftJoin('users', 'direccions.user_id', '=', 'users.id_user')
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->select('roles.name')->get()->first();
        // echo var_dump($roleUser);die;
        return view('direcciones.show', ['direccion' => $direccion, 'roleUser' => $roleUser->name]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Direccion  $direccion
     * @return \Illuminate\Http\Response
     */
    public function edit(Direccion $direccion)
    {
        //
    }

    public function seleccionardireccionfact(Request $request, $id_direccion, $id_user)
    {
        DB::table('direccions')->where('user_id', $id_user)->update(['es_direccion_fact' => 0]);
        DB::table('direccions')->where('id_direccion', $id_direccion)->update(['es_direccion_fact' => 1]);
        Session::flash('message', 'El cambio se realizó con éxito!');
        Session::flash('type', 'success');
        $perfilUser = DB::table('users')->where('users.id_user', '=', $id_user)
        ->leftJoin('perfils', 'perfils.id_perfil', '=', 'users.perfil_id')
        ->select('perfils.nombre')->get()->first();
        if($perfilUser->nombre == 'Proveedor'){
            return Redirect::to('admin/proveedores/ver/'.$id_user);
        }else{
            return Redirect::to('admin/clientes/ver/'.$id_user);
        }
        // return Redirect::to('admin/clientes/ver/'.$id_user);
    }

    public function cambiarestado(Request $request, $id_direccion, $id_user, $estado)
    {
        if($estado){
            $newEstado = 0;
        }else{
            $newEstado = 1;
        }
        DB::table('direccions')->where('id_direccion', $id_direccion)->where('user_id', $id_user)->update(['estado' => $newEstado]);
        Session::flash('message', 'El cambio se realizó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/clientes/ver/'.$id_user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Direccion  $direccion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_user)
    {
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'direccion'=>'required',
            'provincia'=>'required',
            'localidad'=>'required',
            'codigo_postal'=>'required',
            // 'telefono'=>'required',
        );
        
        $perfilUser = DB::table('users')->where('users.id_user', '=', $id_user)
        ->leftJoin('perfils', 'perfils.id_perfil', '=', 'users.perfil_id')
        ->select('perfils.nombre')->get()->first();
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            if($perfilUser->nombre == 'Proveedor'){
                return Redirect::to('admin/proveedores/ver/'.$id_user)
                    ->withErrors($validator);
            }else{
                return Redirect::to('admin/clientes/ver/'.$id_user)
                    ->withErrors($validator);
            }
        } else {
            DB::table('direccions')->where('id_direccion', $request->get('id_direccion'))
            ->update([
                'direccion' => $request->get('direccion'),
                'observaciones' => $request->get('observaciones'),
                'provincia' => $request->get('provincia'),
                'localidad' => $request->get('localidad'),
                'codigo_postal' => $request->get('codigo_postal'),
                'telefono' => $request->get('telefono')
            ]);
            // redirect
            Session::flash('message', 'La dirección se editó con éxito!');
            Session::flash('type', 'success');
            if($perfilUser->nombre == 'Proveedor'){
                return Redirect::to('admin/proveedores/ver/'.$id_user);
            }else{
                return Redirect::to('admin/clientes/ver/'.$id_user);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Direccion  $direccion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Direccion $direccion)
    {
        //
    }
}
