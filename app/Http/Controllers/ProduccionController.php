<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Validator;
use Auth;

class ProduccionController extends Controller
{
    public function index(Request $request)
    {
        $fields = $request->all();
        $limit = isset($fields['limit']) ? $fields['limit'] : 20;
        $estados = DB::table('estado_produccion')->get();
        $search = '';
        $fecha_desde = '';
        $fecha_hasta = '';
        $estadoSelected = null;
        $producciones = DB::table('plan_produccion')
        ->join('productos', 'productos.id_producto', '=', 'plan_produccion.producto_id')
        ->join('estado_produccion', 'estado_produccion.id_estado_produccion', '=', 'plan_produccion.estado_id');
        if(isset($fields['search'])){
            $search = $fields['search'];
            $producciones = $producciones->where('plan_produccion.nro_lote', 'like', '%' . $search . '%');
        }
        if(isset($fields['fecha_desde'])){
            $fecha_desde = $fields['fecha_desde'];
            $producciones = $producciones->whereRaw("cast(plan_produccion.fecha_produccion as date) >= cast('".$fecha_desde."' as date)");
        }
        if(isset($fields['fecha_hasta'])){
            $fecha_hasta = $fields['fecha_hasta'];
            $producciones = $producciones->whereRaw("cast(plan_produccion.fecha_produccion as date) <= cast('".$fecha_hasta."' as date)");
        }
        if(isset($fields['estado_id']) && $fields['estado_id'] !== null){
            $estadoSelected = $fields['estado_id'];
            $producciones = $producciones->where('plan_produccion.estado_id', '=', $estadoSelected);
        }
        $producciones = $producciones
        ->select('plan_produccion.*', 'productos.codigo', 'productos.nombre', 'estado_produccion.nombre as estado')
        ->paginate($limit);
        // echo json_encode($productos);die;
        return view('produccion.list', ['limit' =>  $limit, 'estados' =>  $estados, 'estadoSelected' =>  $estadoSelected, 
        'search' =>  $search, 'producciones' => $producciones, 'fecha_desde' =>  $fecha_desde, 'fecha_hasta' =>  $fecha_hasta]);
    }

/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $productos = DB::table('productos')->where('productos.deleted', 0)->where('productos.estado', 1)
        ->where('productos.tipo', 'Producto')
        ->leftJoin(DB::raw("(SELECT P.id_producto, sum(PP.cantidad) as 'cantidad' FROM productos P 
        left join pedido_producto PP on P.id_producto = PP.producto_id_producto
        left join pedidos PED on PED.id_pedido = PP.pedido_id_pedido
        left join estado_pedido ES on PED.id_pedido = ES.pedido_id_pedido 
        and ES.created_at = (SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = PED.id_pedido)
        left join estados E on E.id_estado = ES.estado_id_estado
        where P.tipo = 'Producto' and PED.tipo = 'Pedido' and (E.nombre = 'En preparación' or E.nombre = 'Artículos pendientes')
        group by P.id_producto) as FALTANTE")
            , 'FALTANTE.id_producto', 'productos.id_producto')
        ->join('matriz_receta', 'matriz_receta.producto_id', 'productos.id_producto');
        $select = ", 0 as cantidad_insumo";
        if(env('CALCULO_PRODUCCION_POR_INSUMO',false)){
            $productos = $productos->leftJoin(DB::raw("(SELECT producto_id, cantidad FROM matriz_receta 
            where insumo_id = ".env('ID_INSUMO_REFERENCIA',730).") as RECETA"), 
            'RECETA.producto_id', 'productos.id_producto');
            $select = ", ifnull(RECETA.cantidad,0) as cantidad_insumo";
        }
        $productos = $productos->select('productos.*')
        ->selectRaw("ifnull(FALTANTE.cantidad,0) as cantidad".$select)
        ->groupBy('productos.id_producto')
        ->get();
        // echo json_encode($productos);die;
        return view('produccion.create', ['productos' => $productos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->all();
        $fields['fecha_vencimiento'] = date("Y-m-d",strtotime($fields['fecha_produccion']."+ 2 month"));
        $fields['estado_id'] = 1;
        // echo json_encode($fields);die;
        $messages = [
            'required' => 'El :attribute es requerido',
            'unique'    => 'El :attribute ingresado ya esta en uso',
          ];
        $rules = array(
            'fecha_produccion'=>'required',
            'fecha_vencimiento'=>'required',
            'estado_id'=>'required',
            'producto_id'=>'required',
            'cantidad'=>'required',
            // 'items'=>'required'
        );

        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/produccion/crear')
                ->withErrors($validator);
            // return view('productos.crear');
        } else {
            $id_plan = DB::table('plan_produccion')->insertGetId(
                [
                'producto_id' => $fields['producto_id'], 
                'estado_id' => $fields['estado_id'], 
                'fecha_produccion' => $fields['fecha_produccion'], 
                'fecha_vencimiento' => $fields['fecha_vencimiento'], 
                'cantidad_planificada' => $fields['cantidad'], 
                'cantidad_final' => $fields['cantidad'], 
                'created_at' =>  date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                ]
            );
            // redirect
            Session::flash('message', 'El plan de producción se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/produccion/ver/'.$id_plan);
            // return view('produccion.ver', [$producto->id_producto]);
        }
    }

    public function update(Request $request, $id)
    {
        $fields = $request->all();
        $fields['fecha_vencimiento'] = date("Y-m-d",strtotime($fields['fecha_produccion']."+ 2 month"));
        // echo json_encode($fields);die;
        $messages = [
            'required' => 'El :attribute es requerido',
            'unique'    => 'El :attribute ingresado ya esta en uso',
          ];
        $rules = array(
            'fecha_produccion'=>'required',
            'fecha_vencimiento'=>'required',
            'nro_lote'=>'required',
            'cantidad'=>'required',
            'items'=>'required'
        );

        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/produccion/crear')
                ->withErrors($validator);
            
        } else {
            DB::table('plan_produccion')
                ->where('id_plan_produccion', $id)
                ->update(['fecha_produccion' => $fields['fecha_produccion'],
                'fecha_vencimiento' => $fields['fecha_vencimiento'],
                'nro_lote' => $fields['nro_lote'],
                'cantidad_planificada' => $fields['cantidad'],
                'cantidad_final' => $fields['cantidad']]);
            foreach($fields['items'] as $item){
                DB::table('produccion_producto')
                    ->where('id_produccion_producto', $item['id_produccion_producto'])
                    ->update(['cantidad_planificada' => $item['cantidad'],
                                'cantidad_real' => $item['cantidad']]);
            }
            Session::flash('message', 'El plan de producción se editó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/produccion/ver/'.$id);
            // return view('produccion.ver', [$producto->id_producto]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produccion = DB::table('plan_produccion')->where('plan_produccion.id_plan_produccion', $id)
        ->join('productos', 'productos.id_producto', '=', 'plan_produccion.producto_id')
        ->join('estado_produccion', 'estado_produccion.id_estado_produccion', '=', 'plan_produccion.estado_id')
        ->select('plan_produccion.*', 'productos.nombre','productos.codigo', 'estado_produccion.nombre as estado')
        ->get()->first();
        // echo json_encode($produccion);die;
        $receta = DB::table('productos')->where('matriz_receta.producto_id', $produccion->producto_id)
        ->join('matriz_receta', 'matriz_receta.insumo_id', '=', 'productos.id_producto')
        ->leftJoin('almacen_producto',
        function($join){
            $join->on('almacen_producto.producto_id_producto', '=', 'productos.id_producto')
            ->on('almacen_producto.almacen_id_almacen', '=', DB::raw(env('ALMACEN_PRINCIPAL', 1)))
            ;
        })
        ->leftJoin(DB::raw("(SELECT AP.producto_id_producto, sum(AP.cantidad) as cantidad
        FROM productos P left join almacen_producto AP ON AP.producto_id_producto = P.id_producto 
        where AP.producto_id_producto = P.id_producto
        group by P.id_producto) as STOCK_ALMACENES"), 'productos.id_producto', '=', 'STOCK_ALMACENES.producto_id_producto')
        ->leftJoin(DB::raw("(SELECT M.almacen_id_destino, M.cantidad FROM almacen_producto AP
        left join movimiento_stock M on AP.id_stock = M.almacen_id_destino
        join productos P on P.id_producto = AP.producto_id_producto
        where M.tipo_movimiento <> 'Ajuste' and cast(M.created_at as date) = cast(NOW() as date) 
        and (P.tipo <> 'Insumo' or M.tipo_movimiento <> 'Producción') 
        order by M.created_at desc) as MOV_DEST"), 'almacen_producto.id_stock', '=', 'MOV_DEST.almacen_id_destino')
        ->leftJoin(DB::raw("(SELECT M.almacen_id_origen, if(M.tipo_movimiento = 'Ajuste', M.cantidad,-1*M.cantidad) AS 'cantidad' FROM movimiento_stock M
        join almacen_producto AP on AP.id_stock = M.almacen_id_origen
        join productos P on P.id_producto = AP.producto_id_producto
        where cast(M.created_at as date) = cast(NOW() as date) 
        and (P.tipo <> 'Producto' or M.tipo_movimiento <> 'Producción') 
        order by M.created_at desc) as MOV_ORI"), 'almacen_producto.id_stock', 'MOV_ORI.almacen_id_origen')
        ->leftJoin(DB::raw("(SELECT P.tipo, P.motivo_devolucion, PP.producto_id_producto, PR.almacen_id_almacen, sum(IF(P.tipo = 'Pedido' or P.tipo = 'Devolucion compra', -1*PR.cantidad,PR.cantidad)) as 'cantidad' FROM producto_remito PR
        join pedido_producto PP on PR.producto_id_producto = PP.id_pedido_producto
        join pedidos P on PP.pedido_id_pedido = P.id_pedido
        where cast(PR.created_at as date) = cast(NOW() as date) and ((P.tipo <> 'Devolucion') or (P.tipo = 'Devolucion' and P.motivo_devolucion <> 'defecto'))
        group by P.tipo, PR.almacen_id_almacen, PP.producto_id_producto
        order by PR.created_at desc) as ITM_REM"), 
            function($join){
                $join->on('almacen_producto.almacen_id_almacen', '=', 'ITM_REM.almacen_id_almacen')
                ->on('almacen_producto.producto_id_producto', '=', 'ITM_REM.producto_id_producto');
            })
        ->select('productos.*','matriz_receta.cantidad as cantidad_receta')
        ->selectRaw("ifnull(STOCK_ALMACENES.cantidad,0.00) as stock, sum(ifnull(MOV_DEST.cantidad,0.00)) as mov_destino, sum(ifnull(MOV_ORI.cantidad,0.00)) as mov_origen, 
        sum(if(ITM_REM.tipo = 'Pedido', ITM_REM.cantidad,0.00)) as venta, sum(if(ITM_REM.tipo = 'Devolucion', ITM_REM.cantidad,0.00)) as devolucion_venta, 
        sum(if(ITM_REM.tipo = 'Compra', ITM_REM.cantidad,0.00)) as compra, sum(if(ITM_REM.tipo = 'Devolucion compra', ITM_REM.cantidad,0.00)) as devolucion_compra")
        ->groupBy('productos.id_producto')
        ->orderBy('productos.nombre')
        ->get();
        // echo json_encode($receta);die;
        if($produccion->estado_id > 1){
            $array_insumos = array();
            foreach($receta as $insumo){
                array_push($array_insumos, $insumo->id_producto);
            }
            $produccion_productos = DB::table('productos')->where('produccion_producto.plan_produccion_id', $id)->whereRaw("productos.tipo = 'Insumo'")
            ->join('produccion_producto', 'produccion_producto.producto_id', '=', 'productos.id_producto')
            ->leftJoin('almacen_producto',
            function($join){
                $join->on('almacen_producto.producto_id_producto', '=', 'productos.id_producto')
                ->on('almacen_producto.almacen_id_almacen', '=', DB::raw(env('ALMACEN_PRINCIPAL', 1)))
                ;
            })
            ->leftJoin(DB::raw("(SELECT AP.producto_id_producto, sum(AP.cantidad) as cantidad
            FROM productos P left join almacen_producto AP ON AP.producto_id_producto = P.id_producto 
            where AP.producto_id_producto = P.id_producto
            group by P.id_producto) as STOCK_ALMACENES"), 'productos.id_producto', '=', 'STOCK_ALMACENES.producto_id_producto')
            ->leftJoin(DB::raw("(SELECT M.almacen_id_destino, M.cantidad FROM almacen_producto AP
            left join movimiento_stock M on AP.id_stock = M.almacen_id_destino
            join productos P on P.id_producto = AP.producto_id_producto
            where M.tipo_movimiento <> 'Ajuste' and cast(M.created_at as date) = cast(NOW() as date) 
            and (P.tipo <> 'Insumo' or M.tipo_movimiento <> 'Producción') 
            order by M.created_at desc) as MOV_DEST"), 'almacen_producto.id_stock', '=', 'MOV_DEST.almacen_id_destino')
            ->leftJoin(DB::raw("(SELECT M.almacen_id_origen, if(M.tipo_movimiento = 'Ajuste', M.cantidad,-1*M.cantidad) AS 'cantidad' FROM movimiento_stock M
            join almacen_producto AP on AP.id_stock = M.almacen_id_origen
            join productos P on P.id_producto = AP.producto_id_producto
            where cast(M.created_at as date) = cast(NOW() as date) 
            and (P.tipo <> 'Producto' or M.tipo_movimiento <> 'Producción') 
            order by M.created_at desc) as MOV_ORI"), 'almacen_producto.id_stock', 'MOV_ORI.almacen_id_origen')
            ->leftJoin(DB::raw("(SELECT P.tipo, P.motivo_devolucion, PP.producto_id_producto, PR.almacen_id_almacen, sum(IF(P.tipo = 'Pedido' or P.tipo = 'Devolucion compra', -1*PR.cantidad,PR.cantidad)) as 'cantidad' FROM producto_remito PR
            join pedido_producto PP on PR.producto_id_producto = PP.id_pedido_producto
            join pedidos P on PP.pedido_id_pedido = P.id_pedido
            where cast(PR.created_at as date) = cast(NOW() as date) and ((P.tipo <> 'Devolucion') or (P.tipo = 'Devolucion' and P.motivo_devolucion <> 'defecto'))
            group by P.tipo, PR.almacen_id_almacen, PP.producto_id_producto
            order by PR.created_at desc) as ITM_REM"), 
                function($join){
                    $join->on('almacen_producto.almacen_id_almacen', '=', 'ITM_REM.almacen_id_almacen')
                    ->on('almacen_producto.producto_id_producto', '=', 'ITM_REM.producto_id_producto');
                })
            ->select('produccion_producto.*','productos.id_producto','productos.nombre','productos.unidad','productos.conversion')
            ->selectRaw("ifnull(STOCK_ALMACENES.cantidad,0.00) as stock, sum(ifnull(MOV_DEST.cantidad,0.00)) as mov_destino, sum(ifnull(MOV_ORI.cantidad,0.00)) as mov_origen, 
            sum(if(ITM_REM.tipo = 'Pedido', ITM_REM.cantidad,0.00)) as venta, sum(if(ITM_REM.tipo = 'Devolucion', ITM_REM.cantidad,0.00)) as devolucion_venta, 
            sum(if(ITM_REM.tipo = 'Compra', ITM_REM.cantidad,0.00)) as compra, sum(if(ITM_REM.tipo = 'Devolucion compra', ITM_REM.cantidad,0.00)) as devolucion_compra")
            ->groupBy('productos.id_producto')
            ->orderBy('productos.nombre')
            ->get();
        }else{
            $produccion_productos = array();
        }
        // echo json_encode($produccion_productos);die;
        return view('produccion.show', ['produccion' => $produccion,'receta' => $receta, 'produccion_productos' => $produccion_productos]);
    }

    public function confirmado(Request $request, $id)
    {
        $fields = $request->all();
        // echo json_encode($fields);die;
        $messages = [
            'required' => 'El :attribute es requerido',
            'unique'    => 'El :attribute ingresado ya esta en uso',
          ];
        $rules = array(
            'producto_id'=>'required',
            'nro_lote'=>'required',
            'fecha_produccion'=>'required',
            'items'=>'required',
            'cantidad'=>'required'
        );

        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/produccion/ver/'.$id)
                ->withErrors($validator);
            // return view('productos.crear');
        } else {
            DB::table('plan_produccion')
                ->where('id_plan_produccion', $id)
                ->update(['fecha_produccion' => $fields['fecha_produccion'],
                'nro_lote' => $fields['nro_lote'],
                'cantidad_planificada' => $fields['cantidad'],
                'cantidad_final' => $fields['cantidad'],
                'estado_id' => 2]);
            foreach($fields['items'] as $item){
                DB::table('produccion_producto')->insert(
                    [
                    'producto_id' => $item['id'], 
                    'plan_produccion_id' => $id,
                    'cantidad_planificada' => $item['cantidad'],
                    'cantidad_real' => $item['cantidad'], 
                    'created_at' =>  date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    ]
                );
            }
            // redirect
            Session::flash('message', 'El plan de producción se confirmó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/produccion/ver/'.$id);
            // return view('produccion.ver', [$producto->id_producto]);
        }
    }

    public function producido($id)
    {
        $insumos = DB::table('produccion_producto')->where('produccion_producto.plan_produccion_id', $id)
        ->where('almacen_producto.almacen_id_almacen', env('ALMACEN_PRINCIPAL',1))
        ->leftJoin('productos', 'productos.id_producto', '=', 'produccion_producto.producto_id')
        ->leftJoin('almacen_producto', 'almacen_producto.producto_id_producto', '=', 'productos.id_producto')
        ->select('produccion_producto.*','almacen_producto.id_stock')
        ->get();
        // echo json_encode($insumos);die;
        foreach($insumos as $insumo){
            DB::table('movimiento_stock')->insert([
                [
                    'tipo_movimiento' => 'Producción',
                    'almacen_id_origen' => $insumo->id_stock,
                    'almacen_id_destino' => $insumo->id_stock,
                    'cantidad' => $insumo->cantidad_planificada,
                    'user_id_user' => Auth::user()->id_user,
                    'created_at' =>  date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]
            ]);
        }
        $plan_produccion = DB::table('plan_produccion')
        ->where('id_plan_produccion', $id)
        ->update(['estado_id' => 3]);

        Session::flash('message', 'El plan pasó a producción con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/produccion/ver/'.$id);
    }

    public function finalizado(Request $request, $id)
    {
        $fields = $request->all();
        // echo json_encode($fields);die;
        $messages = [
            'required' => 'El :attribute es requerido',
            'unique'    => 'El :attribute ingresado ya esta en uso',
          ];
        $rules = array(
            'responsable'=>'required',
            'producto_id'=>'required',
            'fecha_vencimiento'=>'required',
            'cantidad_final'=>'required'
        );

        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/produccion/ver/'.$id)
                ->withErrors($validator);
            // return view('productos.crear');
        } else {
            $almacen_producto = DB::table('almacen_producto')->where('almacen_id_almacen', env('ALMACEN_PRINCIPAL', 1))
            ->where('producto_id_producto', $fields['producto_id'])->get()->first();
            DB::table('movimiento_stock')->insert([
                [
                    'tipo_movimiento' => 'Producción',
                    'almacen_id_origen' => $almacen_producto->id_stock,
                    'almacen_id_destino' => $almacen_producto->id_stock,
                    'cantidad' => $fields['cantidad_final'],
                    'user_id_user' => Auth::user()->id_user,
                    'created_at' =>  date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]
            ]);
            DB::table('plan_produccion')
                ->where('id_plan_produccion', $id)
                ->update(['fecha_vencimiento' => $fields['fecha_vencimiento'],
                        'responsable' => $fields['responsable'],
                        'cantidad_final' => $fields['cantidad_final'],
                        'descripcion' => $fields['descripcion'],
                        'estado_id' => 4]);
            // redirect
            Session::flash('message', 'El plan de producción se finalizó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/produccion/ver/'.$id);
            // return view('produccion.ver', [$producto->id_producto]);
        }
    }

    public function cancelado($id)
    {
        DB::table('plan_produccion')
            ->where('id_plan_produccion', $id)
            ->update(['estado_id' => 5]);
        // redirect
        Session::flash('message', 'El plan de producción se canceló con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/produccion/ver/'.$id);
    }
}
