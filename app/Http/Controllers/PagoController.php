<?php

namespace App\Http\Controllers;

use Auth;
use App\Pago;
use App\TipoMovimiento;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class PagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $fields = $request->all();
        $estadoSelected = 'PENDIENTE';
        $tipo_movimiento = TipoMovimiento::whereRaw("nombre = 'Venta'")->get()->first();
        $pagos = DB::table('pagos')->where('pagos.deleted', 0);
        if(isset($fields['estado_id'])){
            $estadoSelected = $fields['estado_id'];
        }
        if($estadoSelected != 'TODOS'){
            if($estadoSelected == 'PENDIENTE'){
                $pagos = $pagos->whereRaw("(pagos.estado = 'PENDIENTE' or pagos.estado = 'ASIGNADO PARCIAL')");
            }else{
                $pagos = $pagos->where('pagos.estado',$estadoSelected);
            }
        }
        $pagos = $pagos->where("tipo_movimiento_id", $tipo_movimiento ? $tipo_movimiento->id_tipo_movimiento : 1)
        ->leftJoin('users', 'pagos.user_id_user', '=', 'users.id_user')
        ->leftJoin('perfils', 'users.perfil_id', '=', 'perfils.id_perfil')
        ->leftJoin('pago_remito', 'pago_remito.pago_id_pago', '=', 'pagos.id_pago')
        ->select('pagos.*', 'pago_remito.remito_id_remito')
        ->selectRaw("GROUP_CONCAT(pago_remito.remito_id_remito separator ',') as remitos,GROUP_CONCAT(pago_remito.monto separator ',') as monto_remitos, IF(perfils.nombre = 'Mayorista', users.name, concat(users.name, ' ', users.surname)) as name, sum(ifnull(`pago_remito`.`monto`,0)) as pagado")
        ->groupBy('pagos.id_pago')
        ->paginate(10);
        // echo json_encode($pagos);die;
        return view('pagos.list', ['pagos' => $pagos, 'estadoSelected' => $estadoSelected]);
    }

    public function mispagos()
    {
        $tipo_movimiento = TipoMovimiento::whereRaw("nombre = 'Venta'")->get()->first();
        $pagos = DB::table('pagos')->where('pagos.deleted', 0)->where('pagos.user_id_user', '=', Auth::user()->id_user)
        ->whereRaw("tipo_movimiento_id", $tipo_movimiento ? $tipo_movimiento->id_tipo_movimiento : 1)
        ->select('pagos.*')
        ->orderBy('pagos.created_at', 'desc')
        ->paginate(10);
        return view('pagos.mispagos', ['pagos' => $pagos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $medios = DB::table('medios')->where('deleted', 0)->get();
        $clientes = DB::table('users')->where('users.deleted', 0)->where('roles.name', '<>', 'admin')->where('roles.name', '<>', 'administrador')->where('roles.name', '<>', 'Proveedor')
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->leftJoin('perfils', 'users.perfil_id', '=', 'perfils.id_perfil')
        ->select('users.id_user', 'roles.discount as descuento')
        ->selectRaw("IF(perfils.nombre = 'Mayorista', users.name, concat(users.name, ' ', users.surname)) as name")->get();
        return view('pagos.create', ['medios' => $medios, 'clientes' => $clientes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'user_id_user'=>'required',
            'medio_id_medio'=>'required',
            'monto'=>'required',
            'comprobante'=>'max:2048',
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/pagos/crear')
                ->withErrors($validator);
                // return view('medios.crear');
        } else {
            $tipo_movimiento = TipoMovimiento::whereRaw("nombre = 'Venta'")->get()->first();
            $pago = new Pago([
                'user_id_user' => $request->get('user_id_user'),
                'medio_id_medio' => $request->get('medio_id_medio'),
                'monto' => $request->get('monto'),
                'descripcion' => $request->get('descripcion'),
                'estado' => 'PENDIENTE',
                'tipo_movimiento_id' => $tipo_movimiento ? $tipo_movimiento->id_tipo_movimiento : 1, 
                'fecha' =>  date('Y-m-d')
            ]);
            if($request->hasFile('comprobante')) {
                $fileUpload = $request->file('comprobante');
                $ext = $fileUpload->guessExtension();
                $filename = md5(date('Y-m-d H:i:s:u')).".".$ext;
                $pago->comprobante = $filename;
                $fileUpload->move('images/comprobantes/',$filename);
            }
            $pago->save();
            // redirect
            Session::flash('message', 'El pago se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/pagos/index');
            // return view('marcas.index');
        }
    }

    public function nuevopago()
    {
        $medios = DB::table('medios')->where('deleted', 0)->where('publica', 1)->get();
        return view('pagos.nuevopago', ['medios' => $medios]);
    }

    public function addpago(Request $request)
    {
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'medio_id_medio'=>'required',
            'monto'=>'required',
            'comprobante'=>'max:2048',
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/pagos/crear')
                ->withErrors($validator);
                // return view('medios.crear');
        } else {
            $tipo_movimiento = TipoMovimiento::whereRaw("nombre = 'Venta'")->get()->first();
            $pago = new Pago([
                'user_id_user' => Auth::user()->id_user,
                'medio_id_medio' => $request->get('medio_id_medio'),
                'monto' => $request->get('monto'),
                'descripcion' => $request->get('descripcion'),
                'estado' => 'PENDIENTE',
                'tipo_movimiento_id' => $tipo_movimiento ? $tipo_movimiento->id_tipo_movimiento : 1, 
                'fecha' =>  date('Y-m-d')
            ]);
            if($request->hasFile('comprobante')) {
                $fileUpload = $request->file('comprobante');
                $ext = $fileUpload->guessExtension();
                $filename = md5(date('Y-m-d H:i:s:u')).".".$ext;
                $pago->comprobante = $filename;
                $fileUpload->move('images/comprobantes/',$filename);
            }
            $pago->save();
            // redirect
            Session::flash('message', 'El pago se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('mispagos');
            // return view('marcas.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pago  $pago
     * @return \Illuminate\Http\Response
     */
    public function show(Pago $pago)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pago  $pago
     * @return \Illuminate\Http\Response
     */
    public function asignar($id)
    {
        $pago = Pago::where('id_pago',$id)->where('pagos.deleted', 0)
        ->leftJoin('pago_remito', 'pago_remito.pago_id_pago', '=', 'pagos.id_pago')
        ->select('pagos.*')
        ->selectRaw('sum(ifnull(`pago_remito`.`monto`,0)) as pagado')
        ->groupBy('pagos.id_pago')
        ->get()->first();
        $remitos = DB::table('remitos')->where('users.id_user', $pago->user_id_user)->where('pedidos.tipo', '=', 'Pedido')
        ->whereRaw('(`estado_pedido`.`estado_id_estado` < 4 or `estado_pedido`.`estado_id_estado` is null)')
        ->leftJoin('pedidos', 'pedidos.id_pedido', '=', 'remitos.pedido_id_pedido')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('pago_remito', 'pago_remito.remito_id_remito', '=', 'remitos.id_remito')
        ->select('remitos.*', 'users.name', 'users.surname')
        ->selectRaw('`remitos`.`monto` - sum(ifnull(`pago_remito`.`monto`,0)) as falta_pagar')
        ->groupBy('remitos.id_remito')
        ->orderBy('remitos.created_at')
        ->havingRaw('(`remitos`.`monto` - sum(ifnull(`pago_remito`.`monto`,0))) > 0')->get();
        return view('pagos.asignar', ['pago' => $pago, 'remitos' => $remitos, 'id_cliente' => $pago->user_id_user]);
    }

    public function asignarsuccess(Request $request, $id_pago)
    {
        $fields = $request->all();
        $monto_pago = $fields['monto_pago'];
        $id_cliente = $fields['id_cliente'];
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        // echo json_encode($fields);die;
        foreach($fields['items'] as $item){
            if($monto_pago > 0){
                if(isset($item['checkbox'])){
                    if($monto_pago >= $item['monto']){
                        $monto_a_pagar = $item['monto'];
                        $monto_pago = $monto_pago - $item['monto'];
                    }else{
                        $monto_a_pagar = $monto_pago;
                        $monto_pago = 0;
                    }
                    DB::table('pago_remito')->insert(
                        [
                        'pago_id_pago' => $id_pago, 
                        'remito_id_remito' => $item['id_remito'], 
                        'monto' => $monto_a_pagar, 
                        'created_at' =>  date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        ]
                    );
                }
            }else {
                break;
            }
        }
        $pago = Pago::where('id_pago',$id_pago)->get()->first();
        if($monto_pago > 0){
            $pago->estado = 'ASIGNADO PARCIAL';
        }else{
            $pago->estado = 'ASIGNADO';
        }
        $pago->save();
        Session::flash('message', 'El pago se asignó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/clientes/cuentaampliado/'.$id_cliente);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pago  $pago
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pago = Pago::where('id_pago',$id)->get()->first();
        $medios = DB::table('medios')->where('deleted', 0)->get();
        $clientes = DB::table('users')->where('users.deleted', 0)->where('roles.name', '<>', 'admin')->where('roles.name', '<>', 'administrador')->where('roles.name', '<>', 'Proveedor')
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->leftJoin('perfils', 'users.perfil_id', '=', 'perfils.id_perfil')
        ->select('users.id_user', 'roles.discount as descuento')
        ->selectRaw("IF(perfils.nombre = 'Mayorista', users.name, concat(users.name, ' ', users.surname)) as name")->get();
        return view('pagos.edit', ['pago' => $pago, 'medios' => $medios, 'clientes' => $clientes]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pago  $pago
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'user_id_user'=>'required',
            'medio_id_medio'=>'required',
            'monto'=>'required',
            'comprobante'=>'max:2048',
        );
        $fields = $request->all();
        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/pagos/editar/'.$id)
                ->withErrors($validator);
                // return view('medios.crear');
        } else {
            $pago = Pago::where('id_pago',$id)->get()->first();
            $pago->user_id_user = $fields['user_id_user'];
            $pago->medio_id_medio = $fields['medio_id_medio'];
            $pago->monto = $fields['monto'];
            $pago->descripcion = $fields['descripcion'];
            if($request->hasFile('comprobante')) {
                $fileUpload = $request->file('comprobante');
                $ext = $fileUpload->guessExtension();
                $filename = md5(date('Y-m-d H:i:s:u')).".".$ext;
                if($pago->comprobante != ''){
                    \File::delete(public_path('images/comprobantes/' . $pago->comprobante));
                }
                $pago->comprobante = $filename;
                $fileUpload->move('images/comprobantes/',$filename);
            }
            $pago->save();
            // redirect
            Session::flash('message', 'El pago se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/pagos/index');
            // return view('marcas.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pago  $pago
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $pago = Pago::find($id);
        $pago->deleted = 1;
        $pago->save();

        // redirect
        Session::flash('message', 'El pago se eliminó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/pagos/index');
        // return view('pagos.index');
    }
}
