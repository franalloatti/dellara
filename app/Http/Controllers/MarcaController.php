<?php

namespace App\Http\Controllers;

use App\Marca;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class MarcaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $marcas = DB::table('marcas')->where('deleted', 0)->paginate(10);
        return view('marcas.list', ['marcas' => $marcas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('marcas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'nombre'=>'required'
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/marcas/crear')
                ->withErrors($validator);
                // return view('marcas.crear');
        } else {
            $marca = new Marca([
                'nombre' => $request->get('nombre'),
                'descripcion' => $request->get('descripcion')
            ]);
            $marca->save();
            // redirect
            Session::flash('message', 'La marca se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/marcas/index');
            // return view('marcas.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Marca  $marca
     * @return \Illuminate\Http\Response
     */
    public function show(Marca $marca)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Marca  $marca
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $marca = Marca::where('id_marca',$id)->get()->first();
        return view('marcas.edit', ['marca' => $marca]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Marca  $marca
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $marca = Marca::where('id_marca',$id)->get()->first();
        $messages = [
            'required' => 'El :attribute es requerido'
          ];
        $rules = array(
            'nombre'=>'required'
        );
        $fields = $request->all();
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/marcas/editar/'.$id)
                ->withErrors($validator);
                // return view('marcas.editar', [$id]);
        } else {
            $marca->update($fields);
            // redirect
            Session::flash('message', 'La marca se actualizó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/marcas/index');
            // return view('marcas.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Marca  $marca
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $marca = Marca::find($id);
        $marca->deleted = 1;
        $marca->save();

        // redirect
        Session::flash('message', 'La marca se eliminó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/marcas/index');
        // return view('marcas.index');
    }
}
