<?php

namespace App\Http\Controllers;

use App\Rubro;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class RubroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rubros = DB::table('rubros')->where('deleted', 0)->paginate(10);
        return view('rubros.list', ['rubros' => $rubros]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('rubros.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'nombre'=>'required'
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/rubros/crear')
                ->withErrors($validator);
                // return view('rubros.crear');
        } else {
            $rubro = new Rubro([
                'nombre' => $request->get('nombre'),
                'descripcion' => $request->get('descripcion')
            ]);
            $rubro->save();
            // redirect
            Session::flash('message', 'El rubro se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/rubros/index');
            // return view('rubros.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rubro  $rubro
     * @return \Illuminate\Http\Response
     */
    public function show(Rubro $rubro)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rubro  $rubro
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rubro = Rubro::where('id_rubro',$id)->get()->first();
        return view('rubros.edit', ['rubro' => $rubro]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rubro  $rubro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rubro = Rubro::where('id_rubro',$id)->get()->first();
        $messages = [
            'required' => 'El :attribute es requerido'
          ];
        $rules = array(
            'nombre'=>'required'
        );
        $fields = $request->all();
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/rubros/editar/'.$id)
                ->withErrors($validator);
                // return view('rubros.editar', [$id]);
        } else {
            $rubro->update($fields);
            // redirect
            Session::flash('message', 'El rubro se actualizó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/rubros/index');
            // return view('rubros.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rubro  $rubro
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $rubro = Rubro::find($id);
        $rubro->deleted = 1;
        $rubro->save();

        // redirect
        Session::flash('message', 'El rubro se eliminó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/rubros/index');
        // return view('rubros.index');
    }
}
