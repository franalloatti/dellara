<?php

namespace App\Http\Controllers;

use \stdClass;
use App\Producto;
use App\Pedido;
use App\Estado;
use App\Imagen;
use App\Direccion;
use App\User;
use Auth;
use PDF;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ProductosImport;
use App\Exports\ListaPreciosExport;
use Image;
use App\Notifications\MailNuevoPedidoNotificacion;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $fields = $request->all();
        $limit = isset($fields['limit']) ? $fields['limit'] : 20;
        $rubros = DB::table('rubros')->where('deleted', 0)->orderBy('nombre')->get();
        $marcas = DB::table('marcas')->where('deleted', 0)->orderBy('nombre')->get();
        $search = '';
        $marcaSelected = null;
        $rubroSelected = null;
        $productos = DB::table('productos')->where('productos.deleted', 0)->where('productos.tipo', 'Producto');
        if(isset($fields['search'])){
            $search = $fields['search'];
            $productos = $productos->whereRaw("(productos.codigo like '%" . $search . "%' or productos.nombre like '%" . $search . "%')");
        }
        if(isset($fields['marca_id']) && $fields['marca_id'] !== null){
            $marcaSelected = $fields['marca_id'];
            $productos = $productos->where('marcas.id_marca', '=', $marcaSelected);
        }
        if(isset($fields['rubro_id']) && $fields['rubro_id'] !== null){
            $rubroSelected = $fields['rubro_id'];
            $productos = $productos->where('rubros.id_rubro', '=', $rubroSelected);
        }
        $productos = $productos->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('marca_producto', 'marca_producto.producto_id_producto', '=', 'productos.id_producto')
        ->leftJoin('marcas', 'marcas.id_marca', '=', 'marca_producto.marca_id_marca')
        ->leftJoin('producto_rubro', 'producto_rubro.producto_id_producto', '=', 'productos.id_producto')
        ->leftJoin('rubros', 'rubros.id_rubro', '=', 'producto_rubro.rubro_id_rubro')
        ->leftJoin('users', 'users.id_user', '=', 'producto_user.user_id_user')
        ->leftJoin('imagens', function($join){
            $join->on('imagens.producto_id', '=', 'productos.id_producto')
            ->where('imagens.principal', '=', 1);
        })
        ->leftJoin('almacen_producto',
        function($join){
            $join->on('almacen_producto.producto_id_producto', '=', 'productos.id_producto')
            // ->on('almacen_producto.almacen_id_almacen', '=', DB::raw(env('ALMACEN_PRINCIPAL', 1)))
            ;
        })
        ->leftJoin(DB::raw("(SELECT AP.producto_id_producto, sum(AP.cantidad) as cantidad
        FROM productos P left join almacen_producto AP ON AP.producto_id_producto = P.id_producto 
        where AP.producto_id_producto = P.id_producto
        group by P.id_producto) as STOCK_ALMACENES"), 'productos.id_producto', '=', 'STOCK_ALMACENES.producto_id_producto')
        ->leftJoin(DB::raw("(SELECT M.almacen_id_destino, M.cantidad FROM almacen_producto AP
        left join movimiento_stock M on AP.id_stock = M.almacen_id_destino
        join productos P on P.id_producto = AP.producto_id_producto
        where M.tipo_movimiento <> 'Ajuste' and cast(M.created_at as date) = cast(NOW() as date) 
        and (P.tipo <> 'Insumo' or M.tipo_movimiento <> 'Producción') 
        order by M.created_at desc) as MOV_DEST"), 'almacen_producto.id_stock', '=', 'MOV_DEST.almacen_id_destino')
        ->leftJoin(DB::raw("(SELECT M.almacen_id_origen, if(M.tipo_movimiento = 'Ajuste', M.cantidad,-1*M.cantidad) AS 'cantidad' FROM movimiento_stock M
        join almacen_producto AP on AP.id_stock = M.almacen_id_origen
        join productos P on P.id_producto = AP.producto_id_producto
        where cast(M.created_at as date) = cast(NOW() as date) 
        and (P.tipo <> 'Producto' or M.tipo_movimiento <> 'Producción') 
        order by M.created_at desc) as MOV_ORI"), 'almacen_producto.id_stock', 'MOV_ORI.almacen_id_origen')
        ->leftJoin(DB::raw("(SELECT P.tipo, P.motivo_devolucion, PP.producto_id_producto, PR.almacen_id_almacen, sum(IF(P.tipo = 'Pedido' or P.tipo = 'Devolucion compra', -1*PR.cantidad,PR.cantidad)) as 'cantidad' FROM producto_remito PR
        join pedido_producto PP on PR.producto_id_producto = PP.id_pedido_producto
        join pedidos P on PP.pedido_id_pedido = P.id_pedido
        where cast(PR.created_at as date) = cast(NOW() as date) and ((P.tipo <> 'Devolucion') or (P.tipo = 'Devolucion' and P.motivo_devolucion <> 'defecto'))
        group by P.tipo, PR.almacen_id_almacen, PP.producto_id_producto
        order by PR.created_at desc) as ITM_REM"), 
            function($join){
                $join->on('almacen_producto.almacen_id_almacen', '=', 'ITM_REM.almacen_id_almacen')
                ->on('almacen_producto.producto_id_producto', '=', 'ITM_REM.producto_id_producto');
            })
        ->select('productos.*', 'imagens.nombre as nombre_imagen', 'producto_user.costo_dolares as costo', 'users.valor_dolar')
        ->selectRaw("ifnull(STOCK_ALMACENES.cantidad,0.00) as cantidad, sum(ifnull(MOV_DEST.cantidad,0.00)) as mov_destino, sum(ifnull(MOV_ORI.cantidad,0.00)) as mov_origen, 
        sum(if(ITM_REM.tipo = 'Pedido', ITM_REM.cantidad,0.00)) as venta, sum(if(ITM_REM.tipo = 'Devolucion', ITM_REM.cantidad,0.00)) as devolucion_venta, 
        sum(if(ITM_REM.tipo = 'Compra', ITM_REM.cantidad,0.00)) as compra, sum(if(ITM_REM.tipo = 'Devolucion compra', ITM_REM.cantidad,0.00)) as devolucion_compra")
        ->groupBy('productos.id_producto')
        ->orderBy('productos.codigo')->paginate($limit);
        // echo json_encode($productos);die;
        return view('productos.list', ['limit' =>  $limit, 'search' =>  $search, 'marcaSelected' =>  $marcaSelected,'rubroSelected' =>  $rubroSelected, 'marcas' =>  $marcas, 'rubros' =>  $rubros, 'productos' => $productos]);
    }

    public function lista_precios(Request $request)
    {
        $fields = $request->all();
        if($fields['formato'] == 'pdf'){
            $productos = DB::table('productos')->where('productos.deleted', 0)->where('estado', 1)->where('productos.tipo', 'Producto');
            // ->leftJoin('producto_user', 'producto_user.producto_id_producto', '=', 'productos.id_producto')
            $productos = $productos->leftJoin('producto_user', function($join){
                $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
                ->where('producto_user.seleccionado', '=', 1);
            })
            ->leftJoin('marca_producto', 'marca_producto.producto_id_producto', '=', 'productos.id_producto')
            ->leftJoin('marcas', 'marcas.id_marca', '=', 'marca_producto.marca_id_marca')
            ->leftJoin('producto_rubro', 'producto_rubro.producto_id_producto', '=', 'productos.id_producto')
            ->leftJoin('rubros', 'rubros.id_rubro', '=', 'producto_rubro.rubro_id_rubro')
            ->leftJoin('users', 'users.id_user', '=', 'producto_user.user_id_user')
            ->select('productos.*', 'marcas.nombre as marca', 'rubros.nombre as rubro', 'producto_user.costo_dolares as costo', 'users.valor_dolar')
            ->groupBy('productos.id_producto')->orderBy('marcas.nombre')->orderBy('productos.codigo')->get();
            $midescuento = 0;
            if(Auth::user()){
                $midescuento = DB::table('roles')->where('role_user.user_id_user', '=', Auth::user()->id_user)
                ->leftJoin('role_user', 'role_user.role_id_role', '=', 'roles.id_role')
                ->select('roles.discount')->get()->first()->discount;
            }
            // echo json_encode($productos);die;
            $pdf = PDF::loadView('productos.listapreciospdf', ['productos' => $productos, 'midescuento' => $midescuento]);
            return $pdf->download('lista-precios-'.date("d-m-Y").'.pdf');
        }else{
            return Excel::download(new ListaPreciosExport, 'lista-precios-'.date("d-m-Y").'.xls');
        }
    }

    public function detalleproducto(Request $request, $id_producto)
    {
        $producto = Producto::where('id_producto',$id_producto)->with('marcas')->with('rubros')
        ->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('users', 'users.id_user', '=', 'producto_user.user_id_user')->get()->first(); 
        $imagenes = DB::table('imagens')->where('producto_id', $id_producto)->orderBy('principal', 'desc')->get();
        $mirol = array();
        if(Auth::user()){
            $mirol = DB::table('roles')->where('role_user.user_id_user', '=', Auth::user()->id_user)
            ->leftJoin('role_user', 'role_user.role_id_role', '=', 'roles.id_role')->get()->first();
        }
        return response()->json(['producto'=> $producto, 'imagenes' => $imagenes, 'mirol' => $mirol], 200);
    }

    public function carrito()
    {
        $productos = Producto::where('item_carrito.user_id_user',Auth::user()->id_user)->where('productos.deleted', 0)->with('marcas')->with('rubros')
        ->leftJoin('item_carrito', 'item_carrito.producto_id_producto', '=', 'productos.id_producto')
        ->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('users', 'users.id_user', '=', 'producto_user.user_id_user')
        ->leftJoin('imagens', function($join){
            $join->on('imagens.producto_id', '=', 'productos.id_producto')
            ->where('imagens.principal', '=', 1);
        })
        // ->leftJoin(DB::raw("(SELECT `almacens`.`nombre`, `almacen_producto`.`producto_id_producto`, `almacen_producto`.`cantidad` FROM `almacen_producto`
        // join `almacens` on `almacens`.`id_almacen` = `almacen_producto`.`almacen_id_almacen` and `almacens`.`nombre` <> 'Mercado Libre') as TEMP "),function($join){
        //    $join->on('productos.id_producto', '=', 'TEMP.producto_id_producto');
        // })
        ->join('almacen_producto', 'almacen_producto.producto_id_producto', '=', 'productos.id_producto')
        ->join('almacens', function($join) {
            $join->on('almacens.id_almacen', 'almacen_producto.almacen_id_almacen')
                ->where('almacens.id_almacen', '=', DB::raw(env('ALMACEN_PRINCIPAL', 1)));
        })
        ->select('productos.*','imagens.nombre as nombre_imagen','producto_user.costo_dolares as costo', 'users.valor_dolar', 'item_carrito.cantidad', 'item_carrito.id_item_carrito')
        ->selectRaw('ifnull(sum(almacen_producto.cantidad),0) as stock')
        ->groupBy('productos.id_producto')->orderBy('productos.codigo')->get(); 
        $mirol = DB::table('roles')->where('role_user.user_id_user', '=', Auth::user()->id_user)
        ->leftJoin('role_user', 'role_user.role_id_role', '=', 'roles.id_role')->get()->first();
        $direcciones = DB::table('direccions')->where('direccions.user_id', '=', Auth::user()->id_user)->get();
        return view('productos.carrito', ['productos' => $productos, 'mirol' => $mirol, 'direcciones' => $direcciones]);
    }

    public function catalogo(Request $request)
    {
        $fields = $request->all();
        $limit = isset($fields['limit']) ? $fields['limit'] : 20;
        $con_descuento = isset($fields['con_descuento']) && $fields['con_descuento'] ? true : false;
        $rubros = DB::table('rubros')->where('deleted', 0)->orderBy('nombre')->get();
        $marcas = DB::table('marcas')->where('deleted', 0)->orderBy('nombre')->get();
        $search = '';
        $marcaSelected = null;
        $rubroSelected = null;
        $productos = DB::table('productos')->where('productos.deleted', 0)->where('estado', 1)->where('productos.tipo', 'Producto');
        if($con_descuento){
            $productos = $productos->where('productos.tiene_fijado', '=', 1);
        }
        if(isset($fields['search'])){
            $search = $fields['search'];
            $productos = $productos->whereRaw("(productos.codigo like '%" . $search . "%' or productos.nombre like '%" . $search . "%')");
            // $productos = $productos->where('productos.codigo', 'like', '%' . $search . '%')
            // ->orWhere('productos.nombre', 'like', '%' . $search . '%');
            // ->orWhere('marcas.nombre', 'like', '%' . $search . '%')
            // ->orWhere('rubros.nombre', 'like', '%' . $search . '%');
        }
        if(isset($fields['marca_id']) && $fields['marca_id'] !== null){
            $marcaSelected = $fields['marca_id'];
            $productos = $productos->where('marcas.id_marca', '=', $marcaSelected);
        }
        if(isset($fields['rubro_id']) && $fields['rubro_id'] !== null){
            $rubroSelected = $fields['rubro_id'];
            $productos = $productos->where('rubros.id_rubro', '=', $rubroSelected);
        }
        // ->leftJoin('producto_user', 'producto_user.producto_id_producto', '=', 'productos.id_producto')
        $productos = $productos->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('marca_producto', 'marca_producto.producto_id_producto', '=', 'productos.id_producto')
        ->leftJoin('marcas', 'marcas.id_marca', '=', 'marca_producto.marca_id_marca')
        ->leftJoin('producto_rubro', 'producto_rubro.producto_id_producto', '=', 'productos.id_producto')
        ->leftJoin('rubros', 'rubros.id_rubro', '=', 'producto_rubro.rubro_id_rubro')
        ->leftJoin('users', 'users.id_user', '=', 'producto_user.user_id_user')
        ->leftJoin('imagens', function($join){
            $join->on('imagens.producto_id', '=', 'productos.id_producto')
            ->where('imagens.principal', '=', 1);
        })
        // ->leftJoin(DB::raw("(SELECT `almacens`.`nombre`, `almacen_producto`.`producto_id_producto`, `almacen_producto`.`cantidad` FROM `almacen_producto`
        // join `almacens` on `almacens`.`id_almacen` = `almacen_producto`.`almacen_id_almacen` and `almacens`.`nombre` <> 'Mercado Libre') as TEMP "),function($join){
        //    $join->on('productos.id_producto', '=', 'TEMP.producto_id_producto');
        // })
        ->leftJoin('almacen_producto',
        function($join){
            $join->on('almacen_producto.producto_id_producto', '=', 'productos.id_producto')
            ->on('almacen_producto.almacen_id_almacen', '=', DB::raw(env('ALMACEN_PRINCIPAL', 1)));
        })
        ->leftJoin(DB::raw("(SELECT M.almacen_id_destino, M.cantidad FROM almacen_producto AP
        left join movimiento_stock M on AP.id_stock = M.almacen_id_destino
        join productos P on P.id_producto = AP.producto_id_producto
        where M.tipo_movimiento <> 'Ajuste' and cast(M.created_at as date) = cast(NOW() as date) 
        and (P.tipo <> 'Insumo' or M.tipo_movimiento <> 'Producción') 
        order by M.created_at desc) as MOV_DEST"), 'almacen_producto.id_stock', '=', 'MOV_DEST.almacen_id_destino')
        ->leftJoin(DB::raw("(SELECT M.almacen_id_origen, if(M.tipo_movimiento = 'Ajuste', M.cantidad,-1*M.cantidad) AS 'cantidad' FROM movimiento_stock M
        join almacen_producto AP on AP.id_stock = M.almacen_id_origen
        join productos P on P.id_producto = AP.producto_id_producto
        where cast(M.created_at as date) = cast(NOW() as date) 
        and (P.tipo <> 'Producto' or M.tipo_movimiento <> 'Producción')
        order by M.created_at desc) as MOV_ORI"), 'almacen_producto.id_stock', 'MOV_ORI.almacen_id_origen')
        ->leftJoin(DB::raw("(SELECT P.tipo, P.motivo_devolucion, PP.producto_id_producto, PR.almacen_id_almacen, sum(IF(P.tipo = 'Pedido' or P.tipo = 'Devolucion compra', -1*PR.cantidad,PR.cantidad)) as 'cantidad' FROM producto_remito PR
        join pedido_producto PP on PR.producto_id_producto = PP.id_pedido_producto
        join pedidos P on PP.pedido_id_pedido = P.id_pedido
        where cast(PR.created_at as date) = cast(NOW() as date) and ((P.tipo <> 'Devolucion') or (P.tipo = 'Devolucion' and P.motivo_devolucion <> 'defecto'))
        group by P.tipo, PR.almacen_id_almacen, PP.producto_id_producto
        order by PR.created_at desc) as ITM_REM"), 
            function($join){
                $join->on('almacen_producto.almacen_id_almacen', '=', 'ITM_REM.almacen_id_almacen')
                ->on('almacen_producto.producto_id_producto', '=', 'ITM_REM.producto_id_producto');
            }
        )
        ->select('productos.*','imagens.nombre as nombre_imagen','producto_user.costo_dolares as costo','users.valor_dolar','rubros.nombre as rubro','marcas.nombre as marca')
        // ->selectRaw('ifnull(sum(TEMP.cantidad),0) as stock')
        ->selectRaw("ifnull(almacen_producto.cantidad,0.00) as cantidad, sum(ifnull(MOV_DEST.cantidad,0.00)) as mov_destino, sum(ifnull(MOV_ORI.cantidad,0.00)) as mov_origen, 
        sum(if(ITM_REM.tipo = 'Pedido', ITM_REM.cantidad,0.00)) as venta, sum(if(ITM_REM.tipo = 'Devolucion', ITM_REM.cantidad,0.00)) as devolucion_venta, 
        sum(if(ITM_REM.tipo = 'Compra', ITM_REM.cantidad,0.00)) as compra, sum(if(ITM_REM.tipo = 'Devolucion compra', ITM_REM.cantidad,0.00)) as devolucion_compra")
        ->groupBy('productos.id_producto')->orderBy('productos.codigo')->paginate($limit);
        if(Auth::user() !== null){
            $mirol = DB::table('roles')->where('role_user.user_id_user', '=', Auth::user()->id_user)
            ->leftJoin('role_user', 'role_user.role_id_role', '=', 'roles.id_role')->get()->first();
        }else{
            $mirol = new stdClass;
            $mirol->discount = 0;
        }
        // echo json_encode($productos);die;
        return view('productos.catalogo', ['con_descuento' =>  $con_descuento, 'limit' =>  $limit, 'search' =>  $search, 'marcaSelected' =>  $marcaSelected,'rubroSelected' =>  $rubroSelected, 'marcas' =>  $marcas, 'rubros' =>  $rubros, 'productos' => $productos, 'mirol' => $mirol]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proveedores = DB::table('users')->where('users.deleted', 0)->where('roles.name', '=', 'Proveedor')
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->select('users.*')->get();
        $almacenes = DB::table('almacens')->where('deleted', 0)->get();
        $rubros = DB::table('rubros')->where('deleted', 0)->orderBy('nombre')->get();
        $marcas = DB::table('marcas')->where('deleted', 0)->orderBy('nombre')->get();
        return view('productos.create', ['proveedores' => $proveedores, 'almacenes' => $almacenes, 'rubros' => $rubros, 'marcas' => $marcas]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'El :attribute es requerido',
            'unique'    => 'El :attribute ingresado ya esta en uso',
          ];
        $rules = array(
            'codigo'=>'required|unique:productos',
            'nombre'=>'required',
            'marca_id'=>'required',
            'rubro_id'=>'required'
        );

        $validator = Validator::make($request->all(), $rules, $messages);


        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/productos/crear')
                ->withErrors($validator);
            // return view('productos.crear');
        } else {
            $producto = new Producto([
                'codigo' => $request->get('codigo'),
                'nombre' => $request->get('nombre'),
                'descripcion' => $request->get('descripcion'),
                'stock_verde' => $request->get('stock_verde'),
                'stock_amarillo' => $request->get('stock_amarillo'),
                'stock_seguridad' => $request->get('stock_seguridad'),
                'tiene_fijado' => $request->get('tiene_fijado'),
                'precio_fijado' => $request->get('precio_fijado'),
                'margen' => $request->get('margen'),
                'concepto' => $request->get('concepto'),
                'iva' => $request->get('iva')
            ]);
            if($request->get('tiene_fijado') !== null && $request->get('tiene_fijado') == "on"){
                $producto->tiene_fijado = 1;
            }else{
                $producto->tiene_fijado = 0;
            }
            $producto->save();
            DB::table('almacen_producto')->insert([
                [
                    'almacen_id_almacen' => env('ALMACEN_PRINCIPAL', 1),
                    'producto_id_producto' => $producto->id_producto,
                    'cantidad' => 0
                ]
            ]);
            $producto->marcas()->sync([$request->get('marca_id')]);
            $producto->rubros()->sync([$request->get('rubro_id')]);
            // redirect
            Session::flash('message', 'El producto se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/productos/ver/'.$producto->id_producto);
            // return view('productos.ver', [$producto->id_producto]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $producto = DB::table('productos')->where('productos.id_producto', '=', $id)
        ->leftJoin('marca_producto', 'marca_producto.producto_id_producto', '=', 'productos.id_producto')
        ->leftJoin('marcas', 'marcas.id_marca', '=', 'marca_producto.marca_id_marca')
        ->leftJoin('producto_rubro', 'producto_rubro.producto_id_producto', '=', 'productos.id_producto')
        ->leftJoin('rubros', 'rubros.id_rubro', '=', 'producto_rubro.rubro_id_rubro')
        ->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('imagens', function($join){
            $join->on('imagens.producto_id', '=', 'productos.id_producto')
            ->where('imagens.principal', '=', 1);
        })
        ->leftJoin('users', 'users.id_user', '=', 'producto_user.user_id_user')
        ->select('productos.*', 'users.valor_dolar', 'imagens.nombre as nombre_imagen', 'producto_user.costo_dolares', 'marcas.nombre as marca','rubros.nombre as rubro')->get()->first();

        $susalmacenes = DB::table('almacens')->where('almacens.deleted', 0)->where('almacen_producto.producto_id_producto', '=', $id)
        ->leftJoin('almacen_producto', 'almacen_producto.almacen_id_almacen', '=', 'almacens.id_almacen')
        ->leftJoin(DB::raw("(SELECT M.almacen_id_destino, M.cantidad FROM almacen_producto AP
        left join movimiento_stock M on AP.id_stock = M.almacen_id_destino
        join productos P on P.id_producto = AP.producto_id_producto
        where M.tipo_movimiento <> 'Ajuste' and cast(M.created_at as date) = cast(NOW() as date) 
        and (P.tipo <> 'Insumo' or M.tipo_movimiento <> 'Producción') and AP.producto_id_producto = ".$id."
        order by M.created_at desc) as MOV_DEST"), 'almacen_producto.id_stock', '=', 'MOV_DEST.almacen_id_destino')
        ->leftJoin(DB::raw("(SELECT M.almacen_id_origen, if(M.tipo_movimiento = 'Ajuste', M.cantidad,-1*M.cantidad) AS 'cantidad' FROM movimiento_stock M
        join almacen_producto AP on AP.id_stock = M.almacen_id_origen
        join productos P on P.id_producto = AP.producto_id_producto
        where cast(M.created_at as date) = cast(NOW() as date) and AP.producto_id_producto = ".$id." 
        and (P.tipo <> 'Producto' or M.tipo_movimiento <> 'Producción') 
        order by M.created_at desc) as MOV_ORI"), 'almacen_producto.id_stock', 'MOV_ORI.almacen_id_origen')
        ->leftJoin(DB::raw("(SELECT P.tipo, P.motivo_devolucion, PP.producto_id_producto, PR.almacen_id_almacen, sum(IF(P.tipo = 'Pedido' or P.tipo = 'Devolucion compra', -1*PR.cantidad,PR.cantidad)) as 'cantidad' FROM producto_remito PR
        join pedido_producto PP on PR.producto_id_producto = PP.id_pedido_producto
        join pedidos P on PP.pedido_id_pedido = P.id_pedido
        where cast(PR.created_at as date) = cast(NOW() as date) and PP.producto_id_producto = ".$id." and ((P.tipo <> 'Devolucion') or (P.tipo = 'Devolucion' and P.motivo_devolucion <> 'defecto'))
        group by P.tipo, PR.almacen_id_almacen, PP.producto_id_producto
        order by PR.created_at desc) as ITM_REM"), 
            function($join){
                $join->on('almacen_producto.almacen_id_almacen', '=', 'ITM_REM.almacen_id_almacen')
                ->on('almacen_producto.producto_id_producto', '=', 'ITM_REM.producto_id_producto');
            })
        ->select('almacens.*','almacen_producto.id_stock')
        ->selectRaw("ifnull(almacen_producto.cantidad,0.00) as stock, sum(ifnull(MOV_DEST.cantidad,0.00)) as mov_destino, sum(ifnull(MOV_ORI.cantidad,0.00)) as mov_origen, 
        sum(if(ITM_REM.tipo = 'Pedido', ITM_REM.cantidad,0.00)) as venta, sum(if(ITM_REM.tipo = 'Devolucion', ITM_REM.cantidad,0.00)) as devolucion_venta, 
        sum(if(ITM_REM.tipo = 'Compra', ITM_REM.cantidad,0.00)) as compra, sum(if(ITM_REM.tipo = 'Devolucion compra', ITM_REM.cantidad,0.00)) as devolucion_compra")
        ->groupBy('almacens.id_almacen')
        ->get();
        // echo json_encode($susalmacenes);die;
        // ACTUALIZAR STOCK CON MOVIMIENTOS DEL DIA

        $susproveedores = DB::table('users')->where('users.deleted', 0)->where('roles.name', '=', 'Proveedor')->where('producto_user.producto_id_producto', '=', $id)
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->leftJoin('producto_user', 'producto_user.user_id_user', '=', 'users.id_user')
        ->select('users.*','producto_user.id_costo','producto_user.costo_dolares as costo','producto_user.seleccionado','producto_user.margen', 'producto_user.codigo_proveedor')->get();

        $almacenes = DB::table('almacens as A')->where('A.deleted', 0)->get();
        $proveedores = DB::table('users')->where('users.deleted', 0)->where('roles.name', '=', 'Proveedor')
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->select('users.*')->groupBy('users.id_user')->get();
        return view('productos.show', ['producto' => $producto, 'susalmacenes' => $susalmacenes, 'susproveedores' => $susproveedores, 'almacenes' => $almacenes, 'proveedores' => $proveedores]);
    }
    
    public function receta($id)
    {
        $producto = DB::table('productos')->where('productos.id_producto', '=', $id)
        ->leftJoin('marca_producto', 'marca_producto.producto_id_producto', '=', 'productos.id_producto')
        ->leftJoin('marcas', 'marcas.id_marca', '=', 'marca_producto.marca_id_marca')
        ->leftJoin('producto_rubro', 'producto_rubro.producto_id_producto', '=', 'productos.id_producto')
        ->leftJoin('rubros', 'rubros.id_rubro', '=', 'producto_rubro.rubro_id_rubro')
        ->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('imagens', function($join){
            $join->on('imagens.producto_id', '=', 'productos.id_producto')
            ->where('imagens.principal', '=', 1);
        })
        ->leftJoin('users', 'users.id_user', '=', 'producto_user.user_id_user')
        ->select('productos.*', 'users.valor_dolar', 'imagens.nombre as nombre_imagen', 'producto_user.costo_dolares', 'marcas.nombre as marca','rubros.nombre as rubro')
        ->get()->first();
        
        $insumos = DB::table('productos')->where('productos.deleted', 0)->where('productos.tipo', 'Insumo')->get();

        $susinsumos = DB::table('productos')->where('matriz_receta.producto_id', $id)
        ->join('matriz_receta', 'matriz_receta.insumo_id', '=', 'productos.id_producto')
        ->select('productos.*','matriz_receta.cantidad')
        ->get();

        return view('productos.receta', ['producto' => $producto, 'insumos' => $insumos, 'susinsumos' => $susinsumos]);
    }

    public function configreceta(Request $request, $id)
    {
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $fields = $request->all();
        // echo json_encode($fields);die;
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'items'=>'required'
        );

        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/productos/receta/'.$id)
                ->withErrors($validator);
                // return view('marcas.crear');
        } else {
            DB::table('matriz_receta')->where('producto_id',$id)->delete();
            foreach($request->get('items') as $item){
                DB::table('matriz_receta')->insert(
                    [
                    'producto_id' => $id, 
                    'insumo_id' => $item['id'], 
                    'cantidad' => $item['cantidad'], 
                    'created_at' =>  date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    ]
                );
            }
            // redirect
            Session::flash('message', 'La receta se configuró con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/productos/ver/'.$id);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $producto = DB::table('productos')->where('productos.id_producto', '=', $id)
        ->leftJoin('marca_producto', 'marca_producto.producto_id_producto', '=', 'productos.id_producto')
        ->leftJoin('marcas', 'marcas.id_marca', '=', 'marca_producto.marca_id_marca')
        ->leftJoin('producto_rubro', 'producto_rubro.producto_id_producto', '=', 'productos.id_producto')
        ->leftJoin('rubros', 'rubros.id_rubro', '=', 'producto_rubro.rubro_id_rubro')
        ->select('productos.*','marcas.id_marca as marca','rubros.id_rubro as rubro')->get()->first();
        $rubros = DB::table('rubros')->where('deleted', 0)->orderBy('nombre')->get();
        $marcas = DB::table('marcas')->where('deleted', 0)->orderBy('nombre')->get();
        return view('productos.edit', ['producto' => $producto, 'rubros' => $rubros, 'marcas' => $marcas]);
    }

    public function cambiarcantidad(Request $request)
    {
        $fields = $request->all();
        if($fields['cantidad'] == -1){
            DB::table('item_carrito')->where('id_item_carrito', $fields['id_item_carrito'])
            ->decrement('cantidad', 1);
        }else{
            DB::table('item_carrito')->where('id_item_carrito', $fields['id_item_carrito'])
            ->increment('cantidad', 1);
        }
    }

    public function adddireccion(Request $request)
    {
        $messages = [
            'required' => 'El campo :attribute es requerido',
          ];
        $rules = array(
            'direccion'=>'required',
            'provincia'=>'required',
            'localidad'=>'required',
            'codigo_postal'=>'required',
            // 'telefono'=>'required',
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if(null !== $request->get('cliente')){
            $id_user = $request->get('cliente');
        }else{
            $id_user = Auth::user()->id_user;
        }

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()]);
        } else {
            $existeDireccionSeleccionado = DB::table('direccions')->where('user_id', $id_user)->where('es_direccion_fact', 1)->get()->first();
            if(empty($existeDireccionSeleccionado)){
                $es_facturacion = 1;
            }else{
                $es_facturacion = 0;
            }
            $direccion = new Direccion([
                'direccion' => $request->get('direccion'),
                'observaciones' => $request->get('observaciones'),
                'provincia' => $request->get('provincia'),
                'localidad' => $request->get('localidad'),
                'codigo_postal' => $request->get('codigo_postal'),
                'telefono' => $request->get('telefono'),
                'es_direccion_fact' => $es_facturacion,
                'user_id' => $id_user
            ]);
            $direccion->save();
            return response()->json(['success' => 'La dirección se creó con éxito!', 'id' => $direccion->id_direccion]);
        }
    }
    public function addcarrito(Request $request)
    {
        $messages = [
            'required' => 'El :attribute es requerido'
          ];
        $rules = array(
            'cantidad'=>'required',
            'idProducto'=>'required'
        );

        $fields = $request->all();
        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            return response()->json(['error'=> ['message' => $validator->errors()->first()]], 403);
            // return view('productos.crear');
        } else {
            date_default_timezone_set('America/Argentina/Buenos_Aires');
            $existe = DB::table('item_carrito')->where('user_id_user', Auth::user()->id_user)->where('producto_id_producto', $fields['idProducto'])->get()->first();
            if($existe){
                DB::table('item_carrito')->where('user_id_user', Auth::user()->id_user)->where('producto_id_producto', $fields['idProducto'])
                ->update(['cantidad' => $existe->cantidad + $fields['cantidad']]);
            }else{
                DB::table('item_carrito')->insert([
                    [
                        'cantidad' => $fields['cantidad'],
                        'producto_id_producto' => $fields['idProducto'],
                        'user_id_user' => Auth::user()->id_user,
                        'created_at' =>  date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]
                ]);
            }
            return response()->json(['success' => 'El producto se agregó al carrito con éxito!']);
        }
    }

    public function deletecarrito($id)
    {
        DB::table('item_carrito')->where('id_item_carrito', $id)->delete();
        Session::flash('message', 'El producto se eliminó del carrito con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('carrito');
    }

    public function comprar(Request $request)
    {
        $fields = $request->all();
        $messages = [
            'required' => 'La dirección es requerida',
          ];
        $rules = array(
            'id_direccion'=>'required'
        );

        $validator = Validator::make($fields, $rules, $messages);

        date_default_timezone_set('America/Argentina/Buenos_Aires');
        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('carrito')
                ->withErrors($validator);
                // return view('marcas.crear');
        } else {
            $pedido = new Pedido([
                'user_id_user' => Auth::user()->id_user,
                'direccion_id_direccion' => $request->get('id_direccion'),
                'descripcion' => $request->get('observaciones'),
                'usuario_crea_id' => Auth::user()->id_user,
            ]);
            $estado = Estado::where('id_estado',2)->get()->first();
            $pedido->save();
            $pedido->estados()->sync(['estado_id_estado' => $estado->id_estado]);
            $productos = DB::table('item_carrito')->where('productos.deleted', 0)->where('item_carrito.user_id_user', Auth::user()->id_user)
            ->leftJoin('productos', 'productos.id_producto', '=', 'item_carrito.producto_id_producto')
            ->leftJoin('producto_user', function($join){
                $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
                ->where('producto_user.seleccionado', '=', 1);
            })
            ->leftJoin('users', 'producto_user.user_id_user', '=', 'users.id_user')
            ->select('productos.*', 'item_carrito.cantidad', 'producto_user.costo_dolares', 'users.valor_dolar')->get();
            $mirol = DB::table('roles')->where('role_user.user_id_user', '=', Auth::user()->id_user)
            ->leftJoin('role_user', 'role_user.role_id_role', '=', 'roles.id_role')->get()->first();
            foreach($productos as $producto){
                if($producto->tiene_fijado){
                    $precio = $producto->precio_fijado;
                }else{
                    $precio = (($producto->costo_dolares * $producto->valor_dolar)*(1.00 + $producto->margen / 100.00));
                }
                DB::table('pedido_producto')->insert(
                    [
                    'pedido_id_pedido' => $pedido->id_pedido, 
                    'producto_id_producto' => $producto->id_producto, 
                    'cantidad' => $producto->cantidad, 
                    'descuento' => $mirol->discount, 
                    'costo_unitario' => ($producto->costo_dolares * $producto->valor_dolar),
                    'precio_unitario' => $precio,
                    'created_at' =>  date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    ]
                );
            }
            if(env('MAIL_NUEVO_PEDIDO', false)){
                $user = User::where('email', '=', env('MAIL_TO_ADDRESS', '---'))->get()->first();
                if($user){
                    $user->notify(new MailNuevoPedidoNotificacion(Auth::user(), $productos, $pedido->id_pedido));
                }
                if(env('MAIL_TO_ADDRESS_COPY', '') != ''){
                    $user = User::where('email', '=', env('MAIL_TO_ADDRESS_COPY', '---'))->get()->first();
                    if($user){
                        $user->notify(new MailNuevoPedidoNotificacion(Auth::user(), $productos, $pedido->id_pedido));
                    }
                }
            }
            DB::table('item_carrito')->where('item_carrito.user_id_user', Auth::user()->id_user)->delete();
            // redirect
            Session::flash('message', 'El pedido se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('mispedidos');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $producto = Producto::where('id_producto',$id)->get()->first();
        $messages = [
            'required' => 'El :attribute es requerido',
            'unique'    => 'El :attribute ingresado ya esta en uso',
          ];
        $rules = array(
            'nombre'=>'required',
            'marca_id'=>'required',
            'rubro_id'=>'required'
        );
        $fields = $request->all();
        if($fields['codigo'] != $producto->codigo) {
            $rules['codigo'] = 'required|unique:productos';
        }
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/productos/editar/'.$id)
                ->withErrors($validator);
            // return view('productos.editar', [$id]);
        } else {
            if($request->get('tiene_fijado') !== null && $fields['tiene_fijado'] == "on"){
                $fields['tiene_fijado'] = 1;
            }else{
                $fields['tiene_fijado'] = 0;
            }
            $producto->update($fields);
            $producto->marcas()->sync([$fields['marca_id']]);
            $producto->rubros()->sync([$fields['rubro_id']]);
            // redirect
            Session::flash('message', 'El producto se actualizó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/productos/ver/'.$id);
            // return view('productos.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $producto = Producto::find($id);
        $producto->deleted = 1;
        $producto->save();

        // redirect
        Session::flash('message', 'El producto se eliminó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/productos/index');
        // return view('productos.index');
    }

    public function habilitarDeshabilitar($id_producto, $estado)
    {
        if($estado){
            $string = "deshabilitó";
        }else{
            $string = "habilitó";
        }
        DB::table('productos')->where('id_producto', $id_producto)->update(['estado' => !$estado]);
        Session::flash('message', 'El producto se '.$string.' con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/productos/index');
        // return view('productos.ver', [$id_producto]);
    }

    public function seleccionarproveedor($id_producto, $id_proveedor, $redirect = true)
    {
        DB::table('producto_user')->where('producto_id_producto', $id_producto)->update(['seleccionado' => 0]);
        DB::table('producto_user')->where('producto_id_producto', $id_producto)->where('user_id_user', $id_proveedor)->update(['seleccionado' => 1]);
        $proveedor = DB::table('producto_user')->where('producto_id_producto', $id_producto)->where('user_id_user', $id_proveedor)->get()->first();
        // echo json_encode($proveedor->margen);die;
        if(env('MARGEN_PROVEEDOR', false)){
            DB::table('productos')->where('id_producto', $id_producto)->update(['margen' => $proveedor->margen]);
        }
        if($redirect){
            Session::flash('message', 'El proveedor se seleccionó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/productos/ver/'.$id_producto);
        }
        // return view('productos.ver', [$id_producto]);
    }

    public function agregarproveedor(Request $request, $id)
    {
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $fields = $request->all();
        $existeProovedor = DB::table('producto_user')->where('producto_id_producto', $id)->where('user_id_user', $fields['proveedor_id'])->get()->first();
        if(empty($existeProovedor)){
            $existeProovedorSeleccionado = DB::table('producto_user')->where('producto_id_producto', $id)->where('seleccionado', 1)->get()->first();
            if(empty($existeProovedorSeleccionado)){
                if(env('MARGEN_PROVEEDOR', false)){
                    DB::table('productos')->where('id_producto', $id)->update(['margen' => $fields['margen']]);
                }
                $seleccionado = 1;
            }else{
                $seleccionado = 0;
            }
            DB::table('producto_user')->insert([
                [
                    'user_id_user' => $fields['proveedor_id'],
                    'producto_id_producto' => $id,
                    'costo_dolares' => $fields['costo_dolares'],
                    'margen' => $fields['margen'],
                    'codigo_proveedor' => $fields['codigo_proveedor'],
                    'seleccionado' => $seleccionado,
                    'created_at' =>  date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]
            ]);
            Session::flash('message', 'El proveedor se agregó con éxito!');
            Session::flash('type', 'success');
        }else{
            Session::flash('message', 'El proveedor ya se encuentra asociado al producto.');
            Session::flash('type', 'danger');
        }
        return Redirect::to('admin/productos/ver/'.$id);
        // return view('productos.ver', [$id]);
    }

    public function eliminarproveedor(Request $request, $id_producto, $id)
    {
        DB::table('producto_user')->where('id_costo', $id)->delete();
        Session::flash('message', 'El proveedor se eliminó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/productos/ver/'.$id_producto);
        // return view('productos.ver', [$id]);
    }

    public function editarstock(Request $request, $id){
        $fields = $request->all();
        DB::table('almacen_producto')
        ->where('id_stock', $fields['id_stock_modal'])
        ->update(['cantidad' => $fields['stock_modal']]);
        Session::flash('message', 'El stock se actualizó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/productos/ver/'.$id);
    }
    
    public function editarcosto(Request $request, $id){
        $fields = $request->all();
        DB::table('producto_user')
        ->where('id_costo', $fields['id_costo_modal'])
        ->update(['costo_dolares' => $fields['costo_modal'],
        'codigo_proveedor' => $fields['codigo_proveedor_modal'],
        'margen' => $fields['margen_modal']]);
        $proveedor = DB::table('producto_user')->where('id_costo', $fields['id_costo_modal'])->get()->first();
        if(env('MARGEN_PROVEEDOR', false) && $proveedor->seleccionado == 1){
            DB::table('productos')->where('id_producto', $id)->update(['margen' => $fields['margen_modal']]);
        }
        Session::flash('message', 'El costo se actualizó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/productos/ver/'.$id);
    }

    public function agregaralmacen(Request $request, $id)
    {
        $fields = $request->all();
        $existeAlmacen = DB::table('almacen_producto')->where('producto_id_producto', $id)->where('almacen_id_almacen', $fields['almacen_id'])->get()->first();
        if(empty($existeAlmacen)){
            DB::table('almacen_producto')->insert([
                [
                    'almacen_id_almacen' => $fields['almacen_id'],
                    'producto_id_producto' => $id,
                    'cantidad' => $fields['stock']
                ]
            ]);
            Session::flash('message', 'El almacen se agregó con éxito!');
            Session::flash('type', 'success');
        }else{
            Session::flash('message', 'El almacen ya se encuentra asociado al producto.');
            Session::flash('type', 'danger');
        }
        return Redirect::to('admin/productos/ver/'.$id);
        // return view('productos.ver', [$id]);
    }
    
    public function getstockalmacen(Request $request){
        $fields = $request->all();
        $id_almacen = $fields['id_almacen'];
        $id_producto = $fields['id_producto'];
        $almacen_producto = DB::table('almacen_producto')->where('almacen_producto.almacen_id_almacen', $id_almacen)->where('almacen_producto.producto_id_producto', $id_producto)
        ->leftJoin(DB::raw("(SELECT M.almacen_id_destino, M.cantidad FROM almacen_producto AP
        left join movimiento_stock M on AP.id_stock = M.almacen_id_destino
        join productos P on P.id_producto = AP.producto_id_producto
        where M.tipo_movimiento <> 'Ajuste' and cast(M.created_at as date) = cast(NOW() as date) 
        and (P.tipo <> 'Insumo' or M.tipo_movimiento <> 'Producción') and AP.producto_id_producto = ".$id_producto."
        order by M.created_at desc) as MOV_DEST"), 'almacen_producto.id_stock', '=', 'MOV_DEST.almacen_id_destino')
        ->leftJoin(DB::raw("(SELECT M.almacen_id_origen, if(M.tipo_movimiento = 'Ajuste', M.cantidad,-1*M.cantidad) AS 'cantidad' FROM movimiento_stock M
        join almacen_producto AP on AP.id_stock = M.almacen_id_origen
        join productos P on P.id_producto = AP.producto_id_producto
        where cast(M.created_at as date) = cast(NOW() as date) and AP.producto_id_producto = ".$id_producto." 
        and (P.tipo <> 'Producto' or M.tipo_movimiento <> 'Producción') 
        order by M.created_at desc) as MOV_ORI"), 'almacen_producto.id_stock', 'MOV_ORI.almacen_id_origen')
        ->leftJoin(DB::raw("(SELECT P.tipo, P.motivo_devolucion, PP.producto_id_producto, PR.almacen_id_almacen, sum(IF(P.tipo = 'Pedido' or P.tipo = 'Devolucion compra', -1*PR.cantidad,PR.cantidad)) as 'cantidad' FROM producto_remito PR
        join pedido_producto PP on PR.producto_id_producto = PP.id_pedido_producto
        join pedidos P on PP.pedido_id_pedido = P.id_pedido
        where cast(PR.created_at as date) = cast(NOW() as date) and PP.producto_id_producto = ".$id_producto." and ((P.tipo <> 'Devolucion') or (P.tipo = 'Devolucion' and P.motivo_devolucion <> 'defecto'))
        group by P.tipo, PR.almacen_id_almacen, PP.producto_id_producto
        order by PR.created_at desc) as ITM_REM"), 
            function($join){
                $join->on('almacen_producto.almacen_id_almacen', '=', 'ITM_REM.almacen_id_almacen')
                ->on('almacen_producto.producto_id_producto', '=', 'ITM_REM.producto_id_producto');
            })
        ->selectRaw("(ifnull(almacen_producto.cantidad,0.00) + ifnull(MOV_DEST.cantidad,0.00) + ifnull(MOV_ORI.cantidad,0.00) + 
        if(ITM_REM.tipo = 'Pedido', ITM_REM.cantidad,0.00) + if(ITM_REM.tipo = 'Devolucion', ITM_REM.cantidad,0.00) + 
        if(ITM_REM.tipo = 'Compra', ITM_REM.cantidad,0.00) + if(ITM_REM.tipo = 'Devolucion compra', ITM_REM.cantidad,0.00)) as cantidad")
        ->get()->first();
        return $almacen_producto ? number_format($almacen_producto->cantidad,2,",",".") : "0.00";
    }
    
    public function verstock(Request $request){
        $fields = $request->all();
        $rubros = DB::table('rubros')->where('deleted', 0)->orderBy('nombre')->get();
        $marcas = DB::table('marcas')->where('deleted', 0)->orderBy('nombre')->get();
        $search = '';
        $marcaSelected = null;
        $rubroSelected = null;
        $productos = DB::table('productos as P')->where('P.deleted', 0)->where('P.estado', 1)->whereRaw("P.tipo <> 'Servicio'");
        if(isset($fields['search'])){
            $search = $fields['search'];
            $productos = $productos->whereRaw("(P.codigo like '%" . $search . "%' or P.nombre like '%" . $search . "%')");
            // $productos = $productos->where('P.codigo', 'like', '%' . $search . '%')
            // ->orWhere('P.nombre', 'like', '%' . $search . '%');
        }
        if(isset($fields['marca_id']) && $fields['marca_id'] !== null){
            $marcaSelected = $fields['marca_id'];
            $productos = $productos->where('marcas.id_marca', '=', $marcaSelected);
        }
        if(isset($fields['rubro_id']) && $fields['rubro_id'] !== null){
            $rubroSelected = $fields['rubro_id'];
            $productos = $productos->where('rubros.id_rubro', '=', $rubroSelected);
        }
        $productos = $productos->leftJoin('almacen_producto as AP', 'P.id_producto', '=', 'AP.producto_id_producto')
        ->leftJoin(DB::raw("(SELECT M.almacen_id_destino, M.cantidad FROM almacen_producto APP
        left join movimiento_stock M on APP.id_stock = M.almacen_id_destino
        join productos P on P.id_producto = APP.producto_id_producto
        where M.tipo_movimiento <> 'Ajuste' and cast(M.created_at as date) = cast(NOW() as date) 
        and (P.tipo <> 'Insumo' or M.tipo_movimiento <> 'Producción') 
        order by M.created_at desc) as MOV_DEST"), 'AP.id_stock', '=', 'MOV_DEST.almacen_id_destino')
        ->leftJoin(DB::raw("(SELECT M.almacen_id_origen, if(M.tipo_movimiento = 'Ajuste', M.cantidad,-1*M.cantidad) AS 'cantidad' FROM movimiento_stock M
        join almacen_producto APP on APP.id_stock = M.almacen_id_origen
        join productos P on P.id_producto = APP.producto_id_producto
        where cast(M.created_at as date) = cast(NOW() as date) 
        and (P.tipo <> 'Producto' or M.tipo_movimiento <> 'Producción') 
        order by M.created_at desc) as MOV_ORI"), 'AP.id_stock', 'MOV_ORI.almacen_id_origen')
        ->leftJoin(DB::raw("(SELECT PED.tipo, PED.motivo_devolucion, PP.producto_id_producto, PR.almacen_id_almacen, sum(IF(PED.tipo = 'Pedido' or PED.tipo = 'Devolucion compra', -1*PR.cantidad,PR.cantidad)) as 'cantidad' FROM producto_remito PR
        join pedido_producto PP on PR.producto_id_producto = PP.id_pedido_producto
        join pedidos PED on PP.pedido_id_pedido = PED.id_pedido
        where cast(PR.created_at as date) = cast(NOW() as date) and ((PED.tipo <> 'Devolucion') or (PED.tipo = 'Devolucion' and PED.motivo_devolucion <> 'defecto'))
        group by PED.tipo, PR.almacen_id_almacen, PP.producto_id_producto
        order by PR.created_at desc) as ITM_REM"), 
            function($join){
                $join->on('AP.almacen_id_almacen', '=', 'ITM_REM.almacen_id_almacen')
                ->on('AP.producto_id_producto', '=', 'ITM_REM.producto_id_producto');
            })
        ->leftJoin('marca_producto', 'marca_producto.producto_id_producto', '=', 'P.id_producto')
        ->leftJoin('marcas', 'marcas.id_marca', '=', 'marca_producto.marca_id_marca')
        ->leftJoin('producto_rubro', 'producto_rubro.producto_id_producto', '=', 'P.id_producto')
        ->leftJoin('rubros', 'rubros.id_rubro', '=', 'producto_rubro.rubro_id_rubro')
        ->leftJoin('almacens as A', 'A.id_almacen', '=', 'AP.almacen_id_almacen')
        ->select('P.id_producto', 'P.codigo', 'P.tipo', 'P.conversion', 'P.nombre', 'A.id_almacen', 'A.nombre as almacen','rubros.nombre as rubro','marcas.nombre as marca')
        ->selectRaw("if(P.tipo = 'Insumo', concat(' ', P.unidad),'') as unidad, ifnull(AP.cantidad,0.00) as cantidad, sum(ifnull(MOV_DEST.cantidad,0.00)) as mov_destino, sum(ifnull(MOV_ORI.cantidad,0.00)) as mov_origen, 
        sum(if(ITM_REM.tipo = 'Pedido', ITM_REM.cantidad,0.00)) as venta, sum(if(ITM_REM.tipo = 'Devolucion', ITM_REM.cantidad,0.00)) as devolucion_venta, 
        sum(if(ITM_REM.tipo = 'Compra', ITM_REM.cantidad,0.00)) as compra, sum(if(ITM_REM.tipo = 'Devolucion compra', ITM_REM.cantidad,0.00)) as devolucion_compra")
        ->groupBy('AP.producto_id_producto', 'AP.almacen_id_almacen')
        ->get();
        // echo json_encode($productos);die;
        $almacenes = DB::table('almacens')->where('deleted', 0)->get();
        return view('productos.verstock', ['search' =>  $search, 'marcaSelected' =>  $marcaSelected,'rubroSelected' =>  $rubroSelected, 'marcas' =>  $marcas, 'rubros' =>  $rubros, 'stocks' => $productos, 'almacenes' => $almacenes]);
    }

    public function stock(Request $request, $id){
        $fields = $request->all();
        $fecha_hoy = date("Y-m-d");
        $fecha_desde = date("Y-m-d",strtotime($fecha_hoy."- 1 month")); 
        $almacenes = DB::table('almacens')->where('deleted', 0)->get();
        $selectedAlmacen = env('ALMACEN_PRINCIPAL', 1);
        if(isset($fields['almacen_id'])){
            $selectedAlmacen = $fields['almacen_id'];
        }
        if(isset($fields['fecha_desde'])){
            $fecha_desde = $fields['fecha_desde'];
        }
        $stock = DB::table('almacen_producto')->where('almacen_producto.almacen_id_almacen', $selectedAlmacen)->where('almacen_producto.producto_id_producto', $id)
        ->leftJoin('productos as P', 'P.id_producto', '=', 'almacen_producto.producto_id_producto')
        ->leftJoin(DB::raw("(SELECT M.almacen_id_destino, M.cantidad FROM almacen_producto AP
        left join movimiento_stock M on AP.id_stock = M.almacen_id_destino
        join productos P on P.id_producto = AP.producto_id_producto
        where M.tipo_movimiento <> 'Ajuste' and cast(M.created_at as date) = cast(NOW() as date) 
        and (P.tipo <> 'Insumo' or M.tipo_movimiento <> 'Producción') and AP.producto_id_producto = ".$id."
        order by M.created_at desc) as MOV_DEST"), 'almacen_producto.id_stock', '=', 'MOV_DEST.almacen_id_destino')
        ->leftJoin(DB::raw("(SELECT M.almacen_id_origen, if(M.tipo_movimiento = 'Ajuste', M.cantidad,-1*M.cantidad) AS 'cantidad' FROM movimiento_stock M
        join almacen_producto AP on AP.id_stock = M.almacen_id_origen
        join productos P on P.id_producto = AP.producto_id_producto
        where cast(M.created_at as date) = cast(NOW() as date) and AP.producto_id_producto = ".$id." 
        and (P.tipo <> 'Producto' or M.tipo_movimiento <> 'Producción') 
        order by M.created_at desc) as MOV_ORI"), 'almacen_producto.id_stock', 'MOV_ORI.almacen_id_origen')
        ->leftJoin(DB::raw("(SELECT P.tipo, P.motivo_devolucion, PP.producto_id_producto, PR.almacen_id_almacen, sum(IF(P.tipo = 'Pedido' or P.tipo = 'Devolucion compra', -1*PR.cantidad,PR.cantidad)) as 'cantidad' FROM producto_remito PR
        join pedido_producto PP on PR.producto_id_producto = PP.id_pedido_producto
        join pedidos P on PP.pedido_id_pedido = P.id_pedido
        where cast(PR.created_at as date) = cast(NOW() as date) and PP.producto_id_producto = ".$id." and ((P.tipo <> 'Devolucion') or (P.tipo = 'Devolucion' and P.motivo_devolucion <> 'defecto'))
        group by P.tipo, PR.almacen_id_almacen, PP.producto_id_producto
        order by PR.created_at desc) as ITM_REM"), 
            function($join){
                $join->on('almacen_producto.almacen_id_almacen', '=', 'ITM_REM.almacen_id_almacen')
                ->on('almacen_producto.producto_id_producto', '=', 'ITM_REM.producto_id_producto');
            })
        ->selectRaw("P.tipo, P.conversion, if(P.tipo = 'Insumo', concat(' ', P.unidad),'') as unidad, (ifnull(almacen_producto.cantidad,0.00) + sum(ifnull(MOV_DEST.cantidad,0.00)) + sum(ifnull(MOV_ORI.cantidad,0.00)) + 
        sum(if(ITM_REM.tipo = 'Pedido', ITM_REM.cantidad,0.00)) + sum(if(ITM_REM.tipo = 'Devolucion', ITM_REM.cantidad,0.00)) + 
        sum(if(ITM_REM.tipo = 'Compra', ITM_REM.cantidad,0.00)) + sum(if(ITM_REM.tipo = 'Devolucion compra', ITM_REM.cantidad,0.00))) as cantidad")
        ->get()->first();
        // echo json_encode($stock);die;
        if($stock->tipo == 'Insumo'){
            $cantidad = $stock ? ($stock->cantidad * $stock->conversion) : 0;
        }else{
            $cantidad = $stock ? $stock->cantidad : 0;
        }
        $unidad = $stock ? $stock->unidad : '';
        $movstock_origen = DB::table('almacen_producto as AP')->where('AP.almacen_id_almacen', $selectedAlmacen)
        ->where('AP.producto_id_producto', $id)->whereRaw("cast(M.created_at as date) >= cast('".$fecha_desde."' as date)")
        ->join('movimiento_stock as M', 'M.almacen_id_origen', '=', 'AP.id_stock')
        ->join('productos as P', function($join){
            $join->on('P.id_producto', '=', 'AP.producto_id_producto')
            ->whereRaw("(P.tipo <> 'Producto' or M.tipo_movimiento <> 'Producción')");
        })
        ->select('M.tipo_movimiento', 'M.descripcion', 'M.created_at', 'M.cantidad', 'M.id_movimiento_stock', 'P.tipo', 'P.conversion')
        ->orderBy('M.created_at', 'desc')->get();
        $cantMov_origen = $movstock_origen->count();
        $movstock_destino = DB::table('almacen_producto as AP')->where('AP.almacen_id_almacen', $selectedAlmacen)->where('AP.producto_id_producto', $id)
        ->whereRaw("M.tipo_movimiento <> 'Ajuste'")->whereRaw("cast(M.created_at as date) >= cast('".$fecha_desde."' as date)")
        ->join('movimiento_stock as M', 'M.almacen_id_destino', '=', 'AP.id_stock')
        ->join('productos as P', function($join){
            $join->on('P.id_producto', '=', 'AP.producto_id_producto')
            ->whereRaw("(P.tipo <> 'Insumo' or M.tipo_movimiento <> 'Producción')");
        })
        ->select('M.tipo_movimiento', 'M.descripcion', 'M.created_at', 'M.cantidad', 'M.id_movimiento_stock', 'P.tipo', 'P.conversion')
        ->orderBy('M.created_at', 'desc')->get();
        $cantMov_destino = $movstock_destino->count();
        $item_remito = DB::table('producto_remito as PR')->where('PR.almacen_id_almacen', $selectedAlmacen)->where('PP.producto_id_producto', $id)
        ->whereRaw("cast(PR.created_at as date) >= cast('".$fecha_desde."' as date) and ((P.tipo <> 'Devolucion') or (P.tipo = 'Devolucion' and P.motivo_devolucion <> 'defecto'))")
        ->join('pedido_producto as PP', 'PR.producto_id_producto', '=', 'PP.id_pedido_producto')
        ->join('productos as PROD', 'PR.producto_id_producto', '=', 'PROD.id_producto')
        ->join('pedidos as P', 'P.id_pedido', '=', 'PP.pedido_id_pedido')
        ->select('P.tipo', 'PROD.tipo as tipo_producto', 'PROD.conversion', 'PR.remito_id_remito as id_remito', 'PR.created_at', 'P.id_pedido')
        ->selectRaw('PR.cantidad as cantidad')
        // ->groupBy('PR.almacen_id_almacen', 'PP.producto_id_producto', 'P.tipo')
        ->orderBy('PR.created_at', 'desc')
        ->get();
        $cant_items = $item_remito->count();
        $index_mo = 0;$index_md = 0;$index_i = 0;
        $array_movimientos = array();
        if($cant_items > 0){
            $fecha_remito = strtotime($item_remito[$index_i]->created_at);
        }else{
            $fecha_remito = date('1970-01-01');
        }
        if($cantMov_origen > 0){
            $fecha_origen = strtotime($movstock_origen[$index_mo]->created_at);
        }else{
            $fecha_origen = date('1970-01-01');
        }
        if($cantMov_destino > 0){
            $fecha_destino = strtotime($movstock_destino[$index_md]->created_at);
        }else{
            $fecha_destino = date('1970-01-01');
        }

        while($index_i < $cant_items || $index_md < $cantMov_destino || $index_mo < $cantMov_origen){
            // echo ($index_mo == $cantMov_origen) ? 'SI' : 'NO';
            $movimiento = new stdClass;
            if(($index_i < $cant_items) 
            && ($fecha_remito >= $fecha_origen) 
            && ($fecha_remito >= $fecha_destino)){
                if($item_remito[$index_i]->tipo_producto == 'Insumo'){
                    $mov_cantidad = $item_remito[$index_i]->cantidad * $item_remito[$index_i]->conversion;
                }else{
                    $mov_cantidad = $item_remito[$index_i]->cantidad;
                }
                $movimiento->cantidad = $mov_cantidad.$unidad;
                $movimiento->fecha = $item_remito[$index_i]->created_at;
                $movimiento->stock = $cantidad.$unidad;
                if($item_remito[$index_i]->tipo == 'Pedido'){
                    $movimiento->detalle = "Remito Nro. ".$item_remito[$index_i]->id_remito;
                    $movimiento->descripcion_movimiento = 'Venta';
                    $movimiento->link = route('remitos.ver', [$item_remito[$index_i]->id_remito]);
                    $cantidad = $cantidad + $mov_cantidad;
                }else if($item_remito[$index_i]->tipo == 'Devolucion'){
                    $movimiento->detalle = "Nota de credito Nro. ".$item_remito[$index_i]->id_remito;
                    $movimiento->descripcion_movimiento = 'Devolucion venta';
                    $movimiento->link = route('devoluciones.ver', [$item_remito[$index_i]->id_pedido]);
                    $cantidad = $cantidad - $mov_cantidad;
                }else if($item_remito[$index_i]->tipo == 'Compra'){
                    $movimiento->detalle = "Remito Nro. ".$item_remito[$index_i]->id_remito;
                    $movimiento->descripcion_movimiento = $item_remito[$index_i]->tipo;
                    $movimiento->link = route('compras.ver', [$item_remito[$index_i]->id_pedido]);
                    $cantidad = $cantidad - $mov_cantidad;
                }else if($item_remito[$index_i]->tipo == 'Devolucion compra'){
                    $movimiento->detalle = "Nota de credito Nro. ".$item_remito[$index_i]->id_remito;
                    $movimiento->link = route('compras.ver', [$item_remito[$index_i]->id_pedido]);
                    $movimiento->descripcion_movimiento = $item_remito[$index_i]->tipo;
                    $cantidad = $cantidad + $mov_cantidad;
                }
                $index_i++;
                if($cant_items > $index_i){
                    $fecha_remito = strtotime($item_remito[$index_i]->created_at);
                }else{            
                    $fecha_remito = date('1970-01-01');
                }
            }else if(($index_mo < $cantMov_origen)
            && ($fecha_origen >= $fecha_remito) 
            && ($fecha_origen >= $fecha_destino)){
                if($movstock_origen[$index_mo]->tipo == 'Insumo'){
                    $mov_cantidad = $movstock_origen[$index_mo]->cantidad * $movstock_origen[$index_mo]->conversion;
                }else{
                    $mov_cantidad = $movstock_origen[$index_mo]->cantidad;
                }
                $movimiento->detalle = $movstock_origen[$index_mo]->descripcion;
                $movimiento->cantidad = $mov_cantidad.$unidad;
                $movimiento->fecha = $movstock_origen[$index_mo]->created_at;
                $movimiento->link = route('productos.vermovimiento', [$movstock_origen[$index_mo]->id_movimiento_stock]);
                $movimiento->stock = $cantidad.$unidad;
                if($movstock_origen[$index_mo]->tipo_movimiento == 'Ajuste'){
                    $cantidad = $cantidad - $mov_cantidad;
                    $movimiento->descripcion_movimiento = 'Ajuste';
                }else if($movstock_origen[$index_mo]->tipo_movimiento == 'Transferencia'){
                    $cantidad = $cantidad + $mov_cantidad;
                    $movimiento->descripcion_movimiento = 'Transferencia';
                }else if($movstock_origen[$index_mo]->tipo_movimiento == 'Producción'){
                    $cantidad = $cantidad + $mov_cantidad;
                    $movimiento->descripcion_movimiento = 'Producción';
                }
                $index_mo++;
                if($cantMov_origen > $index_mo){
                    $fecha_origen = strtotime($movstock_origen[$index_mo]->created_at);
                }else{
                    $fecha_origen = date('1970-01-01');
                }
            }else if(($index_md < $cantMov_destino)
            && ($fecha_destino >= $fecha_remito) 
            && ($fecha_destino >= $fecha_origen)){
                if($movstock_destino[$index_md]->tipo == 'Insumo'){
                    $mov_cantidad = $movstock_destino[$index_md]->cantidad * $movstock_destino[$index_md]->conversion;
                }else{
                    $mov_cantidad = $movstock_destino[$index_md]->cantidad;
                }
                $movimiento->detalle = $movstock_destino[$index_md]->descripcion;
                $movimiento->cantidad = $mov_cantidad.$unidad;
                $movimiento->fecha = $movstock_destino[$index_md]->created_at;
                $movimiento->stock = $cantidad.$unidad;
                $movimiento->link = route('productos.vermovimiento', [$movstock_destino[$index_md]->id_movimiento_stock]);
                $movimiento->descripcion_movimiento = $movstock_destino[$index_md]->tipo_movimiento;
                $cantidad = $cantidad - $mov_cantidad;
                $index_md++;
                if($cantMov_destino > $index_md){
                    $fecha_destino = strtotime($movstock_destino[$index_md]->created_at);
                }else{
                    $fecha_destino = date('1970-01-01');
                }
            }
            array_push($array_movimientos, $movimiento);

        }
        // echo json_encode($array_movimientos);die;
        return view('productos.stock', ['id_producto' => $id, 'almacenes' => $almacenes, 'selectedAlmacen' => $selectedAlmacen, 'fecha_desde' => $fecha_desde, 'movimientos' => $array_movimientos, 'stock_final' => $cantidad.$unidad]);
    }

    public function precios(Request $request){
        $fields = $request->all();
        $rubros = DB::table('rubros')->where('deleted', 0)->orderBy('nombre')->get();
        $marcas = DB::table('marcas')->where('deleted', 0)->orderBy('nombre')->get();
        $proveedores = DB::table('users')->where('users.deleted', 0)->whereRaw("perfils.nombre = 'Proveedor'")
        ->leftJoin('perfils', 'perfils.id_perfil', 'users.perfil_id')->get();
        $search = '';
        $marcaSelected = null;
        $rubroSelected = null;
        $proveedorSelected = null;
        $productos = DB::table('productos')->where('productos.deleted', 0)->where('productos.estado', 1)->whereRaw("productos.tipo <> 'Servicio'");
        if(isset($fields['search'])){
            $search = $fields['search'];
            $productos = $productos->whereRaw("(productos.codigo like '%" . $search . "%' or productos.nombre like '%" . $search . "%')");
            // $productos = $productos->where('productos.codigo', 'like', '%' . $search . '%')
            // ->orWhere('productos.nombre', 'like', '%' . $search . '%');
        }
        if(isset($fields['marca_id']) && $fields['marca_id'] !== null){
            $marcaSelected = $fields['marca_id'];
            $productos = $productos->where('marcas.id_marca', '=', $marcaSelected);
        }
        if(isset($fields['rubro_id']) && $fields['rubro_id'] !== null){
            $rubroSelected = $fields['rubro_id'];
            $productos = $productos->where('rubros.id_rubro', '=', $rubroSelected);
        }
        if(isset($fields['user_id']) && $fields['user_id'] !== null){
            $proveedorSelected = $fields['user_id'];
            $productos = $productos->where('users.id_user', '=', $proveedorSelected);
        }
        $productos = $productos->join('producto_user', 'producto_user.producto_id_producto', '=', 'productos.id_producto')
        ->join('users', 'users.id_user', '=', 'producto_user.user_id_user')
        ->leftJoin('imagens', function($join){
            $join->on('imagens.producto_id', '=', 'productos.id_producto')
            ->where('imagens.principal', '=', 1);
        })
        ->leftJoin('marca_producto', 'marca_producto.producto_id_producto', '=', 'productos.id_producto')
        ->leftJoin('marcas', 'marcas.id_marca', '=', 'marca_producto.marca_id_marca')
        ->leftJoin('producto_rubro', 'producto_rubro.producto_id_producto', '=', 'productos.id_producto')
        ->leftJoin('rubros', 'rubros.id_rubro', '=', 'producto_rubro.rubro_id_rubro')
        ->select('productos.nombre', 'productos.codigo', 'marcas.nombre as marca', 'rubros.nombre as rubro', 'users.name as proveedor', 
        'producto_user.costo_dolares as costo', 'producto_user.margen as margen_proveedor', 'producto_user.seleccionado', 'producto_user.codigo_proveedor', 
        'producto_user.id_costo','users.valor_dolar','users.id_user','productos.id_producto','imagens.nombre as nombre_imagen')
        ->orderBy('productos.codigo')
        ->get();
        return view('productos.precios', ['productos' => $productos, 'search' => $search, 'marcas' => $marcas,'marcaSelected' => $marcaSelected, 
        'proveedores' => $proveedores, 'proveedorSelected' => $proveedorSelected, 'rubroSelected' => $rubroSelected, 'rubros' => $rubros]);
    }

    public function editarmargen(Request $request){
        $fields = $request->all();
        DB::table('producto_user')
        ->where('id_costo', $fields['id_costo'])
        ->update(['margen' => $fields['margen'], 'costo_dolares' => $fields['costo']]);
        $proveedor = DB::table('producto_user')->where('id_costo', $fields['id_costo'])->get()->first();
        // echo json_encode($proveedor);die;
        if(env('MARGEN_PROVEEDOR', false) && $proveedor->seleccionado == 1){
            DB::table('productos')
            ->where('id_producto', $proveedor->producto_id_producto)
            ->update(['margen' => $fields['margen']]);
        }
        // Session::flash('message', 'El costo se actualizó con éxito!');
        // Session::flash('type', 'success');
        // return Redirect::to('admin/productos/precios');
    }

    public function eliminaralmacen(Request $request, $id_producto, $id)
    {
        DB::table('almacen_producto')->where('id_stock', $id)->delete();
        Session::flash('message', 'El almacen se eliminó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/productos/ver/'.$id_producto);
        // return view('productos.ver', [$id]);
    }

    public function vermovimiento($id)
    {
        $movimiento = DB::table('movimiento_stock as M')->where('M.id_movimiento_stock', $id)
        ->join('users as U', 'U.id_user', '=', 'M.user_id_user')
        ->join('almacen_producto as APO', 'APO.id_stock', '=', 'M.almacen_id_origen')
        ->join('almacens as AO', 'AO.id_almacen', '=', 'APO.almacen_id_almacen')
        ->join('productos as P', 'P.id_producto', '=', 'APO.producto_id_producto')
        ->join('almacen_producto as APD', 'APD.id_stock', '=', 'M.almacen_id_destino')
        ->join('almacens as AD', 'AD.id_almacen', '=', 'APD.almacen_id_almacen')
        ->select('M.*', 'AO.nombre as almacen_origen', 'AD.nombre as almacen_destino', 'P.nombre as producto', 'P.codigo as codigo')
        ->selectRaw("concat(U.name, ' ', U.surname) as usuario")
        ->get()->first();
        // echo json_encode($movimiento);die;
        return view('productos.vermovimiento', ['movimiento' => $movimiento]);
    }

    public function agregarmovimiento(Request $request, $id)
    {
        $fields = $request->all();
        // echo json_encode($fields);
        // die;
        $messages = [
            'required' => 'El :attribute es requerido',
            'unique'    => 'El :attribute ingresado ya esta en uso',
          ];
        $rules = array(
            'almacen_id_origen'=>'required',
            'almacen_id_destino'=>'required',
            'cantidad'=>'required'
        );
        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/productos/ver/'.$id)
                ->withErrors($validator);
        } else {
            if($fields['tipo_movimiento'] == 'Ajuste'){
                DB::table('movimiento_stock')->insert([
                    [
                        'tipo_movimiento' => 'Ajuste',
                        'almacen_id_origen' => $fields['almacen_id_origen'],
                        'almacen_id_destino' => $fields['almacen_id_origen'],
                        'descripcion' => $fields['descripcion'],
                        'cantidad' => $fields['cantidad'],
                        'user_id_user' => Auth::user()->id_user,
                        'created_at' =>  date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]
                ]);
            }else if($fields['tipo_movimiento'] == 'Transferencia'){
                DB::table('movimiento_stock')->insert([
                    [
                        'tipo_movimiento' => 'Transferencia',
                        'almacen_id_origen' => $fields['almacen_id_origen'],
                        'almacen_id_destino' => $fields['almacen_id_destino'],
                        'descripcion' => $fields['descripcion'],
                        'cantidad' => $fields['cantidad'],
                        'user_id_user' => Auth::user()->id_user,
                        'created_at' =>  date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]
                ]);
            }
            Session::flash('message', 'El movimiento se agregó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/productos/ver/'.$id);
        }
    }

    public function toimportar(Request $request)
    {
        Excel::import(new ProductosImport,$request->file('file'));
           
        return back();
    }

    public function getDownload(){
        $file="./productos.csv";
        return Response::download($file);
    }

    public function imagenes($id_producto)
    {
       return view('productos.imagenes', ['id_producto' => $id_producto]);
    }

    public function processimages(){
        $imagenes_OK = array();
        $imagenes_ERROR = array('No se pudieron procesar:');
        $path = $_SERVER['DOCUMENT_ROOT'] . "/Fotos productos";
        // Abrimos la carpeta que nos pasan como parámetro
        $dir = opendir($path);
        // Leo todos los ficheros de la carpeta
        while ($elemento = readdir($dir)){
            // Tratamos los elementos . y .. que tienen todas las carpetas
            if( $elemento != "." && $elemento != ".."){
                // Si es una carpeta
                if( is_dir($path.$elemento) ){
                    // Muestro la carpeta
                    echo "<p><strong>CARPETA: ". $elemento ."</strong></p>";
                // Si es un fichero
                } else {
                    // Muestro el fichero
                    // echo "<br />". $elemento;
                    
                    $codigo_producto = explode(".", $elemento)[0];
                    $search_cod = $codigo_producto;
                    $principal = 1;
                    if((substr($codigo_producto, -1) == "A") || (substr($codigo_producto, -1) == "B") || (substr($codigo_producto, -1) == "C") || (substr($codigo_producto, -1) == "D")){
                        $search_cod = substr($codigo_producto, 0, -1);
                        $principal = 0;
                    }
                    $producto = DB::table('productos')->where('codigo', $search_cod)->get()->first();
                    if($producto !== null){
                        DB::table('imagens')->where('nombre', '=', $elemento)->delete();
                        $img = Image::make(public_path('Fotos productos') . "/" . $elemento);
                        // $img = Image::make($path.$elemento);
                        if (!file_exists(public_path('images/'.$producto->id_producto ."/"))) {
                            mkdir('images/'.$producto->id_producto);
                        }
                        $img->save(public_path('images/'.$producto->id_producto.'/'.$elemento));
                        $img->resize(200, 200, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                        $img->save(public_path('images/'.$producto->id_producto.'/MIN'.$elemento));

                        $newImagen = new Imagen([
                            'nombre' => $elemento,
                            'producto_id' => $producto->id_producto,
                            'principal' => $principal
                        ]);
                        $newImagen->save();
                        array_push($imagenes_OK, $elemento);
                    }else{
                        array_push($imagenes_ERROR, $elemento);
                    }
                }
            }
        }
        return response()->json(['errores'=> $imagenes_ERROR], 200);
    }

    public function upload_image(Request $request)
    {
        $image = $request->file('file');
        $id_producto = $request->get('id_producto');
        $imageName = $image->getClientOriginalName();

        $image->move(public_path('images/' . $id_producto), $imageName);

        $img = Image::make(public_path('images/' . $id_producto) . "/" . $imageName);
        $img->resize(200, 200, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $img->save('images/'.$id_producto.'/MIN'.$imageName);

        $existe = DB::table('imagens')->where('producto_id', $id_producto)->get();
        if(count($existe) > 0){
            $principal = 0;
        }else{
            $principal = 1;
        }

        $newImagen = new Imagen([
            'nombre' => $imageName,
            'producto_id' => $id_producto,
            'principal' => $principal
        ]);
        $newImagen->save();

        return response()->json(['success' => $imageName]);	 
    }
	
	function fetch_image(Request $request)
    {
        $id_producto = $request->get('id_producto');
        // $images = \File::allFiles(public_path('images/'.$id_producto));
        $images = DB::table('imagens')->where('producto_id', $id_producto)->get();
        $output = '<div class="row">';
        foreach($images as $image)
        {
        if($image->principal){
            $style = "background-color: lightgray;";
        }else{
            $style = "";
        }
        $output .= '<div class="col-md-3 mb-10" style="margin-bottom: 30px;">
                        <div class="row">
                            <a target="_blank" href="'.asset('images/' . $id_producto . "/" . $image->nombre).'">
                                <img src="'.asset('images/' . $id_producto . "/MIN" . $image->nombre).'" class="img-thumbnail" width="250" height="250"/>
                            </a>
                        </div>
                        <div class="row">
                            <button type="button" class="btn btn-link remove_image" name="'.$image->nombre.'" id="'.$image->id_imagen.'">Quitar</button>
                            <button style="'.$style.'" type="button" class="btn btn-link ppal_image" name="'.$image->nombre.'" id="'.$image->id_imagen.'">Principal</button>
                        </div>
                    </div>';
        }
        $output .= '</div>';
        echo $output;
    }
 
    function delete_image(Request $request)
    {
        // echo var_dump($request->all());die;
        $id_producto = $request->get('id_producto');
        if($request->get('id'))
        {
            $imagen = Imagen::find($request->get('id'));
            $imagen->delete();
            \File::delete(public_path('images/' . $id_producto . "/" . $request->get('name')));
            \File::delete(public_path('images/' . $id_producto . "/MIN" . $request->get('name')));
        }
    }
 
    function ppal_image(Request $request)
    {
        // echo var_dump($request->all());die;
        $id_producto = $request->get('id_producto');
        if($request->get('id'))
        {
            DB::table('imagens')->where('producto_id', $id_producto)->update(['principal' => 0]);
            DB::table('imagens')->where('id_imagen', $request->get('id'))->update(['principal' => 1]);
        }
    }
}
