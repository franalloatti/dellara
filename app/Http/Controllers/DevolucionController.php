<?php

namespace App\Http\Controllers;

use Auth;
use App\Pedido;
use App\Remito;
use App\Estado;
use App\Direccion;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class DevolucionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function todas(Request $request)
    {
        $fields = $request->all();
        $search = '';
        $pedidos = DB::table('pedidos')->where('pedidos.tipo', '=', 'Devolucion');
        if(isset($fields['search'])){
            $search = $fields['search'];
            $pedidos = $pedidos->where('pedidos.id_pedido', 'like', '%' . $search . '%')
            ->orWhere('users.name', 'like', '%' . $search . '%')
            ->orWhere('users.surname', 'like', '%' . $search . '%')
            ->orWhere('estados.descripcion', 'like', '%' . $search . '%');
        }
        $pedidos = $pedidos->leftJoin('pedido_producto', 'pedido_producto.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('remitos', 'remitos.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('users as proveedor', 'proveedor.id_user', '=', 'producto_user.user_id_user')
        // ->leftJoin('estado_pedido', 'estado_pedido.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('perfils', 'users.perfil_id', '=', 'perfils.id_perfil')
        ->select('pedidos.*','remitos.id_remito','estados.descripcion as estado')
        ->selectRaw("IF(perfils.nombre = 'Mayorista', users.name, concat(users.name, ' ', users.surname)) as name, sum((pedido_producto.precio_unitario * pedido_producto.cantidad) * (1-(pedido_producto.descuento/100))) as monto")
        ->groupBy('pedidos.id_pedido')
        ->paginate(10);
        return view('devoluciones.todas', ['search' =>  $search, 'pedidos' => $pedidos]);
    }

    public function pendientes(Request $request)
    {
        $fields = $request->all();
        $search = '';
        $pedidos = DB::table('pedidos')->where('pedidos.tipo', '=', 'Devolucion');
        if(isset($fields['search'])){
            $search = $fields['search'];
            $pedidos = $pedidos->where('pedidos.id_pedido', 'like', '%' . $search . '%')
            ->orWhere('users.name', 'like', '%' . $search . '%')
            ->orWhere('users.surname', 'like', '%' . $search . '%')
            ->orWhere('estados.descripcion', 'like', '%' . $search . '%');
        }
        $pedidos = $pedidos->leftJoin('pedido_producto', 'pedido_producto.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('remitos', 'remitos.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('users as proveedor', 'proveedor.id_user', '=', 'producto_user.user_id_user')
        // ->leftJoin('estado_pedido', 'estado_pedido.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('perfils', 'users.perfil_id', '=', 'perfils.id_perfil')
        ->select('pedidos.*','remitos.id_remito','estados.descripcion as estado','estado_pedido.estado_id_estado')
        ->selectRaw("IF(perfils.nombre = 'Mayorista', users.name, concat(users.name, ' ', users.surname)) as name, sum((pedido_producto.precio_unitario * pedido_producto.cantidad) * (1-(pedido_producto.descuento/100))) as monto")
        ->groupBy('pedidos.id_pedido')
        ->havingRaw('estado_id_estado = 2')
        ->paginate(10);
        return view('devoluciones.pendientes', ['search' =>  $search, 'pedidos' => $pedidos]);
    }

    public function misdevoluciones(Request $request)
    {
        $fields = $request->all();
        $search = '';
        $pedidos = DB::table('pedidos')->where('pedidos.tipo', '=', 'Devolucion')->where('pedidos.user_id_user', '=', Auth::user()->id_user);
        if(isset($fields['search'])){
            $search = $fields['search'];
            $pedidos = $pedidos->where('pedidos.id_pedido', 'like', '%' . $search . '%')
            ->orWhere('users.name', 'like', '%' . $search . '%')
            ->orWhere('users.surname', 'like', '%' . $search . '%')
            ->orWhere('estados.descripcion', 'like', '%' . $search . '%');
        }
        $pedidos = $pedidos->leftJoin('pedido_producto', 'pedido_producto.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        // ->leftJoin('estado_pedido', 'estado_pedido.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->select('pedidos.*', 'estados.descripcion as estado', 'estados.id_estado', 'users.name', 'users.surname')
        ->selectRaw('sum((pedido_producto.precio_unitario * pedido_producto.cantidad) * (1-(pedido_producto.descuento/100))) as monto')
        ->groupBy('pedidos.id_pedido')
        // ->having('id_estado', '>', '1')
        ->paginate(10);
        return view('devoluciones.misdevoluciones', ['search' =>  $search, 'pedidos' => $pedidos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientes = DB::table('users')->where('users.deleted', 0)->where('roles.name', '<>', 'admin')->where('roles.name', '<>', 'administrador')->where('roles.name', '<>', 'Proveedor')
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->leftJoin('perfils', 'users.perfil_id', '=', 'perfils.id_perfil')
        ->select('users.id_user', 'roles.discount as descuento')
        ->selectRaw("IF(perfils.nombre = 'Mayorista', users.name, concat(users.name, ' ', users.surname)) as name")->get();
        return view('devoluciones.create', ['clientes' => $clientes]);
    }

    public function createcliente()
    {
        $cliente = Auth::user();
        $productos = DB::table('pedidos')->where('pedidos.user_id_user', $cliente->id_user)->where('pedidos.tipo', 'Pedido')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('remitos', 'remitos.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('producto_remito', 'remitos.id_remito', '=', 'producto_remito.remito_id_remito')
        ->leftJoin('pedido_producto', 'pedido_producto.id_pedido_producto', '=', 'producto_remito.producto_id_producto')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->select('producto_remito.*', 'remitos.id_remito', 'pedidos.created_at as fecha_pedido', 'productos.id_producto', 'productos.nombre', 'productos.codigo', 'estado_pedido.estado_id_estado')
        ->havingRaw('estado_id_estado = 3')
        ->get();
        return view('devoluciones.createcliente', ['productos' => $productos, 'cliente' => $cliente]);
    }

    public function getproductos($user_id_user = null){
        $productos = DB::table('pedidos')->where('pedidos.user_id_user', $user_id_user)->where('pedidos.tipo', 'Pedido')
        ->join('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->join('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->join('remitos', 'remitos.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->join('producto_remito', 'remitos.id_remito', '=', 'producto_remito.remito_id_remito')
        ->join('pedido_producto', 'pedido_producto.id_pedido_producto', '=', 'producto_remito.producto_id_producto')
        ->join('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->select('producto_remito.*', 'remitos.id_remito', 'pedidos.created_at as fecha_pedido', 'productos.id_producto', 'productos.nombre', 'productos.codigo', 'estado_pedido.estado_id_estado')
        ->selectRaw('sum(producto_remito.cantidad) as cantidad_total')
        ->havingRaw('estado_id_estado = 3')
        ->groupBy('productos.id_producto')
        ->get();
        return $productos;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->all();
        // echo json_encode($fields);
        // die;
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'user_id_user'=>'required',
            'motivo_devolucion'=>'required',
            'items'=>'required'
        );

        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/devoluciones/crear')
                ->withErrors($validator);
                // return view('marcas.crear');
        } else {
            $direccion = DB::table('direccions')->where('user_id', $request->get('user_id_user'))->where('es_direccion_fact', 1)->get()->first();
            $pedido = new Pedido([
                'tipo' => 'Devolucion',
                'user_id_user' => $request->get('user_id_user'),
                'motivo_devolucion' => $request->get('motivo_devolucion'),
                'descripcion' => $request->get('descripcion'),
                'direccion_id_direccion' => $direccion ? $direccion->id_direccion : 1,
                'usuario_crea_id' => Auth::user()->id_user,
                'created_at' =>  date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
            $pedido->save();
            $pedido->estados()->sync(['estado_id_estado' => 2], false);
            foreach($request->get('items') as $item){
                DB::table('pedido_producto')->insert(
                    [
                    'pedido_id_pedido' => $pedido->id_pedido, 
                    'producto_id_producto' => $item['id'], 
                    'cantidad' => $item['cantidad'], 
                    'descuento' => 0, 
                    'costo_unitario' => $item['costo_unitario'],
                    'precio_unitario' => $item['precio_unitario'],
                    'created_at' =>  date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    ]
                );
            }
            // redirect
            Session::flash('message', 'La solicitud de devolución se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/devoluciones/ver/'.$pedido->id_pedido);
        }
    }

    public function storecliente(Request $request)
    {
        $fields = $request->all();
        // echo json_encode($fields);
        // die;
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'user_id_user'=>'required',
            'motivo_devolucion'=>'required',
            'items'=>'required'
        );

        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('devoluciones/createcliente')
                ->withErrors($validator);
                // return view('marcas.crear');
        } else {
            $direccion = DB::table('direccions')->where('user_id', $request->get('user_id_user'))->where('es_direccion_fact', 1)->get()->first();
            $pedido = new Pedido([
                'tipo' => 'Devolucion',
                'user_id_user' => $request->get('user_id_user'),
                'motivo_devolucion' => $request->get('motivo_devolucion'),
                'direccion_id_direccion' => $direccion ? $direccion->id_direccion : 1,
                'usuario_crea_id' => Auth::user()->id_user,
                'created_at' =>  date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
            $pedido->save();
            $pedido->estados()->sync(['estado_id_estado' => 2], false);
            foreach($request->get('items') as $item){
                DB::table('pedido_producto')->insert(
                    [
                    'pedido_id_pedido' => $pedido->id_pedido, 
                    'producto_id_producto' => $item['id'], 
                    'cantidad' => $item['cantidad'], 
                    'descuento' => 0, 
                    'costo_unitario' => $item['costo_unitario'],
                    'precio_unitario' => $item['precio_unitario'],
                    'created_at' =>  date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    ]
                );
            }
            // redirect
            Session::flash('message', 'La solicitud de devolución se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('misdevoluciones');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pedido  $pedido
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pedido = DB::table('pedidos')->where('pedidos.id_pedido', $id)
        ->leftJoin('pedido_producto', 'pedido_producto.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('remitos', 'remitos.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('perfils', 'users.perfil_id', '=', 'perfils.id_perfil')
        ->leftJoin('direccions', 'direccions.id_direccion', '=', 'pedidos.direccion_id_direccion')
        ->select('pedidos.*', 'remitos.id_remito', 'estados.descripcion as estado', 'direccions.direccion', 'direccions.localidad', 'direccions.provincia')
        ->selectRaw("IF(perfils.nombre = 'Mayorista', users.name, concat(users.name, ' ', users.surname)) as name, sum(pedido_producto.precio_unitario * pedido_producto.cantidad) as monto_sin_desc, sum((pedido_producto.precio_unitario * pedido_producto.cantidad) * (1-(pedido_producto.descuento/100))) as monto_con_desc")
        ->groupBy('pedidos.id_pedido')
        ->get()->first();
        $productos = DB::table('pedido_producto')->where('pedido_producto.pedido_id_pedido', $id)
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('users', 'producto_user.user_id_user', '=', 'users.id_user')
        ->leftJoin(DB::raw("(SELECT `almacens`.`nombre`, `almacen_producto`.`producto_id_producto`, `almacen_producto`.`cantidad` FROM `almacen_producto`
        join `almacens` on `almacens`.`id_almacen` = `almacen_producto`.`almacen_id_almacen` and `almacens`.`nombre` <> 'Mercado Libre') as TEMP "),function($join){
           $join->on('productos.id_producto', '=', 'TEMP.producto_id_producto');
        })
        // ->join('almacen_producto', 'almacen_producto.producto_id_producto', '=', 'productos.id_producto')
        // // ->leftJoin('almacens', 'almacens.id_almacen', '=', 'almacen_producto.almacen_id_almacen')
        // ->join('almacens', function($join) {
        //     $join->on('almacens.id_almacen', 'almacen_producto.almacen_id_almacen')
        //         ->where('almacens.nombre', '<>', "Mercado Libre");
        // })
        ->select('pedido_producto.*','productos.nombre','productos.codigo', 'users.valor_dolar')
        ->selectRaw('ifnull(sum(TEMP.cantidad),0) as stock')
        ->groupBy('pedido_producto.producto_id_producto')->get();
        $estados = DB::table('estados')->get();
        return view('devoluciones.show', ['pedido' => $pedido, 'productos' => $productos, 'estados' => $estados]);
    }

    public function midevolucion($id)
    {
        $pedido = DB::table('pedidos')->where('pedidos.id_pedido', $id)
        ->leftJoin('pedido_producto', 'pedido_producto.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('direccions', 'direccions.id_direccion', '=', 'pedidos.direccion_id_direccion')
        ->select('pedidos.*','estados.descripcion as estado', 'users.name', 'users.surname', 'direccions.direccion', 'direccions.localidad', 'direccions.provincia')
        ->selectRaw('sum(pedido_producto.precio_unitario * pedido_producto.cantidad) as monto_sin_desc, sum((pedido_producto.precio_unitario * pedido_producto.cantidad) * (1-(pedido_producto.descuento/100))) as monto_con_desc')
        ->groupBy('pedidos.id_pedido')
        ->get()->first();
        $productos = DB::table('pedido_producto')->where('pedido_producto.pedido_id_pedido', $id)
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin(DB::raw("(SELECT `almacens`.`nombre`, `almacen_producto`.`producto_id_producto`, `almacen_producto`.`cantidad` FROM `almacen_producto`
        join `almacens` on `almacens`.`id_almacen` = `almacen_producto`.`almacen_id_almacen` and `almacens`.`nombre` <> 'Mercado Libre') as TEMP "),function($join){
           $join->on('productos.id_producto', '=', 'TEMP.producto_id_producto');
        })
        // ->join('almacen_producto', 'almacen_producto.producto_id_producto', '=', 'productos.id_producto')
        // // ->leftJoin('almacens', 'almacens.id_almacen', '=', 'almacen_producto.almacen_id_almacen')
        // ->join('almacens', function($join) {
        //     $join->on('almacens.id_almacen', 'almacen_producto.almacen_id_almacen')
        //         ->where('almacens.nombre', '<>', "Mercado Libre");
        // })
        ->select('pedido_producto.*','productos.nombre','productos.codigo')
        ->selectRaw('ifnull(sum(TEMP.cantidad),0) as stock')
        ->groupBy('pedido_producto.producto_id_producto')->get();
        $estados = DB::table('estados')->get();
        return view('devoluciones.midevolucion', ['pedido' => $pedido, 'productos' => $productos, 'estados' => $estados]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pedido  $pedido
     * @return \Illuminate\Http\Response
     */
    public function notadecredito($id)
    {
        $pedido = DB::table('pedidos')->where('pedidos.tipo', '=', 'Devolucion')->where('pedidos.id_pedido', $id)
        ->leftJoin('pedido_producto', 'pedido_producto.pedido_id_pedido', '=', 'pedidos.id_pedido')
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('estado_pedido', function($join) {
            $join->on('pedidos.id_pedido', 'estado_pedido.pedido_id_pedido')
                ->on('estado_pedido.created_at', DB::raw('(SELECT MAX(es.created_at) from estado_pedido as es where es.pedido_id_pedido = pedidos.id_pedido)'));
        })
        ->leftJoin('estados', 'estados.id_estado', '=', 'estado_pedido.estado_id_estado')
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('perfils', 'users.perfil_id', '=', 'perfils.id_perfil')
        ->leftJoin('direccions', 'direccions.id_direccion', '=', 'pedidos.direccion_id_direccion')
        ->select('pedidos.*','estados.descripcion as estado', 'direccions.id_direccion', 'direccions.direccion', 'direccions.localidad', 'direccions.provincia')
        ->selectRaw("IF(perfils.nombre = 'Mayorista', users.name, concat(users.name, ' ', users.surname)) as name, sum(pedido_producto.precio_unitario * pedido_producto.cantidad) as monto_sin_desc, sum((pedido_producto.precio_unitario * pedido_producto.cantidad) * (1-(pedido_producto.descuento/100))) as monto_con_desc")
        ->groupBy('pedidos.id_pedido')
        ->get()->first();
        $direcciones = DB::table('direccions')->where('direccions.user_id', $pedido->user_id_user)->get();
        $productos = DB::table('pedido_producto')->where('pedido_producto.pedido_id_pedido', $id)
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('users', 'producto_user.user_id_user', '=', 'users.id_user')
        ->join('almacen_producto', 'almacen_producto.producto_id_producto', '=', 'productos.id_producto')
        // ->leftJoin('almacens', 'almacens.id_almacen', '=', 'almacen_producto.almacen_id_almacen')
        ->join('almacens', function($join) {
            $join->on('almacens.id_almacen', 'almacen_producto.almacen_id_almacen')
                ->where('almacens.nombre', '<>', "Mercado Libre");
        })
        ->select('pedido_producto.*','productos.id_producto','productos.margen','productos.nombre','productos.codigo','users.valor_dolar', 'producto_user.costo_dolares')
        ->selectRaw('sum(almacen_producto.cantidad) as stock')
        ->groupBy('pedido_producto.producto_id_producto')->get();
        return view('devoluciones.notadecredito', ['pedido' => $pedido, 'productos' => $productos, 'direcciones' => $direcciones]);
    }

    public function rechazar($id_pedido)
    {
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        DB::table('estado_pedido')->insert(
            [
            'pedido_id_pedido' => $id_pedido, 
            'estado_id_estado' => 4, 
            'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            ]
        );
        // redirect
        Session::flash('message', 'El pedido de devolución fue rechazado!');
        Session::flash('type', 'success');
        return Redirect::to('admin/devoluciones/ver/'.$id_pedido);
    }

    public function aceptar($id_pedido)
    {
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $pedido = DB::table('pedidos')->where('pedidos.tipo', '=', 'Devolucion')
        ->where('pedidos.id_pedido', $id_pedido)->get()->first();
        $remito = new Remito([
            'pedido_id_pedido' => $id_pedido
        ]);
        $productos = DB::table('pedido_producto')->where('pedido_producto.pedido_id_pedido', $id_pedido)->get();
        $remito->save();
        $totalMontoRemito = 0;
        foreach($productos as $producto){
            $totalMontoRemito = $totalMontoRemito + ($producto->cantidad * $producto->precio_unitario);
            DB::table('producto_remito')->insert(
                [
                'remito_id_remito' => $remito->id_remito, 
                'producto_id_producto' => $producto->id_pedido_producto, 
                'cantidad' => $producto->cantidad, 
                'descuento' => $producto->descuento, 
                'costo_unitario' => $producto->costo_unitario,
                'precio_unitario' => $producto->precio_unitario,
                'created_at' =>  date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                ]
            );
            // if($pedido->motivo_devolucion == 'error'){
            //     $almacen_producto = DB::table('almacen_producto')->where('producto_id_producto', $producto->producto_id_producto)->where('almacen_id_almacen', 1)
            //         ->increment('cantidad', $producto->cantidad);
            // }
        }
        $remito->update(['monto' => $totalMontoRemito]);
        DB::table('estado_pedido')->insert(
            [
            'pedido_id_pedido' => $id_pedido, 
            'estado_id_estado' => 3, 
            'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            ]
        );
        // redirect
        Session::flash('message', 'El pedido de devolución fue aceptado!');
        Session::flash('type', 'success');
        return Redirect::to('admin/devoluciones/ver/'.$id_pedido);
    }

    public function edit($id)
    {
        $pedido = DB::table('pedidos')->where('pedidos.id_pedido', $id)
        ->leftJoin('users', 'users.id_user', '=', 'pedidos.user_id_user')
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->select('pedidos.*', 'users.name', 'users.surname', 'roles.discount')
        ->groupBy('pedidos.id_pedido')
        ->get()->first();
        $productosPedido = DB::table('pedido_producto')->where('pedido_producto.pedido_id_pedido', $id)
        ->leftJoin('productos', 'productos.id_producto', '=', 'pedido_producto.producto_id_producto')
        ->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('users', 'producto_user.user_id_user', '=', 'users.id_user')
        ->join('almacen_producto', 'almacen_producto.producto_id_producto', '=', 'productos.id_producto')
        // ->leftJoin('almacens', 'almacens.id_almacen', '=', 'almacen_producto.almacen_id_almacen')
        ->join('almacens', function($join) {
            $join->on('almacens.id_almacen', 'almacen_producto.almacen_id_almacen')
                ->where('almacens.nombre', '<>', "Mercado Libre");
        })
        ->select('pedido_producto.*','productos.id_producto','productos.margen','productos.nombre','productos.codigo','users.valor_dolar', 'producto_user.costo_dolares')
        ->selectRaw('sum(almacen_producto.cantidad) as stock')
        ->groupBy('pedido_producto.producto_id_producto')->get();
        return view('devoluciones.edit', ['pedido' => $pedido, 'productosPedido' => $productosPedido]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pedido  $pedido
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fields = $request->all();
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        // echo json_encode($fields);die;
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'user_id_user'=>'required',
            'items'=>'required'
        );

        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/devoluciones/editar'.$id)
                ->withErrors($validator);
                // return view('marcas.crear');
        } else {
            $pedido = Pedido::where('id_pedido',$id)->get()->first();
            $pedido->motivo_devolucion = $fields['motivo_devolucion'];
            $pedido->update();
            $totalMontoRemito = 0;
            foreach($request->get('items') as $item){
                $totalMontoRemito = $totalMontoRemito + ($item['cantidad'] * $item['precio_unitario']);
                DB::table('pedido_producto')
                ->where('id_pedido_producto', $item['id'])
                ->update(
                    [
                    'cantidad' => $item['cantidad'], 
                    'precio_unitario' => $item['precio_unitario'],
                    ]
                );
            }
            DB::table('remitos')->where('pedido_id_pedido', $id)->update(['cantidad' => $totalMontoRemito]);
            // redirect
            Session::flash('message', 'El solicitud de devolución se actualizó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/devoluciones/ver/'.$pedido->id_pedido);
        }
    }
}
