<?php

namespace App\Http\Controllers;

use App\Medio;
use Auth;
use App\Pago;
use App\TipoMovimiento;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class MedioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $medios = DB::table('medios')->where('medios.deleted', 0)
        ->leftJoin(DB::raw("(SELECT ARQ1.cuenta_id, ARQ1.monto as 'monto_arqueo', ARQ1.created_at as 'fecha_max' FROM arqueos as ARQ1 where ARQ1.created_at = 
        (select max(ARQ2.created_at) from arqueos as ARQ2 where ARQ1.cuenta_id = ARQ2.cuenta_id group by ARQ2.cuenta_id)) as ARQ") , 'ARQ.cuenta_id', '=', 'medios.id_medio')
        ->leftJoin(DB::raw("(select medio_id_medio, sum(monto) as 'monto_pagos' from `pagos` where consolidado = 'N' and deleted = 0
                            group by medio_id_medio) as TEMP") , 'TEMP.medio_id_medio', '=', 'medios.id_medio')
        ->select('medios.*')
        ->selectRaw("ifnull(ARQ.monto_arqueo,0) as monto_arqueo, ifnull(ARQ.fecha_max,'') as fecha_arqueo, ifnull(TEMP.monto_pagos,0) as monto_pagos")
        ->groupBy('medios.id_medio')
        ->paginate(10);
        return view('medios.list', ['medios' => $medios]);
    }

    
    public function movimientos(Request $request, $id_cuenta)
    {
        $fields = $request->all();
        $search = '';
        $tipoMovSelected = null;
        $tipo_movimientos = DB::table('tipo_movimientos')->whereRaw("tipo_movimientos.nombre <> 'Venta' and tipo_movimientos.periodo = 0")
        ->whereRaw("tipo_movimientos.nombre <> 'Compra productos'")->get();
        $ultimo_arqueo = DB::table('arqueos')->where('arqueos.cuenta_id', $id_cuenta)->orderBy('arqueos.created_at', 'desc')->get()->first();
        // echo json_encode($ultimo_arqueo);die;
        if($ultimo_arqueo){
            $monto = $ultimo_arqueo->monto;
        }else{
            $monto = 0;
        }
        $pagos = DB::table('pagos')->where('pagos.deleted', 0)->where('pagos.medio_id_medio', $id_cuenta)->whereRaw("pagos.consolidado <> 'S'");
        if(isset($fields['tipo_movimiento_id'])){
            $tipoMovSelected = $fields['tipo_movimiento_id'];
            $pagos = $pagos->where('pagos.tipo_movimiento_id', $tipoMovSelected);
        }
        if(isset($fields['search'])){
            $search = $fields['search'];
            $pagos = $pagos->whereRaw("(pagos.nombre like '%" . $search . "%' or pagos.descripcion like '%" . $search . "%')");
        }
        $pagos = $pagos->leftJoin('tipo_movimientos', 'tipo_movimientos.id_tipo_movimiento', '=', 'pagos.tipo_movimiento_id')
        ->leftJoin('medios', 'medios.id_medio', '=', 'pagos.medio_id_medio')
        ->leftJoin('users', 'pagos.user_id_user', '=', 'users.id_user')
        ->leftJoin('perfils', 'perfils.id_perfil', '=', 'users.perfil_id')
        ->select('pagos.*', 'tipo_movimientos.tipo', 'tipo_movimientos.nombre as tipo_movimiento')
        ->selectRaw("medios.nombre as cuenta, IF(perfils.nombre = 'Mayorista', users.name, concat(users.name, ' ', users.surname)) as name")
        ->orderBy('pagos.created_at', 'desc')
        ->get();
        $cuenta = DB::table('medios')->where('id_medio', $id_cuenta)->get()->first();

        $total = $monto;

        foreach ($pagos as $row) {

            $total = $row->monto + $total;
        }

  
        
         

        // echo json_encode($pagos);die;
        return view('medios.movimientos', ['tipoMovSelected' => $tipoMovSelected, 'tipo_movimientos' => $tipo_movimientos, 
                    'search' => $search, 'monto' => $monto, 'pagos' => $pagos, 'cuenta' => $cuenta, 'total'=> $total],);
    }

    public function arquear(Request $request){
        $fields = $request->all();
        // echo json_encode($fields);die;
        if($fields['monto_sistema'] <> $fields['monto_arqueo']){
            $tipo_movimiento = TipoMovimiento::whereRaw("tipo = 'Arqueo'")->get()->first();
            $pago = new Pago([
                'user_id_user' => Auth::user()->id_user,
                'medio_id_medio' => $fields['id_cuenta'],
                'monto' => $fields['monto_arqueo'] - $fields['monto_sistema'],
                'descripcion' => 'Arqueo de cuenta por exceso o defecto',
                'fecha' => date('Y-m-d'),
                'estado' => 'ASIGNADO',
                'tipo_movimiento_id' => $tipo_movimiento ? $tipo_movimiento->id_tipo_movimiento : 8
            ]);
            $pago->save();
        }
        $mov_no_consolidados = DB::table('pagos')->where('pagos.medio_id_medio', $fields['id_cuenta'])->where('pagos.consolidado', 'N')->where('pagos.deleted', 0)
        ->leftJoin('tipo_movimientos', 'tipo_movimientos.id_tipo_movimiento', '=', 'pagos.tipo_movimiento_id')
        ->select('pagos.*', 'tipo_movimientos.tipo', 'tipo_movimientos.nombre as tipo_movimiento')->get();
        $ultimo_arqueo = DB::table('arqueos')->where('arqueos.cuenta_id', $fields['id_cuenta'])->orderBy('arqueos.created_at', 'desc')->get()->first();
        $monto = $ultimo_arqueo ? $ultimo_arqueo->monto : 0;
        foreach($mov_no_consolidados as $movimiento){
            // if($movimiento->nombre <> 'Compra productos'){
                $monto = $monto + $movimiento->monto;
            // }else{
                // $monto = $monto + $movimiento->monto;
            // }
            DB::table('pagos')->where('id_pago', $movimiento->id_pago)
            ->update(['consolidado' => 'S']);
        }
        DB::table('arqueos')->insert([
            [
                'cuenta_id' => $fields['id_cuenta'],
                'monto' => $monto,
                'created_at' =>  date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
        ]);
        // redirect
        Session::flash('message', 'El arqueo se realizó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/medios/index');
    }

    public function arqueos($id){
        $arqueos = DB::table('arqueos')->where('arqueos.cuenta_id', $id)
        ->orderBy('arqueos.created_at', 'desc')
        ->paginate(10);
        $cuenta = DB::table('medios')->where('medios.id_medio', $id)->get()->first();
        return view('medios.arqueos', ['arqueos' => $arqueos, 'cuenta' => $cuenta]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('medios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'nombre'=>'required'
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/medios/crear')
                ->withErrors($validator);
                // return view('medios.crear');
        } else {
            $medio = new Medio([
                'nombre' => $request->get('nombre'),
                'descripcion' => $request->get('descripcion')
            ]);
            $medio->save();
            // redirect
            Session::flash('message', 'El medio de pago se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/medios/index');
            // return view('medios.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Medio  $medio
     * @return \Illuminate\Http\Response
     */
    public function show(Medio $medio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Medio  $medio
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $medio = Medio::where('id_medio',$id)->get()->first();
        return view('medios.edit', ['medio' => $medio]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Medio  $medio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $medio = Medio::where('id_medio',$id)->get()->first();
        $messages = [
            'required' => 'El :attribute es requerido'
          ];
        $rules = array(
            'nombre'=>'required'
        );
        $fields = $request->all();
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/medios/editar/'.$id)
                ->withErrors($validator);
                // return view('medios.editar', [$id]);
        } else {
            if(isset($fields['publica'])){
                $fields['publica'] = 1;
            }else{
                $fields['publica'] = 0;
            }
            $medio->update($fields);
            // redirect
            Session::flash('message', 'El medio de pago se actualizó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/medios/index');
            // return view('medios.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Medio  $medio
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $medio = Medio::find($id);
        $medio->deleted = 1;
        $medio->save();

        // redirect
        Session::flash('message', 'El medio de pago se eliminó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/medios/index');
        // return view('medios.index');
    }
}
