<?php

namespace App\Http\Controllers;

use \stdClass;
use App\Producto;
use App\Pedido;
use App\Estado;
use App\Imagen;
use App\Direccion;
use Auth;
use PDF;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ProductosImport;
use App\Exports\ListaPreciosExport;
use Image;

class InsumoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $fields = $request->all();
        $limit = isset($fields['limit']) ? $fields['limit'] : 20;
        $search = '';
        $insumos = DB::table('productos')->where('productos.deleted', 0)->where('productos.tipo', 'Insumo');
        if(isset($fields['search'])){
            $search = $fields['search'];
            $insumos = $insumos->whereRaw("(productos.codigo like '%" . $search . "%' or productos.nombre like '%" . $search . "%')");
            // $insumos = $insumos->where('productos.codigo', 'like', '%' . $search . '%')
            // ->orWhere('productos.nombre', 'like', '%' . $search . '%');
        }
        // ->leftJoin('producto_user', 'producto_user.producto_id_producto', '=', 'productos.id_producto')
        $insumos = $insumos->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('users', 'users.id_user', '=', 'producto_user.user_id_user')
        ->leftJoin('almacen_producto',
        function($join){
            $join->on('almacen_producto.producto_id_producto', '=', 'productos.id_producto')
            // ->on('almacen_producto.almacen_id_almacen', '=', DB::raw(env('ALMACEN_PRINCIPAL', 1)))
            ;
        })
        ->leftJoin(DB::raw("(SELECT AP.producto_id_producto, sum(AP.cantidad) as cantidad
        FROM productos P left join almacen_producto AP ON AP.producto_id_producto = P.id_producto 
        where AP.producto_id_producto = P.id_producto
        group by P.id_producto) as STOCK_ALMACENES"), 'productos.id_producto', '=', 'STOCK_ALMACENES.producto_id_producto')
        ->leftJoin(DB::raw("(SELECT M.almacen_id_destino, M.cantidad FROM almacen_producto AP
        left join movimiento_stock M on AP.id_stock = M.almacen_id_destino
        join productos P on P.id_producto = AP.producto_id_producto
        where M.tipo_movimiento <> 'Ajuste' and cast(M.created_at as date) = cast(NOW() as date) 
        and (P.tipo <> 'Insumo' or M.tipo_movimiento <> 'Producción') 
        order by M.created_at desc) as MOV_DEST"), 'almacen_producto.id_stock', '=', 'MOV_DEST.almacen_id_destino')
        ->leftJoin(DB::raw("(SELECT M.almacen_id_origen, if(M.tipo_movimiento = 'Ajuste', M.cantidad,-1*M.cantidad) AS 'cantidad' FROM movimiento_stock M
        join almacen_producto AP on AP.id_stock = M.almacen_id_origen
        join productos P on P.id_producto = AP.producto_id_producto
        where cast(M.created_at as date) = cast(NOW() as date) 
        and (P.tipo <> 'Producto' or M.tipo_movimiento <> 'Producción') 
        order by M.created_at desc) as MOV_ORI"), 'almacen_producto.id_stock', 'MOV_ORI.almacen_id_origen')
        ->leftJoin(DB::raw("(SELECT P.tipo, P.motivo_devolucion, PP.producto_id_producto, PR.almacen_id_almacen, sum(IF(P.tipo = 'Pedido' or P.tipo = 'Devolucion compra', -1*PR.cantidad,PR.cantidad)) as 'cantidad' FROM producto_remito PR
        join pedido_producto PP on PR.producto_id_producto = PP.id_pedido_producto
        join pedidos P on PP.pedido_id_pedido = P.id_pedido
        where cast(PR.created_at as date) = cast(NOW() as date) and ((P.tipo <> 'Devolucion') or (P.tipo = 'Devolucion' and P.motivo_devolucion <> 'defecto'))
        group by P.tipo, PR.almacen_id_almacen, PP.producto_id_producto
        order by PR.created_at desc) as ITM_REM"), 
            function($join){
                $join->on('almacen_producto.almacen_id_almacen', '=', 'ITM_REM.almacen_id_almacen')
                ->on('almacen_producto.producto_id_producto', '=', 'ITM_REM.producto_id_producto');
            })
        // ->leftJoin(DB::raw("(SELECT `almacens`.`nombre`, `almacen_producto`.`producto_id_producto`, `almacen_producto`.`cantidad` FROM `almacen_producto`
        // join `almacens` on `almacens`.`id_almacen` = `almacen_producto`.`almacen_id_almacen` and `almacens`.`nombre` <> 'Mercado Libre') as TEMP "),function($join){
        //    $join->on('productos.id_producto', '=', 'TEMP.producto_id_producto');
        // })
        ->select('productos.*', 'producto_user.costo_dolares as costo', 'users.valor_dolar')
        // ->selectRaw('ifnull(sum(TEMP.cantidad),0) as stock')
        ->selectRaw("ifnull(STOCK_ALMACENES.cantidad,0.00) as cantidad, sum(ifnull(MOV_DEST.cantidad,0.00)) as mov_destino, sum(ifnull(MOV_ORI.cantidad,0.00)) as mov_origen, 
        sum(if(ITM_REM.tipo = 'Pedido', ITM_REM.cantidad,0.00)) as venta, sum(if(ITM_REM.tipo = 'Devolucion', ITM_REM.cantidad,0.00)) as devolucion_venta, 
        sum(if(ITM_REM.tipo = 'Compra', ITM_REM.cantidad,0.00)) as compra, sum(if(ITM_REM.tipo = 'Devolucion compra', ITM_REM.cantidad,0.00)) as devolucion_compra")
        ->groupBy('productos.id_producto')->orderBy('productos.codigo')->paginate($limit);
        return view('insumos.list', ['limit' =>  $limit, 'search' =>  $search, 'insumos' => $insumos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rubros = DB::table('rubros')->where('deleted', 0)->orderBy('nombre')->get();
        $proveedores = DB::table('users')->where('users.deleted', 0)->where('roles.name', '=', 'Proveedor')
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->select('users.*')->get();
        $almacenes = DB::table('almacens')->where('deleted', 0)->get();
        return view('insumos.create', ['proveedores' => $proveedores, 'almacenes' => $almacenes, 'rubros' => $rubros]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'El :attribute es requerido',
            'unique'    => 'El :attribute ingresado ya esta en uso',
          ];
        $rules = array(
            'codigo'=>'required|unique:productos',
            'nombre'=>'required'
        );

        $validator = Validator::make($request->all(), $rules, $messages);


        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/insumos/crear')
                ->withErrors($validator);
            // return view('insumos.crear');
        } else {
            $insumo = new Producto([
                'codigo' => $request->get('codigo'),
                'nombre' => $request->get('nombre'),
                'descripcion' => $request->get('descripcion'),
                'stock_seguridad' => $request->get('stock_seguridad'),
                'conversion' => $request->get('conversion') != '' ? $request->get('conversion') : 1,
                'unidad' => $request->get('unidad'),
                'tipo' => 'Insumo'
            ]);
            $insumo->save();
            DB::table('almacen_producto')->insert([
                [
                    'almacen_id_almacen' => env('ALMACEN_PRINCIPAL', 1),
                    'producto_id_producto' => $insumo->id_producto,
                    'cantidad' => 0
                ]
            ]);
            $insumo->rubros()->sync([$request->get('rubro_id')]);
            // redirect
            Session::flash('message', 'El insumo se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/insumos/ver/'.$insumo->id_producto);
            // return view('insumos.ver', [$insumo->id_producto]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Producto  $insumo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $insumo = DB::table('productos')->where('productos.id_producto', '=', $id)
        ->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('producto_rubro', 'producto_rubro.producto_id_producto', '=', 'productos.id_producto')
        ->leftJoin('rubros', 'rubros.id_rubro', '=', 'producto_rubro.rubro_id_rubro')
        ->leftJoin('users', 'users.id_user', '=', 'producto_user.user_id_user')
        ->select('productos.*', 'users.valor_dolar', 'producto_user.costo_dolares','rubros.nombre as rubro')->get()->first();

        $susalmacenes = DB::table('almacens')->where('almacens.deleted', 0)->where('almacen_producto.producto_id_producto', '=', $id)
        ->leftJoin('almacen_producto', 'almacen_producto.almacen_id_almacen', '=', 'almacens.id_almacen')
        ->leftJoin(DB::raw("(SELECT M.almacen_id_destino, M.cantidad FROM almacen_producto AP
        left join movimiento_stock M on AP.id_stock = M.almacen_id_destino
        join productos P on P.id_producto = AP.producto_id_producto
        where M.tipo_movimiento <> 'Ajuste' and cast(M.created_at as date) = cast(NOW() as date) 
        and (P.tipo <> 'Insumo' or M.tipo_movimiento <> 'Producción') and AP.producto_id_producto = ".$id."
        order by M.created_at desc) as MOV_DEST"), 'almacen_producto.id_stock', '=', 'MOV_DEST.almacen_id_destino')
        ->leftJoin(DB::raw("(SELECT M.almacen_id_origen, if(M.tipo_movimiento = 'Ajuste', M.cantidad,-1*M.cantidad) AS 'cantidad' FROM movimiento_stock M
        join almacen_producto AP on AP.id_stock = M.almacen_id_origen
        join productos P on P.id_producto = AP.producto_id_producto
        where cast(M.created_at as date) = cast(NOW() as date) and AP.producto_id_producto = ".$id." 
        and (P.tipo <> 'Producto' or M.tipo_movimiento <> 'Producción') 
        order by M.created_at desc) as MOV_ORI"), 'almacen_producto.id_stock', 'MOV_ORI.almacen_id_origen')
        ->leftJoin(DB::raw("(SELECT P.tipo, P.motivo_devolucion, PP.producto_id_producto, PR.almacen_id_almacen, sum(IF(P.tipo = 'Pedido' or P.tipo = 'Devolucion compra', -1*PR.cantidad,PR.cantidad)) as 'cantidad' FROM producto_remito PR
        join pedido_producto PP on PR.producto_id_producto = PP.id_pedido_producto
        join pedidos P on PP.pedido_id_pedido = P.id_pedido
        where cast(PR.created_at as date) = cast(NOW() as date) and PP.producto_id_producto = ".$id." and ((P.tipo <> 'Devolucion') or (P.tipo = 'Devolucion' and P.motivo_devolucion <> 'defecto'))
        group by P.tipo, PR.almacen_id_almacen, PP.producto_id_producto
        order by PR.created_at desc) as ITM_REM"), 
            function($join){
                $join->on('almacen_producto.almacen_id_almacen', '=', 'ITM_REM.almacen_id_almacen')
                ->on('almacen_producto.producto_id_producto', '=', 'ITM_REM.producto_id_producto');
            })
        ->select('almacens.*','almacen_producto.id_stock')
        ->selectRaw("ifnull(almacen_producto.cantidad,0.00) as stock, sum(ifnull(MOV_DEST.cantidad,0.00)) as mov_destino, sum(ifnull(MOV_ORI.cantidad,0.00)) as mov_origen, 
        sum(if(ITM_REM.tipo = 'Pedido', ITM_REM.cantidad,0.00)) as venta, sum(if(ITM_REM.tipo = 'Devolucion', ITM_REM.cantidad,0.00)) as devolucion_venta, 
        sum(if(ITM_REM.tipo = 'Compra', ITM_REM.cantidad,0.00)) as compra, sum(if(ITM_REM.tipo = 'Devolucion compra', ITM_REM.cantidad,0.00)) as devolucion_compra")
        ->groupBy('almacens.id_almacen')
        ->get();
        // echo json_encode($susalmacenes);
        // die;

        $susproveedores = DB::table('users')->where('users.deleted', 0)->where('roles.name', '=', 'Proveedor')->where('producto_user.producto_id_producto', '=', $id)
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->leftJoin('producto_user', 'producto_user.user_id_user', '=', 'users.id_user')
        ->select('users.*','producto_user.id_costo','producto_user.costo_dolares as costo','producto_user.seleccionado', 'producto_user.codigo_proveedor')->get();

        $almacenes = DB::table('almacens')->where('deleted', 0)->get();
        $proveedores = DB::table('users')->where('users.deleted', 0)->where('roles.name', '=', 'Proveedor')
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->select('users.*')->groupBy('users.id_user')->get();
        return view('insumos.show', ['insumo' => $insumo, 'susalmacenes' => $susalmacenes, 'susproveedores' => $susproveedores, 'almacenes' => $almacenes, 'proveedores' => $proveedores]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Producto  $insumo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rubros = DB::table('rubros')->where('deleted', 0)->orderBy('nombre')->get();
        $insumo = DB::table('productos')->where('productos.id_producto', '=', $id)
        ->leftJoin('producto_rubro', 'producto_rubro.producto_id_producto', '=', 'productos.id_producto')
        ->leftJoin('rubros', 'rubros.id_rubro', '=', 'producto_rubro.rubro_id_rubro')
        ->select('productos.*','rubros.id_rubro as rubro')->get()->first();
        return view('insumos.edit', ['insumo' => $insumo, 'rubros' => $rubros]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Producto  $insumo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $insumo = Producto::where('id_producto',$id)->get()->first();
        $messages = [
            'required' => 'El :attribute es requerido',
            'unique'    => 'El :attribute ingresado ya esta en uso',
          ];
        $rules = array(
            'nombre'=>'required',
        );
        $fields = $request->all();
        if($fields['codigo'] != $insumo->codigo) {
            $rules['codigo'] = 'required|unique:productos';
        }
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/insumos/editar/'.$id)
                ->withErrors($validator);
            // return view('insumos.editar', [$id]);
        } else {
            if($fields['conversion'] == ''){
                $fields['conversion'] = 1;
            }
            $insumo->update($fields);
            $insumo->rubros()->sync([$fields['rubro_id']]);
            // redirect
            Session::flash('message', 'El insumo se actualizó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/insumos/ver/'.$id);
            // return view('insumos.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Producto  $insumo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $insumo = Producto::find($id);
        $insumo->deleted = 1;
        $insumo->save();

        // redirect
        Session::flash('message', 'El insumo se eliminó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/insumos/index');
        // return view('insumos.index');
    }

    public function habilitarDeshabilitar($id_insumo, $estado)
    {
        if($estado){
            $string = "deshabilitó";
        }else{
            $string = "habilitó";
        }
        DB::table('productos')->where('id_producto', $id_insumo)->update(['estado' => !$estado]);
        Session::flash('message', 'El insumo se '.$string.' con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/insumos/index');
        // return view('insumos.ver', [$id_insumo]);
    }

    public function seleccionarproveedor($id_insumo, $id_proveedor)
    {
        DB::table('producto_user')->where('producto_id_producto', $id_insumo)->update(['seleccionado' => 0]);
        DB::table('producto_user')->where('producto_id_producto', $id_insumo)->where('user_id_user', $id_proveedor)->update(['seleccionado' => 1]);
        Session::flash('message', 'El proveedor se seleccionó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/insumos/ver/'.$id_insumo);
        // return view('insumos.ver', [$id_insumo]);
    }

    public function agregarproveedor(Request $request, $id)
    {
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $fields = $request->all();
        $existeProovedor = DB::table('producto_user')->where('producto_id_producto', $id)->where('user_id_user', $fields['proveedor_id'])->get()->first();
        if(empty($existeProovedor)){
            $existeProovedorSeleccionado = DB::table('producto_user')->where('producto_id_producto', $id)->where('seleccionado', 1)->get()->first();
            if(empty($existeProovedorSeleccionado)){
                $seleccionado = 1;
            }else{
                $seleccionado = 0;
            }
            DB::table('producto_user')->insert([
                [
                    'user_id_user' => $fields['proveedor_id'],
                    'producto_id_producto' => $id,
                    'costo_dolares' => $fields['costo_dolares'],
                    'codigo_proveedor' => $fields['codigo_proveedor'],
                    'seleccionado' => $seleccionado,
                    'created_at' =>  date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]
            ]);
            Session::flash('message', 'El proveedor se agregó con éxito!');
            Session::flash('type', 'success');
        }else{
            Session::flash('message', 'El proveedor ya se encuentra asociado al insumo.');
            Session::flash('type', 'danger');
        }
        return Redirect::to('admin/insumos/ver/'.$id);
        // return view('insumos.ver', [$id]);
    }

    public function agregarmovimiento(Request $request, $id)
    {
        $fields = $request->all();
        // echo json_encode($fields);
        // die;
        $messages = [
            'required' => 'El :attribute es requerido',
            'unique'    => 'El :attribute ingresado ya esta en uso',
          ];
        $rules = array(
            'almacen_id_origen'=>'required',
            'almacen_id_destino'=>'required',
            'cantidad'=>'required',
            'conversion'=>'required'
        );
        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/insumos/ver/'.$id)
                ->withErrors($validator);
        } else {
            if($fields['tipo_movimiento'] == 'Ajuste'){
                DB::table('movimiento_stock')->insert([
                    [
                        'tipo_movimiento' => 'Ajuste',
                        'almacen_id_origen' => $fields['almacen_id_origen'],
                        'almacen_id_destino' => $fields['almacen_id_origen'],
                        'descripcion' => $fields['descripcion'],
                        'cantidad' => $fields['cantidad'] / $fields['conversion'],
                        'user_id_user' => Auth::user()->id_user,
                        'created_at' =>  date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]
                ]);
            }else if($fields['tipo_movimiento'] == 'Transferencia'){
                DB::table('movimiento_stock')->insert([
                    [
                        'tipo_movimiento' => 'Transferencia',
                        'almacen_id_origen' => $fields['almacen_id_origen'],
                        'almacen_id_destino' => $fields['almacen_id_destino'],
                        'descripcion' => $fields['descripcion'],
                        'cantidad' => $fields['cantidad'] / $fields['conversion'],
                        'user_id_user' => Auth::user()->id_user,
                        'created_at' =>  date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]
                ]);
            }
            Session::flash('message', 'El movimiento se agregó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/insumos/ver/'.$id);
        }
    }

    public function eliminarproveedor(Request $request, $id_insumo, $id)
    {
        DB::table('producto_user')->where('id_costo', $id)->delete();
        Session::flash('message', 'El proveedor se eliminó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/insumos/ver/'.$id_insumo);
        // return view('insumos.ver', [$id]);
    }

    public function editarstock(Request $request, $id){
        $fields = $request->all();
        DB::table('almacen_producto')
        ->where('id_stock', $fields['id_stock_modal'])
        ->update(['cantidad' => $fields['stock_modal']]);
        Session::flash('message', 'El stock se actualizó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/insumos/ver/'.$id);
    }
    
    public function editarcosto(Request $request, $id){
        $fields = $request->all();
        DB::table('producto_user')
        ->where('id_costo', $fields['id_costo_modal'])
        ->update(['costo_dolares' => $fields['costo_modal'],
        'codigo_proveedor' => $fields['codigo_proveedor_modal']]);
        Session::flash('message', 'El costo se actualizó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/insumos/ver/'.$id);
    }

    public function agregaralmacen(Request $request, $id)
    {
        $fields = $request->all();
        $existeAlmacen = DB::table('almacen_producto')->where('producto_id_producto', $id)->where('almacen_id_almacen', $fields['almacen_id'])->get()->first();
        if(empty($existeAlmacen)){
            DB::table('almacen_producto')->insert([
                [
                    'almacen_id_almacen' => $fields['almacen_id'],
                    'producto_id_producto' => $id,
                    'cantidad' => $fields['stock']
                ]
            ]);
            Session::flash('message', 'El almacen se agregó con éxito!');
            Session::flash('type', 'success');
        }else{
            Session::flash('message', 'El almacen ya se encuentra asociado al insumo.');
            Session::flash('type', 'danger');
        }
        return Redirect::to('admin/insumos/ver/'.$id);
        // return view('insumos.ver', [$id]);
    }

    public function eliminaralmacen(Request $request, $id_insumo, $id)
    {
        DB::table('almacen_producto')->where('id_stock', $id)->delete();
        Session::flash('message', 'El almacen se eliminó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/insumos/ver/'.$id_insumo);
        // return view('insumos.ver', [$id]);
    }
}
