<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Password;
use Laravel\Passport\Client;
use App\Mail\ResetPassword;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use \Illuminate\Auth\Passwords\PasswordResetServiceProvider;

class AuthController extends Controller
{
    private $client;

    public function __construct()
    {
        $this->client = Client::where('password_client', true)->first();//Client::find(1);
        if(!$this->client){
            die('Passport is not installed properly');
        }
    }

    /**
     * [login description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function loginAdmin(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 422);
        }

        $data = [
            'grant_type'    => 'password',
            'client_id'     => $this->client->id,
            'client_secret' => $this->client->secret,
            'username'      => $request->get('email'),
            'password'      => $request->get('password'),
        ];

        $response = $this->handleOauthResponse(
            app()->handle(Request::create('/oauth/token', 'POST', $data))
        );

        if($response) {
            $user = new User;
            $dataUser = $user::where('email', $request->get('email'))->get()->first();
            $roles = $dataUser->roles()->where('name','admin')->get()->toArray();
            if(empty($roles)) {
                return response()->json(['error'=> ['message' => 'Los datos de acceso son incorrectos.']], 403);
            }
            $dataUser->images;
            $dataUser->profile;
            if(!$dataUser->status) {
                return response()->json(['error'=> ['message' => 'Su cuenta no se encuentra activa.']], 403);
            }
            date_default_timezone_set('America/Argentina/Buenos_Aires');
            $user = User::where('email',$request->get('email'))->get()->first();
            $hoy = date("Y-m-d H:i:s");
            $fields['last_login_at'] = $hoy;
            $user->update($fields);
        }
        

        return response()->json(
            $response ? [
                'access_token'  => $response->access_token,
                'refresh_token' => $response->refresh_token,
                'user'          => $dataUser->toArray()
            ] : ['error' => ['message' => 'Los datos de acceso son incorrectos.']]
            ,
            $response ? 200 : 401
        );
    }


    /**
     * [login description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string'
        ],config('messages'));

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 422);
        }

        $data = [
            'grant_type'    => 'password',
            'client_id'     => $this->client->id,
            'client_secret' => $this->client->secret,
            'username'      => $request->get('email'),
            'password'      => $request->get('password'),
        ];

        $response = $this->handleOauthResponse(
            app()->handle(Request::create('/oauth/token', 'POST', $data))
        );

        if($response) {
            $user = new User;
            $dataUser = $user::where('email', $request->get('email'))->get()->first();

            $dataUser->images;
            $dataUser->profile;
            if(!$dataUser->status) {
                return response()->json(['error'=> ['message' => 'Su cuenta no se encuentra activa.']], 403);
            }
        }


        return response()->json(
            $response ? [
                'access_token'  => $response->access_token,
                'refresh_token' => $response->refresh_token,
                'user'          => $dataUser->toArray()
            ] : ['error' => ['message' => 'Los datos de acceso son incorrectos.']]
            ,
            $response ? 200 : 401
        );
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request) {
        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $request->user()->token()->id)
            ->update([
                'revoked' => true,
            ]);
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Get new access token from refresh token
     */
    public function refreshToken(Request $request) {
        $validator = Validator::make($request->all(), [
            'refresh_token' => 'required',
        ],config('messages'));

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 422);
        }
        $data = [
            'grant_type'    => 'refresh_token',
            'client_id'     => $this->client->id,
            'client_secret' => $this->client->secret,
            'refresh_token' => $request->get('refresh_token'),
        ];

        $request = Request::create('/oauth/token', 'POST', $data);

        return app()->handle($request);
    }

    /**
     * Handle the OAuth2 Response
     */
    private function handleOauthResponse($response) {
        // Check if the request was successful
        if ($response->getStatusCode() != 200) {
            return null;
        }

        // Get the data from the response
        return json_decode($response->getContent());
    }


    /**
     * [getResetToken description]
     * @param Request $request [description]
     */
    public function getResetToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 422);
        }

        $user = User::where('email', $request->input('email'))->first();

        if (!$user) {
            return response()->json(['error' => ['message'=>'El usuario ingresado no existe.']], 404);
        }

        $token   = Password::broker()->createToken($user);
        $code    = substr(sha1(date("Y-m-d H:i:s")), 0, 12);
        $title   = 'Reset password';
        $content = 'Your code is:'.$code;

        //$emailDefault = config('mail.emailDefault');
        $emailDefault = $request->input('email');

        Mail::send(
            new ResetPassword(
                $emailDefault,
                $code,
                $user->name
            )
        );

        return response()->json(['result'=>$token], 200);
    }
    /**
     * [newPassword description]
     * @return [type] [description]
     */
    public function checkCode(Request $request) {
        $validator = Validator::make($request->all(), [
            'code' => 'required',
        ],config('messages'));

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 422);
        }

        $resets = DB::table("password_resets")->get();
        if(!empty($resets)) {
            foreach($resets as $reset) {
                $code = substr(sha1($reset->created_at), 0, 12);
                if($request->code == $code) {
                    return response()->json(['email'=>$reset->email], 200);
                }
            }
            return response()->json(['error'=>['message'=>'El codigo es invalido']], 422);
        }
        return response()->json(['error'=>['message'=>'El codigo es invalido']], 422);
    }

    /**
     * [newPassword description]
     * @return [type] [description]
     */
    public function newPassword(Request $request) {
        $validator = Validator::make($request->all(), [
            'code' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
        ],config('messages'));

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 422);
        }

        $reset = DB::table("password_resets")->where('email', $request->email)->first();
        if(!empty($reset)) {
            $code = substr(sha1($reset->created_at), 0, 12);

            if($request->code == $code) {
                $user = User::where('email', $request->input('email'))->first();
                $user->password = app('hash')->make($request->password);
                $user->save();
                DB::table("password_resets")->where('email', $request->email)->delete();
                return response()->json(['message'=>'Contraseña cambiada exitosamente.'], 200);
            }
        }
        return response()->json(['error'=>['message'=>'El codigo es invalido.']], 422);
    }

    public function getProfile() {
        $user = Auth::user();
        $user->images;
        $user->profile;
        return $user;
    }
}
