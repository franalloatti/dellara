<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Remito;
use App\Empresa;
use App\Factura;
use App\ItemFactura;
use App\Pedido;
use App\Producto;
use App\User;
use App\Direccion;
use Illuminate\Support\Facades\DB;
use Afip;
use QrCode;
use PDF;

class FacturaController extends Controller
{
    //Función encargada de levantar la vista con los listados de facturas
    public function index()
    {
        return view('facturas.list');
    }
    //Función consumida por la vista facturas.list mediante ajax. Devuelve todos los valores necesarios para completar la tabla
    public function getFacturas()
    {
        $tabla = DB::select('SELECT f.CbteFch as fecha ,CONCAT(u.surname," ",u.name) as cliente, f.CbteDesde as numero, f.ImpTotal as monto, f.id_factura as id_factura
        FROM factura as f  
        INNER JOIN remitos as r on f.id_remito = r.id_remito 
        INNER JOIN pedidos as p on r.pedido_id_pedido = p.id_pedido
        INNER JOIN users as u on p.user_id_user = u.id_user
        WHERE f.Facturado = 1');
        
        return datatables()->of($tabla)->toJson();
    }

    //Función que permite determinar el concepto de la factura, si es un producto, servicio o productos y servicios
    public function conceptoFactura($pedido){
        $productos = $pedido->productos;
        $flagProducto = 0;
        $flagServicio = 0;
        $flagProductoServicio = 0;
        $concepto = 1;
        foreach ($productos as $producto) {
            switch ($producto->concpeto) {
                case '1':
                    $flagProducto = 1;
                    break;
                case '2':
                    $flagServicio = 1;
                    break;
                case '3':
                    $flagProductoServicio = 1;
                    break;
            }
        }
        if($flagProductoServicio){
            $concepto = 3;
        }else{ 
            if($flagProducto && $flagServicio){
                $concepto = 3;
            }else{
                if($flagServicio){
                    $concepto = 2;
                }
            }
        }
        return $concepto;

    }
    //Esta función se encarga de completar la tabla factura_item, discriminando el porcentaje de iva y monto de cada uno de los productos
    // Carga los items en la tabla item_factura, para posteriormente en la función validar los agrupa
    public function alicuotas($idFactura,$idPedido){
        //preguntamos si ya no se cargaron los items
        $alicuotaExiste = ItemFactura::where('factura_id',$idFactura)->exists();
        $pedidos = DB::table('pedido_producto')->where('pedido_id_pedido', $idPedido)->get();
        $impNetoGravado = 0;
        $impNetoNoGravado = 0;
        $ImpOpExento = 0;
        $impIva = 0;
        foreach($pedidos as $pedido){
            $baseImp = $pedido->precio_unitario * $pedido->cantidad * (1 - ($pedido->descuento)/100); 
            $ivaProducto = Producto::find($pedido->producto_id_producto);
            switch ($ivaProducto->iva) {
                case 1: //Exento
                    $importe = 0;
                    $ImpOpExento += $baseImp;
                    break;
                case 2: // No gravado
                    $importe = 0;
                    $impNetoNoGravado += $baseImp;
                    break;
                case 3: //0%
                    $importe = $baseImp * 0; 
                    $impNetoGravado += $baseImp;
                    break;
                case 4: //10.5%
                    $importe = $baseImp * 0.105;
                    $impNetoGravado += $baseImp; 
                    break;
                case 5: //21%
                    $importe = $baseImp * 0.21; 
                    $impNetoGravado += $baseImp;
                    break;
                case 6: //27%
                    $importe = $baseImp * 0.27; 
                    $impNetoGravado += $baseImp;
                    break;
                case 8: //5%
                    $importe = $baseImp * 0.05; 
                    $impNetoGravado += $baseImp;
                    break; 
                case 9://2.5%
                    $importe = $baseImp * 0.025; 
                    $impNetoGravado += $baseImp;
                    break;
            } 
            $impIva += $importe; //PREGUNTAR SI ES ASÍ
            //Si no existe solo devuelvo los totales
            if(!$alicuotaExiste){    
                ItemFactura::create([
                    'factura_id' => $idFactura,
                    'Id' => $ivaProducto->iva,
                    'BaseImp' => $baseImp,
                    'Importe' => round($importe,2)
                ]);
            }
        }
        return [$impNetoGravado,$impNetoNoGravado,$ImpOpExento,$impIva];
    }
    //Función que recibe como parámetro el tipo de comprobante y según el mismo relaciona que letra corresponde al comprobante
    public function letraFactura($CbteTipo){
        $letra = 'X';
        if (in_array($CbteTipo, [1 , 2 , 3 , 4 , 5 , 34 , 39 , 60 , 63 , 201 , 202 , 203])){
            $letra = 'A';
        }
        if (in_array($CbteTipo, [6 , 7 , 8 , 9 , 10 , 35 , 40 , 64 , 61 , 206 , 207 , 208])){
            $letra = 'B';
        }
        if (in_array($CbteTipo, [11 , 12 , 13 , 15 , 211 , 212 , 213])){
            $letra = 'C';
        }
        if (in_array($CbteTipo, [51 , 52 , 53 , 54])){
            $letra = 'M';
        }
        return $letra;
    }

    public function mepeoFacturacion($razonSocialEmpresa,$razonSocialCliente,$responsableInscriptoEmpresa){
        $CbteTipo = 0;
        //Si la razón social de la empresa es monotributista o excento
        if($razonSocialEmpresa == 6 || $razonSocialEmpresa == 4){
            if($razonSocialCliente != 8 && $razonSocialCliente != 9){
                $CbteTipo = 11;  //FACTURA C
            }else{
                $CbteTipo = 19;  //FACTURA E
            }
        }elseif($razonSocialEmpresa == 1){ //Si es responsable inscripto
            if($razonSocialCliente == 1 || $razonSocialCliente == 6){
                $CbteTipo = 1; //FACTURA A
                if($responsableInscriptoEmpresa=='M'){
                    $CbteTipo = 51; //FACTURA M
                }
            }elseif($razonSocialCliente == 5 || $razonSocialCliente == 4){
                $CbteTipo = 6; //FACTURA B
            }else{
                $CbteTipo = 19; //FACTURA E
            }
        }
        return $CbteTipo; 
    }

    //Funnción que nos  convierte el string de la razón social de la empresa en un valor entero, según la tabla del  informe.
    public function mepeoCategoriaIVA($categoriaIVA){
        switch ($categoriaIVA) {
            case 'IVA Responsable Inscripto':
                $razonSocialEmpresa = 1;
                break;
            
            case 'IVA Sujeto Exento':
                $razonSocialEmpresa = 4;
                break;
            
            case 'Consumidor Final':
                $razonSocialEmpresa = 5;
                break;
            
            case 'Responsable Monotributo':
                $razonSocialEmpresa = 6;
                break;
            default:
                $razonSocialEmpresa = 1;
                break;
        }
        return $razonSocialEmpresa;
    }
    //Funnción que nos  convierte el entero de la razón social de la empresa en el string.
    public function mepeoCategoriaIVAInverso($categoriaIVA){
        switch ($categoriaIVA) {
            case 1:
                $razonSocialEmpresa = 'IVA Responsable Inscripto';
                break;
            case 2:
                $razonSocialEmpresa = 'IVA Responsable no Inscripto';
                break;
            case 3:
                $razonSocialEmpresa = 'IVA no Responsable';
                break;
            case 4:
                $razonSocialEmpresa = 'IVA Sujeto Exento';
                break;
            
            case 5:
                $razonSocialEmpresa = 'Consumidor Final';
                break;
            
            case 6:
                $razonSocialEmpresa = 'Responsable Monotributo';
                break;

            case 7:
                $razonSocialEmpresa = 'Sujeto no Categorizado';
                break;

            case 8:
                $razonSocialEmpresa = 'Proveedor del Exterior';
                break;

            case 9:
                $razonSocialEmpresa = 'Cliente del Exterior';
                break;
                
            case 10:
                $razonSocialEmpresa = ' IVA Liberado – Ley Nº 19.640';
                break;

            case 11:
                $razonSocialEmpresa = 'IVA Responsable Inscripto – Agente de Percepción';
                break;

            case 12:
                $razonSocialEmpresa = 'Pequeño Contribuyente Eventual';
                break;

            case 13:
                $razonSocialEmpresa = 'Monotributista Social';
                break;

            case 14:
                $razonSocialEmpresa = 'Pequeño Contribuyente Eventual Social';
                break;

            default:
                $razonSocialEmpresa = 'IVA Responsable Inscripto';
                break;
        }
        return $razonSocialEmpresa;
    }
    //Función encargada de adquirir todos los datos necesarios para crear una instancia de factura.
    //Recibe como parametro el id del remito a facturar y devuelve el id de la factura creada 
    public function generar($remito_id,$idDomicilio)
    {
        $empresa = Empresa::find(1);
        $remito = Remito::find($remito_id);
        $facturaExiste = Factura::where('id_remito',$remito_id)->exists();
        $pedido = Pedido::find($remito->pedido_id_pedido);
        $usuario = User::find($pedido->user_id_user); 
        $CbteTipo = $this->mepeoFacturacion($this->mepeoCategoriaIVA($empresa->categoriaIVA),$usuario->responsabilidad,$empresa->tipoFactA); // Tipo de comprobante (ver tipos disponibles), recibe dos enteros
        if(!$facturaExiste){
            switch ($CbteTipo) {
                case 1: //FACTURA A
                    $idFactura = $this->generarFacturaA($empresa, $remito, $pedido, $usuario, $idDomicilio, $CbteTipo);
                    break;
                    
                case 6: //FACTURA B
                    $idFactura = $this->generarFacturaA($empresa, $remito, $pedido, $usuario, $idDomicilio, $CbteTipo);
                    break;
                    
                case 11: //FACTURA C
                    $idFactura = $this->generarFacturaBC($empresa, $remito, $pedido, $usuario, $idDomicilio, $CbteTipo);
                    break;
                
                case 51: //FACTURA M
                    $idFactura = $this->generarFacturaA($empresa, $remito, $pedido, $usuario, $idDomicilio, $CbteTipo);
                    break;
                
                case 19: //FACTURA E
                    # no implementado
                    break;
            }
        }else{
            $facturaExiste = Factura::where('id_remito',$remito_id)->get();
            $idFactura = $facturaExiste->first()->id_factura;
        }
        
        return $idFactura;
    }

    //Función encargada de adquirir todos los datos necesarios para crear una instancia de factura tipo A.
    public function generarFacturaA($empresa, $remito, $pedido, $usuario, $idDomicilio, $CbteTipo)
    {
        //Si no tiene cuit entonces lo tomop cómo consumidor Final?
        if(is_null($usuario->cuit)){ //Nunca debería entrar acá, porque estamos en factura A y no puede hacerse a un consumidor final
            $docTipo = 99;
            $docNro = 0;
            //TODO: Deberíamos hacer un error, que si entra acá salga con error u algo
        }else{
            $docTipo = 80;
            $docNro = $usuario->cuit;
        }
        
        // $idFactura = Factura::latest('id_factura')->first()->get('id_factura');
        // $idFactura++;
        // var_dump($idFactura);die;
        
        //Comenzamos a completar datos
        $factura = new Factura();
        $factura->save();
        $idFactura = $factura->id_factura;
        //Calculamos alicuotas y ya se crea de paso, los items de la factura
        $alicuotas = $this->alicuotas($idFactura, $pedido->id_pedido);
        $impNetoGravado = $alicuotas[0]; 
        $impNetoNoGravado = $alicuotas[1]; 
        $ImpOpExento = $alicuotas[2]; 
        $impIva = $alicuotas[3]; 
        $factura->id_remito = $remito->id_remito;
        $factura->CantReg = 1; // Cantidad de comprobantes a registrar
        $factura->PtoVta = $empresa->PtoVenta;    // Punto de venta
        $factura->CbteTipo = $CbteTipo; //En este caso debería ser 1 O 6
        // - 01, 02, 03, 04, 05 ,34, 39, 60, 63, 201, 202, 203 para los clase A
        // - 06, 07, 08, 09, 10, 35, 40,64, 61, 206, 207, 208 para los clase B.
        // - 11, 12, 13, 15, 211, 212, 213 para los clase C.
        // - 51, 52, 53, 54 para los clase M.
        // - 49 para los Bienes Usados
        $concepto = $this->conceptoFactura($pedido);
        $factura->Concepto = $concepto;  // Concepto del Comprobante: (1)Productos, (2)Servicios, (3)Productos y Servicios
        switch ($concepto) {
            case 1: //productos
                $factura->CbteFch = intval(date('Ymd')); // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
                break;
            
            default: //Puede ser 2 o 3, es decir, servicios o productos y servicios
                $factura->CbteFch       = intval(date('Ymd')); // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
                $factura->FchServDesde 	= intval(date('Ymd')); // (Opcional) Fecha de inicio del servicio (yyyymmdd), obligatorio para Concepto 2 y 3
                $factura->FchServHasta 	= intval(date('Ymd')); // (Opcional) Fecha de fin del servicio (yyyymmdd), obligatorio para Concepto 2 y 3
                $factura->FchVtoPago 	= intval(date('Ymd')); // (Opcional) Fecha de vencimiento del servicio (yyyymmdd), obligatorio para Concepto 2 y 3
                break;
        }
        $factura->DocTipo = $docTipo; // Tipo de documento del comprador (99 consumidor final, ver tipos disponibles)
        $factura->DocNro = $docNro; // Número de documento del comprador (0 consumidor final)

        $factura->ImpTotal = $impNetoNoGravado + $ImpOpExento + $impNetoGravado + $impIva; // Importe total del comprobante =  Importe neto no gravado + Importe exento + Importe neto gravado 
                                        // + todos los campos de IVA al XX% + Importe de tributos
                                        //(EN ESTE CASO NO SE TIENEN EN CUENTA OTROS IPORTES DE TRIBUTO)
        $factura->ImpTotConc = $impNetoNoGravado; // Importe neto no gravado, Debe ser menor o igual a Importe total y no puede ser menor a cero
        $factura->ImpNeto = $impNetoGravado; // Importe neto gravado
        $factura->ImpOpEx = $ImpOpExento; // Importe exento de IVA
        $factura->ImpIVA = $impIva; //Importe total de IVA (Suma de los importes del array de IVA)
        $factura->ImpTrib = 0; //Importe total de tributos (Suma de los importes del array de tributos)
        $factura->MonId = 'PES';  //Tipo de moneda usada en el comprobante (ver tipos disponibles)('PES' para pesos argentinos) 
        $factura->MonCotiz = 1; // Cotización de la moneda usada (1 para pesos argentinos)  
        $factura->Facturado = 0;
        $factura->id_direccion = $idDomicilio;

        $factura->save();      
      
           
        return $idFactura;
    }
    
    //Función encargada de adquirir todos los datos necesarios para crear una instancia de factura tipo C.
    public function generarFacturaBC($empresa, $remito, $pedido, $usuario, $idDomicilio,$CbteTipo)
    {
        //Si no tiene cuit entonces lo tomop cómo consumidor Final?
        if(is_null($usuario->cuit)){ //Nunca debería entrar acá, porque estamos en factura A y no puede hacerse a un consumidor final
            $docTipo = 99;
            $docNro = 0;
            //TODO: Deberíamos hacer un error, que si entra acá salga con error u algo
        }else{
            $docTipo = 80;
            $docNro = $usuario->cuit;
        }
        
        $factura = new Factura();
        $factura->save();
        $idFactura = $factura->id_factura;

        //Calculamos alicuotas y ya se crea de paso, los items de la factura
        $alicuotas = $this->alicuotas($idFactura, $pedido->id_pedido);
        $impNetoGravado = $alicuotas[0]; 
        $impNetoNoGravado = $alicuotas[1]; 
        $ImpOpExento = $alicuotas[2]; 
        $impIva = $alicuotas[3]; 

        //Comenzamos a completar datos
        $factura->id_remito = $remito->id_remito;
        $factura->CantReg = 1; // Cantidad de comprobantes a registrar
        $factura->PtoVta = $empresa->PtoVenta;   // Punto de venta
        $factura->CbteTipo = $CbteTipo; //En este caso debería ser 11
        // - 01, 02, 03, 04, 05 ,34, 39, 60, 63, 201, 202, 203 para los clase A
        // - 06, 07, 08, 09, 10, 35, 40,64, 61, 206, 207, 208 para los clase B.
        // - 11, 12, 13, 15, 211, 212, 213 para los clase C.
        // - 51, 52, 53, 54 para los clase M.
        // - 49 para los Bienes Usados
        $concepto = $this->conceptoFactura($pedido);
        $factura->Concepto = $concepto;  // Concepto del Comprobante: (1)Productos, (2)Servicios, (3)Productos y Servicios
        switch ($concepto) {
            case 1: //productos
                $factura->CbteFch = intval(date('Ymd')); // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
                break;
            
            default: //Puede ser 2 o 3, es decir, servicios o productos y servicios
                $factura->CbteFch       = intval(date('Ymd')); // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
                $factura->FchServDesde 	= intval(date('Ymd')); // (Opcional) Fecha de inicio del servicio (yyyymmdd), obligatorio para Concepto 2 y 3
                $factura->FchServHasta 	= intval(date('Ymd')); // (Opcional) Fecha de fin del servicio (yyyymmdd), obligatorio para Concepto 2 y 3
                $factura->FchVtoPago 	= intval(date('Ymd')); // (Opcional) Fecha de vencimiento del servicio (yyyymmdd), obligatorio para Concepto 2 y 3
                break;
        }
        $factura->DocTipo = $docTipo; // Tipo de documento del comprador (99 consumidor final, ver tipos disponibles)
        $factura->DocNro = $docNro; // Número de documento del comprador (0 consumidor final)

        $factura->ImpTotal = $impNetoNoGravado + $ImpOpExento + $impNetoGravado + $impIva; // Importe total del comprobante =  Importe neto no gravado + Importe exento + Importe neto gravado 
                                        // + todos los campos de IVA al XX% + Importe de tributos
                                        //(EN ESTE CASO NO SE TIENEN EN CUENTA OTROS IPORTES DE TRIBUTO)
        $factura->ImpTotConc = 0; // Importe neto no gravado, Debe ser menor o igual a Importe total y no puede ser menor a cero  IVA CERO EN ESTE CASO
        $factura->ImpNeto = $impNetoGravado+$impIva; // Importe neto gravado, $remito->monto; // Importe del subtotal
        $factura->ImpOpEx = 0; // Importe exento de IVA CERO EN ESTE CASO
        $factura->ImpIVA = 0; //Importe total de IVA (Suma de los importes del array de IVA)  
        $factura->ImpTrib = 0; //Importe total de tributos (Suma de los importes del array de tributos)
        $factura->MonId = 'PES';  //Tipo de moneda usada en el comprobante (ver tipos disponibles)('PES' para pesos argentinos) 
        $factura->MonCotiz = 1; // Cotización de la moneda usada (1 para pesos argentinos)  
        $factura->Facturado = 0;
        $factura->id_direccion = $idDomicilio;

        $factura->save();      
      
        return $idFactura;
    }

    //Función encargada de realizar la conexión y crear la factura con AFIP
    //Recibe cómo parámetro el id de la factura a validar
    public function validar($id)
    {
        $factura = Factura::find($id);
        $empresa = Empresa::find(1);
        $items = ItemFactura::where('factura_id',$factura->id_factura)->whereNotIn('Id',[1,2])->select('Id',DB::raw('TRUNCATE(SUM(BaseImp),2) AS BaseImp, TRUNCATE(SUM(Importe),2) AS Importe' ))->groupBy('Id')->get();
        $Iva = json_decode(json_encode($items), true);
        $afip = new Afip(array('CUIT' => $empresa->cuit, 'production' => $empresa->produccion ? true : false));
        // Obtenemos el último valor del comprobante
        $ultimaFactura = $this->ultimaFactura($empresa->PtoVenta, $factura->CbteTipo);
        $factura->CbteDesde = $ultimaFactura+1;  // Número de comprobante o numero del primer comprobante en caso de ser mas de uno
        $factura->CbteHasta = $ultimaFactura+1; // Número de comprobante o numero del último comprobante en caso de ser mas de uno
        $factura->save();
        //pregunto si la factura es tipo c
        if($factura->CbteTipo==11){
            $data = array(
                'CantReg' 	 => $factura->CantReg,
                'PtoVta' 	 => $factura->PtoVta,
                'CbteTipo' 	 => $factura->CbteTipo,
                'Concepto' 	 => $factura->Concepto,
                'DocTipo' 	 => $factura->DocTipo,
                'DocNro' 	 => $factura->DocNro,
                'CbteDesde'  => $factura->CbteDesde,
                'CbteHasta'  => $factura->CbteHasta,
                'CbteFch' 	 => intval(date('Ymd')), 
                'ImpTotal' 	 => $factura->ImpTotal,
                'ImpTotConc' => $factura->ImpTotConc,
                'ImpNeto' 	 => $factura->ImpNeto,
                'ImpOpEx' 	 => $factura->ImpOpEx,
                'ImpIVA' 	 => $factura->ImpIVA,
                'ImpTrib' 	 => $factura->ImpTrib,
                'FchServDesde'=> $factura->FchServDesde,
                'FchServHasta'=> $factura->FchServHasta,
                'FchVtoPago'=> $factura->FchVtoPago,
                'MonId' 	 => $factura->MonId, 
                'MonCotiz' 	 => $factura->MonCotiz,   
            );
        }else{
            $data = array(
                'CantReg' 	 => $factura->CantReg,
                'PtoVta' 	 => $factura->PtoVta,
                'CbteTipo' 	 => $factura->CbteTipo,
                'Concepto' 	 => $factura->Concepto,
                'DocTipo' 	 => $factura->DocTipo,
                'DocNro' 	 => $factura->DocNro,
                'CbteDesde'  => $factura->CbteDesde,
                'CbteHasta'  => $factura->CbteHasta,
                'CbteFch' 	 => intval(date('Ymd')), 
                'ImpTotal' 	 => $factura->ImpTotal,
                'ImpTotConc' => $factura->ImpTotConc,
                'ImpNeto' 	 => $factura->ImpNeto,
                'ImpOpEx' 	 => $factura->ImpOpEx,
                'ImpIVA' 	 => $factura->ImpIVA,
                'ImpTrib' 	 => $factura->ImpTrib,
                'FchServDesde'=> $factura->FchServDesde,
                'FchServHasta'=> $factura->FchServHasta,
                'FchVtoPago'=> $factura->FchVtoPago,
                'MonId' 	 => $factura->MonId, 
                'MonCotiz' 	 => $factura->MonCotiz,  
                'Iva' 		 => $Iva, 
            );
        }
        
        // var_dump($data);
        try {
            $res = $afip->ElectronicBilling->CreateVoucher($data);
        }
        catch (Exception $e)
        {
            $excepcion_capturada = $e->getMessage();
            return $excepcion_capturada;   
        }
        $factura->Facturado = 1;
        $factura->CAE = $res['CAE'];
        $factura->fechaCAE =  $res['CAEFchVto'];
        $factura->save();
    }
    
    //Función que muestra el número de la última factura generada. No se utilzia, solo está para probar la conexión con AFIP
    public function ultimaFactura($ptoVenta, $tipoComp)
    {
        $empresa = Empresa::find(1);
        $afip = new Afip(array('CUIT' => $empresa->cuit, 'production' => $empresa->produccion ? true : false));
        $last_voucher = $afip->ElectronicBilling->GetLastVoucher($ptoVenta,$tipoComp);
        // echo $last_voucher;
        // $valfac = $last_voucher + 1;
        return $last_voucher;
    }
    //Función que recopila y genera los datos para imprimir la factura
    public function imprimir($id)
    {
        $empresa = Empresa::find(1);
        $factura = Factura::find($id);
        $remito = Remito::find($factura->id_remito);
        $pedidos = Pedido::find($remito->pedido_id_pedido);
        $cliente = User::find($pedidos->user_id_user);
        $direccion = Direccion::find($factura->id_direccion);
 
        $letra = $this->letraFactura($factura->CbteTipo);
        

        $url = 'https://www.afip.gob.ar/fe/qr/'; // URL que pide AFIP que se ponga en el QR. 
        $datos_cmp_base_64 = json_encode([ 
            "ver" => 1,                         // Numérico 1 digito -  OBLIGATORIO – versión del formato de los datos del comprobante	1
            "fecha" => $factura->CbteFch,            // full-date (RFC3339) - OBLIGATORIO – Fecha de emisión del comprobante
            "cuit" => (int) $empresa->cuit,        // Numérico 11 dígitos -  OBLIGATORIO – Cuit del Emisor del comprobante  
            "ptoVta" => (int) $empresa->PtoVenta,               // Numérico hasta 5 digitos - OBLIGATORIO – Punto de venta utilizado para emitir el comprobante
            "tipoCmp" => (int) $factura->CbteTipo,               // Numérico hasta 3 dígitos - OBLIGATORIO – tipo de comprobante (según Tablas del sistema. Ver abajo )
            "nroCmp" => (int) $factura->CbteDesde,               // Numérico hasta 8 dígitos - OBLIGATORIO – Número del comprobante
            "importe" => (float) $factura->ImpTotal,         // Decimal hasta 13 enteros y 2 decimales - OBLIGATORIO – Importe Total del comprobante (en la moneda en la que fue emitido)
            "moneda" => $factura->MonId,                  // 3 caracteres - OBLIGATORIO – Moneda del comprobante (según Tablas del sistema. Ver Abajo )
            "ctz" =>   $factura->MonCotiz,                 // Decimal hasta 13 enteros y 6 decimales - OBLIGATORIO – Cotización en pesos argentinos de la moneda utilizada (1 cuando la moneda sea pesos)
            "tipoDocRec" =>  $factura->DocTipo,               // Numérico hasta 2 dígitos - DE CORRESPONDER – Código del Tipo de documento del receptor (según Tablas del sistema )
            "nroDocRec" =>  $factura->DocNro,        // Numérico hasta 20 dígitos - DE CORRESPONDER – Número de documento del receptor correspondiente al tipo de documento indicado
            "tipoCodAut" => "E",                // string - OBLIGATORIO – “A” para comprobante autorizado por CAEA, “E” para comprobante autorizado por CAE
            "codAut" => (int) $factura->CAE    // Numérico 14 dígitos -  OBLIGATORIO – Código de autorización otorgado por AFIP para el comprobante
        ]); 

        $datos_cmp_base_64 = base64_encode($datos_cmp_base_64); 
        $to_qr = $url.'?p='.$datos_cmp_base_64;
        
        $qr = QrCode::size(100)->generate($to_qr);
        $facturaNumero = str_pad($factura->CbteDesde, 8, "0", STR_PAD_LEFT);
        $puntoVenta = str_pad($empresa->PtoVenta, 5, "0", STR_PAD_LEFT);
        
        if($factura->CbteTipo==1 || $factura->CbteTipo==51){ //Factura A, M
            $tabla = DB::select('SELECT p.codigo, p.nombre,
            CASE 
                WHEN p.iva = 1 THEN "Exento"
                WHEN p.iva = 2 THEN "No gravado"
                WHEN p.iva = 3 THEN "IVA 0%"
                WHEN p.iva = 4 THEN "IVA 10.5%"
                WHEN p.iva = 5 THEN "IVA 21%"
                WHEN p.iva = 6 THEN "IVA 27%"
                WHEN p.iva = 8 THEN "IVA 5%"
                WHEN p.iva = 9 THEN "IVA 2.5%"
            END as ivaCategoriaProd, pp.precio_unitario, pp.descuento, 
            pp.cantidad,
            CASE
                WHEN p.iva = 1 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1 AS DECIMAL(12,2)) 
                WHEN p.iva = 2 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1 AS DECIMAL(12,2)) 
                WHEN p.iva = 3 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1 AS DECIMAL(12,2))
                WHEN p.iva = 4 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1.105 AS DECIMAL(12,2))
                WHEN p.iva = 5 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1.21 AS DECIMAL(12,2))
                WHEN p.iva = 6 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1.27 AS DECIMAL(12,2))
                WHEN p.iva = 8 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1.05 AS DECIMAL(12,2))
                WHEN p.iva = 9 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1.025 AS DECIMAL(12,2))
            END  as subTotalIva,
            CAST( pp.precio_unitario * (1-pp.descuento/100) AS DECIMAL(12,2)) * pp.cantidad as subTotal
            FROM remitos as r INNER JOIN producto_remito as pr ON r.id_remito = pr.remito_id_remito 
            INNER JOIN pedido_producto as pp ON pp.id_pedido_producto = pr.producto_id_producto
            INNER JOIN productos as p ON p.id_producto = pp.producto_id_producto
            WHERE r.id_remito =' . $remito->id_remito);
            
            $alicuotas = DB::select('SELECT 
            CASE 
                WHEN Id = 1 THEN "Exento"
                WHEN Id = 2 THEN "No gravado"
                WHEN Id = 3 THEN "IVA 0%"
                WHEN Id = 4 THEN "IVA 10.5%"
                WHEN Id = 5 THEN "IVA 21%"
                WHEN Id = 6 THEN "IVA 27%"
                WHEN Id = 8 THEN "IVA 5%"
                WHEN Id = 9 THEN "IVA 2.5%"
            END as ivaCategoria, TRUNCATE(SUM(BaseImp),2) AS BaseImp, TRUNCATE(SUM(Importe),2) AS Importe
            FROM factura_item as fi 
            WHERE fi.factura_id ='.$id.'
            AND fi.Id NOT IN (1,2) 
            GROUP BY fi.Id');

            $data = [
                'qr' => $qr,
                'empresa' => $empresa,
                'factura' => $factura,
                'facturaNumero' => $facturaNumero,
                'puntoVenta' => $puntoVenta,
                'tabla' => $tabla,
                'factura' => $factura,
                'cliente' => $cliente,
                'responsabilidad' => $this->mepeoCategoriaIVAInverso($cliente->responsabilidad),
                'direccion' => $direccion,
                'letra' => $letra,
                'alicuotas' => $alicuotas
            ];
            $pdf = PDF::loadView('facturas.imprimirA',$data);
            // return view('facturas.imprimirA',$data);
        }else{ //Sino B o C
            $tabla = DB::select('SELECT p.codigo, p.nombre, p.iva, 
            CASE
                WHEN p.iva = 1 THEN CAST( pp.precio_unitario * 1 AS DECIMAL(12,2)) 
                WHEN p.iva = 2 THEN CAST( pp.precio_unitario * 1 AS DECIMAL(12,2)) 
                WHEN p.iva = 3 THEN CAST( pp.precio_unitario * 1 AS DECIMAL(12,2))
                WHEN p.iva = 4 THEN CAST( pp.precio_unitario * 1.105 AS DECIMAL(12,2))
                WHEN p.iva = 5 THEN CAST( pp.precio_unitario * 1.21 AS DECIMAL(12,2))
                WHEN p.iva = 6 THEN CAST( pp.precio_unitario * 1.27 AS DECIMAL(12,2))
                WHEN p.iva = 8 THEN CAST( pp.precio_unitario * 1.05 AS DECIMAL(12,2))
                WHEN p.iva = 9 THEN CAST( pp.precio_unitario * 1.025 AS DECIMAL(12,2))
            END  as precio_unitario, pp.descuento, pp.cantidad,
            CASE
                WHEN p.iva = 1 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1 AS DECIMAL(12,2)) 
                WHEN p.iva = 2 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1 AS DECIMAL(12,2)) 
                WHEN p.iva = 3 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1 AS DECIMAL(12,2))
                WHEN p.iva = 4 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1.105 AS DECIMAL(12,2))
                WHEN p.iva = 5 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1.21 AS DECIMAL(12,2))
                WHEN p.iva = 6 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1.27 AS DECIMAL(12,2))
                WHEN p.iva = 8 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1.05 AS DECIMAL(12,2))
                WHEN p.iva = 9 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1.025 AS DECIMAL(12,2))
            END  as subTotalIva,
            CAST( pp.precio_unitario * (1-pp.descuento/100) AS DECIMAL(12,2)) * pp.cantidad as subTotal
            FROM remitos as r INNER JOIN producto_remito as pr ON r.id_remito = pr.remito_id_remito 
            INNER JOIN pedido_producto as pp ON pp.id_pedido_producto = pr.producto_id_producto
            INNER JOIN productos as p ON p.id_producto = pp.producto_id_producto
            WHERE r.id_remito =' . $remito->id_remito);

            $data = [
                'qr' => $qr,
                'empresa' => $empresa,
                'factura' => $factura,
                'facturaNumero' => $facturaNumero,
                'puntoVenta' => $puntoVenta,
                'tabla' => $tabla,
                'factura' => $factura,
                'cliente' => $cliente,
                'responsabilidad' => $this->mepeoCategoriaIVAInverso($cliente->responsabilidad),
                'direccion' => $direccion,
                'letra' => $letra
            ];
            $pdf = PDF::loadView('facturas.imprimirBC',$data);
        }
        return $pdf->download('factura-'.$factura->CbteDesde.'.pdf');
        // return view('facturas.imprimirA',$data);

    }
    //Función que genera una factura con los mismos datos que que imprime pero con otro formato
    public function ver($id)
    {
        $empresa = Empresa::find(1);
        $factura = Factura::find($id);
        $remito = Remito::find($factura->id_remito);
        $pedidos = Pedido::find($remito->pedido_id_pedido);
        $cliente = User::find($pedidos->user_id_user);
        $direccion = Direccion::find($factura->id_direccion);
        
        $letra = $this->letraFactura($factura->CbteTipo);
        
        
        $url = 'https://www.afip.gob.ar/fe/qr/'; // URL que pide AFIP que se ponga en el QR. 
        $datos_cmp_base_64 = json_encode([ 
            "ver" => 1,                         // Numérico 1 digito -  OBLIGATORIO – versión del formato de los datos del comprobante	1
            "fecha" => $factura->CbteFch,            // full-date (RFC3339) - OBLIGATORIO – Fecha de emisión del comprobante
            "cuit" => (int) $empresa->cuit,        // Numérico 11 dígitos -  OBLIGATORIO – Cuit del Emisor del comprobante  
            "ptoVta" => (int) $empresa->PtoVenta,               // Numérico hasta 5 digitos - OBLIGATORIO – Punto de venta utilizado para emitir el comprobante
            "tipoCmp" => (int) $factura->CbteTipo,               // Numérico hasta 3 dígitos - OBLIGATORIO – tipo de comprobante (según Tablas del sistema. Ver abajo )
            "nroCmp" => (int) $factura->CbteDesde,               // Numérico hasta 8 dígitos - OBLIGATORIO – Número del comprobante
            "importe" => (float) $factura->ImpTotal,         // Decimal hasta 13 enteros y 2 decimales - OBLIGATORIO – Importe Total del comprobante (en la moneda en la que fue emitido)
            "moneda" => $factura->MonId,                  // 3 caracteres - OBLIGATORIO – Moneda del comprobante (según Tablas del sistema. Ver Abajo )
            "ctz" =>   $factura->MonCotiz,                 // Decimal hasta 13 enteros y 6 decimales - OBLIGATORIO – Cotización en pesos argentinos de la moneda utilizada (1 cuando la moneda sea pesos)
            "tipoDocRec" =>  $factura->DocTipo,               // Numérico hasta 2 dígitos - DE CORRESPONDER – Código del Tipo de documento del receptor (según Tablas del sistema )
            "nroDocRec" =>  $factura->DocNro,        // Numérico hasta 20 dígitos - DE CORRESPONDER – Número de documento del receptor correspondiente al tipo de documento indicado
            "tipoCodAut" => "E",                // string - OBLIGATORIO – “A” para comprobante autorizado por CAEA, “E” para comprobante autorizado por CAE
            "codAut" => (int) $factura->CAE    // Numérico 14 dígitos -  OBLIGATORIO – Código de autorización otorgado por AFIP para el comprobante
        ]); 
        
        $datos_cmp_base_64 = base64_encode($datos_cmp_base_64); 
        $to_qr = $url.'?p='.$datos_cmp_base_64;
        
        $qr = QrCode::size(100)->generate($to_qr);
        $facturaNumero = str_pad($factura->CbteDesde, 8, "0", STR_PAD_LEFT);
        $puntoVenta = str_pad($empresa->PtoVenta, 5, "0", STR_PAD_LEFT);
        
        if($factura->CbteTipo!=11){
            $tabla = DB::select('SELECT p.codigo, p.nombre, p.iva, pp.precio_unitario, pp.descuento, 
            pp.cantidad,
            CASE
                WHEN p.iva = 1 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1 AS DECIMAL(12,2)) 
                WHEN p.iva = 2 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1 AS DECIMAL(12,2)) 
                WHEN p.iva = 3 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1 AS DECIMAL(12,2))
                WHEN p.iva = 4 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1.105 AS DECIMAL(12,2))
                WHEN p.iva = 5 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1.21 AS DECIMAL(12,2))
                WHEN p.iva = 6 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1.27 AS DECIMAL(12,2))
                WHEN p.iva = 8 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1.05 AS DECIMAL(12,2))
                WHEN p.iva = 9 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1.025 AS DECIMAL(12,2))
            END  as subTotalIva,
            CAST( pp.precio_unitario * (1-pp.descuento/100) AS DECIMAL(12,2)) * pp.cantidad as subTotal
            FROM remitos as r INNER JOIN producto_remito as pr ON r.id_remito = pr.remito_id_remito 
            INNER JOIN pedido_producto as pp ON pp.id_pedido_producto = pr.producto_id_producto
            INNER JOIN productos as p ON p.id_producto = pp.producto_id_producto
            WHERE r.id_remito =' . $remito->id_remito);
            
            $alicuotas = DB::select('SELECT 
            CASE 
                WHEN Id = 3 THEN "IVA 0%"
                WHEN Id = 4 THEN "IVA 10.5%"
                WHEN Id = 5 THEN "IVA 21%"
                WHEN Id = 6 THEN "IVA 27%"
                WHEN Id = 8 THEN "IVA 5%"
                WHEN Id = 9 THEN "IVA 2.5%"
            END as ivaCategoria, TRUNCATE(SUM(BaseImp),2) AS BaseImp, TRUNCATE(SUM(Importe),2) AS Importe
            FROM factura_item as fi 
            WHERE fi.factura_id = 12
            AND fi.Id NOT IN (1,2) 
            GROUP BY fi.Id');
            
            $data = [
                'qr' => $qr,
                'empresa' => $empresa,
                'factura' => $factura,
                'facturaNumero' => $facturaNumero,
                'puntoVenta' => $puntoVenta,
                'tabla' => $tabla,
                'factura' => $factura,
                'cliente' => $cliente,
                'direccion' => $direccion,
                'letra' => $letra,
                'alicuotas' => $alicuotas
            ];
            return view('facturas.verA',$data);
        }else{
            $tabla = DB::select('SELECT p.codigo, p.nombre, p.iva, 
            CASE
                WHEN p.iva = 1 THEN CAST( pp.precio_unitario * 1 AS DECIMAL(12,2)) 
                WHEN p.iva = 2 THEN CAST( pp.precio_unitario * 1 AS DECIMAL(12,2)) 
                WHEN p.iva = 3 THEN CAST( pp.precio_unitario * 1 AS DECIMAL(12,2))
                WHEN p.iva = 4 THEN CAST( pp.precio_unitario * 1.105 AS DECIMAL(12,2))
                WHEN p.iva = 5 THEN CAST( pp.precio_unitario * 1.21 AS DECIMAL(12,2))
                WHEN p.iva = 6 THEN CAST( pp.precio_unitario * 1.27 AS DECIMAL(12,2))
                WHEN p.iva = 8 THEN CAST( pp.precio_unitario * 1.05 AS DECIMAL(12,2))
                WHEN p.iva = 9 THEN CAST( pp.precio_unitario * 1.025 AS DECIMAL(12,2))
            END  as precio_unitario, pp.descuento, pp.cantidad,
            CASE
                WHEN p.iva = 1 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1 AS DECIMAL(12,2)) 
                WHEN p.iva = 2 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1 AS DECIMAL(12,2)) 
                WHEN p.iva = 3 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1 AS DECIMAL(12,2))
                WHEN p.iva = 4 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1.105 AS DECIMAL(12,2))
                WHEN p.iva = 5 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1.21 AS DECIMAL(12,2))
                WHEN p.iva = 6 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1.27 AS DECIMAL(12,2))
                WHEN p.iva = 8 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1.05 AS DECIMAL(12,2))
                WHEN p.iva = 9 THEN CAST( pp.precio_unitario * (1-pp.descuento/100) * pp.cantidad * 1.025 AS DECIMAL(12,2))
            END  as subTotalIva,
            CAST( pp.precio_unitario * (1-pp.descuento/100) AS DECIMAL(12,2)) * pp.cantidad as subTotal
            FROM remitos as r INNER JOIN producto_remito as pr ON r.id_remito = pr.remito_id_remito 
            INNER JOIN pedido_producto as pp ON pp.id_pedido_producto = pr.producto_id_producto
            INNER JOIN productos as p ON p.id_producto = pp.producto_id_producto
            WHERE r.id_remito =' . $remito->id_remito);

            $data = [
                'qr' => $qr,
                'empresa' => $empresa,
                'factura' => $factura,
                'facturaNumero' => $facturaNumero,
                'puntoVenta' => $puntoVenta,
                'tabla' => $tabla,
                'factura' => $factura,
                'cliente' => $cliente,
                'direccion' => $direccion,
                'letra' => $letra
            ];
            return view('facturas.verBC',$data);

        }        
    }
    //función encargada de traer datos de un conttribuyente a partir de su CUIT
    public function consulta($cuit)
    {
        $empresa = Empresa::find(1);
        $afip = new Afip(array('CUIT' => $empresa->cuit, 'production' =>  $empresa->produccion ? true : false));
        $taxpayer_details = $afip->RegisterScopeThirteen->GetTaxpayerDetails($cuit); //Devuelve los datos del contribuyente correspondiente al identificador 20111111111     
        //1) Me fijo si el array NO está vacío, si es así, operamos
        if(!is_null($taxpayer_details)){
            //Seleccionamos los datos necesarios
            if(array_key_exists('razonSocial',$taxpayer_details)){
                $respuesta = $taxpayer_details->razonSocial;
            }elseif(array_key_exists('apellido',$taxpayer_details) AND array_key_exists('nombre',$taxpayer_details)){
                $respuesta = $taxpayer_details->apellido. " " .$taxpayer_details->nombre;
            }elseif(array_key_exists('apellido',$taxpayer_details) || array_key_exists('nombre',$taxpayer_details)){
                if(array_key_exists('apellido',$taxpayer_details)){
                    $respuesta = $taxpayer_details->apellido;
                }else{
                    $respuesta = $taxpayer_details->nombre;
                }
            }
        }elseif(is_null($taxpayer_details)){ // si es NULL enotnces, está vació y AFIP anda
            // ERROR DOCUMENTO
            $respuesta = 2;
        }else{ //Si no se cumplen esas opciones AFIP, está caído...
            // ERROR AFIP
            $respuesta = 3;
        }

        return $respuesta;

    }
    //devuelve el estado de conexión con el webservice de afip
    public function estado()
    {
        $empresa = Empresa::find(1);
        $afip = new Afip(array('CUIT' => $empresa->cuit, 'production' => $empresa->produccion ? true : false));
        $server_status = $afip->ElectronicBilling->GetServerStatus();
        echo 'Este es el estado del servidor:';
        echo '<pre>';
        print_r($server_status);
        echo '</pre>';
    }
    public function domicilioFiscal($idUsuario)
    {
        $direcciones = Direccion::where('user_id',$idUsuario)->where('es_direccion_fact',1)->select('id_direccion','direccion','codigo_postal','localidad','provincia')->get();
        return json_encode($direcciones);
    }
    
    //devuelve informacion de un comprobante
    public function infoComprobante($nroComp, $ptoVenta, $tipoComp)
    {
        $empresa = Empresa::find(1);
        $afip = new Afip(array('CUIT' => $empresa->cuit, 'production' => $empresa->produccion ? true : false));
        $voucher_info = $afip->ElectronicBilling->GetVoucherInfo($nroComp,$ptoVenta,$tipoComp); //Devuelve la información del comprobante 1 para el punto de venta 1 y el tipo de comprobante 6 (Factura B)

        if($voucher_info === NULL){
            echo 'El comprobante no existe';
        }
        else{
            echo 'Esta es la información del comprobante:';
            echo '<pre>';
            print_r($voucher_info);
            echo '</pre>';
        }
    }
    //Listado de alicuotas de AFIP
    public function alicuotasGet(){
        $empresa = Empresa::find(1);
        $afip = new Afip(array('CUIT' => $empresa->cuit, 'production' => $empresa->produccion ? true : false));
        $aloquot_types = $afip->ElectronicBilling->GetAliquotTypes();
        echo '<pre>';
        print_r($aloquot_types);
        echo '</pre>';
    }
    
    //Puntos de ventas disponibles
    public function ptoVtaGet(){
        $empresa = Empresa::find(1);
        $afip = new Afip(array('CUIT' => $empresa->cuit, 'production' => $empresa->produccion ? true : false));
        $aloquot_types = $afip->ElectronicBilling->GetSalesPoints();
        echo '<pre>';
        print_r($aloquot_types);
        echo '</pre>';
    }
    
    //Puntos de ventas disponibles
    public function comprobantesGet(){
        $empresa = Empresa::find(1);
        $afip = new Afip(array('CUIT' => $empresa->cuit, 'production' => $empresa->produccion ? true : false));
        $aloquot_types = $afip->ElectronicBilling->GetVoucherTypes();
        echo '<pre>';
        print_r($aloquot_types);
        echo '</pre>';
    }

    //Conceptos disponibles
    public function conceptosGet(){
        $empresa = Empresa::find(1);
        $afip = new Afip(array('CUIT' => $empresa->cuit, 'production' => $empresa->produccion ? true : false));
        $aloquot_types = $afip->ElectronicBilling->GetConceptTypes();
        echo '<pre>';
        print_r($aloquot_types);
        echo '</pre>';
    }
    
    //Documentos disponibles
    public function documentosGet(){
        $empresa = Empresa::find(1);
        $afip = new Afip(array('CUIT' => $empresa->cuit, 'production' => $empresa->produccion ? true : false));
        $aloquot_types = $afip->ElectronicBilling->GetDocumentTypes();
        echo '<pre>';
        print_r($aloquot_types);
        echo '</pre>';
    }

    //Documentos disponibles
    public function opcionesComproGet(){
        $empresa = Empresa::find(1);
        $afip = new Afip(array('CUIT' => $empresa->cuit, 'production' => $empresa->produccion ? true : false));
        $aloquot_types = $afip->ElectronicBilling->GetOptionsTypes();
        echo '<pre>';
        print_r($aloquot_types);
        echo '</pre>';
    }

    //tributos disponibles
    public function tributosGet(){
        $empresa = Empresa::find(1);
        $afip = new Afip(array('CUIT' => $empresa->cuit, 'production' => $empresa->produccion ? true : false));
        $aloquot_types = $afip->ElectronicBilling->GetTaxTypes();
        echo '<pre>';
        print_r($aloquot_types);
        echo '</pre>';
    }
}