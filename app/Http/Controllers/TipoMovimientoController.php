<?php

namespace App\Http\Controllers;

use App\TipoMovimiento;
use App\Pago;
use Auth;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class TipoMovimientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipo_movimientos = DB::table('tipo_movimientos')->where('deleted', 0)->paginate(10);
        // echo json_encode($tipo_movimientos);die;
        return view('tipo_movimientos.list', ['tipo_movimientos' => $tipo_movimientos]);
    }

    public function gastosperiodicos(Request $request)
    {
        $fields = $request->all();
        $search = '';
        $medios = DB::table('medios')->where('deleted', 0)->get();
        $tipo_movimientos = DB::table('tipo_movimientos as T')->where('T.deleted', 0)->where('T.periodo', '<>', 0);
        if(isset($fields['search'])){
            $search = $fields['search'];
            $tipo_movimientos = $tipo_movimientos->where('T.nombre', 'like', '%' . $search . '%')
            ->orWhere('T.observaciones', 'like', '%' . $search . '%');
        }
        $tipo_movimientos = $tipo_movimientos->leftJoin(DB::raw("(select `P`.`tipo_movimiento_id`, MAX(`P`.`fecha`) as ultimo_pago, ifnull(P.monto,0) as monto 
        from `pagos` as `P` group by `P`.`tipo_movimiento_id`) as TEMP") , 'T.id_tipo_movimiento', '=', 'TEMP.tipo_movimiento_id')
        ->whereRaw("T.tipo = 'Gasto'")->select('T.*')->selectRaw('TEMP.ultimo_pago, ifnull(TEMP.monto,0) as monto')
        ->paginate(10);
        // echo json_encode($tipo_movimientos);die;
        return view('tipo_movimientos.gastosperiodicos', ['search' => $search, 'medios' => $medios, 'tipo_movimientos' => $tipo_movimientos]);
    }

    public function pagar(Request $request){

        $fields = $request->all();
        // echo json_encode($fields);die;
        $messages = [
            'required' => 'El :attribute es requerido',
        ];
        $rules = array(
            'monto_gasto'=>'required',
            'id_gasto_pagar'=>'required',
            'medio_id_medio'=>'required',
            'fecha'=>'required',
        );

        $validator = Validator::make($fields, $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/gastos/periodicos')
                ->withErrors($validator);
                // return view('medios.crear');
        } else {
            $pago = new Pago([
                'user_id_user' => Auth::user()->id_user,
                'medio_id_medio' => $fields['medio_id_medio'],
                'monto' => -1*$fields['monto_gasto'],
                'descripcion' => $fields['descripcion'],
                'fecha' => $fields['fecha'],
                'estado' => 'PENDIENTE',
                'tipo_movimiento_id' => $fields['id_gasto_pagar']
            ]);
            $pago->save();
            // redirect
            Session::flash('message', 'El gasto periódico se pagó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/gastos/periodicos');
            // return view('marcas.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tipo_movimientos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'El :attribute es requerido',
          ];
        $rules = array(
            'nombre_add'=>'required'
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/gastos/periodicos')
                ->withErrors($validator);
                // return view('tipo_movimientos.crear');
        } else {
            $tipo_movimiento = new TipoMovimiento([
                'nombre' => $request->get('nombre_add'),
                'observaciones' => $request->get('observaciones_add'),
                'tipo' => 'Gasto',
                'periodo' => 1
            ]);
            $tipo_movimiento->save();
            // redirect
            Session::flash('message', 'El gasto periódico se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/gastos/periodicos');
            // return view('tipo_movimientos.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TipoMovimiento  $tipo_movimiento
     * @return \Illuminate\Http\Response
     */
    public function show(TipoMovimiento $tipo_movimiento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TipoMovimiento  $tipo_movimiento
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipo_movimiento = TipoMovimiento::where('id_tipo_movimiento',$id)->get()->first();
        return view('tipo_movimientos.edit', ['tipo_movimiento' => $tipo_movimiento]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TipoMovimiento  $tipo_movimiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $fields = $request->all();
        $messages = [
            'required' => 'El :attribute es requerido'
          ];
        $rules = array(
            'nombre'=>'required',
            'id_gasto'=>'required'
        );
        $fields = $request->all();
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/gastos/periodicos')
                ->withErrors($validator);
        } else {
            $tipo_movimiento = TipoMovimiento::where('id_tipo_movimiento',$fields['id_gasto'])->get()->first();
            $tipo_movimiento->update($fields);
            // redirect
            Session::flash('message', 'El gasto se actualizó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/gastos/periodicos');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoMovimiento  $tipo_movimiento
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $tipo_movimiento = TipoMovimiento::find($id);
        $tipo_movimiento->deleted = 1;
        $tipo_movimiento->save();

        // redirect
        Session::flash('message', 'El gasto periódico se eliminó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/gastos/periodicos');
        // return view('tipo_movimientos.index');
    }
}
