<?php

namespace App\Http\Controllers;

use \stdClass;
use App\Producto;
use App\Pedido;
use App\Estado;
use App\Imagen;
use App\Direccion;
use Auth;
use PDF;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ProductosImport;
use App\Exports\ListaPreciosExport;
use Image;

class ServicioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $fields = $request->all();
        $limit = isset($fields['limit']) ? $fields['limit'] : 20;
        $search = '';
        $servicios = DB::table('productos')->where('productos.deleted', 0)->where('productos.tipo', 'Servicio');
        if(isset($fields['search'])){
            $search = $fields['search'];
            $servicios = $servicios->whereRaw("(productos.codigo like '%" . $search . "%' or productos.nombre like '%" . $search . "%')");
            // $servicios = $servicios->where('productos.codigo', 'like', '%' . $search . '%')
            // ->orWhere('productos.nombre', 'like', '%' . $search . '%');
        }
        // ->leftJoin('producto_user', 'producto_user.producto_id_producto', '=', 'productos.id_producto')
        $servicios = $servicios->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('users', 'users.id_user', '=', 'producto_user.user_id_user')
        ->select('productos.*', 'producto_user.costo_dolares as costo', 'users.valor_dolar')
        ->groupBy('productos.id_producto')->orderBy('productos.codigo')->paginate($limit);
        return view('servicios.list', ['limit' =>  $limit, 'search' =>  $search, 'servicios' => $servicios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proveedores = DB::table('users')->where('users.deleted', 0)->where('roles.name', '=', 'Proveedor')
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->select('users.*')->get();
        return view('servicios.create', ['proveedores' => $proveedores]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'El :attribute es requerido',
            'unique'    => 'El :attribute ingresado ya esta en uso',
          ];
        $rules = array(
            'codigo'=>'required|unique:productos',
            'nombre'=>'required'
        );

        $validator = Validator::make($request->all(), $rules, $messages);


        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/servicios/crear')
                ->withErrors($validator);
            // return view('servicios.crear');
        } else {
            $id=DB::select("SHOW TABLE STATUS LIKE 'productos'");
            $next_id=$id[0]->Auto_increment;
            $servicio = new Producto([
                'nombre' => $request->get('nombre'),
                'descripcion' => $request->get('descripcion'),
                'tipo' => 'Servicio',
                'concepto' => 2,
                'codigo' => 'SERV-'.$next_id
            ]);
            $servicio->save();
            // redirect
            Session::flash('message', 'El servicio se creó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/servicios/ver/'.$servicio->id_producto);
            // return view('servicios.ver', [$servicio->id_producto]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Producto  $servicio
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $servicio = DB::table('productos')->where('productos.id_producto', '=', $id)
        ->leftJoin('producto_user', function($join){
            $join->on('producto_user.producto_id_producto', '=', 'productos.id_producto')
            ->where('producto_user.seleccionado', '=', 1);
        })
        ->leftJoin('users', 'users.id_user', '=', 'producto_user.user_id_user')
        ->select('productos.*', 'users.valor_dolar', 'producto_user.costo_dolares')->get()->first();

        $susproveedores = DB::table('users')->where('users.deleted', 0)->where('roles.name', '=', 'Proveedor')->where('producto_user.producto_id_producto', '=', $id)
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->leftJoin('producto_user', 'producto_user.user_id_user', '=', 'users.id_user')
        ->select('users.*','producto_user.id_costo','producto_user.costo_dolares as costo','producto_user.seleccionado', 'producto_user.codigo_proveedor')->get();

        $proveedores = DB::table('users')->where('users.deleted', 0)->where('roles.name', '=', 'Proveedor')
        ->leftJoin('role_user', 'role_user.user_id_user', '=', 'users.id_user')
        ->leftJoin('roles', 'roles.id_role', '=', 'role_user.role_id_role')
        ->select('users.*')->groupBy('users.id_user')->get();
        return view('servicios.show', ['servicio' => $servicio, 'susproveedores' => $susproveedores, 'proveedores' => $proveedores]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Producto  $servicio
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $servicio = DB::table('productos')->where('productos.id_producto', '=', $id)
        ->select('productos.*')->get()->first();
        return view('servicios.edit', ['servicio' => $servicio]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Producto  $servicio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $servicio = Producto::where('id_producto',$id)->get()->first();
        $messages = [
            'required' => 'El :attribute es requerido',
            'unique'    => 'El :attribute ingresado ya esta en uso',
          ];
        $rules = array(
            'nombre'=>'required',
        );
        $fields = $request->all();
        if($fields['codigo'] != $servicio->codigo) {
            $rules['codigo'] = 'required|unique:productos';
        }
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Session::flash('message', $validator->errors()->first());
            Session::flash('type', 'danger');
            return Redirect::to('admin/servicios/editar/'.$id)
                ->withErrors($validator);
            // return view('servicios.editar', [$id]);
        } else {
            $servicio->update($fields);
            // redirect
            Session::flash('message', 'El servicio se actualizó con éxito!');
            Session::flash('type', 'success');
            return Redirect::to('admin/servicios/ver/'.$id);
            // return view('servicios.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Producto  $servicio
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $servicio = Producto::find($id);
        $servicio->deleted = 1;
        $servicio->save();

        // redirect
        Session::flash('message', 'El servicio se eliminó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/servicios/index');
        // return view('servicios.index');
    }

    public function habilitarDeshabilitar($id_servicio, $estado)
    {
        if($estado){
            $string = "deshabilitó";
        }else{
            $string = "habilitó";
        }
        DB::table('productos')->where('id_producto', $id_servicio)->update(['estado' => !$estado]);
        Session::flash('message', 'El servicio se '.$string.' con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/servicios/index');
        // return view('servicios.ver', [$id_servicio]);
    }

    public function seleccionarproveedor($id_servicio, $id_proveedor)
    {
        DB::table('producto_user')->where('producto_id_producto', $id_servicio)->update(['seleccionado' => 0]);
        DB::table('producto_user')->where('producto_id_producto', $id_servicio)->where('user_id_user', $id_proveedor)->update(['seleccionado' => 1]);
        Session::flash('message', 'El proveedor se seleccionó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/servicios/ver/'.$id_servicio);
        // return view('servicios.ver', [$id_servicio]);
    }

    public function agregarproveedor(Request $request, $id)
    {
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $fields = $request->all();
        $existeProovedor = DB::table('producto_user')->where('producto_id_producto', $id)->where('user_id_user', $fields['proveedor_id'])->get()->first();
        if(empty($existeProovedor)){
            $existeProovedorSeleccionado = DB::table('producto_user')->where('producto_id_producto', $id)->where('seleccionado', 1)->get()->first();
            if(empty($existeProovedorSeleccionado)){
                $seleccionado = 1;
            }else{
                $seleccionado = 0;
            }
            DB::table('producto_user')->insert([
                [
                    'user_id_user' => $fields['proveedor_id'],
                    'producto_id_producto' => $id,
                    'costo_dolares' => $fields['costo_dolares'],
                    'codigo_proveedor' => $fields['codigo_proveedor'],
                    'seleccionado' => $seleccionado,
                    'created_at' =>  date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]
            ]);
            Session::flash('message', 'El proveedor se agregó con éxito!');
            Session::flash('type', 'success');
        }else{
            Session::flash('message', 'El proveedor ya se encuentra asociado al servicio.');
            Session::flash('type', 'danger');
        }
        return Redirect::to('admin/servicios/ver/'.$id);
        // return view('servicios.ver', [$id]);
    }

    public function eliminarproveedor(Request $request, $id_servicio, $id)
    {
        DB::table('producto_user')->where('id_costo', $id)->delete();
        Session::flash('message', 'El proveedor se eliminó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/servicios/ver/'.$id_servicio);
        // return view('servicios.ver', [$id]);
    }
    
    public function editarcosto(Request $request, $id){
        $fields = $request->all();
        DB::table('producto_user')
        ->where('id_costo', $fields['id_costo_modal'])
        ->update(['costo_dolares' => $fields['costo_modal'],
        'codigo_proveedor' => $fields['codigo_proveedor_modal']]);
        Session::flash('message', 'El costo se actualizó con éxito!');
        Session::flash('type', 'success');
        return Redirect::to('admin/servicios/ver/'.$id);
    }
}
