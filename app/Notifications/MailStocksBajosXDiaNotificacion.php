<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class MailStocksBajosXDiaNotificacion extends Notification
{
    use Queueable;
    private $productos;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($productos)
    {
        $this->productos = $productos;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail = (new MailMessage);

        $mail = $mail->line('Los siguientes productos están con stock por debajo del nivel de seguridad.');
        foreach($this->productos as $producto){
            $mail = $mail->line(new HtmlString('- (Código: '. $producto->codigo. ") " . $producto->nombre . " - Stock: ".
            $producto->stock . " - Stock de seguridad: ". $producto->stock_seguridad . ' - <a href="'.url('/admin/productos/ver/'.$producto->id_producto).'">Ver producto</a>'));
        }
        return $mail->subject('Notificación Stocks Bajos al día: '.date('d/m/Y'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
