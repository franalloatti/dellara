<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class MailNuevosPedidosXDiaNotificacion extends Notification
{
    use Queueable;
    private $pedidos;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($pedidos)
    {
        $this->pedidos = $pedidos;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail = (new MailMessage);

        $mail = $mail->line('En el día '.date('d/m/Y').' se recibieron los siguientes pedidos.');
        foreach($this->pedidos as $pedido) {
            $mail = $mail->line(new HtmlString('- '. $pedido->name . ' - Nro pedido: <a href="'.url('/admin/pedidos/ver/'.$pedido->id_pedido).'">'.$pedido->id_pedido.'</a>'));
        }
        return $mail->subject('Notificación Pedidos del '. date('d/m/Y'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
