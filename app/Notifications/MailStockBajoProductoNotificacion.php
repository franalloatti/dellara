<?php

namespace App\Notifications;

use App\Producto;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class MailStockBajoProductoNotificacion extends Notification
{
    use Queueable;
    private $producto;
    private $stock_actual;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Producto $producto, $stock_actual)
    {
        $this->producto = $producto;
        $this->stock_actual = $stock_actual;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail = (new MailMessage);

        $mail = $mail->line('El siguiente producto se encuentra con stock por debajo del nivel de seguridad.');
        $mail = $mail->line('- (Código: '. $this->producto->codigo. ") " . $this->producto->nombre . " - Stock: ".
        $this->stock_actual . " - Stock de seguridad: ". $this->producto->stock_seguridad);
        $mail = $mail->action('Ver producto', url('/admin/productos/ver/'.$this->producto->id_producto));
        // echo json_encode($mail);die;
        return $mail->subject('Notificación Producto Stock Bajo');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
