<?php

namespace App\Notifications;

use App\Producto;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class StockBajoNotificacion extends Notification
{
    use Queueable;
    private $producto;
    private $stock_actual;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Producto $producto, $stock_actual)
    {
        $this->producto = $producto;
        $this->stock_actual = $stock_actual;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
            return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id_producto' => $this->producto->id_producto,
            'nombre' => $this->producto->nombre,
            'codigo' => $this->producto->codigo,
            'stock_seguridad' => $this->producto->stock_seguridad,
            'stock_actual' => $this->stock_actual,
            'fecha' => date('Y-m-d H:i:s')
        ];
    }
}
