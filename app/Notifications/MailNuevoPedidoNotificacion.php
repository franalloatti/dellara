<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class MailNuevoPedidoNotificacion extends Notification
{
    use Queueable;
    private $cliente;
    private $productos;
    private $id_pedido;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($cliente, $productos, $id_pedido)
    {
        $this->cliente = $cliente;
        $this->productos = $productos;
        $this->id_pedido = $id_pedido;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail = (new MailMessage);

        $mail = $mail->line('Se ha recibido un nuevo pedido de: '.$this->cliente->name.'.')
        ->line('El detalle del pedido se encuentra a continuación:');
        foreach($this->productos as $producto) {
        $mail = $mail->line('- (Código: '. $producto->codigo. ") " . $producto->nombre . " - Cantidad: ". $producto->cantidad);
        }
        $mail = $mail->action('Ver pedido.', url('/admin/pedidos/ver/'.$this->id_pedido));
        // echo json_encode($mail);die;
        return $mail->subject('Notificación Pedido Nuevo');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
