<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    protected $primaryKey = 'id_perfil';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'descripcion', 'deleted',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    public function permisos() {
        return $this
        ->belongsToMany(Permiso::class)
        ->withTimestamps();
    }

    public function users() {
        return $this
        ->belongsToMany(User::class)
        ->withTimestamps();
    }

    public function hasPermiso($permiso, $accion)
    {
        if ($this->permisos()->where('nombre', $permiso)->where($accion, 1)->first()) {
            return true;
        }
        return false;
    }
}
