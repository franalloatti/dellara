<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $primaryKey = 'id_pedido';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id_user', 'direccion_id_direccion', 'tipo', 'motivo_devolucion', 'descripcion', 'deleted', 'ruta_archivo', 
        'usuario_crea_id', 'fecha_estimada_recepcion', 'observaciones_publicas', 'flete', 'nro_factura'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    public function user() {
        return $this
        ->belongsTo(User::class)
        ->withTimestamps();
    }

    public function direccion() {
        return $this
        ->belongsTo(Direccion::class)
        ->withTimestamps();
    }

    public function remitos() {
        return $this
        ->belongsToMany(Remito::class)
        ->withTimestamps();
    }

    public function productos() {
        return $this
        ->belongsToMany(Producto::class)
        ->withTimestamps();
    }

    public function estados() {
        return $this
        ->belongsToMany(Estado::class)
        ->withTimestamps();
    }

    public function parent()
    {
        return $this->belongsToMany(Pedido::class, 'pedido_pedido', 'pedido_id_padre');
    }

    public function children()
    {
        return $this->hasMany(Pedido::class, 'pedido_pedido', 'pedido_id_hijo');
    }
}
