<?php

namespace App\Imports;

use App\Producto;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ProductosImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Producto([
            'codigo'     => $row['codigo'],
            'nombre'    => $row['nombre'], 
            'descripcion' => $row['descripcion'], 
            'stock_verde' => $row['stock_verde'], 
            'stock_verde' => $row['stock_amarillo'], 
            'stock_verde' => $row['stock_seguridad'], 
            'margen' => $row['margen'], 
            'tiene_fijado' => $row['tiene_fijado'],
            'precio_fijado' => $row['precio_fijado']
        ]);
    }
}
