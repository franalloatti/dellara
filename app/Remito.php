<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Remito extends Model
{
    protected $primaryKey = 'id_remito';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pedido_id_pedido', 'monto', 'descuento', 'descripcion', 'deleted', 'monto_sin_iva'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    public function pagos() {
        return $this
        ->belongsToMany(Pago::class)
        ->withTimestamps();
    }

    // public function productos() {
    //     return $this
    //     ->belongsToMany(Producto::class)
    //     ->withTimestamps();
    // }

    public function pedido() {
        return $this
        ->belongsTo(Pedido::class)
        ->withTimestamps();
    }
}
