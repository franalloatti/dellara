<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', ['as' => 'welcome', function () {
//     return view('welcome');
// }]);
Auth::routes(['verify' => true]);

Route::get('/', ['as' => 'productos.catalogo', 'uses' => 'ProductoController@catalogo'])->middleware('permiso:Productos,ver');
Route::get('/catalogo', ['as' => 'productos.catalogo', 'uses' => 'ProductoController@catalogo'])->middleware('permiso:Productos,ver');
Route::post('/catalogo', ['as' => 'productos.catalogo', 'uses' => 'ProductoController@catalogo'])->middleware('permiso:Productos,ver');
Route::get('/descargarcatalogo', ['as' => 'productos.descargarcatalogo', function () { return view('productos.descargarcatalogo');}])->middleware('verified')->middleware('permiso:Productos,ver');
Route::post('/lista_precios', ['as' => 'productos.lista_precios', 'uses' => 'ProductoController@lista_precios'])->middleware('permiso:Productos,ver');
Route::get('/addcarrito', ['as' => 'productos.addcarrito', 'uses' => 'ProductoController@addcarrito'])->middleware('verified')->middleware('permiso:Pedidos,crear');
Route::get('/adddireccion', ['as' => 'productos.adddireccion', 'uses' => 'ProductoController@adddireccion'])->middleware('verified')->middleware('permiso:Direcciones,crear');
Route::get('/cambiarcantidad', ['as' => 'productos.cambiarcantidad', 'uses' => 'ProductoController@cambiarcantidad'])->middleware('verified')->middleware('permiso:Pedidos,editar');
Route::get('/deletecarrito/{id_item_carrito}', ['as' => 'productos.deletecarrito', 'uses' => 'ProductoController@deletecarrito'])->middleware('verified')->middleware('permiso:Pedidos,crear');
Route::get('/carrito', ['as' => 'productos.carrito', 'uses' => 'ProductoController@carrito'])->middleware('verified')->middleware('permiso:Pedidos,ver');
Route::post('/comprar', ['as' => 'productos.comprar', 'uses' => 'ProductoController@comprar'])->middleware('verified')->middleware('permiso:Pedidos,crear');
Route::get('/detalleproducto/{id}', ['as' => 'productos.detalleproducto', 'uses' => 'ProductoController@detalleproducto'])->middleware('permiso:Productos,ver');
Route::get('/getstockalmacen', ['as' => 'productos.getstockalmacen', 'uses' => 'ProductoController@getstockalmacen'])->middleware('permiso:Productos,ver');

Route::get('/mispedidos', ['as' => 'pedidos.mispedidos', 'uses' => 'PedidoController@mispedidos'])->middleware('verified')->middleware('permiso:Pedidos,ver');
Route::post('/mispedidos', ['as' => 'pedidos.mispedidos', 'uses' => 'PedidoController@mispedidos'])->middleware('verified')->middleware('permiso:Pedidos,ver');
Route::get('/mipedido/{id_pedido}', ['as' => 'pedidos.mipedido', 'uses' => 'PedidoController@mipedido'])->middleware('verified')->middleware('permiso:Pedidos,ver');

Route::get('/devoluciones/crear', ['as' => 'devoluciones.crearcliente', 'uses' => 'DevolucionController@createcliente'])->middleware('verified')->middleware('permiso:Devoluciones,crear');
Route::post('/devoluciones/guardar', ['as' => 'devoluciones.guardarcliente', 'uses' => 'DevolucionController@storecliente'])->middleware('verified')->middleware('permiso:Devoluciones,crear');
Route::get('/misdevoluciones', ['as' => 'devoluciones.misdevoluciones', 'uses' => 'DevolucionController@misdevoluciones'])->middleware('verified')->middleware('permiso:Devoluciones,ver');
Route::post('/misdevoluciones', ['as' => 'devoluciones.misdevoluciones', 'uses' => 'DevolucionController@misdevoluciones'])->middleware('verified')->middleware('permiso:Devoluciones,ver');
Route::get('/midevolucion/{id_pedido}', ['as' => 'devoluciones.midevolucion', 'uses' => 'DevolucionController@midevolucion'])->middleware('verified')->middleware('permiso:Devoluciones,ver');

Route::get('/mispagos', ['as' => 'pagos.mispagos', 'uses' => 'PagoController@mispagos'])->middleware('verified')->middleware('permiso:Pagos,ver');
Route::get('/nuevopago', ['as' => 'pagos.nuevopago', 'uses' => 'PagoController@nuevopago'])->middleware('verified')->middleware('permiso:Pagos,crear');
Route::post('/addpago', ['as' => 'pagos.addpago', 'uses' => 'PagoController@addpago'])->middleware('verified')->middleware('permiso:Pagos,crear');

Route::get('/marcarleidas', ['as' => 'notificacion.marcarleidas', function(){ auth()->user()->unreadNotifications->markAsRead(); return redirect()->back();}])->middleware('verified');
Route::get('/marcarleida/{id}', ['as' => 'notificacion.marcarleida', 'uses' => 'UserController@marcarleida']);

Route::get('/cuenta', ['as' => 'clientes.cuenta', 'uses' => 'ClienteController@cuenta'])->middleware('verified')->middleware('permiso:Estados de cuentas,ver');

Route::prefix('client')->group(function () {
    Route::get('/home', 'HomeController@client')->name('client')->middleware('verified');

});

Route::prefix('admin')->group(function () {
    
    Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

    // Roles
    Route::get('/roles/crear', ['as' => 'roles.crear', 'uses' => 'RoleController@create'])->middleware('verified')->middleware('permiso:Tipos de clientes,crear');
    Route::get('/roles/editar/{id}', ['as' => 'roles.editar', 'uses' => 'RoleController@edit'])->middleware('verified')->middleware('permiso:Tipos de clientes,editar');
    Route::post('/roles/actualizar/{id}', ['as' => 'roles.actualizar', 'uses' => 'RoleController@update'])->middleware('verified')->middleware('permiso:Tipos de clientes,editar');
    Route::post('/roles/guardar', ['as' => 'roles.guardar', 'uses' => 'RoleController@store'])->middleware('verified')->middleware('permiso:Tipos de clientes,crear');
    // Route::get('/roles/update/{id}', ['as' => 'roles.update', 'uses' => 'RoleController@update'])->middleware('verified');
    Route::get('/roles/borrar/{id}', ['as' => 'roles.borrar', 'uses' => 'RoleController@destroy'])->middleware('verified')->middleware('permiso:Tipos de clientes,borrar');
    Route::get('/roles/index', ['as' => 'roles.index', 'uses' => 'RoleController@index'])->middleware('verified')->middleware('permiso:Tipos de clientes,ver');
   
    // Clientes
    Route::get('/clientes/crear', ['as' => 'clientes.crear', 'uses' => 'ClienteController@create'])->middleware('verified')->middleware('permiso:Clientes,crear');
    Route::get('/clientes/editar/{id}', ['as' => 'clientes.editar', 'uses' => 'ClienteController@edit'])->middleware('verified')->middleware('permiso:Clientes,editar');
    Route::post('/clientes/actualizar/{id}', ['as' => 'clientes.actualizar', 'uses' => 'ClienteController@update'])->middleware('verified')->middleware('permiso:Clientes,editar');
    Route::get('/clientes/suspender/{id}', ['as' => 'clientes.suspender', 'uses' => 'ClienteController@suspend'])->middleware('verified')->middleware('permiso:Clientes,editar');
    Route::post('/clientes/guardar', ['as' => 'clientes.guardar', 'uses' => 'ClienteController@store'])->middleware('verified')->middleware('permiso:Clientes,crear');
    // Route::get('/clientes/update/{id}', ['as' => 'clientes.update', 'uses' => 'ClienteController@update'])->middleware('verified');
    Route::get('/clientes/borrar/{id}', ['as' => 'clientes.borrar', 'uses' => 'ClienteController@destroy'])->middleware('verified')->middleware('permiso:Clientes,borrar');
    Route::get('/clientes/index', ['as' => 'clientes.index', 'uses' => 'ClienteController@index'])->middleware('verified')->middleware('permiso:Clientes,ver');
    Route::post('/clientes/index', ['as' => 'clientes.index', 'uses' => 'ClienteController@index'])->middleware('verified')->middleware('permiso:Clientes,ver');
    Route::get('/clientes/indexconsumidor', ['as' => 'clientes.indexconsumidor', 'uses' => 'ClienteController@indexconsumidor'])->middleware('verified')->middleware('permiso:Clientes,ver');
    Route::post('/clientes/indexconsumidor', ['as' => 'clientes.indexconsumidor', 'uses' => 'ClienteController@indexconsumidor'])->middleware('verified')->middleware('permiso:Clientes,ver');
    Route::get('/clientes/ver/{id}', ['as' => 'clientes.ver', 'uses' => 'ClienteController@show'])->middleware('verified')->middleware('permiso:Clientes,ver');
    Route::get('/clientes/cuenta/{id}', ['as' => 'clientes.cuenta', 'uses' => 'ClienteController@cuenta'])->middleware('verified')->middleware('permiso:Estados de cuentas,ver');
    Route::get('/clientes/cuentaampliado/{id}', ['as' => 'clientes.cuentaampliado', 'uses' => 'ClienteController@cuentaampliado'])->middleware('verified')->middleware('permiso:Estados de cuentas,ver');
    
    // Proveedores
    Route::get('/proveedores/crear', ['as' => 'proveedores.crear', 'uses' => 'ProveedorController@create'])->middleware('verified')->middleware('permiso:Proveedores,crear');
    Route::get('/proveedores/editar/{id}', ['as' => 'proveedores.editar', 'uses' => 'ProveedorController@edit'])->middleware('verified')->middleware('permiso:Proveedores,editar');
    Route::post('/proveedores/actualizar/{id}', ['as' => 'proveedores.actualizar', 'uses' => 'ProveedorController@update'])->middleware('verified')->middleware('permiso:Proveedores,editar');
    Route::get('/proveedores/suspender/{id}', ['as' => 'proveedores.suspender', 'uses' => 'ProveedorController@suspend'])->middleware('verified')->middleware('permiso:Proveedores,editar');
    Route::post('/proveedores/guardar', ['as' => 'proveedores.guardar', 'uses' => 'ProveedorController@store'])->middleware('verified')->middleware('permiso:Proveedores,crear');
    // Route::get('/proveedores/update/{id}', ['as' => 'proveedores.update', 'uses' => 'ProveedorController@update'])->middleware('verified');
    Route::get('/proveedores/borrar/{id}', ['as' => 'proveedores.borrar', 'uses' => 'ProveedorController@destroy'])->middleware('verified')->middleware('permiso:Proveedores,borrar');
    Route::get('/proveedores/index', ['as' => 'proveedores.index', 'uses' => 'ProveedorController@index'])->middleware('verified')->middleware('permiso:Proveedores,ver');
    Route::post('/proveedores/index', ['as' => 'proveedores.index', 'uses' => 'ProveedorController@index'])->middleware('verified')->middleware('permiso:Proveedores,ver');
    Route::get('/proveedores/ver/{id}', ['as' => 'proveedores.ver', 'uses' => 'ProveedorController@show'])->middleware('verified')->middleware('permiso:Proveedores,ver');
    Route::get('/proveedores/actualizar_dol', ['as' => 'proveedores.actualizar_dol', 'uses' => 'ProveedorController@actualizar_dol'])->middleware('verified')->middleware('permiso:Proveedores,editar');
    Route::post('/proveedores/actualizar_dol', ['as' => 'proveedores.actualizar_dol_success', 'uses' => 'ProveedorController@actualizar_dol_success'])->middleware('verified')->middleware('permiso:Proveedores,editar');
    Route::get('/proveedores/cuenta/{id}', ['as' => 'proveedores.cuenta', 'uses' => 'ProveedorController@cuenta'])->middleware('verified')->middleware('permiso:Estados de cuentas,ver');
    Route::get('/proveedores/cuentaampliado/{id}', ['as' => 'proveedores.cuentaampliado', 'uses' => 'ProveedorController@cuentaampliado'])->middleware('verified')->middleware('permiso:Estados de cuentas,ver');
    Route::get('/proveedores/addpago', ['as' => 'proveedores.addpago', 'uses' => 'ProveedorController@addpago'])->middleware('verified')->middleware('permiso:Pagos,crear');
    Route::post('/proveedores/addpago', ['as' => 'proveedores.addpagosuccess', 'uses' => 'ProveedorController@addpagosuccess'])->middleware('verified')->middleware('permiso:Pagos,crear');
    Route::get('/proveedores/asignarpago/{id}', ['as' => 'proveedores.asignarpago', 'uses' => 'ProveedorController@asignarpago'])->middleware('verified')->middleware('permiso:Pagos,crear');
    Route::post('/proveedores/asignarpago/{id}', ['as' => 'proveedores.asignarpagosuccess', 'uses' => 'ProveedorController@asignarpagosuccess'])->middleware('verified')->middleware('permiso:Pagos,crear');
    Route::get('/proveedores/pagos', ['as' => 'proveedores.pagos', 'uses' => 'ProveedorController@pagos'])->middleware('verified')->middleware('permiso:Pagos,ver');
    Route::get('/proveedores/editarpago/{id}', ['as' => 'proveedores.editarpago', 'uses' => 'ProveedorController@editarpago'])->middleware('verified')->middleware('permiso:Pagos,editar');
    Route::post('/proveedores/actualizarpago/{id}', ['as' => 'proveedores.actualizarpago', 'uses' => 'ProveedorController@actualizarpago'])->middleware('verified')->middleware('permiso:Pagos,editar');
    Route::get('/proveedores/borrarpago/{id}', ['as' => 'proveedores.borrarpago', 'uses' => 'ProveedorController@borrarpago'])->middleware('verified')->middleware('permiso:Pagos,borrar');
    
    // Productos
    Route::get('/productos/crear', ['as' => 'productos.crear', 'uses' => 'ProductoController@create'])->middleware('verified')->middleware('permiso:Productos,crear');
    Route::get('/productos/editar/{id}', ['as' => 'productos.editar', 'uses' => 'ProductoController@edit'])->middleware('verified')->middleware('permiso:Productos,editar');
    Route::post('/productos/actualizar/{id}', ['as' => 'productos.actualizar', 'uses' => 'ProductoController@update'])->middleware('verified')->middleware('permiso:Productos,editar');
    Route::post('/productos/guardar', ['as' => 'productos.guardar', 'uses' => 'ProductoController@store'])->middleware('verified')->middleware('permiso:Productos,crear');
    Route::get('/productos/seleccionarproveedor/{id_producto}/{id_proveedor}/{redirect?}', ['as' => 'productos.seleccionarproveedor', 'uses' => 'ProductoController@seleccionarproveedor'])->middleware('verified')->middleware('permiso:Productos,editar');
    Route::post('/productos/agregarproveedor/{id_producto}', ['as' => 'productos.agregarproveedor', 'uses' => 'ProductoController@agregarproveedor'])->middleware('verified')->middleware('permiso:Productos,editar');
    Route::get('/productos/eliminarproveedor/{id_producto}/{id_proveedor}', ['as' => 'productos.eliminarproveedor', 'uses' => 'ProductoController@eliminarproveedor'])->middleware('verified')->middleware('permiso:Productos,editar');
    Route::post('/productos/agregaralmacen/{id_producto}', ['as' => 'productos.agregaralmacen', 'uses' => 'ProductoController@agregaralmacen'])->middleware('verified')->middleware('permiso:Productos,editar');
    Route::get('/productos/eliminaralmacen/{id_producto}/{id_almacen}', ['as' => 'productos.eliminaralmacen', 'uses' => 'ProductoController@eliminaralmacen'])->middleware('verified')->middleware('permiso:Productos,editar');
    Route::post('/productos/agregarmovimiento/{id_producto}', ['as' => 'productos.agregarmovimiento', 'uses' => 'ProductoController@agregarmovimiento'])->middleware('verified')->middleware('permiso:Productos,editar');
    Route::post('/productos/editarstock/{id_producto}', ['as' => 'productos.editarstock', 'uses' => 'ProductoController@editarstock'])->middleware('verified')->middleware('permiso:Productos,editar');
    Route::post('/productos/editarcosto/{id_producto}', ['as' => 'productos.editarcosto', 'uses' => 'ProductoController@editarcosto'])->middleware('verified')->middleware('permiso:Productos,editar');
    // Route::get('/productos/update/{id}', ['as' => 'productos.update', 'uses' => 'ProductoController@update'])->middleware('verified');
    Route::get('/productos/borrar/{id}', ['as' => 'productos.borrar', 'uses' => 'ProductoController@destroy'])->middleware('verified')->middleware('permiso:Productos,borrar');
    Route::get('/productos/index', ['as' => 'productos.index', 'uses' => 'ProductoController@index'])->middleware('verified')->middleware('permiso:Productos,ver');
    Route::post('/productos/index', ['as' => 'productos.index', 'uses' => 'ProductoController@index'])->middleware('verified')->middleware('permiso:Productos,ver');
    Route::get('/productos/ver/{id}', ['as' => 'productos.ver', 'uses' => 'ProductoController@show'])->middleware('verified')->middleware('permiso:Productos,ver');
    Route::get('/productos/importar', ['as' => 'productos.importar', function () { return view('productos.import');}])->middleware('verified')->middleware('permiso:Productos,crear');
    Route::post('/productos/toimportar', ['as' => 'productos.toimportar', 'uses' => 'ProductoController@toimportar'])->middleware('verified')->middleware('permiso:Productos,crear');
    Route::get('/productos/download', ['as' => 'productos.download', 'uses' => 'ProductoController@getDownload'])->middleware('verified')->middleware('permiso:Productos,ver');
    Route::get('/productos/imagenes/{id_producto}',  ['as' => 'productos.imagenes', 'uses' => 'ProductoController@imagenes'])->middleware('verified')->middleware('permiso:Productos,editar');
    Route::post('/productos/upload_image',  ['as' => 'productos.upload_image', 'uses' => 'ProductoController@upload_image'])->middleware('verified')->middleware('permiso:Productos,editar');
    Route::get('/productos/fetch_image', ['as' => 'productos.fetch_image', 'uses' => 'ProductoController@fetch_image'])->middleware('verified')->middleware('permiso:Productos,editar');
    Route::get('/productos/delete_image', ['as' => 'productos.delete_image', 'uses' => 'ProductoController@delete_image'])->middleware('verified')->middleware('permiso:Productos,editar');
    Route::get('/productos/ppal_image', ['as' => 'productos.ppal_image', 'uses' => 'ProductoController@ppal_image'])->middleware('verified')->middleware('permiso:Productos,editar');
    Route::get('/productos/habilitarDeshabilitar/{id_producto}/{estado}', ['as' => 'productos.habilitarDeshabilitar', 'uses' => 'ProductoController@habilitarDeshabilitar'])->middleware('verified')->middleware('permiso:Productos,editar');
    Route::get('/productos/importarimagenes', ['as' => 'productos.importarimagenes', function () { return view('productos.importarimagenes');}])->middleware('permiso:Productos,editar')->middleware('verified');
    Route::get('/productos/processimages', ['as' => 'productos.processimages', 'uses' => 'ProductoController@processimages'])->middleware('permiso:Productos,editar')->middleware('verified');
    Route::get('/productos/verstock', ['as' => 'productos.verstock', 'uses' => 'ProductoController@verstock'])->middleware('permiso:Productos,ver')->middleware('verified');
    Route::post('/productos/verstock', ['as' => 'productos.verstock', 'uses' => 'ProductoController@verstock'])->middleware('permiso:Productos,ver')->middleware('verified');
    Route::get('/productos/stock/{id_producto}', ['as' => 'productos.stock', 'uses' => 'ProductoController@stock'])->middleware('permiso:Productos,ver')->middleware('verified');
    Route::post('/productos/stock/{id_producto}', ['as' => 'productos.stock', 'uses' => 'ProductoController@stock'])->middleware('permiso:Productos,ver')->middleware('verified');
    Route::get('/productos/vermovimiento/{id_movimiento_stock}', ['as' => 'productos.vermovimiento', 'uses' => 'ProductoController@vermovimiento'])->middleware('permiso:Productos,ver')->middleware('verified');
    Route::get('/productos/precios', ['as' => 'productos.precios', 'uses' => 'ProductoController@precios'])->middleware('permiso:Productos,editar')->middleware('verified');
    Route::post('/productos/precios', ['as' => 'productos.precios', 'uses' => 'ProductoController@precios'])->middleware('permiso:Productos,editar')->middleware('verified');
    Route::get('/productos/editarmargen', ['as' => 'productos.editarmargen', 'uses' => 'ProductoController@editarmargen'])->middleware('permiso:Productos,editar')->middleware('verified');
    Route::get('/productos/receta/{id_producto}', ['as' => 'productos.receta', 'uses' => 'ProductoController@receta'])->middleware('permiso:Productos,editar')->middleware('verified');
    Route::post('/productos/configreceta/{id_producto}', ['as' => 'productos.configreceta', 'uses' => 'ProductoController@configreceta'])->middleware('permiso:Productos,editar')->middleware('verified');
    
    //Planes de producción
    Route::get('/produccion/crear', ['as' => 'produccion.crear', 'uses' => 'ProduccionController@create'])->middleware('verified')->middleware('permiso:Produccion,crear');
    Route::get('/produccion/editar/{id}', ['as' => 'produccion.editar', 'uses' => 'ProduccionController@edit'])->middleware('verified')->middleware('permiso:Produccion,editar');
    Route::post('/produccion/actualizar/{id}', ['as' => 'produccion.actualizar', 'uses' => 'ProduccionController@update'])->middleware('verified')->middleware('permiso:Produccion,editar');
    Route::post('/produccion/guardar', ['as' => 'produccion.guardar', 'uses' => 'ProduccionController@store'])->middleware('verified')->middleware('permiso:Produccion,crear');
    Route::get('/produccion/ver/{id}', ['as' => 'produccion.ver', 'uses' => 'ProduccionController@show'])->middleware('verified')->middleware('permiso:Produccion,ver');
    Route::get('/produccion/index', ['as' => 'produccion.index', 'uses' => 'ProduccionController@index'])->middleware('verified')->middleware('permiso:Produccion,ver');
    Route::post('/produccion/index', ['as' => 'produccion.index', 'uses' => 'ProduccionController@index'])->middleware('verified')->middleware('permiso:Produccion,ver');
    Route::post('/produccion/confirmado/{id_plan}', ['as' => 'produccion.confirmado', 'uses' => 'ProduccionController@confirmado'])->middleware('verified')->middleware('permiso:Produccion,editar');
    Route::get('/produccion/producido/{id_plan}', ['as' => 'produccion.producido', 'uses' => 'ProduccionController@producido'])->middleware('verified')->middleware('permiso:Produccion,editar');
    Route::post('/produccion/finalizado/{id_plan}', ['as' => 'produccion.finalizado', 'uses' => 'ProduccionController@finalizado'])->middleware('verified')->middleware('permiso:Produccion,editar');
    Route::get('/produccion/cancelado/{id_plan}', ['as' => 'produccion.cancelado', 'uses' => 'ProduccionController@cancelado'])->middleware('verified')->middleware('permiso:Produccion,borrar');
    
    // Insumos
    Route::get('/insumos/crear', ['as' => 'insumos.crear', 'uses' => 'InsumoController@create'])->middleware('verified')->middleware('permiso:Insumos,crear');
    Route::get('/insumos/editar/{id}', ['as' => 'insumos.editar', 'uses' => 'InsumoController@edit'])->middleware('verified')->middleware('permiso:Insumos,editar');
    Route::post('/insumos/actualizar/{id}', ['as' => 'insumos.actualizar', 'uses' => 'InsumoController@update'])->middleware('verified')->middleware('permiso:Insumos,editar');
    Route::post('/insumos/guardar', ['as' => 'insumos.guardar', 'uses' => 'InsumoController@store'])->middleware('verified')->middleware('permiso:Insumos,crear');
    Route::get('/insumos/seleccionarproveedor/{id_producto}/{id_proveedor}', ['as' => 'insumos.seleccionarproveedor', 'uses' => 'InsumoController@seleccionarproveedor'])->middleware('verified')->middleware('permiso:Insumos,editar');
    Route::post('/insumos/agregarproveedor/{id_producto}', ['as' => 'insumos.agregarproveedor', 'uses' => 'InsumoController@agregarproveedor'])->middleware('verified')->middleware('permiso:Insumos,editar');
    Route::get('/insumos/eliminarproveedor/{id_producto}/{id_proveedor}', ['as' => 'insumos.eliminarproveedor', 'uses' => 'InsumoController@eliminarproveedor'])->middleware('verified')->middleware('permiso:Insumos,editar');
    Route::post('/insumos/agregaralmacen/{id_producto}', ['as' => 'insumos.agregaralmacen', 'uses' => 'InsumoController@agregaralmacen'])->middleware('verified')->middleware('permiso:Insumos,editar');
    Route::get('/insumos/eliminaralmacen/{id_producto}/{id_almacen}', ['as' => 'insumos.eliminaralmacen', 'uses' => 'InsumoController@eliminaralmacen'])->middleware('verified')->middleware('permiso:Insumos,editar');
    Route::post('/insumos/editarstock/{id_producto}', ['as' => 'insumos.editarstock', 'uses' => 'InsumoController@editarstock'])->middleware('verified')->middleware('permiso:Insumos,editar');
    Route::post('/insumos/editarcosto/{id_producto}', ['as' => 'insumos.editarcosto', 'uses' => 'InsumoController@editarcosto'])->middleware('verified')->middleware('permiso:Insumos,editar');
    Route::get('/insumos/habilitarDeshabilitar/{id_producto}/{estado}', ['as' => 'insumos.habilitarDeshabilitar', 'uses' => 'InsumoController@habilitarDeshabilitar'])->middleware('verified')->middleware('permiso:Insumos,editar');
    // Route::get('/insumos/update/{id}', ['as' => 'insumos.update', 'uses' => 'InsumoController@update'])->middleware('verified');
    Route::get('/insumos/borrar/{id}', ['as' => 'insumos.borrar', 'uses' => 'InsumoController@destroy'])->middleware('verified')->middleware('permiso:Insumos,borrar');
    Route::get('/insumos/index', ['as' => 'insumos.index', 'uses' => 'InsumoController@index'])->middleware('verified')->middleware('permiso:Insumos,ver');
    Route::post('/insumos/index', ['as' => 'insumos.index', 'uses' => 'InsumoController@index'])->middleware('verified')->middleware('permiso:Insumos,ver');
    Route::get('/insumos/ver/{id}', ['as' => 'insumos.ver', 'uses' => 'InsumoController@show'])->middleware('verified')->middleware('permiso:Insumos,ver');
    Route::post('/insumos/agregarmovimiento/{id_producto}', ['as' => 'insumos.agregarmovimiento', 'uses' => 'InsumoController@agregarmovimiento'])->middleware('verified')->middleware('permiso:Insumos,editar');
    
    // Servicios
    Route::get('/servicios/crear', ['as' => 'servicios.crear', 'uses' => 'ServicioController@create'])->middleware('verified')->middleware('permiso:Servicios,crear');
    Route::get('/servicios/editar/{id}', ['as' => 'servicios.editar', 'uses' => 'ServicioController@edit'])->middleware('verified')->middleware('permiso:Servicios,editar');
    Route::post('/servicios/actualizar/{id}', ['as' => 'servicios.actualizar', 'uses' => 'ServicioController@update'])->middleware('verified')->middleware('permiso:Servicios,editar');
    Route::post('/servicios/guardar', ['as' => 'servicios.guardar', 'uses' => 'ServicioController@store'])->middleware('verified')->middleware('permiso:Servicios,crear');
    Route::get('/servicios/seleccionarproveedor/{id_producto}/{id_proveedor}', ['as' => 'servicios.seleccionarproveedor', 'uses' => 'ServicioController@seleccionarproveedor'])->middleware('verified')->middleware('permiso:Servicios,editar');
    Route::post('/servicios/agregarproveedor/{id_producto}', ['as' => 'servicios.agregarproveedor', 'uses' => 'ServicioController@agregarproveedor'])->middleware('verified')->middleware('permiso:Servicios,editar');
    Route::get('/servicios/eliminarproveedor/{id_producto}/{id_proveedor}', ['as' => 'servicios.eliminarproveedor', 'uses' => 'ServicioController@eliminarproveedor'])->middleware('verified')->middleware('permiso:Servicios,editar');
    Route::post('/servicios/editarcosto/{id_producto}', ['as' => 'servicios.editarcosto', 'uses' => 'ServicioController@editarcosto'])->middleware('verified')->middleware('permiso:Servicios,editar');
    Route::get('/servicios/habilitarDeshabilitar/{id_producto}/{estado}', ['as' => 'servicios.habilitarDeshabilitar', 'uses' => 'ServicioController@habilitarDeshabilitar'])->middleware('verified')->middleware('permiso:Servicios,editar');
    // Route::get('/servicios/update/{id}', ['as' => 'servicios.update', 'uses' => 'ServicioController@update'])->middleware('verified');
    Route::get('/servicios/borrar/{id}', ['as' => 'servicios.borrar', 'uses' => 'ServicioController@destroy'])->middleware('verified')->middleware('permiso:Servicios,borrar');
    Route::get('/servicios/index', ['as' => 'servicios.index', 'uses' => 'ServicioController@index'])->middleware('verified')->middleware('permiso:Servicios,ver');
    Route::post('/servicios/index', ['as' => 'servicios.index', 'uses' => 'ServicioController@index'])->middleware('verified')->middleware('permiso:Servicios,ver');
    Route::get('/servicios/ver/{id}', ['as' => 'servicios.ver', 'uses' => 'ServicioController@show'])->middleware('verified')->middleware('permiso:Servicios,ver');
    
    // Rubros
    Route::get('/rubros/crear', ['as' => 'rubros.crear', 'uses' => 'RubroController@create'])->middleware('verified')->middleware('permiso:Rubros,crear');
    Route::get('/rubros/editar/{id}', ['as' => 'rubros.editar', 'uses' => 'RubroController@edit'])->middleware('verified')->middleware('permiso:Rubros,editar');
    Route::post('/rubros/actualizar/{id}', ['as' => 'rubros.actualizar', 'uses' => 'RubroController@update'])->middleware('verified')->middleware('permiso:Rubros,editar');
    Route::post('/rubros/guardar', ['as' => 'rubros.guardar', 'uses' => 'RubroController@store'])->middleware('verified')->middleware('permiso:Rubros,crear');
    // Route::get('/rubros/update/{id}', ['as' => 'rubros.update', 'uses' => 'RubroController@update'])->middleware('verified');
    Route::get('/rubros/borrar/{id}', ['as' => 'rubros.borrar', 'uses' => 'RubroController@destroy'])->middleware('verified')->middleware('permiso:Rubros,borrar');
    Route::get('/rubros/index', ['as' => 'rubros.index', 'uses' => 'RubroController@index'])->middleware('verified')->middleware('permiso:Rubros,ver');
    
    // Marcas
    Route::get('/marcas/crear', ['as' => 'marcas.crear', 'uses' => 'MarcaController@create'])->middleware('verified')->middleware('permiso:Marcas,crear');
    Route::get('/marcas/editar/{id}', ['as' => 'marcas.editar', 'uses' => 'MarcaController@edit'])->middleware('verified')->middleware('permiso:Marcas,editar');
    Route::post('/marcas/actualizar/{id}', ['as' => 'marcas.actualizar', 'uses' => 'MarcaController@update'])->middleware('verified')->middleware('permiso:Marcas,editar');
    Route::post('/marcas/guardar', ['as' => 'marcas.guardar', 'uses' => 'MarcaController@store'])->middleware('verified')->middleware('permiso:Marcas,crear');
    // Route::get('/marcas/update/{id}', ['as' => 'marcas.update', 'uses' => 'MarcaController@update'])->middleware('verified');
    Route::get('/marcas/borrar/{id}', ['as' => 'marcas.borrar', 'uses' => 'MarcaController@destroy'])->middleware('verified')->middleware('permiso:Marcas,borrar');
    Route::get('/marcas/index', ['as' => 'marcas.index', 'uses' => 'MarcaController@index'])->middleware('verified')->middleware('permiso:Marcas,ver');
    
    // Gastos
    Route::get('/gastos/periodicos', ['as' => 'gastos.periodicos', 'uses' => 'TipoMovimientoController@gastosperiodicos'])->middleware('verified')->middleware('permiso:Pagos,crear');
    Route::post('/gastos/periodicos', ['as' => 'gastos.periodicos', 'uses' => 'TipoMovimientoController@gastosperiodicos'])->middleware('verified')->middleware('permiso:Pagos,crear');
    Route::post('/gastos/guardar', ['as' => 'gastos.guardar', 'uses' => 'TipoMovimientoController@store'])->middleware('verified')->middleware('permiso:Pagos,crear');
    Route::post('/gastos/pagar', ['as' => 'gastos.pagar', 'uses' => 'TipoMovimientoController@pagar'])->middleware('verified')->middleware('permiso:Pagos,crear');
    Route::post('/gastos/actualizar', ['as' => 'gastos.actualizar', 'uses' => 'TipoMovimientoController@update'])->middleware('verified')->middleware('permiso:Pagos,editar');
    Route::get('/gastos/borrar/{id}', ['as' => 'gastos.borrar', 'uses' => 'TipoMovimientoController@destroy'])->middleware('verified')->middleware('permiso:Pagos,borrar');

    // Movimientos
    Route::get('/movimientos/crear', ['as' => 'movimientos.crear', 'uses' => 'MovimientoController@create'])->middleware('verified')->middleware('permiso:Pagos,crear');
    Route::get('/movimientos/editar/{id}', ['as' => 'movimientos.editar', 'uses' => 'MovimientoController@edit'])->middleware('verified')->middleware('permiso:Pagos,editar');
    Route::post('/movimientos/actualizar', ['as' => 'movimientos.actualizar', 'uses' => 'MovimientoController@update'])->middleware('verified')->middleware('permiso:Pagos,editar');
    Route::post('/movimientos/guardar', ['as' => 'movimientos.guardar', 'uses' => 'MovimientoController@store'])->middleware('verified')->middleware('permiso:Pagos,crear');
    Route::get('/movimientos/borrar/{id}', ['as' => 'movimientos.borrar', 'uses' => 'MovimientoController@destroy'])->middleware('verified')->middleware('permiso:Pagos,borrar');
    Route::get('/movimientos/index', ['as' => 'movimientos.index', 'uses' => 'MovimientoController@index'])->middleware('verified')->middleware('permiso:Pagos,ver');
    Route::post('/movimientos/index', ['as' => 'movimientos.index', 'uses' => 'MovimientoController@index'])->middleware('verified')->middleware('permiso:Pagos,ver');
    Route::get('/movimientos/reportes', ['as' => 'movimientos.reportes', function () { return view('movimientos.reportes');}])->middleware('verified')->middleware('permiso:Pagos,ver');
    Route::post('/movimientos/reportes', ['as' => 'movimientos.getreporte', 'uses' => 'MovimientoController@getreporte'])->middleware('verified')->middleware('permiso:Pagos,ver');
    
    // Pagos
    Route::get('/pagos/crear', ['as' => 'pagos.crear', 'uses' => 'PagoController@create'])->middleware('verified')->middleware('permiso:Pagos,crear');
    Route::get('/pagos/editar/{id}', ['as' => 'pagos.editar', 'uses' => 'PagoController@edit'])->middleware('verified')->middleware('permiso:Pagos,editar');
    Route::post('/pagos/actualizar/{id}', ['as' => 'pagos.actualizar', 'uses' => 'PagoController@update'])->middleware('verified')->middleware('permiso:Pagos,editar');
    Route::post('/pagos/guardar', ['as' => 'pagos.guardar', 'uses' => 'PagoController@store'])->middleware('verified')->middleware('permiso:Pagos,crear');
    // Route::get('/pagos/update/{id}', ['as' => 'pagos.update', 'uses' => 'PagoController@update'])->middleware('verified');
    Route::get('/pagos/borrar/{id}', ['as' => 'pagos.borrar', 'uses' => 'PagoController@destroy'])->middleware('verified')->middleware('permiso:Pagos,borrar');
    Route::get('/pagos/index', ['as' => 'pagos.index', 'uses' => 'PagoController@index'])->middleware('verified')->middleware('permiso:Pagos,ver');
    Route::post('/pagos/index', ['as' => 'pagos.index', 'uses' => 'PagoController@index'])->middleware('verified')->middleware('permiso:Pagos,ver');
    Route::get('/pagos/asignar/{id}', ['as' => 'pagos.asignar', 'uses' => 'PagoController@asignar'])->middleware('verified')->middleware('permiso:Pagos,crear');
    Route::post('/pagos/asignarsuccess/{id}', ['as' => 'pagos.asignarsuccess', 'uses' => 'PagoController@asignarsuccess'])->middleware('verified')->middleware('permiso:Pagos,crear');

    // Direcciones
    Route::get('/direcciones/crear/{id_user}', ['as' => 'direcciones.crear', 'uses' => 'DireccionController@create'])->middleware('verified')->middleware('permiso:Direcciones,crear');
    Route::get('/direcciones/editar/{id}', ['as' => 'direcciones.editar', 'uses' => 'DireccionController@edit'])->middleware('verified')->middleware('permiso:Direcciones,editar');
    Route::post('/direcciones/actualizar/{id}', ['as' => 'direcciones.actualizar', 'uses' => 'DireccionController@update'])->middleware('verified')->middleware('permiso:Direcciones,editar');
    Route::post('/direcciones/guardar/{id_user}', ['as' => 'direcciones.guardar', 'uses' => 'DireccionController@store'])->middleware('verified')->middleware('permiso:Direcciones,crear');
    Route::get('/direcciones/seleccionardireccionfact/{id_direccion}/{id_user}', ['as' => 'direcciones.seleccionardireccionfact', 'uses' => 'DireccionController@seleccionardireccionfact'])->middleware('verified')->middleware('permiso:Direcciones,editar');
    Route::get('/direcciones/cambiarestado/{id_direccion}/{id_user}/{estado}', ['as' => 'direcciones.cambiarestado', 'uses' => 'DireccionController@cambiarestado'])->middleware('verified')->middleware('permiso:Direcciones,editar');
    // Route::get('/direcciones/update/{id}', ['as' => 'direcciones.update', 'uses' => 'DireccionController@update'])->middleware('verified');
    Route::get('/direcciones/borrar/{id}', ['as' => 'direcciones.borrar', 'uses' => 'DireccionController@destroy'])->middleware('verified');
    Route::get('/direcciones/index', ['as' => 'direcciones.index', 'uses' => 'DireccionController@index'])->middleware('verified');
    Route::get('/direcciones/ver/{id}', ['as' => 'direcciones.ver', 'uses' => 'DireccionController@show'])->middleware('verified');
    Route::get('/direcciones/autoGenerar/{id}', ['as' => 'direcciones.crearAutomaticamente', 'uses' => 'DireccionController@crearAutomaticamente'])->middleware('verified');

    // Pedidos
    Route::get('/pedidos/crear', ['as' => 'pedidos.crear', 'uses' => 'PedidoController@create'])->middleware('verified')->middleware('permiso:Pedidos,crear');
    Route::get('/pedidos/editar/{id}', ['as' => 'pedidos.editar', 'uses' => 'PedidoController@edit'])->middleware('verified')->middleware('permiso:Pedidos,editar');
    Route::post('/pedidos/actualizar/{id}', ['as' => 'pedidos.actualizar', 'uses' => 'PedidoController@update'])->middleware('verified')->middleware('permiso:Pedidos,editar');
    Route::post('/pedidos/guardar', ['as' => 'pedidos.guardar', 'uses' => 'PedidoController@store'])->middleware('verified')->middleware('permiso:Pedidos,crear');
    // Route::get('/pedidos/update/{id}', ['as' => 'pedidos.update', 'uses' => 'PedidoController@update'])->middleware('verified');
    Route::get('/pedidos/borrar/{id}', ['as' => 'pedidos.borrar', 'uses' => 'PedidoController@destroy'])->middleware('verified')->middleware('permiso:Pedidos,borrar');
    Route::get('/pedidos/index', ['as' => 'pedidos.index', 'uses' => 'PedidoController@index'])->middleware('verified')->middleware('permiso:Pedidos,ver');
    Route::post('/pedidos/index', ['as' => 'pedidos.index', 'uses' => 'PedidoController@index'])->middleware('verified')->middleware('permiso:Pedidos,ver');
    Route::get('/pedidos/enpreparacion', ['as' => 'pedidos.enpreparacion', 'uses' => 'PedidoController@enpreparacion'])->middleware('verified')->middleware('permiso:Pedidos,ver');
    Route::post('/pedidos/enpreparacion', ['as' => 'pedidos.enpreparacion', 'uses' => 'PedidoController@enpreparacion'])->middleware('verified')->middleware('permiso:Pedidos,ver');
    Route::get('/pedidos/direccionescliente/{id?}', ['as' => 'pedidos.direccionescliente', 'uses' => 'PedidoController@direccionescliente'])->middleware('verified')->middleware('permiso:Pedidos,crear');
    Route::get('/pedidos/ver/{id}', ['as' => 'pedidos.ver', 'uses' => 'PedidoController@show'])->middleware('verified')->middleware('permiso:Pedidos,ver');
    Route::get('/pedidos/remito/{id}', ['as' => 'pedidos.remito', 'uses' => 'PedidoController@remito'])->middleware('verified')->middleware('permiso:Pedidos,editar');
    Route::get('/pedidos/cambiarestado/{id_pedido}/{id_estado}', ['as' => 'pedidos.cambiarestado', 'uses' => 'PedidoController@cambiarestado'])->middleware('verified')->middleware('permiso:Pedidos,editar');
    Route::get('/pedidos/reportes', ['as' => 'pedidos.reportes', function () { return view('pedidos.reportes');}])->middleware('verified')->middleware('permiso:Pedidos,ver');
    Route::post('/pedidos/reportes', ['as' => 'pedidos.getreporte', 'uses' => 'PedidoController@getreporte'])->middleware('verified')->middleware('permiso:Pedidos,ver');
    Route::post('/pedidos/editarRemito/{id}', ['as' => 'pedidos.editarRemito', 'uses' => 'PedidoController@editarRemito'])->middleware('verified')->middleware('permiso:Pedidos,editar');
    Route::get('/pedidos/getultimoprecio/{id_user}/{id_producto}', ['as' => 'pedidos.getultimoprecio', 'uses' => 'PedidoController@getultimoprecio'])->middleware('verified')->middleware('permiso:Pedidos,crear');
    
    // Compras
    Route::get('/compras/crear', ['as' => 'compras.crear', 'uses' => 'CompraController@create'])->middleware('verified')->middleware('permiso:Compras,crear');
    Route::get('/compras/editar/{id}', ['as' => 'compras.editar', 'uses' => 'CompraController@edit'])->middleware('verified')->middleware('permiso:Compras,editar');
    Route::post('/compras/actualizar/{id}', ['as' => 'compras.actualizar', 'uses' => 'CompraController@update'])->middleware('verified')->middleware('permiso:Compras,editar');
    Route::post('/compras/guardar', ['as' => 'compras.guardar', 'uses' => 'CompraController@store'])->middleware('verified')->middleware('permiso:Compras,crear');
    Route::get('/compras/pendientes', ['as' => 'compras.pendientes', 'uses' => 'CompraController@pendientes'])->middleware('verified')->middleware('permiso:Compras,ver');
    Route::post('/compras/pendientes', ['as' => 'compras.pendientes', 'uses' => 'CompraController@pendientes'])->middleware('verified')->middleware('permiso:Compras,ver');
    Route::get('/compras/todas', ['as' => 'compras.todas', 'uses' => 'CompraController@todas'])->middleware('verified')->middleware('permiso:Compras,ver');
    Route::post('/compras/todas', ['as' => 'compras.todas', 'uses' => 'CompraController@todas'])->middleware('verified')->middleware('permiso:Compras,ver');
    Route::get('/compras/getproductos/{id_cliente?}', ['as' => 'compras.getproductos', 'uses' => 'CompraController@getproductos'])->middleware('verified')->middleware('permiso:Compras,editar');
    Route::get('/compras/ver/{id}', ['as' => 'compras.ver', 'uses' => 'CompraController@show'])->middleware('verified')->middleware('permiso:Compras,ver');
    Route::get('/compras/imprimir/{id}', ['as' => 'compras.imprimir', 'uses' => 'CompraController@imprimir'])->middleware('verified')->middleware('permiso:Compras,ver');
    Route::get('/compras/remito/{id}', ['as' => 'compras.remito', 'uses' => 'CompraController@remito'])->middleware('verified')->middleware('permiso:Compras,crear');
    Route::get('/compras/aprobar/{id_pedido}', ['as' => 'compras.aprobar', 'uses' => 'CompraController@aprobar'])->middleware('verified')->middleware('permiso:Compras,editar');
    Route::get('/compras/recibir/{id_pedido}', ['as' => 'compras.recibir', 'uses' => 'CompraController@recibir'])->middleware('verified')->middleware('permiso:Compras,editar');
    Route::post('/compras/recibirsuccess/{id_pedido}', ['as' => 'compras.recibirsuccess', 'uses' => 'CompraController@recibirsuccess'])->middleware('verified')->middleware('permiso:Compras,editar');
    Route::post('/compras/pagar', ['as' => 'compras.pagar', 'uses' => 'CompraController@pagar'])->middleware('verified')->middleware('permiso:Compras,editar');
    Route::get('/compras/cancelar/{id_pedido}', ['as' => 'compras.cancelar', 'uses' => 'CompraController@cancelar'])->middleware('verified')->middleware('permiso:Compras,borrar');
    Route::get('/compras/direccionesproveedor/{id?}', ['as' => 'compras.direccionesproveedor', 'uses' => 'CompraController@direccionesproveedor'])->middleware('verified')->middleware('permiso:Compras,crear');
    Route::get('/compras/faltante/{id}', ['as' => 'compras.faltante', 'uses' => 'CompraController@faltante'])->middleware('verified')->middleware('permiso:Compras,crear');
    Route::get('/compras/excedente/{id}', ['as' => 'compras.excedente', 'uses' => 'CompraController@excedente'])->middleware('verified')->middleware('permiso:Devoluciones compras,crear');
    Route::get('/compras/createdevolucion', ['as' => 'compras.createdevolucion', 'uses' => 'CompraController@createdevolucion'])->middleware('verified')->middleware('permiso:Devoluciones compras,crear');
    Route::post('/compras/generardevolucion/{id}', ['as' => 'compras.generardevolucion', 'uses' => 'CompraController@generardevolucion'])->middleware('verified')->middleware('permiso:Devoluciones compras,crear');
    Route::get('/compras/getproductoscomprados/{id_proveedor?}', ['as' => 'compras.getproductoscomprados', 'uses' => 'CompraController@getproductoscomprados'])->middleware('verified')->middleware('permiso:Devoluciones compras,crear');
    Route::get('/compras/devoluciones', ['as' => 'compras.devoluciones', 'uses' => 'CompraController@devoluciones'])->middleware('verified')->middleware('permiso:Devoluciones compras,ver');
    Route::get('/compras/verdevolucion/{id}', ['as' => 'compras.verdevolucion', 'uses' => 'CompraController@verdevolucion'])->middleware('verified')->middleware('permiso:Devoluciones compras,ver');
    Route::get('/compras/aceptardev/{id_pedido}', ['as' => 'compras.aceptardev', 'uses' => 'CompraController@aceptar'])->middleware('verified')->middleware('permiso:Devoluciones compras,editar');
    Route::get('/compras/rechazardev/{id_pedido}', ['as' => 'compras.rechazardev', 'uses' => 'CompraController@rechazar'])->middleware('verified')->middleware('permiso:Devoluciones compras,editar');
    Route::post('/compras/addarchivo/{id_pedido}', ['as' => 'compras.addarchivo', 'uses' => 'CompraController@addarchivo'])->middleware('verified')->middleware('permiso:Devoluciones compras,editar');
    Route::get('/compras/deletearchivo/{id_pedido}', ['as' => 'compras.deletearchivo', 'uses' => 'CompraController@deletearchivo'])->middleware('verified')->middleware('permiso:Devoluciones compras,editar');
    Route::get('/compras/reportes', ['as' => 'compras.reportes', function () { return view('compras.reportes');}])->middleware('verified')->middleware('permiso:Compras,ver');
    Route::post('/compras/reportes', ['as' => 'compras.getreporte', 'uses' => 'CompraController@getreporte'])->middleware('verified')->middleware('permiso:Compras,ver');
    Route::post('/compras/addordenpago', ['as' => 'compras.addordenpago', 'uses' => 'CompraController@addordenpago'])->middleware('verified')->middleware('permiso:Ordenes de pago,crear');
    Route::get('/compras/ordenespagos', ['as' => 'compras.ordenespagos', 'uses' => 'CompraController@ordenespagos'])->middleware('verified')->middleware('permiso:Ordenes de pago,ver');
    Route::post('/compras/ordenespagos', ['as' => 'compras.ordenespagos', 'uses' => 'CompraController@ordenespagos'])->middleware('verified')->middleware('permiso:Ordenes de pago,ver');
    Route::post('/compras/pagarorden', ['as' => 'compras.pagarorden', 'uses' => 'CompraController@pagarorden'])->middleware('verified')->middleware('permiso:Ordenes de pago,editar');
    Route::get('/compras/deleteordenpago/{id}/{url_redirect}/{id_pedido?}', ['as' => 'compras.deleteordenpago', 'uses' => 'CompraController@deleteordenpago'])->middleware('verified')->middleware('permiso:Ordenes de pago,borrar');

    // Devoluciones
    Route::get('/devoluciones/crear', ['as' => 'devoluciones.crear', 'uses' => 'DevolucionController@create'])->middleware('verified')->middleware('permiso:Devoluciones,crear');
    Route::get('/devoluciones/editar/{id}', ['as' => 'devoluciones.editar', 'uses' => 'DevolucionController@edit'])->middleware('verified')->middleware('permiso:Devoluciones,editar');
    Route::post('/devoluciones/actualizar/{id}', ['as' => 'devoluciones.actualizar', 'uses' => 'DevolucionController@update'])->middleware('verified')->middleware('permiso:Devoluciones,editar');
    Route::post('/devoluciones/guardar', ['as' => 'devoluciones.guardar', 'uses' => 'DevolucionController@store'])->middleware('verified')->middleware('permiso:Devoluciones,crear');
    Route::get('/devoluciones/pendientes', ['as' => 'devoluciones.pendientes', 'uses' => 'DevolucionController@pendientes'])->middleware('verified')->middleware('permiso:Devoluciones,ver');
    Route::post('/devoluciones/pendientes', ['as' => 'devoluciones.pendientes', 'uses' => 'DevolucionController@pendientes'])->middleware('verified')->middleware('permiso:Devoluciones,ver');
    Route::get('/devoluciones/todas', ['as' => 'devoluciones.todas', 'uses' => 'DevolucionController@todas'])->middleware('verified')->middleware('permiso:Devoluciones,ver');
    Route::post('/devoluciones/todas', ['as' => 'devoluciones.todas', 'uses' => 'DevolucionController@todas'])->middleware('verified')->middleware('permiso:Devoluciones,ver');
    Route::get('/devoluciones/getproductos/{id_cliente?}', ['as' => 'devoluciones.getproductos', 'uses' => 'DevolucionController@getproductos'])->middleware('verified')->middleware('permiso:Devoluciones,editar');
    Route::get('/devoluciones/ver/{id}', ['as' => 'devoluciones.ver', 'uses' => 'DevolucionController@show'])->middleware('verified')->middleware('permiso:Devoluciones,ver');
    Route::get('/devoluciones/remito/{id}', ['as' => 'devoluciones.remito', 'uses' => 'DevolucionController@remito'])->middleware('verified')->middleware('permiso:Devoluciones,crear');
    Route::get('/devoluciones/aceptar/{id_pedido}', ['as' => 'devoluciones.aceptar', 'uses' => 'DevolucionController@aceptar'])->middleware('verified')->middleware('permiso:Devoluciones,crear');
    Route::get('/devoluciones/rechazar/{id_pedido}', ['as' => 'devoluciones.rechazar', 'uses' => 'DevolucionController@rechazar'])->middleware('verified')->middleware('permiso:Devoluciones,crear');

    // Remitos
    Route::get('/remitos/crear', ['as' => 'remitos.crear', 'uses' => 'RemitoController@create'])->middleware('verified')->middleware('permiso:Remitos,crear');
    Route::get('/remitos/editar/{id}', ['as' => 'remitos.editar', 'uses' => 'RemitoController@edit'])->middleware('verified')->middleware('permiso:Remitos,editar');
    Route::post('/remitos/actualizar/{id}', ['as' => 'remitos.actualizar', 'uses' => 'RemitoController@update'])->middleware('verified')->middleware('permiso:Remitos,editar');
    Route::post('/remitos/guardar', ['as' => 'remitos.guardar', 'uses' => 'RemitoController@store'])->middleware('verified')->middleware('permiso:Remitos,crear');
    // Route::get('/remitos/update/{id}', ['as' => 'remitos.update', 'uses' => 'RemitoController@update'])->middleware('verified');
    Route::get('/remitos/borrar/{id}', ['as' => 'remitos.borrar', 'uses' => 'RemitoController@destroy'])->middleware('verified')->middleware('permiso:Remitos,borrar');
    Route::get('/remitos/index', ['as' => 'remitos.index', 'uses' => 'RemitoController@index'])->middleware('verified')->middleware('permiso:Remitos,ver');
    Route::post('/remitos/index', ['as' => 'remitos.index', 'uses' => 'RemitoController@index'])->middleware('verified')->middleware('permiso:Remitos,ver');
    Route::get('/remitos/direccionescliente/{id?}', ['as' => 'remitos.direccionescliente', 'uses' => 'RemitoController@direccionescliente'])->middleware('verified')->middleware('permiso:Remitos,editar');
    Route::get('/remitos/ver/{id}', ['as' => 'remitos.ver', 'uses' => 'RemitoController@show'])->middleware('verified')->middleware('permiso:Remitos,ver');
    Route::get('/remitos/imprimir/{id}', ['as' => 'remitos.imprimir', 'uses' => 'RemitoController@imprimir'])->middleware('verified')->middleware('permiso:Remitos,ver');
    Route::get('/remitos/imprimirtransporte/{id}', ['as' => 'remitos.imprimirtransporte', 'uses' => 'RemitoController@imprimirtransporte'])->middleware('verified')->middleware('permiso:Remitos,ver');
    Route::get('/remitos/remito/{id}', ['as' => 'remitos.remito', 'uses' => 'RemitoController@remito'])->middleware('verified')->middleware('permiso:Remitos,ver');
    Route::get('/remitos/cambiarestado/{id_pedido}/{id_estado}', ['as' => 'remitos.cambiarestado', 'uses' => 'RemitoController@cambiarestado'])->middleware('verified')->middleware('permiso:Remitos,editar');
    Route::post('/remitos/cobrar', ['as' => 'remitos.cobrar', 'uses' => 'RemitoController@cobrar'])->middleware('verified')->middleware('permiso:Pagos,crear');
    
    // Notas de credito
    Route::get('/notascredito/crear', ['as' => 'notascredito.crear', 'uses' => 'NotacreditoController@create'])->middleware('verified')->middleware('permiso:Notas de credito,crear');
    Route::get('/notascredito/editar/{id}', ['as' => 'notascredito.editar', 'uses' => 'NotacreditoController@edit'])->middleware('verified')->middleware('permiso:Notas de credito,editar');
    Route::post('/notascredito/actualizar/{id}', ['as' => 'notascredito.actualizar', 'uses' => 'NotacreditoController@update'])->middleware('verified')->middleware('permiso:Notas de credito,editar');
    Route::post('/notascredito/guardar', ['as' => 'notascredito.guardar', 'uses' => 'NotacreditoController@store'])->middleware('verified')->middleware('permiso:Notas de credito,crear');
    // Route::get('/notascredito/update/{id}', ['as' => 'notascredito.update', 'uses' => 'NotacreditoController@update'])->middleware('verified');
    Route::get('/notascredito/borrar/{id}', ['as' => 'notascredito.borrar', 'uses' => 'NotacreditoController@destroy'])->middleware('verified');
    Route::get('/notascredito/index', ['as' => 'notascredito.index', 'uses' => 'NotacreditoController@index'])->middleware('verified');
    Route::post('/notascredito/index', ['as' => 'notascredito.index', 'uses' => 'NotacreditoController@index'])->middleware('verified');
    Route::get('/notascredito/direccionescliente/{id?}', ['as' => 'notascredito.direccionescliente', 'uses' => 'NotacreditoController@direccionescliente'])->middleware('verified');
    Route::get('/notascredito/ver/{id}', ['as' => 'notascredito.ver', 'uses' => 'NotacreditoController@show'])->middleware('verified');
    Route::get('/notascredito/imprimir/{id}', ['as' => 'notascredito.imprimir', 'uses' => 'NotacreditoController@imprimir'])->middleware('verified');
    Route::get('/notascredito/remito/{id}', ['as' => 'notascredito.remito', 'uses' => 'NotacreditoController@remito'])->middleware('verified');
    Route::get('/notascredito/cambiarestado/{id_pedido}/{id_estado}', ['as' => 'notascredito.cambiarestado', 'uses' => 'NotacreditoController@cambiarestado'])->middleware('verified');

    //Facturas
    Route::get('/factura', ['as' => 'facturas.index', 'uses' => 'FacturaController@index'])->middleware('verified');
    Route::get('/factura/getFacturas', ['as' => 'facturas.getFacturas', 'uses' => 'FacturaController@getFacturas'])->middleware('verified');
    Route::get('/factura/generar/{id}/{idDomicilio}', ['as' => 'facturas.generar', 'uses' => 'FacturaController@generar'])->middleware('verified');
    Route::get('/factura/validar/{id}', ['as' => 'facturas.validar', 'uses' => 'FacturaController@validar'])->middleware('verified');
    Route::get('/factura/imprimir/{id}', ['as' => 'facturas.imprimir', 'uses' => 'FacturaController@imprimir'])->middleware('verified');
    Route::get('/factura/ver/{id}', ['as' => 'facturas.ver', 'uses' => 'FacturaController@ver'])->middleware('verified');
    Route::get('/factura/domicilioFiscal/{id}', ['as' => 'facturas.domicilioFiscal', 'uses' => 'FacturaController@domicilioFiscal'])->middleware('verified');
    Route::get('/factura/consulta/{cuit}', ['as' => 'facturas.consulta', 'uses' => 'FacturaController@consulta'])->middleware('verified');
    Route::get('/factura/estado', ['as' => 'facturas.estado', 'uses' => 'FacturaController@estado'])->middleware('verified');
    Route::get('/factura/infoComprobante/{nroComp}/{ptoVenta}/{tipoComp}', ['as' => 'facturas.infoComprobante', 'uses' => 'FacturaController@infoComprobante'])->middleware('verified');
    Route::get('/factura/alicuotasGet', ['as' => 'facturas.alicuotasGet', 'uses' => 'FacturaController@alicuotasGet'])->middleware('verified');
    Route::get('/factura/ptoVtaGet', ['as' => 'facturas.ptoVtaGet', 'uses' => 'FacturaController@ptoVtaGet'])->middleware('verified');
    Route::get('/factura/comprobantesGet', ['as' => 'facturas.comprobantesGet', 'uses' => 'FacturaController@comprobantesGet'])->middleware('verified');
    Route::get('/factura/conceptosGet', ['as' => 'facturas.conceptosGet', 'uses' => 'FacturaController@conceptosGet'])->middleware('verified');
    Route::get('/factura/documentosGet', ['as' => 'facturas.documentosGet', 'uses' => 'FacturaController@documentosGet'])->middleware('verified');
    Route::get('/factura/opcionesComproGet', ['as' => 'facturas.opcionesComproGet', 'uses' => 'FacturaController@opcionesComproGet'])->middleware('verified');
    Route::get('/factura/tributosGet', ['as' => 'facturas.tributosGet', 'uses' => 'FacturaController@tributosGet'])->middleware('verified');
    Route::get('/factura/ultimaFactura/{ptoVenta}/{tipoComp}', ['as' => 'facturas.ultimaFactura', 'uses' => 'FacturaController@ultimaFactura'])->middleware('verified');
});
 
Route::prefix('admin')->group(function () {
       
    // Perfiles
    Route::get('/perfiles/crear', ['as' => 'perfiles.crear', 'uses' => 'PerfilController@create'])->middleware('verified')->middleware('permiso:Perfiles,crear');
    Route::get('/perfiles/editar/{id}', ['as' => 'perfiles.editar', 'uses' => 'PerfilController@edit'])->middleware('verified')->middleware('permiso:Perfiles,editar');
    Route::post('/perfiles/actualizar/{id}', ['as' => 'perfiles.actualizar', 'uses' => 'PerfilController@update'])->middleware('verified')->middleware('permiso:Perfiles,editar');
    Route::post('/perfiles/guardar', ['as' => 'perfiles.guardar', 'uses' => 'PerfilController@store'])->middleware('verified')->middleware('permiso:Perfiles,crear');
    // Route::get('/perfiles/update/{id}', ['as' => 'perfiles.update', 'uses' => 'PerfilController@update'])->middleware('verified');
    Route::get('/perfiles/borrar/{id}', ['as' => 'perfiles.borrar', 'uses' => 'PerfilController@destroy'])->middleware('verified')->middleware('permiso:Perfiles,borrar');
    Route::get('/perfiles/index', ['as' => 'perfiles.index', 'uses' => 'PerfilController@index'])->middleware('verified')->middleware('permiso:Perfiles,ver');

    // Usuarios
    Route::get('/usuarios/crear', ['as' => 'usuarios.crear', 'uses' => 'UserController@create'])->middleware('verified')->middleware('permiso:Usuarios,crear');
    Route::get('/usuarios/editar/{id}', ['as' => 'usuarios.editar', 'uses' => 'UserController@edit'])->middleware('verified')->middleware('permiso:Usuarios,editar');
    Route::post('/usuarios/actualizar/{id}', ['as' => 'usuarios.actualizar', 'uses' => 'UserController@update'])->middleware('verified')->middleware('permiso:Usuarios,editar');
    Route::get('/usuarios/suspender/{id}', ['as' => 'usuarios.suspender', 'uses' => 'UserController@suspend'])->middleware('verified')->middleware('permiso:Usuarios,editar');
    Route::post('/usuarios/guardar', ['as' => 'usuarios.guardar', 'uses' => 'UserController@store'])->middleware('verified')->middleware('permiso:Usuarios,crear');
    // Route::get('/usuarios/update/{id}', ['as' => 'usuarios.update', 'uses' => 'UserController@update'])->middleware('verified');
    Route::get('/usuarios/borrar/{id}', ['as' => 'usuarios.borrar', 'uses' => 'UserController@destroy'])->middleware('verified')->middleware('permiso:Usuarios,borrar');
    Route::get('/usuarios/index', ['as' => 'usuarios.index', 'uses' => 'UserController@index'])->middleware('verified')->middleware('permiso:Usuarios,ver');
    Route::post('/usuarios/index', ['as' => 'usuarios.index', 'uses' => 'UserController@index'])->middleware('verified')->middleware('permiso:Usuarios,ver');

    // Medios
    Route::get('/medios/crear', ['as' => 'medios.crear', 'uses' => 'MedioController@create'])->middleware('verified')->middleware('permiso:Medios de pagos,crear');
    Route::get('/medios/editar/{id}', ['as' => 'medios.editar', 'uses' => 'MedioController@edit'])->middleware('verified')->middleware('permiso:Medios de pagos,editar');
    Route::post('/medios/actualizar/{id}', ['as' => 'medios.actualizar', 'uses' => 'MedioController@update'])->middleware('verified')->middleware('permiso:Medios de pagos,editar');
    Route::post('/medios/guardar', ['as' => 'medios.guardar', 'uses' => 'MedioController@store'])->middleware('verified')->middleware('permiso:Medios de pagos,crear');
    // Route::get('/medios/update/{id}', ['as' => 'medios.update', 'uses' => 'MedioController@update'])->middleware('verified');
    Route::get('/medios/borrar/{id}', ['as' => 'medios.borrar', 'uses' => 'MedioController@destroy'])->middleware('verified')->middleware('permiso:Medios de pagos,borrar');
    Route::get('/medios/index', ['as' => 'medios.index', 'uses' => 'MedioController@index'])->middleware('verified')->middleware('permiso:Medios de pagos,ver');
    Route::get('/medios/arqueos/{id}', ['as' => 'medios.arqueos', 'uses' => 'MedioController@arqueos'])->middleware('verified')->middleware('permiso:Medios de pagos,editar');
    Route::post('/medios/arquear', ['as' => 'medios.arquear', 'uses' => 'MedioController@arquear'])->middleware('verified')->middleware('permiso:Medios de pagos,editar');
    Route::get('/medios/movimientos/{id}', ['as' => 'medios.movimientos', 'uses' => 'MedioController@movimientos'])->middleware('verified')->middleware('permiso:Medios de pagos,editar');
    Route::post('/medios/movimientos/{id}', ['as' => 'medios.movimientos', 'uses' => 'MedioController@movimientos'])->middleware('verified')->middleware('permiso:Medios de pagos,editar');
    
    // Almacenes
    Route::get('/almacenes/crear', ['as' => 'almacenes.crear', 'uses' => 'AlmacenController@create'])->middleware('verified')->middleware('permiso:Almacenes,ver');
    Route::get('/almacenes/editar/{id}', ['as' => 'almacenes.editar', 'uses' => 'AlmacenController@edit'])->middleware('verified')->middleware('permiso:Almacenes,editar');
    Route::post('/almacenes/actualizar/{id}', ['as' => 'almacenes.actualizar', 'uses' => 'AlmacenController@update'])->middleware('verified')->middleware('permiso:Almacenes,editar');
    Route::post('/almacenes/guardar', ['as' => 'almacenes.guardar', 'uses' => 'AlmacenController@store'])->middleware('verified')->middleware('permiso:Almacenes,ver');
    // Route::get('/almacenes/update/{id}', ['as' => 'almacenes.update', 'uses' => 'AlmacenController@update'])->middleware('verified');
    Route::get('/almacenes/borrar/{id}', ['as' => 'almacenes.borrar', 'uses' => 'AlmacenController@destroy'])->middleware('verified')->middleware('permiso:Almacenes,borrar');
    Route::get('/almacenes/index', ['as' => 'almacenes.index', 'uses' => 'AlmacenController@index'])->middleware('verified')->middleware('permiso:Almacenes,ver');
});
// Route::get('/', ['as' => 'app', function () {
//     return view('layouts/app');
// }]);

// Route::get('/{any?}', function () {
//     return view('index');
// })->where('any', '^(?!api).*$')->where('any', '^(?!admin).*$');