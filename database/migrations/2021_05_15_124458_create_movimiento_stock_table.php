<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovimientoStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimiento_stock', function (Blueprint $table) {
            $table->increments('id_movimiento_stock');
            $table->string('tipo_movimiento')->nullable();
            $table->decimal('cantidad', 10, 2)->default(0);
            $table->string('descripcion')->nullable();
            $table->integer('user_id_user')->unsigned();
            $table->foreign('user_id_user')->references('id_user')->on('users');
            $table->integer('almacen_id_origen')->unsigned();
            $table->foreign('almacen_id_origen')->references('id_stock')->on('almacen_producto');
            $table->integer('almacen_id_destino')->unsigned();
            $table->foreign('almacen_id_destino')->references('id_stock')->on('almacen_producto');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movimiento_stock');
    }
}
