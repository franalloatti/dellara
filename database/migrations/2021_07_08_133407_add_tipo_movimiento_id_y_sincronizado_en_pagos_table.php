<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTipoMovimientoIdYSincronizadoEnPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pagos', function(Blueprint $table) {
            $table->integer('tipo_movimiento_id')->unsigned();
            $table->foreign('tipo_movimiento_id')->references('id_tipo_movimiento')->on('tipo_movimientos');
            $table->enum('consolidado', ['N','S'])->default('N');
            $table->string('nombre');
            $table->date('fecha')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pagos', function(Blueprint $table) {
            $table->dropColumn('tipo_movimiento_id');
        });
    }
}
