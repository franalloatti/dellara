<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMargenEnProductoUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('producto_user', function(Blueprint $table) {
            $table->decimal('margen', 5, 2)->nullable()->default(100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('producto_user', function(Blueprint $table) {
            $table->dropColumn('margen');
        });
    }
}
// CONSULTA PARA COPIAR LOS MARGENES DEL PRODUCTO
// update producto_user as PPU
// inner join (
// select PU.id_costo, P.margen from productos as P
// join producto_user as PU ON P.id_producto = PU.producto_id_producto
// ) as A on A.id_costo = PPU.id_costo
// set PPU.margen = A.margen