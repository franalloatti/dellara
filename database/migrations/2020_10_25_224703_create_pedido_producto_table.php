<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidoProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido_producto', function (Blueprint $table) {
            $table->increments('id_pedido_producto');
            $table->integer('pedido_id_pedido')->unsigned();
            $table->integer('producto_id_producto')->unsigned();
            $table->foreign('pedido_id_pedido')->references('id_pedido')->on('pedidos');
            $table->foreign('producto_id_producto')->references('id_producto')->on('productos');
            $table->integer('cantidad')->nullable()->default(0);
            $table->decimal('descuento', 4, 2)->nullable()->default(0);
            $table->decimal('costo_unitario', 10, 2)->nullable()->default(0);
            $table->decimal('precio_unitario', 10, 2)->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedido_producto');
    }
}
