<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturaItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factura_item', function (Blueprint $table) {
            $table->id('id_item_factura');
            $table->unsignedBigInteger('factura_id');
            $table->integer('Id')->default(5);
            $table->float('BaseImp');
            $table->float('Importe');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factura_item');
    }
}
