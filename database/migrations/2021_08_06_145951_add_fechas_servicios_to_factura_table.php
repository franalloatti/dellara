<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFechasServiciosToFacturaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('factura', function (Blueprint $table) {
            $table->date('FchServDesde')->nullable();
            $table->date('FchServHasta')->nullable();
            $table->date('FchVtoPago')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('factura', function (Blueprint $table) {
            $table->dropColumn('FchServDesde');
            $table->dropColumn('FchServHasta');
            $table->dropColumn('FchVtoPago');
        });
    }
}
