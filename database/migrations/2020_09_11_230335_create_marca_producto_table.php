<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarcaProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marca_producto', function (Blueprint $table) {
            $table->increments('id_marca_producto');
            $table->integer('marca_id_marca')->unsigned();
            $table->integer('producto_id_producto')->unsigned();
            $table->foreign('marca_id_marca')->references('id_marca')->on('marcas');
            $table->foreign('producto_id_producto')->references('id_producto')->on('productos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marca_producto');
    }
}
