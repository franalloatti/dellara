<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa', function (Blueprint $table) {
            $table->id('id_empresa');
            $table->string('nombre');
            $table->bigInteger("cuit");
            $table->integer("telefono")->nullable();
            $table->string("email")->nullable();
            $table->string("direccion")->nullable();
            $table->string("ciudad")->nullable();
            $table->string("provincia")->nullable();
            $table->string("pais")->nullable();
            $table->enum("categoriaIVA",['IVA Responsable Inscripto','IVA Sujeto Exento','Consumidor Final','Responsable Monotributo'])->nullable();
            $table->integer("iibb")->nullable();
            $table->date("inicioActividades")->nullable();
            $table->integer("PtoVenta")->nullable();
            $table->string("rutaKey")->nullable();
            $table->string("rutaCert")->nullable();
            $table->string("rutaLogo")->nullable();
            $table->date("fechaVenCert")->nullable();
            $table->integer("ultimaFactura")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emresa');
    }
}
