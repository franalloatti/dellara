<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatrizRecetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matriz_receta', function (Blueprint $table) {
            $table->increments('id_matriz_receta');
            $table->integer('producto_id')->unsigned();
            $table->foreign('producto_id')->references('id_producto')->on('productos');
            $table->integer('insumo_id')->unsigned();
            $table->foreign('insumo_id')->references('id_producto')->on('productos');
            $table->decimal('cantidad', 10, 2)->default();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matriz_receta');
    }
}
