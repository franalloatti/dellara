<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductoUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto_user', function (Blueprint $table) {
            $table->increments('id_costo');
            $table->integer('producto_id_producto')->unsigned();
            $table->integer('user_id_user')->unsigned();
            $table->foreign('user_id_user')->references('id_user')->on('users');
            $table->foreign('producto_id_producto')->references('id_producto')->on('productos');
            $table->string('codigo_proveedor')->nullable();
            $table->tinyInteger('seleccionado')->default(0);
            $table->decimal('costo_dolares', 10, 2)->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto_user');
    }
}
