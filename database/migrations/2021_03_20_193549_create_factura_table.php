<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factura', function (Blueprint $table) {
            $table->id('id_factura');
            $table->unsignedBigInteger('id_remito');
            $table->integer('CantReg');
            $table->integer('PtoVta');
            $table->integer('CbteTipo');
            $table->integer('Concepto');
            $table->integer('DocTipo');
            $table->bigInteger('DocNro');
            $table->integer('CbteDesde');
            $table->integer('CbteHasta');
            $table->date('CbteFch');
            $table->float('ImpTotal');
            $table->float('ImpTotConc');
            $table->float('ImpNeto');
            $table->float('ImpOpEx');
            $table->float('ImpIVA');
            $table->float('ImpTrib');
            $table->string('MonId')->default('PES');
            $table->float('MonCotiz')->default(1);
            $table->boolean('Facturado');
            $table->biginteger('CAE')->nullable();
            $table->date('fechaCAE')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factura');
    }
}
