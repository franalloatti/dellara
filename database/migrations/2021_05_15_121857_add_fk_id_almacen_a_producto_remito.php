<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkIdAlmacenAProductoRemito extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('producto_remito', function(Blueprint $table) {
            $table->integer('almacen_id_almacen')->unsigned()->default(1);
            $table->foreign('almacen_id_almacen')->references('id_almacen')->on('almacens');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('producto_remito', function(Blueprint $table) {
            $table->dropColumn('almacen_id_almacen');
        });
    }
}
