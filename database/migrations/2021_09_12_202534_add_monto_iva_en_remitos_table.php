<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMontoIvaEnRemitosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('remitos', function(Blueprint $table) {
            $table->decimal('monto_sin_iva', 10, 2)->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function remitos()
    {
        Schema::table('pedidos', function (Blueprint $table) {
            $table->dropColumn('monto_sin_iva');
        });
    }
}
