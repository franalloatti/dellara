<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->increments('id_pedido');
            $table->integer('user_id_user')->unsigned();
            $table->integer('direccion_id_direccion')->unsigned();
            $table->string('tipo', 45)->default('Pedido');
            $table->string('motivo_devolucion', 50)->nullable();
            $table->string('descripcion', 300)->nullable();
            $table->foreign('user_id_user')->references('id_user')->on('users');
            $table->foreign('direccion_id_direccion')->references('id_direccion')->on('direccions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
