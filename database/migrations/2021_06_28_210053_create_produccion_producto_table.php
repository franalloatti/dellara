<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduccionProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produccion_producto', function (Blueprint $table) {
            $table->increments('id_produccion_producto');
            $table->integer('producto_id')->unsigned();
            $table->foreign('producto_id')->references('id_producto')->on('productos');
            $table->integer('plan_produccion_id')->unsigned();
            $table->foreign('plan_produccion_id')->references('id_plan_produccion')->on('plan_produccion');
            $table->decimal('cantidad_planificada', 10, 2)->nullable();
            $table->decimal('cantidad_real', 10, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produccion_producto');
    }
}
