<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id_producto');
            $table->string('codigo')->unique();
            $table->string('nombre');
            $table->string('descripcion')->nullable();
            $table->decimal('margen', 5, 2)->nullable()->default(100);
            $table->bigInteger('stock_verde')->nullable()->default(0);
            $table->bigInteger('stock_amarillo')->nullable()->default(0);
            $table->bigInteger('stock_seguridad')->nullable()->default(0);
            $table->tinyInteger('tiene_fijado')->default(0);
            $table->tinyInteger('estado')->default(1);
            $table->decimal('precio_fijado', 10, 2)->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
