<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstadoPedidoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estado_pedido', function (Blueprint $table) {
            $table->increments('id_estado_pedido');
            $table->integer('estado_id_estado')->unsigned();
            $table->integer('pedido_id_pedido')->unsigned();
            $table->foreign('pedido_id_pedido')->references('id_pedido')->on('pedidos');
            $table->foreign('estado_id_estado')->references('id_estado')->on('estados');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estado_pedido');
    }
}
