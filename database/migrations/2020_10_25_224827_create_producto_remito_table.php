<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductoRemitoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto_remito', function (Blueprint $table) {
            $table->increments('id_producto_remito');
            $table->integer('producto_id_producto')->unsigned();
            $table->integer('remito_id_remito')->unsigned();
            $table->foreign('remito_id_remito')->references('id_remito')->on('remitos');
            $table->foreign('producto_id_producto')->references('id_pedido_producto')->on('pedido_producto');
            $table->integer('cantidad')->nullable()->default(0);
            $table->decimal('descuento', 4, 2)->nullable()->default(0);
            $table->decimal('costo_unitario', 10, 2)->nullable()->default(0);
            $table->decimal('precio_unitario', 10, 2)->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto_remito');
    }
}
