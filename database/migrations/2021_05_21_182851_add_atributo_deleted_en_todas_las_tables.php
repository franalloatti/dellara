<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAtributoDeletedEnTodasLasTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('almacens', function(Blueprint $table) {
            $table->tinyInteger('deleted')->default(0);
        });
        Schema::table('direccions', function(Blueprint $table) {
            $table->tinyInteger('deleted')->default(0);
        });
        Schema::table('marcas', function(Blueprint $table) {
            $table->tinyInteger('deleted')->default(0);
        });
        Schema::table('medios', function(Blueprint $table) {
            $table->tinyInteger('deleted')->default(0);
        });
        Schema::table('pagos', function(Blueprint $table) {
            $table->tinyInteger('deleted')->default(0);
        });
        Schema::table('pedidos', function(Blueprint $table) {
            $table->tinyInteger('deleted')->default(0);
        });
        Schema::table('perfils', function(Blueprint $table) {
            $table->tinyInteger('deleted')->default(0);
        });
        Schema::table('permisos', function(Blueprint $table) {
            $table->tinyInteger('deleted')->default(0);
        });
        Schema::table('productos', function(Blueprint $table) {
            $table->tinyInteger('deleted')->default(0);
        });
        Schema::table('remitos', function(Blueprint $table) {
            $table->tinyInteger('deleted')->default(0);
        });
        Schema::table('roles', function(Blueprint $table) {
            $table->tinyInteger('deleted')->default(0);
        });
        Schema::table('rubros', function(Blueprint $table) {
            $table->tinyInteger('deleted')->default(0);
        });
        Schema::table('users', function(Blueprint $table) {
            $table->tinyInteger('deleted')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('almacens', function(Blueprint $table) {
            $table->dropColumn('deleted');
        });
        Schema::table('direccions', function(Blueprint $table) {
            $table->dropColumn('deleted');
        });
        Schema::table('marcas', function(Blueprint $table) {
            $table->dropColumn('deleted');
        });
        Schema::table('medios', function(Blueprint $table) {
            $table->dropColumn('deleted');
        });
        Schema::table('pagos', function(Blueprint $table) {
            $table->dropColumn('deleted');
        });
        Schema::table('pedidos', function(Blueprint $table) {
            $table->dropColumn('deleted');
        });
        Schema::table('perfils', function(Blueprint $table) {
            $table->dropColumn('deleted');
        });
        Schema::table('permisos', function(Blueprint $table) {
            $table->dropColumn('deleted');
        });
        Schema::table('productos', function(Blueprint $table) {
            $table->dropColumn('deleted');
        });
        Schema::table('remitos', function(Blueprint $table) {
            $table->dropColumn('deleted');
        });
        Schema::table('roles', function(Blueprint $table) {
            $table->dropColumn('deleted');
        });
        Schema::table('rubros', function(Blueprint $table) {
            $table->dropColumn('deleted');
        });
        Schema::table('users', function(Blueprint $table) {
            $table->dropColumn('deleted');
        });
    }
}
