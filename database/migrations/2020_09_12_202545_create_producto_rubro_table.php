<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductoRubroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto_rubro', function (Blueprint $table) {
            $table->increments('id_producto_rubro');
            $table->integer('producto_id_producto')->unsigned();
            $table->integer('rubro_id_rubro')->unsigned();
            $table->foreign('rubro_id_rubro')->references('id_rubro')->on('rubros');
            $table->foreign('producto_id_producto')->references('id_producto')->on('productos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto_rubro');
    }
}
