<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanProduccionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_produccion', function (Blueprint $table) {
            $table->increments('id_plan_produccion');
            $table->integer('producto_id')->unsigned();
            $table->foreign('producto_id')->references('id_producto')->on('productos');
            $table->integer('estado_id')->unsigned();
            $table->foreign('estado_id')->references('id_estado_produccion')->on('estado_produccion');
            $table->decimal('cantidad_planificada', 10, 2)->nullable();
            $table->date('fecha_produccion')->nullable();
            $table->date('fecha_vencimiento')->nullable();
            $table->string('nro_lote')->unique()->nullable();
            $table->string('responsable')->nullable();
            $table->decimal('cantidad_final', 10, 2)->nullable();
            $table->string('descripcion', 500)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_produccion');
    }
}
