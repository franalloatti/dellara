<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdenPagoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orden_pago', function (Blueprint $table) {
            $table->increments('id_orden_pago');
            $table->decimal('monto', 10, 2)->nullable()->default(0);
            $table->string('tipo');
            $table->string('numero')->nullable();
            $table->date('fecha_emision')->nullable();
            $table->date('fecha_posible_pago')->nullable();
            $table->date('fecha_efectivo_pago')->nullable();
            $table->string('observaciones', 300)->nullable();
            $table->string('ruta_imagen')->nullable();
            $table->tinyInteger('pagado')->default(0);
            $table->integer('remito_id_remito')->unsigned();
            $table->foreign('remito_id_remito')->references('id_remito')->on('remitos');
            $table->integer('pago_id_pago')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orden_pago');
    }
}
