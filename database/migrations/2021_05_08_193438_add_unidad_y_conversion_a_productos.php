<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUnidadYConversionAProductos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('productos', function(Blueprint $table) {
            $table->string('unidad', 45)->nullable()->default('unidad');
            $table->decimal('conversion', 10, 2)->nullable()->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('productos', function(Blueprint $table) {
            $table->dropColumn('unidad');
            $table->dropColumn('conversion');
        });
    }
}
