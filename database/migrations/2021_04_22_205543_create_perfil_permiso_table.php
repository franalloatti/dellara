<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerfilPermisoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perfil_permiso', function (Blueprint $table) {
            $table->increments('id_perfil_permiso');
            $table->integer('perfil_id_perfil')->unsigned();
            $table->integer('permiso_id_permiso')->unsigned();
            $table->foreign('perfil_id_perfil')->references('id_perfil')->on('perfils');
            $table->foreign('permiso_id_permiso')->references('id_permiso')->on('permisos');
            $table->tinyInteger('ver')->default(0);
            $table->tinyInteger('crear')->default(0);
            $table->tinyInteger('editar')->default(0);
            $table->tinyInteger('borrar')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perfil_permiso');
    }
}
