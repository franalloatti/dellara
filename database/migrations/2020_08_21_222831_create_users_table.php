<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id_user');
            $table->string('name')->nullable();
            $table->string('surname')->nullable();
            $table->string('nombre_fantasia')->nullable();
            $table->string('cuit')->nullable();
            $table->bigInteger('numero_iibb')->nullable();
            $table->bigInteger('documento')->nullable();
            $table->string('username')->unique()->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->bigInteger('telefono')->nullable();
            $table->string('telefono_movil')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->string('description_status')->nullable();
            $table->timestamp('last_login_at')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->decimal('valor_dolar', 6, 2)->nullable()->default(1);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
