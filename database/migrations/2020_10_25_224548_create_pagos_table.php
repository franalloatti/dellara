<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagos', function (Blueprint $table) {
            $table->increments('id_pago');
            $table->integer('user_id_user')->unsigned();
            $table->integer('medio_id_medio')->unsigned();
            $table->foreign('user_id_user')->references('id_user')->on('users');
            $table->foreign('medio_id_medio')->references('id_medio')->on('medios');
            $table->decimal('monto', 10, 2)->nullable()->default(0.00);
            $table->string('comprobante')->nullable();
            $table->string('descripcion')->nullable();
            $table->string('estado')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagos');
    }
}
