<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemCarritoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_carrito', function (Blueprint $table) {
            $table->increments('id_item_carrito');
            $table->integer('cantidad')->nullable()->default(0);
            $table->integer('producto_id_producto')->unsigned();
            $table->integer('user_id_user')->unsigned();
            $table->foreign('user_id_user')->references('id_user')->on('users');
            $table->foreign('producto_id_producto')->references('id_producto')->on('productos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_carrito');
    }
}
