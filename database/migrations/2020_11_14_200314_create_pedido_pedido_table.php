<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidoPedidoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido_pedido', function (Blueprint $table) {
            $table->increments('id_pedido_pedido');
            $table->integer('pedido_id_padre')->unsigned();
            $table->integer('pedido_id_hijo')->unsigned();
            $table->foreign('pedido_id_padre')->references('id_pedido')->on('pedidos');
            $table->foreign('pedido_id_hijo')->references('id_pedido')->on('pedidos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedido_pedido');
    }
}
