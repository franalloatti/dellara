<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlmacenProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('almacen_producto', function (Blueprint $table) {
            $table->increments('id_stock');
            $table->integer('almacen_id_almacen')->unsigned();
            $table->integer('producto_id_producto')->unsigned();
            $table->foreign('almacen_id_almacen')->references('id_almacen')->on('almacens');
            $table->foreign('producto_id_producto')->references('id_producto')->on('productos');
            $table->integer('cantidad')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('almacen_producto');
    }
}
