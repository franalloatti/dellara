<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CambiarIvaADecimalEnTablas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('productos', function(Blueprint $table) {
            $table->decimal('iva', 3, 1)->change();
        });
        Schema::table('producto_remito', function(Blueprint $table) {
            $table->decimal('iva', 3, 1)->change();
        });
        Schema::table('pedido_producto', function(Blueprint $table) {
            $table->decimal('iva', 3, 1)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('productos', function(Blueprint $table) {
            $table->dropColumn('iva');
        });
        Schema::table('producto_remito', function(Blueprint $table) {
            $table->dropColumn('iva');
        });
        Schema::table('pedido_producto', function(Blueprint $table) {
            $table->dropColumn('iva');
        });
    }
}
