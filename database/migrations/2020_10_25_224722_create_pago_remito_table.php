<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagoRemitoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pago_remito', function (Blueprint $table) {
            $table->increments('id_pago_remito');
            $table->integer('pago_id_pago')->unsigned();
            $table->integer('remito_id_remito')->unsigned();
            $table->foreign('pago_id_pago')->references('id_pago')->on('pagos');
            $table->foreign('remito_id_remito')->references('id_remito')->on('remitos');
            $table->decimal('monto', 10, 2)->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pago_remito');
    }
}
