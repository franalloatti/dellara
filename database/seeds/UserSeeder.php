<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\Perfil;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_administrador = Role::where('name', 'administrador')->first();
        $role_admin = Role::where('name', 'admin')->first();             
        $perfil_superadmin = Perfil::where('nombre', 'Superadmin')->first();
        $perfil_admin = Perfil::where('nombre', 'Admin')->first();   

        $user = new User();
        $user->name = 'Francisco';
        $user->surname = 'Alloatti';
        $user->email = 'orealloatti@hotmail.com';
        $user->password = bcrypt('12121212');
        $user->status = 1;
        $user->perfil_id = $perfil_superadmin->id_perfil;
        $user->save();
        $user->roles()->attach($role_admin);

        $user = new User();
        $user->name = 'Ernesto';
        $user->surname = 'Peroche';
        $user->email = 'eperoche@gmail.com';
        $user->password = bcrypt('secretsecret');
        $user->status = 1;
        $user->perfil_id = $perfil_superadmin->id_perfil;
        $user->save();
        $user->roles()->attach($role_admin);

        $user = new User();
        $user->name = 'Usuario';
        $user->surname = 'Administrador';
        $user->email = 'administrador@gmail.com';
        $user->password = bcrypt('secretsecret');
        $user->status = 1;
        $user->perfil_id = $perfil_admin->id_perfil;
        $user->save();
        $user->roles()->attach($role_administrador);
    }
}
