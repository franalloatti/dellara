<?php

use Illuminate\Database\Seeder;
use App\TipoMovimiento;

class TipoMovimiento2Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipo_movimiento = new TipoMovimiento();
        $tipo_movimiento->nombre = 'Arqueo de cuenta';
        $tipo_movimiento->tipo = 'Arqueo';
        $tipo_movimiento->periodo = 0;
        $tipo_movimiento->save();
    }
}
