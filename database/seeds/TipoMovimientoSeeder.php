<?php

use Illuminate\Database\Seeder;
use App\TipoMovimiento;

class TipoMovimientoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipo_movimiento = new TipoMovimiento();
        $tipo_movimiento->nombre = 'Venta';
        $tipo_movimiento->tipo = 'Ingreso';
        $tipo_movimiento->periodo = 0;
        $tipo_movimiento->save();
        
        $tipo_movimiento = new TipoMovimiento();
        $tipo_movimiento->nombre = 'Compra productos';
        $tipo_movimiento->tipo = 'Compra';
        $tipo_movimiento->periodo = 0;
        $tipo_movimiento->save();
        
        $tipo_movimiento = new TipoMovimiento();
        $tipo_movimiento->nombre = 'Movimiento entre cuentas';
        $tipo_movimiento->tipo = 'Transferencia';
        $tipo_movimiento->periodo = 0;
        $tipo_movimiento->save();
        
        $tipo_movimiento = new TipoMovimiento();
        $tipo_movimiento->nombre = 'Retiro ganancias';
        $tipo_movimiento->tipo = 'Retiro';
        $tipo_movimiento->periodo = 0;
        $tipo_movimiento->save();
        
        $tipo_movimiento = new TipoMovimiento();
        $tipo_movimiento->nombre = 'Gasto';
        $tipo_movimiento->tipo = 'Gasto';
        $tipo_movimiento->periodo = 0;
        $tipo_movimiento->save();

        $tipo_movimiento = new TipoMovimiento();
        $tipo_movimiento->nombre = 'Inversion';
        $tipo_movimiento->tipo = 'Inversion';
        $tipo_movimiento->periodo = 0;
        $tipo_movimiento->save();
    }
}
