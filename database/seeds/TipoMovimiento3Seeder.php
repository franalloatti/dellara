<?php

use Illuminate\Database\Seeder;
use App\TipoMovimiento;

class TipoMovimiento3Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipo_movimiento = new TipoMovimiento();
        $tipo_movimiento->nombre = 'Ingreso';
        $tipo_movimiento->tipo = 'Ingreso';
        $tipo_movimiento->periodo = 0;
        $tipo_movimiento->save();
    }
}
