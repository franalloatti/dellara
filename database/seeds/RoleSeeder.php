<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = 'admin';
        $role->discount = 0;
        $role->save();        
        $role = new Role();
        $role->name = 'administrador';
        $role->discount = 0;
        $role->save();       
        $role = new Role();
        $role->name = 'Proveedor';
        $role->discount = 0;
        $role->save();    
        $role = new Role();
        $role->name = 'Consumidor final';
        $role->discount = 0;
        $role->save();
    }
}
