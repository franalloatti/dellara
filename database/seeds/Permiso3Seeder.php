<?php

use Illuminate\Database\Seeder;
use App\Permiso;

class Permiso3Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permiso = new Permiso();
        $permiso->nombre = 'Produccion';
        $permiso->descripcion = 'Gestion de produccion';
        $permiso->save();  
    }
}
