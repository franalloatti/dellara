<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);
        $this->call(PermisoSeeder::class);
        $this->call(Permiso2Seeder::class);
        $this->call(Permiso3Seeder::class);
        $this->call(Permiso4Seeder::class);
        $this->call(Permiso5Seeder::class);
        $this->call(Permiso6Seeder::class);
        $this->call(PerfilSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(DireccionSeeder::class);
        $this->call(EstadoSeeder::class);
        $this->call(EstadoProduccionSeeder::class);
        $this->call(ServicioSeeder::class);
        $this->call(TipoMovimientoSeeder::class);
        $this->call(TipoMovimiento2Seeder::class);
        $this->call(TipoMovimiento3Seeder::class);
    }
}
