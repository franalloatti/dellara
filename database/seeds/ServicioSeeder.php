<?php

use Illuminate\Database\Seeder;
use App\Producto;
use Illuminate\Support\Facades\DB;

class ServicioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $servicio = new Producto();
        $id=DB::select("SHOW TABLE STATUS LIKE 'productos'");
        $next_id=$id[0]->Auto_increment;
        $servicio->codigo = 'SERV-'.$next_id;
        $servicio->nombre = 'Servicio logístico';
        $servicio->descripcion = 'Servicio logístico';
        $servicio->tipo = 'Servicio';
        $servicio->save();    
    }
}
