<?php

use Illuminate\Database\Seeder;
use App\Permiso;

class PermisoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permiso = new Permiso();
        $permiso->nombre = 'Almacenes';
        $permiso->descripcion = 'Gestion de almacenes';
        $permiso->save();    
        $permiso = new Permiso();
        $permiso->nombre = 'Clientes';
        $permiso->descripcion = 'Gestion de clientes';
        $permiso->save();   
        $permiso = new Permiso();
        $permiso->nombre = 'Compras';
        $permiso->descripcion = 'Gestion de compras';
        $permiso->save();   
        $permiso = new Permiso();
        $permiso->nombre = 'Devoluciones';
        $permiso->descripcion = 'Gestion de devoluciones';
        $permiso->save();      
        $permiso = new Permiso();
        $permiso->nombre = 'Direcciones';
        $permiso->descripcion = 'Gestion de direcciones';
        $permiso->save();    
        $permiso = new Permiso();
        $permiso->nombre = 'Estados de cuentas';
        $permiso->descripcion = 'Gestion de estados de cuentas';
        $permiso->save();      
        $permiso = new Permiso();
        $permiso->nombre = 'Insumos';
        $permiso->descripcion = 'Gestion de insumos';
        $permiso->save();     
        $permiso = new Permiso();
        $permiso->nombre = 'Marcas';
        $permiso->descripcion = 'Gestion de marcas';
        $permiso->save();    
        $permiso = new Permiso();
        $permiso->nombre = 'Medios de pagos';
        $permiso->descripcion = 'Gestion de medios de pagos';
        $permiso->save();  
        $permiso = new Permiso();
        $permiso->nombre = 'Notas de credito';
        $permiso->descripcion = 'Gestion de notas de credito';
        $permiso->save();     
        $permiso = new Permiso();
        $permiso->nombre = 'Pagos';
        $permiso->descripcion = 'Gestion de pagos';
        $permiso->save();   
        $permiso = new Permiso();
        $permiso->nombre = 'Pedidos';
        $permiso->descripcion = 'Gestion de pedidos';
        $permiso->save();   
        $permiso = new Permiso();
        $permiso->nombre = 'Perfiles';
        $permiso->descripcion = 'Gestion de perfiles';
        $permiso->save();   
        $permiso = new Permiso();
        $permiso->nombre = 'Productos';
        $permiso->descripcion = 'Gestion de productos';
        $permiso->save();    
        $permiso = new Permiso();
        $permiso->nombre = 'Proveedores';
        $permiso->descripcion = 'Gestion de proveedores';
        $permiso->save();  
        $permiso = new Permiso();
        $permiso->nombre = 'Remitos';
        $permiso->descripcion = 'Gestion de remitos';
        $permiso->save();  
        $permiso = new Permiso();
        $permiso->nombre = 'Remotos';
        $permiso->descripcion = 'Gestion de logística';
        $permiso->save();   
        $permiso = new Permiso();
        $permiso->nombre = 'Rubros';
        $permiso->descripcion = 'Gestion de rubros';
        $permiso->save();       
        $permiso = new Permiso();
        $permiso->nombre = 'Servicios';
        $permiso->descripcion = 'Gestion de servicios';
        $permiso->save(); 
        $permiso = new Permiso();
        $permiso->nombre = 'Tipos de clientes';
        $permiso->descripcion = 'Gestion de tipos de clientes';
        $permiso->save();  
        $permiso = new Permiso();
        $permiso->nombre = 'Usuarios';
        $permiso->descripcion = 'Gestion de usuarios';
        $permiso->save();   
    }
}
