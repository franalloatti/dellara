<?php

use Illuminate\Database\Seeder;
use App\Direccion;

class DireccionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $direccion = new Direccion();
        $direccion->id_direccion = 1;
        $direccion->user_id = 1;
        $direccion->direccion = 'Dirección por defecto';
        $direccion->localidad = "Santa Fe";
        $direccion->provincia = "Santa Fe";
        $direccion->codigo_postal = "3000";
        $direccion->telefono = "12345678";
        $direccion->save(); 
    }
}
