<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstadoProduccionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estado_produccion')->insert([
            'nombre' => 'Planificado',
            'descripcion' => 'Planificado',
            'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('estado_produccion')->insert([
            'nombre' => 'Confirmado',
            'descripcion' => 'Confirmado',
            'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('estado_produccion')->insert([
            'nombre' => 'En producción',
            'descripcion' => 'En producción',
            'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('estado_produccion')->insert([
            'nombre' => 'Finalizado',
            'descripcion' => 'Finalizado',
            'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('estado_produccion')->insert([
            'nombre' => 'Cancelado',
            'descripcion' => 'Cancelado',
            'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
