<?php

use Illuminate\Database\Seeder;
use App\Estado;

class EstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $estado = new Estado();
        $estado->id_estado = 1;
        $estado->nombre = 'Artículos pendientes';
        $estado->descripcion = "Artículos en espera de stock";
        $estado->descripcion_compra = "Aprobada";
        $estado->save();  
        $estado = new Estado();
        $estado->id_estado = 2;
        $estado->nombre = 'En preparación';
        $estado->descripcion = "Solicitada";
        $estado->descripcion_compra = "Recibida";
        $estado->save();  
        $estado = new Estado();
        $estado->id_estado = 3;
        $estado->nombre = 'Entregado';
        $estado->descripcion = "Aceptada";
        $estado->descripcion_compra = "Pagada";
        $estado->save();     
        $estado = new Estado();
        $estado->id_estado = 4;
        $estado->nombre = 'Cancelado';
        $estado->descripcion = "Rechazada";
        $estado->descripcion_compra = "Cancelada";
        $estado->save();        
    }
}
