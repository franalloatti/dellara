<?php

use Illuminate\Database\Seeder;
use App\Permiso;

class Permiso6Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permiso = new Permiso();
        $permiso->nombre = 'Ordenes de pago';
        $permiso->descripcion = 'Gestion de ordenes de pago';
        $permiso->save();  
    }
}
