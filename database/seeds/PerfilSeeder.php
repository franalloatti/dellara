<?php

use Illuminate\Database\Seeder;
use App\Perfil;
use App\Permiso;
use Illuminate\Support\Facades\DB;

class PerfilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permisos = Permiso::get();
        $perfilSuperadmin = new Perfil();
        $perfilSuperadmin->nombre = 'Superadmin';
        $perfilSuperadmin->descripcion = 'Superadmin';
        $perfilSuperadmin->redirect_login = 'pedidos.index';
        $perfilSuperadmin->save();
        foreach($permisos as $permiso){
            DB::table('perfil_permiso')->insert([
                'perfil_id_perfil' => $perfilSuperadmin->id_perfil,
                'permiso_id_permiso' => $permiso->id_permiso,
                'ver' => 1,
                'crear' => 1,
                'editar' => 1,
                'borrar' => 1,
                'created_at' =>  date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
        $perfilAdmin = new Perfil();
        $perfilAdmin->nombre = 'Admin';
        $perfilAdmin->descripcion = 'Admin';
        $perfilAdmin->redirect_login = 'pedidos.index';
        $perfilAdmin->save();
        foreach($permisos as $permiso){
            DB::table('perfil_permiso')->insert([
                'perfil_id_perfil' => $perfilAdmin->id_perfil,
                'permiso_id_permiso' => $permiso->id_permiso,
                'created_at' =>  date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
        $perfilConsumidor = new Perfil();
        $perfilConsumidor->nombre = 'Consumidor final';
        $perfilConsumidor->descripcion = 'Consumidor final';
        $perfilConsumidor->redirect_login = 'pedidos.mispedidos';
        $perfilConsumidor->save();
        foreach($permisos as $permiso){
            DB::table('perfil_permiso')->insert([
                'perfil_id_perfil' => $perfilConsumidor->id_perfil,
                'permiso_id_permiso' => $permiso->id_permiso,
                'created_at' =>  date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
        $perfilMayorista = new Perfil();
        $perfilMayorista->nombre = 'Mayorista';
        $perfilMayorista->descripcion = 'Mayorista';
        $perfilMayorista->redirect_login = 'pedidos.mispedidos';
        $perfilMayorista->save();
        foreach($permisos as $permiso){
            DB::table('perfil_permiso')->insert([
                'perfil_id_perfil' => $perfilMayorista->id_perfil,
                'permiso_id_permiso' => $permiso->id_permiso,
                'created_at' =>  date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
        $perfilProveedor = new Perfil();
        $perfilProveedor->nombre = 'Proveedor';
        $perfilProveedor->descripcion = 'Proveedor';
        $perfilProveedor->redirect_login = 'pedidos.mispedidos';
        $perfilProveedor->save();
        foreach($permisos as $permiso){
            DB::table('perfil_permiso')->insert([
                'perfil_id_perfil' => $perfilProveedor->id_perfil,
                'permiso_id_permiso' => $permiso->id_permiso,
                'created_at' =>  date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
    }
}
