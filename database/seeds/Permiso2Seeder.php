<?php

use Illuminate\Database\Seeder;
use App\Permiso;

class Permiso2Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permiso = new Permiso();
        $permiso->nombre = 'Devoluciones compras';
        $permiso->descripcion = 'Gestion de devoluciones de compras';
        $permiso->save();   
    }
}
