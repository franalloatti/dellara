<?php

use Illuminate\Database\Seeder;
use App\Permiso;

class Permiso4Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permiso = new Permiso();
        $permiso->nombre = 'Facturas';
        $permiso->descripcion = 'Gestion de facturas';
        $permiso->save();  
    }
}
