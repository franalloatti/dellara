<?php

use Illuminate\Database\Seeder;
use App\Permiso;

class Permiso5Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permiso = new Permiso();
        $permiso->nombre = 'Catalogo';
        $permiso->descripcion = 'Gestion de catálogo de productos';
        $permiso->save();  
    }
}
